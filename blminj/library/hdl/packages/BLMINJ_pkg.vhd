------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        01/11/2013
Module Name:    BLMINJ_pkg

-----------
Description
-----------
    
    BLMINJ Constants etc.

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;


package BLMINJ_pkg is
    
    
    -- Header Specs
    constant BLMINJ_HEADER_size:    integer := 64;
    constant BLMINJ_CARD_NO_size:   integer := 4;
    constant BLMINJ_TYPE_size:      integer := 4;
    constant BLMINJ_SPC1_size:      integer := 8;
    constant BLMINJ_SPC2_size:      integer := 8;
    constant BLMINJ_PAGE_NO_size:   integer := 24;
    constant BLMINJ_TIMESTAMP_size: integer := 16;
    
    
    -- Card Numbers
    constant BLEDP_CARD_NO:         std_logic_vector(BLMINJ_CARD_NO_size-1 downto 0)    := "0100";
    constant BLEPM_CARD_NO:         std_logic_vector(BLMINJ_CARD_NO_size-1 downto 0)    := "0010";
    constant DAB64x_CARD_NO:        std_logic_vector(BLMINJ_CARD_NO_size-1 downto 0)    := "0001";
    
    -- Acquisition Type
    constant ACQ_TYPE:              std_logic_vector(BLMINJ_TYPE_size-1 downto 0)       := "0001";
    constant ACQ_SPC1:              std_logic_vector(BLMINJ_SPC1_size-1 downto 0)       := (others => '0');
    constant ACQ_SPC2:              std_logic_vector(BLMINJ_SPC2_size-1 downto 0)       := (others => '0');
    constant ACQ_PKG_SIZE:          integer := 10;
    
    -- SSM Type
    constant SSM_TYPE:              std_logic_vector(BLMINJ_TYPE_size-1 downto 0)       := "0011";
    constant SSM_SPC1:              std_logic_vector(BLMINJ_SPC1_size-1 downto 0)       := (others => '0');
    constant SSM_SPC2:              std_logic_vector(BLMINJ_SPC2_size-1 downto 0)       := (others => '0');
    constant SSM_PAGE_NO:           std_logic_vector(BLMINJ_PAGE_NO_size-1 downto 0)    := (others => '0');
    constant SSM_PKG_SIZE:          integer := 1;
    
    -- Processing Type
    constant PROC_TYPE:             std_logic_vector(BLMINJ_TYPE_size-1 downto 0)       := "0010";
    
    constant PROC_SPC2_LoC:         std_logic_vector(BLMINJ_SPC2_size-1 downto 0)       := "00000010";
    constant PROC_SPC2_LoBP:        std_logic_vector(BLMINJ_SPC2_size-1 downto 0)       := "00000011";
    constant PROC_SPC2_EVO:         std_logic_vector(BLMINJ_SPC2_size-1 downto 0)       := "00001001";
    constant PROC_SPC2_CAP:         std_logic_vector(BLMINJ_SPC2_size-1 downto 0)       := "00001010";
    constant PROC_SPC2_PM:          std_logic_vector(BLMINJ_SPC2_size-1 downto 0)       := "00001011";
    constant PROC_SPC2_RS:          slv_array(0 to 15)(BLMINJ_SPC2_size-1 downto 0) := ("00010000", "00010001", "00010010", "00010011", "00010100", 
                                                                                        "00010101", "00010110", "00010111", "00011000", "00011001", 
                                                                                        "00011010", "00011011", "00011100", "00011101", "00011110", 
                                                                                        "00011111");
    constant PROC_LoC_PKG_SIZE:     integer := 3;
    constant PROC_LoBP_PKG_SIZE:    integer := 3;
    constant PROC_RS_PKG_SIZE:      integer := 2;
    constant PROC_EVO_PKG_SIZE:     integer := 1;
    constant PROC_CAP_PKG_SIZE:     integer := 1;
    constant PROC_PM_PKG_SIZE:      integer := 1;
    
    
    -- Diagnostic Type
    constant DIAG_TYPE:             std_logic_vector(BLMINJ_TYPE_size-1 downto 0) := "0100";
    constant DIAG_SPC1:             std_logic_vector(BLMINJ_SPC1_size-1 downto 0) := (others => '0');
    constant DIAG_SPC2:             std_logic_vector(BLMINJ_SPC2_size-1 downto 0) := (others => '0');
    constant DIAG_PAGES_BLEDP:      integer := 4;
    constant DIAG_PAGES_BLEPM:      integer := 10;
    constant DIAG_PAGES_DAB64x:     integer := 2;
    
    constant DIAG_PKG_SIZE_16:      integer := 128;
    constant DIAG_PKG_SIZE_64:      integer := 32;
    
    
end;
 
 
package body BLMINJ_pkg is
    

end package body;
