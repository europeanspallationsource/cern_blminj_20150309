------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        01/11/2011
Module Name:    vhdl_func_pkg

-----------
Description
-----------
    
    Commonly used functions for generic design and general VHDL shortcomings

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;


package vhdl_func_pkg is
 
    -- Array
    type usign_array is array(natural range <>) of UNSIGNED;
    type slv_array  is array(natural range <>) of std_logic_vector;
    type slv_array_array is array(natural range <>) of slv_array;
    
    type int_array  is array(natural range <>) of integer;
    type time_array is array(natural range <>) of time;
    type array16b_t is array(natural range <>) of std_logic_vector(15 downto 0);
    type array8b_t is array(natural range <>) of std_logic_vector(7 downto 0);  
    type array9b_t is array(natural range <>) of std_logic_vector(8 downto 0);
    type array3b_t is array(natural range <>) of std_logic_vector(2 downto 0);
    
    -- SLV to SL conversion 
    function vectorize(s: std_logic) return std_logic_vector;
    function scalarize(v: in std_logic_vector) return std_ulogic;
    
    -- calculate max counter value
    function get_maxcnt (clk_in_ns: integer; max_timer_ns : integer) return integer;
    
    -- get vectorsize for a know maximum value 
    function get_vsize (max_value : integer) return integer;
    
    -- get vectorsize for an address 
    function get_vsize_addr (num_words : integer) return integer;
    
    -- reverse bit order of slv
    function bit_reverse(s1:std_logic_vector) return std_logic_vector;
    
    -- floor the input number at zero
    function floor_at_zero(i1: integer) return integer;
    
    
    -- multiplexing inputs using FPGA ressources more efficiently
    function eff_mux(inputs: slv_array; num_inp: integer; sel: std_logic_vector; out_size: integer) return std_logic_vector;
    
end;
 
 
package body vhdl_func_pkg is
    
    -- SLV to SL conversion
    function vectorize(s: std_logic) return std_logic_vector is
        variable v: std_logic_vector(0 downto 0);
    begin
        v(0) := s;
        return v;
    end vectorize;
    
    function scalarize(v: in std_logic_vector) return std_ulogic is
    begin
        assert v'length = 1
        report "scalarize: output port must be single bit!"
        severity FAILURE;
        return v(v'LEFT);
    end scalarize;

    -- calculate max counter value
    function get_maxcnt (clk_in_ns: integer; max_timer_ns : integer) return integer is
    begin
       return (max_timer_ns/clk_in_ns);
    end get_maxcnt;
    
    -- get vectorsize for a know maximum value 
    function get_vsize (max_value : integer) return integer is
    begin
      return INTEGER(CEIL(LOG2(REAL(max_value)))) + 1;
    end get_vsize;
    
    -- get vectorsize for an address 
    function get_vsize_addr (num_words : integer) return integer is
    begin
      return INTEGER(CEIL(LOG2(REAL(num_words))));
    end get_vsize_addr;
    
    
    
    -- reverse bit order of slv
    function bit_reverse(s1:std_logic_vector) return std_logic_vector is
        variable rr : std_logic_vector(s1'high downto s1'low);
    begin
        for ii in s1'high downto s1'low loop
            rr(ii) := s1(s1'high-ii);
        end loop;
        return rr;
    end bit_reverse;
    
    
    -- floor the input number at zero
    function floor_at_zero (i1 : integer) return integer is
    begin
        if i1 < 0 then
            return 0;
        else
            return i1;
        end if;
    end floor_at_zero;
    
    
    function eff_mux(inputs: slv_array; num_inp: integer; sel: std_logic_vector; out_size: integer) return std_logic_vector is
         variable tmp : std_logic_vector(out_size-1 downto 0);
    begin
      tmp := (others => '0');
      for I in 0 to num_inp-1 loop
        if I = TO_INTEGER(UNSIGNED(sel)) then
          tmp := tmp or inputs(I);
        end if;
      end loop;
      return tmp;
    end eff_mux;
    
    
end package body;
