------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    01/06/2013
--Module Name:    altgx_rst_control
--Project Name:   altgx_rst_control
--Description:   
--
--
--  Date            Author      Change
--  11/11/2014      MK          Added one more input synchronization stage
--                              Added output synchronization registers
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity altgx_rst_control is
generic (
	t_clk_period_ns 	: natural := 10;
	t_ltd_auto_ns		: natural := 4000;
	t_digitalreset_ns	: natural := 200;
	t_analogreset_ns	: natural := 50
);
port	(
	--local clock and reset
	clk:				in std_logic;
	nrst:				in std_logic;
	
	--transceiver status
	busy:				in std_logic;
	pll_locked:			in std_logic;
	rx_freqlocked:		in std_logic;
	
	--transceiver reset signals
	pll_areset:			out std_logic;
	tx_digitalreset:	out std_logic;
	rx_analogreset:		out std_logic;
	rx_digitalreset:	out std_logic
);
end entity altgx_rst_control;


architecture rtl of altgx_rst_control is

attribute keep: boolean;

constant t_pll_areset_ns : natural := 1100;
constant t_pll_areset_tick : natural := t_pll_areset_ns/t_clk_period_ns;
constant t_ltd_auto_tick : natural := t_ltd_auto_ns/t_clk_period_ns;
constant t_digitalgreset_tick : natural := t_digitalreset_ns/t_clk_period_ns;
constant t_analogreset_tick : natural := t_analogreset_ns/t_clk_period_ns;

signal l_busy:			std_logic;
signal l_pll_locked:	std_logic;
signal l_rx_freqlocked:	std_logic;
signal d_busy:			std_logic;
signal d_pll_locked:	std_logic;
signal d_rx_freqlocked:	std_logic;
signal l_tx_digitalreset:	std_logic;
signal l_pll_areset:		std_logic;
signal l_rx_analogreset:	std_logic;
signal l_rx_digitalreset:	std_logic;

signal rst_sequence:	std_logic;
signal rx_rst_sequence:	std_logic;

signal pllarst_cnt : integer range 0 to t_pll_areset_tick-1;
signal txdrst_cnt : integer range 0 to t_digitalgreset_tick-1;
signal rxarst_cnt : integer range 0 to t_analogreset_tick-1;
signal rxdrst_cnt : integer range 0 to t_ltd_auto_tick-1;

BEGIN
	
	assert (t_pll_areset_tick>1) report "t_pll_areset_tick should be greater than 1" severity error;
	assert (t_ltd_auto_tick>1) report "t_ltd_auto_tick should be greater than 1" severity error;
	assert (t_digitalgreset_tick>1) report "t_digitalgreset_tick should be greater than 1" severity error;
	assert (t_analogreset_tick>1) report "t_analogreset_tick should be greater than 1" severity error;
	
	--synchronise transceiver status to the local clock
	sync_in_p: process (clk)
	begin
		if rising_edge (clk) then
			if nrst = '0' then
				l_busy <= '0';
				d_busy <= '0';
				l_pll_locked <= '0';
				d_pll_locked <= '0';
				l_rx_freqlocked <= '0';
				d_rx_freqlocked <= '0';
			else
                d_busy <= busy;
				d_pll_locked <= pll_locked;
				d_rx_freqlocked <= rx_freqlocked;
				l_busy <= d_busy;
				l_pll_locked <= d_pll_locked;
				l_rx_freqlocked <= d_rx_freqlocked;
			end if;
		end if;
	end process;
	
	--start algx reset sequence when there is a general reset or altgx reconfig is busy
	rst_sequence <= l_busy or not nrst;
	
	--pll areset signal min 1us
	pllarst_p: process (clk)
	begin
		if rising_edge (clk) then
			if rst_sequence = '1' then
				pllarst_cnt <= 0;
			elsif l_pll_areset = '1' then
				pllarst_cnt <= pllarst_cnt + 1;
			end if;
            
            pll_areset <= l_pll_areset;
            
		end if;
	end process;
	l_pll_areset <= '0' when pllarst_cnt = t_pll_areset_tick-1 else '1';
	
	--after pll reset is released the pll should lock
	--release the tx_digitalreset after it is locked
	txdigrst_p: process (clk)
	begin
		if rising_edge (clk) then
			if rst_sequence = '1' or l_pll_locked = '0' then
				txdrst_cnt <= 0;
			elsif l_tx_digitalreset = '1' then
				txdrst_cnt <= txdrst_cnt + 1;
			end if;
            
            tx_digitalreset <= l_tx_digitalreset;
            
		end if;
	end process;
	l_tx_digitalreset <= '0' when txdrst_cnt = t_digitalgreset_tick-1 else '1';
	
	
	--rx analog reset signal
	--release after busy is deasserted and after the tx reset is done (not sure if the second is necessary)
	rx_rst_sequence <= l_tx_digitalreset or l_busy;
	rxarst_p: process (clk)
	begin
		if rising_edge (clk) then
			if rx_rst_sequence = '1' then
				rxarst_cnt <= 0;
			elsif l_rx_analogreset = '1' then
				rxarst_cnt <= rxarst_cnt + 1;
			end if;
            
            rx_analogreset <= l_rx_analogreset;
            
		end if;
	end process;
	l_rx_analogreset <= '0' when rxarst_cnt = t_analogreset_tick-1 else '1';
	
	
	--rx digital reset signal
	--release after rx_freqlocked is asserted + defined delay
	rxdrst_p: process (clk)
	begin
		if rising_edge (clk) then
			if l_rx_freqlocked = '0' then
				rxdrst_cnt <= 0;
			elsif l_rx_digitalreset = '1' then
				rxdrst_cnt <= rxdrst_cnt + 1;
			end if;
            
            rx_digitalreset <= l_rx_digitalreset;
            
		end if;
	end process;
	l_rx_digitalreset <= '0' when rxdrst_cnt = t_ltd_auto_tick-1 else '1';
	
	

end architecture rtl;
