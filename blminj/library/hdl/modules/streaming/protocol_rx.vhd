------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    01/07/2013
--Module Name:    protocol_rx
--Project Name:   protocol_rx
--Description:   
--
--
--  Date            Author      Change
--  12/11/2014      MK          Added FIFO underflow protection 
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity protocol_rx is
generic (
	start_kword 		: std_logic_vector(7 downto 0) := x"3C";	-- K.28.1
	stop_kword			: std_logic_vector(7 downto 0) := x"5C"		-- K.28.2
);
port	(
	--local clock and reset
	proto_clk:			in std_logic;
	proto_nrst:			in std_logic;
	
	--output port
	proto_data:				out std_logic_vector(15 downto 0);
	proto_startofpacket:	out std_logic;
	proto_endofpacket:		out std_logic;
	proto_valid:			out std_logic;
	proto_ready:			in std_logic;
	
	--proto error signal
	proto_error:			out std_logic_vector(7 downto 0);
	
	--transceiver signals
	rx_digitalreset:	in std_logic;
	rx_errdetect:		in std_logic_vector(1 downto 0);
	rx_ctrldetect:		in std_logic_vector(1 downto 0);
	rx_dataout:			in std_logic_vector(15 downto 0);
	rx_clockout:		in std_logic
	
);
end entity protocol_rx;


architecture rtl of protocol_rx is

signal fifo_wr_full : std_logic;
signal rx_error : std_logic;

signal rx_digitalreset_sync : std_logic;
signal fifo_wrrequest_mux : std_logic;
signal fifo_wrrequest : std_logic;
signal fifo_wrrequest_d1 : std_logic;

signal rx_startofpacket : std_logic;
signal rx_startofpacket_d1 : std_logic;
signal rx_startofpacket_d2 : std_logic;
signal rx_endofpacket : std_logic;
signal fifo_din : std_logic_vector(17 downto 0);
signal fifo_output : std_logic_vector(17 downto 0);

signal rx_dataout_d1 : std_logic_vector(15 downto 0);

type StateType is (rx_idle, rx_any);
signal state, next_state : StateType;

signal fifo_rdempty : std_logic;
signal fifo_rdreq : std_logic;

signal toggle_err0_en : std_logic;
signal toggle_err0 : std_logic;
signal fifo_wr_full_d1 : std_logic;
signal fifo_wr_full_ext_d1 : std_logic;
signal fifo_wr_full_ext : std_logic;
signal proto_err0 : std_logic;
signal proto_err0_d1 : std_logic;
signal proto_err0_d2 : std_logic;

BEGIN

	protocol_fifo_i : entity work.protocol_fifo 
	PORT MAP (
		data	=> fifo_din,
		rdclk	=> proto_clk,
		rdreq	=> fifo_rdreq,
		wrclk	=> rx_clockout,
		wrreq	=> fifo_wrrequest_d1,
		q	 	=> fifo_output,
		rdempty	=> fifo_rdempty,
		wrfull	=> fifo_wr_full
	);
	
	-- connect FIFO to the protocol output port
	proto_startofpacket <= fifo_output(17);
	proto_endofpacket <= fifo_output(16);
	proto_data <= fifo_output(15 downto 0);
	proto_valid <= not fifo_rdempty;
    fifo_rdreq <= proto_ready and not fifo_rdempty;
	
	-- connect pipeline signals to the input of the FIFO
	fifo_din <= rx_startofpacket_d2 & rx_endofpacket & rx_dataout_d1;
	
	--synchronise rx_digitalreset to the transceiver clock
	drst_synch_p: process(rx_clockout)
	begin
		if rising_edge(rx_clockout) then
			rx_digitalreset_sync <= rx_digitalreset;
		end if;
	end process;
	
	-- detect reception SOP and EOP
	rx_startofpacket <= '1' when rx_ctrldetect = "11" and rx_dataout = start_kword & start_kword else '0';
	rx_endofpacket <= '1' when rx_ctrldetect = "11" and rx_dataout = stop_kword & stop_kword else '0';
	
	--pipeline registers
	delay_p: process(rx_clockout)
	begin
		if rising_edge(rx_clockout) then
			rx_startofpacket_d1 <= rx_startofpacket;
			rx_startofpacket_d2 <= rx_startofpacket_d1;
			
			rx_dataout_d1 <= rx_dataout;
			
			fifo_wrrequest_d1 <= fifo_wrrequest;
		end if;
	end process;
		
	-- error in any half of the received word
	rx_error <= rx_errdetect(1) or rx_errdetect(0);
	
	-- receive protocol state machine
	-- synchronous to the transceiver clock (rx_clockout)
	fsm_cnt_cnt_p: process(rx_clockout)
	begin
		if rising_edge(rx_clockout) then
			if rx_digitalreset_sync = '1' or rx_error = '1' then
				state <= rx_idle;
			else
				state <= next_state;
			end if;
		end if;
	end process;

	fsm_cnt_cl_p: process(state, rx_startofpacket, rx_endofpacket)
	begin
		next_state <= state;
		fifo_wrrequest_mux <= '0';
		
		case state is
		
			when rx_idle =>
				if rx_startofpacket = '1' then
					next_state <= rx_any;
				end if;
			
			when rx_any =>
				fifo_wrrequest_mux <= '1';
				if rx_endofpacket = '1' then
					next_state <= rx_idle;
				end if;

		end case;
	end process;
	
	--mask fifo_wrrequest when k-word received or when FIFO is full
	fifo_wrrequest <= '0' when (state = rx_any and rx_ctrldetect = "11") or fifo_wr_full = '1' else fifo_wrrequest_mux;
	
	--report error when FIFO is full
	--error signal must be synchronous to the protocol clock domain
	err_p: process(rx_clockout)
	begin
		if rising_edge(rx_clockout) then
			
			if rx_digitalreset_sync = '1' then
				toggle_err0 <= '0';
			elsif toggle_err0_en = '1' then
				toggle_err0 <= not toggle_err0;
			end if;
		
			fifo_wr_full_d1 <= fifo_wr_full;
			fifo_wr_full_ext_d1 <= fifo_wr_full_ext;
		end if;
	end process;
	
	--fifo_wr_full extension and rising edge detection signals
	fifo_wr_full_ext <= fifo_wr_full_d1 or fifo_wr_full;
	toggle_err0_en <= fifo_wr_full_ext and not fifo_wr_full_ext_d1;
	
	err_cross_p: process(proto_clk)
	begin
		if rising_edge(proto_clk) then
			proto_err0_d1 <= toggle_err0;
			proto_err0_d2 <= proto_err0_d1;
		end if;
	end process;
	--detect rising or falling edge of toggle_err0
	proto_err0 <= (proto_err0_d1  and not proto_err0_d2) or (proto_err0_d2  and not proto_err0_d1);
	proto_error(0) <= proto_err0;
		
end architecture rtl;
