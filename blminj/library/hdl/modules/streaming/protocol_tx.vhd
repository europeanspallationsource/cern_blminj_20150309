------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    01/07/2013
--Module Name:    protocol_tx
--Project Name:   protocol_tx
--Description:   
--
--
--
--
--  Date            Author      Change
--  10/11/2014      MK          Fixed byte re-ordering pattern in the protocol_tx. According to the manual the byte pattern should appear only in the least 
--                              significant byte while it was present in both bytes. This fix the problem with data not being received after the FPGA 
--                              reprogramming which was restored after the optic fibre unplug-plug operation.
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity protocol_tx is
generic (
	start_kword 		: std_logic_vector(7 downto 0) := x"3C";	-- K.28.1
	stop_kword			: std_logic_vector(7 downto 0) := x"5C";	-- K.28.2
	byteorder_kword		: std_logic_vector(7 downto 0) := x"FB";	-- K.27.7
	idle_kword			: std_logic_vector(7 downto 0) := x"BC"		-- K.28.5
);
port	(
	--local clock and reset
	proto_clk:			in std_logic;
	proto_nrst:			in std_logic;
	
	--input port
	proto_data:				in std_logic_vector(15 downto 0);
	proto_startofpacket:	in std_logic;
	proto_endofpacket:		in std_logic;
	proto_valid:			in std_logic;
	proto_ready:			out std_logic;
	
	--transceiver signals
	tx_digitalreset:	in std_logic;
	tx_cntrlenable:		out std_logic_vector(1 downto 0);
	tx_datain:			out std_logic_vector(15 downto 0);
	tx_clockout:		in std_logic
);
end entity protocol_tx;


architecture rtl of protocol_tx is

signal fifo_din : std_logic_vector(17 downto 0);
signal fifo_wr_full : std_logic;

signal tx_digitalreset_sync : std_logic := '0';	-- initialisation just for the simulation purpose
signal fifo_rdempty : std_logic;
signal fifo_rdrequest : std_logic;
signal fifo_rdrequest_mux : std_logic;
signal fifo_output : std_logic_vector(17 downto 0);
signal tx_datain_mux : std_logic_vector(15 downto 0);
signal tx_cntrlenable_mux : std_logic_vector(1 downto 0);

type StateType is (tx_idle_kword, tx_byteorder_kword, tx_start_kword, tx_stop_kword, tx_dword, tx_error);
signal state, next_state : StateType;

signal fifo_startofpacket : std_logic;
signal fifo_endofpacket : std_logic;
signal fifo_data : std_logic_vector(15 downto 0);

BEGIN
	
	-- combine SOP/EOP with data
	fifo_din <= proto_startofpacket & proto_endofpacket & proto_data;
	
	-- if fifo is full the input is not ready
	proto_ready <= not fifo_wr_full;
	
	-- put data to FIFO when it is valid (proto_valid)
	-- write side is synchronous to the system clock
	-- read side is synchronous to the transciver clock (tx_clockout)
	protocol_fifo_i : entity work.protocol_fifo 
	PORT MAP (
		data	=> fifo_din,
		rdclk	=> tx_clockout,
		rdreq	=> fifo_rdrequest,
		wrclk	=> proto_clk,
		wrreq	=> proto_valid,
		q	 	=> fifo_output,
		rdempty	=> fifo_rdempty,
		wrfull	=> fifo_wr_full
	);
	
	--synchronise tx_digitalreset to the transceiver clock
	drst_synch_p: process(tx_clockout)
	begin
		if rising_edge(tx_clockout) then
			tx_digitalreset_sync <= not tx_digitalreset;
		end if;
	end process;
	
	-- split output (read side) of the fifo to SOP/EOP and data
	fifo_startofpacket <= fifo_output(17);
	fifo_endofpacket <= fifo_output(16);
	fifo_data <= fifo_output(15 downto 0);
	
	
	-- transmit protocol state machine
	-- synchronous to the transceiver clock (tx_clockout)
	fsm_cnt_cnt_p: process(tx_clockout)
	begin
		if rising_edge(tx_clockout) then
			if tx_digitalreset_sync = '0' then
				state <= tx_idle_kword;
			else
				state <= next_state;
			end if;
		end if;
	end process;

	fsm_cnt_cl_p: process(state, fifo_rdempty, fifo_startofpacket, fifo_endofpacket, fifo_data)
	begin
		next_state <= state;
		fifo_rdrequest_mux <= '0';
		tx_datain_mux <= idle_kword & idle_kword;
		tx_cntrlenable_mux <= "11";
		
		case state is
		
			when tx_byteorder_kword =>
				tx_datain_mux <= x"00" & byteorder_kword;
				tx_cntrlenable_mux <= "01";
				next_state <= tx_idle_kword;
			
			when tx_idle_kword =>
				tx_datain_mux <= idle_kword & idle_kword;
				tx_cntrlenable_mux <= "11";
				if fifo_rdempty = '0' and fifo_startofpacket = '1' then
					next_state <= tx_start_kword;
				elsif fifo_rdempty = '0' then
					next_state <= tx_error;			-- if not SOP go to error and remove from FIFO
				else
					next_state <= tx_byteorder_kword;
				end if;
			
			when tx_start_kword =>
				tx_datain_mux <= start_kword & start_kword;
				tx_cntrlenable_mux <= "11";
				next_state <= tx_dword;
			
			-- in this implementation
			-- second SOP is ignored if it appears in the FIFO
			-- i.e. it is not causing start_kwords to be transmitted again
			when tx_dword =>				
				fifo_rdrequest_mux <= '1';		
				tx_datain_mux <= fifo_data;
				tx_cntrlenable_mux <= "00";
				if fifo_endofpacket = '1' and fifo_rdempty = '0' then		-- fifo_rdempty = '0' condition necessary when the fifo gets empty just before last dword appears
					next_state <= tx_stop_kword;							-- without this the fsm will go straight to tx_stop_kword and the last dword won't be transmitted
				end if;
				
			when tx_stop_kword =>
				tx_datain_mux <= stop_kword & stop_kword;
				tx_cntrlenable_mux <= "11";
				next_state <= tx_byteorder_kword;

			when tx_error =>
				tx_datain_mux <= idle_kword & idle_kword;		-- transmit idle
				tx_cntrlenable_mux <= "11";
				fifo_rdrequest_mux <= '1';						-- remove from FIFO
				next_state <= tx_idle_kword;
			
		end case;
	end process;
	
	-- when fifo gets empty don't read it and transmit idle kwords
	fifo_rdrequest <= '0' when state = tx_dword and fifo_rdempty = '1' else fifo_rdrequest_mux;
	tx_datain <= idle_kword & idle_kword when state = tx_dword and fifo_rdempty = '1' else tx_datain_mux;
	tx_cntrlenable <= "11" when state = tx_dword and fifo_rdempty = '1' else tx_cntrlenable_mux;


end architecture rtl;
