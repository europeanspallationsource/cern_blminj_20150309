--------------------------------------------------------------------------------
-- Copyright (C) 1999-2008 Easics NV.
-- This source file may be used and distributed without restriction
-- provided that this copyright statement is not removed from the file
-- and that any derivative work contains the original copyright notice
-- and the associated disclaimer.
--
-- THIS SOURCE FILE IS PROVIDED "AS IS" AND WITHOUT ANY EXPRESS
-- OR IMPLIED WARRANTIES, INCLUDING, WITHOUT LIMITATION, THE IMPLIED
-- WARRANTIES OF MERCHANTIBILITY AND FITNESS FOR A PARTICULAR PURPOSE.
--
-- Purpose : synthesizable CRC function
--   * polynomial: (0 1 2 4 5 7 8 10 11 12 16 22 23 26 32)
--   * Data width: 16
--
-- Info : tools@easics.be
--        http://www.easics.com
--------------------------------------------------------------------------------

------------------------------
--Modified by:
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    04/07/2013
--Module Name:    CRC32_D16
--Project Name:   CRC32_D16
--Description:   
--
--
------------------------------

library ieee;
use ieee.std_logic_1164.all;

-- polynomial: (0 1 2 4 5 7 8 10 11 12 16 22 23 26 32)
-- Data width: 16
-- convention: the first serial bit is D[15]

entity CRC32_D16 is
port (
	D: in std_logic_vector(15 downto 0);
	crc: in std_logic_vector(31 downto 0);
	newcrc: out std_logic_vector(31 downto 0)
);
end CRC32_D16;

architecture rtl of CRC32_D16 is
begin

    newcrc(0) <= D(12) xor D(10) xor D(9) xor D(6) xor D(0) xor crc(16) xor crc(22) xor crc(25) xor crc(26) xor crc(28);
    newcrc(1) <= D(13) xor D(12) xor D(11) xor D(9) xor D(7) xor D(6) xor D(1) xor D(0) xor crc(16) xor crc(17) xor crc(22) xor crc(23) xor crc(25) xor crc(27) xor crc(28) xor crc(29);
    newcrc(2) <= D(14) xor D(13) xor D(9) xor D(8) xor D(7) xor D(6) xor D(2) xor D(1) xor D(0) xor crc(16) xor crc(17) xor crc(18) xor crc(22) xor crc(23) xor crc(24) xor crc(25) xor crc(29) xor crc(30);
    newcrc(3) <= D(15) xor D(14) xor D(10) xor D(9) xor D(8) xor D(7) xor D(3) xor D(2) xor D(1) xor crc(17) xor crc(18) xor crc(19) xor crc(23) xor crc(24) xor crc(25) xor crc(26) xor crc(30) xor crc(31);
    newcrc(4) <= D(15) xor D(12) xor D(11) xor D(8) xor D(6) xor D(4) xor D(3) xor D(2) xor D(0) xor crc(16) xor crc(18) xor crc(19) xor crc(20) xor crc(22) xor crc(24) xor crc(27) xor crc(28) xor crc(31);
    newcrc(5) <= D(13) xor D(10) xor D(7) xor D(6) xor D(5) xor D(4) xor D(3) xor D(1) xor D(0) xor crc(16) xor crc(17) xor crc(19) xor crc(20) xor crc(21) xor crc(22) xor crc(23) xor crc(26) xor crc(29);
    newcrc(6) <= D(14) xor D(11) xor D(8) xor D(7) xor D(6) xor D(5) xor D(4) xor D(2) xor D(1) xor crc(17) xor crc(18) xor crc(20) xor crc(21) xor crc(22) xor crc(23) xor crc(24) xor crc(27) xor crc(30);
    newcrc(7) <= D(15) xor D(10) xor D(8) xor D(7) xor D(5) xor D(3) xor D(2) xor D(0) xor crc(16) xor crc(18) xor crc(19) xor crc(21) xor crc(23) xor crc(24) xor crc(26) xor crc(31);
    newcrc(8) <= D(12) xor D(11) xor D(10) xor D(8) xor D(4) xor D(3) xor D(1) xor D(0) xor crc(16) xor crc(17) xor crc(19) xor crc(20) xor crc(24) xor crc(26) xor crc(27) xor crc(28);
    newcrc(9) <= D(13) xor D(12) xor D(11) xor D(9) xor D(5) xor D(4) xor D(2) xor D(1) xor crc(17) xor crc(18) xor crc(20) xor crc(21) xor crc(25) xor crc(27) xor crc(28) xor crc(29);
    newcrc(10) <= D(14) xor D(13) xor D(9) xor D(5) xor D(3) xor D(2) xor D(0) xor crc(16) xor crc(18) xor crc(19) xor crc(21) xor crc(25) xor crc(29) xor crc(30);
    newcrc(11) <= D(15) xor D(14) xor D(12) xor D(9) xor D(4) xor D(3) xor D(1) xor D(0) xor crc(16) xor crc(17) xor crc(19) xor crc(20) xor crc(25) xor crc(28) xor crc(30) xor crc(31);
    newcrc(12) <= D(15) xor D(13) xor D(12) xor D(9) xor D(6) xor D(5) xor D(4) xor D(2) xor D(1) xor D(0) xor crc(16) xor crc(17) xor crc(18) xor crc(20) xor crc(21) xor crc(22) xor crc(25) xor crc(28) xor crc(29) xor crc(31);
    newcrc(13) <= D(14) xor D(13) xor D(10) xor D(7) xor D(6) xor D(5) xor D(3) xor D(2) xor D(1) xor crc(17) xor crc(18) xor crc(19) xor crc(21) xor crc(22) xor crc(23) xor crc(26) xor crc(29) xor crc(30);
    newcrc(14) <= D(15) xor D(14) xor D(11) xor D(8) xor D(7) xor D(6) xor D(4) xor D(3) xor D(2) xor crc(18) xor crc(19) xor crc(20) xor crc(22) xor crc(23) xor crc(24) xor crc(27) xor crc(30) xor crc(31);
    newcrc(15) <= D(15) xor D(12) xor D(9) xor D(8) xor D(7) xor D(5) xor D(4) xor D(3) xor crc(19) xor crc(20) xor crc(21) xor crc(23) xor crc(24) xor crc(25) xor crc(28) xor crc(31);
    newcrc(16) <= D(13) xor D(12) xor D(8) xor D(5) xor D(4) xor D(0) xor crc(0) xor crc(16) xor crc(20) xor crc(21) xor crc(24) xor crc(28) xor crc(29);
    newcrc(17) <= D(14) xor D(13) xor D(9) xor D(6) xor D(5) xor D(1) xor crc(1) xor crc(17) xor crc(21) xor crc(22) xor crc(25) xor crc(29) xor crc(30);
    newcrc(18) <= D(15) xor D(14) xor D(10) xor D(7) xor D(6) xor D(2) xor crc(2) xor crc(18) xor crc(22) xor crc(23) xor crc(26) xor crc(30) xor crc(31);
    newcrc(19) <= D(15) xor D(11) xor D(8) xor D(7) xor D(3) xor crc(3) xor crc(19) xor crc(23) xor crc(24) xor crc(27) xor crc(31);
    newcrc(20) <= D(12) xor D(9) xor D(8) xor D(4) xor crc(4) xor crc(20) xor crc(24) xor crc(25) xor crc(28);
    newcrc(21) <= D(13) xor D(10) xor D(9) xor D(5) xor crc(5) xor crc(21) xor crc(25) xor crc(26) xor crc(29);
    newcrc(22) <= D(14) xor D(12) xor D(11) xor D(9) xor D(0) xor crc(6) xor crc(16) xor crc(25) xor crc(27) xor crc(28) xor crc(30);
    newcrc(23) <= D(15) xor D(13) xor D(9) xor D(6) xor D(1) xor D(0) xor crc(7) xor crc(16) xor crc(17) xor crc(22) xor crc(25) xor crc(29) xor crc(31);
    newcrc(24) <= D(14) xor D(10) xor D(7) xor D(2) xor D(1) xor crc(8) xor crc(17) xor crc(18) xor crc(23) xor crc(26) xor crc(30);
    newcrc(25) <= D(15) xor D(11) xor D(8) xor D(3) xor D(2) xor crc(9) xor crc(18) xor crc(19) xor crc(24) xor crc(27) xor crc(31);
    newcrc(26) <= D(10) xor D(6) xor D(4) xor D(3) xor D(0) xor crc(10) xor crc(16) xor crc(19) xor crc(20) xor crc(22) xor crc(26);
    newcrc(27) <= D(11) xor D(7) xor D(5) xor D(4) xor D(1) xor crc(11) xor crc(17) xor crc(20) xor crc(21) xor crc(23) xor crc(27);
    newcrc(28) <= D(12) xor D(8) xor D(6) xor D(5) xor D(2) xor crc(12) xor crc(18) xor crc(21) xor crc(22) xor crc(24) xor crc(28);
    newcrc(29) <= D(13) xor D(9) xor D(7) xor D(6) xor D(3) xor crc(13) xor crc(19) xor crc(22) xor crc(23) xor crc(25) xor crc(29);
    newcrc(30) <= D(14) xor D(10) xor D(8) xor D(7) xor D(4) xor crc(14) xor crc(20) xor crc(23) xor crc(24) xor crc(26) xor crc(30);
    newcrc(31) <= D(15) xor D(11) xor D(9) xor D(8) xor D(5) xor crc(15) xor crc(21) xor crc(24) xor crc(25) xor crc(27) xor crc(31);
	
end architecture rtl;
