------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    20/11/2014
--Module Name:    acq_packet_decoder
--Project Name:   acq_packet_decoder
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity acq_packet_decoder is
port    (
    --clock and reset
    gen_clk:            in std_logic;
    gen_nrst:           in std_logic;
    
    --acquisition channel outputs
    acq_ch1:            out std_logic_vector(19 downto 0);
    acq_ch2:            out std_logic_vector(19 downto 0);
    acq_ch3:            out std_logic_vector(19 downto 0);
    acq_ch4:            out std_logic_vector(19 downto 0);
    acq_ch5:            out std_logic_vector(19 downto 0);
    acq_ch6:            out std_logic_vector(19 downto 0);
    acq_ch7:            out std_logic_vector(19 downto 0);
    acq_ch8:            out std_logic_vector(19 downto 0);
    acq_en:             out std_logic;
    
    --header information
    dec_timestamp:      out std_logic_vector(23 downto 0);
    dec_card_no:        out std_logic_vector(7 downto 0);
    dec_pkt_type:       out std_logic_vector(7 downto 0);
    
    --decoder input port
    dec_data:           in std_logic_vector(15 downto 0);
    dec_startofpacket:  in std_logic;
    dec_endofpacket:    in std_logic;
    dec_valid:          in std_logic;
    dec_ready:          out std_logic
);
end entity acq_packet_decoder;


architecture rtl of acq_packet_decoder is

signal crc_calc_en: std_logic;
signal crc_calc_rst: std_logic;
signal crc_reg: std_logic_vector(31 downto 0);
signal newcrc: std_logic_vector(31 downto 0);

signal gen_enabled: std_logic;

signal pkt_dwords_valid: std_logic;
signal pkt_dwords_done: std_logic;
signal pkt_dwords_rst: std_logic;
signal pkt_dwords_cnt: integer range 0 to 16 := 0;    --initial value for the simulation

signal gen_data_mux: std_logic_vector(15 downto 0);

BEGIN    

    -- enable register
	en_p: process(gen_clk)
	begin
		if rising_edge(gen_clk) then
			if gen_nrst = '0' or pkt_dwords_rst = '1' then
				gen_enabled <= '0';
			elsif gen_enable = '1' then
				gen_enabled <= '1';
			end if;
		end if;
	end process;
    
    -- packet dwords counter
    pdw_cnt_p: process(gen_clk)
    begin
        if rising_edge(gen_clk) then
            if gen_nrst = '0' or pkt_dwords_rst = '1' then
                pkt_dwords_cnt <= 16;
            elsif gen_ready = '1' and gen_enabled = '1' then
                pkt_dwords_cnt <= pkt_dwords_cnt - 1;
            end if;
        end if;
    end process;
    pkt_dwords_valid <= '1' when pkt_dwords_cnt <= 15 and pkt_dwords_cnt >= 0 else '0';
    pkt_dwords_rst <= '1' when pkt_dwords_cnt = 0 else '0';
    pkt_dwords_done <= '1' when pkt_dwords_cnt = 16 else '0';
    gen_done <= pkt_dwords_done;
    
    
    -- multiplex packet dowrds and SOP/EOP
    gen_data_mux <= gen_card_no & gen_pkt_type                      when pkt_dwords_cnt = 15 else
                    x"00" & gen_timestamp(23 downto 16)             when pkt_dwords_cnt = 14 else
                    gen_timestamp(15 downto 0)                      when pkt_dwords_cnt = 13 else
                    x"0000"                                         when pkt_dwords_cnt = 12 else
                    acq_ch1(19 downto 4)                            when pkt_dwords_cnt = 11 else
                    acq_ch1(3 downto 0) & acq_ch2(19 downto 8)      when pkt_dwords_cnt = 10 else
                    acq_ch2(7 downto 0) & acq_ch3(19 downto 12)     when pkt_dwords_cnt = 9 else
                    acq_ch3(11 downto 0) & acq_ch4(19 downto 16)    when pkt_dwords_cnt = 8 else
                    acq_ch4(15 downto 0)                            when pkt_dwords_cnt = 7 else
                    acq_ch5(19 downto 4)                            when pkt_dwords_cnt = 6 else
                    acq_ch5(3 downto 0) & acq_ch6(19 downto 8)      when pkt_dwords_cnt = 5 else
                    acq_ch6(7 downto 0) & acq_ch7(19 downto 12)     when pkt_dwords_cnt = 4 else
                    acq_ch7(11 downto 0) & acq_ch8(19 downto 16)    when pkt_dwords_cnt = 3 else
                    acq_ch8(15 downto 0)                            when pkt_dwords_cnt = 2 else
                    crc_reg(31 downto 16)                           when pkt_dwords_cnt = 1 else
                    crc_reg(15 downto 0)                            when pkt_dwords_cnt = 0 else
                    x"0000";
    
    gen_data <= gen_data_mux;
    gen_startofpacket <= '1' when pkt_dwords_cnt = 15 else '0';
    gen_endofpacket <= '1' when pkt_dwords_cnt = 0 else '0';
    gen_valid <= pkt_dwords_valid and gen_ready;
    
    
    -- crc enable register
    crc_calc_en <= '1' when pkt_dwords_cnt > 1 and gen_ready = '1' else '0';
    crc_calc_rst <= pkt_dwords_done;
    -- crc register
    crc_p: process(gen_clk)
    begin
        if rising_edge(gen_clk) then
            if gen_nrst = '0' or crc_calc_rst = '1' then
                crc_reg <= (others => '1');
            elsif crc_calc_en = '1' then
                crc_reg <= newcrc;
            end if;
        end if;
    end process;
    -- crc combinatorial "gate"
    CRC32_D16_i : entity work.CRC32_D16 
    PORT MAP (
        D        => gen_data_mux,
        crc        => crc_reg,
        newcrc    => newcrc
    );
    
    
        
end architecture rtl;
