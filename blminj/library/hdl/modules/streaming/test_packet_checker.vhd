------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    05/07/2013
--Module Name:    test_packet_checker
--Project Name:   test_packet_checker
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity test_packet_checker is
port	(
	--clock and reset
	chk_clk:			in std_logic;
	chk_nrst:			in std_logic;
	
	--checker input port
	chk_data:			in std_logic_vector(15 downto 0);
	chk_startofpacket:	in std_logic;
	chk_endofpacket:	in std_logic;
	chk_valid:			in std_logic;
	chk_ready:			out std_logic;
	
	--checker output
	chk_no_err:			out std_logic;
	chk_header:			out std_logic_vector(15 downto 0);
	chk_sequence:		out std_logic_vector(31 downto 0);
	chk_length:			out std_logic_vector(15 downto 0);
	chk_crc_err:		out std_logic;
	chk_hdr_err:		out std_logic;
	chk_seq_err:		out std_logic;
	chk_sop_err:		out std_logic;
	chk_eop_err:		out std_logic
);
end entity test_packet_checker;


architecture rtl of test_packet_checker is

signal chk_endofpacket_d1: std_logic;
signal chk_valid_d1: std_logic;

signal pkt_dwords_cnt: unsigned(15 downto 0);
signal length_reg: std_logic_vector(15 downto 0);

signal seq_received: std_logic_vector(31 downto 0);
signal seq_received_prev: std_logic_vector(31 downto 0);
signal seq_received_flag: std_logic;

signal hdr_received: std_logic_vector(15 downto 0);
signal hdr_received_prev: std_logic_vector(15 downto 0);

signal crc_calc_en: std_logic;
signal crc_reg: std_logic_vector(31 downto 0);
signal crc_reg_mux: std_logic_vector(31 downto 0);
signal newcrc: std_logic_vector(31 downto 0);

signal chk_crc_err_d0: std_logic;
signal chk_hdr_err_d0: std_logic;
signal chk_hdr_err_d1: std_logic;
signal chk_seq_err_d0: std_logic;
signal chk_seq_err_d1: std_logic;


BEGIN

	-- always ready
	--chk_ready <= chk_valid;
	chk_ready <= not (chk_endofpacket_d1 and chk_valid_d1);
	
	-- delay eop signal by one clock cycle 
	eop_reg_p: process(chk_clk)
	begin
		if rising_edge(chk_clk) then
			chk_endofpacket_d1 <= chk_endofpacket;
			chk_valid_d1 <= chk_valid;
			chk_hdr_err_d1 <= chk_hdr_err_d0;
			chk_seq_err_d1 <= chk_seq_err_d0;
		end if;
	end process;
	
	-- no error signal when a packet is correct
	chk_no_err <= '1' when chk_endofpacket_d1 = '1' and chk_valid_d1 = '1' and chk_crc_err_d0 = '0' and chk_hdr_err_d1 = '0' and chk_seq_err_d1 = '0' else '0';
	
	-- packet dwords counter and register
	pdw_cnt_p: process(chk_clk)
	begin
		if rising_edge(chk_clk) then
			if chk_nrst = '0' or (chk_endofpacket_d1 = '1' and chk_valid_d1 = '1') then
				pkt_dwords_cnt <= (others=>'0');
			elsif chk_valid = '1' then
				pkt_dwords_cnt <= pkt_dwords_cnt + 1;
			end if;	
			
			if chk_endofpacket_d1 = '1' and chk_valid_d1 = '1' then
				length_reg <= std_logic_vector(pkt_dwords_cnt);
			end if;	
			
		end if;
	end process;
	chk_length <= length_reg;
	
	-- crc enable register
	crc_calc_en <= chk_valid;
	-- crc register
	crc_p: process(chk_clk)
	begin
		if rising_edge(chk_clk) then
			if chk_nrst = '0' then
				crc_reg <= (others => '0');
			elsif crc_calc_en = '1' then
				crc_reg <= newcrc;
			end if;
		end if;
	end process;
	-- crc combinatorial "gate"
	CRC32_D16_i : entity work.CRC32_D16 
	PORT MAP (
		D		=> chk_data,
		crc		=> crc_reg_mux,
		newcrc	=> newcrc
	);
	-- check for the CRC error
	chk_crc_err_d0 <= '1' when chk_endofpacket_d1 = '1' and chk_valid_d1 = '1' and unsigned(crc_reg) /= 0 else '0';
	chk_crc_err <= chk_crc_err_d0;
	crc_reg_mux <= x"00000000" when pkt_dwords_cnt = 0 else crc_reg;
	
	
	-- received sequence number register and previous value register
	seq_rcv_p: process(chk_clk)
	begin
		if rising_edge(chk_clk) then
			if chk_nrst = '0' or (chk_endofpacket_d1 = '1' and chk_valid_d1 = '1') then
				seq_received <= (others => '0');
				seq_received_flag <= '0';
			elsif chk_valid = '1' and pkt_dwords_cnt = 1 then
				seq_received(31 downto 16) <= chk_data;
			elsif chk_valid = '1' and pkt_dwords_cnt = 2 then
				seq_received(15 downto 0) <= chk_data;
			end if;
			
			if chk_endofpacket_d1 = '1' and chk_valid_d1 = '1' then
				seq_received_prev <= seq_received;
				seq_received_flag <= '1';
			end if;
			
		end if;
	end process;
	-- check for the sequence error
	chk_seq_err_d0 <= '1' when chk_endofpacket = '1' and chk_valid = '1' and unsigned(seq_received) /= unsigned(seq_received_prev) + 1 and seq_received_flag = '1' else '0';
	chk_seq_err <= chk_seq_err_d0;
	chk_sequence <= seq_received_prev;
	
	
	-- received header register and previous value register
	hdr_rcv_p: process(chk_clk)
	begin
		if rising_edge(chk_clk) then
			if chk_nrst = '0' or (chk_endofpacket_d1 = '1' and chk_valid_d1 = '1') then
				hdr_received <= (others => '0');
			elsif chk_valid = '1' and pkt_dwords_cnt = 0 then
				hdr_received <= chk_data;
			end if;
			
			if chk_endofpacket_d1 = '1' and chk_valid_d1 = '1' then
				hdr_received_prev <= hdr_received;
			end if;
			
		end if;
	end process;
	-- check for the header error
	chk_hdr_err_d0 <= '1' when chk_endofpacket = '1' and chk_valid = '1' and unsigned(hdr_received) /= unsigned(hdr_received_prev) and seq_received_flag = '1' else '0';
	chk_hdr_err <= chk_hdr_err_d0;
	chk_header <= hdr_received;
	
	-- mission SOP error
	chk_sop_err <= '1' when chk_valid = '1' and pkt_dwords_cnt = 0 and chk_startofpacket = '0' else '0';
	-- missing EOP error (SOP twice without EOP in between)
	--chk_eop_err <= '1' when chk_valid = '1' and pkt_dwords_cnt /= 0 and chk_startofpacket = '1' else '0';
	chk_eop_err <= '1' when chk_valid = '1' and pkt_dwords_cnt /= 0 and chk_startofpacket = '1' and chk_endofpacket_d1 = '0' and chk_valid_d1 = '0' else '0';
	
		
end architecture rtl;
