library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use ieee.math_real.all;
use STD.textio.all;
use IEEE.STD_LOGIC_TEXTIO.all;

entity TB_packet_validator is
end TB_packet_validator;


architecture behaviour of TB_packet_validator is

constant CLOCK_PERIOD : time := 20 ns;
constant console_print : boolean := true;

signal done_sim          : STD_LOGIC;

signal clk          : STD_LOGIC;
signal nrst         : STD_LOGIC;


signal timestamp_val:       unsigned(23 downto 0) := (others=>'0');

signal in_data:             std_logic_vector(15 downto 0);
signal in_startofpacket:    std_logic;
signal in_endofpacket:      std_logic;
signal in_valid:            std_logic;
signal in_ready:            std_logic; 
signal out_data:            std_logic_vector(15 downto 0);
signal out_startofpacket:   std_logic;
signal out_endofpacket:     std_logic;
signal out_valid:           std_logic;
signal out_ready:           std_logic; 
signal packet_length:       std_logic_vector(7 downto 0);
signal packet_card_no:      std_logic_vector(3 downto 0);
signal packet_type_no:      std_logic_vector(3 downto 0);
signal packet_timestamp:    std_logic_vector(23 downto 0);
signal valid_packet:        std_logic;
signal err_drop_word:       std_logic;
signal err_drop_bad_crc:    std_logic;
signal err_drop_no_eop:     std_logic;
signal err_tmp_fifo_full:   std_logic;

signal in_fifo_din : std_logic_vector(17 downto 0);
signal in_fifo_output : std_logic_vector(17 downto 0);
signal in_fifo_wrreq : std_logic;
signal in_fifo_rdempty : std_logic;
signal in_fifo_full : std_logic;

signal valid_fifo_din : std_logic_vector(17 downto 0);
signal valid_fifo_output : std_logic_vector(17 downto 0);
signal valid_fifo_rdreq : std_logic;
signal valid_fifo_wrreq : std_logic;
signal valid_fifo_rdempty : std_logic;
signal valid_fifo_full : std_logic;

        
--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------    Testing functions  ------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------

  function nextCRC32_D16
    (Data: std_logic_vector(15 downto 0);
     crc:  std_logic_vector(31 downto 0))
    return std_logic_vector is

    variable d:      std_logic_vector(15 downto 0);
    variable c:      std_logic_vector(31 downto 0);
    variable newcrc: std_logic_vector(31 downto 0);

  begin
    d := Data;
    c := crc;

    newcrc(0) := d(12) xor d(10) xor d(9) xor d(6) xor d(0) xor c(16) xor c(22) xor c(25) xor c(26) xor c(28);
    newcrc(1) := d(13) xor d(12) xor d(11) xor d(9) xor d(7) xor d(6) xor d(1) xor d(0) xor c(16) xor c(17) xor c(22) xor c(23) xor c(25) xor c(27) xor c(28) xor c(29);
    newcrc(2) := d(14) xor d(13) xor d(9) xor d(8) xor d(7) xor d(6) xor d(2) xor d(1) xor d(0) xor c(16) xor c(17) xor c(18) xor c(22) xor c(23) xor c(24) xor c(25) xor c(29) xor c(30);
    newcrc(3) := d(15) xor d(14) xor d(10) xor d(9) xor d(8) xor d(7) xor d(3) xor d(2) xor d(1) xor c(17) xor c(18) xor c(19) xor c(23) xor c(24) xor c(25) xor c(26) xor c(30) xor c(31);
    newcrc(4) := d(15) xor d(12) xor d(11) xor d(8) xor d(6) xor d(4) xor d(3) xor d(2) xor d(0) xor c(16) xor c(18) xor c(19) xor c(20) xor c(22) xor c(24) xor c(27) xor c(28) xor c(31);
    newcrc(5) := d(13) xor d(10) xor d(7) xor d(6) xor d(5) xor d(4) xor d(3) xor d(1) xor d(0) xor c(16) xor c(17) xor c(19) xor c(20) xor c(21) xor c(22) xor c(23) xor c(26) xor c(29);
    newcrc(6) := d(14) xor d(11) xor d(8) xor d(7) xor d(6) xor d(5) xor d(4) xor d(2) xor d(1) xor c(17) xor c(18) xor c(20) xor c(21) xor c(22) xor c(23) xor c(24) xor c(27) xor c(30);
    newcrc(7) := d(15) xor d(10) xor d(8) xor d(7) xor d(5) xor d(3) xor d(2) xor d(0) xor c(16) xor c(18) xor c(19) xor c(21) xor c(23) xor c(24) xor c(26) xor c(31);
    newcrc(8) := d(12) xor d(11) xor d(10) xor d(8) xor d(4) xor d(3) xor d(1) xor d(0) xor c(16) xor c(17) xor c(19) xor c(20) xor c(24) xor c(26) xor c(27) xor c(28);
    newcrc(9) := d(13) xor d(12) xor d(11) xor d(9) xor d(5) xor d(4) xor d(2) xor d(1) xor c(17) xor c(18) xor c(20) xor c(21) xor c(25) xor c(27) xor c(28) xor c(29);
    newcrc(10) := d(14) xor d(13) xor d(9) xor d(5) xor d(3) xor d(2) xor d(0) xor c(16) xor c(18) xor c(19) xor c(21) xor c(25) xor c(29) xor c(30);
    newcrc(11) := d(15) xor d(14) xor d(12) xor d(9) xor d(4) xor d(3) xor d(1) xor d(0) xor c(16) xor c(17) xor c(19) xor c(20) xor c(25) xor c(28) xor c(30) xor c(31);
    newcrc(12) := d(15) xor d(13) xor d(12) xor d(9) xor d(6) xor d(5) xor d(4) xor d(2) xor d(1) xor d(0) xor c(16) xor c(17) xor c(18) xor c(20) xor c(21) xor c(22) xor c(25) xor c(28) xor c(29) xor c(31);
    newcrc(13) := d(14) xor d(13) xor d(10) xor d(7) xor d(6) xor d(5) xor d(3) xor d(2) xor d(1) xor c(17) xor c(18) xor c(19) xor c(21) xor c(22) xor c(23) xor c(26) xor c(29) xor c(30);
    newcrc(14) := d(15) xor d(14) xor d(11) xor d(8) xor d(7) xor d(6) xor d(4) xor d(3) xor d(2) xor c(18) xor c(19) xor c(20) xor c(22) xor c(23) xor c(24) xor c(27) xor c(30) xor c(31);
    newcrc(15) := d(15) xor d(12) xor d(9) xor d(8) xor d(7) xor d(5) xor d(4) xor d(3) xor c(19) xor c(20) xor c(21) xor c(23) xor c(24) xor c(25) xor c(28) xor c(31);
    newcrc(16) := d(13) xor d(12) xor d(8) xor d(5) xor d(4) xor d(0) xor c(0) xor c(16) xor c(20) xor c(21) xor c(24) xor c(28) xor c(29);
    newcrc(17) := d(14) xor d(13) xor d(9) xor d(6) xor d(5) xor d(1) xor c(1) xor c(17) xor c(21) xor c(22) xor c(25) xor c(29) xor c(30);
    newcrc(18) := d(15) xor d(14) xor d(10) xor d(7) xor d(6) xor d(2) xor c(2) xor c(18) xor c(22) xor c(23) xor c(26) xor c(30) xor c(31);
    newcrc(19) := d(15) xor d(11) xor d(8) xor d(7) xor d(3) xor c(3) xor c(19) xor c(23) xor c(24) xor c(27) xor c(31);
    newcrc(20) := d(12) xor d(9) xor d(8) xor d(4) xor c(4) xor c(20) xor c(24) xor c(25) xor c(28);
    newcrc(21) := d(13) xor d(10) xor d(9) xor d(5) xor c(5) xor c(21) xor c(25) xor c(26) xor c(29);
    newcrc(22) := d(14) xor d(12) xor d(11) xor d(9) xor d(0) xor c(6) xor c(16) xor c(25) xor c(27) xor c(28) xor c(30);
    newcrc(23) := d(15) xor d(13) xor d(9) xor d(6) xor d(1) xor d(0) xor c(7) xor c(16) xor c(17) xor c(22) xor c(25) xor c(29) xor c(31);
    newcrc(24) := d(14) xor d(10) xor d(7) xor d(2) xor d(1) xor c(8) xor c(17) xor c(18) xor c(23) xor c(26) xor c(30);
    newcrc(25) := d(15) xor d(11) xor d(8) xor d(3) xor d(2) xor c(9) xor c(18) xor c(19) xor c(24) xor c(27) xor c(31);
    newcrc(26) := d(10) xor d(6) xor d(4) xor d(3) xor d(0) xor c(10) xor c(16) xor c(19) xor c(20) xor c(22) xor c(26);
    newcrc(27) := d(11) xor d(7) xor d(5) xor d(4) xor d(1) xor c(11) xor c(17) xor c(20) xor c(21) xor c(23) xor c(27);
    newcrc(28) := d(12) xor d(8) xor d(6) xor d(5) xor d(2) xor c(12) xor c(18) xor c(21) xor c(22) xor c(24) xor c(28);
    newcrc(29) := d(13) xor d(9) xor d(7) xor d(6) xor d(3) xor c(13) xor c(19) xor c(22) xor c(23) xor c(25) xor c(29);
    newcrc(30) := d(14) xor d(10) xor d(8) xor d(7) xor d(4) xor c(14) xor c(20) xor c(23) xor c(24) xor c(26) xor c(30);
    newcrc(31) := d(15) xor d(11) xor d(9) xor d(8) xor d(5) xor c(15) xor c(21) xor c(24) xor c(25) xor c(27) xor c(31);
    return newcrc;
  end nextCRC32_D16;


--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------    Testing procedures  ------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------

procedure create_packet(    signal clk : in std_logic;
                            constant crc_initial_value : in std_logic_vector(31 downto 0);
                            constant SOP : in boolean;
                            constant EOP : in boolean;
                            constant huge : in boolean;
                            constant console_print : in boolean;
                            signal timestamp_val : inout unsigned(23 downto 0);
                            signal in_fifo_din : out std_logic_vector(17 downto 0);
                            signal in_fifo_wrreq : out std_logic;
                            signal valid_fifo_din : out std_logic_vector(17 downto 0);
                            signal valid_fifo_wrreq : out std_logic
                            ) is

variable rand : real;
variable my_line : line;
variable seed1, seed2 : positive;
variable pkt_dwords : integer;
variable dword_data : std_logic_vector(15 downto 0);
variable crc32 : std_logic_vector(31 downto 0);

begin

    -- increment timestamp if the packet is valid
    if not huge and SOP and EOP and crc_initial_value = x"FFFFFFFF" then
        timestamp_val <= timestamp_val + 1;
        --write(my_line, string'("Timestamp " & integer'image(to_integer(timestamp_val))));
        --writeline(output, my_line);
    end if;
    
    -- set the seed to the current time of the simulation
    seed1 := now / 1 ns;
    seed2 := now / 1 ns + 1;
    if console_print then
        write(my_line, string'("Creating packet: seed1 " & integer'image(seed1) & ", seed2 " & integer'image(seed2)));
        writeline(output, my_line);
    end if;
    uniform(seed1, seed2, rand);
    
    in_fifo_wrreq <= '0';
    valid_fifo_wrreq <= '0';
    in_fifo_din <= (others=>'0');
    valid_fifo_din <= (others=>'0');
    
    -- start CRC calculation from all bits set
    -- the same is implemented in the packet_validator
    crc32 := crc_initial_value;
    
    -- random packet length data from 1 to 20 dwords + 2 crc dwords + 3 dwords header
    uniform(seed1, seed2, rand);
    rand := rand*real(19);
    pkt_dwords := integer(round(rand));
    
    if huge then
        pkt_dwords := 900;
    end if;
    
    if console_print then
        write(my_line, string'("Creating packet: length " & integer'image(pkt_dwords+6) & ", CRC initial "));
        hwrite(my_line, crc32);
        writeline(output, my_line);
    end if;
    
    -- generate packet header 3 dwords and SOP
    dword_data := x"4100";
    crc32 := nextCRC32_D16(dword_data, crc32);
    wait until rising_edge(clk);
    in_fifo_din(15 downto 0) <= dword_data;
    valid_fifo_din(15 downto 0) <= dword_data;
    in_fifo_wrreq <= '1';
    valid_fifo_wrreq <= '1';
    if SOP then
        in_fifo_din(17) <= '1';
        valid_fifo_din(17) <= '1';
    end if;
    
    if console_print then
        hwrite(my_line, crc32);
        writeline(output, my_line);
    end if;
    dword_data := x"00" & std_logic_vector(timestamp_val(23 downto 16));
    crc32 := nextCRC32_D16(dword_data, crc32);
    wait until rising_edge(clk);
    in_fifo_din(15 downto 0) <= dword_data;
    valid_fifo_din(15 downto 0) <= dword_data;
    in_fifo_din(17) <= '0';
    valid_fifo_din(17) <= '0';
    
    if console_print then
        hwrite(my_line, crc32);
        writeline(output, my_line);
    end if;
    dword_data := std_logic_vector(timestamp_val(15 downto 0));
    crc32 := nextCRC32_D16(dword_data, crc32);
    wait until rising_edge(clk);
    in_fifo_din(15 downto 0) <= dword_data;
    valid_fifo_din(15 downto 0) <= dword_data;
    
    -- generate packet dwords
    for I in 0 to pkt_dwords loop
    
        -- generate random dword data
        uniform(seed1, seed2, rand);
        rand := rand*real((2**16-1));
        dword_data := std_logic_vector(to_unsigned(integer(round(rand)), 16));
        
        -- print CRC hexadecimal values while it is calculated
        if console_print then
            hwrite(my_line, crc32);
            writeline(output, my_line);
        end if;
        
        crc32 := nextCRC32_D16(dword_data, crc32);
        
        -- wait for the clk edge to set the new data
        wait until rising_edge(clk);
        
        -- set the new data
        in_fifo_din(15 downto 0) <= dword_data;
        valid_fifo_din(15 downto 0) <= dword_data;

	end loop;
    
    if console_print then
        hwrite(my_line, crc32);
        writeline(output, my_line);
    end if;
    
    -- add two CRC dwords and EOP
    wait until rising_edge(clk);
    in_fifo_din(15 downto 0) <= crc32(31 downto 16);
    valid_fifo_din(15 downto 0) <= crc32(31 downto 16);
    
    wait until rising_edge(clk);
    in_fifo_din(15 downto 0) <= crc32(15 downto 0);
    valid_fifo_din(15 downto 0) <= crc32(15 downto 0);
    if EOP then
        in_fifo_din(16) <= '1';
        valid_fifo_din(16) <= '1';
    end if;
    
    wait until rising_edge(clk);
    in_fifo_wrreq <= '0';
    valid_fifo_wrreq <= '0';
    in_fifo_din <= (others=>'0');
    valid_fifo_din <= (others=>'0');
    
    
    -- avoid input FIFO overflow by addig idle time for the packet processing
    for I in 0 to pkt_dwords + 6 loop
        wait until rising_edge(clk);
    end loop;

end procedure create_packet ;


--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------    Architecture begin  ------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
begin

DUT_packet_validator: entity work.packet_validator
port map (
    --local clock and reset
    clk                 => clk,
    nrst                => nrst,
    
    --input port
    in_data             => in_data,
    in_startofpacket    => in_startofpacket,
    in_endofpacket      => in_endofpacket,
    in_valid            => in_valid,
    in_ready            => in_ready,
    
    --output port
    out_data            => out_data,
    out_startofpacket   => out_startofpacket,
    out_endofpacket     => out_endofpacket,
    out_valid           => out_valid,
    out_ready           => out_ready,
    
    --status port
    packet_length       => packet_length,
    packet_card_no      => packet_card_no,
    packet_type_no      => packet_type_no,
    packet_timestamp    => packet_timestamp,
    valid_packet        => valid_packet,
    err_drop_word       => err_drop_word,
    err_drop_bad_crc    => err_drop_bad_crc ,
    err_drop_no_eop     => err_drop_no_eop,
    err_tmp_fifo_full   => err_tmp_fifo_full
);

-- helper FIFO to store input packets for the DUT_packet_validator
in_fifo_i : entity work.protocol_fifo 
PORT MAP (
    data    => in_fifo_din,
    rdclk   => clk,
    rdreq   => in_ready,
    wrclk   => clk,
    wrreq   => in_fifo_wrreq,
    q       => in_fifo_output,
    rdempty => in_fifo_rdempty,
    wrfull  => in_fifo_full
);
in_startofpacket <= in_fifo_output(17);
in_endofpacket <= in_fifo_output(16);
in_data <= in_fifo_output(15 downto 0);
in_valid <= not in_fifo_rdempty;

-- helper FIFO to store only valid input packets
-- for comparing them with the DUT_packet_validator output
valid_fifo_i : entity work.protocol_fifo 
PORT MAP (
    data    => valid_fifo_din,
    rdclk   => clk,
    rdreq   => valid_fifo_rdreq,
    wrclk   => clk,
    wrreq   => valid_fifo_wrreq,
    q       => valid_fifo_output,
    rdempty => valid_fifo_rdempty,
    wrfull  => valid_fifo_full
);

-- clk process
process
begin
loop
    clk<='0' ;
    wait for CLOCK_PERIOD/2;
    clk<='1';
    wait for CLOCK_PERIOD/2;
    if done_sim = '1' then
        wait;
    end if;
end loop;
end process;

--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------    Main simulation flow process ---------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
process
variable rand : real;
variable seed1, seed2 : positive;
begin

    done_sim <= '0';
    nrst <= '0';
    
    in_fifo_wrreq <= '0';
    valid_fifo_wrreq <= '0';
    in_fifo_din <= (others=>'0');
    valid_fifo_din <= (others=>'0');
    
    wait for 100 ns;
    nrst <= '1';
    
    --while in_fifo_full = '0' loop
    for I in 0 to 20000 loop
        uniform(seed1, seed2, rand);
        if rand > 0.5 then
            create_packet (clk, x"FFFFFFFF", true, true, false, false, timestamp_val, in_fifo_din, in_fifo_wrreq, valid_fifo_din, valid_fifo_wrreq);   -- valid CRC
        else
            uniform(seed1, seed2, rand);
            if rand > 0.5 then
                create_packet (clk, x"FFFF0000", true, true, false, false, timestamp_val, in_fifo_din, in_fifo_wrreq, in_fifo_din, in_fifo_wrreq);     -- invalid CRC (don't put into valid FIFO!)
            elsif rand < 0.25 then
                uniform(seed1, seed2, rand);
                if rand > 0.25 then 
                    create_packet (clk, x"FFFFFFFF", false, true, false, false, timestamp_val, in_fifo_din, in_fifo_wrreq, in_fifo_din, in_fifo_wrreq);    -- invalid SOP (don't put into valid FIFO!)
                    --report "SOP" severity error;
                else
                    create_packet (clk, x"FFFFFFFF", false, false, true, false, timestamp_val, in_fifo_din, in_fifo_wrreq, in_fifo_din, in_fifo_wrreq);    -- invalid huge packet (don't put into valid FIFO!)
                    --report "huge" severity error;
                end if;
            else
                create_packet (clk, x"FFFFFFFF", true, false, false, false, timestamp_val, in_fifo_din, in_fifo_wrreq, in_fifo_din, in_fifo_wrreq);    -- invalid EOP (don't put into valid FIFO!)
                --report "EOP" severity error;
            end if;
        end if;
    end loop;
    
    
    --wait until packet_validator read all input data
    --wait until rising_edge(in_fifo_rdempty);
    
    wait for 1000 ns;
    done_sim <= '1';
    
    report "Simulation completed" severity failure;

end process;


--------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------- Valid FIFO vs packet_validator comparator process ---------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
valid_fifo_rdreq <= out_valid;
out_ready <= out_valid;

process
variable my_line : line;
begin
    
    if out_valid = '0' then
        wait until out_valid = '1';
        wait until rising_edge(clk);
    end if;
    
    if valid_fifo_output /= out_startofpacket & out_endofpacket & out_data then
        report "Invalid data passed" severity error;
        write(my_line, string'("Valid input stimulus was: "));
        hwrite(my_line, valid_fifo_output);
        write(my_line, string'("; Packet validator output is: "));
        hwrite(my_line, out_startofpacket & out_endofpacket & out_data);
        writeline(output, my_line);
    elsif false then
        report "Valid data passed" severity note;
        write(my_line, string'("Valid input stimulus was: "));
        hwrite(my_line, valid_fifo_output);
        write(my_line, string'("; Packet validator output is: "));
        hwrite(my_line, out_startofpacket & out_endofpacket & out_data);
        writeline(output, my_line);
    end if;

    wait until rising_edge(clk);

end process;


--------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------- Packet timestamp checker ----------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
process
variable local_timestamp : unsigned(23 downto 0) := to_unsigned(1, 24);
begin

    wait until rising_edge(valid_packet);
    
    assert unsigned(packet_timestamp) = local_timestamp report "Packet timestamp is wrong. Expected: " & integer'image(to_integer(local_timestamp)) & " received: " & integer'image(to_integer(unsigned(packet_timestamp))) severity error;
    
    local_timestamp := local_timestamp + 1;

end process;

end behaviour;


