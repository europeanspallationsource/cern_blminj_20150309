#the altera_mf library necesary for the fifo simulation can be compiled just once
#it makes the compilation much faster
#uncomment if compiling for the first time

#create altera_mf library and map it to work
#compile the library
#vlib altera_mf
#vmap work altera_mf
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf_components.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf.vhd 


#create lib library and map it to work
#compile the library
vlib lib
vmap work lib

vcom -novopt -O0 "../../../protocol_fifo.vhd"
vcom -novopt -O0 "../../../CRC32_D16.vhd"
vcom -novopt -O0 -cover bcest "../../../packet_validator.vhd"
vcom -novopt -O0 "../src/TB_packet_validator.vhd"



vsim -novopt -coverage -msgmode both work.TB_packet_validator -t 1ps

#add wave *

########################################################
# Packet validator signals
########################################################

add wave sim:/TB_packet_validator/clk
add wave sim:/TB_packet_validator/nrst
#add wave sim:/TB_packet_validator/in_valid
#add wave sim:/TB_packet_validator/in_ready
#
#add wave -radix hexadecimal sim:/TB_packet_validator/in_fifo_output
#add wave -radix hexadecimal sim:/TB_packet_validator/in_fifo_din
#add wave sim:/TB_packet_validator/in_fifo_wrreq
#add wave -radix hexadecimal sim:/TB_packet_validator/valid_fifo_din
#add wave sim:/TB_packet_validator/valid_fifo_wrreq
#
#add wave sim:/TB_packet_validator/DUT_packet_validator/state
#add wave sim:/TB_packet_validator/DUT_packet_validator/in_valid
#add wave sim:/TB_packet_validator/DUT_packet_validator/in_startofpacket
#add wave sim:/TB_packet_validator/DUT_packet_validator/err_drop_word
#add wave -radix unsigned sim:/TB_packet_validator/DUT_packet_validator/dword_cnt
#add wave -radix hexadecimal sim:/TB_packet_validator/DUT_packet_validator/crc_reg

#add wave -radix unsigned sim:/TB_packet_validator/DUT_packet_validator/dword_cnt
#add wave -radix hexadecimal sim:/TB_packet_validator/DUT_packet_validator/out_fifo_output
#add wave sim:/TB_packet_validator/DUT_packet_validator/out_fifo_wrreq
#add wave sim:/TB_packet_validator/DUT_packet_validator/out_fifo_rdreq
#add wave sim:/TB_packet_validator/DUT_packet_validator/out_fifo_rdempty
#add wave sim:/TB_packet_validator/DUT_packet_validator/tmp_fifo_rdempty
#add wave -radix hexadecimal sim:/TB_packet_validator/DUT_packet_validator/tmp_fifo_din
#add wave -radix hexadecimal sim:/TB_packet_validator/DUT_packet_validator/hdr_cnt
#add wave sim:/TB_packet_validator/DUT_packet_validator/crc_reset
#add wave -radix hexadecimal sim:/TB_packet_validator/DUT_packet_validator/tmp_fifo_output
#add wave sim:/TB_packet_validator/DUT_packet_validator/tmp_fifo_rdreq
#add wave sim:/TB_packet_validator/DUT_packet_validator/tmp_fifo_wrreq
add wave sim:/TB_packet_validator/DUT_packet_validator/state
#add wave sim:/TB_packet_validator/DUT_packet_validator/valid_pkt
#add wave sim:/TB_packet_validator/DUT_packet_validator/tmp_fifo_rdreq
#add wave -radix hexadecimal sim:/TB_packet_validator/DUT_packet_validator/hdr_cnt
#add wave -radix unsigned sim:/TB_packet_validator/DUT_packet_validator/timestamp

add wave -radix unsigned sim:/TB_packet_validator/packet_timestamp
add wave -radix unsigned sim:/TB_packet_validator/packet_card_no
add wave -radix unsigned sim:/TB_packet_validator/packet_type_no
add wave -radix unsigned sim:/TB_packet_validator/packet_length
add wave sim:/TB_packet_validator/valid_packet
add wave sim:/TB_packet_validator/err_drop_word
add wave sim:/TB_packet_validator/err_drop_bad_crc
add wave sim:/TB_packet_validator/err_drop_no_eop
add wave sim:/TB_packet_validator/err_tmp_fifo_full
#add wave -radix hexadecimal sim:/TB_packet_validator/valid_fifo_output
#add wave sim:/TB_packet_validator/valid_fifo_rdempty
#add wave sim:/TB_packet_validator/valid_fifo_rdreq
#
#add wave -radix hexadecimal sim:/TB_packet_validator/out_data
#add wave sim:/TB_packet_validator/out_valid
#add wave sim:/TB_packet_validator/out_ready
#add wave sim:/TB_packet_validator/in_fifo_full
#add wave sim:/TB_packet_validator/in_fifo_rdempty
#add wave sim:/TB_packet_validator/valid_fifo_full



run -all
wave zoomfull
