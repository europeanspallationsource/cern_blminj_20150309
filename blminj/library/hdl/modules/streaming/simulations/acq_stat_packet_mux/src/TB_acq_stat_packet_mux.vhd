library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use ieee.math_real.all;
use STD.textio.all;
use IEEE.STD_LOGIC_TEXTIO.all;

entity TB_acq_stat_packet_mux is
end TB_acq_stat_packet_mux;


architecture behaviour of TB_acq_stat_packet_mux is

constant CLOCK_PERIOD : time := 20 ns;
constant console_print : boolean := true;

signal done_sim          : STD_LOGIC;

signal clk          : STD_LOGIC;
signal nrst         : STD_LOGIC;




signal in_data:             std_logic_vector(15 downto 0);
signal in_startofpacket:    std_logic;
signal in_endofpacket:      std_logic;
signal in_valid:            std_logic;
signal in_ready:            std_logic; 

signal out_acq_data:            std_logic_vector(15 downto 0);
signal out_acq_startofpacket:   std_logic;
signal out_acq_endofpacket:     std_logic;
signal out_acq_valid:           std_logic;
signal out_acq_ready:           std_logic; 

signal out_stat_data:            std_logic_vector(15 downto 0);
signal out_stat_startofpacket:   std_logic;
signal out_stat_endofpacket:     std_logic;
signal out_stat_valid:           std_logic;
signal out_stat_ready:           std_logic; 

signal err_drop_word:           std_logic; 


signal in_fifo_din : std_logic_vector(17 downto 0);
signal in_fifo_output : std_logic_vector(17 downto 0);
signal in_fifo_wrreq : std_logic;
signal in_fifo_rdempty : std_logic;
signal in_fifo_full : std_logic;

signal valid_acq_fifo_din : std_logic_vector(17 downto 0);
signal valid_acq_fifo_output : std_logic_vector(17 downto 0);
signal valid_acq_fifo_rdreq : std_logic;
signal valid_acq_fifo_wrreq : std_logic;
signal valid_acq_fifo_rdempty : std_logic;
signal valid_acq_fifo_full : std_logic;
signal valid_acq_fifo_0_output : std_logic_vector(17 downto 0);
signal valid_acq_fifo_0_rdempty : std_logic;
signal valid_acq_fifo_0_rdreq : std_logic;
signal valid_acq_fifo_1_full : std_logic;
signal valid_acq_fifo_1_wrreq : std_logic;

signal valid_stat_fifo_din : std_logic_vector(17 downto 0);
signal valid_stat_fifo_output : std_logic_vector(17 downto 0);
signal valid_stat_fifo_rdreq : std_logic;
signal valid_stat_fifo_wrreq : std_logic;
signal valid_stat_fifo_rdempty : std_logic;
signal valid_stat_fifo_full : std_logic;
signal valid_stat_fifo_0_output : std_logic_vector(17 downto 0);
signal valid_stat_fifo_0_rdempty : std_logic;
signal valid_stat_fifo_0_rdreq : std_logic;
signal valid_stat_fifo_1_full : std_logic;
signal valid_stat_fifo_1_wrreq : std_logic;


signal out_acq_fifo_din : std_logic_vector(17 downto 0);
signal out_acq_fifo_output : std_logic_vector(17 downto 0);
signal out_acq_fifo_rdreq : std_logic;
signal out_acq_fifo_rdempty : std_logic;
signal out_acq_fifo_full : std_logic;


signal out_stat_fifo_din : std_logic_vector(17 downto 0);
signal out_stat_fifo_output : std_logic_vector(17 downto 0);
signal out_stat_fifo_rdreq : std_logic;
signal out_stat_fifo_rdempty : std_logic;
signal out_stat_fifo_full : std_logic;

signal stop_reading_acq_outputs : std_logic;
signal stop_reading_stat_outputs : std_logic;

--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------    Testing procedures  ------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------

procedure create_packet(    signal clk : in std_logic;
                            constant SOP : in boolean;
                            constant invalid_type_no : in boolean;
                            constant acq_stat : in boolean;
                            signal in_fifo_din : out std_logic_vector(17 downto 0);
                            signal in_fifo_wrreq : out std_logic;
                            signal in_fifo_full : in std_logic;
                            signal valid_fifo_din : out std_logic_vector(17 downto 0);
                            signal valid_fifo_wrreq : out std_logic;
                            signal valid_fifo_full : in std_logic
                            ) is
variable rand : real;
variable seed1, seed2 : positive;
variable dword_data : std_logic_vector(15 downto 0);
variable pkt_dwords : integer;
variable pkt_type : std_logic_vector(3 downto 0);
begin

    -- set the seed to the current time of the simulation
    seed1 := now / 1 ns;
    seed2 := now / 1 ns + 1;
    uniform(seed1, seed2, rand);
    
    if acq_stat then
        -- create acquisition packet
        pkt_dwords := 2;
        pkt_type := "0001";
    else
        -- create status packet
        pkt_dwords := 3;
        pkt_type := "0010";
    end if;
    
    -- create invalid type no
    if invalid_type_no then
        pkt_type := "1100";
    end if;
    
    in_fifo_wrreq <= '0';
    valid_fifo_wrreq <= '0';
    in_fifo_din <= (others=>'0');
    valid_fifo_din <= (others=>'0');
    
    -- generate packet header 3 dwords and SOP
    wait until rising_edge(clk);
    wait for 1 ns;
    if in_fifo_full = '1' or valid_fifo_full = '1' then
        wait until in_fifo_full = '0' and valid_fifo_full = '0';
        wait until rising_edge(clk);
    end if;
    in_fifo_din(15 downto 0) <= x"4" & pkt_type & x"00";
    valid_fifo_din(15 downto 0) <= x"4" & pkt_type & x"00";
    in_fifo_wrreq <= '1';
    valid_fifo_wrreq <= '1';
    if SOP then
        in_fifo_din(17) <= '1';
        valid_fifo_din(17) <= '1';
    end if;
    
    wait until rising_edge(clk);
    wait for 1 ns;
    if in_fifo_full = '1' or valid_fifo_full = '1' then
        in_fifo_wrreq <= '0';
        valid_fifo_wrreq <= '0';
        wait until in_fifo_full = '0' and valid_fifo_full = '0';
        wait until rising_edge(clk);
        in_fifo_wrreq <= '1';
        valid_fifo_wrreq <= '1';
    end if;
    in_fifo_din(15 downto 0) <= x"5AA5";
    valid_fifo_din(15 downto 0) <= x"5AA5";
    in_fifo_din(17) <= '0';
    valid_fifo_din(17) <= '0';
    
    wait until rising_edge(clk);
    wait for 1 ns;
    if in_fifo_full = '1' or valid_fifo_full = '1' then
        in_fifo_wrreq <= '0';
        valid_fifo_wrreq <= '0';
        wait until in_fifo_full = '0' and valid_fifo_full = '0';
        wait until rising_edge(clk);
        in_fifo_wrreq <= '1';
        valid_fifo_wrreq <= '1';
    end if;
    in_fifo_din(15 downto 0) <= x"A55A";
    valid_fifo_din(15 downto 0) <= x"A55A";
    
    -- generate packet dwords
    for I in 0 to pkt_dwords loop
    
        --report " Variable = " & integer'image(I);
        -- generate random wait state
        uniform(seed1, seed2, rand);
        if I < pkt_dwords and rand < 0.3 then
            in_fifo_wrreq <= '0';
            valid_fifo_wrreq <= '0';
            wait until rising_edge(clk);
            in_fifo_wrreq <= '1';
            valid_fifo_wrreq <= '1';
        end if;
    
        -- generate random dword data
        uniform(seed1, seed2, rand);
        rand := rand*real((2**16-1));
        dword_data := std_logic_vector(to_unsigned(integer(round(rand)), 16));
        
        -- wait for the clk edge to set the new data
        wait until rising_edge(clk);
        wait for 1 ns;
        if in_fifo_full = '1' or valid_fifo_full = '1' then
            in_fifo_wrreq <= '0';
            valid_fifo_wrreq <= '0';
            wait until in_fifo_full = '0' and valid_fifo_full = '0';
            wait until rising_edge(clk);
            in_fifo_wrreq <= '1';
            valid_fifo_wrreq <= '1';
        end if;
        
        -- set the new data
        in_fifo_din(15 downto 0) <= dword_data;
        valid_fifo_din(15 downto 0) <= dword_data;
        
        if I = pkt_dwords then
            in_fifo_din(16) <= '1';
            valid_fifo_din(16) <= '1';
        end if;

	end loop;
    
    wait until rising_edge(clk);
    in_fifo_wrreq <= '0';
    valid_fifo_wrreq <= '0';
    in_fifo_din <= (others=>'0');
    valid_fifo_din <= (others=>'0');

end procedure create_packet ;


--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------    Architecture begin  ------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
begin

DUT_acq_stat_packet_mux: entity work.acq_stat_packet_mux
port map (
    --local clock and reset
    clk                     => clk,
    nrst                    => nrst,
    
    --input port
    in_data                 => in_data,
    in_startofpacket        => in_startofpacket,
    in_endofpacket          => in_endofpacket,
    in_valid                => in_valid,
    in_ready                => in_ready,
    
    --acquisition output port
    out_acq_data            => out_acq_data,
    out_acq_startofpacket   => out_acq_startofpacket,
    out_acq_endofpacket     => out_acq_endofpacket,
    out_acq_valid           => out_acq_valid,
    out_acq_ready           => out_acq_ready,
    
    --acquisition output port
    out_stat_data           => out_stat_data,
    out_stat_startofpacket  => out_stat_startofpacket,
    out_stat_endofpacket    => out_stat_endofpacket,
    out_stat_valid          => out_stat_valid,
    out_stat_ready          => out_stat_ready,
    
    --status port
    err_drop_word           => err_drop_word
);

-- helper FIFO to store input packets for the DUT_acq_stat_packet_mux
in_fifo_i : entity work.protocol_fifo 
PORT MAP (
    data    => in_fifo_din,
    rdclk   => clk,
    rdreq   => in_ready,
    wrclk   => clk,
    wrreq   => in_fifo_wrreq,
    q       => in_fifo_output,
    rdempty => in_fifo_rdempty,
    wrfull  => in_fifo_full
);
in_startofpacket <= in_fifo_output(17);
in_endofpacket <= in_fifo_output(16);
in_data <= in_fifo_output(15 downto 0);
in_valid <= not in_fifo_rdempty;

-- helper FIFO to store acq output packets of the DUT_acq_stat_packet_mux
out_acq_fifo_i : entity work.protocol_fifo 
PORT MAP (
    data    => out_acq_fifo_din,
    rdclk   => clk,
    rdreq   => out_acq_fifo_rdreq,
    wrclk   => clk,
    wrreq   => out_acq_valid,
    q       => out_acq_fifo_output,
    rdempty => out_acq_fifo_rdempty,
    wrfull  => out_acq_fifo_full
);

out_acq_fifo_din <= out_acq_startofpacket & out_acq_endofpacket & out_acq_data;
out_acq_ready <= not out_acq_fifo_full;


-- helper FIFO to store stat output packets of the DUT_acq_stat_packet_mux
out_stat_fifo_i : entity work.protocol_fifo 
PORT MAP (
    data    => out_stat_fifo_din,
    rdclk   => clk,
    rdreq   => out_stat_fifo_rdreq,
    wrclk   => clk,
    wrreq   => out_stat_valid,
    q       => out_stat_fifo_output,
    rdempty => out_stat_fifo_rdempty,
    wrfull  => out_stat_fifo_full
);

out_stat_fifo_din <= out_stat_startofpacket & out_stat_endofpacket & out_stat_data;
out_stat_ready <= not out_stat_fifo_full;

-- helper FIFO to store only valid acquisition packets
-- for comparing them with the DUT_acq_stat_packet_mux output
valid_acq_fifo_0_i : entity work.protocol_fifo 
PORT MAP (
    data    => valid_acq_fifo_din,
    rdclk   => clk,
    rdreq   => valid_acq_fifo_0_rdreq,
    wrclk   => clk,
    wrreq   => valid_acq_fifo_wrreq,
    q       => valid_acq_fifo_0_output,
    rdempty => valid_acq_fifo_0_rdempty,
    wrfull  => valid_acq_fifo_full
);

valid_acq_fifo_i : entity work.protocol_fifo 
PORT MAP (
    data    => valid_acq_fifo_0_output,
    rdclk   => clk,
    rdreq   => valid_acq_fifo_rdreq,
    wrclk   => clk,
    wrreq   => valid_acq_fifo_1_wrreq,
    q       => valid_acq_fifo_output,
    rdempty => valid_acq_fifo_rdempty,
    wrfull  => valid_acq_fifo_1_full
);

valid_acq_fifo_0_rdreq <= not valid_acq_fifo_1_full;
valid_acq_fifo_1_wrreq <= not valid_acq_fifo_0_rdempty;

-- helper FIFO to store only valid status packets
-- for comparing them with the DUT_acq_stat_packet_mux output
valid_stat_fifo_0_i : entity work.protocol_fifo 
PORT MAP (
    data    => valid_stat_fifo_din,
    rdclk   => clk,
    rdreq   => valid_stat_fifo_0_rdreq,
    wrclk   => clk,
    wrreq   => valid_stat_fifo_wrreq,
    q       => valid_stat_fifo_0_output,
    rdempty => valid_stat_fifo_0_rdempty,
    wrfull  => valid_stat_fifo_full
);

valid_stat_fifo_i : entity work.protocol_fifo 
PORT MAP (
    data    => valid_stat_fifo_0_output,
    rdclk   => clk,
    rdreq   => valid_stat_fifo_rdreq,
    wrclk   => clk,
    wrreq   => valid_stat_fifo_1_wrreq,
    q       => valid_stat_fifo_output,
    rdempty => valid_stat_fifo_rdempty,
    wrfull  => valid_stat_fifo_1_full
);

valid_stat_fifo_0_rdreq <= not valid_stat_fifo_1_full;
valid_stat_fifo_1_wrreq <= not valid_stat_fifo_0_rdempty;

-- clk process
process
begin
loop
    clk<='0' ;
    wait for CLOCK_PERIOD/2;
    clk<='1';
    wait for CLOCK_PERIOD/2;
    if done_sim = '1' then
        wait;
    end if;
end loop;
end process;

--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------    Main simulation flow process ---------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
main_proc: process
variable rand : real;
variable seed1, seed2 : positive;
begin

    done_sim <= '0';
    nrst <= '0';
    
    in_fifo_wrreq <= '0';
    valid_acq_fifo_wrreq <= '0';
    valid_stat_fifo_wrreq <= '0';
    in_fifo_din <= (others=>'0');
    valid_acq_fifo_din <= (others=>'0');
    valid_stat_fifo_din <= (others=>'0');
    
    wait for 100 ns;
    nrst <= '1';
    
    for I in 0 to 1000000 loop
        uniform(seed1, seed2, rand);
        if rand > 0.1 then          -- create valid packets
            if rand > 0.5 then
                create_packet (clk, true, false, false, in_fifo_din, in_fifo_wrreq, in_fifo_full, valid_stat_fifo_din, valid_stat_fifo_wrreq, valid_stat_fifo_full);   -- valid status packet
            else
                create_packet (clk, true, false, true, in_fifo_din, in_fifo_wrreq, in_fifo_full, valid_acq_fifo_din, valid_acq_fifo_wrreq, valid_acq_fifo_full);   -- valid acquisition packet
            end if;
        else                        -- create invalid packets
            if rand > 0.5 then
                create_packet (clk, false, false, true, in_fifo_din, in_fifo_wrreq, in_fifo_full, in_fifo_din, in_fifo_wrreq, in_fifo_full);   -- invalid acquisition packet (SOP)
            else
                create_packet (clk, true, true, false, in_fifo_din, in_fifo_wrreq, in_fifo_full, in_fifo_din, in_fifo_wrreq, in_fifo_full);   -- invalid packet (type)
            end if;
        end if;
    end loop;
    
    
    wait for 1000 ns;
    done_sim <= '1';
    
    report "Simulation completed" severity failure;

end process;

--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------    Stop reading output proceses ---------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
process
begin

    stop_reading_stat_outputs <= '0';
    
    wait for 4*CLOCK_PERIOD;
    stop_reading_stat_outputs <= '1';
    if out_stat_fifo_full = '0' then
        wait until rising_edge(out_stat_fifo_full) or rising_edge(done_sim);
    end if;
    
    wait until rising_edge(clk);
    

end process;

process
begin

    stop_reading_acq_outputs <= '0';
    
    wait for 4*CLOCK_PERIOD;
    stop_reading_acq_outputs <= '1';
    if out_acq_fifo_full = '0' then
        wait until rising_edge(out_acq_fifo_full) or rising_edge(done_sim);
    end if;
    
    wait until rising_edge(clk);
    

end process;


--------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------- Valid acquisition FIFO vs acq_stat_packet_mux comparator process ------------------------
--------------------------------------------------------------------------------------------------------------------------------------

valid_acq_fifo_rdreq <= not valid_acq_fifo_rdempty and not out_acq_fifo_rdempty and not stop_reading_acq_outputs;
out_acq_fifo_rdreq <= not valid_acq_fifo_rdempty and not out_acq_fifo_rdempty and not stop_reading_acq_outputs;

process
variable my_line : line;
begin

    
    if valid_acq_fifo_rdempty = '1' or out_acq_fifo_rdempty = '1' then
        wait until valid_acq_fifo_rdempty = '0' and out_acq_fifo_rdempty = '0';
        wait until rising_edge(clk);
    end if;
    
    if stop_reading_acq_outputs = '0' then
        if valid_acq_fifo_output /= out_acq_fifo_output then
            report "Invalid data passed" severity error;
            write(my_line, string'("Valid input stimulus was: "));
            hwrite(my_line, valid_acq_fifo_output);
            write(my_line, string'("; Packet MUX ACQ output is: "));
            hwrite(my_line, out_acq_fifo_output);
            writeline(output, my_line);
        elsif false then
            report "Valid data passed" severity note;
            write(my_line, string'("Valid input stimulus was: "));
            hwrite(my_line, valid_acq_fifo_output);
            write(my_line, string'("; Packet MUX ACQ output is: "));
            hwrite(my_line, out_acq_fifo_output);
            writeline(output, my_line);
        end if;
    end if;

    wait until rising_edge(clk);

end process;



--------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------- Valid status FIFO vs acq_stat_packet_mux comparator process -----------------------------
--------------------------------------------------------------------------------------------------------------------------------------
valid_stat_fifo_rdreq <= not valid_stat_fifo_rdempty and not out_stat_fifo_rdempty and not stop_reading_stat_outputs;
out_stat_fifo_rdreq <= not valid_stat_fifo_rdempty and not out_stat_fifo_rdempty and not stop_reading_stat_outputs;

process
variable my_line : line;
begin
    
    if valid_stat_fifo_rdempty = '1' or out_stat_fifo_rdempty = '1' then
        wait until valid_stat_fifo_rdempty = '0' and out_stat_fifo_rdempty = '0';
        wait until rising_edge(clk);
    end if;
    
    if stop_reading_stat_outputs = '0' then
        if valid_stat_fifo_output /= out_stat_fifo_output then
            report "Invalid data passed" severity error;
            write(my_line, string'("Valid input stimulus was: "));
            hwrite(my_line, valid_stat_fifo_output);
            write(my_line, string'("; Packet MUX STAT output is: "));
            hwrite(my_line, out_stat_fifo_output);
            writeline(output, my_line);
        elsif false then
            report "Valid data passed" severity note;
            write(my_line, string'("Valid input stimulus was: "));
            hwrite(my_line, valid_stat_fifo_output);
            write(my_line, string'("; Packet MUX STAT output is: "));
            hwrite(my_line, out_stat_fifo_output);
            writeline(output, my_line);
        end if;
    end if;

    wait until rising_edge(clk);

end process;


--------------------------------------------------------------------------------------------------------------------------------------
-------------------------------------------- Check if the TB writes to any FIFO which is full -----------------------------
--------------------------------------------------------------------------------------------------------------------------------------

process
begin

   if in_fifo_wrreq = '1' then
       assert in_fifo_full = '0' report "Writing to full FIFO (in_fifo_i)" severity failure;
   end if;
   
   if out_acq_valid = '1' then
       assert out_acq_fifo_full = '0' report "Writing to full FIFO (out_acq_fifo_i)" severity failure;
   end if;
   
   if out_stat_valid = '1' then
       assert out_stat_fifo_full = '0' report "Writing to full FIFO (out_stat_fifo_i)" severity failure;
   end if;
   
   if valid_acq_fifo_wrreq = '1' then
       assert valid_acq_fifo_full = '0' report "Writing to full FIFO (valid_acq_fifo_i)" severity failure;
   end if;
   
   if valid_stat_fifo_wrreq = '1' then
       assert valid_stat_fifo_full = '0' report "Writing to full FIFO (valid_stat_fifo_i)" severity failure;
   end if;
   
   wait until rising_edge(clk);
    

end process;


end behaviour;


