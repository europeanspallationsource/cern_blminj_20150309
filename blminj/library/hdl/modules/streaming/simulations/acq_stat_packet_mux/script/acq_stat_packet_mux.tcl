#the altera_mf library necesary for the fifo simulation can be compiled just once
#it makes the compilation much faster
#uncomment if compiling for the first time

#create altera_mf library and map it to work
#compile the library
#vlib altera_mf
#vmap work altera_mf
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf_components.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf.vhd 


#create lib library and map it to work
#compile the library
vlib lib
vmap work lib

vcom -novopt -O0 "../../../protocol_fifo.vhd"
vcom -novopt -O0 -cover bcest "../../../acq_stat_packet_mux.vhd"
vcom -novopt -O0 "../src/TB_acq_stat_packet_mux.vhd"



vsim -novopt -coverage -msgmode both work.TB_acq_stat_packet_mux -t 1ps

#add wave *

########################################################
# Packet validator signals
########################################################

add wave sim:/TB_acq_stat_packet_mux/clk
add wave sim:/TB_acq_stat_packet_mux/nrst
add wave -radix hexadecimal sim:/TB_acq_stat_packet_mux/in_data
add wave sim:/TB_acq_stat_packet_mux/in_startofpacket
add wave sim:/TB_acq_stat_packet_mux/in_endofpacket
add wave sim:/TB_acq_stat_packet_mux/in_valid
add wave sim:/TB_acq_stat_packet_mux/in_ready

add wave -radix hexadecimal sim:/TB_acq_stat_packet_mux/out_acq_data
add wave sim:/TB_acq_stat_packet_mux/out_acq_startofpacket
add wave sim:/TB_acq_stat_packet_mux/out_acq_endofpacket
add wave sim:/TB_acq_stat_packet_mux/out_acq_valid
add wave sim:/TB_acq_stat_packet_mux/out_acq_ready

add wave -radix hexadecimal sim:/TB_acq_stat_packet_mux/out_stat_data
add wave sim:/TB_acq_stat_packet_mux/out_stat_startofpacket
add wave sim:/TB_acq_stat_packet_mux/out_stat_endofpacket
add wave sim:/TB_acq_stat_packet_mux/out_stat_valid
add wave sim:/TB_acq_stat_packet_mux/out_stat_ready

add wave sim:/TB_acq_stat_packet_mux/DUT_acq_stat_packet_mux/state
add wave sim:/TB_acq_stat_packet_mux/DUT_acq_stat_packet_mux/err_drop_word
#add wave sim:/TB_acq_stat_packet_mux/DUT_acq_stat_packet_mux/in_data_type_no
#
#add wave sim:/TB_acq_stat_packet_mux/out_acq_fifo_rdempty
#add wave sim:/TB_acq_stat_packet_mux/valid_acq_fifo_rdempty
#add wave sim:/TB_acq_stat_packet_mux/out_acq_fifo_rdreq
#add wave sim:/TB_acq_stat_packet_mux/valid_acq_fifo_rdreq

add wave sim:/TB_acq_stat_packet_mux/stop_reading_stat_outputs
add wave sim:/TB_acq_stat_packet_mux/stop_reading_acq_outputs
add wave sim:/TB_acq_stat_packet_mux/valid_acq_fifo_full
add wave sim:/TB_acq_stat_packet_mux/valid_stat_fifo_full
add wave sim:/TB_acq_stat_packet_mux/out_acq_fifo_full
add wave sim:/TB_acq_stat_packet_mux/out_stat_fifo_full


#add wave sim:/TB_acq_stat_packet_mux/valid_stat_fifo_full
#add wave sim:/TB_acq_stat_packet_mux/valid_stat_fifo_wrreq

add wave sim:/TB_acq_stat_packet_mux/valid_stat_fifo_rdempty
add wave sim:/TB_acq_stat_packet_mux/out_stat_fifo_rdempty
add wave -radix hexadecimal sim:/TB_acq_stat_packet_mux/valid_stat_fifo_output
add wave -radix hexadecimal sim:/TB_acq_stat_packet_mux/out_stat_fifo_output


run -all
wave zoomfull
