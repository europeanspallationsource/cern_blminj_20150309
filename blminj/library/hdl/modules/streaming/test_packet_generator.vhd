------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    29/10/2014
--Module Name:    test_packet_generator
--Project Name:   test_packet_generator
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity test_packet_generator is
generic (
	t_clk_period_ns 	: natural := 10;
	t_pkt_interval_ns	: natural := 0
);
port	(
	--clock and reset
	gen_clk:			in std_logic;
	gen_nrst:			in std_logic;
	
	--generator output port
	gen_data:			out std_logic_vector(15 downto 0);
	gen_startofpacket:	out std_logic;
	gen_endofpacket:	out std_logic;
	gen_valid:			out std_logic;
	gen_ready:			in std_logic;
	
	--header
	gen_header:			in std_logic_vector(15 downto 0);
	
	--control
	gen_packets:		in std_logic_vector(31 downto 0);
	gen_done:			out std_logic;
	gen_enable:			in std_logic
);
end entity test_packet_generator;


architecture rtl of test_packet_generator is

--number of clock ticks between messages set by user
constant t_pkt_ticks : natural := t_pkt_interval_ns/t_clk_period_ns;
--number of packet double words: 1 dword for header, 2 dwords for counter, 2 dwords for CRC
constant pkt_dwords : natural := 5; 
--number of packet double words + interval ticks
constant all_pkt_dwords : natural := pkt_dwords + t_pkt_ticks + 1;

signal crc_calc_en: std_logic;
signal crc_calc_rst: std_logic;
signal crc_reg: std_logic_vector(31 downto 0);
signal newcrc: std_logic_vector(31 downto 0);


signal gen_enabled: std_logic;
signal gen_packets_reg: unsigned(31 downto 0);


signal pkt_dwords_valid: std_logic;
signal pkt_dwords_done: std_logic;
signal pkt_dwords_cnt: integer range 0 to all_pkt_dwords := 0;	--initial value for the simulation

signal pkt_number_cnt: unsigned(31 downto 0);
signal pkt_number_done: std_logic;

signal gen_data_mux: std_logic_vector(15 downto 0);

BEGIN
	
	
	
	-- enable register
	en_p: process(gen_clk)
	begin
		if rising_edge(gen_clk) then
			if gen_nrst = '0' or pkt_number_done = '1' then
				gen_enabled <= '0';
			elsif gen_enable = '1' then
				gen_enabled <= '1';
			end if;
		end if;
		
		if rising_edge(gen_clk) then
			if gen_nrst = '0' then
				gen_packets_reg <= (others=>'0');
			elsif gen_enable = '1' then
				gen_packets_reg <= unsigned(gen_packets);
			end if;
		end if;
		
	end process;
	
	gen_done <= not gen_enabled;
	
	
	-- packet number counter
	pkt_cnt_p: process(gen_clk)
	begin
		if rising_edge(gen_clk) then
			if gen_nrst = '0' or pkt_number_done = '1' then
				pkt_number_cnt <= (others=>'0');
			elsif pkt_dwords_done = '1' then
				pkt_number_cnt <= pkt_number_cnt + 1;
			end if;
		end if;
	end process;
	pkt_number_done <= '1' when pkt_number_cnt = gen_packets_reg else '0';
	
	
	
	-- packet dwords counter
	pdw_cnt_p: process(gen_clk)
	begin
		if rising_edge(gen_clk) then
			if gen_nrst = '0' or pkt_dwords_done = '1' then
				pkt_dwords_cnt <= all_pkt_dwords;
			elsif gen_enabled = '1' and gen_ready = '1' then
				pkt_dwords_cnt <= pkt_dwords_cnt - 1;
			end if;	
		end if;
	end process;
	pkt_dwords_done <= '1' when pkt_dwords_cnt = 0 else '0';
	
	
	-- multiplex packet dowrds and SOP/EOP
	gen_data_mux <= gen_header 										when pkt_dwords_cnt = 4 else
					std_logic_vector(pkt_number_cnt(31 downto 16))	when pkt_dwords_cnt = 3 else
					std_logic_vector(pkt_number_cnt(15 downto 0))	when pkt_dwords_cnt = 2 else
					crc_reg(31 downto 16)							when pkt_dwords_cnt = 1 else
					crc_reg(15 downto 0)							when pkt_dwords_cnt = 0 else
					x"0000";
	
	gen_data <= gen_data_mux;
	gen_startofpacket <= '1' when pkt_dwords_cnt = 4 else '0';
	gen_endofpacket <= '1' when pkt_dwords_cnt = 0 else '0';
	
	pkt_dwords_valid <= '1' when pkt_dwords_cnt <= 4 and pkt_dwords_cnt >= 0 else '0';
	gen_valid <= pkt_dwords_valid and gen_ready;
	
	
	-- crc enable register
	crc_calc_en <= '1' when pkt_dwords_cnt > 1 and gen_ready = '1' else '0';
	crc_calc_rst <= pkt_dwords_done;
	-- crc register
	crc_p: process(gen_clk)
	begin
		if rising_edge(gen_clk) then
			if gen_nrst = '0' or crc_calc_rst = '1' then
				crc_reg <= (others => '0');
			elsif crc_calc_en = '1' then
				crc_reg <= newcrc;
			end if;
		end if;
	end process;
	-- crc combinatorial "gate"
	CRC32_D16_i : entity work.CRC32_D16 
	PORT MAP (
		D		=> gen_data_mux,
		crc		=> crc_reg,
		newcrc	=> newcrc
	);
	
	
		
end architecture rtl;
