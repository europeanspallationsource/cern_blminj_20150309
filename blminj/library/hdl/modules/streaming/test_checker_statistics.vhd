------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    08/07/2013
--Module Name:    test_checker_statistics
--Project Name:   test_checker_statistics
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity test_checker_statistics is
port	(
	--clock and reset
	clk:			in std_logic;
	nrst:			in std_logic;
	
	--reset counters input
	cnt_rst:		in std_logic;
	
	--correct frame
	no_err:			in std_logic;
	
	--error inputs
	crc_err:		in std_logic;
	hdr_err:		in std_logic;
	seq_err:		in std_logic;
	sop_err:		in std_logic;
	eop_err:		in std_logic;
	rx_fifo_err:	in std_logic;
	
	--counters outputs
	no_error:		out std_logic_vector(31 downto 0);
	crc_errors:		out std_logic_vector(31 downto 0);
	hdr_errors:		out std_logic_vector(31 downto 0);
	seq_errors:		out std_logic_vector(31 downto 0);
	sop_errors:		out std_logic_vector(31 downto 0);
	eop_errors:		out std_logic_vector(31 downto 0);
	rx_fifo_errors:	out std_logic_vector(31 downto 0)
	
);
end entity test_checker_statistics;


architecture rtl of test_checker_statistics is

signal crc_error_cnt: unsigned(31 downto 0);
signal hdr_error_cnt: unsigned(31 downto 0);
signal seq_error_cnt: unsigned(31 downto 0);
signal eop_error_cnt: unsigned(31 downto 0);
signal sop_error_cnt: unsigned(31 downto 0);
signal no_error_cnt: unsigned(31 downto 0);
signal rx_fifo_error_cnt: unsigned(31 downto 0);

BEGIN

	-- rx protocol fifo full error counter
	rxfe_cnt_p: process(clk)
	begin
		if rising_edge(clk) then
			if nrst = '0' or cnt_rst = '1' then
				rx_fifo_error_cnt <= (others=>'0');
			elsif rx_fifo_err = '1' then
				rx_fifo_error_cnt <= rx_fifo_error_cnt + 1;
			end if;	
		end if;
	end process;
	rx_fifo_errors <= std_logic_vector(rx_fifo_error_cnt);

	-- no error counter
	noe_cnt_p: process(clk)
	begin
		if rising_edge(clk) then
			if nrst = '0' or cnt_rst = '1' then
				no_error_cnt <= (others=>'0');
			elsif no_err = '1' then
				no_error_cnt <= no_error_cnt + 1;
			end if;	
		end if;
	end process;
	no_error <= std_logic_vector(no_error_cnt);
	
	-- crc errors counter
	crce_cnt_p: process(clk)
	begin
		if rising_edge(clk) then
			if nrst = '0' or cnt_rst = '1' then
				crc_error_cnt <= (others=>'0');
			elsif crc_err = '1' then
				crc_error_cnt <= crc_error_cnt + 1;
			end if;	
		end if;
	end process;
	crc_errors <= std_logic_vector(crc_error_cnt);
	
	-- header errors counter
	hdre_cnt_p: process(clk)
	begin
		if rising_edge(clk) then
			if nrst = '0' or cnt_rst = '1' then
				hdr_error_cnt <= (others=>'0');
			elsif hdr_err = '1' then
				hdr_error_cnt <= hdr_error_cnt + 1;
			end if;	
		end if;
	end process;
	hdr_errors <= std_logic_vector(hdr_error_cnt);
	
	-- sequence errors counter
	seqe_cnt_p: process(clk)
	begin
		if rising_edge(clk) then
			if nrst = '0' or cnt_rst = '1' then
				seq_error_cnt <= (others=>'0');
			elsif seq_err = '1' then
				seq_error_cnt <= seq_error_cnt + 1;
			end if;	
		end if;
	end process;
	seq_errors <= std_logic_vector(seq_error_cnt);
	
	-- missing eop kword errors counter
	eope_cnt_p: process(clk)
	begin
		if rising_edge(clk) then
			if nrst = '0' or cnt_rst = '1' then
				eop_error_cnt <= (others=>'0');
			elsif eop_err = '1' then
				eop_error_cnt <= eop_error_cnt + 1;
			end if;	
		end if;
	end process;
	eop_errors <= std_logic_vector(eop_error_cnt);
	
	-- missing sop kword errors counter
	sope_cnt_p: process(clk)
	begin
		if rising_edge(clk) then
			if nrst = '0' or cnt_rst = '1' then
				sop_error_cnt <= (others=>'0');
			elsif sop_err = '1' then
				sop_error_cnt <= sop_error_cnt + 1;
			end if;	
		end if;
	end process;
	sop_errors <= std_logic_vector(sop_error_cnt);
	
	
	
end architecture rtl;
