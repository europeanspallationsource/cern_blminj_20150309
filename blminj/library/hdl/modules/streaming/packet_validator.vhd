------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    13/11/2014
--Module Name:    packet_validator
--Project Name:   packet_validator
--Description:   
--  This module reads the incoming data from the input port and stores it into the temporary queue
--  It validates the CRC32 (Ethernet) checksum attached to the lats two 16 bit words of the packet
--  It copies the valid packet only to the output FIFO queue and it deletes the corrupted packets
--  IMPORTANT NOTE!!!
--  The data stream must be idle for at least the same time interval as the transmission of valid data
--  otherwise there will be data overflow in the FIFO queue of the module attached to the input port
--  
--  TODO:
--  The TB_packet_validator test-bench should test transition to the wait_not_full state if the 
--  packet_validator is expected to enter it. This will be the case when the output is not read
--  on time by the following stages (packet decoding, processing etc).
--  
--  Date            Author      Change
--  
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity packet_validator is
port    (
    --local clock and reset
    clk:                in std_logic;
    nrst:               in std_logic;
    
    --input port
    in_data:            in std_logic_vector(15 downto 0);
    in_startofpacket:   in std_logic;
    in_endofpacket:     in std_logic;
    in_valid:           in std_logic;
    in_ready:           out std_logic;
    
    --output port
    out_data:           out std_logic_vector(15 downto 0);
    out_startofpacket:  out std_logic;
    out_endofpacket:    out std_logic;
    out_valid:          out std_logic;
    out_ready:          in std_logic;
    
    --status port
    packet_length:      out std_logic_vector(7 downto 0);
    packet_card_no:     out std_logic_vector(3 downto 0);
    packet_type_no:     out std_logic_vector(3 downto 0);
    packet_timestamp:   out std_logic_vector(23 downto 0);
    valid_packet:       out std_logic;
    err_drop_word:      out std_logic;
    err_drop_bad_crc:   out std_logic;
    err_drop_no_eop:    out std_logic;
    err_tmp_fifo_full:  out std_logic
    
);
end entity packet_validator;


architecture rtl of packet_validator is

type StateType is (idle, wr_tmp, wait_data, wr_out, wait_not_full, clr_tmp, check_crc);
signal state, next_state : StateType;

signal crc_reset :  std_logic;
signal crc_load :   std_logic;
signal crc_ok :     std_logic;
signal tmp_fifo_wrreq : std_logic;
signal tmp_fifo_rdreq : std_logic;
signal tmp_fifo_rdempty : std_logic;
signal out_fifo_wrreq : std_logic;
signal out_fifo_rdreq : std_logic;
signal out_fifo_wrfull : std_logic;
signal out_fifo_rdempty : std_logic;

signal tmp_fifo_din : std_logic_vector(17 downto 0);
signal tmp_fifo_output : std_logic_vector(17 downto 0);
signal out_fifo_output : std_logic_vector(17 downto 0);

signal crc_reg: std_logic_vector(31 downto 0);
signal newcrc: std_logic_vector(31 downto 0);

signal dword_cnt: unsigned(7 downto 0);
signal hdr_cnt: unsigned(1 downto 0);

signal card_no:     std_logic_vector(3 downto 0);
signal type_no:     std_logic_vector(3 downto 0);
signal timestamp:   std_logic_vector(23 downto 0);

signal valid_pkt:      std_logic;
signal drop_word:      std_logic;
signal drop_bad_crc:   std_logic;
signal drop_no_eop:    std_logic;
signal tmp_fifo_full:  std_logic;

BEGIN
    
    -------------------------------------------------------------
    -- Two stages FIFO queues
    -------------------------------------------------------------
    
    -- temporary stage FIFO to store incoming not verified data
    tmp_fifo_i : entity work.protocol_fifo 
    PORT MAP (
        data    => tmp_fifo_din,
        rdclk   => clk,
        rdreq   => tmp_fifo_rdreq,
        wrclk   => clk,
        wrreq   => tmp_fifo_wrreq,
        q       => tmp_fifo_output,
        rdempty => tmp_fifo_rdempty,
        wrfull  => tmp_fifo_full     -- should be never full because it is for one packet only
    );
    
    -- connect temporary FIFO to the validator input port
    tmp_fifo_din <= in_startofpacket & in_endofpacket & in_data;
    
    
    -- output stage FIFO to store verified packets
    out_fifo_i : entity work.protocol_fifo 
    PORT MAP (
        data    => tmp_fifo_output,
        rdclk   => clk,
        rdreq   => out_fifo_rdreq,
        wrclk   => clk,
        wrreq   => out_fifo_wrreq,
        q       => out_fifo_output,
        rdempty => out_fifo_rdempty,
        wrfull  => out_fifo_wrfull
    );
    
    -- connect output FIFO to the validator output port
    out_startofpacket <= out_fifo_output(17);
    out_endofpacket <= out_fifo_output(16);
    out_data <= out_fifo_output(15 downto 0);
    out_valid <= not out_fifo_rdempty;
    out_fifo_rdreq <= out_ready and not out_fifo_rdempty;
    
    -------------------------------------------------------------
    -- Driving FSM
    -------------------------------------------------------------
    -- FSM to drive reading and verifying incoming data
    -- and storing it to the output stage FIFO
    fsm_cnt_p: process(clk)
    begin
        if rising_edge(clk) then
            if nrst = '0' then
                state <= idle;
            else
                state <= next_state;
            end if;
        end if;
    end process;

    fsm_cl_p: process(state, in_valid, in_startofpacket, in_endofpacket, crc_ok, tmp_fifo_rdempty, out_fifo_wrfull)
    begin
        next_state <= state;
        in_ready <= '0';
        crc_reset <= '0';
        crc_load <= '0';
        tmp_fifo_wrreq <= '0';
        tmp_fifo_rdreq <= '0';
        out_fifo_wrreq <= '0';
        valid_pkt <= '0';
        drop_word <= '0';
        drop_bad_crc <= '0';
        drop_no_eop <= '0';
        
        case state is
        
            when idle =>
                crc_reset <= '1';
                if in_valid = '1' and in_startofpacket = '1' then
                    tmp_fifo_wrreq <= '1';
                    in_ready <= '1';
                    crc_load <= '1';
                    crc_reset <= '0';
                    next_state <= wr_tmp;
                elsif in_valid = '1' then
                    in_ready <= '1';
                    drop_word <= '1';
                    next_state <= idle;
                end if;
                
            when wr_tmp =>
                tmp_fifo_wrreq <= '1';
                in_ready <= '1';
                crc_load <= '1';
                if in_valid = '0' then
                    tmp_fifo_wrreq <= '0';
                    in_ready <= '0';
                    crc_load <= '0';
                    next_state <= wait_data;
                elsif in_endofpacket = '1' then
                    next_state <= check_crc;
                elsif in_startofpacket = '1' then
                    tmp_fifo_wrreq <= '0';
                    in_ready <= '0';
                    crc_load <= '0';
                    drop_no_eop <= '1';
                    next_state <= clr_tmp;
                end if;
            
            
            when check_crc =>
                if crc_ok = '1' and out_fifo_wrfull = '0' then
                    valid_pkt <= '1';
                    next_state <= wr_out;
                elsif crc_ok = '0' then
                    drop_bad_crc <= '1';
                    next_state <= clr_tmp;
                end if;
                
            when wait_data =>
                if in_valid = '1' then
                    next_state <= wr_tmp;
                end if;
                
            when clr_tmp =>
                crc_reset <= '1';
                tmp_fifo_rdreq <= '1';
                if tmp_fifo_rdempty = '1' then
                    tmp_fifo_rdreq <= '0';
                    next_state <= idle;
                end if;
                
            
            when wr_out =>
                tmp_fifo_rdreq <= '1';
                out_fifo_wrreq <= '1';
                crc_reset <= '1';
                if tmp_fifo_rdempty = '1' then
                    tmp_fifo_rdreq <= '0';
                    out_fifo_wrreq <= '0';
                    next_state <= idle;
                elsif out_fifo_wrfull = '1' then
                    tmp_fifo_rdreq <= '0';
                    out_fifo_wrreq <= '0';
                    next_state <= wait_not_full;
                end if;
                
            when wait_not_full =>
                if out_fifo_wrfull = '0' then
                    next_state <= wr_out;
                end if;
                
            when others =>
                next_state <= idle;

        end case;
    end process;
    
    -- output sync stage
    sync_p: process(clk)
    begin
        if rising_edge(clk) then
            valid_packet <= valid_pkt;
            err_drop_word <= drop_word;
            err_drop_bad_crc <= drop_bad_crc;
            err_drop_no_eop <= drop_no_eop;
            err_tmp_fifo_full <= tmp_fifo_full;
            if nrst = '0' then
                packet_length <= (others=>'0');
            elsif valid_pkt = '1' then
                packet_length <= std_logic_vector(dword_cnt);
                packet_card_no <= card_no;
                packet_type_no <= type_no;
                packet_timestamp <= timestamp;
            end if;
        end if;
    end process;
    
    -------------------------------------------------------------
    -- CRC verification
    -------------------------------------------------------------
    
    -- crc register
    crc_p: process(clk)
    begin
        if rising_edge(clk) then
            if crc_reset = '1' then
                crc_reg <= (others => '1');
            elsif crc_load = '1' then
                crc_reg <= newcrc;
            end if;
        end if;
    end process;
    -- crc combinatorial "gate"
    CRC32_D16_i : entity work.CRC32_D16 
    PORT MAP (
        D       => in_data,
        crc     => crc_reg,
        newcrc  => newcrc
    );
    -- crc check
    crc_ok <= '1' when unsigned(crc_reg) = 0 else '0';
    
    -------------------------------------------------------------
    -- Packet length counter
    -------------------------------------------------------------
    
    lcnt_p: process(clk)
    begin
        if rising_edge(clk) then
            if crc_reset = '1' then
                dword_cnt <= (others => '0');
            elsif crc_load = '1' and dword_cnt /= 255 then
                dword_cnt <= dword_cnt + 1;
            end if;
        end if;
    end process;
    
    -------------------------------------------------------------
    -- Packet header counter
    -------------------------------------------------------------
    
    -- header words counter
    hdrcnt_p: process(clk)
    begin
        if rising_edge(clk) then
            if tmp_fifo_rdreq = '1' or nrst = '0' then
                hdr_cnt <= (others => '0');
            elsif tmp_fifo_wrreq = '1' and hdr_cnt /= "11" then
                hdr_cnt <= hdr_cnt + 1;
            end if;
        end if;
    end process;
    
    -- store the header information
    hdrst_p: process(clk)
    begin
        if rising_edge(clk) then
            if nrst = '0' then
                card_no <= (others => '0');
                type_no <= (others => '0');
                timestamp <= (others => '0');
            elsif tmp_fifo_wrreq = '1' and hdr_cnt = 0 then
                card_no <= tmp_fifo_din(15 downto 12);
                type_no <= tmp_fifo_din(11 downto 8);
            elsif tmp_fifo_wrreq = '1' and hdr_cnt = 1 then
                timestamp(23 downto 16) <= tmp_fifo_din(7 downto 0);
            elsif tmp_fifo_wrreq = '1' and hdr_cnt = 2 then
                timestamp(15 downto 0) <= tmp_fifo_din(15 downto 0);
            end if;
        end if;
    end process;
        
end architecture rtl;
