------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    18/11/2014
--Module Name:    acq_stat_packet_mux
--Project Name:   acq_stat_packet_mux
--
--  Description:
--  This module takes the streaming input and depending on the data type set in the 1st word of the header it writes
--  that stream either to the acquisition stream output (type "0001") or to the the status stream output (type "0010")
--  
--  NOTE:
--  This module is not designed to validate the input packets. In case of missing EOP in the input packet the MUX will 
--  continue passing the data on the same, previously selected output.
--  The only protection implemented here is to drop input words when there is no SOP or the packet type is neither "0001"
--  nor "0010". The err_drop_word is to inform about it. That mechanism ensures that the stream should never be blocked.
--
--  TODO:
--  
--  Date            Author      Change
--  
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity acq_stat_packet_mux is
port    (
    --local clock and reset
    clk:                    in std_logic;
    nrst:                   in std_logic;
    
    --input port
    in_data:                in std_logic_vector(15 downto 0);
    in_startofpacket:       in std_logic;
    in_endofpacket:         in std_logic;
    in_valid:               in std_logic;
    in_ready:               out std_logic;
    
    --acquisition output port
    out_acq_data:           out std_logic_vector(15 downto 0);
    out_acq_startofpacket:  out std_logic;
    out_acq_endofpacket:    out std_logic;
    out_acq_valid:          out std_logic;
    out_acq_ready:          in std_logic;
    
    --status output port
    out_stat_data:          out std_logic_vector(15 downto 0);
    out_stat_startofpacket: out std_logic;
    out_stat_endofpacket:   out std_logic;
    out_stat_valid:         out std_logic;
    out_stat_ready:         in std_logic;
    
    --status port
    err_drop_word:          out std_logic
    
);
end entity acq_stat_packet_mux;


architecture rtl of acq_stat_packet_mux is

type StateType is (idle, pass_acq, pass_stat);
signal state, next_state : StateType;

signal drop_word : std_logic;
signal in_data_type_no: std_logic_vector(3 downto 0);

BEGIN

    -- Input data type number (in the 1st word of the packet)
    in_data_type_no <= in_data(11 downto 8);
    
    -- Connect the input data port to the output data ports
    out_acq_data <= in_data;
    out_acq_startofpacket <= in_startofpacket;
    out_acq_endofpacket <= in_endofpacket;
    out_stat_data <= in_data;
    out_stat_startofpacket <= in_startofpacket;
    out_stat_endofpacket <= in_endofpacket;
    
    -------------------------------------------------------------
    -- Driving FSM
    -------------------------------------------------------------
    fsm_cnt_p: process(clk)
    begin
        if rising_edge(clk) then
            if nrst = '0' then
                state <= idle;
            else
                state <= next_state;
            end if;
        end if;
    end process;

    fsm_cl_p: process(state, in_valid, in_startofpacket, in_endofpacket, out_acq_ready, out_stat_ready, in_data_type_no)
    begin
        next_state <= state;
        drop_word <= '0';
        in_ready <= '0';
        out_acq_valid <= '0';
        out_stat_valid <= '0';
        
        
        case state is
        
            when idle =>
                if in_valid = '1' and in_startofpacket = '1' and in_data_type_no = "0001" then
                    next_state <= pass_acq;
                elsif in_valid = '1' and in_startofpacket = '1' and in_data_type_no = "0010" then
                    next_state <= pass_stat;
                elsif in_valid = '1' then   -- this should bever happen with the valid packets
                    in_ready <= '1';
                    drop_word <= '1';
                    next_state <= idle;
                end if;
                
            when pass_acq =>
                in_ready <= '1';
                out_acq_valid <= '1';
                if in_valid = '1' and in_endofpacket = '1' and out_acq_ready = '1' then
                    next_state <= idle;
                elsif in_valid = '0' or out_acq_ready = '0' then
                    in_ready <= '0';
                    out_acq_valid <= '0';
                    next_state <= pass_acq;
                end if;
            
            when pass_stat =>
                in_ready <= '1';
                out_stat_valid <= '1';
                if in_valid = '1' and in_endofpacket = '1' and out_stat_ready = '1' then
                    next_state <= idle;
                elsif in_valid = '0' or out_stat_ready = '0' then
                    in_ready <= '0';
                    out_stat_valid <= '0';
                    next_state <= pass_stat;
                end if;
                
                
            when others =>
                next_state <= idle;

        end case;
    end process;
    
    -- output sync stage
    sync_p: process(clk)
    begin
        if rising_edge(clk) then
            err_drop_word <= drop_word;
        end if;
    end process;
        
end architecture rtl;
