------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/07/2014
Module Name:    DAB64x_TimingControl

-----------------
Short Description
-----------------

    Creates all Timing Signals necessary for the DAB64x Operational Firmware.
   
------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------


   

--------------
Implementation
--------------
    
    detailed description of the actual implementation
    

-----------
Limitations
-----------
    

----------------
Missing Features
----------------    
    

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    use work.BLMINJ_pkg.all;
   
entity DAB64x_TimingControl is


generic
(
    cnt_size:           integer     := 32;
    T_CLK:              time        := 12500 ps; --12.5 ns
    T_INP_BLOCK_START:  time        := 500 ns;
    T_INP_BLOCK_STOP:   time        := 200 ms;
    T_SB_PBL:           time        := 800 ns
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Inputs
    BP_pulse_sync:      in  std_logic;
    -- Outputs
    INP_BLOCK:          out std_logic := '0';
    FINAL_PBL_pulse:    out std_logic;
    BP_pulse:           out std_logic;
    BP_CNT:             out std_logic_vector(cnt_size-1 downto 0)
);

end entity DAB64x_TimingControl;


architecture struct of DAB64x_TimingControl is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant INP_BLOCK_START_value:     std_logic_vector(cnt_size-1 downto 0) := STD_LOGIC_VECTOR(TO_UNSIGNED(T_INP_BLOCK_START/T_CLK-1, cnt_size));
    constant INP_BLOCK_STOP_value:      std_logic_vector(cnt_size-1 downto 0) := STD_LOGIC_VECTOR(TO_UNSIGNED(T_INP_BLOCK_STOP/T_CLK-1, cnt_size));    
    constant FINAL_PBL_pulse_value:     std_logic_vector(cnt_size-1 downto 0) := STD_LOGIC_VECTOR(TO_UNSIGNED(T_SB_pbl/T_CLK-1, cnt_size));    
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal CC_CNT_int:          std_logic_vector(cnt_size-1 downto 0);
    signal INP_BLOCK_START:     std_logic;
    signal INP_BLOCK_STOP:      std_logic;
    
    
BEGIN

    --===========--
    -- INP_BLOCK --
    --===========--

    process(clk) is
    begin
        if rising_edge(clk) then
            if INP_BLOCK_START = '1' then
                INP_BLOCK <= '1';
            elsif INP_BLOCK_STOP = '1' then
                INP_BLOCK <= '0';
            end if;
        end if;
    end process;
    
    
    --===============--
    -- Combinatorics --
    --===============--

    BP_pulse    <= BP_pulse_sync;
    
    --==============--
    -- Timing Chain --
    --==============--
    
    -- CC CNT --
    CC_CNT_inst: entity work.basic_cnt
    generic map
    (
        data_size   => cnt_size
    )
    port map 
    (
        clk         => clk,
        rst         => BP_pulse_sync,
        inc         => '1',
        cnt         => CC_CNT_int
    );
    
    -- BP CNT --
    BP_CNT_inst: entity work.basic_cnt
    generic map
    (
        data_size   => cnt_size
    )
    port map 
    (
        clk         => clk,
        rst         => '0',
        inc         => BP_pulse_sync,
        cnt         => BP_CNT
    );
    
    -- INP_BLOCK_START PULSER --
    INP_BLOCK_START_pulser: entity work.pulser
    generic map
    (
        data_size           => cnt_size
    )
    port map 
    (
        clk                 => clk,
        PARAM_pulse_value   => INP_BLOCK_START_value,
        cnt_value           => CC_CNT_int,
        cnt_pulse           => '1',
        sync_pulse          => '0',
        pulse               => INP_BLOCK_START
    );
    
    -- INP_BLOCK_STOP PULSER --
    INP_BLOCK_STOP_pulser: entity work.pulser
    generic map
    (
        data_size           => cnt_size
    )
    port map 
    (
        clk                 => clk,
        PARAM_pulse_value   => INP_BLOCK_STOP_value,
        cnt_value           => CC_CNT_int,
        cnt_pulse           => '1',
        sync_pulse          => '0',
        pulse               => INP_BLOCK_STOP
    );
       
    -- FINAL_PBL PULSER --
    FINAL_PBL_pulser: entity work.pulser
    generic map
    (
        data_size           => cnt_size
    )
    port map 
    (
        clk                 => clk,
        PARAM_pulse_value   => FINAL_PBL_pulse_value,
        cnt_value           => CC_CNT_int,
        cnt_pulse           => '1',
        sync_pulse          => '0',
        pulse               => FINAL_PBL_pulse
    );
    
end architecture struct;
