------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/05/2014
Module Name:    BLEPM_TimingControl

-----------------
Short Description
-----------------

    Creates all Timing Signals necessary for the BLEPM Operational Firmware.
   

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------


   

--------------
Implementation
--------------
    
    detailed description of the actual implementation
    

-----------
Limitations
-----------
    

----------------
Missing Features
----------------


    
    

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    use work.BLMINJ_pkg.all;
   
entity BLEPM_TimingControl is


generic
(
    cnt_size:       integer     := 32
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Inputs
    BP_pulse_sync:      in  std_logic;
    BIn_pulse_sync:     in  std_logic;
    BOut_pulse_sync:    in  std_logic;
    observed_smpl:      in  std_logic;
    -- Outputs
    BP_pulse:           out std_logic;
    BIn_pulse:          out std_logic;
    BOut_pulse:         out std_logic;
    EVOstart_pulse:     out std_logic;
    EVOstop_pulse:      out std_logic;
    EVO_pulse:          out std_logic;
    CAPstart_pulse:     out std_logic;
    CAPstop_pulse:      out std_logic;
    CAP_pulse:          out std_logic;
    BEAM_ACT:           out std_logic;
    BP_CNT:             out std_logic_vector(cnt_size-1 downto 0);
    ACQ_CNT:            out std_logic_vector(cnt_size-1 downto 0);
    EVO_CNT:            out std_logic_vector(cnt_size-1 downto 0);
    CAP_CNT:            out std_logic_vector(cnt_size-1 downto 0);
    -- PARAM
    PARAM_CAP_START:    in  std_logic_vector(cnt_size-1 downto 0);
    PARAM_CAP_STOP:     in  std_logic_vector(cnt_size-1 downto 0);
    PARAM_CAP_SMPLRATE: in  std_logic_vector(cnt_size-1 downto 0);
    PARAM_EVO_SMPLRATE: in  std_logic_vector(cnt_size-1 downto 0);
    PARAM_MS_SMPLRATE:  in  std_logic_vector(cnt_size-1 downto 0);
    PARAM_MACH_TYPE:    in  std_logic_vector(cnt_size-1 downto 0)
);

end entity BLEPM_TimingControl;


architecture struct of BLEPM_TimingControl is
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal BEAM_ACT_reg:        std_logic := '0';
    signal EVO_ACT:             std_logic := '0';
    signal CAP_ACT:             std_logic := '0';
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- Pulses --   
    signal ms_pulse_int:            std_logic;
    signal CAPstart_pulse_int:      std_logic;
    signal CAPstop_pulse_int:       std_logic;
    signal CAP_pulse_int:           std_logic;
    signal EVOstart_pulse_int:      std_logic;
    signal EVOstop_pulse_int:       std_logic;
    signal EVO_pulse_int:           std_logic;
    signal BIn_pulse_sync_to_OS:    std_logic;
    signal BOut_pulse_sync_to_OS:   std_logic;
    signal BP_pulse_sync_to_OS:     std_logic;
    
    -- Counters --  
    signal ACQ_CNT_ms_int:      std_logic_vector(cnt_size-1 downto 0);
    signal ACQ_CNT_CAP_int:     std_logic_vector(cnt_size-1 downto 0);
    signal ACQ_CNT_EVO_int:     std_logic_vector(cnt_size-1 downto 0);
    signal ACQ_CNT_EVO_rst:     std_logic;
    signal ms_CNT_int:          std_logic_vector(cnt_size-1 downto 0);
    signal EVO_CNT_int:         std_logic_vector(cnt_size-1 downto 0);
    signal EVO_CNT_inc:         std_logic;
    signal EVO_CNT_rst:         std_logic;
    signal CAP_CNT_int:         std_logic_vector(cnt_size-1 downto 0);
    signal BP_CNT_int:          std_logic_vector(cnt_size-1 downto 0);
    
BEGIN

    --================--
    -- General Pulses --
    --================--
    
    BP_pulse            <= BP_pulse_sync_to_OS;
    BIn_pulse           <= BIn_pulse_sync_to_OS;
    BOut_pulse          <= BOut_pulse_sync_to_OS; 
        
    CAPstart_pulse      <= CAPstart_pulse_int;
    CAPstop_pulse       <= CAPstop_pulse_int;
    CAP_pulse           <= CAP_pulse_int;
    
    EVOstart_pulse      <= EVOstart_pulse_int;
    EVOstop_pulse       <= EVOstop_pulse_int;
    
    
    --==========================--
    -- Machine Dependent Pulses --
    --==========================--
    
    EVOstart_pulse_int  <=  BIn_pulse_sync_to_OS    when PARAM_MACH_TYPE = 32UX"1" OR PARAM_MACH_TYPE = 32UX"2" else -- L4 = 32UX"1", PSB = 32UX"2"
                            '1';
    EVOstop_pulse_int   <=  BOut_pulse_sync_to_OS   when PARAM_MACH_TYPE = 32UX"1" OR PARAM_MACH_TYPE = 32UX"2" else
                            '0';
    EVO_pulse           <=  observed_smpl           when PARAM_MACH_TYPE = 32UX"1" else
                            EVO_pulse_int;
    
    
    ACQ_CNT_EVO_rst     <=  BIn_pulse_sync_to_OS OR EVO_pulse_int when PARAM_MACH_TYPE = 32UX"2" else -- PSB = 32UX"2"
                            BP_pulse_sync_to_OS OR EVO_pulse_int;
                            
    EVO_CNT_rst         <=  BIn_pulse_sync_to_OS        when PARAM_MACH_TYPE = 32UX"1" OR PARAM_MACH_TYPE = 32UX"2" else -- L4 = 32UX"1", PSB = 32UX"2"
                            BP_pulse_sync_to_OS;
    
    EVO_CNT_inc         <=  observed_smpl AND EVO_ACT   when PARAM_MACH_TYPE = 32UX"1" else
                            EVO_pulse_int;
    
    --==========--
    -- Counters --
    --==========--
    
    BP_CNT      <=  BP_CNT_int;
    ACQ_CNT     <=  ACQ_CNT_ms_int;
    EVO_CNT     <=  STD_LOGIC_VECTOR(UNSIGNED(EVO_CNT_int) - 1);
    CAP_CNT     <=  STD_LOGIC_VECTOR(UNSIGNED(CAP_CNT_int) - 1);
    
    
    --==================--
    -- SYNC TO OBS SMPL --
    --==================--
    
    BIn_sync_to_OS_inst: entity work.sync_pulse
    port map 
    (
        clk         => clk,
        async_pulse => BIn_pulse_sync,
        ref_pulse   => observed_smpl,
        sync_pulse  => BIn_pulse_sync_to_OS
    );
    
    
    BOut_sync_to_OS_inst: entity work.sync_pulse
    port map 
    (
        clk         => clk,
        async_pulse => BOut_pulse_sync,
        ref_pulse   => observed_smpl,
        sync_pulse  => BOut_pulse_sync_to_OS
    );
    
    BP_sync_to_OS_inst: entity work.sync_pulse
    port map 
    (
        clk         => clk,
        async_pulse => BP_pulse_sync,
        ref_pulse   => observed_smpl,
        sync_pulse  => BP_pulse_sync_to_OS
    );
    
    --===============--
    -- ACTIVE STATUS --
    --===============--
    
    BEAM_ACT    <= BEAM_ACT_reg;
    
    process(clk) is
    begin
    
        if rising_edge(clk) then
            -- BEAM ACTIVE
            if BIn_pulse_sync_to_OS = '1' then
                BEAM_ACT_reg <= '1';
            elsif BOut_pulse_sync_to_OS = '1' then
                BEAM_ACT_reg <= '0';
            end if;
            
            -- EVOLUTION ACTIVE
            if EVOstart_pulse_int = '1' then
                EVO_ACT <= '1';
            elsif EVOstop_pulse_int = '1' then
                EVO_ACT <= '0';
            end if;
            
            -- CAPTURE ACTIVE
            if CAPstart_pulse_int = '1' then
                CAP_ACT <= '1';
            elsif CAPstop_pulse_int = '1' then
                CAP_ACT <= '0';
            end if;
            
        end if;
        
    end process;
    
    --==========--
    -- ms Chain --
    --==========--
    
    -- ACQ CNT --
    ACQ_CNT_inst: entity work.basic_cnt
    generic map
    (
        data_size   => cnt_size
    )
    port map 
    (
        clk         => clk,
        rst         => ms_pulse_int OR BP_pulse_sync_to_OS,
        inc         => observed_smpl,
        cnt         => ACQ_CNT_ms_int
    );
    
    -- MS PULSER --
    ms_pulser: entity work.pulser
    generic map
    (
        data_size           => cnt_size
    )
    port map 
    (
        clk                 => clk,
        PARAM_pulse_value   => PARAM_MS_SMPLRATE,
        cnt_value           => ACQ_CNT_ms_int,
        cnt_pulse           => observed_smpl,
        sync_pulse          => '0',
        pulse               => ms_pulse_int
    );
    
    -- MS CNT --
    MS_CNT_inst: entity work.basic_cnt
    generic map
    (
        data_size   => cnt_size
    )
    port map 
    (
        clk => clk,
        rst => BP_pulse_sync_to_OS,
        inc => ms_pulse_int,
        cnt => ms_CNT_int
    );
    
    --=====================--
    -- ms_CNT DEPENDENCIES --
    --=====================--
    
    -- CAPstart PULSER (in ms from BP) --
    CAPstart_pulser: entity work.pulser
    generic map
    (
        data_size           => cnt_size
    )
    port map 
    (
        clk                 => clk,
        PARAM_pulse_value   => PARAM_CAP_START,
        cnt_value           => ms_CNT_int,
        cnt_pulse           => ms_pulse_int,
        sync_pulse          => '0',
        pulse               => CAPstart_pulse_int
    );
    
    -- CAPstop PULSER (in ms from BP) --
    CAPstop_pulser: entity work.pulser
    generic map
    (
        data_size           => cnt_size
    )
    port map 
    (
        clk                 => clk,
        PARAM_pulse_value   => PARAM_CAP_STOP,
        cnt_value           => ms_CNT_int,
        cnt_pulse           => ms_pulse_int,
        sync_pulse          => '0',
        pulse               => CAPstop_pulse_int
    );
    
    --===========--
    -- CAP Chain --
    --===========--
    
    -- ACQ2 CNT --
    ACQ_CNT_CAP_inst: entity work.basic_cnt
    generic map
    (
        data_size   => cnt_size
    )
    port map 
    (
        clk         => clk,
        rst         => CAP_pulse_int OR CAPstart_pulse_int,
        inc         => observed_smpl AND CAP_ACT,
        cnt         => ACQ_CNT_CAP_int
    );
    
    -- CAP PULSER --
    CAP_pulser: entity work.pulser
    generic map
    (
        data_size           => cnt_size
    )
    port map 
    (
        clk                 => clk,
        PARAM_pulse_value   => PARAM_CAP_SMPLRATE,
        cnt_value           => ACQ_CNT_CAP_int,
        cnt_pulse           => observed_smpl AND CAP_ACT,
        sync_pulse          => '0',
        pulse               => CAP_pulse_int
    );
    
    -- CAP CNT --
    CAP_CNT_inst: entity work.basic_cnt
    generic map
    (
        data_size   => cnt_size
    )
    port map 
    (
        clk => clk,
        rst => CAPstart_pulse_int,
        inc => CAP_pulse_int,
        cnt => CAP_CNT_int
    );
    
    --===========--
    -- EVO Chain --
    --===========--
    
    -- ACQ CNT_EVO --
    ACQ_CNT_EVO_inst: entity work.basic_cnt
    generic map
    (
        data_size   => cnt_size
    )
    port map 
    (
        clk         => clk,
        rst         => ACQ_CNT_EVO_rst,
        inc         => observed_smpl AND EVO_ACT,
        cnt         => ACQ_CNT_EVO_int
    );
        
    -- EVO PULSER --
    EVO_pulser: entity work.pulser
    generic map
    (
        data_size           => cnt_size
    )
    port map 
    (
        clk                 => clk,
        PARAM_pulse_value   => PARAM_EVO_SMPLRATE,
        cnt_value           => ACQ_CNT_EVO_int,
        cnt_pulse           => observed_smpl AND EVO_ACT,
        sync_pulse          => '0',
        pulse               => EVO_pulse_int
    );
    
    -- EVO CNT --
    EVO_CNT_inst: entity work.basic_cnt
    generic map
    (
        data_size   => cnt_size
    )
    port map 
    (
        clk         => clk,
        rst         => EVO_CNT_rst,
        inc         => EVO_CNT_inc,
        cnt         => EVO_CNT_int
    );
    
    --==========--
    -- BP Chain --
    --==========--
    
    -- BP CNT --
    BP_CNT_inst: entity work.basic_cnt
    generic map
    (
        data_size   => cnt_size
    )
    port map 
    (
        clk         => clk,
        rst         => '0',
        inc         => BP_pulse_sync_to_OS,
        cnt         => BP_CNT_int
    );
    
    
end architecture struct;
