------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/02/2012
Module Name:    mapper_16_6


-----------
Description
-----------

    maps 16 bit inputs to 6 bit outputs defined in a case statement
    
    
------------------
Generics/Constants
------------------

    none
    
-----------
Limitations
-----------
    
    none
    
----------------------------
Necessary Packages/Libraries
----------------------------

    none

-----------------
Necessary Modules
-----------------

    none
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

------------------------
Not implemented Features
------------------------

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity mapper_16_7 is

port
(
    map_in:     in std_logic_vector(15 downto 0);
    map_out:    out std_logic_vector(6 downto 0)
);

end entity mapper_16_7;


architecture struct of mapper_16_7 is
    
BEGIN

    --======--
    -- CASE --
    --======--

    process(all) is
    begin
        case map_in is
            
            when X"0000" => map_out <= 7X"00";
            when X"0100" => map_out <= 7X"01";
            when X"0200" => map_out <= 7X"02";
            when X"0300" => map_out <= 7X"03";
            when X"0403" => map_out <= 7X"04";
            when X"0405" => map_out <= 7X"05";
            when X"0500" => map_out <= 7X"06";
            when X"0600" => map_out <= 7X"07";
            when X"0700" => map_out <= 7X"08";
            when X"0800" => map_out <= 7X"09";
            when X"0900" => map_out <= 7X"0A";
            when X"0A00" => map_out <= 7X"0B";
            when X"0B00" => map_out <= 7X"0C";
            when X"0C00" => map_out <= 7X"0D";
            when X"0D00" => map_out <= 7X"0E";
            when X"0E00" => map_out <= 7X"0F";
            when X"0E01" => map_out <= 7X"10";
            when X"0E02" => map_out <= 7X"11";
            when X"0E03" => map_out <= 7X"12";
            when X"0E04" => map_out <= 7X"13";
            when X"0F01" => map_out <= 7X"14";
            when X"0F03" => map_out <= 7X"15";
            when X"1001" => map_out <= 7X"16";
            when X"1003" => map_out <= 7X"17";
            when X"1101" => map_out <= 7X"18";
            when X"1103" => map_out <= 7X"19";
            when X"1201" => map_out <= 7X"1A";
            when X"1203" => map_out <= 7X"1B";
            when X"1301" => map_out <= 7X"1C";
            when X"1303" => map_out <= 7X"1D";
            when X"1401" => map_out <= 7X"1E";
            when X"1403" => map_out <= 7X"1F";
            when X"1501" => map_out <= 7X"20";
            when X"1503" => map_out <= 7X"21";
            when X"1601" => map_out <= 7X"22";
            when X"1603" => map_out <= 7X"23";
            
            when X"1744" => map_out <= 7X"24";
            when X"1745" => map_out <= 7X"25";
            when X"1746" => map_out <= 7X"26";
            when X"1747" => map_out <= 7X"27";
            when X"1748" => map_out <= 7X"28";
            when X"1749" => map_out <= 7X"29";
            when X"174A" => map_out <= 7X"2A";
            when X"174B" => map_out <= 7X"2B";
            when X"174C" => map_out <= 7X"2C";
            when X"174D" => map_out <= 7X"2D";
            when X"174E" => map_out <= 7X"2E";
            when X"174F" => map_out <= 7X"2F";
            when X"1750" => map_out <= 7X"30";
            when X"1751" => map_out <= 7X"31";
            when X"1752" => map_out <= 7X"32";
            when X"1753" => map_out <= 7X"33";
            when X"17E0" => map_out <= 7X"34";
            when X"17E1" => map_out <= 7X"35";
            when X"17E2" => map_out <= 7X"36";
            when X"17E3" => map_out <= 7X"37";
            when X"17E4" => map_out <= 7X"38";
            when X"17E5" => map_out <= 7X"39";
            when X"17E6" => map_out <= 7X"3A";
            when X"17E7" => map_out <= 7X"3B";
            when X"17E8" => map_out <= 7X"3C";
            when X"17E9" => map_out <= 7X"3D";
            when X"1800" => map_out <= 7X"3E";
            when X"1944" => map_out <= 7X"3F";
            when X"1945" => map_out <= 7X"40";
            when X"1946" => map_out <= 7X"41";
            when X"1947" => map_out <= 7X"42";
            when X"1948" => map_out <= 7X"43";
            when X"1949" => map_out <= 7X"44";
            when X"194A" => map_out <= 7X"45";
            when X"194B" => map_out <= 7X"46";
            when X"194C" => map_out <= 7X"47";
            when X"194D" => map_out <= 7X"48";
            when X"194E" => map_out <= 7X"49";
            when X"194F" => map_out <= 7X"4A";
            when X"1950" => map_out <= 7X"4B";
            when X"1951" => map_out <= 7X"4C";
            when X"1952" => map_out <= 7X"4D";
            when X"1953" => map_out <= 7X"4E";
            when X"19E0" => map_out <= 7X"4F";
            when X"19E1" => map_out <= 7X"50";
            when X"19E2" => map_out <= 7X"51";
            when X"19E3" => map_out <= 7X"52";
            when X"19E4" => map_out <= 7X"53";
            when X"19E5" => map_out <= 7X"54";
            when X"19E6" => map_out <= 7X"55";
            when X"19E7" => map_out <= 7X"56";
            when X"19E8" => map_out <= 7X"57";
            when X"19E9" => map_out <= 7X"58";
            when X"1A00" => map_out <= 7X"59";
            
            
            when others  => map_out <= "1111111";
            
        end case;
    end process;
    
    
end architecture struct;
