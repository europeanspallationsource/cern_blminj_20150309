------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/07/2014
Module Name:    edge_detect

-----------------
Short Description
-----------------

    This is an edge detect module, to shorten a long (synchronized) incoming pulse to a pulse with 1 CC length.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    rising      := sets, if it should detect the rising edge (= 1) or the falling edge (= 0).

    
--------------
Implementation
--------------
    
    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use IEEE.std_logic_misc.all;
    use work.vhdl_func_pkg.all;
   
entity edge_detect is
generic
(
    rising:      integer := 1
);
port
(
    -- Clock
    clk:                in  std_logic;
    -- Input
    long_pulse:         in  std_logic;
    -- Output
    oneCC_pulse:        out std_logic
);

end entity edge_detect;


architecture struct of edge_detect is


    --===========--
    -- REGISTERS --
    --===========--
    
    signal input_reg:       std_logic := '0';
    
BEGIN
    
    --==========--
    -- REGISTER --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            input_reg <= long_pulse;
        end if;
    end process;
   
    
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    det_rising_edge: if rising = 1 generate
        
        oneCC_pulse <= long_pulse AND NOT input_reg;
        
    end generate;
    
    det_falling_edge: if rising = 0 generate
        
        oneCC_pulse <= NOT long_pulse AND input_reg;
        
    end generate;
    
  
end architecture struct;
