------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/02/2012
Module Name:    timer

-----------
Description
-----------

    Implements a timer, that triggers the output "upd" for one CC every upd_intv_ms and can be resetted by the "rst" signal.

------------------
Generics/Constants
------------------

    Tclk_ns         := period of the clk input in ns
    interval_ms     := interval in ms between the upd output is showing '1' for one CC

-----------
Limitations
-----------

    < limitations for the generics and/or the module itself >

    
-----------------
Necessary Modules
-----------------

    vhdl_func_pkg

---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    < detailed descriptions of the internal functionality of the module including port usage >

    
------------------------
Not implemented Features
------------------------

    < informations about missing features or nice-to-have addons >



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity timer is

generic
(
    Tclk_ns:        real;
    upd_intv_ms:    integer
);

port
(
    -- Clock
    clk:        in  std_logic;
    -- Controls
    rst:        in  std_logic;
    -- Output
    upd:        out std_logic
);

end entity timer;


architecture struct of timer is
    
    --===========--
    -- CONSTANTS --
    --===========--

    -- Update Timer
    constant upd_rate:      integer := INTEGER(1000000.0/Tclk_ns) * upd_intv_ms;
    constant upd_vsize:     integer := get_vsize(upd_rate);
    

    --==========--
    -- COUNTERS --
    --==========--
    
    -- Update Timer
    signal upd_timer:       UNSIGNED(upd_vsize-1 downto 0) := (upd_vsize-1 downto 0 => '0');
    
    signal upd_timer_rst:   std_logic;

BEGIN
    
    upd_timer_rst <=    '1' when upd_timer = TO_UNSIGNED(upd_rate, upd_vsize) OR rst = '1' else
                        '0';
    
    --=======--
    -- PORTS --
    --=======--
    
    upd <=  '1' when upd_timer = TO_UNSIGNED(upd_rate, upd_vsize) else
            '0';
    
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if upd_timer_rst = '1' then
                upd_timer <= (upd_vsize-1 downto 0 => '0');
            else
                upd_timer <= upd_timer + 1;
            end if;
            
        end if;
    end process;

    
end architecture struct;
