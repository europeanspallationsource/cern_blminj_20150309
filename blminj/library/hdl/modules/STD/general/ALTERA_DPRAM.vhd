------------------------------
/*
Company:       CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2013
Module Name:    ALTERA_DPRAM


-----------------
Short Description
-----------------

    This is a dual port Altera ALTsyncram wrapped into a generic structure.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    

--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

library altera_mf;
    use altera_mf.altera_mf_components.all;

entity ALTERA_DPRAM is

generic
(
    portA_numwords:         integer;
    portA_data_size:        integer;
    portA_addr_size:        integer;
    portA_byteena_size:     integer;
    portB_numwords:         integer;
    portB_data_size:        integer;
    portB_addr_size:        integer;
    portB_byteena_size:     integer;
    init_file:              string := "UNUSED";
    ram_type:               string := "AUTO";
    read_during_write:      string := "OLD_DATA";
    device_family:          string
);

port
(
    -- Clock
    clk:                    in  std_logic;
    -- PortA
    portA_addr:             in  std_logic_vector(portA_addr_size-1 downto 0);
    portA_data:             in  std_logic_vector(portA_data_size-1 downto 0);
    portA_byteena:          in  std_logic_vector(portA_byteena_size-1 downto 0);
	portA_wren:             in  std_logic;
	portA_q:                out std_logic_vector(portA_data_size-1 downto 0);
    -- PortB
    portB_addr:             in  std_logic_vector(portB_addr_size-1 downto 0);
    portB_data:             in  std_logic_vector(portB_data_size-1 downto 0);
    portB_byteena:          in  std_logic_vector(portB_byteena_size-1 downto 0);
	portB_wren:             in  std_logic;
	portB_q:                out std_logic_vector(portB_data_size-1 downto 0)
);

end entity ALTERA_DPRAM;


architecture struct of ALTERA_DPRAM is
     
BEGIN
    
    
    --===================--
    -- ALTERA 2 PORT RAM --
    --===================--
    
    altsyncram_component : altsyncram
	GENERIC MAP 
    (
        -- Header
        operation_mode                      => "BIDIR_DUAL_PORT",
        lpm_type                            => "altsyncram",
        intended_device_family              => device_family,
        ram_block_type                      => ram_type,
        init_file                           => init_file,
		read_during_write_mode_mixed_ports  => read_during_write,
        -- Port A
        widthad_a                           => portA_addr_size,
        width_a                             => portA_data_size,
        numwords_a                          => portA_numwords,
        width_byteena_a                     => portA_byteena_size,
        outdata_reg_a                       => "UNREGISTERED",
        -- Port B
        widthad_b                           => portB_addr_size,
		width_b                             => portB_data_size,
        numwords_b                          => portB_numwords,
		width_byteena_b                     => portB_byteena_size,
        outdata_reg_b                       => "UNREGISTERED",
        -- ACLR
        indata_aclr_a                       => "NONE",
		indata_aclr_b                       => "NONE",
        address_aclr_a                      => "NONE",
		address_aclr_b                      => "NONE",
        byteena_aclr_a                      => "NONE",
		byteena_aclr_b                      => "NONE",
        outdata_aclr_a                      => "NONE",
		outdata_aclr_b                      => "NONE",
        wrcontrol_aclr_a                    => "NONE",
		wrcontrol_aclr_b                    => "NONE",
        -- Misc
		power_up_uninitialized              => "FALSE",
        -- Clock for Port B
		address_reg_b                       => "CLOCK0",
		byteena_reg_b                       => "CLOCK0",
		indata_reg_b                        => "CLOCK0",
		wrcontrol_wraddress_reg_b           => "CLOCK0"	
	)
	PORT MAP 
    (
		clock0      => clk,
        address_a   => portA_addr,
		data_a      => portA_data,
        byteena_a   => portA_byteena,
		wren_a      => portA_wren,
		q_a         => portA_q,
		address_b   => portB_addr,
        data_b      => portB_data,
		byteena_b   => portB_byteena,
		wren_b      => portB_wren,
		q_b         => portB_q
	);
    
    
    
end architecture struct;