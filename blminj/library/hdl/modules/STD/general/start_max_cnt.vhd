------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/12/2013
Module Name:    start_max_cnt

-----------------
Short Description
-----------------
    
    This is a basic counter module, which is triggered by a start signal and increases its value every time inc goes to 1.
    It automatically reset after reaching a predefined maximum value.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    data_size       := size of the counter
    max             := value at which the counter resets

    
--------------
Implementation
--------------
    
    The first increase is essentially ignored. This is done by checking for max as the limit
    and substracting one from the counter in general.
    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use IEEE.std_logic_misc.all;
    use work.vhdl_func_pkg.all;
   
entity start_max_cnt is
generic
(
    data_size:      integer;
    max:            integer
);
port
(
    -- Clock
    clk:            in  std_logic;
    -- Input
    inc:            in  std_logic;
    start:          in  std_logic;
    -- Output
    cnt:            out std_logic_vector(data_size-1 downto 0);
    stopped:        out std_logic
);

end entity start_max_cnt;


architecture struct of start_max_cnt is
    
    --======--
    -- FSMs --
    --======--
    
    type state_t is (stop, run);
    signal present_state, next_state: state_t := stop;
    
    --==========--
    -- COUNTERS --
    --==========--
    signal cnt_int:        UNSIGNED(data_size-1 downto 0) := (others => '0');
    signal cnt_inc:        std_logic;
    signal cnt_rst:        std_logic;
    
BEGIN
    
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    cnt <= STD_LOGIC_VECTOR(cnt_int - 1);
     
    --=========--
    -- COUNTER --
    --=========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if cnt_rst = '1' then
                cnt_int <= (others => '0');
            elsif cnt_inc = '1' then
                cnt_int <= cnt_int + 1;
            end if;
        end if;
    end process;
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- Counter
        cnt_rst <= '0';
        cnt_inc <= '0';
        stopped <= '1';
        
        case present_state is
            
            --===============--
            when stop =>
            --===============--
            
                if start = '1' then
                    stopped <= '0';
                    if inc = '1' then
                        if cnt_int = max then
                            cnt_rst     <= '1';
                            next_state  <= stop;
                        else
                            cnt_inc     <= '1';
                            next_state  <= run;
                        end if;
                    else
                        next_state <= run;
                    end if;
                else
                    next_state <= stop;
                end if;
            
            --===============--
            when run =>
            --===============--
                
                stopped <= '0';
                if inc = '1' then
                    if cnt_int = max-1 then
                        cnt_rst     <= '1';
                        next_state  <= stop;
                    else
                        cnt_inc     <= '1';
                        next_state  <= run;
                    end if;
                else
                    next_state <= run;
                end if;
            
            
        end case;
    end process;
end architecture struct;
