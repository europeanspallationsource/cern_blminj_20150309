------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/12/2013
Module Name:    pulser

-----------------
Short Description
-----------------

    This is a pulser moduel.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    data_size       := size of the counter

    
--------------
Implementation
--------------
    
    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use IEEE.std_logic_misc.all;
    use work.vhdl_func_pkg.all;
   
entity pulser is
generic
(
    data_size:      integer
);
port
(
    -- Clock
    clk:                in  std_logic;
    -- PARAM
    PARAM_pulse_value:  in  std_logic_vector(data_size-1 downto 0);
    -- Input
    cnt_value:          in  std_logic_vector(data_size-1 downto 0);
    cnt_pulse:          in  std_logic;
    sync_pulse:         in  std_logic;
    -- Output
    pulse:              out std_logic
);

end entity pulser;


architecture struct of pulser is

    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal pulse_value:     UNSIGNED(data_size-1 downto 0);
    signal cnt_int:         UNSIGNED(data_size-1 downto 0);
    
BEGIN
    
    
    
    --=======--
    -- PARAM --
    --=======--

    pulse_value <= UNSIGNED(PARAM_pulse_value);
    
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    cnt_int <= UNSIGNED(cnt_value);
    
    process(all) is
    begin
        if (sync_pulse = '1' AND cnt_int > pulse_value/2 AND cnt_int < pulse_value) then -- sync check
            pulse <= '1';
        elsif cnt_int = pulse_value then -- std check
            pulse <= cnt_pulse; --fwd pulse from counter
        else 
            pulse <= '0';
        end if;
    end process;
    
  
end architecture struct;
