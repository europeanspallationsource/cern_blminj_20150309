------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/12/2013
Module Name:    basic_cnt

-----------------
Short Description
-----------------
    
    This is a basic counter module, increasing its value every time "inc" goes to 1 and resets to 0 when
    "rst" is 1.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    data_size       := size of the counter

    
--------------
Implementation
--------------
    
    see short description
    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use IEEE.std_logic_misc.all;
    use work.vhdl_func_pkg.all;
   
entity basic_cnt is
generic
(
    data_size:      integer
);
port
(
    -- Clock
    clk:            in  std_logic;
    -- Input
    rst:            in  std_logic;
    inc:            in  std_logic;
    -- Output
    cnt:            out std_logic_vector(data_size-1 downto 0)
);

end entity basic_cnt;


architecture struct of basic_cnt is
    
    signal cnt_int:        UNSIGNED(data_size-1 downto 0) := (others => '0');
    
BEGIN
    
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    cnt <= STD_LOGIC_VECTOR(cnt_int);
     
    --=========--
    -- COUNTER --
    --=========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if rst = '1' then
                cnt_int <= (others => '0');
            elsif inc = '1' then
                cnt_int <= cnt_int + 1;
            end if;
        end if;
    end process;
   
    
    
end architecture struct;
