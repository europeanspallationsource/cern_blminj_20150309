------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/05/2014
Module Name:    append

-----------------
Short Description
-----------------

    This module appends a certain amount(out_data_size/inp_data_size) of valid input data to one output data word. 
    There are multiple control signals (smpl, rst, full_out_rdy) to give information to the external controller.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    inp_data_size   := size of single input data
    out_data_size   := size of combined output data
    
--------------
Implementation
--------------
    
    The output is created by appending the smaller incoming data word to a larger word (MSB first). The out_data signal
    always shows the so far constructed word plus ZEROs at the LSBs.
    The control signals should be used/interpreted as follows:
    
    smpl            - the shown inp_data in this CC is sampled/append to the rest
    full_out_rdy    - this signal is '1' when the complete output data word is shown at the out_data bus
    sync_rst        - should be used, when the out_data word is either done (full_out_rdy) or the external control knows, that
                      there is no more input word to come
    
    
-----------
Limitations
-----------
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity append is

generic
(
    inp_data_size:      integer;
    out_data_size:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Inputs
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    -- Outputs
    out_data:       out std_logic_vector(out_data_size-1 downto 0);
    -- Controls
    smpl:           in  std_logic;
    sync_rst:       in  std_logic;
    full_out_rdy:   out std_logic
    
);

end entity append;


architecture struct of append is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- number of slices
    constant slice_n:          integer := INTEGER(CEIL(REAL(out_data_size)/REAL(inp_data_size)));
    constant slice_n_vsize:    integer := get_vsize(slice_n);
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Header Slice Counter
    signal slice_cnt:       UNSIGNED(slice_n_vsize-1 downto 0) := TO_UNSIGNED(slice_n-1, slice_n_vsize);
    signal slice_rst:       std_logic;
    signal slice_dec:       std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal stored_val:      std_logic_vector(out_data_size-1 downto inp_data_size); -- doesn't store the last value
    
BEGIN

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if slice_rst= '1' then
                slice_cnt <= TO_UNSIGNED(slice_n-1, slice_n_vsize);
            elsif slice_dec = '1' then
                slice_cnt <= slice_cnt - 1;
            end if;
            
        end if;
    end process;
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    -- Control
    process (all) is
    begin
        -- DEFAULTS
        full_out_rdy    <= '0';
        slice_rst       <= sync_rst;
        slice_dec       <= '0';
        
        if smpl = '1' then
            if slice_cnt = 0 then
                full_out_rdy    <= '1';
            else
                slice_dec       <= '1';
            end if;
        
        end if;
    end process;

    -- Output
    process (all) is
    begin
        if slice_cnt = 0 then
            out_data <= stored_val & inp_data;
        else
            out_data(out_data_size-1 downto inp_data_size*TO_INTEGER(slice_cnt))    <= stored_val(out_data_size-1 downto inp_data_size*TO_INTEGER(slice_cnt));
            out_data(inp_data_size*TO_INTEGER(slice_cnt)-1 downto 0)                <= (others => '0');
        end if;
    end process;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if smpl = '1' then
                if slice_cnt = slice_n-1 then
                    stored_val(out_data_size-1 downto inp_data_size*(TO_INTEGER(slice_cnt+1)))                          <= (others => '0');  
                    stored_val(inp_data_size*(TO_INTEGER(slice_cnt)+1)-1 downto inp_data_size*TO_INTEGER(slice_cnt))    <= inp_data;
                elsif slice_cnt > 0 then
                    stored_val(inp_data_size*(TO_INTEGER(slice_cnt)+1)-1 downto inp_data_size*TO_INTEGER(slice_cnt))    <= inp_data;
                end if;
            end if;
                    
        end if;
    end process;
    
    
    
end architecture struct;
