------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/12/2013
Module Name:    sync_pulse

-----------------
Short Description
-----------------

    This module syncronizes an asynchronous pulse(async_pulse) depending on a reference pulse (ref_pulse). In result the sync_pulse pulses
    during the same CC as the reference pulse. The reference pulse should occur much more often then the incoming one.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    none

    
--------------
Implementation
--------------
    
    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use IEEE.std_logic_misc.all;
    use work.vhdl_func_pkg.all;
   
entity sync_pulse is

port
(
    -- Clock
    clk:                in  std_logic;
    -- Input
    async_pulse:        in  std_logic;
    ref_pulse:          in  std_logic;
    -- Output
    sync_pulse:         out std_logic
);

end entity sync_pulse;


architecture struct of sync_pulse is

    --==========--
    -- COUNTERS --
    --==========--
    
    signal rem_pulse:       std_logic := '0';
    signal rem_pulse_rst:   std_logic;
    signal rem_pulse_set:   std_logic;
    
    
BEGIN
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if rem_pulse_rst = '1' then
                rem_pulse <= '0';
            elsif rem_pulse_set = '1' then
                rem_pulse <= '1';
            end if;
        end if;
        
    end process;
    
    -- Control rem_pulse
    rem_pulse_rst   <= ref_pulse AND rem_pulse;
    rem_pulse_set   <= async_pulse AND NOT ref_pulse;
    
    sync_pulse      <= (ref_pulse AND async_pulse) OR (ref_pulse AND rem_pulse);
    
    
end architecture struct;
