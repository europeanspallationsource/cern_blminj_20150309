------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/11/2011
Module Name:    sync
Project Name:   general
Description:
Is able to synchronze a given asynchronous input(data_width) to the dest_clk clock region with the given number of stages.
*/

------------------------------


library IEEE;
   use IEEE.std_logic_1164.all;
   use IEEE.numeric_std.all;
   use work.vhdl_func_pkg.all;
      

entity sync is

generic(
		data_width: integer     := 32;
		stages:		integer	    := 2;
        init_value: std_logic   := '0'
	);


port(
		-- Inputs
		signal dest_clk:        in std_logic;
		signal async_data: 		in std_logic_vector(data_width-1 downto 0);
		-- Outputs
		signal sync_data: 		out std_logic_vector(data_width-1 downto 0)
	);
	
end entity sync;



architecture struct of sync is
    
    -- Array
	signal data_array: slv_array(0 to stages) (data_width-1 downto 0) := (others => (others => init_value));
    
begin
    data_array(0) <= async_data;
      
    loop_stages: for i in 1 to stages generate  
      
        process(dest_clk) is  
		begin 
            if rising_edge(dest_clk) then
                data_array(i) <= data_array(i-1);
			end if;
		end process; 
        
    end generate;
      
    sync_data <= data_array(stages);

end architecture struct;


