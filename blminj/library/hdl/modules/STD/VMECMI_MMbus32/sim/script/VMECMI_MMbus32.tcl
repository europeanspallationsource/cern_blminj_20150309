#the altera_mf library necesary for the fifo simulation can be compiled just once
#it makes the compilation much faster
#uncomment if compiling for the first time

#create altera_mf library and map it to work
#compile the library
#vlib altera_mf
#vmap work altera_mf
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf_components.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf.vhd 


#create lib library and map it to work
#compile the library
vlib lib
vmap work lib

vcom -novopt -O0 "../src/vme_cs_registers.vhd"
vcom -novopt -O0 "../src/ram_32.vhd"
vcom -novopt -O0 -cover bcest "../../VMECMI_MMbus32.vhd"
vcom -novopt -O0 "../src/TB_VMECMI_MMbus32.vhd"



#vsim -novopt -coverage -msgmode both work.TB_VMECMI_MMbus32 -t 1ps
vsim -novopt -msgmode both work.TB_VMECMI_MMbus32 -t 1ps

#add wave *


add wave sim:/TB_VMECMI_MMbus32/clk
add wave sim:/TB_VMECMI_MMbus32/nrst
add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/rdreq_data
add wave sim:/TB_VMECMI_MMbus32/rdreq_vld
add wave sim:/TB_VMECMI_MMbus32/rdreq_next

add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/DUT_VMECMI_MMbus32/read_address_reg
add wave sim:/TB_VMECMI_MMbus32/DUT_VMECMI_MMbus32/state

add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/read_address
add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/read_data
add wave sim:/TB_VMECMI_MMbus32/DUT_VMECMI_MMbus32/read_l
add wave sim:/TB_VMECMI_MMbus32/DUT_VMECMI_MMbus32/read_h
add wave sim:/TB_VMECMI_MMbus32/read_req

add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/rdout_data
add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/rdout_vld

add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/wrreq_data
add wave sim:/TB_VMECMI_MMbus32/wrreq_vld
add wave sim:/TB_VMECMI_MMbus32/wrreq_next

add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/write_address
add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/write_data
add wave sim:/TB_VMECMI_MMbus32/write_req

add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/ram0_address
add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/ram0_write_data
add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/ram0_wr_enable
add wave -radix hexadecimal sim:/TB_VMECMI_MMbus32/ram0_read_data




run -all
wave zoomfull
