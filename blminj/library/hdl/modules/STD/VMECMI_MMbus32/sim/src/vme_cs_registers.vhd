--------------------------------
--
--Company:        CERN - BE/BI/BL
--Engineer:       Maciej Kwiatkowski
--Updated:        10/03/2014
--Module Name:    vme_cs_registers
--
--
-------------
--Description
-------------
--
--    This is the CMI module for a register set implemented by the CS card.
--
--
--------------------
--Generics/Constants
--------------------
--
--    ...
--    
------------------------------
--Necessary Packages/Libraries
------------------------------
--    
--    vhdl_func_pkg
--    
-------------------
--Necessary Modules
-------------------
--
--    none
--    
--
-----------------
--Necessary Cores
-----------------
--
--    none 
--    
----------------
--Implementation
----------------
--    
--------------------------
--Not implemented Features
--------------------------
--
--
--------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;


entity vme_cs_registers is
port
(
    -- Clock
    clk:                    in  std_logic;
    
    -- Memory mapped bus
    read_address:           in std_logic_vector(23 downto 0);
    read_data:              out std_logic_vector(31 downto 0);
    read_req:               in std_logic;
        
    write_address:          in std_logic_vector(23 downto 0);
    write_data:             in std_logic_vector(31 downto 0);
    write_req:              in std_logic;
    
    --------------------------------------------------------
	-- memory interface to/from the logic
	--------------------------------------------------------
    ram0_wr:                out std_logic_vector(31 downto 0);
    ram0_wr_en:             out std_logic;
    ram0_rd:                in std_logic_vector(31 downto 0);
    
    --------------------------------------------------------
    -- registers interface to/from the logic
    --------------------------------------------------------
    
    -- status registers to/from the logic
    -- pass through register, VME R
    synth_date_rd:          in std_logic_vector(31 downto 0);
    synth_time_rd:          in std_logic_vector(31 downto 0);
    serial_id_h_rd:         in std_logic_vector(31 downto 0);
    serial_id_l_rd:         in std_logic_vector(31 downto 0);
    status_bits_rd:         in std_logic_vector(31 downto 0);
    temperature_rd:         in std_logic_vector(31 downto 0);
    
    -- timing registers from the logic
    -- pass through registers, VME R
    beam_in_msec_rd:        in std_logic_vector(31 downto 0);
    beam_out_msec_rd:       in std_logic_vector(31 downto 0);
    basic_period_msec_rd:   in std_logic_vector(31 downto 0);
    
    -- interlock registers to/from the logic
    -- pass through registers, VME R
    cibu1_status_rd:        in std_logic_vector(31 downto 0);
    cibu2_status_rd:        in std_logic_vector(31 downto 0);
    hw_ilock1_rd:           in std_logic_vector(31 downto 0);
    hw_ilock2_rd:           in std_logic_vector(31 downto 0);
    sw_ilock1_rd:           in std_logic_vector(31 downto 0);
    sw_ilock2_rd:           in std_logic_vector(31 downto 0);
    current_user_rd:        in std_logic_vector(31 downto 0);
    wdg_timer_rd:           in std_logic_vector(31 downto 0);
    wdg_time_rd:            in std_logic_vector(31 downto 0);
    -- pass through registers, VME W
    comm_hw_ilock1_wr:          out std_logic_vector(31 downto 0);
    comm_hw_ilock1_wr_en:       out std_logic;
    comm_hw_ilock2_wr:          out std_logic_vector(31 downto 0);
    comm_hw_ilock2_wr_en:       out std_logic;
    comm_sw_ilock1_wr:          out std_logic_vector(31 downto 0);
    comm_sw_ilock1_wr_en:       out std_logic;
    comm_sw_ilock2_wr:          out std_logic_vector(31 downto 0);
    comm_sw_ilock2_wr_en:       out std_logic;
    comm_set_user_wr:           out std_logic_vector(31 downto 0);
    comm_set_user_wr_en:        out std_logic;
    comm_set_wdg_time_wr:       out std_logic_vector(31 downto 0);
    comm_set_wdg_time_wr_en:    out std_logic;
    comm_reset_wdg_wr:          out std_logic_vector(31 downto 0);
    comm_reset_wdg_wr_en:       out std_logic;
    
    -- high voltage registers to/from the logic
    -- pass through registers, VME R
    hv1_volt_value_rd:          in std_logic_vector(31 downto 0);
    hv2_volt_value_rd:          in std_logic_vector(31 downto 0);
    hv1_volt_max_rd:            in std_logic_vector(31 downto 0);
    hv2_volt_max_rd:            in std_logic_vector(31 downto 0);
    hv1_volt_min_rd:            in std_logic_vector(31 downto 0);
    hv2_volt_min_rd:            in std_logic_vector(31 downto 0);
    hv1_curr_value_rd:          in std_logic_vector(31 downto 0);
    hv2_curr_value_rd:          in std_logic_vector(31 downto 0);
    hv1_curr_max_rd:            in std_logic_vector(31 downto 0);
    hv2_curr_max_rd:            in std_logic_vector(31 downto 0);
    hv1_curr_min_rd:            in std_logic_vector(31 downto 0);
    hv2_curr_min_rd:            in std_logic_vector(31 downto 0);
    hv_comparators_rd:          in std_logic_vector(31 downto 0);
    hv1_volt_high_cnt_rd:       in std_logic_vector(31 downto 0);
    hv1_volt_low_cnt_rd:        in std_logic_vector(31 downto 0);
    hv1_curr_high_cnt_rd:       in std_logic_vector(31 downto 0);
    hv1_curr_low_cnt_rd:        in std_logic_vector(31 downto 0);
    hv2_volt_high_cnt_rd:       in std_logic_vector(31 downto 0);
    hv2_volt_low_cnt_rd:        in std_logic_vector(31 downto 0);
    hv2_curr_high_cnt_rd:       in std_logic_vector(31 downto 0);
    hv2_curr_low_cnt_rd:        in std_logic_vector(31 downto 0);
    -- pass through registers, VME RW
    hv1_dc_level_wr:        out std_logic_vector(31 downto 0);
    hv1_dc_level_wr_en:     out std_logic;
    hv1_dc_level_rd:        in std_logic_vector(31 downto 0);
    hv1_ac_amplitude_wr:    out std_logic_vector(31 downto 0);
    hv1_ac_amplitude_wr_en: out std_logic;
    hv1_ac_amplitude_rd:    in std_logic_vector(31 downto 0);
    hv1_ac_gain_p1_wr:      out std_logic_vector(31 downto 0);
    hv1_ac_gain_p1_wr_en:   out std_logic;
    hv1_ac_gain_p1_rd:      in std_logic_vector(31 downto 0);
    hv1_ac_gain_p2_wr:      out std_logic_vector(31 downto 0);
    hv1_ac_gain_p2_wr_en:   out std_logic;
    hv1_ac_gain_p2_rd:      in std_logic_vector(31 downto 0);
    hv2_dc_level_wr:        out std_logic_vector(31 downto 0);
    hv2_dc_level_wr_en:     out std_logic;
    hv2_dc_level_rd:        in std_logic_vector(31 downto 0);
    hv2_ac_amplitude_wr:    out std_logic_vector(31 downto 0);
    hv2_ac_amplitude_wr_en: out std_logic;
    hv2_ac_amplitude_rd:    in std_logic_vector(31 downto 0);
    hv2_ac_gain_p1_wr:      out std_logic_vector(31 downto 0);
    hv2_ac_gain_p1_wr_en:   out std_logic;
    hv2_ac_gain_p1_rd:      in std_logic_vector(31 downto 0);
    hv2_ac_gain_p2_wr:      out std_logic_vector(31 downto 0);
    hv2_ac_gain_p2_wr_en:   out std_logic;
    hv2_ac_gain_p2_rd:      in std_logic_vector(31 downto 0);
    hv_ac_frequency_wr:     out std_logic_vector(31 downto 0);
    hv_ac_frequency_wr_en:  out std_logic;
    hv_ac_frequency_rd:     in std_logic_vector(31 downto 0);
    
    -- low voltage registers to/from the logic
    -- pass through registers, VME R
    vme3v3_value_rd:        in std_logic_vector(31 downto 0);
    vme5v_value_rd:         in std_logic_vector(31 downto 0);
    analog_p15v_value_rd:   in std_logic_vector(31 downto 0);
    analog_n15v_value_rd:   in std_logic_vector(31 downto 0);
    analog_5v_value_rd:     in std_logic_vector(31 downto 0);
    ref_5v_value_rd:        in std_logic_vector(31 downto 0);
    ref_10va_value_rd:      in std_logic_vector(31 downto 0);
    ref_10vb_value_rd:      in std_logic_vector(31 downto 0);
    vme3v3_min_rd:          in std_logic_vector(31 downto 0);
    vme3v3_max_rd:          in std_logic_vector(31 downto 0);
    vme5v_min_rd:           in std_logic_vector(31 downto 0);
    vme5v_max_rd:           in std_logic_vector(31 downto 0);
    analog_p15v_min_rd:     in std_logic_vector(31 downto 0);
    analog_p15v_max_rd:     in std_logic_vector(31 downto 0);
    analog_n15v_min_rd:     in std_logic_vector(31 downto 0);
    analog_n15v_max_rd:     in std_logic_vector(31 downto 0);
    analog_5v_min_rd:       in std_logic_vector(31 downto 0);
    analog_5v_max_rd:       in std_logic_vector(31 downto 0);
    ref_5v_min_rd:          in std_logic_vector(31 downto 0);
    ref_5v_max_rd:          in std_logic_vector(31 downto 0);
    ref_10va_min_rd:        in std_logic_vector(31 downto 0);
    ref_10va_max_rd:        in std_logic_vector(31 downto 0);
    ref_10vb_min_rd:        in std_logic_vector(31 downto 0);
    ref_10vb_max_rd:        in std_logic_vector(31 downto 0);
    lv_comparators_rd:      in std_logic_vector(31 downto 0);
    analog5v_cnt_rd:        in std_logic_vector(31 downto 0);
    analog15v_cnt_rd:       in std_logic_vector(31 downto 0);
    vme3v3_cnt_rd:          in std_logic_vector(31 downto 0);
    vme5v_cnt_rd:           in std_logic_vector(31 downto 0);
    vme12v_cnt_rd:          in std_logic_vector(31 downto 0)
);

end entity vme_cs_registers;


architecture rtl of vme_cs_registers is

attribute keep: boolean;

-------------------------------------------
-- register constants
-------------------------------------------
constant status_regs_base:      unsigned(23 downto 0) := x"800000";
constant ilock_regs_base_rd:    unsigned(23 downto 0) := x"900000";
constant ilock_regs_base_wr:    unsigned(23 downto 0) := x"980000";
constant voltage_regs_base_rd:  unsigned(23 downto 0) := x"A00000";
constant voltage_regs_base_wr:  unsigned(23 downto 0) := x"A80000";

signal read_data_d0 : std_logic_vector(31 downto 0);
signal read_data_d1 : std_logic_vector(31 downto 0);
signal read_address_d1 : std_logic_vector(23 downto 0);
    
BEGIN
    
    
    
    
    -------------------------------------------
    -- 32 bit readout to the VME
    -- pass-through registers end with *_wr
    -- built-in registers end with *_reg
    -- zeros when the register is VME write only or not defined
    ------------------------------------------- 
    read_data_d0 <= synth_date_rd           when unsigned(read_address) = status_regs_base + 0 else
                    synth_time_rd           when unsigned(read_address) = status_regs_base + 4 else
                    serial_id_h_rd          when unsigned(read_address) = status_regs_base + 8 else
                    serial_id_l_rd          when unsigned(read_address) = status_regs_base + 12 else
                    status_bits_rd          when unsigned(read_address) = status_regs_base + 16 else
                    beam_in_msec_rd         when unsigned(read_address) = status_regs_base + 20 else
                    beam_out_msec_rd        when unsigned(read_address) = status_regs_base + 24 else
                    basic_period_msec_rd    when unsigned(read_address) = status_regs_base + 28 else
                    temperature_rd          when unsigned(read_address) = status_regs_base + 32 else
                    
                    cibu1_status_rd         when unsigned(read_address) = ilock_regs_base_rd + 0 else
                    cibu2_status_rd         when unsigned(read_address) = ilock_regs_base_rd + 4 else
                    hw_ilock1_rd            when unsigned(read_address) = ilock_regs_base_rd + 8 else
                    hw_ilock2_rd            when unsigned(read_address) = ilock_regs_base_rd + 12 else
                    sw_ilock1_rd            when unsigned(read_address) = ilock_regs_base_rd + 16 else
                    sw_ilock2_rd            when unsigned(read_address) = ilock_regs_base_rd + 20 else
                    wdg_timer_rd            when unsigned(read_address) = ilock_regs_base_rd + 24 else
                    wdg_time_rd             when unsigned(read_address) = ilock_regs_base_rd + 28 else
                    current_user_rd         when unsigned(read_address) = ilock_regs_base_rd + 32 else
                    
                    hv1_volt_value_rd       when unsigned(read_address) = voltage_regs_base_rd + 0 else
                    hv2_volt_value_rd       when unsigned(read_address) = voltage_regs_base_rd + 4 else
                    hv1_volt_max_rd         when unsigned(read_address) = voltage_regs_base_rd + 8 else
                    hv2_volt_max_rd         when unsigned(read_address) = voltage_regs_base_rd + 12 else
                    hv1_volt_min_rd         when unsigned(read_address) = voltage_regs_base_rd + 16 else
                    hv2_volt_min_rd         when unsigned(read_address) = voltage_regs_base_rd + 20 else
                    hv1_curr_value_rd       when unsigned(read_address) = voltage_regs_base_rd + 24 else
                    hv2_curr_value_rd       when unsigned(read_address) = voltage_regs_base_rd + 28 else
                    hv1_curr_max_rd         when unsigned(read_address) = voltage_regs_base_rd + 32 else
                    hv2_curr_max_rd         when unsigned(read_address) = voltage_regs_base_rd + 36 else
                    hv1_curr_min_rd         when unsigned(read_address) = voltage_regs_base_rd + 40 else
                    hv2_curr_min_rd         when unsigned(read_address) = voltage_regs_base_rd + 44 else
                    hv1_volt_high_cnt_rd    when unsigned(read_address) = voltage_regs_base_rd + 48 else
                    hv1_volt_low_cnt_rd     when unsigned(read_address) = voltage_regs_base_rd + 52 else
                    hv1_curr_high_cnt_rd    when unsigned(read_address) = voltage_regs_base_rd + 56 else
                    hv1_curr_low_cnt_rd     when unsigned(read_address) = voltage_regs_base_rd + 60 else
                    hv2_volt_high_cnt_rd    when unsigned(read_address) = voltage_regs_base_rd + 64 else
                    hv2_volt_low_cnt_rd     when unsigned(read_address) = voltage_regs_base_rd + 68 else
                    hv2_curr_high_cnt_rd    when unsigned(read_address) = voltage_regs_base_rd + 72 else
                    hv2_curr_low_cnt_rd     when unsigned(read_address) = voltage_regs_base_rd + 76 else
                    hv_comparators_rd       when unsigned(read_address) = voltage_regs_base_rd + 80 else
                    x"00000000"             when unsigned(read_address) = voltage_regs_base_rd + 84 else
                    vme3v3_value_rd         when unsigned(read_address) = voltage_regs_base_rd + 88 else
                    vme5v_value_rd          when unsigned(read_address) = voltage_regs_base_rd + 92 else
                    analog_p15v_value_rd    when unsigned(read_address) = voltage_regs_base_rd + 96 else
                    analog_n15v_value_rd    when unsigned(read_address) = voltage_regs_base_rd + 100 else
                    analog_5v_value_rd      when unsigned(read_address) = voltage_regs_base_rd + 104 else
                    ref_5v_value_rd         when unsigned(read_address) = voltage_regs_base_rd + 108 else
                    ref_10va_value_rd       when unsigned(read_address) = voltage_regs_base_rd + 112 else
                    ref_10vb_value_rd       when unsigned(read_address) = voltage_regs_base_rd + 116 else
                    vme3v3_max_rd           when unsigned(read_address) = voltage_regs_base_rd + 120 else
                    vme3v3_min_rd           when unsigned(read_address) = voltage_regs_base_rd + 124 else
                    vme5v_max_rd            when unsigned(read_address) = voltage_regs_base_rd + 128 else
                    vme5v_min_rd            when unsigned(read_address) = voltage_regs_base_rd + 132 else
                    analog_p15v_max_rd      when unsigned(read_address) = voltage_regs_base_rd + 136 else
                    analog_p15v_min_rd      when unsigned(read_address) = voltage_regs_base_rd + 140 else
                    analog_n15v_max_rd      when unsigned(read_address) = voltage_regs_base_rd + 144 else
                    analog_n15v_min_rd      when unsigned(read_address) = voltage_regs_base_rd + 148 else
                    analog_5v_max_rd        when unsigned(read_address) = voltage_regs_base_rd + 152 else
                    analog_5v_min_rd        when unsigned(read_address) = voltage_regs_base_rd + 156 else
                    ref_5v_max_rd           when unsigned(read_address) = voltage_regs_base_rd + 160 else
                    ref_5v_min_rd           when unsigned(read_address) = voltage_regs_base_rd + 164 else
                    ref_10va_max_rd         when unsigned(read_address) = voltage_regs_base_rd + 168 else
                    ref_10va_min_rd         when unsigned(read_address) = voltage_regs_base_rd + 172 else
                    ref_10vb_max_rd         when unsigned(read_address) = voltage_regs_base_rd + 176 else
                    ref_10vb_min_rd         when unsigned(read_address) = voltage_regs_base_rd + 180 else
                    lv_comparators_rd       when unsigned(read_address) = voltage_regs_base_rd + 184 else
                    analog5v_cnt_rd         when unsigned(read_address) = voltage_regs_base_rd + 188 else
                    analog15v_cnt_rd        when unsigned(read_address) = voltage_regs_base_rd + 192 else
                    vme3v3_cnt_rd           when unsigned(read_address) = voltage_regs_base_rd + 196 else
                    vme5v_cnt_rd            when unsigned(read_address) = voltage_regs_base_rd + 200 else
                    vme12v_cnt_rd           when unsigned(read_address) = voltage_regs_base_rd + 204 else
                    
                    hv1_dc_level_rd         when unsigned(read_address) = voltage_regs_base_wr + 0  else
                    hv2_dc_level_rd         when unsigned(read_address) = voltage_regs_base_wr + 4  else
                    hv1_ac_amplitude_rd     when unsigned(read_address) = voltage_regs_base_wr + 8  else
                    hv2_ac_amplitude_rd     when unsigned(read_address) = voltage_regs_base_wr + 12 else
                    hv1_ac_gain_p1_rd       when unsigned(read_address) = voltage_regs_base_wr + 16 else
                    hv2_ac_gain_p1_rd       when unsigned(read_address) = voltage_regs_base_wr + 20 else
                    hv1_ac_gain_p2_rd       when unsigned(read_address) = voltage_regs_base_wr + 24 else
                    hv2_ac_gain_p2_rd       when unsigned(read_address) = voltage_regs_base_wr + 28 else
                    hv_ac_frequency_rd      when unsigned(read_address) = voltage_regs_base_wr + 32 else

                    (others => '0');
    
    out_reg: process(clk) is
    begin
        if rising_edge(clk) then
            read_data_d1 <= read_data_d0;
            read_address_d1 <= read_address;
        end if;
    end process;
    
    read_data <= ram0_rd when unsigned(read_address_d1) <= 65535 else read_data_d1;
    
    -------------------------------------------
    -- VME write selects to the 32 bit built-in registers
    -- data strobe to the logic for the pass-through registers
    -- registers must be VME writable
    -------------------------------------------
    
    comm_hw_ilock1_wr_en <=     '1' when write_req = '1' and unsigned(write_address) = ilock_regs_base_wr + 0  else '0';
    comm_hw_ilock2_wr_en <=     '1' when write_req = '1' and unsigned(write_address) = ilock_regs_base_wr + 4  else '0';
    comm_sw_ilock1_wr_en <=     '1' when write_req = '1' and unsigned(write_address) = ilock_regs_base_wr + 8  else '0';
    comm_sw_ilock2_wr_en <=     '1' when write_req = '1' and unsigned(write_address) = ilock_regs_base_wr + 12  else '0';
    comm_reset_wdg_wr_en <=     '1' when write_req = '1' and unsigned(write_address) = ilock_regs_base_wr + 16  else '0';
    comm_set_wdg_time_wr_en <=  '1' when write_req = '1' and unsigned(write_address) = ilock_regs_base_wr + 20  else '0';
    comm_set_user_wr_en <=      '1' when write_req = '1' and unsigned(write_address) = ilock_regs_base_wr + 24  else '0';
    
    hv1_dc_level_wr_en <=       '1' when write_req = '1' and unsigned(write_address) = voltage_regs_base_wr + 0  else '0';
    hv2_dc_level_wr_en <=       '1' when write_req = '1' and unsigned(write_address) = voltage_regs_base_wr + 4  else '0';
    hv1_ac_amplitude_wr_en <=   '1' when write_req = '1' and unsigned(write_address) = voltage_regs_base_wr + 8  else '0';
    hv2_ac_amplitude_wr_en <=   '1' when write_req = '1' and unsigned(write_address) = voltage_regs_base_wr + 12  else '0';
    hv1_ac_gain_p1_wr_en <=     '1' when write_req = '1' and unsigned(write_address) = voltage_regs_base_wr + 16  else '0';
    hv2_ac_gain_p1_wr_en <=     '1' when write_req = '1' and unsigned(write_address) = voltage_regs_base_wr + 20  else '0';
    hv1_ac_gain_p2_wr_en <=     '1' when write_req = '1' and unsigned(write_address) = voltage_regs_base_wr + 24  else '0';
    hv2_ac_gain_p2_wr_en <=     '1' when write_req = '1' and unsigned(write_address) = voltage_regs_base_wr + 28  else '0';
    hv_ac_frequency_wr_en <=    '1' when write_req = '1' and unsigned(write_address) = voltage_regs_base_wr + 32  else '0';

    ram0_wr_en <=            '1' when write_req = '1' and unsigned(write_address) <= 65535 else '0';

    
    -------------------------------------------
    -- pass-through 32 bit registers
    -- read data to the logic
    -- registers must be VME writable
    -------------------------------------------
    
    comm_hw_ilock1_wr <= write_data;
    comm_hw_ilock2_wr <= write_data;
    comm_sw_ilock1_wr <= write_data;
    comm_sw_ilock2_wr <= write_data;
    comm_set_user_wr <= write_data;
    comm_set_wdg_time_wr <= write_data;
    comm_reset_wdg_wr <= write_data;
    hv1_dc_level_wr <= write_data;
    hv1_ac_amplitude_wr <= write_data;
    hv1_ac_gain_p1_wr <= write_data;
    hv1_ac_gain_p2_wr <= write_data;
    hv2_dc_level_wr <= write_data;
    hv2_ac_amplitude_wr <= write_data;
    hv2_ac_gain_p1_wr <= write_data;
    hv2_ac_gain_p2_wr <= write_data;
    hv_ac_frequency_wr <= write_data;
    
    ram0_wr <= write_data;


    
end architecture rtl;
