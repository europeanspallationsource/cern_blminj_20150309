library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use ieee.math_real.all;
use STD.textio.all;
use IEEE.STD_LOGIC_TEXTIO.all;

entity TB_VMECMI_MMbus32 is
end TB_VMECMI_MMbus32;


architecture behaviour of TB_VMECMI_MMbus32 is

constant CLOCK_PERIOD : time := 20 ns;

signal done_sim          : STD_LOGIC;


signal clk          : STD_LOGIC;
signal nrst       : STD_LOGIC;

signal rdreq_data:      std_logic_vector(60 downto 0);
signal rdreq_vld:       std_logic;
signal rdreq_next:      std_logic;
signal wrreq_data:      std_logic_vector(132 downto 0);
signal wrreq_vld:       std_logic;
signal wrreq_next:      std_logic;
signal rdout_data:      std_logic_vector(63 downto 0);
signal rdout_vld:       std_logic;
signal rdout_next:      std_logic;
signal read_address:    std_logic_vector(23 downto 0);
signal read_data:       std_logic_vector(31 downto 0);
signal read_req:        std_logic;
signal write_address:   std_logic_vector(23 downto 0);
signal write_data:      std_logic_vector(31 downto 0);
signal write_req:       std_logic;

signal ram0_address:        std_logic_vector(13 downto 0);
signal ram0_read_data:      std_logic_vector(31 downto 0);
signal ram0_write_data:     std_logic_vector(31 downto 0);
signal ram0_wr_enable:      std_logic;

signal comm_hw_ilock1_wr:       std_logic_vector(31 downto 0);
signal comm_hw_ilock1_wr_en:    std_logic;
signal comm_hw_ilock2_wr:       std_logic_vector(31 downto 0);
signal comm_hw_ilock2_wr_en:    std_logic;
signal comm_sw_ilock1_wr:       std_logic_vector(31 downto 0);
signal comm_sw_ilock1_wr_en:    std_logic;
signal comm_sw_ilock2_wr:       std_logic_vector(31 downto 0);
signal comm_sw_ilock2_wr_en:    std_logic;
signal comm_set_user_wr:        std_logic_vector(31 downto 0);
signal comm_set_user_wr_en:     std_logic;
signal comm_set_wdg_time_wr:    std_logic_vector(31 downto 0);
signal comm_set_wdg_time_wr_en: std_logic;
signal comm_reset_wdg_wr:       std_logic_vector(31 downto 0);
signal comm_reset_wdg_wr_en:    std_logic;


--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------	Architecture begin  ------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
begin

DUT_VMECMI_MMbus32: entity work.VMECMI_MMbus32
port map (
    clk            => clk          ,
    nrst           => nrst         ,
    rdreq_data     => rdreq_data   ,
    rdreq_vld      => rdreq_vld    ,
    rdreq_next     => rdreq_next   ,
    wrreq_data     => wrreq_data   ,
    wrreq_vld      => wrreq_vld    ,
    wrreq_next     => wrreq_next   ,
    rdout_data     => rdout_data   ,
    rdout_vld      => rdout_vld    ,
    rdout_next     => rdout_next   ,
    read_address   => read_address ,
    read_data      => read_data    ,
    read_req       => read_req     ,
    write_address  => write_address,
    write_data     => write_data   ,
    write_req      => write_req
);


DUT_vme_cs_registers: entity work.vme_cs_registers
port map (
    clk                     => clk,
    
    read_address            => read_address ,
    read_data               => read_data    ,
    read_req                => read_req     ,
    write_address           => write_address,
    write_data              => write_data   ,
    write_req               => write_req    ,
    
    ram0_wr                 => ram0_write_data,
    ram0_wr_en              => ram0_wr_enable,
    ram0_rd                 => ram0_read_data,
    
    synth_date_rd           => x"20141125",
    synth_time_rd           => x"00001237",
    serial_id_h_rd          => x"5AA500FF",
    serial_id_l_rd          => x"FF00A55A",
    status_bits_rd          => x"A5A5F0F0",
    temperature_rd          => x"5A5A0F0F",
    beam_in_msec_rd         => x"00000350",
    beam_out_msec_rd        => x"00000850",
    basic_period_msec_rd    => x"00001200",
    cibu1_status_rd         => x"00000001",
    cibu2_status_rd         => x"00000002",
    hw_ilock1_rd            => x"00000003",
    hw_ilock2_rd            => x"00000004",
    sw_ilock1_rd            => x"00000005",
    sw_ilock2_rd            => x"00000006",
    current_user_rd         => x"00000009",
    wdg_timer_rd            => x"00000007",
    wdg_time_rd             => x"00000008",
    comm_hw_ilock1_wr       => comm_hw_ilock1_wr,
    comm_hw_ilock1_wr_en    => comm_hw_ilock1_wr_en,
    comm_hw_ilock2_wr       => comm_hw_ilock2_wr,
    comm_hw_ilock2_wr_en    => comm_hw_ilock2_wr_en,
    comm_sw_ilock1_wr       => comm_sw_ilock1_wr,
    comm_sw_ilock1_wr_en    => comm_sw_ilock1_wr_en,
    comm_sw_ilock2_wr       => comm_sw_ilock2_wr,
    comm_sw_ilock2_wr_en    => comm_sw_ilock2_wr_en,
    comm_set_user_wr        => comm_set_user_wr,
    comm_set_user_wr_en     => comm_set_user_wr_en,
    comm_set_wdg_time_wr    => comm_set_wdg_time_wr,
    comm_set_wdg_time_wr_en => comm_set_wdg_time_wr_en,
    comm_reset_wdg_wr       => comm_reset_wdg_wr,
    comm_reset_wdg_wr_en    => comm_reset_wdg_wr_en,
    hv1_volt_value_rd       => x"10000001",
    hv2_volt_value_rd       => x"10000002",
    hv1_volt_max_rd         => x"10000003",
    hv2_volt_max_rd         => x"10000004",
    hv1_volt_min_rd         => x"10000005",
    hv2_volt_min_rd         => x"10000006",
    hv1_curr_value_rd       => x"10000007",
    hv2_curr_value_rd       => x"10000008",
    hv1_curr_max_rd         => x"10000009",
    hv2_curr_max_rd         => x"1000000A",
    hv1_curr_min_rd         => x"1000000B",
    hv2_curr_min_rd         => x"1000000C",
    hv1_volt_high_cnt_rd    => x"1000000D",
    hv1_volt_low_cnt_rd     => x"1000000E",
    hv1_curr_high_cnt_rd    => x"1000000F",
    hv1_curr_low_cnt_rd     => x"10000010",
    hv2_volt_high_cnt_rd    => x"10000011",
    hv2_volt_low_cnt_rd     => x"10000012",
    hv2_curr_high_cnt_rd    => x"10000013",
    hv2_curr_low_cnt_rd     => x"10000014",
    hv_comparators_rd       => x"10000015",
    hv1_dc_level_wr         => open,
    hv1_dc_level_wr_en      => open,
    hv1_dc_level_rd         => x"5AA5A55A",
    hv1_ac_amplitude_wr     => open,
    hv1_ac_amplitude_wr_en  => open,
    hv1_ac_amplitude_rd     => x"5AA5A55A",
    hv1_ac_gain_p1_wr       => open,
    hv1_ac_gain_p1_wr_en    => open,
    hv1_ac_gain_p1_rd       => x"5AA5A55A",
    hv1_ac_gain_p2_wr       => open,
    hv1_ac_gain_p2_wr_en    => open,
    hv1_ac_gain_p2_rd       => x"5AA5A55A",
    hv2_dc_level_wr         => open,
    hv2_dc_level_wr_en      => open,
    hv2_dc_level_rd         => x"5AA5A55A",
    hv2_ac_amplitude_wr     => open,
    hv2_ac_amplitude_wr_en  => open,
    hv2_ac_amplitude_rd     => x"5AA5A55A",
    hv2_ac_gain_p1_wr       => open,
    hv2_ac_gain_p1_wr_en    => open,
    hv2_ac_gain_p1_rd       => x"5AA5A55A",
    hv2_ac_gain_p2_wr       => open,
    hv2_ac_gain_p2_wr_en    => open,
    hv2_ac_gain_p2_rd       => x"5AA5A55A",
    hv_ac_frequency_wr      => open,
    hv_ac_frequency_wr_en   => open,
    hv_ac_frequency_rd      => x"5AA5A55A",
    vme3v3_value_rd         => x"5AA5A55A",
    vme5v_value_rd          => x"5AA5A55A",
    analog_p15v_value_rd    => x"5AA5A55A",
    analog_n15v_value_rd    => x"5AA5A55A",
    analog_5v_value_rd      => x"5AA5A55A",
    ref_5v_value_rd         => x"5AA5A55A",
    ref_10va_value_rd       => x"5AA5A55A",
    ref_10vb_value_rd       => x"5AA5A55A",
    vme3v3_min_rd           => x"5AA5A55A",
    vme3v3_max_rd           => x"5AA5A55A",
    vme5v_min_rd            => x"5AA5A55A",
    vme5v_max_rd            => x"5AA5A55A",
    analog_p15v_min_rd      => x"5AA5A55A",
    analog_p15v_max_rd      => x"5AA5A55A",
    analog_n15v_min_rd      => x"5AA5A55A",
    analog_n15v_max_rd      => x"5AA5A55A",
    analog_5v_min_rd        => x"5AA5A55A",
    analog_5v_max_rd        => x"5AA5A55A",
    ref_5v_min_rd           => x"5AA5A55A",
    ref_5v_max_rd           => x"5AA5A55A",
    ref_10va_min_rd         => x"5AA5A55A",
    ref_10va_max_rd         => x"5AA5A55A",
    ref_10vb_min_rd         => x"5AA5A55A",
    ref_10vb_max_rd         => x"5AA5A55A",
    lv_comparators_rd       => x"5AA5A55A",
    analog5v_cnt_rd         => x"5AA5A55A",
    analog15v_cnt_rd        => x"5AA5A55A",
    vme3v3_cnt_rd           => x"5AA5A55A",
    vme5v_cnt_rd            => x"5AA5A55A",
    vme12v_cnt_rd           => x"5AA5A55A"
);

-- test ram block
DUT_ram_block: ENTITY work.ram_32
PORT MAP
(
    address     => ram0_address,
    clock       => clk,
    data        => ram0_write_data,
    wren        => ram0_wr_enable,
    q           => ram0_read_data
);

ram0_address <= write_address(15 downto 2) when ram0_wr_enable = '1' else read_address(15 downto 2);
    
-- clk process
process
begin
loop
	clk<='0' ;
	wait for CLOCK_PERIOD/2;
    clk<='1';
	wait for CLOCK_PERIOD/2;
	if done_sim = '1' then
		wait;
	end if;
end loop;
end process;

-- Main simulation flow process
process
variable offset : natural := 0;
begin

    done_sim <= '0';
    nrst <= '0';
    
    comm_hw_ilock1_wr <= x"00000000";
    comm_hw_ilock1_wr_en <= '0';
    comm_hw_ilock2_wr <= x"00000000";
    comm_hw_ilock2_wr_en <= '0';
    comm_sw_ilock1_wr <= x"00000000";
    comm_sw_ilock1_wr_en <= '0';
    comm_sw_ilock2_wr <= x"00000000";
    comm_sw_ilock2_wr_en <= '0';
    comm_set_user_wr <= x"00000000";
    comm_set_user_wr_en <= '0';
    comm_set_wdg_time_wr <= x"00000000";
    comm_set_wdg_time_wr_en <= '0';
    comm_reset_wdg_wr <= x"00000000";
    comm_reset_wdg_wr_en <= '0';
    
    rdreq_data  <= '0' & x"000000000000000";
    rdreq_vld   <= '0';
    wrreq_data  <= '0' & x"000000000000000000000000000000000";
    wrreq_vld   <= '0';
    rdout_next  <= '1';
    
    wait for 100 ns;
    nrst <= '1';
    
    -- read HV registers
    wait until rising_edge(clk);
    rdreq_vld   <= '1';
    rdreq_data(27 downto 0) <= std_logic_vector(to_unsigned(28573696, 28));
    for I in 1 to 20 loop
    
        wait until rising_edge(clk);
        
        if rdreq_next = '1' then
            offset := offset + 1;
            rdreq_data(27 downto 0) <= std_logic_vector(to_unsigned(28573696, 28) + offset);
        end if;
        
    end loop;
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    rdreq_vld   <= '0';
    
    
    -- write to ram xD000000 address (27262976)
    -- write to ram xD000008 address (27262977)
    wait until rising_edge(clk);
    wrreq_vld <= '1';
    wrreq_data(96 downto 72) <= std_logic_vector(to_unsigned(27262977, 25));    -- address bits
    wrreq_data(71 downto 64) <= x"0F";                                          -- byte enable bits
    wrreq_data(63 downto 32) <= x"30000002";                                    -- data high 32 bits
    wrreq_data(31 downto 0) <= x"30000001";                                     -- data low 32 bits
    wait until rising_edge(clk);
    wrreq_data(71 downto 64) <= x"F0";
    wait until rising_edge(clk);
    wrreq_data  <= '0' & x"000000000000000000000000000000000";
    wrreq_vld   <= '0';
    
    -- break
    wait until rising_edge(clk);
    wait until rising_edge(clk);
    
    -- read ram
    wait until rising_edge(clk);
    rdreq_vld   <= '1';
    rdreq_data(27 downto 0) <= std_logic_vector(to_unsigned(27262977, 28));
    wait until rising_edge(clk);
    rdreq_vld   <= '0';
    
    wait for 400 ns;
    done_sim <= '1';
    
    report "Simulation completed" severity failure;

end process;


end behaviour;


