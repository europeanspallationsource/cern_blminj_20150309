------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    20/11/2014
--Module Name:    VMECMI_MMbus32
--Project Name:   VMECMI_MMbus32
--Description:   
--  This module translates the VME_Core_CMA bus (3x CMI port) 
--  into the standard 32 bit memory mapped bus
--  
--  
--  Date            Author      Change
--  
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity VMECMI_MMbus32 is
port    (
    --local clock and reset
    clk:                in std_logic;
    nrst:               in std_logic;
    
    -- VME Access Controller
    rdreq_data:         in std_logic_vector(60 downto 0);
    rdreq_vld:          in std_logic;
    rdreq_next:         out  std_logic;
    
    wrreq_data:         in std_logic_vector(132 downto 0);
    wrreq_vld:          in std_logic;
    wrreq_next:         out  std_logic;
    
    rdout_data:         out  std_logic_vector(63 downto 0);
    rdout_vld:          out  std_logic;
    rdout_next:         in std_logic;
    
    -- Memory mapped bus
    read_address:       out std_logic_vector(23 downto 0);
    read_data:          in std_logic_vector(31 downto 0);
    read_req:           out std_logic;
    
    write_address:      out std_logic_vector(23 downto 0);
    write_data:         out std_logic_vector(31 downto 0);
    write_req:          out std_logic
);
end entity VMECMI_MMbus32;


architecture rtl of VMECMI_MMbus32 is

signal be_int:          std_logic_vector(7 downto 0);
signal high_32_bit:     std_logic;

type StateType is (idle, read_high, read_low);
signal state, next_state : StateType;

signal read_address_reg : std_logic_vector(20 downto 0);
signal read_l : std_logic;
signal read_h : std_logic;
signal read_l_d1 : std_logic;
signal read_h_d1 : std_logic;
signal read_h_d2 : std_logic;
signal rdreq_next_d0 : std_logic;

BEGIN
    
    -----------------------------------------------
    -- write request from the VME core
    -----------------------------------------------
    
    -- decode requested VME address and data to write
    
    -- high and low 32 bit register selects
    -- address must be 32 bit aligned
    be_int <= wrreq_data(71 downto 64);
    high_32_bit <= '1' when be_int = x"F0" else '0';
    
    write_address <= wrreq_data(92 downto 72) & "000" when high_32_bit = '1' else wrreq_data(92 downto 72) & "100";
    write_data <= wrreq_data(63 downto 32) when high_32_bit = '1' else wrreq_data(31 downto 0);
    
    wrreq_next <= '1';
    write_req <= wrreq_vld;

    
    -----------------------------------------------
    -- read request from the VME core
    -----------------------------------------------
    
    
    fsm_cnt_p: process(clk)
    begin
        if rising_edge(clk) then
            if nrst = '0' then
                state <= idle;
            else
                state <= next_state;
            end if;
        end if;
    end process;

    fsm_cl_p: process(state, rdreq_vld, rdreq_data)
    begin
        next_state <= state;
        rdreq_next_d0 <= '1';
        read_h <= '0';
        read_l <= '0';
        
        case state is
        
            when idle =>
                if rdreq_vld = '1' then
                    next_state <= read_low;
                end if;
                
            when read_low =>
                rdreq_next_d0 <= '0';
                read_l <= '1';
                next_state <= read_high;
            
            when read_high =>
                read_h <= '1';
                if rdreq_vld = '1' then
                    next_state <= read_low;
                else
                    next_state <= idle;
                end if;
                
            when others =>
                next_state <= idle;

        end case;
    end process;
    
    rdreq_next <= rdreq_next_d0;
    
    -- read request is always for 64 bit data
    -- it is necessary to make 2 x 32 bit requests on the MM bus
    
    rd_addr_reg: process(clk) is
    begin
        if rising_edge(clk) then
            if nrst = '0' then
                read_address_reg <= (others=>'0');
            elsif rdreq_vld = '1' and rdreq_next_d0 = '1' then
                read_address_reg <= rdreq_data(20 downto 0);
            end if;
        end if;
    end process;
    
    read_address <= read_address_reg & "000" when read_l = '1' else read_address_reg & "100";
    
    data_64_reg: process(clk) is
    begin
        if rising_edge(clk) then
            if read_l_d1 = '1' then
                rdout_data(63 downto 32) <= read_data;
            elsif read_h_d1 = '1' then
                rdout_data(31 downto 0) <= read_data;
            end if;
        end if;
    end process;
    
    rdout_vld <= read_h_d2;
    
    ff_reg: process(clk) is
    begin
        if rising_edge(clk) then
            if nrst = '0' then
                read_h_d1 <= '0';
                read_h_d2 <= '0';
                read_l_d1 <= '0';
            else
                read_h_d1 <= read_h;
                read_h_d2 <= read_h_d1;
                read_l_d1 <= read_l;
            end if;
        end if;
    end process;
    
    read_req <= read_h or read_l;
    
    
        
end architecture rtl;
