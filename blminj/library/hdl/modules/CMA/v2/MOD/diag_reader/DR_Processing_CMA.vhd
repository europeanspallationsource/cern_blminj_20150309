------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/09/2014
Module Name:    DR_Processing_CMA


-----------------
Short Description
-----------------

    This is the internal Diagnostic Reader Processing Module for one given Diagnostic Interface.
    It takes N inputs (depending on the Diag. Interface), compares them with a Threshold, stores them, slices and packages them
    into the 16+2 package.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    header_data_size    := size of the header CMI input data port
    body_data_size      := size of the body CMI input data port
    out_data_size       := size of the output CMI data port

--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DR_Processing_CMA is

generic
(
    inp_data_size:      integer;
    header_size:        integer;
    pkg_size:           integer;
    tag_n:              integer
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Timing Pulses
    UPDATE_pulse:       in  std_logic;
    -- Parameter
    PARAM_headers:      in  slv_array(tag_n-1 downto 0)(header_size-1 downto 0);
    -- CMI Input
    inp_data:           in  slv_array(tag_n-1 downto 0)(inp_data_size-1 downto 0);
    inp_vld:            in  std_logic_vector(tag_n-1 downto 0);
    inp_next:           out std_logic_vector(tag_n-1 downto 0);
    -- CMI Output
    out_data:           out std_logic_vector(17 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic
);

end entity DR_Processing_CMA;


architecture struct of DR_Processing_CMA is
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- storage_CMA
    constant storage_data_size:     integer := inp_data_size;
    -- slice_CMA
    constant slice_data_size:       integer := 16;
    -- pkg_CMA
    constant pkg_data_size:         integer := 18;
    
    -- mux_CMA
    constant mux_data_size:         integer := pkg_data_size;
    constant mux_tx_number:         integer := tag_n;
    constant mux_rx_number:         integer := 1;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- storage_CMA
    signal storage_data:            slv_array(tag_n-1 downto 0)(storage_data_size-1 downto 0);
    signal storage_vld:             std_logic_vector(tag_n-1 downto 0);
    signal storage_next:            std_logic_vector(tag_n-1 downto 0);
        
    -- slice_CMA
    signal slice_data:              slv_array(tag_n-1 downto 0)(slice_data_size-1 downto 0);
    signal slice_vld:               std_logic_vector(tag_n-1 downto 0);
    signal slice_next:              std_logic_vector(tag_n-1 downto 0);

    -- pkg_CMA
    signal pkg_data:                slv_array(tag_n-1 downto 0)(pkg_data_size-1 downto 0);
    signal pkg_vld:                 std_logic_vector(tag_n-1 downto 0);
    signal pkg_next:                std_logic_vector(tag_n-1 downto 0);
    
    -- mux_CMA
    signal mux_tx_data:             slv_array(mux_tx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_tx_vld:              std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_tx_next:             std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_rx_data:             slv_array(mux_rx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_rx_vld:              std_logic_vector(mux_rx_number-1 downto 0);
    signal mux_rx_next:             std_logic_vector(mux_rx_number-1 downto 0);
    
    signal ARB_mux_tx_vld:          std_logic_vector(mux_tx_number-1 downto 0);
    signal ARB_mux_tx_next:         std_logic_vector(mux_tx_number-1 downto 0);
    signal ARB_mux_rx_vld:          std_logic;
    signal ARB_mux_rx_next:         std_logic;
    signal ARB_mux_sel:             std_logic_vector(get_vsize_addr(mux_tx_number)-1 downto 0);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- UPDATE/DATA AVAILABLE
    signal data_av:         std_logic_vector(tag_n-1 downto 0);
    signal upd_int:         std_logic;
    
BEGIN

    
    --====================--
    -- UPD/DATA AVAILABLE --
    --====================--
        
    upd_int <=  UPDATE_pulse when AND_REDUCE(data_av) = '1' else
                '0';
    
    --==============--
    -- PROCESSESING --
    --==============--
    
    
    proc_tags: for i in 0 to tag_n-1 generate
    
        sync_storage_CMA_inst: entity work.sync_storage_CMA
        GENERIC MAP
        (
            data_size       => storage_data_size
        )
        PORT MAP
        (
            clk             => clk,
            upd_data        => inp_data(i),
            upd_vld         => inp_vld(i),
            upd_next        => inp_next(i),
            stored_data     => storage_data(i),
            stored_vld      => storage_vld(i),
            stored_next     => storage_next(i),
            upd_storage     => upd_int,
            new_data_av     => data_av(i)
        );
    
        slice_CMA_inst: entity work.slice_CMA
        GENERIC MAP
        (
            inp_data_size   => storage_data_size,
            out_data_size   => slice_data_size
        )
        PORT MAP
        (
            clk             => clk,
            inp_data        => storage_data(i),
            inp_vld         => storage_vld(i),
            inp_next        => storage_next(i),
            out_data        => slice_data(i),
            out_vld         => slice_vld(i),
            out_next        => slice_next(i)
        );
        
        package_CMA_inst: entity work.package_CMA
        GENERIC MAP
        (
            inp_data_size   => storage_data_size,
            pkg_size        => pkg_size,
            header_size     => header_size
        )
        PORT MAP
        (
            clk             => clk,
            PARAM_header    => PARAM_headers(i),
            pkg_done        => open,
            inp_data        => slice_data(i),
            inp_vld         => slice_vld(i),
            inp_next        => slice_next(i),
            out_data        => pkg_data(i),
            out_vld         => pkg_vld(i),
            out_next        => pkg_next(i)
        );
    
    end generate;
    
    --=============--
    -- MULTIPLEXER --
    --=============--
    
    -- mux_CMI --
    mux_CMI: entity work.MM_CMI 
    generic map
    (
        tx_number       => mux_tx_number,
        rx_number       => mux_rx_number,
        data_size       => mux_data_size
    )
    port map 
    (
        clk             => clk,
        tx_data         => mux_tx_data,
        tx_vld          => mux_tx_vld,
        tx_next         => mux_tx_next,
        rx_data         => mux_rx_data,
        rx_vld          => mux_rx_vld,
        rx_next         => mux_rx_next,
        ARB_tx_vld      => ARB_mux_tx_vld,
        ARB_tx_next     => ARB_mux_tx_next,
        ARB_rx_vld      => ARB_mux_rx_vld,
        ARB_rx_next     => ARB_mux_rx_next,
        ARB_sel         => ARB_mux_sel
        
    );

    mux_CMI_ARB: entity work.priority_pkg_mux_ARB
    generic map
    (
        tx_number       => mux_tx_number,
        data_size       => mux_data_size
    )
    port map
    (
        clk             => clk,
        full_data       => mux_tx_data,
        ARB_tx_vld      => ARB_mux_tx_vld,
        ARB_tx_next     => ARB_mux_tx_next,
        ARB_rx_vld      => ARB_mux_rx_vld,
        ARB_rx_next     => ARB_mux_rx_next,
        ARB_sel         => ARB_mux_sel
    );
    
end architecture struct;
