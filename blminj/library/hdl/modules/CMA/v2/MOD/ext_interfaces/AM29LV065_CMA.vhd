------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Balzs Nemeth
Updated:        08/05/2014
Module Name:    AM29LV065_CMA

-----------
Description
-----------

    for the flash

------------------
Generics/Constants
------------------
	
	Tclk_ns: time:= 12.5 ns --for timings, 80MHz?
	
----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

---------------
Necessary Cores
---------------

--------------
Implementation
--------------
    you can access with cmi to the Flash
	 there are rdreq, wr, erase, functions
	 It has the same priority as above.
	 after action it puts the FLASH_BUS_REQ ='1' and start the processing after FLASH_BUS_ACK = '1'
	 if you want to read it sends out via rdout cmi port.
	 
	 in idle state 
	 FLASH_A:                inout std_logic_vector(22 downto 0); is '0'
    FLASH_CEn:              inout std_logic; is 'Z'
    FLASH_OEn:              inout std_logic; is 'Z'
    FLASH_WEn:              inout std_logic; is 'Z'
    Stratix_Flash_csN:      out std_logic; is '1'
	 
------------------------
Not implemented Features
------------------------
	--reset handling after reset
	--reset handling after Vcc
	--block write function

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

entity AM29LV065_CMA is
generic(

	Tclk_ns: time:= 12.5 ns --for timings or real??? 80MHz
);
port
(
    -- clock
    clk:          in  std_logic;
    -- Flash (as long as there is no DAB64x complete FW)
    FLASH_A:                inout std_logic_vector(22 downto 0);
    FLASH_CEn:              inout std_logic;
    FLASH_OEn:              inout std_logic;
    FLASH_WEn:              inout std_logic;
    Stratix_Flash_csN:      out std_logic;
	 
--	 FLASH_D_in:				in std_logic_vector(7 downto 0);
--	 FLASH_D_out:				out std_logic_vector(7 downto 0);
--	 FLASH_ACC:					out std_logic;
--	 FLASH_RST:					out std_logic;
--	 FLASH_RY:				 	in std_logic;
	 
	 -- Flash_Interface
    FLASH_DATA_IN:          in std_logic_vector(7 downto 0); -- from flash
    FLASH_DATA_OUT:         out  std_logic_vector(7 downto 0); -- to flash
    FLASH_BUS_REQ:          out  std_logic; --if i want to use the data bus
    FLASH_BUS_ACK:          in std_logic; -- if data bus is ready
	 FLASH_BUS_DIR:			 out std_logic; --'1' active, '0' highZ
	 
	 -- CMI write
    wr_data:           		in std_logic_vector(30 downto 0); -- 7-0 is data, 30-8 address
    wr_vld:            		in std_logic;
    wr_next:          		out std_logic;
	 
	 -- CMI rdreq
    rdreq_data:           in std_logic_vector(22 downto 0);
    rdreq_vld:            in std_logic;
    rdreq_next:           out std_logic;
	 
	 -- CMI output
    rdout_data:           out std_logic_vector(7 downto 0);
    rdout_vld:            out std_logic;
    rdout_next:           in  std_logic;
	 
	 -- CMI erase
    erase_data:           in std_logic_vector(7 downto 0);
    erase_vld:            in std_logic;
    erase_next:           out  std_logic
);
    
end entity AM29LV065_CMA;


architecture logic of AM29LV065_CMA is
    
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    --constant Tclk_ns:               real := 12.5; -- 80 MHz
    
    -- Sequence Gen
    constant delay_max:      			integer := 100 ns /Tclk_ns;
	 constant delay_reset:      		integer := 500 ns /Tclk_ns;
	 constant delay_30:					integer := 30 ns / Tclk_ns;
	 constant delay_45:					integer := 45 ns / Tclk_ns;
	 constant delay_90:					integer := 90 ns / Tclk_ns;
	 constant delay_5000:				integer := 5 us / Tclk_ns; --rewrite
	 constant delay_50000:				integer := 50 us / Tclk_ns; --after reset
	 constant delay_900ms:				integer := 900 ms / Tclk_ns; --chip erase
	 
	 constant delay_number:				integer := 7;
	 constant counter_prog_size:		integer := 4;
	 constant block_size:				integer := 128;
	 --type mem is array (0 to delay_number)of 	integer;
	 --delay_array:							mem:=(delay_reset,delay_45,delay_30)
    signal timer:							integer;
	 signal timer_n:						integer;
	 signal timer_p:						integer;
	 
	 type s_state is (s_idle, s_reset, s_write,s_program,s_erase,s_read,s_blockwrite);
    signal s_present, s_next:			s_state;
	 signal program_state_n:			s_state;
	 signal program_state_p:			s_state;
	 signal counter_prog:				unsigned (counter_prog_size-1 downto 0);
	 signal counter_prog_last:			unsigned (counter_prog_size-1 downto 0);
	 
	 signal block_map: 					std_logic_vector(block_size-1 downto 0); --1 if sectos is not erased, 0 if erase
	 
	 signal rdout_data_int:          std_logic_vector(7 downto 0);
	 signal rdreq_data_int:				std_logic_vector(22 downto 0);
	 signal wr_data_int:					std_logic_vector(7 downto 0);
	-- signal erase_data_int:				std_logic_vector(7 downto 0);
    signal rdout_vld_int:           std_logic;
	 signal wr_next_int:          	std_logic;
	 signal rdreq_next_int:          std_logic;
	 signal erase_next_int:				std_logic;
	 signal wr_addr_int_p:				std_logic_vector(22 downto 0);
	 signal wr_addr_int_n:				std_logic_vector(22 downto 0);
	 signal wr_data_int_n:				std_logic_vector(7 downto 0);
	 signal wr_data_int_p:				std_logic_vector(7 downto 0);
	 
	 signal FLASH_A_int:             std_logic_vector(22 downto 0);
	 --signal FLASH_D_out_int:			std_logic_vector(7 downto 0);
    signal FLASH_CEn_int:           std_logic;
    signal FLASH_OEn_int:           std_logic;
    signal FLASH_WEn_int:           std_logic;
	 --signal FLASH_ACC_int:				std_logic;
	 --signal FLASH_RST_int:				std_logic;
	 signal Stratix_Flash_csN_int:   std_logic;
	 signal program_command:			std_logic_vector(7 downto 0);
	 signal timer_en:						std_logic;
	 signal res:							std_logic := '0';
	 signal timer_res:					std_logic;
	 signal timer_load:					std_logic;
	 
begin
    
process (clk) is
    begin
        if rising_edge(clk) then
				if res = '1' then
					s_present 		<= s_reset;

				else
					timer_p			<= timer_n;
					s_present 		<= s_next;
				end if;
				
        end if;
    end process;
	 
program_timer: process (clk) is
	begin
		if rising_edge(clk) then
			if timer_res = '1' or res = '1' then
				timer 				<= 0;
				counter_prog 		<= (others => '0');
				timer_load 			<= '1';
			elsif timer_en = '1' then
				if timer = 0 and timer_load = '0' then
					counter_prog 	<= counter_prog + 1; --program counter ++
					timer_load 		<= '1';
				elsif timer > 0 then
					timer 			<= timer -1;
				elsif timer_load = '1' then --timer load
				timer 				<= timer_n;
				timer_load 			<= '0';
				end if;
			end if;
			wr_addr_int_p			<= wr_addr_int_n;
			wr_data_int_p			<= wr_data_int_n;
			program_state_p		<= program_state_n;
			counter_prog_last 	<= counter_prog;
			
		end if;
end process;

process (clk) is
    begin
        if rising_edge(clk) then
            if rdout_next = '1' then
					rdout_data 		<= rdout_data_int;
					rdout_vld 		<= rdout_vld_int;
				end if;
        end if;
    end process;
    
process (all) is
    begin
        -- DEFAULTS
        -- CMI
			wr_next_int 					<= '0';
			rdreq_next_int 				<= '0';
			erase_next_int					<= '0';
			rdout_vld_int 					<= '0';
        	wr_next 							<= wr_next_int or not wr_vld;
			rdreq_next 						<= rdreq_next_int or not rdreq_vld;
			erase_next						<= erase_next_int or not erase_vld;

			FLASH_A 							<= FLASH_A_int;
			FLASH_CEn 						<= FLASH_CEn_int;
			FLASH_OEn 						<= FLASH_OEn_int;
			FLASH_WEn 						<= FLASH_WEn_int;
			Stratix_Flash_csN 			<= Stratix_Flash_csN_int;
			
			--FLASH_D_out 					<= FLASH_D_out_int;
			--FLASH_ACC 						<= FLASH_ACC_int;
			--FLASH_RST 						<= FLASH_RST_int;
			
			FLASH_DATA_OUT					<= (others => '0');
			FLASH_BUS_REQ					<= '0';
			
			FLASH_A_int 					<= (others => '0');
			FLASH_CEn_int 					<= 'Z';
			FLASH_OEn_int 					<= 'Z';
			FLASH_WEn_int 					<= 'Z';
			Stratix_Flash_csN_int 		<= '1'; --??
			
			--FLASH_D_out_int 				<= (others => '0');
			--FLASH_ACC_int 					<= '0'; --acceleration off
			--FLASH_RST_int 					<= ;
			
			timer_en							<= '0';
			program_state_n				<= program_state_p;
			wr_data_int_n					<= wr_data_int_p;
			wr_addr_int_n					<= wr_addr_int_p;
			FLASH_BUS_DIR					<= '0'; -- highZ
			timer_res						<= '0';
			s_next							<= s_present;
			timer_n							<= timer_p;
			
			rdout_data_int					<= FLASH_DATA_IN;
			
			if program_state_p = s_erase then
				program_command 		<= x"55"; --erase
				FLASH_A_int				<= "000" & x"000" & wr_data_int_p; --erase address
			else
				program_command 		<= x"A0"; --write
				FLASH_A_int				<= wr_addr_int_p; --write address
			end if;
					
			case s_present is
            
            when s_idle =>
					timer_n						<= 0;
 					s_next 						<= s_idle; 
					wr_next_int 				<= '1';
					rdreq_next_int 			<= '1';
					erase_next_int				<= '1';
					timer_res					<= '1';
					if rdreq_vld = '1' then --read
						wr_next_int 			<= '0';
						erase_next_int			<= '0';
						wr_addr_int_n 			<= rdreq_data;
						s_next 					<= s_read;
					elsif wr_vld = '1' then --write
						rdreq_next_int 		<= '0';
						erase_next_int			<= '0';
						wr_data_int_n 			<= wr_data( 7 downto 0);
						wr_addr_int_n			<= wr_data( 30 downto 8);
						s_next 					<= s_program;
						program_state_n		<= s_write;
					elsif erase_vld = '1' then --erase a block
						wr_next_int 			<= '0';
						rdreq_next_int 		<= '0';
						wr_data_int_n 			<= erase_data;
						s_next 					<= s_program;
						program_state_n		<= s_erase;
					end if;
            
            when s_reset => --I dont know... :)
				
 					s_next <= s_reset; 
					
				when s_write =>
				
					s_next <= s_write; 
					--unused

            when s_program =>
				
 					s_next 						<= s_program;   
					
					FLASH_CEn_int 				<= '1';
					FLASH_OEn_int 				<= '1';
					FLASH_WEn_int 				<= '1';
					FLASH_BUS_REQ				<= '1';
					FLASH_BUS_DIR				<= '0';
					FLASH_DATA_OUT				<= x"00";
					
					if FLASH_BUS_ACK = '1' then
						timer_en					<= '1';
						FLASH_CEn_int 			<= '0';
						if counter_prog = "0000" then --precycle
							
							FLASH_OEn_int 		<= '1';
							FLASH_WEn_int 		<= '1';
							timer_n				<= delay_30;
						
						elsif counter_prog = "0001" then -- programcommand
							FLASH_OEn_int 		<= '1';
							FLASH_WEn_int 		<= '0';
							FLASH_BUS_DIR		<= '1';
							FLASH_DATA_OUT		<= program_command; --A0 if write, 55 if erase
							timer_n				<= delay_45;
						elsif counter_prog = "0010" then -- wait
							FLASH_OEn_int 		<= '1';
							FLASH_WEn_int 		<= '1';
							timer_n				<= delay_30;
						elsif counter_prog = "0011" then -- program data or erase sector
							FLASH_OEn_int 		<= '1';
							FLASH_WEn_int 		<= '0';
							FLASH_BUS_DIR		<= '1';
							if program_state_p = s_erase then
								FLASH_DATA_OUT	<= x"30"; -- x"30" erase
							else
								FLASH_DATA_OUT	<= wr_data_int_p; --data for write
							end if;
							timer_n				<= delay_45;
						elsif counter_prog = "0100" then --long wait
							FLASH_OEn_int 		<= '1';
							FLASH_WEn_int 		<= '1';
							timer_n				<= delay_5000;
						elsif counter_prog = "0101" then --get status
							FLASH_OEn_int 		<= '0';
							FLASH_WEn_int 		<= '1';
							timer_n				<= delay_90;
						elsif counter_prog = "0110" then -- wait
							FLASH_OEn_int 		<= '1';
							FLASH_WEn_int 		<= '1';
							timer_n				<= delay_30;
						elsif counter_prog = "0111" then --read back if needed
							FLASH_OEn_int 		<= '0';
							FLASH_WEn_int 		<= '1';
							timer_n				<= delay_90;
						else
							s_next 				<= s_idle;
						end if;
					end if;
				
				when s_erase =>
				
					s_next <= s_erase;
					--unused
            
				
            when s_read =>
					s_next <= s_read;
					FLASH_CEn_int 				<= '1';
					FLASH_OEn_int 				<= '1';
					FLASH_WEn_int 				<= '1';
					FLASH_BUS_REQ				<= '1';
					FLASH_BUS_DIR				<= '0';
					FLASH_A_int					<= wr_addr_int_p;
					if FLASH_BUS_ACK = '1' then
						timer_en					<= '1';
						FLASH_CEn_int 			<= '0';
						if counter_prog = "0000" then --precycle
							timer_n				<= delay_30;
						elsif counter_prog = "0001" then -- read ask
							FLASH_OEn_int 		<= '0';
							timer_n				<= delay_45;
							--itt tartottam
						elsif counter_prog = "0010" then -- read out
							timer_en 			<= '0';
							FLASH_OEn_int 		<= '0';
							rdout_vld_int		<= '1';
							if rdout_next = '1' then
								s_next 			<= s_idle;
							end if;
						end if;
						
					end if;
					
            when s_blockwrite =>
				
					s_next <= s_blockwrite;	
		
				when others =>
				
					s_next <= s_idle;
                
        end case;
    end process;
   
    
    
end architecture logic;