------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Update Date:    20/06/2012
Module Name:    DS2411_CMA 

-----------
Description
-----------

    CMI Module for 1-wire communication with the Maxim DS2411 Serial Number Chip.

------------------
Generics/Constants
------------------

    Tclk_ns := system clock period in ns (minimum clock frequency needs to be 2 MHz = 500 ns)

    timer_vsize := vectorsize for the internal timer
    T_xxxx      := internal DS2411 timings
    instr       := locally stored readout instruction
    
-----------
Limitations
-----------

    < limitations for the generics and/or the module itself >


----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
 
-----------------
Necessary Modules
-----------------

    CMI

---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    This module uses the ReadROM command to access the 48-bit ID including the 8-bit CRC and the 8-bit Family Code (01h for Maxim). 

    The Implementation consist mainly of the following controls:

    1. CMI Control

    - manages the internal CMI interfaces
    - is triggered by a vld on the req-CMI
    - after a request, it presents the ID value on the rdout-CMI output


    4. INTF Control

    - implements one read access to the connected chip
    - is triggered by intf_en


------------------------
Not implemented Features
------------------------

    - Search ROM / Overdrive Cmd missing
    - "Strange Conditions" states / ERROR HANDLING

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DS2411_CMA is

generic
(
    Tclk_ns: integer
);

port
(
    -- Internal Clock
    clk:            in  std_logic;
    -- 1-WIRE
    DATA:           inout std_logic;
    -- CMI Request Input
    rdreq_data:     in  std_logic;
    rdreq_vld:      in  std_logic;
    rdreq_next:     out std_logic;
    -- CMI Output
    rdout_data:     out std_logic_vector(63 downto 0); 
    rdout_vld:      out std_logic;
    rdout_next:     in  std_logic
);

end entity DS2411_CMA;


architecture struct of DS2411_CMA is

    --===========--
    -- CONSTANTS --
    --===========--
    
    -- Timer Vsize
    constant timer_vsize: integer := get_vsize(get_maxcnt(Tclk_ns, 600000));
    
    -- DS2411 1-wire Timings (length in given CCs)
    constant T_RSTL:    integer := 520000/Tclk_ns;
    constant T_rel:     integer := 20000/Tclk_ns;
    constant T_RSTH:    integer := 500000/Tclk_ns;
    constant T_MSP:     integer := 60000/Tclk_ns;
    constant T_SLOT:    integer := 80000/Tclk_ns;
    constant T_W0L:     integer := 70000/Tclk_ns;
    constant T_W1L:     integer := 10000/Tclk_ns;
    constant T_RL:      integer := 5000/Tclk_ns;
    constant T_MSR:     integer := 12000/Tclk_ns;
    
    -- Instruction
    constant instr:     std_logic_vector(7 downto 0) := "11001100"; -- invers command 0x33 (LSB first)

    --=====--
    -- FSM --
    --=====--
    
    type cmi_state_t is (idle, read_req, send_rdout);
    signal cmi_present_state, cmi_next_state: cmi_state_t := idle;
    
    type intf_state_t is (idle, send_rstpulse, read_prespulse, got_answer, send_instr, read_ID);
    signal intf_present_state, intf_next_state: intf_state_t := idle;
    
    --=========--
    -- COUNTER --
    --=========--
    
    -- Bit Counter
    signal bit_cnt:         UNSIGNED(2 downto 0) := "111";
    signal bit_rst:         std_logic;
    signal bit_dec:         std_logic;
    
    -- Byte Counter (for read/write access)
    signal byte_cnt:        UNSIGNED(2 downto 0) := "111";
    signal byte_rst:        std_logic;
    signal byte_dec:        std_logic;
    
    -- Timer
    signal timer:           UNSIGNED(timer_vsize-1 downto 0) := (timer_vsize-1 downto 0 => '0');
    signal timer_set:       std_logic_vector(1 downto 0);
    signal timer_dec:       std_logic;
    
    --==========--
    -- REGISTER --
    --==========--
    
    signal readout:     std_logic_vector(63 downto 0);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- Read Enable
    signal read_acq:        std_logic;
    
    -- Store data in the right register
    signal store_acq:       std_logic;
    
    -- Input Line Reg
    signal DATA_reg:        std_logic;
    
    -- 3-state DATA line control
    signal forcelow:        std_logic;
    
    -- CMI Readout  
    signal rdout_vld_int:   std_logic;
    
    -- Controls
    signal intf_en:         std_logic;
    signal intf_busy:       std_logic;
    
BEGIN

    --=======--
    -- PORTS --
    --=======--

    DATA <= '0' when forcelow = '1' else 'Z';

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if bit_rst= '1' then
                bit_cnt <= "111";
            elsif bit_dec = '1' then
                bit_cnt <= bit_cnt - 1;
            end if;
            
            if byte_rst = '1' then
                byte_cnt <= "111";
            elsif byte_dec = '1' then
                byte_cnt <= byte_cnt - 1;
            end if;
                
            if timer_set = "00" then
                timer <= TO_UNSIGNED(T_RSTL, timer_vsize);
            elsif timer_set = "01" then
                timer <= TO_UNSIGNED(T_RSTH, timer_vsize);
            elsif timer_set = "10" then
                timer <= TO_UNSIGNED(T_SLOT, timer_vsize);
            elsif timer_set = "11" then
                if timer_dec = '1' then
                    timer <= timer - 1;
                end if;
            end if;
            
        end if;
    end process;

    
    --=============--
    -- CMI CONTROL --
    --=============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            cmi_present_state <= cmi_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        rdout_vld_int   <= '0';
        rdreq_next      <= '1';
        -- INTF ctrl
        intf_en         <= '0';
        
        
        case cmi_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if rdreq_vld = '1' then
                    rdreq_next      <= '0';
                    intf_en         <= '1';
                    cmi_next_state  <= read_req;
                else
                    cmi_next_state  <= idle;
                end if;
            
            --===============--
            when read_req =>
            --===============--
                
                rdreq_next <= '0';
                if intf_busy = '1' then
                    cmi_next_state  <= read_req;
                else
                    cmi_next_state  <= send_rdout;
                end if;
                
            --===============--
            when send_rdout =>
            --===============--
                
                if rdout_next = '1' then
                    rdout_vld_int   <= '1';
                    cmi_next_state  <= idle;
                else
                    rdreq_next      <= '0';
                    cmi_next_state  <= send_rdout;
                end if;
            
            --===============--
            when others =>
            --===============--
            
                cmi_next_state <= idle;
                
        end case;
    end process;
    
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            
            if rdout_next = '1' then
                rdout_data  <= readout;
                rdout_vld   <= rdout_vld_int;
            end if;
                
        end if;
    end process;
    
    
    --============--
    -- R/W ACCESS --
    --============--

    process(clk) is
    begin
        if rising_edge(clk) then
            DATA_reg <= DATA; -- put the input in a reg
            if read_acq = '1' then
                readout <= DATA_reg & readout(63 downto 1);
            end if;
        end if;
    end process;
    
    --==============--
    -- INTF CONTROL --
    --==============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            intf_present_state <= intf_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- counters
        bit_rst     <= '0';
        bit_dec     <= '0';
        byte_dec    <= '0';
        byte_rst    <= '0';
        -- timer
        timer_set   <= "11"; -- no change of setting
        timer_dec   <= '0';
        -- read
        read_acq    <= '0';
        -- 1-WIRE
        forcelow    <= '0'; -- Z
        -- Control Status
        intf_busy   <= '1';
        
        case intf_present_state is
            
            --===============--
            when idle =>
            --===============--
            
                intf_busy   <= '0';
                if intf_en = '1' then
                    timer_set       <= "00"; -- set to T_RSTL
                    intf_next_state <= send_rstpulse;
                else
                    intf_next_state <= idle;
                end if;
            
            --===============--
            when send_rstpulse =>
            --===============--
            
                if timer = 0 then
                    timer_set       <= "01"; -- set to T_RSTH
                    intf_next_state <= read_prespulse;
                else
                    timer_dec       <= '1';
                    intf_next_state <= send_rstpulse;
                    if timer > T_rel then
                        forcelow <= '1';
                    end if;
                end if;
            
            --===============--
            when read_prespulse =>
            --===============--
            
                if timer = 0 then -- NO answer
                    timer_set       <= "00"; -- set to T_RSTL
                    intf_next_state <= send_rstpulse;
                else 
                    timer_dec       <= '1';
                    intf_next_state <= read_prespulse;
                    if timer < (T_RSTH - T_MSP) then
                        if DATA_reg = '0' then
                            intf_next_state <= got_answer;
                        end if;
                    end if;
                end if;
                
            --===============--
            when got_answer =>
            --===============--
            
                if timer = 0 then -- end of T_RSTH
                    if DATA_reg = '1' then 
                        timer_set       <= "10"; -- set to T_SLOT
                        intf_next_state <= send_instr;
                    else
                        intf_next_state <= send_rstpulse;
                    end if;
                else 
                    timer_dec       <= '1';
                    intf_next_state <= got_answer;
                end if;
                
            --================--
            when send_instr =>
            --================--
            
                if timer = 0 then
                    timer_set   <= "10"; -- set to T_SLOT
                    if bit_cnt = 0 then
                        bit_rst         <= '1';
                        intf_next_state <= read_ID;
                    else
                        bit_dec         <= '1';
                        intf_next_state <= send_instr;
                    end if;
                else
                    timer_dec           <= '1';
                    intf_next_state     <= send_instr;
                    if instr(TO_INTEGER(bit_cnt)) = '1' then
                        if timer > (T_SLOT - T_W1L) then -- hold 0 for T_W1L
                            forcelow <= '1';
                        end if;
                    else
                        if timer > (T_SLOT - T_W0L) then -- hold 0 for T_W0L
                            forcelow <= '1';
                        end if;
                    end if;     
                end if;
                
            --================--
            when read_ID =>
            --================--
            
                intf_next_state <= read_ID;
                if timer = 0 then
                    timer_set <= "10"; -- set to T_SLOT
                    if bit_cnt = 0 then
                        bit_rst <= '1';
                        if byte_cnt = 0 then                        
                            byte_rst        <= '1';
                            intf_next_state <= idle; -- end readout
                        else
                            byte_dec        <= '1';
                        end if;
                    else
                        bit_dec <= '1';
                    end if;
                else
                    timer_dec <= '1';
                    if timer > (T_SLOT - T_RL) then
                        forcelow <= '1';
                    elsif timer = (T_SLOT - T_MSR) then 
                        read_acq <= '1'; -- capture data
                    end if;
                end if;
            
            --===============--
            when others =>
            --===============--
            
                intf_next_state <= idle;
            
        end case;
    end process;
    
end architecture struct;
