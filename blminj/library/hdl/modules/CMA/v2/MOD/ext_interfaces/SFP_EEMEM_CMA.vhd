------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Oliver Bitterling
Create Date:    21/08/2012
Module Name:    SFP_EEMEM_CMA

-----------
Description
-----------

    Implements I2C communication with the memory block in an SFP module.

------------------
Generics/Constants
------------------

    Tclk_ns         := system clock period in ns
    Tintfclk_ns     := local clock period in ns for the I2C Interface (2x SCL)
    
    clkdom_diff     := timing difference between the system clock and the necessary interface timing
    intfclk_vsize   := size of the timer needed for the given timing difference
    
-----------
Limitations
-----------

    Tintfclk_ns     := max. frequency of 800 KHz

                       
                       

----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

    CMI
    
---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    For both read and write request, a address and instruction is necessary. They are defined as the following:

    slave_addr (8 bit)
    ------------
    
    [8:1]   = points to either the SFP memory block at A0h or A2h
    [0]     = WRn

    instr (8 bit)
    ------------

    is the 8-bit memory address


    The Implementation consist mainly of the following controls:

    1. CMI Control

    - manages the internal CMI interfaces
    - is triggered by a vld on the req-CMI
    - after this request, present all locally stored readout values (RDAC1_stored, RDAC3_stored) one after the other on the rdout-CMI
    - can trigger over the wr_req-CMI a write access with priority over the acquisition read 


    4. INTF Control

    - implements one read/write access to the connected chip
    - is triggered by intf_en

    -----

    Local Clock Generator

    The fast clock (clk, period: Tclk_ns) is used in all clocked processes, but is only enabled every clkdom_diff cycles, 
    to simulate a rising edge (enable_clk_re) or a falling edge (enable_clk_fe) of a local clock with a period of Tintfclk_ns.

    
------------------------
Not implemented Features
------------------------
    
    - disconnected SFP resolution
    - "Strange Conditions" states / ERROR HANDLING

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    

entity SFP_EEMEM_CMA is

generic
(
    Tclk_ns:        integer := 8; 
    Tintfclk_ns:    integer := 4000 -- 250 kHz (default)
);

port
(
    -- Internal Clock
    clk:             in std_logic;
    -- I2C Interface
    SDA:             inout std_logic;
    SCL:             out std_logic;
    
    -- CMI Request Input
    rd_req_data:     in  std_logic_vector(8 downto 0); --contain address of requested data
    rd_req_vld:      in  std_logic;
    rd_req_next:     out std_logic;

    -- CMI Write Input
    wr_req_data:     in  std_logic_vector(16 downto 0); -- contains data to be written
    wr_req_vld:      in  std_logic;
    wr_req_next:     out std_logic;
    
    -- CMI Output
    rdout_data:     out std_logic_vector(16 downto 0); -- contains read data
    rdout_vld:      out std_logic;
    rdout_next:     in  std_logic
    
);
    
end entity SFP_EEMEM_CMA;

architecture struct of SFP_EEMEM_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- Local <-> General Clock Timing/Difference
    constant clkdom_diff:   integer := Tintfclk_ns/Tclk_ns;
    constant intfclk_vsize: integer := get_vsize(clkdom_diff);
    
    
    --=====--
    -- FSM --
    --=====--
    
    type cmi_state_t is (idle, write_req, read_req, send_rdout);
    signal cmi_present_state, cmi_next_state: cmi_state_t := idle;
    
    type intf_state_t is (idle, sync_with_timer, start, stop, send_slave_addr_wr, send_instr, send_slave_addr_rd, send_data, read_data, ack, release_for_sec_start);
    signal intf_present_state, intf_next_state: intf_state_t := idle;
    
    --=========--
    -- COUNTER --
    --=========--
    
    -- Bit Counter
    signal bit_cnt:         UNSIGNED(2 downto 0) := "111";
    signal bit_rst:         std_logic;
    signal bit_dec:         std_logic;
    
    -- Byte Counter (for read/write access)
    signal byte_cnt:        UNSIGNED(2 downto 0) := "000";
    signal byte_rst:        std_logic;
    signal byte_inc:        std_logic;
    
     -- Local Clock
    signal intfclk_timer:  UNSIGNED(intfclk_vsize-1 downto 0) := (intfclk_vsize-1 downto 0 => '0');
    signal intfclk_rst:    std_logic;
    
    --==========--
    -- REGISTER --
    --==========--
    
    -- I2C 
    signal readout:         std_logic_vector(7 downto 0);
    Signal slave_addr_wr:   std_logic_vector(7 downto 0) := x"A0"; 
    Signal slave_addr_rd:   std_logic_vector(7 downto 0) := x"A1"; 
    Signal instr:           std_logic_vector(7 downto 0);
    Signal writein:         std_logic_vector(7 downto 0);
    
    -- Readouts
    Signal tag_reg:         std_logic_vector(8 downto 0);
    signal rdout_data_reg:  std_logic_vector(16 downto 0);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- CMI Control
    signal rdout_vld_int:   std_logic;
    signal upd_acctype:     std_logic;
    signal WRn:             std_logic;
    
    -- CMI <-> INTF 
    signal intf_en:         std_logic;
    signal intf_busy:       std_logic;
    
    -- INTF Control
    signal shift_en:        std_logic;
    signal SDA_int:         std_logic;
    signal SDA_int_reg:     std_logic := '1';
    signal SDA_sel:         UNSIGNED(2 downto 0) := "000";
    
    -- Generated Clock
    signal SCL_int:         std_logic := '1';
    signal toggle_SCL:      std_logic;
    
    -- Interface Clock Control
    signal intfclk_fe:      std_logic;
    signal intfclk_re:      std_logic;
    
begin
    
    --=======--
    -- PORTS --
    --=======--
    
    SCL <= 'Z' when SCL_int = '1' else '0'; 
    SDA <= 'Z' when SDA_int_reg = '1' else '0';
    
    
    -- 90 degree data output (with respect to SCL)
    process (clk) is
    begin
        if rising_edge(clk) then
            if intfclk_fe = '1' then
                SDA_int_reg <= SDA_int;
            end if;
        end if;
    end process;
    
    
    --======================--
    -- CREATING LOCAL CLOCK --
    --======================--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if intfclk_rst = '1' then
                intfclk_timer <= (intfclk_vsize-1 downto 0 => '0');
            else
                intfclk_timer <= intfclk_timer + 1;
            end if;
        end if;
    end process;
    
    process(all) is
    begin
        -- DEFAULTS
        intfclk_rst <= '0';
        intfclk_re  <= '0';
        intfclk_fe  <= '0';
        
        if intfclk_timer = clkdom_diff - 1 then
            intfclk_rst     <= '1';
            intfclk_re      <= '1';
        elsif intfclk_timer = clkdom_diff/2 then
            intfclk_fe      <= '1';
        end if;
        
    end process;
    
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
                
                if bit_rst = '1' then
                    bit_cnt <= "111";
                elsif bit_dec = '1' then
                    bit_cnt <= bit_cnt - 1;
                end if;
                
                if byte_rst = '1' then
                    byte_cnt <= "000";
                elsif byte_inc = '1' then
                    byte_cnt <= byte_cnt + 1;
                end if;
    
        end if;
    end process;

    
    --=============--
    -- CMI CONTROL --
    --=============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            cmi_present_state <= cmi_next_state;    
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        rd_req_next     <= '1';
        wr_req_next     <= '1';
        rdout_vld_int   <= '0';
        -- access update
        upd_acctype     <= '0';
        -- Write/Read
        WRn             <= '1';
        -- INTF ctrl
        intf_en         <= '0';
        
        
        case cmi_present_state is
            
            
            --===============--
            when idle =>
            --===============--
                
                if wr_req_vld = '1' then -- PRIORITY
                    rd_req_next     <= NOT rd_req_vld;
                    wr_req_next     <= '0';
                    WRn             <= '0';
                    upd_acctype     <= '1';
                    intf_en         <= '1';
                    cmi_next_state  <= write_req;
                
                elsif rd_req_vld = '1' then
                    rd_req_next     <= '0';
                    wr_req_next     <= NOT wr_req_vld;
                    WRn             <= '1';
                    upd_acctype     <= '1';
                    intf_en         <= '1';
                    cmi_next_state  <= read_req;
                else
                    cmi_next_state  <= idle;
                end if;
            
            --===============--
            when read_req =>
            --===============--
                
                WRn         <= '1';
                wr_req_next <= NOT wr_req_vld;
                rd_req_next <= '0';
                
                if intf_busy = '1' then
                    cmi_next_state  <= read_req;
                else
                    cmi_next_state  <= send_rdout;
                end if;
                
            --===============--
            when write_req =>
            --===============--
                
                WRn             <= '0';
                rd_req_next     <= NOT rd_req_vld;
                
                if intf_busy = '1' then
                    wr_req_next     <= '0';
                    cmi_next_state  <= write_req;
                else
                    wr_req_next     <= '1';
                    cmi_next_state  <= idle;
                end if; 
            
            --===============--
            when send_rdout =>
            --===============--
                
                if rdout_next = '1' then
                    rdout_vld_int   <= '1';
                    rd_req_next     <= '1';
                    cmi_next_state  <= idle;
                else
                    rd_req_next     <= '0';
                    wr_req_next     <= NOT wr_req_vld;
                    cmi_next_state  <= send_rdout;
                end if;
                
                
            --===============--
            when others =>
            --===============--
            
                cmi_next_state  <= idle; -- to be filled with ERROR HANDLING
            
        end case;
    end process;
    
    
    
    
    
    --====================--
    -- READOUT CMI OUTREG --
    --====================--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if rdout_next = '1' then
                rdout_vld   <= rdout_vld_int;
                rdout_data  <= slave_addr_rd(1) & instr & readout;
            end if;
        
        end if;
    end process;
    
    
    --=============--
    -- ACCESS TYPE --
    --=============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            
            if upd_acctype = '1' then
                if WRn = '1' then
                    slave_addr_wr   <= "101000" & rd_req_data(8) & '0'; 
                    slave_addr_rd   <= "101000" & rd_req_data(8) & '1'; 
                    instr           <= rd_req_data(7 downto 0);
                else
                    slave_addr_wr   <= "101000" & wr_req_data(16) & '0';
                    slave_addr_rd   <= "101000" & wr_req_data(16) & '1';
                    instr           <= wr_req_data(15 downto 8);
                    writein         <= wr_req_data(7 downto 0);
                end if;
            end if;
           
        end if;
    end process;
    
    
    
    --============--
    -- R/W ACCESS --
    --============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if shift_en = '1' then
                readout <= readout(6 downto 0) & SDA;
            end if;    
        end if;
    end process;
    
    SDA_int <=  '0'                                 when SDA_sel = "000" else
                slave_addr_wr(TO_INTEGER(bit_cnt))  when SDA_sel = "001" else
                slave_addr_rd(TO_INTEGER(bit_cnt))  when SDA_sel = "010" else   
                instr(TO_INTEGER(bit_cnt))          when SDA_sel = "011" else
                writein(TO_INTEGER(bit_cnt))        when SDA_sel = "100" else
                '1'                                 when SDA_sel = "111" else
                '1';
    
    --===========--
    -- CLOCK GEN --
    --===========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            if intfclk_re = '1' then
            
                if toggle_SCL = '1' then
                    SCL_int <= NOT SCL_int;
                else
                    SCL_int <= '1';
                end if;
            
            end if;
        end if;
    end process;    
    
    --==============--
    -- INTF CONTROL --
    --==============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            intf_present_state <= intf_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        toggle_SCL      <= '1';
        -- counters
        bit_rst         <= '0';
        bit_dec         <= '0';
        byte_inc        <= '0';
        byte_rst        <= '0';
        -- store rdout
        shift_en        <= '0';
        -- I2C Control Status
        intf_busy       <= '1';
        -- I2C Mux
        SDA_sel         <= "111"; --Z
        
        case intf_present_state is
            
            --===============--
            when idle =>
            --===============--
            
                intf_busy       <= '0';
                toggle_SCL      <= '0';
                
                if intf_en = '1' then
                    intf_next_state <= sync_with_timer;
                else
                    intf_next_state <= idle;
                end if;
            
            --===============--
            when sync_with_timer =>
            --===============--
            
                toggle_SCL      <= '0';
                if intfclk_re = '1' then
                    intf_next_state <= start;
                else
                    intf_next_state <= sync_with_timer;
                end if;
            
            
            --===============--
            when start =>
            --===============--
            
                SDA_sel <= "000"; -- 1 -> 0 SDA transition while SCL is 1
                
                if intfclk_re = '1' then
                    if byte_cnt = "000" then
                        intf_next_state <= send_slave_addr_wr;
                    else
                        intf_next_state <= send_slave_addr_rd;
                    end if;
                else
                    intf_next_state <= start;
                end if;
            
            --===============--
            when send_slave_addr_wr =>
            --===============--
            
            
                intf_next_state <= send_slave_addr_wr;
                SDA_sel         <= "001";
                
                if intfclk_re = '1' then
                    if SCL_int = '1' then
                        if bit_cnt = "000" then
                            bit_rst         <= '1';
                            intf_next_state <= ack; 
                        else
                            bit_dec         <= '1';
                        end if;
                    end if;
                end if;
                
            --===============--
            when send_slave_addr_rd =>
            --===============--
            
                intf_next_state <= send_slave_addr_rd;
                SDA_sel         <= "010";
                
                if intfclk_re = '1' then
                    if SCL_int = '1' then
                        if bit_cnt = "000" then
                            bit_rst         <= '1';
                            intf_next_state <= ack; 
                        else
                            bit_dec         <= '1';
                        end if;
                    end if;
                end if;
            
            --===============--
            when send_instr =>
            --===============--
            
                intf_next_state <= send_instr;
                SDA_sel         <= "011";
                
                if intfclk_re = '1' then
                    if SCL_int = '1' then
                        if bit_cnt = "000" then
                            bit_rst         <= '1';
                            intf_next_state <= ack; 
                        else
                            bit_dec         <= '1';
                        end if;
                    end if;
                end if;
            
            --===============--
            when send_data =>
            --===============--
            
                intf_next_state <= send_data;
                SDA_sel         <= "100";
                
                if intfclk_re = '1' then
                    if SCL_int = '1' then
                        if bit_cnt = "000" then
                            bit_rst         <= '1';
                            intf_next_state <= ack; 
                        else
                            bit_dec         <= '1';
                        end if;
                    end if;
                end if;
            
            --===============--
            when read_data =>
            --===============--
            
                intf_next_state <= read_data;
                SDA_sel         <= "111";
                
                if intfclk_re = '1' then
                    if SCL_int = '1' then
                        if bit_cnt = "000" then
                            bit_rst         <= '1';
                            intf_next_state <= ack; 
                        else
                            bit_dec         <= '1';
                        end if;
                    else
                        shift_en <= '1'; -- take it with the next rising edge
                    end if;
                end if;
                    
            --===============--
            when ack =>
            --===============--
            
                intf_next_state <= ack;
                SDA_sel         <= "111"; -- Z
                
                if intfclk_re = '1' then
                    if SCL_int = '1' then -- SCL high
                        if byte_cnt = "011" then -- read byte done
                            byte_rst        <= '1';
                            intf_next_state <= stop;
                        else -- await ack
                            if SDA = '0' then
                                byte_inc <= '1'; -- next byte
                                
                                if byte_cnt = "000" then
                                    intf_next_state     <= send_instr;
                                elsif byte_cnt = "001" AND WRn = '0' then
                                    intf_next_state     <= send_data;
                                elsif byte_cnt = "001" AND WRn = '1' then
                                    intf_next_state     <= release_for_sec_start;
                                elsif byte_cnt = "010" AND WRn = '0' then
                                    byte_rst            <= '1';
                                    intf_next_state     <= stop;
                                elsif byte_cnt = "010" AND WRn = '1' then 
                                    intf_next_state     <= read_data;
                                end if;
                            end if;
                        end if;
                    end if;
                end if;
            
            --===============--
            when stop =>
            --===============--
                SDA_sel         <= "000";
                intf_next_state <= stop;
                
                if intfclk_re = '1' then
                    if SCL_int = '1' then
                        toggle_SCL         <= '0';
                        intf_next_state <= idle; -- 0 -> 1 SDA transition while SCL is 1
                    end if;
                end if;
            
            --===============--
            when release_for_sec_start =>
            --===============--
            
                SDA_sel         <= "111";
                intf_next_state  <= release_for_sec_start;
                
                if intfclk_re = '1' then
                    if SCL_int = '1' then
                        toggle_SCL         <= '0';
                        intf_next_state <= start;
                    end if;
                end if;
            
            --===============--
            when others =>
            --===============--
            
                intf_next_state <= idle; -- to be filled with ERROR HANDLING
                
        end case;
    end process;


    
end architecture struct;
