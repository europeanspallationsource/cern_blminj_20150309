------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Update Date:    25/05/2012
Module Name:    AD7927_CMA 

-----------
Description
-----------

    CMI Module to access the Analog Devices AD7927 via SPI

------------------
Generics/Constants
------------------

    Tclk_ns         := system clock period in ns, no restrictions
    Tintfclk_ns     := local clock period in ns for the SPI Interface
    
    clkdom_diff     := timing difference between the system clock and the necessary interface timing
    intfclk_vsize   := size of the timer needed for the given timing difference
    T_quiet         := idle time between readouts (50 us)
    quiet_vsize     := vectorsize for the quiet timer

-----------
Limitations
-----------

    Tintfclk_ns     := needs to be between 10 kHz and 20 MHz

----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
 
-----------------
Necessary Modules
-----------------

    CMI

---------------
Necessary Cores
---------------

    none  
    
    
--------------
Implementation
--------------

    The ADC is read out without using the shadow register or sequences (SHADOW = 0, SEQ = 0) and constantly powered (PM = 11).

    The rd_req CMI transport only the address ADD(2:0)! 

    The Implementation consist mainly of the following 4 controls:

    1. CMI Control

    - manages the internal CMI interfaces
    - is triggered by a vld on the req-CMI
    - after this request, present the requested values on the rdout-CMI

    3. INTF Control

    - implements one read/write access to the connected chip
    - is triggered by intf_en

    -----

    Local Clock Generator

    The fast clock (clk, period: Tclk_ns) is used in all clocked processes, but is only enabled (enable_clk) every clkdom_diff cycles,
    so that it simulates a local clock with a period of Tintfclk_ns.


------------------------
Not implemented Features
------------------------

    - add other modes (shadow register, shutdown)
    - change mode via CMI Cmd
    - "Strange Conditions" states / ERROR HANDLING

*/
------------------------------


library IEEE;
   use IEEE.std_logic_1164.all;
   use IEEE.numeric_std.all;
   use IEEE.math_real.all;
   use work.vhdl_func_pkg.all;
      

entity AD7927_CMA is

generic
(
    Tclk_ns:        integer := 8; 
    Tintfclk_ns:    integer := 2000 -- 500 kHz (default)
);

port
(
    -- Internal Clock
    clk:            in  std_logic;
    -- SPI Connection
    CSn:            out std_logic;
    SCLK:           out std_logic;
    DOUT:           in  std_logic;
    DIN:            out std_logic;
    -- CMI Request Input
    rd_req_data:    in  std_logic_vector(2 downto 0);
    rd_req_vld:     in  std_logic;
    rd_req_next:    out std_logic;
    -- CMI Output
    rdout_data:     out std_logic_vector(14 downto 0); -- tag: 3 bits, data: 12 bits
    rdout_vld:      out std_logic;
    rdout_next:     in  std_logic
);
    
end entity AD7927_CMA;



architecture struct of AD7927_CMA is

    --===========--
    -- CONSTANTS --
    --===========--
    
    -- Local <-> General Clock Timing/Difference
    constant clkdom_diff:   integer := Tintfclk_ns/Tclk_ns;
    constant intfclk_vsize: integer := get_vsize(clkdom_diff);
    
    -- Quiet Timer
    constant T_quiet:       integer := integer(CEIL(real(50000)/real(Tclk_ns)));  -- quiet time of 50 us
    constant quiet_vsize:   integer := get_vsize(T_quiet);
    
    --=====--
    -- FSM --
    --=====--
    type cmi_state_t is (idle, read_req, send_rdout, quiet_phase);
    signal cmi_present_state, cmi_next_state: cmi_state_t := idle;
    
    type intf_state_t is (idle, sync_with_timer, acc_spi);
    signal intf_present_state, intf_next_state: intf_state_t := idle;
    
    --=========--
    -- COUNTER --
    --=========--
    
    -- Bit Position Pointer
    signal bit_cnt:         UNSIGNED(3 downto 0) := "1111";
    signal bit_rst:         std_logic;
    signal bit_dec:         std_logic;
    
    -- Quiet Timer
    signal quiet_timer:     UNSIGNED(quiet_vsize-1 downto 0) := (quiet_vsize-1 downto 0 => '0');
    signal quiet_rst:       std_logic;
    signal quiet_inc:       std_logic;
    
    -- Local Clock
    signal intfclk_timer:   UNSIGNED(intfclk_vsize-1 downto 0) := (intfclk_vsize-1 downto 0 => '0');
    signal intfclk_rst:     std_logic;
    
    --==========--
    -- REGISTER --
    --==========--

    -- SPI Read Register
    signal readout:         UNSIGNED(14 downto 0);
    -- SPI Write Register
    signal write_cmd:       std_logic_vector(15 downto 0);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- CMI
    signal rdout_vld_int:   std_logic;
    
    -- Access
    signal upd_wrcmd:       std_logic;
    
    -- CMI <-> INTF 
    signal intf_en:         std_logic;
    signal intf_busy:       std_logic;
    
    -- INTF WR/RD access
    signal read_acc:        std_logic;
    signal write_acc:       std_logic;
    
    -- Interface Clock Control
    signal intfclk_re:      std_logic;
    
BEGIN

    --=======--
    -- PORTS --
    --=======--

    SCLK <= '1' when intfclk_timer < clkdom_diff/2 else
            '0';

    --======================--
    -- CREATING LOCAL CLOCK --
    --======================--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if intfclk_rst = '1' then
                intfclk_timer <= (intfclk_vsize-1 downto 0 => '0');
            else
                intfclk_timer <= intfclk_timer + 1;
            end if;
        end if;
    end process;
    
    process(all) is
    begin
        -- DEFAULTS
        intfclk_rst <= '0';
        intfclk_re  <= '0';
        
        if intfclk_timer = clkdom_diff - 1 then
            intfclk_rst     <= '1';
            intfclk_re      <= '1';
        end if;
        
    end process;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
                if bit_rst = '1' then
                    bit_cnt <= "1111";
                elsif bit_dec = '1' then
                    bit_cnt <= bit_cnt - 1;
                end if;
                
                if quiet_rst = '1' then
                    quiet_timer <= (quiet_vsize-1 downto 0 => '0');
                elsif quiet_inc = '1' then
                    quiet_timer <= quiet_timer + 1;
                end if;
                
        end if;
    end process;
    
    
    --=============--
    -- CMI CONTROL --
    --=============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            cmi_present_state <= cmi_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- Counter
        quiet_inc       <= '0';
        quiet_rst       <= '0';
        -- CMI
        rdout_vld_int   <= '0';
        rd_req_next     <= '1';
        -- Instruction update
        upd_wrcmd       <= '0';
        -- INTF ctrl
        intf_en         <= '0';
        
        
        case cmi_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if rd_req_vld = '1' then
                    rd_req_next     <= '0';
                    upd_wrcmd       <= '1';
                    intf_en         <= '1';
                    cmi_next_state  <= read_req;
                else
                    cmi_next_state  <= idle;
                end if;
            
            --===============--
            when read_req =>
            --===============--
                
                rd_req_next <= '0';
                if intf_busy = '1' then
                    cmi_next_state  <= read_req;
                else
                    cmi_next_state  <= send_rdout;
                end if;
                
            --===============--
            when send_rdout =>
            --===============--
                
                rd_req_next     <= '0';
                if rdout_next = '1' then
                    rdout_vld_int   <= '1';
                    cmi_next_state  <= quiet_phase;
                else
                    cmi_next_state  <= send_rdout;
                end if;
            
            --===============--
            when quiet_phase =>
            --===============--
            
                
                if quiet_timer = T_quiet then
                    quiet_rst       <= '1';
                    rd_req_next     <= '1';
                    cmi_next_state  <= idle;
                else
                    quiet_inc       <= '1';
                    rd_req_next     <= '0';
                    cmi_next_state  <= quiet_phase;
                end if; 
            
            --===============--
            when others =>
            --===============--
          
                cmi_next_state <= idle;
                
           
        end case;
    end process;

    
    --============--
    -- CMI OUTREG --
    --============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if rdout_next = '1' then
                rdout_vld   <= rdout_vld_int;
                rdout_data  <= STD_LOGIC_VECTOR(readout);
            end if;
            
        end if;
    end process;

    --===========--
    -- WRITE CMD --
    --===========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if upd_wrcmd = '1' then
                write_cmd <= "100" & rd_req_data & "110011" & "0000"; -- settings see AD7927 documentation
            end if;
            
        end if;
    end process;    

    
    --============--
    -- R/W ACCESS --
    --============--
    
    -- Write Access --
    DIN <= write_cmd(TO_INTEGER(bit_cnt)) when write_acc = '1' else '1';

    -- Read Access (shift into rdout register) --   
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if read_acc = '1' then
                readout <= readout(13 downto 0) & DOUT;
            end if;
            
        end if;
    end process;
    
    --==============--
    -- INTF CONTROL --
    --==============--
    
    process(clk)
    begin
        if rising_edge(clk) then
            intf_present_state <= intf_next_state;
        end if;
    end process;
    
    process (all)
    begin
        -- DEFAULTS

        -- Bit Pointer
        bit_rst         <= '0';
        bit_dec         <= '0';
        -- R/W 
        read_acc        <= '0';
        write_acc       <= '0';
        -- chip select
        CSn             <= '1';
        -- Busy
        intf_busy       <= '1';
        
        
        case intf_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                intf_busy       <= '0';
                if intf_en = '1' then
                    intf_next_state <= sync_with_timer;
                else
                    intf_next_state <= idle;
                end if;     
            
            --===============--
            when sync_with_timer =>
            --===============--
            
                if intfclk_re = '1' then
                    intf_next_state <= acc_spi;
                else
                    intf_next_state <= sync_with_timer;
                end if;
            
            
            --===============--
            when acc_spi =>
            --===============--
            
                CSn         <= '0';
                write_acc   <= '1';
                if intfclk_re = '1' then -- on rising edge
                    if bit_cnt = 0 then
                        bit_rst         <= '1';
                        read_acc        <= '0';
                        intf_next_state <= idle;
                    else
                        read_acc        <= '1';
                        bit_dec         <= '1';
                        intf_next_state <= acc_spi;
                    end if;
                else
                    intf_next_state <= acc_spi;
                end if;
                
            --===============--
            when others =>
            --===============--
            
                intf_next_state <= idle; -- to be filled with ERROR HANDLING
                
        end case;
    end process;
    
    
        
end architecture struct;
