------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Oliver Bitterling
Create Date:    4/09/2012
Module Name:    CY7C1460_CMA

-----------
Description
-----------

    CMI Interfac for the CY7C1460 SRAM

--------
Generics
--------

    ram_data_size       := size of the RAM data bus.
    ram_addr_size       := size of the RAM addr bus.
    cmi_data_size       := size of the CMI data bus. Must be smaller or equal to ram_data_size.
    cmi_addr_size       := size of the CMI addr bus. Must be smaller or equal to ram_addr_size.

-----------
Limitations
-----------

    Maximum clock frequency of 250 MHz.

    cmi_data_size must be smaller or equal to ram_data_size.
    cmi_addr_size must be smaller or equal to ram_addr_size.

----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

    altera_mf.scfifo
    
--------------
Implementation
--------------
    
    The interface can handle one read or write request per clock cycle. 
    The interface needs 4 clock cycles to complete a request . 
    In the first cycle the received address is written into the RAM and both address and data 
    is written into a 3 register FIFO where data propagates one register per clock cycle.
    During the third clock cycle data is either send from the register to the RAM or received 
    and written into an output FIFO. The output FIFO is necessary to store the results of already
    accepted read requests when rdout_next is zero. The last clock cycle is used by the output FIFO.
    
    All data to the RAM is written on the falling edge and data from the RAM is read on the rising edge.
    
    

    
    

*/

library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.numeric_std.all;
	use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
LIBRARY altera_mf;
    USE altera_mf.altera_mf_components.all;
	

entity CY7C1460_CMA is

Generic(
    ram_data_size:      Integer := 36;
    ram_addr_size:      Integer := 20;
    cmi_data_size:      Integer := 16;
    cmi_addr_size:      Integer := 16
    
    );
port(
	-- Clock
	signal clk:			in std_logic;
	
	-- RAM Interface
	signal data:        inout std_logic_vector(ram_data_size-1 downto 0)    := (others => '0');
	signal addr:		out std_logic_vector(ram_addr_size-1 downto 0)      := (others => '0');
    signal ce:          out std_logic_vector(2 downto 0);               --Chip select
    signal we:          out std_logic;                                  --Write enable
    signal adv_ld:      out std_logic;                                  --Advance addr counter
    signal cen:         out std_logic;                                  --Clock enable
    signal oe:          out std_logic;                                  --Direction of data
    signal bw:          out std_logic_vector(3 downto 0);
    
    
	
	-- CMI Request Input
	signal req_data:	in std_logic_vector(cmi_addr_size-1 downto 0); --contain address of requested data
	signal req_vld:		in std_logic;
	signal req_next:	out std_logic := '1';

	-- CMI Write Input
	signal wr_data:		in std_logic_vector(cmi_addr_size + cmi_data_size-1 downto 0); -- contains data to be written
	signal wr_vld:		in std_logic;
	signal wr_next:		out std_logic := '1';
	
	-- CMI Output
	signal rdout_data:	out std_logic_vector(cmi_addr_size + cmi_data_size-1 downto 0); -- contains read data
	signal rdout_vld:	out std_logic;
	signal rdout_next:	in std_logic
	
);
	
end entity CY7C1460_CMA;

architecture struct of CY7C1460_CMA is
	
    ---------------
    -- Constants --
    ---------------
    
    Constant fifo_depth:    natural := 3;
    Constant lpm_widthu:	natural := natural(ceil(LOG2 (real(fifo_depth))));
    
    ---------------
    -- Registers --
    ---------------
    
    signal conveyor:                slv_array(2 downto 0):= (others => (cmi_data_size + cmi_addr_size => '1', others => '0'));
    signal FIFO_data_int:           std_logic_vector(cmi_data_size + cmi_addr_size-1 downto 0) := (others => '0');
    signal FIFO_vld_int:            std_logic := '0';
    signal addr_int:                std_logic_vector(ram_addr_size-1 downto 0)      := (others => '0');
    signal we_int:                  std_logic := '1';
    signal data_int:                std_logic_vector(ram_data_size-1 downto 0)    := (others => '0');
    
    
    
    signal vld:                     std_logic_vector(1 downto 0);
    signal req_next_int:            std_logic;
    signal RdReq:                   std_logic;
    signal empty:                   std_logic;
    
    
    
Begin
    
    wr_next <= '1'; --Write priority
    req_next_int <= (Not req_vld) OR ((not wr_vld) And rdout_next);
    req_next <= req_next_int;
    
    -- Filling FIFO with data from both CMI interfaces
    vld <= wr_vld & (req_vld and req_next_int);
    WITH vld SELECT 
        conveyor(0) <= '1' & '0' & wr_data when "10",
        '1' & '0' & wr_data when "11",
        '1' & '1' & req_data & (cmi_data_size-1 downto 0 => '0') when "01",
        (cmi_data_size + cmi_addr_size => '1', others => '0') when "00",
        (cmi_data_size + cmi_addr_size => '1', others => '0') when others;
    
    
    addr <= addr_int;
    we <= we_int;
    
    -- permament configuration signals to RAM
    bw <= "0000"; -- Braucht vielleicht eine bessere Lösung
    adv_ld <= '0'; -- no burst necessary
    ce(2 downto 1) <= "01"; 
    cen <= '0'; -- clock alwasy activ
    
    
    
    
    
    Process(clk)
    Begin
        if rising_edge(clk) then
            
            --Basic FIFO
            conveyor(2) <= conveyor(1);
            conveyor(1) <= conveyor(0);
            
            
            If not conveyor(1)(cmi_data_size + cmi_addr_size) then           
                data_int <= (ram_data_size-cmi_data_size-1 downto 0 => '0') & conveyor(1)(cmi_data_size-1 downto 0);
            else
                data_int <= (others => 'Z'); 
            end if;

        end if;
    End process;

    
    Process(clk) 
    Begin   
        if falling_edge(clk) then

            --Data to RAM is written on the falling edge to secure hold times. 
            data <= data_int;
            addr_int <= (ram_addr_size-cmi_addr_size-1 downto 0 => '0') & conveyor(0)(cmi_data_size + cmi_addr_size-1 downto cmi_data_size);
            we_int <= conveyor(0)(cmi_data_size + cmi_addr_size); 
            
            -- Deselect chip while there are no requests. Might be not necessary.
            ce(0) <=  not conveyor(0)(cmi_data_size + cmi_addr_size +1);
            
            -- Deactivate RAM output pins to prevent overlapping on data line while switching from writing to reading. Might be not necessary.
            if not conveyor(2)(cmi_data_size + cmi_addr_size) and conveyor(1)(cmi_data_size + cmi_addr_size) then
                oe <= '1';
            else
                oe <= '0';
            end if;
           
        end if;
    end process;
            
    
    -- Transfere read data to output FIFO
    FIFO_vld_int <= conveyor(2)(cmi_data_size + cmi_addr_size) And conveyor(2)(cmi_data_size + cmi_addr_size+1); -- Is package valid and result of a read request?
    WITH conveyor(2)(cmi_data_size + cmi_addr_size) SELECT
        FIFO_data_int <=    conveyor(2)(cmi_data_size + cmi_addr_size-1 downto cmi_data_size) & data(cmi_data_size-1 downto 0) when '1',
                            (others => '1') when others;
    
    
    -- CMI FIFO
    RdReq       <= rdout_next AND NOT(Empty);
    Process(clk)
	Begin
		if rising_edge(clk) then
			if rdout_next = '1' then
				rdout_vld <= NOT(Empty);
			end if;
		end if;
	End process;
    
    -- Output FIFO to prevent data loss due to data congestion
    Fifo	:	Entity altera_mf.scfifo
		generic map(
		add_ram_output_register		=>"OFF",
		allow_rwcycle_when_full		=>"OFF",
		almost_empty_value			=>0,
		almost_full_value			=>0,
		intended_device_family		=>"unused",
		lpm_numwords				=>fifo_depth,
		lpm_showahead				=>"OFF",
		lpm_width					=>cmi_data_size + cmi_addr_size,
		lpm_widthu					=>lpm_widthu
		)
		port map(
		aclr			=>'0',
		almost_empty	=>open,
		almost_full		=>open,
		clock			=>clk,
		data			=>FIFO_data_int,
		empty			=>empty,
		full			=>open,
		q 				=>rdout_data,
		rdreq			=>RdReq,
		sclr			=>'0',
		usedw			=>open,
		wrreq			=>FIFO_vld_int
		);
        
end architecture struct;
