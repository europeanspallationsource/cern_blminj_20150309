------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Update Date:    25/05/2012
Module Name:    AD7626_CMA

-----------
Description
-----------

Accessing the ADC AD7626 via Echo Mode

--------
Generics
--------

    Tclk_ns         := CC length

    timer_vsize     := vectorsize for the local timer
    T_xx            := timings
    echo_vsize      := vectorsize for the echo timer
    T_echo_delay    := delay between DCO and CLK

 
-----------
Limitations
-----------

    Tclk_ns     := Maximum Clock Frequency Tclk_ns = 4ns = 250 MHz (due to the minimum cycle time of 100ns)
                   Minimum Clock Frequency Tclk_ns = 40ns = 25 MHz (due to the T_cnvh)

----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
 
-----------------
Necessary Modules
-----------------

    CMI

---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    Every request to the ADC is done during 1 cycle. The duration of this cycle has to be set between 100ns and 10000ns. (Default: 25 * Tclk_ns)

    Every cycle T_cyc starts with a CNV high for T_cnvh (@16ns), afterwards it waits for T_start_clk(min @96ns) to start CLK.

    Shortly afterwards the ADC sends the 16 Bits of data over a period of 16*Tclk_ns, 
    which are being stored in a shift register clocked by the echoed clock DCO.

    The readout in the shift register is stored 20ns after the last clock edge has been sent (20ns + T_start_clk + 16*Tclk_ns MOD T_cyc).

    With the run signal the ADC can be turned on and off. The run signal is evaluated at every start of cycle (timer == 0).

------------------------
Not implemented Features
------------------------

    - Self Clocked Mode
    - "Strange Conditions" states / ERROR HANDLING

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity AD7626_CMA is

generic(
    Tclk_ns: integer := 4 -- 250 MHz (default)
);


port(
    -- Clock
    signal intclk:          in std_logic;
    -- ADC I/O
    signal CLK:             out std_logic;
    signal CNV:             out std_logic;
    signal DCO:             in std_logic;
    signal D:               in std_logic;
    -- CMI TX
    signal adc_next:        in std_logic;
    signal adc_vld:         out std_logic;
    signal adc_data:        out std_logic_vector(15 downto 0);
    
    -- Trigger
    signal start_of_cnv:    in std_logic
    
);

end entity AD7626_CMA;


architecture struct of AD7626_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- vectorsize of the timer
    constant timer_vsize:   integer := get_vsize(get_maxcnt(Tclk_ns, 10000)); -- max cycle time is 10000 ns
    
    -- Timings for different actions
    constant T_cyc:         integer := 25; -- 250 MHz(4ns) -> 100ns -> 25 counts;   
    constant T_cnvh:        integer := integer(CEIL(real(16)/real(Tclk_ns)));   -- fixed @~16ns
    constant T_start_clk:   integer := integer(CEIL(real(96)/real(Tclk_ns))); -- fixed @~96ns;
    
    -- Delay between CLK and DCO
    constant T_echo_delay:  integer := integer(CEIL(real(20)/real(Tclk_ns))); -- fixed @~20ns;
    constant echo_vsize:    integer := get_vsize(T_echo_delay);
    
    --=====--
    -- FSM --
    --=====--
    
    type intf_state_t is (idle, wait_for_clkstart, send_clkpulse, store_data);
    signal intf_present_state, intf_next_state: intf_state_t := idle;
    
    type tim_state_t is (idle, busy);
    signal tim_present_state, tim_next_state: tim_state_t := idle;
    
    --=========--
    -- COUNTER --
    --=========--
    
    -- Byte Counter (for read/write access)
    signal hword_cnt:   UNSIGNED(3 downto 0) := "1111";
    signal hword_rst:   std_logic;
    signal hword_dec:   std_logic;
    
    -- Echo Delay Counter
    signal echo_cnt:        UNSIGNED(echo_vsize-1 downto 0) := (echo_vsize-1 downto 0 => '0');
    signal echo_rst:        std_logic;
    signal echo_inc:        std_logic;
    
    -- Timer
    signal timer:       UNSIGNED(timer_vsize-1 downto 0) := (timer_vsize-1 downto 0 => '0');
    
    
    
    --==========--
    -- REGISTER --
    --==========--
    
    signal shift_rdout:         std_logic_vector(15 downto 0);
    signal shift_rdout_sync:    std_logic_vector(15 downto 0);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- Store readout
    signal store_acq:       std_logic;
    -- CMI
    signal new_data:        std_logic;
    -- Clock Signals
    signal CLK_oe:          std_logic;
    signal CLK_oe_180:      std_logic;
    -- Timing Controls
    signal run_acq:         std_logic;
    signal timer_en:        std_logic;
    
BEGIN

    --=======--
    -- PORTS --
    --=======--
    
    -- CLK creation with Gating
    CLK <= intclk when CLK_oe_180 = '1' else '0';
    
--  -- DDR CLK creation from clk
--  clkout: entity work.ddrclk
--  PORT MAP
--  (
--      datain_h            => vectorize(CLK_oe_180),
--      datain_l            => "0",
--      outclock            => intclk,
--      scalarize(dataout)  => CLK
--  );

    -- CLK_oe HACK
    process(intclk) is
    begin
        if falling_edge(intclk) then
            CLK_oe_180 <= CLK_oe;
        end if;
    end process;
    
    
    -- CNV creation @ start of cycle 
    process (all) is
    begin
        CNV <= '0';
        if run_acq = '1' then
            if timer < T_cnvh then
                CNV <= '1';
            end if;
        end if;
    end process;
    
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(intclk) is
    begin
        if rising_edge(intclk) then
        
            if hword_rst= '1' then
                hword_cnt <= "1111";
            elsif hword_dec = '1' then
                hword_cnt <= hword_cnt - 1;
            end if;
            
            if echo_rst = '1' then
                echo_cnt <= (echo_vsize-1 downto 0 => '0');
            elsif echo_inc = '1' then
                echo_cnt <= echo_cnt + 1;
            end if;
            
        end if;
    end process;
    
    
    -- Enabled Timer [0:T_cyc)
    process(intclk) is
    begin
        if rising_edge(intclk) then
        
            if timer_en = '1' then
                if timer = (T_cyc - 1) then
                    timer <= (timer_vsize-1 downto 0 => '0');
                else
                    timer <= timer + 1;
                end if;
            end if;
            
        end if;
    end process;

    --===============--
    -- TIMER CONTROL --
    --===============--
    
    process (intclk) is
    begin
        if rising_edge(intclk) then
            tim_present_state <= tim_next_state;
        end if;
    end process;
    
    
    process (all) is
    begin
        
        case tim_present_state is
            
            --===============--
            when idle =>
            --===============--
            
                run_acq     <= '0';
                timer_en    <= '0';
                
                if start_of_cnv = '1' then
                    tim_next_state <= busy;
                else
                    tim_next_state <= idle;
                end if;
            
            --===============--
            when busy =>
            --===============--
            
                run_acq     <= '1';
                timer_en    <= '1';
                
                if timer = (T_cyc - 1) then
                    if start_of_cnv = '1' then
                        tim_next_state <= busy;
                    else
                        tim_next_state <= idle;
                    end if;
                else
                    tim_next_state <= busy;
                end if;
            
             --===============--
            when others =>
            --===============--
            
                tim_next_state <= idle;
            
        end case;
    end process;
    
    
    --===============--
    -- TIMED RUN CMD --
    --===============--
    
--  process(intclk) is
--  begin
--      if rising_edge(intclk) then
--          if timer = (T_cyc - 1) then -- @timer = 0
--              run_acq <= run;
--          end if;
--      end if;
--  end process;
    
    --============================--
    -- CMI Protocol (with Outreg) --
    --============================--
    
    process(intclk) is
    begin
        if rising_edge(intclk) then
            
            if (adc_next OR NOT new_data) then
                new_data <= store_acq;
            end if;
            -- Output Register
            if adc_next = '1' then
                adc_data    <= shift_rdout_sync;
                adc_vld     <= new_data;
            end if;
        end if;
    end process;

    --============--
    -- ADC ACCESS --
    --============--
    
    -- Shift Register clocked by ADC Clock Reflection (DCO)
    process(DCO) is
    begin
        if rising_edge(DCO) then
            shift_rdout <= shift_rdout(14 downto 0) & D;
        end if;
    end process;
    
    -- Synchronize to local clock
    process(intclk) is
    begin
        if rising_edge(intclk) then
            if store_acq = '1' then
                shift_rdout_sync <= shift_rdout;
            end if;
        end if;
    end process;

    --==============--
    -- INTF CONTROL --
    --==============--
    
    process (intclk) is
    begin
        if rising_edge(intclk) then
            intf_present_state <= intf_next_state;
        end if;
    end process;
    
    
    process (all) is
    begin
        -- DEFAULTS
        -- Byte Counter
        hword_rst   <= '0';
        hword_dec   <= '0';
        -- Echo Counter
        echo_rst    <= '0';
        echo_inc    <= '0';
        -- Store
        store_acq   <= '0';
        -- CLK enable
        CLK_oe      <= '0';
        
        case intf_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if run_acq = '1' then
                    intf_next_state <= wait_for_clkstart;
                else
                    intf_next_state <= idle;
                end if;
            
            --===============--
            when wait_for_clkstart =>
            --===============--
        
                intf_next_state <= wait_for_clkstart;
                
                if timer = T_start_clk - 1  then
                    CLK_oe          <= '1'; -- async enable
                    intf_next_state <= send_clkpulse;
                end if;
                
                
            --===============--
            when send_clkpulse =>
            --===============--
                
                if hword_cnt = 0 then
                    hword_rst       <= '1';
                    intf_next_state <= store_data;
                else
                    CLK_oe          <= '1';
                    hword_dec       <= '1';
                    intf_next_state <= send_clkpulse;
                end if;
            
            --===============--
            when store_data =>
            --===============--
                
                if echo_cnt = T_echo_delay - 1 then
                    echo_rst        <= '1';
                    store_acq       <= '1';
                    intf_next_state <= idle;
                else
                    echo_inc        <= '1';
                    intf_next_state <= store_data;
                end if;
            
            --===============--
            when others =>
            --===============--
            
                intf_next_state <= idle;
            
        end case;
    end process;
    
end architecture struct;
