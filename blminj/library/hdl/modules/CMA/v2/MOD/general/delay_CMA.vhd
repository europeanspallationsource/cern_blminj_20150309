------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/01/2012
Module Name:    delay_CMA

-----------------
Short Description
-----------------

    Implements a pipeline of length "delay_len"

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    data_size           := size of input/output data    
    delay_len           := length of the pipeline    

--------------
Implementation
--------------
    
    implements small shift register, that shifts with every next signal.
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity delay_CMA is

generic
(
    data_size:      integer;
    delay_len:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity delay_CMA;


architecture struct of delay_CMA is
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal data_array:  slv_array(delay_len-1 downto 0)(data_size-1 downto 0)   := (others => (others => '0'));
    signal vld_array:   std_logic_vector(delay_len-1 downto 0)                  := (others => '0');
    
BEGIN
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    inp_next    <= out_next OR NOT inp_vld;
    out_data    <= data_array(delay_len-1);
    out_vld     <= vld_array(delay_len-1);
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                data_array(delay_len-1 downto 1)    <= data_array(delay_len-2 downto 0);
                data_array(0)                       <= inp_data;
                vld_array(delay_len-1 downto 1)     <= vld_array(delay_len-2 downto 0);
                vld_array(0)                        <= inp_vld;
            end if;
                    
        end if;
    end process;
    
    
end architecture struct;

