------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/11/2013
Module Name:    const_CMA

-----------------
Short Description
-----------------

    Shows a constant value at the output. out_vld is always 1.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    data_size           := size of the input and of the stored threshold

--------------
Implementation
--------------
    


-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity const_CMA is

generic
(
    data_size:      integer
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Threshold
    PARAM_const:        in  std_logic_vector(data_size-1 downto 0);
    -- CMI Output
    out_data:           out std_logic_vector(data_size-1 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic
);

end entity const_CMA;


architecture struct of const_CMA is
    
    
BEGIN

    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    out_vld     <= '1';
    out_data    <= PARAM_const;
    
    
    
end architecture struct;

