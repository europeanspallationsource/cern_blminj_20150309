------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/11/2013
Module Name:    sample_CMA

-----------------
Short Description
-----------------

    amples the input on a given trigger, holds it there for the sample period and presents it once to the CMI output.

------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    data_size           := size of the input/output
    

--------------
Implementation
--------------
    
 

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

*/
------------------------------


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
   
entity sample_CMA is

generic
(
    data_size:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Standard Input
    inp:            in  std_logic_vector(data_size-1 downto 0);
    -- CMI Output
    out_data:       out std_logic_vector(data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic;
    -- Commands
    trigger:        in  std_logic
);

end entity sample_CMA;


architecture struct of sample_CMA is
    
    --======--
    -- FSMs --
    --======--
    
    type state_t is (idle, triggered);
    signal present_state, next_state: state_t := idle;
    
    --=================--
    -- GENERAL SIGNALs --
    --=================--

    signal out_vld_int:     std_logic;
    
BEGIN
    
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if out_next = '1' then
                out_data    <= inp;
                out_vld     <= out_vld_int;
            end if;
        end if;
    end process;
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process(all)
    begin
        -- DEFAULT
        out_vld_int <= '0';
        
        case present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if trigger = '1' then
                    if out_next = '1' then
                        out_vld_int <= '1';
                        next_state <= idle;
                    else
                        next_state <= triggered;
                    end if;
                else
                    next_state <= idle;
                end if;
                
            --===============--
            when triggered =>
            --===============--
                
                if out_next = '1' then
                    out_vld_int <= '1';
                    next_state <= idle;
                else
                    next_state <= triggered;
                end if;
            
         
            --===============--
            when others =>
            --===============--
                
                next_state <= idle;
                
        end case;   
            
    end process;
    
end architecture struct;
