------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    concat_CMA

-----------------
Short Description
-----------------

    This module concatinates the data points of multiple CMA inputs to create one CMA output with all data.
    THe output is updated everytime one of the inputs gets an update. For the other parts the older value is forwarded.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of each input data
    inp_number          := number of inputs

    
--------------
Implementation
--------------
    

    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------

    different data sizes for the inputs

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use IEEE.std_logic_misc.all;
    use work.vhdl_func_pkg.all;
   
entity concat_CMA is

generic
(
    inp_number:     integer;
    inp_data_size:  integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  slv_array(inp_number-1 downto 0)(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic_vector(inp_number-1 downto 0);
    inp_next:       out std_logic_vector(inp_number-1 downto 0);
    -- CMI Output
    out_data:       out std_logic_vector(inp_number*inp_data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity concat_CMA;


architecture struct of concat_CMA is
    
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal last_upd:        slv_array(inp_number-1 downto 0)(inp_data_size-1 downto 0);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal mux_value:       slv_array(inp_number-1 downto 0)(inp_data_size-1 downto 0);
    
BEGIN
    
     
    --===============--
    -- COMBINATORICS --
    --===============--
    
    next_comm:
    for i in 0 to inp_number-1 generate
        inp_next(i) <= out_next;
    end generate;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    last_values: 
    for i in 0 to inp_number-1 generate
        
        -- Local Stored Last Values
        process(clk) is
        begin
            if rising_edge(clk) then
                if inp_vld(i) = '1' then
                    last_upd(i) <= inp_data(i);
                end if;
            end if;
        end process;
        
        -- Multiplexer
        mux_value(i) <= inp_data(i) when inp_vld(i) = '1' else last_upd(i);
        
    end generate;
    

    --============--
    -- CMI OUTREG --
    --============--
    cmi_outreg: 
    for i in 0 to inp_number-1 generate
        
        process (clk) is
        begin
            if rising_edge(clk) then
                if out_next = '1' then
                    out_data(inp_data_size*(i+1)-1 downto inp_data_size*i) <= mux_value(i);
                end if;
            end if;
        end process;
    
    end generate;
    
    process (clk) is
    begin
        if rising_edge(clk) then
            if out_next = '1' then
                out_vld <= OR_REDUCE(inp_vld);
            end if;
        end if;
    end process;
    
    
   
    
    
end architecture struct;
