------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    24/09/2013
Module Name:    accu_CMA

-----------------
Short Description
-----------------

    Accumulates incoming data points (controlled by start/stop/rst) and counts the number of data points (smpl_cnt) during accumulation.
    When pbl = 1 and the accumulation isn't stopped, the result and #samples are published on the output (one after the other), including 1 tag bit.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the input 
    out_data_size       := size of the output (includes 1 tag bit+accu_size)
    
    fill_data_size      := missing filler bits (accu_size-inp_data_size)
    accu_size           := actual size of the accu and smpl_cnt
 

--------------
Implementation
--------------
    
    Handling of External Timing Pulses:
    
    start | stop ||    run   
      0      0       nothing
      0      1       rst to 0
      1      0       set to 1
      1      1       rst to 0   
    
    If  run = 1 and inp_vld = 1 the data is being sampled.
    
    rst is directly used for the accu and smpl_cnt.
    
    If pbl = 1 and run = 1 (still) the most recent accumulator value and sample count is published to the output.
    
    The output is published in the following order (including a one tag bit):
    
    '0' + accumulator value
    '1' + #samples 
    
     
    - Last Publish, if stop is not in sync with the last publish
      Working:
            pbl | stop ||           commment
             1     0       pbl before stop
             1     1       last pbl in sync with stop
             1     0       no pbl, nothing is tracked
      
      Not Working:
            pbl | stop ||           commment
             1     0       pbl before stop
             0     1       stop
             1     0       pbl should happen (incl. last part of tracked period)
      
      Solved with Last Publish Tracker
     
     
-----------
Limitations
-----------
    
    During publishing(standard: 2CCs), there can't be another pbl event. There is no next connection between input and output.

----------------
Missing Features
----------------

    
      
            

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity accu_CMA is

generic
(
    inp_data_size:      integer;
    out_data_size:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- External Timing Pulses
    start:          in  std_logic;
    stop:           in  std_logic;
    pbl:            in  std_logic;
    rst:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(out_data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity accu_CMA;


architecture struct of accu_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant accu_size:         integer := out_data_size-1;
    constant fill_data_size:    integer := accu_size-inp_data_size;
    
    --======--
    -- FSMS --
    --======--
    
    type pbl_state_t is (idle, accu_pbl, smpl_pbl);
    signal pbl_present_state, pbl_next_state: pbl_state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Sample Counter     
    signal smpl_cnt:        UNSIGNED(accu_size-1 downto 0) := (others => '0');
    signal smpl_rst:        std_logic;
    signal smpl_inc:        std_logic;
    
    -- Run Toggle
    signal run:             std_logic := '0';
    signal run_set:         std_logic;
    signal run_rst:         std_logic;
    
    -- Last Publish Tracker
    signal last_pbl:        std_logic := '0';
    signal last_pbl_rst:    std_logic;
    SIGNAL last_pbl_set:    std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal accu:            UNSIGNED(accu_size-1 downto 0) := (others => '0'); 
    signal pbl_accu:        std_logic_vector(out_data_size-1 downto 0) := (others => '0');
    signal pbl_smpl:        std_logic_vector(out_data_size-1 downto 0) := (others => '0');
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal out_vld_int:     std_logic;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal accu_rst:        std_logic;
    signal accu_en:         std_logic;
    signal pbl_en:          std_logic;
    signal sel_smpl:        std_logic;
    signal SnS:             std_logic_vector(2 downto 1);
    signal accu_inp:        UNSIGNED(accu_size-1 downto 0);
    signal last_pbl_done:   std_logic;
    
BEGIN
    
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    SnS <= start & stop;
    
    -- Start/Stop Handling
    process(all) is
    begin
        --DEFAULTS
        run_set <= '0';
        run_rst <= '0';
        
        case SnS is
            when "00" => null;
            when "01" => run_rst <= '1';
            when "10" => run_set <= '1';
            when "11" => run_rst <= '1';
            when others => null;
        end case;
        
    end process;
    
    -- Accu/Sample Counter Control 
    accu_en     <= inp_vld AND run;
    smpl_inc    <= inp_vld AND run;
    
    accu_rst    <= rst;
    smpl_rst    <= rst;
    
    -- Input CMI
    inp_next    <= '1';
    
    -- Connect Accu Operand
    accu_inp    <= (fill_data_size-1 downto 0 => '0') & UNSIGNED(inp_data);
    
    -- Last Publish
    last_pbl_set <= start;
    last_pbl_rst <= '1' when last_pbl_done = '1' OR (pbl = '1' AND stop = '1') else '0';
    
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            -- Sample Counter
            if smpl_rst= '1' then
                if smpl_inc = '1' then
                    smpl_cnt <= (accu_size-1 downto 1 => '0') & '1';
                else
                    smpl_cnt <= (others => '0');
                end if;
            elsif smpl_inc = '1' then
                smpl_cnt <= smpl_cnt + 1;
            end if;
            
            -- Run Tracker
            if run_rst= '1' then
                run <= '0';
            elsif run_set = '1' then
                run <= '1';
            end if;
            
            -- Last Publish Tracker
            if last_pbl_rst= '1' then
                last_pbl <= '0';
            elsif last_pbl_set = '1' then
                last_pbl <= '1';
            end if;
        end if;
    end process;

    --=============--
    -- ACCUMULATOR --
    --=============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if accu_rst = '1' then
                if accu_en = '1' then
                    accu <= accu_inp;
                else
                    accu <= (others => '0');
                end if;
            elsif accu_en = '1' then
                accu <= accu + accu_inp;
            end if;
            
        end if;
    end process;
    
    --==============--
    -- PUBLISH REGS --
    --==============--
    
    -- Accu Output
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if pbl_en = '1' then
                pbl_accu <= '0' & STD_LOGIC_VECTOR(accu);
                pbl_smpl <= '1' & STD_LOGIC_VECTOR(smpl_cnt);
            end if;
                    
        end if;
    end process;
    
    --============--
    -- CMI OUTREG --
    --============--
    
    -- Accu Output
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_vld    <= out_vld_int;
                if sel_smpl = '1' then
                    out_data   <= pbl_smpl;
                else
                    out_data   <= pbl_accu;
                end if;
            end if;
                  
        end if;
    end process;   
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            pbl_present_state <= pbl_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        out_vld_int     <= '0';
        sel_smpl        <= '0';
        last_pbl_done   <= '0';
        pbl_en          <= '0';
        
        case pbl_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if (pbl = '1' AND run = '1') OR (pbl = '1' AND run = '0' AND last_pbl = '1') then
                    last_pbl_done   <= '1';
                    pbl_en          <= '1';
                    pbl_next_state  <= accu_pbl;
                else
                    pbl_next_state  <= idle;
                end if;      
            
            --===============--
            when accu_pbl =>
            --===============--
                
                if out_next = '1' then
                    out_vld_int     <= '1';
                    pbl_next_state  <= smpl_pbl;
                else
                    pbl_next_state  <= accu_pbl;
                end if;
                
            --===============--
            when smpl_pbl =>
            --===============--
                
                if out_next = '1' then
                    out_vld_int     <= '1';
                    sel_smpl        <= '1';
                    pbl_next_state  <= idle;
                else
                    pbl_next_state  <= smpl_pbl;
                end if;
                
        end case;
    end process;
  
    
    
end architecture struct;

