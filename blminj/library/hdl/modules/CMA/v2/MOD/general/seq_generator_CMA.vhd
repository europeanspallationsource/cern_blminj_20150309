------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/08/2012
Module Name:    seq_generator_CMA  

-----------
Description
-----------

    Creates a sequence of predefined outputs(.mif file) after a trigger. It is not reset by an additional trigger before the sequence ends. 

------------------
Generics/Constants
------------------

    data_size   := size of the output data port and width of the memory
    init_file   := .mif file for the ROM (data_size x numwords)
    numwords    := number of words in the sequence 
    
    addr_size   := size for the ROM addr input
    
-----------
Limitations
-----------


----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
    altera_mf

-----------------
Necessary Modules
-----------------

    CMI
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

------------------------
Not implemented Features
------------------------

    - change between ROM implementation and one that is in logic


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

library altera_mf;
    use altera_mf.altera_mf_components.all;
    
    
entity seq_generator_CMA is

generic
(
    data_size:          integer;
    numwords:           integer;
    init_file:          string;
    device_family:      string
);

port
(
    -- Clock
    clk:                in  std_logic;
        -- Timing
    trigger:            in  std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(data_size-1 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic

);

end entity seq_generator_CMA;


architecture struct of seq_generator_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--

    constant addr_size: integer := get_vsize_addr(numwords);
    
    --======--
    -- FSMs --
    --======--
    
    type state_t is (idle, run_seq);
    signal present_state, next_state: state_t := idle;
    
    --==========--
    -- COUNTERs --
    --==========--
    
    -- Bit Counter
    signal addr_cnt:        UNSIGNED(addr_size-1 downto 0) := (addr_size-1 downto 0 => '0');
    signal addr_rst:        std_logic;
    signal addr_inc:        std_logic;
    
    --=================--
    -- GENERAL SIGNALs --
    --=================--
    
    signal rom_addr:        std_logic_vector(addr_size-1 downto 0);
    signal next_addr:       std_logic;
    signal out_vld_int:     std_logic;
    
    signal q:               std_logic_vector(data_size-1 downto 0);
    
BEGIN

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if addr_rst= '1' then
                addr_cnt <= (addr_size-1 downto 0 => '0');
            elsif addr_inc = '1' then
                addr_cnt <= addr_cnt + 1;
            end if;
            
        end if;
    end process;

    --=====--
    -- ROM --
    --=====--

    ROM : altera_mf.altera_mf_components.altsyncram
    GENERIC MAP 
    (
        operation_mode          => "ROM",
        init_file               => init_file,
        intended_device_family  => device_family,
        width_a                 => data_size,
        widthad_a               => addr_size,
        numwords_a              => numwords,
        address_aclr_a          => "NONE",
        outdata_aclr_a          => "NONE",
        outdata_reg_a           => "UNREGISTERED",
        width_byteena_a         => 1,
        lpm_hint                => "ENABLE_RUNTIME_MOD=NO",
        lpm_type                => "altsyncram" 
    )
    PORT MAP 
    (
        address_a               => rom_addr,
        clock0                  => clk,
        q_a                     => out_data
    );
    
    
    rom_addr    <=  STD_LOGIC_VECTOR(addr_cnt + 1) when next_addr = '1' else
                    STD_LOGIC_VECTOR(addr_cnt);

    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_vld     <= out_vld_int;
            end if;
                    
        end if;
    end process;

    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        out_vld_int <= '0';
        addr_rst    <= '0';
        addr_inc    <= '0';
        next_addr   <= '0';
        
        case present_state is
            
            --===============--
            when idle =>
            --===============--
            
                if trigger = '1' then
                    next_state <= run_seq;
                else
                    next_state <= idle;
                end if;
            
            --===============--
            when run_seq =>
            --===============--
                
                if out_next = '1' then
                    out_vld_int <= '1';
                    next_addr   <= '1';
                    if addr_cnt = numwords-1 then
                        addr_rst    <= '1';
                        next_state  <= idle;
                    else
                        addr_inc    <= '1';
                        next_state  <= run_seq;
                    end if;
                else
                    next_addr   <= '0';
                    next_state  <= run_seq;
                end if;
            
            --===============--
            when others =>
            --===============--
            
                next_state <= idle;
                
        end case;
    end process;
    
end architecture struct;
