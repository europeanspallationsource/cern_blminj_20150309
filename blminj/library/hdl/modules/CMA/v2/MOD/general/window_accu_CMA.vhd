------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    25/09/2013
Module Name:    window_accu_CMA

-----------------
Short Description
-----------------

    stores the input in a shift register and outputs the accumulated result over a predefined window size

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    inp_data_size           := size of the input/sample data
    out_data_size           := size of the output data (RS = Running Sum)
    window_length           := length of the RS window (Type: Time)
    gran                    := granularity/occurence of the input samples (Type: Time)
    
    SR_size                 := size of the shift register to store the full window
    signed_inp_data_size    := the unsigned inp_data_size increased by 1

--------------
Implementation
--------------
    
    With inp_vld = '1' the inp_data is shifted into the shift register (SR) and at the same time the accumulator (accu)
    is updated with the result of (new_smpl - old_smpl) + accu. The actual calculation is done SIGNED and is casted afterwards
    to UNSIGNED.
    
    The input is only blocked, in case the result of the accumulation couldn't be forwarded to the next module before a new sample
    arrived at the input.

-----------
Limitations
-----------
    
    Two additions are happening in series (decreases clock frequency to a certain extend)

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity window_accu_CMA is

generic
(
    inp_data_size:      integer;
    out_data_size:      integer;
    window_length:      time;
    gran:               time
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(out_data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity window_accu_CMA;


architecture struct of window_accu_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant SR_size:               integer := window_length/gran;
    constant signed_inp_data_size:  integer := inp_data_size+1;

    --===========--
    -- REGISTERS --
    --===========--
    
    signal accu:        UNSIGNED(out_data_size-1 downto 0)                           := (others => '0');
    signal SR:          usign_array(SR_size-1 downto 0)(inp_data_size-1 downto 0)    := (others => (others => '0'));
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal SR_inp:          UNSIGNED(inp_data_size-1 downto 0);
    signal SR_out:          UNSIGNED(inp_data_size-1 downto 0);
    signal SR_en:           std_logic;
    signal SR_shift:        std_logic;
    
    signal accu_en:         std_logic;
    
    signal new_smpl:        SIGNED(signed_inp_data_size-1 downto 0);
    signal old_smpl:        SIGNED(signed_inp_data_size-1 downto 0);
    signal diff_smpl:       SIGNED(signed_inp_data_size-1 downto 0);
    signal new_accu:        SIGNED(out_data_size-1 downto 0);
    
    
BEGIN
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    inp_next    <= out_next OR NOT inp_vld;
    
    SR_inp      <= UNSIGNED(inp_data);
    
    SR_shift    <= inp_vld AND out_next;
    accu_en     <= inp_vld AND out_next;
    
    --================--
    -- SHIFT REGISTER --
    --================--
    
    process(clk)
    begin
        if rising_edge(clk) then
            if SR_shift = '1' then
                SR(SR_size-1 downto 1)  <= SR(SR_size-2 downto 0);
                SR(0)                   <= SR_inp;
            end if;
        end if;
    end process;
    
    SR_out <= SR(SR_size-1);
    
    --==========--
    -- FUNCTION --
    --==========--
    
    new_smpl    <= SIGNED('0' & SR_inp);
    old_smpl    <= SIGNED('0' & SR_out);
    diff_smpl   <= new_smpl - old_smpl;
    
    new_accu    <= SIGNED(accu) + diff_smpl;
   
    --=============--
    -- ACCUMULATOR --
    --=============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if accu_en = '1' then
                accu <= UNSIGNED(new_accu);
            end if;
            
        end if;
    end process;
    
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_data    <= STD_LOGIC_VECTOR(new_accu);
                out_vld     <= inp_vld;
            end if;
                    
        end if;
    end process;
    
    
end architecture struct;

