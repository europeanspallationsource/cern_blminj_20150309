------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Oliver Bitterling
Updated:        01/08/2012
Module Name:    fifo_CMA  

-----------
Description
-----------

    fifo_CMA module with CMI input and output.

------------------
Generics/Constants
------------------

    data_size   := size of the input/output and fifo_CMA entry
    fifo_depth  := depth of the utilized fifo_CMA 


-----------
Limitations
-----------

    < limitations for the generics and/or the module itself >


----------------------------
Necessary Packages/Libraries
----------------------------

    altera_mf
 
-----------------
Necessary Modules
-----------------

    CMI

---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    < detailed descriptions of the internal functionality of the module including port usage >

    
------------------------
Not implemented Features
------------------------

    < informations about missing features or nice-to-have addons >


*/
------------------------------

LIBRARY ieee;
    use ieee.std_logic_1164.all;
    use ieee.math_real.all;

LIBRARY altera_mf;
    use altera_mf.altera_mf_components.all;


entity fifo_CMA is

generic
(
    data_size:  integer := 16;
    fifo_depth: integer := 32
);

port
(   
    -- Clocks
    clk:        in  std_logic;
    -- CMI Input
    inp_data:   in  std_logic_vector(data_size-1 downto 0);
    inp_vld:    in  std_logic;
    inp_next:   out std_logic;
    -- CMI Output       
    out_data:   out std_logic_vector(data_size-1 downto 0);
    out_vld:    out std_logic := '0';
    out_next:   in  std_logic   
);
    
end entity fifo_CMA;

architecture struct of fifo_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant lpm_widthu :   natural := natural(ceil(LOG2 (real(fifo_depth))));
    
    --=================--
    -- GENERAL SIGNALs --
    --=================--
    
    -- fifo Signals
    signal full, empty      : std_logic;
    Signal wr_req, rd_req   : std_logic;
    Signal data, q          : std_logic_vector(data_size-1 downto 0);
    
    
BEGIN
    
    --======--
    -- fifo --
    --======--
    
    fifo: altera_mf.altera_mf_components.scfifo
    GENERIC MAP 
    (
        add_ram_output_register => "OFF",
        intended_device_family  => "Cyclone IV GX",
        lpm_numwords            => fifo_depth,
        lpm_showahead           => "OFF",
        lpm_type                => "scfifo",
        lpm_width               => data_size,
        lpm_widthu              => lpm_widthu,
        overflow_checking       => "ON",
        underflow_checking      => "ON",
        use_eab                 => "ON"
    )
    PORT MAP 
    (
        clock   => clk,
        data    => data,
        rdreq   => rd_req,
        wrreq   => wr_req,
        empty   => empty,
        full    => full,
        q       => q
    );

    
    --==============--
    -- COMBINATORIC --
    --==============--
    
    wr_req      <= inp_vld AND NOT(full);
    data        <= inp_data;
    inp_next    <= NOT(full) OR NOT(inp_vld);
    
    out_data    <= q;
    rd_req      <= out_next AND NOT(empty);
    
    
    --============--
    -- CMI OUTREG --
    --============--
    
    Process(clk)
    Begin
        if rising_edge(clk) then
            if out_next = '1' then
                out_vld <= NOT(empty);
            end if;
        end if;
    End process;
    
    
    
End Architecture struct;