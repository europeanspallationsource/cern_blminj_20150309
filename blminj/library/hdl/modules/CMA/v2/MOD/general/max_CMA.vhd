------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    24/09/2013
Module Name:    max_CMA

-----------------
Short Description
-----------------

    Checks for and stores the maximum value out of all incoming data points during an active period defined by (start, stop, rst).
    With pbl = '1' the current max value is published on the output.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    data_size       := size of input and output 

--------------
Implementation
--------------
    
    Handling of External Timing Pulses:
    
    start | stop ||    run   
      0      0       nothing
      0      1       rst to 0
      1      0       set to 1
      1      1       rst to 0   
    
    If run = 1 and inp_vld = 1 the data is being sampled.
    
    The rst input is directly used for resetting the maximum value to 0.
    
    If pbl = 1 and run = 1 (still) the maximum value is published to the output.
    
    
    
    - Last Publish, if stop is not in sync with the last publish
      Working:
            pbl | stop ||           commment
             1     0       pbl before stop
             1     1       last pbl in sync with stop
             1     0       no pbl, nothing is tracked
      
      Not Working:
            pbl | stop ||           commment
             1     0       pbl before stop
             0     1       stop
             1     0       pbl should happen (incl. last part of tracked period)
      
      Solved with Last Publish Tracker
      
-----------
Limitations
-----------
    
    During publishing(standard: 1CCs), there can't be another pbl event. There is no next connection between input and output.

----------------
Missing Features
----------------
            

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity max_CMA is

generic
(
    data_size:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- External Timing Pulses
    start:          in  std_logic;
    stop:           in  std_logic;
    pbl:            in  std_logic;
    rst:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity max_CMA;


architecture struct of max_CMA is

    --======--
    -- FSMS --
    --======--
    
    type pbl_state_t is (idle, max_pbl);
    signal pbl_present_state, pbl_next_state: pbl_state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Run Toggle
    signal run:             std_logic;
    signal run_set:         std_logic;
    signal run_rst:         std_logic;
    
    -- Last Publish Tracker
    signal last_pbl:        std_logic := '0';
    signal last_pbl_rst:    std_logic;
    signal last_pbl_set:    std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal max_value:       UNSIGNED(data_size-1 downto 0) := (others => '0');
    signal pbl_max_value:   std_logic_vector(data_size-1 downto 0) := (others => '0');
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal out_vld_int:     std_logic;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal max_value_en:    std_logic;
    signal max_value_rst:   std_logic;
    signal pbl_en:          std_logic;
    signal SnS:             std_logic_vector(1 downto 0);
    signal last_pbl_done:   std_logic;
    
BEGIN
    
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    SnS <= start & stop;
    
    -- Start/Stop Handling
    process(all) is
    begin
        --DEFAULTS
        run_set <= '0';
        run_rst <= '0';
        
        case SnS is
            when "00" => null;
            when "01" => run_rst <= '1';
            when "10" => run_set <= '1';
            when "11" => run_rst <= '1';
            when others => null;
        end case;
        
    end process;
    
    -- Accu/Sample Counter Control 
    max_value_en     <= inp_vld AND run;
    max_value_rst    <= rst;
    
    -- Input CMI
    inp_next        <= '1';
    
     -- Last Publish
    last_pbl_set <= start;
    last_pbl_rst <= '1' when last_pbl_done = '1' OR (pbl = '1' AND stop = '1') else '0';
    
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            -- Run Tracker
            if run_rst= '1' then
                run <= '0';
            elsif run_set = '1' then
                run <= '1';
            end if;
            
            -- Last Publish Tracker
            if last_pbl_rst= '1' then
                last_pbl <= '0';
            elsif last_pbl_set = '1' then
                last_pbl <= '1';
            end if;
            
        end if;
    end process;

    --==============--
    -- MAX FUNCTION --
    --==============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if max_value_rst = '1' then
                max_value <= (others => '0');
            elsif max_value_en = '1' then
                if UNSIGNED(inp_data) > max_value then
                    max_value <= UNSIGNED(inp_data);
                end if;
            end if;
            
        end if;
    end process;
    
    --==============--
    -- PUBLISH REGS --
    --==============--
    
    -- Accu Output
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if pbl_en = '1' then
                pbl_max_value <= STD_LOGIC_VECTOR(max_value);
            end if;
                    
        end if;
    end process;
    
    --============--
    -- CMI OUTREG --
    --============--
    
    -- Accu Output
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_vld    <= out_vld_int;
                out_data   <= pbl_max_value;
            end if;
                  
        end if;
    end process;   
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            pbl_present_state <= pbl_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        out_vld_int     <= '0';
        pbl_en          <= '0';
        last_pbl_done   <= '0';
        
        
        case pbl_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if (pbl = '1' AND run = '1') OR (pbl = '1' AND run = '0' AND last_pbl = '1') then
                    last_pbl_done   <= '1';
                    pbl_en          <= '1';
                    pbl_next_state  <= max_pbl;
                else
                    pbl_next_state  <= idle;
                end if;
                
            --===============--
            when max_pbl =>
            --===============--
                
                if out_next = '1' then
                    out_vld_int         <= '1';
                    pbl_next_state  <= idle;
                else
                    pbl_next_state  <= max_pbl;
                end if;
                
                
        end case;
    end process;
  
    
    
end architecture struct;

