------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/01/2012
Module Name:    smpl_detect_CMA

-----------------
Short Description
-----------------

    A module to detect data passing. Sends out a '1' on smpl_det for 1 CC, when there is a valid input data.
    Sends out the next, when a new valid data point arrives.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    data_size           := size of input/output data    

--------------
Implementation
--------------
    
    implements small shift register, that shifts with every next signal.
    

-----------
Limitations
-----------
    
    If it is used to measure the timing of incoming data points, one incoming should have been taken by the next modules
    (out_next = '1'), before the next arrives.

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity smpl_detect_CMA is

generic
(
    data_size:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Status
    smpl_det:       out std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity smpl_detect_CMA;


architecture struct of smpl_detect_CMA is

    --===========--
    -- REGISTERS --
    --===========--

    signal not_taken:   std_logic := '0';


BEGIN
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    smpl_det <= '1' when inp_vld = '1' AND not_taken = '0' else
                '0';
    
    inp_next <= out_next OR NOT inp_vld;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if inp_vld = '1' AND out_next = '0' then
                not_taken <= '1';
            else
                not_taken <= '0';
            end if;
            
        end if;
    end process;
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_data    <= inp_data;
                out_vld     <= inp_vld;
            end if;
                    
        end if;
    end process;

    
end architecture struct;

