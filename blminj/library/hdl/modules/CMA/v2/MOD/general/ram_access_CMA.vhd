------------------------------
/*
Company:       CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2013
Module Name:    ram_access_CMA

    
-----------------
Short Description
-----------------

    This CMA module enables a one port connection between the CMA world and a typical internal FPGA RAM.


------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    wrreq_data_size     := size of the CMI wrreq data port (includes data, addr and byte_enable for the RAM) 
    rdreq_data_size     := size of the CMI rdreq data port (includes only an address)
    rdout_data_size     := size of the CMI rdout data port (includes read date from the RAM)
    wrreq_addr_MSB      := MSB of the address in the wrreq data port (real position)
    wrreq_data_MSB      := MSB of the data part in the wrreq data port (real position)
    wrreq_byteena_MSB   := MSB of the byte_enable in the wrreq data port, if existing (real position)
    RAM_addr_size       := size of the RAM address port (also used as the size for the cutout from the wrreq port)
    RAM_data_size       := size of the RAM data port (also used as the size for the cutout from the wrreq port)
    RAM_byteena_size    := size of the RAM byte_enable port (has to be set to RAM_data_size/8)
    full_byteena        := the full RAM_byteena is getting generated internally (all set to 1) and is not part of the input data port
    
    wrreq_byteena_MSB   := LSB of the byte_enable in the wrreq data port (calculated with the RAM_byteena_size)
    wrreq_addr_MSB      := LSB of the address in the wrreq data port (calculated with the RAM_address_size)
    wrreq_data_MSB      := LSB of the data part in the wrreq data port (calculated with the RAM_data_size)
    
    
--------------
Implementation
--------------
    
    essentially direct connection between CMA and RAM
     
     
-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/      
------------------------


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

entity ram_access_CMA is

generic
(
    wrreq_data_size:        integer;
    rdreq_data_size:        integer;
    rdout_data_size:        integer;
    wrreq_addr_MSB:         integer;
    wrreq_data_MSB:         integer;
    wrreq_byteena_MSB:      integer;
    RAM_addr_size:          integer;
    RAM_data_size:          integer;
    RAM_byteena_size:       integer;
    full_byteena:           integer := 0
    
);

port
(
    -- Clock
    clk:                    in  std_logic;
    -- CMI Input
    rdreq_data:             in  std_logic_vector(rdreq_data_size-1 downto 0);
    rdreq_vld:              in  std_logic;
    rdreq_next:             out std_logic;
    wrreq_data:             in  std_logic_vector(wrreq_data_size-1 downto 0);
    wrreq_vld:              in  std_logic;
    wrreq_next:             out std_logic;
    -- CMI Output
    rdout_data:             out std_logic_vector(rdout_data_size-1 downto 0);
    rdout_vld:              out std_logic := '0';
    rdout_next:             in  std_logic; 
    -- RAM Connection
    RAM_addr:               out std_logic_vector(RAM_addr_size-1 downto 0);
    RAM_data:               out std_logic_vector(RAM_data_size-1 downto 0);
    RAM_byteena:            out std_logic_vector(RAM_byteena_size-1 downto 0);
	RAM_wren:               out std_logic;
	RAM_q:                  in  std_logic_vector(RAM_data_size-1 downto 0)
);

end entity ram_access_CMA;


architecture struct of ram_access_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--

    
    constant wrreq_addr_LSB:    integer := wrreq_addr_MSB - RAM_addr_size + 1;
    constant wrreq_data_LSB:    integer := wrreq_data_MSB - RAM_data_size + 1;
    constant wrreq_byteena_LSB: integer := wrreq_byteena_MSB - RAM_byteena_size + 1;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal addr_int:      std_logic_vector(RAM_addr_size-1 downto 0);
    signal wren_int:      std_logic;
    
    
BEGIN
    
    
    --==================--
    -- CMI OUTREG RDREQ --
    --==================--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if rdout_next = '1' then
                rdout_vld <= rdreq_vld;
            end if;
        end if;
    end process;
    
    
    --===============--
    -- RD/WR Control --
    --===============--
    
    process(all) is
    begin
    -- Defaults
    rdreq_next <= '1';
    wrreq_next <= '1';
    
    if wrreq_vld = '1' then
        addr_int      <= wrreq_data(wrreq_addr_MSB downto wrreq_addr_LSB);
        wren_int      <= '1';
        rdreq_next    <= NOT rdreq_vld;
    else
        addr_int      <= rdreq_data(RAM_addr_size-1 downto 0);
        wren_int      <= '0';
    end if;
    end process;
    
    
    --================--
    -- RAM Connection --
    --================--
    
    RAM_addr        <= addr_int;
    RAM_data        <= wrreq_data(wrreq_data_MSB downto wrreq_data_LSB);
    RAM_wren        <= wren_int;
    rdout_data      <= RAM_q(rdout_data_size-1 downto 0);
    
        
    full_byteena_gen: 
    if full_byteena = 1 generate
        begin
            RAM_byteena <= (others => '1');
        end;
    end generate;
    
    passed_byteena_gen:
    if full_byteena = 0 generate
        begin
            RAM_byteena <= wrreq_data(wrreq_byteena_MSB downto wrreq_byteena_LSB);
        end;
    end generate;

    
end architecture struct;