------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/11/2013
Module Name:    compare_pass_CMA

-----------------
Short Description
-----------------

    Compares the input data to a supplied parameter. Can be set to compare for over-Threshold, under-Threshold and Not-Equal. Sets the
    result output to 1, if the data input passes the threshold or is not equal. Additionally the input is passed to the output.
    
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    data_size           := size of the input/output
    param_data_size      := size of the the comparator value (doesn't need to be the same size as inp_data_size)
    comp_type           := the type of the comparison (overTH_signed = 4, underTH_signed = 3, notEqual = 2, overTH = 1, underTH = 0)

--------------
Implementation
--------------
    
    Depending on configuration, the module checks for overTH or underTH and directly uses the inp_vld signal
    as vld signal for the output. In turn a new data point is only accepted, if the output is able to accept data.
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity compare_pass_CMA is

generic
(
    data_size:          integer;
    param_data_size:    integer;
    comp_type:          integer
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Threshold
    PARAM_comp_value:   in  std_logic_vector(param_data_size-1 downto 0);
    -- CMI Input
    inp_data:           in  std_logic_vector(data_size-1 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(data_size-1 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic;
    -- Std Output
    result:             out std_logic
);

end entity compare_pass_CMA;


architecture struct of compare_pass_CMA is
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal chk_done:        std_logic := '0';
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal out_vld_int:     std_logic;
    signal chk_inp:         std_logic;
    
BEGIN
    
           
    --================--
    -- CHECK DONE REG --
    --================--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            
            if inp_vld = '1' AND out_next = '0' then
                chk_done <= '1';
            else
                chk_done <= '0';
            end if;
            
        end if;
    end process;
    
    --===============--
    -- OOMBINATORICS --
    --===============--
    
    -- Next
    inp_next <= out_next OR NOT inp_vld;
     
    -- Trigger Check
    chk_inp <=  inp_vld when chk_done = '0' else
                '0';
    
    
    overTH_signed: 
    if comp_type = 4 generate
        
        process (all) is
        begin
            if chk_inp = '1' AND SIGNED(inp_data) > SIGNED(PARAM_comp_value) then
                result <= '1';
            else
                result <= '0';
            end if;
            
        end process;
        
    end generate;
    
    underTH_signed: 
    if comp_type = 3 generate
    
        process (clk) is
        begin
            if chk_inp = '1' AND SIGNED(inp_data) < SIGNED(PARAM_comp_value) then
                result <= '1';
            else
                result <= '0';
            end if;
            
        end process;
        
    end generate;
    
    notEqual: 
    if comp_type = 2 generate
        
        process (all) is
        begin
            if chk_inp = '1' AND UNSIGNED(inp_data) /= UNSIGNED(PARAM_comp_value) then
                result <= '1';
            else
                result <= '0';
            end if;
            
        end process;
        
    end generate;
    
    
    overTH_unsigned: 
    if comp_type = 1 generate
        
        process (all) is
        begin
            if chk_inp = '1' AND UNSIGNED(inp_data) > UNSIGNED(PARAM_comp_value) then
                result <= '1';
            else
                result <= '0';
            end if;
            
        end process;
        
    end generate;
    
    underTH_unsigned: 
    if comp_type = 0 generate
    
        process (clk) is
        begin
            if chk_inp = '1' AND UNSIGNED(inp_data) < UNSIGNED(PARAM_comp_value) then
                result <= '1';
            else
                result <= '0';
            end if;
            
        end process;
        
    end generate;
 
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
        
            if out_next = '1' then
                out_vld     <= inp_vld;
                out_data    <= inp_data;
            end if;
            
        end if;
    end process;
    
    
end architecture struct;

