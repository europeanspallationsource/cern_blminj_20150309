------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/10/2014
Module Name:    removeCMA_CMA

-----------------
Short Description
-----------------

    This module removes the CMA Interface and just forwards the data. The output gets an update, every time the input is valid.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the input data
    out_data_size       := size of the output data
    delay_len           := length of the pipeline    

--------------
Implementation
--------------
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity removeCMA_CMA is

generic
(
    inp_data_size:      integer;
    out_data_size:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- Output
    out_data:       out std_logic_vector(out_data_size-1 downto 0)
);

end entity removeCMA_CMA;


architecture struct of removeCMA_CMA is
   
BEGIN
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    inp_next    <= '1';
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if inp_vld = '1' then
                out_data <= inp_data(out_data_size-1 downto 0);
            end if;
                    
        end if;
    end process;
    
    
end architecture struct;

