------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    combine_CMA

-----------------
Short Description
-----------------

    This module combines a certain amount(out_data_size/inp_data_size) of valid input data to one output data word. 
    The first input is positioned at the MSB of the final word and the last input is positioned at the LSB of the final word.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    inp_data_size   := size of single input data
    out_data_size   := size of combined output data


    
--------------
Implementation
--------------
    
    
-----------
Limitations
-----------
    
    - obviously input has to be smaller than output size
    - waits forever if there are not enough slices to combine on the input

----------------
Missing Features
----------------

    external parameter update 

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity combine_CMA is

generic
(
    inp_data_size:      integer;
    out_data_size:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(out_data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity combine_CMA;


architecture struct of combine_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- number of slices
    constant slice_n:          integer := INTEGER(CEIL(REAL(out_data_size)/REAL(inp_data_size)));
    constant slice_n_vsize:    integer := get_vsize(slice_n);
    
    --======--
    -- FSMs --
    --======--
    
    type comb_state_t is (store, publish);
    signal present_state, next_state: comb_state_t := store;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Header Slice Counter
    signal slice_cnt:       UNSIGNED(slice_n_vsize-1 downto 0) := TO_UNSIGNED(slice_n-1, slice_n_vsize);
    signal slice_rst:       std_logic;
    signal slice_dec:       std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal stored_val:      std_logic_vector(out_data_size-1 downto inp_data_size); -- doesn't store the last value
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal out_vld_int:     std_logic;
    
    
BEGIN


    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if slice_rst= '1' then
                slice_cnt <= TO_UNSIGNED(slice_n-1, slice_n_vsize);
            elsif slice_dec = '1' then
                slice_cnt <= slice_cnt - 1;
            end if;
            
        end if;
    end process;
    
    
    --===========--
    -- REGISTERS --
    --===========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if inp_vld = '1' then
                if slice_cnt = slice_n-1 then
                    stored_val(out_data_size-1 downto inp_data_size*(TO_INTEGER(slice_cnt+1)))                          <= (others => '0');  
                    stored_val(inp_data_size*(TO_INTEGER(slice_cnt)+1)-1 downto inp_data_size*TO_INTEGER(slice_cnt))    <= inp_data;
                elsif slice_cnt > 0 then
                    stored_val(inp_data_size*(TO_INTEGER(slice_cnt)+1)-1 downto inp_data_size*TO_INTEGER(slice_cnt))    <= inp_data;
                end if;
            end if;
                    
        end if;
    end process;
    
    

    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_data    <= stored_val & inp_data;
                out_vld     <= out_vld_int;
            end if;
                    
        end if;
    end process;
    
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        inp_next        <= '1';
        out_vld_int     <= '0';
        -- Counter
        slice_dec       <= '0';
        slice_rst       <= '0';
        
        
        case present_state is
            
            --===============--
            when store =>
            --===============--
                
                if inp_vld = '1' then
                    if slice_cnt = 1 then -- one before the last
                        slice_dec   <= '1';
                        next_state  <= publish;
                    else
                        slice_dec   <= '1';
                        next_state  <= store;
                    end if;
                else
                    next_state <= store;
                end if;     
            
            --===============--
            when publish =>
            --===============--
               
                if inp_vld = '1' then
                    if out_next = '1' then
                        out_vld_int <= '1';
                        slice_rst   <= '1';
                        next_state  <= store;
                    else
                        inp_next    <= '0';
                        next_state  <= publish;
                    end if;
                else
                    next_state <= publish;
                end if;
            
        end case;
    end process;
  
    
    
end architecture struct;
