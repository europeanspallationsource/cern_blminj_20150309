
------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/09/2013
Module Name:    slice_CMA

-----------------
Short Description
-----------------

    slices a generic sized data input into multiple pieces with a predefined output size (MSB first)

------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the input data
    out_data_size       := size of each output data slice
    
    slice_n             := # of slices
    slice_n_vsize       := vectorsize for the slice counter
    
--------------
Implementation
--------------
    
    The number of slices (slice_n) that are necessary to accomodate the complete input data are calculated beforehand. 
    Depending on this number, the initial data is filled with '0' if applicable. This is then called the full_data, which in turn is then publish on the 
    out_data output for the next slice_n cycles including the starting one. During this operation, the input is locked (inp_next = '0').
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity slice_CMA is

generic
(
   inp_data_size:    integer;
   out_data_size:    integer 
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(out_data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity slice_CMA;


architecture struct of slice_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- number of slices
    constant slice_n:           integer := INTEGER(CEIL(REAL(inp_data_size)/REAL(out_data_size)));
    constant slice_n_vsize:     integer := get_vsize(slice_n);
    
    --======--
    -- FSMS --
    --======--
    
    type slice_state_t is (idle, send_data);
    signal slice_present_state, slice_next_state: slice_state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Slice Counter
    signal slice_cnt:       UNSIGNED(slice_n_vsize-1 downto 0) := TO_UNSIGNED(slice_n-1, slice_n_vsize);
    signal slice_rst:       std_logic;
    signal slice_dec:       std_logic;

    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal full_data:       std_logic_vector(out_data_size*slice_n-1 downto 0);
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal out_vld_int:  std_logic;
    
    
BEGIN

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if slice_rst= '1' then
                slice_cnt <= TO_UNSIGNED(slice_n-1, slice_n_vsize);
            elsif slice_dec = '1' then
                slice_cnt <= slice_cnt - 1;
            end if;
            
        end if;
    end process;

    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_vld  <= out_vld_int;
                out_data <= full_data((TO_INTEGER(slice_cnt)+1)*out_data_size-1 downto TO_INTEGER(slice_cnt)*out_data_size);
            end if;
                    
        end if;
    end process;
    
    
    --============--
    -- FULL INPUT --
    --============--
    
    full_data <= (out_data_size*slice_n-1 downto inp_data_size => '0') & inp_data;
    
    --===============--
    -- SLICE CONTROL --
    --===============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            slice_present_state <= slice_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        inp_next        <= '1';
        out_vld_int     <= '0';
        -- Slice Counter
        slice_rst       <= '0';
        slice_dec       <= '0';

        
        case slice_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if inp_vld = '1' then
                    inp_next <= '0';
                    if out_next = '1' then
                        out_vld_int <= '1';
                        if slice_cnt = 0 then
                            inp_next            <= '1';
                            slice_rst           <= '1';
                            slice_next_state    <= idle;
                        else
                            slice_dec           <= '1';
                            slice_next_state    <= send_data;
                        end if;
                    else
                        slice_next_state    <= send_data;
                    end if;
                else
                    slice_next_state <= idle;
                end if;
                
                
            --===============--
            when send_data =>
            --===============-- 
            
                inp_next <= '0';
                if out_next = '1' then
                    out_vld_int <= '1';
                    if slice_cnt = 0 then
                        inp_next            <= '1';
                        slice_rst           <= '1';
                        slice_next_state    <= idle;
                    else
                        slice_dec           <= '1';
                        slice_next_state    <= send_data;
                    end if;
                else
                    slice_next_state <= send_data;
                end if;

        end case;
    end process;
    
end architecture struct;
