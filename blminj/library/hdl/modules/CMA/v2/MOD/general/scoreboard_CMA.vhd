------------------------------
/*
Company:       CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/02/2014
Module Name:    scoreboard_CMA


-----------------
Short Description
-----------------

    This is a scoreboard, which scores passing datawords(depending on their address) and starts sending out the scoreboard 
    when the pbl signal is active. The Parameter input is the start/base address for the scoreboard in the memory (where the 
    SB is written in the end).

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the input CMI data bus
    out_data_size       := size of the output CMI data bus
    inp_addr_MSB        := the real MSB of the address part on the input data bus
    inp_d_MSB           := the real MSB of the data part on the input data bus 
    out_addr_size       := the size of the address part on the output bus
    out_d_size          := the size of the data part on the output bus
    
    SB_size             := size of the Scoreboard (depends on out_addr_size)
    SB_word_n           := number of words for the scoreboard (depends on out_d_size)
    SB_word_n_vsize     := size of the vector to count SB words
    inp_addr_LSB        := the LSB of the address part on the input data bus (calculated with out_addr_size)
    inp_d_LSB           := the LSB of the data part on the input data bus (calculated with out_d_size)
    
    

--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity scoreboard_CMA is

generic
(
    inp_data_size:      integer;
    inp_addr_MSB:       integer;
    inp_d_MSB:          integer;
    out_data_size:      integer;
    out_addr_size:      integer;
    out_d_size:         integer
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Commands
    pbl:                in  std_logic;
    -- Parameter
    PARAM_SB_base_addr: in  std_logic_vector(out_addr_size-1 downto 0);
    -- CMI Input
    inp_data:           in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(out_data_size-1 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic
);

end entity scoreboard_CMA;


architecture struct of scoreboard_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- Scoreboard
    constant SB_size:           integer := (2**out_addr_size);
    constant SB_word_n:         integer := INTEGER(CEIL(REAL(SB_size)/REAL(out_d_size)));
    constant SB_word_n_vsize:   integer := out_addr_size;
    
    constant inp_addr_LSB:      integer := inp_addr_MSB - out_addr_size + 1;
    constant inp_d_LSB:         integer := inp_d_MSB - out_d_size + 1;
    
    --======--
    -- FSMS --
    --======--
    
    type state_t is (idle, send_sb);
    signal present_state, next_state: state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Bit Counter
    signal word_cnt:        UNSIGNED(SB_word_n_vsize-1 downto 0) := TO_UNSIGNED(SB_word_n-1, SB_word_n_vsize);
    signal word_rst:        std_logic;
    signal word_dec:        std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal SB_data:         std_logic_vector(SB_size-1 downto 0) := (others =>'0');
    signal SB_last_addr:    UNSIGNED(out_addr_size-1 downto 0);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal SB_addr:         std_logic_vector(out_addr_size-1 downto 0);
    signal SB_inp:          UNSIGNED(Out_addr_size-1 downto 0);
    signal SB_rst:          std_logic;
    signal out_sel:         std_logic;
    signal out_vld_int:     std_logic;
    
BEGIN   
    
    
    --===========--
    -- PARAMETER --
    --===========--
    

    SB_last_addr <= UNSIGNED(PARAM_SB_base_addr)+TO_UNSIGNED(SB_word_n-1,out_addr_size);

    
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if word_rst = '1' then
                word_cnt <= TO_UNSIGNED(SB_word_n-1, SB_word_n_vsize);
            elsif word_dec = '1' then
                word_cnt <= word_cnt - 1;
            end if;
            
        end if;
    end process;
    
    --============--
    -- SCOREBOARD --
    --============--
    
    SB_inp <= UNSIGNED(inp_data(inp_addr_MSB downto inp_addr_LSB));
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if SB_rst = '1' then
                SB_data <= (others => '0');
            elsif inp_vld = '1' then
                SB_data(TO_INTEGER(SB_inp)) <= '1';
            end if;
            
        end if;
    end process;
    
    SB_addr <= STD_LOGIC_VECTOR(SB_last_addr - word_cnt);
    
    --============--
    -- CMI OUTREG --
    --============--
               
	process (clk) is
	begin
		if rising_edge(clk) then
			
            if out_next = '1' then
                out_vld <= out_vld_int;
                if out_sel = '1' then
                    out_data <= SB_addr & SB_data((TO_INTEGER(word_cnt)+1)*out_d_size - 1 downto TO_INTEGER(word_cnt)*out_d_size);
                else
                    out_data <= inp_data(inp_addr_MSB downto inp_addr_LSB) & inp_data(inp_d_MSB downto inp_d_LSB);
                end if;
			end if;
			
		end if;
	end process;

    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        inp_next    <= out_next OR NOT inp_vld;
        out_vld_int <= inp_vld;
        -- select output 
        out_sel     <= '0';
        -- SB Reset
        SB_rst      <= '0';
        -- Word Cnt
        word_rst    <= '0';
        word_dec    <= '0';
        
        
        case present_state is
            
            --===============--
            when idle =>
            --===============--
            
                if pbl = '1' then
                    next_state  <= send_sb;
                    inp_next    <= NOT inp_vld;
                    out_vld_int <= '0';
                else
                    next_state  <= idle;
                end if;
                
            --===============--
            when send_sb =>
            --===============--
            
                inp_next    <= NOT inp_vld;
                out_sel     <= '1';
                if out_next = '1' then
                    out_vld_int  <= '1';
                    if word_cnt = 0 then
                        word_rst        <= '1';
                        SB_rst          <= '1';
                        next_state      <= idle;
                    else
                        word_dec        <= '1';
                        next_state      <= send_sb;
                    end if;
                else
                    next_state <= send_sb;
                end if;
                
            --===============--
            when others =>
            --===============--
                    
                next_state <= idle;
            
        end case;
    end process;
    
end architecture struct;
