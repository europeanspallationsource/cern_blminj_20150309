------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2014
Module Name:    param_table_CMA


-----------------
Short Description
-----------------

   This module implements register table (table_size x inp_data_size). It sotres incoming data one after the other in register
   cells, which are accessible via the params output.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    inp_data_size   := size of the CMI input data port
    table_size      := size of the table (number of registers)
    
    table_vsize     := vector size of the table_cnt
    
--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity param_table_CMA is

generic
(
    inp_data_size:  integer;
    table_size:     integer
    
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- Output 
    params:         out slv_array(0 to table_size-1)(inp_data_size-1 downto 0) 
);

end entity param_table_CMA;


architecture struct of param_table_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant table_vsize:       integer := get_vsize(table_size);
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Table Counter
    signal table_cnt:           UNSIGNED(table_vsize-1 downto 0) := (others => '0');
    signal table_rst:           std_logic;
    signal table_inc:           std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal param_table:         slv_array(0 to table_size-1)(inp_data_size-1 downto 0) := (others => (others => '0'));
    
BEGIN   
  
    --===============--
    -- COMBINATORICS --
    --===============--  
  
    inp_next    <= '1';
    params      <= param_table;
     
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if table_cnt = table_size-1 then
                table_cnt <= (others => '0');
            elsif inp_vld = '1' then
                table_cnt <= table_cnt + 1;
            end if;
            
        end if;
    end process;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if inp_vld = '1' then
                param_table(TO_INTEGER(table_cnt)) <= inp_data;
            end if;
            
        end if;
    end process;
    
end architecture struct;
