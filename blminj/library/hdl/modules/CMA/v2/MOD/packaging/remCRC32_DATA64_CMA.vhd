------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    remCRC32_DATA64_CMA

-----------------
Short Description
-----------------

    This module removes the header from a package (determined by the sop and size of header) and removes the sop/eop bits from the data.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    data_size           := size of input/output data
    header_size         := total size of the header (in bit)
    data_slice_n        := number of data slices in the package
    
    act_data_size       := size of the actual data part (without the 2 extra bits)
    header_slice_n      := number of header slices in the package
    cslice_n            := number of header and data slices combined
    cslice_n_vsize      := slv vector size for the number of combined slices
    
--------------
Implementation
--------------
    

    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity remCRC32_DATA64_CMA is

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(65 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(65 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity remCRC32_DATA64_CMA;


architecture struct of remCRC32_DATA64_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
  
    constant data_size:         integer := 66; 
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal reg_data:        std_logic_vector(data_size-1 downto 0) := (others => '0');
    signal reg_vld:         std_logic := '0';
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal eop:             std_logic;
    signal new_eop:         std_logic;
    signal reg_en:          std_logic;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal out_vld_int:     std_logic;
    signal reg_vld_int:     std_logic;
    
    
BEGIN

    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    eop <= inp_data(64);
    
    
    --================--
    -- REGISTER STEPS --
    --================--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if reg_en = '1' then
                reg_vld     <= reg_vld_int;
                reg_data    <= inp_data;
            end if;
                    
        end if;
    end process;

    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_vld <= out_vld_int;
                if new_eop = '1' then
                    out_data <= "01" & reg_data(63 downto 0);
                else
                    out_data <= reg_data;
                end if;
                
            end if;
                    
        end if;
    end process;
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    -- Front of Reg
    
    inp_next        <= reg_en OR NOT inp_vld; -- Inv Filter
    
    reg_vld_int     <=  '0' when inp_vld = '1' AND reg_vld = '1' AND eop = '1' else
                        inp_vld;
                        
    reg_en          <= (inp_vld OR NOT reg_vld) AND (out_next OR NOT reg_vld); -- first part: normal condition/second part: Inv Filter
    
    -- Front of Output Reg
    
    out_vld_int     <=  '0' when inp_vld = '0' AND reg_vld = '1' else
                        reg_vld;
                        
    new_eop         <= reg_vld AND eop;
    
    
    
end architecture struct;
