------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/09/2013
Module Name:    appCRC32_DATA16_CMA

-----------------
Short Description
-----------------

    This module adds a CRC32 (Ethernet) at the end of a package. The input is 18 bit (sop, eop, 16 bit of data).

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    none

--------------
Implementation
--------------
    
    The CRC circuit starts with sop = '1' and runs until eop = '1'. The input data is forwarded during the process until the input shows the last package entry (eop = '1').
    The eop bit is changed to '0' for the last data in order to end the package with the CRC. Afterwards the 32bit CRC is added at the end of the package, 
    flagging the last (LSB of the CRC) data word with eop = '1'. During this action, the input is locked (inp_next = '0').
    
-----------
Limitations
-----------
    
    incoming data is delayed by 2 CC due to the addition of the CRC to the previous package


----------------
Missing Features
----------------

    not generic (due to the CRC combinatory structure)

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity appCRC32_DATA16_CMA is

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(17 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(17 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity appCRC32_DATA16_CMA;


architecture struct of appCRC32_DATA16_CMA is
    
    --======--
    -- FSMS --
    --======--
    
    type crc_state_t is (fwd_data, append_crc);
    signal crc_present_state, crc_next_state: crc_state_t := fwd_data;

    --===========--
    -- REGISTERS --
    --===========--
    
    signal crc_high:        std_logic := '1';
    signal crc_out_reg:     std_logic_vector(31 downto 0) := X"00000000";
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal crc_high_tgl:    std_logic;
    signal crc_out:         std_logic_vector(31 downto 0);
    signal crc_in:          std_logic_vector(31 downto 0);
    signal d:               std_logic_vector(15 downto 0);
    signal sop:             std_logic;
    signal eop:             std_logic;
    signal app_crc:         std_logic;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal inp_next_int:    std_logic;
    signal out_vld_int:     std_logic;
    
BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    -- Next
    inp_next <= inp_next_int OR NOT inp_vld;
    
    -- General
    sop <= inp_data(17);
    eop <= inp_data(16);
    d   <= inp_data(15 downto 0);

    -- CRC
    crc_in <=   X"00000000" when sop = '1' else
                crc_out_reg;
    
    -- CRC Parallel Algorithm
    crc_out(0)  <= d(0) XOR d(6) XOR d(9) XOR d(10) XOR crc_in(16) XOR crc_in(26) XOR crc_in(25) XOR d(12) XOR crc_in(22) XOR crc_in(28); 
    crc_out(1)  <= d(0) XOR d(1) XOR d(7) XOR d(11) XOR crc_in(17) XOR crc_in(27) XOR d(13) XOR crc_in(23) XOR crc_in(29) XOR d(6) XOR d(9) XOR crc_in(16) XOR crc_in(25) XOR d(12) XOR crc_in(22) XOR crc_in(28); 
    crc_out(2)  <= d(0) XOR d(1) XOR d(2) XOR d(8) XOR crc_in(18) XOR d(14) XOR crc_in(24) XOR crc_in(30) XOR d(7) XOR crc_in(17) XOR d(13) XOR crc_in(23) XOR crc_in(29) XOR d(6) XOR d(9) XOR crc_in(16) XOR crc_in(25) XOR crc_in(22); 
    crc_out(3)  <= d(1) XOR d(2) XOR d(3) XOR d(9) XOR crc_in(19) XOR d(15) XOR crc_in(25) XOR crc_in(31) XOR d(8) XOR crc_in(18) XOR d(14) XOR crc_in(24) XOR crc_in(30) XOR d(7) XOR d(10) XOR crc_in(17) XOR crc_in(26) XOR crc_in(23); 
    crc_out(4)  <= d(0) XOR d(2) XOR d(3) XOR d(4) XOR crc_in(20) XOR crc_in(19) XOR d(15) XOR crc_in(31) XOR d(8) XOR d(11) XOR crc_in(18) XOR crc_in(27) XOR crc_in(24) XOR d(6) XOR crc_in(16) XOR d(12) XOR crc_in(22) XOR crc_in(28); 
    crc_out(5)  <= d(0) XOR d(1) XOR d(3) XOR d(4) XOR d(5) XOR crc_in(21) XOR crc_in(20) XOR crc_in(19) XOR d(7) XOR crc_in(17) XOR d(13) XOR crc_in(23) XOR crc_in(29) XOR d(6) XOR d(10) XOR crc_in(16) XOR crc_in(26) XOR crc_in(22); 
    crc_out(6)  <= d(1) XOR d(2) XOR d(4) XOR d(5) XOR d(6) XOR crc_in(22) XOR crc_in(21) XOR crc_in(20) XOR d(8) XOR crc_in(18) XOR d(14) XOR crc_in(24) XOR crc_in(30) XOR d(7) XOR d(11) XOR crc_in(17) XOR crc_in(27) XOR crc_in(23); 
    crc_out(7)  <= d(0) XOR d(2) XOR d(3) XOR d(5) XOR d(7) XOR crc_in(23) XOR crc_in(21) XOR crc_in(19) XOR d(15) XOR crc_in(31) XOR d(8) XOR crc_in(18) XOR crc_in(24) XOR d(10) XOR crc_in(16) XOR crc_in(26); 
    crc_out(8)  <= d(0) XOR d(1) XOR d(3) XOR d(4) XOR d(8) XOR crc_in(24) XOR crc_in(20) XOR crc_in(19) XOR d(11) XOR crc_in(17) XOR crc_in(27) XOR d(10) XOR crc_in(16) XOR crc_in(26) XOR d(12) XOR crc_in(28); 
    crc_out(9)  <= d(1) XOR d(2) XOR d(4) XOR d(5) XOR d(9) XOR crc_in(25) XOR crc_in(21) XOR crc_in(20) XOR d(12) XOR crc_in(18) XOR crc_in(28) XOR d(11) XOR crc_in(17) XOR crc_in(27) XOR d(13) XOR crc_in(29); 
    crc_out(10) <= d(0) XOR d(2) XOR d(3) XOR d(5) XOR crc_in(21) XOR d(13) XOR crc_in(19) XOR crc_in(29) XOR crc_in(18) XOR d(14) XOR crc_in(30) XOR d(9) XOR crc_in(16) XOR crc_in(25); 
    crc_out(11) <= d(0) XOR d(1) XOR d(3) XOR d(4) XOR d(14) XOR crc_in(20) XOR crc_in(30) XOR crc_in(19) XOR d(15) XOR crc_in(31) XOR crc_in(17) XOR d(9) XOR crc_in(16) XOR crc_in(25) XOR d(12) XOR crc_in(28); 
    crc_out(12) <= d(0) XOR d(1) XOR d(2) XOR d(4) XOR d(5) XOR d(15) XOR crc_in(21) XOR crc_in(31) XOR crc_in(20) XOR crc_in(18) XOR crc_in(17) XOR d(13) XOR crc_in(29) XOR d(6) XOR d(9) XOR crc_in(16) XOR crc_in(25) XOR d(12) XOR crc_in(22) XOR crc_in(28); 
    crc_out(13) <= d(1) XOR d(2) XOR d(3) XOR d(5) XOR d(6) XOR crc_in(22) XOR crc_in(21) XOR crc_in(19) XOR crc_in(18) XOR d(14) XOR crc_in(30) XOR d(7) XOR d(10) XOR crc_in(17) XOR crc_in(26) XOR d(13) XOR crc_in(23) XOR crc_in(29); 
    crc_out(14) <= d(2) XOR d(3) XOR d(4) XOR d(6) XOR d(7) XOR crc_in(23) XOR crc_in(22) XOR crc_in(20) XOR crc_in(19) XOR d(15) XOR crc_in(31) XOR d(8) XOR d(11) XOR crc_in(18) XOR crc_in(27) XOR d(14) XOR crc_in(24) XOR crc_in(30); 
    crc_out(15) <= d(3) XOR d(4) XOR d(5) XOR d(7) XOR d(8) XOR crc_in(24) XOR crc_in(23) XOR crc_in(21) XOR crc_in(20) XOR d(9) XOR d(12) XOR crc_in(19) XOR crc_in(28) XOR d(15) XOR crc_in(25) XOR crc_in(31); 
    crc_out(16) <= d(0) XOR d(4) XOR d(5) XOR d(8) XOR crc_in(0) XOR crc_in(24) XOR crc_in(21) XOR d(13) XOR crc_in(20) XOR crc_in(29) XOR crc_in(16) XOR d(12) XOR crc_in(28); 
    crc_out(17) <= d(1) XOR d(5) XOR d(6) XOR d(9) XOR crc_in(1) XOR crc_in(25) XOR crc_in(22) XOR d(14) XOR crc_in(21) XOR crc_in(30) XOR crc_in(17) XOR d(13) XOR crc_in(29); 
    crc_out(18) <= d(2) XOR d(6) XOR d(7) XOR d(10) XOR crc_in(2) XOR crc_in(26) XOR crc_in(23) XOR d(15) XOR crc_in(22) XOR crc_in(31) XOR crc_in(18) XOR d(14) XOR crc_in(30); 
    crc_out(19) <= d(3) XOR d(7) XOR d(8) XOR d(11) XOR crc_in(3) XOR crc_in(27) XOR crc_in(24) XOR crc_in(23) XOR crc_in(19) XOR d(15) XOR crc_in(31); 
    crc_out(20) <= d(4) XOR d(8) XOR d(9) XOR d(12) XOR crc_in(4) XOR crc_in(28) XOR crc_in(25) XOR crc_in(24) XOR crc_in(20); 
    crc_out(21) <= d(5) XOR d(9) XOR d(10) XOR d(13) XOR crc_in(5) XOR crc_in(29) XOR crc_in(26) XOR crc_in(25) XOR crc_in(21); 
    crc_out(22) <= d(0) XOR d(11) XOR d(14) XOR crc_in(6) XOR crc_in(30) XOR crc_in(27) XOR d(9) XOR crc_in(16) XOR crc_in(25) XOR d(12) XOR crc_in(28); 
    crc_out(23) <= d(0) XOR d(1) XOR d(15) XOR crc_in(7) XOR crc_in(31) XOR crc_in(17) XOR d(13) XOR crc_in(29) XOR d(6) XOR d(9) XOR crc_in(16) XOR crc_in(25) XOR crc_in(22); 
    crc_out(24) <= d(1) XOR d(2) XOR crc_in(8) XOR crc_in(18) XOR d(14) XOR crc_in(30) XOR d(7) XOR d(10) XOR crc_in(17) XOR crc_in(26) XOR crc_in(23); 
    crc_out(25) <= d(2) XOR d(3) XOR crc_in(9) XOR crc_in(19) XOR d(15) XOR crc_in(31) XOR d(8) XOR d(11) XOR crc_in(18) XOR crc_in(27) XOR crc_in(24); 
    crc_out(26) <= d(0) XOR d(3) XOR d(4) XOR crc_in(10) XOR crc_in(20) XOR crc_in(19) XOR d(6) XOR d(10) XOR crc_in(16) XOR crc_in(26) XOR crc_in(22); 
    crc_out(27) <= d(1) XOR d(4) XOR d(5) XOR crc_in(11) XOR crc_in(21) XOR crc_in(20) XOR d(7) XOR d(11) XOR crc_in(17) XOR crc_in(27) XOR crc_in(23); 
    crc_out(28) <= d(2) XOR d(5) XOR d(6) XOR crc_in(12) XOR crc_in(22) XOR crc_in(21) XOR d(8) XOR d(12) XOR crc_in(18) XOR crc_in(28) XOR crc_in(24); 
    crc_out(29) <= d(3) XOR d(6) XOR d(7) XOR crc_in(13) XOR crc_in(23) XOR crc_in(22) XOR d(9) XOR d(13) XOR crc_in(19) XOR crc_in(29) XOR crc_in(25); 
    crc_out(30) <= d(4) XOR d(7) XOR d(8) XOR crc_in(14) XOR crc_in(24) XOR crc_in(23) XOR d(10) XOR d(14) XOR crc_in(20) XOR crc_in(30) XOR crc_in(26); 
    crc_out(31) <= d(5) XOR d(8) XOR d(9) XOR crc_in(15) XOR crc_in(25) XOR crc_in(24) XOR d(11) XOR d(15) XOR crc_in(21) XOR crc_in(31) XOR crc_in(27); 
        
    
    --===========--
    -- REGISTERS --
    --===========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            if inp_vld = '1' AND inp_next_int = '1' then
                crc_out_reg <= crc_out;
            end if;
        end if;
        
    end process;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            if crc_high_tgl = '1' then
                crc_high <= NOT crc_high;
            end if;
        end if;
        
    end process;
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_vld <= out_vld_int;
                if app_crc = '1' then
                    if crc_high = '1' then
                        out_data <= "00" & crc_out(31 downto 16);
                    else
                        out_data <= "01" & crc_out(15 downto 0); -- adding eop
                    end if;
                else
                    if eop = '1' then
                        out_data <= "00" & inp_data(15 downto 0);
                    else
                        out_data <= inp_data;
                    end if;
                end if;
            end if;
                    
        end if;
    end process;
     
    --=============--
    -- CRC CONTROL --
    --=============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            crc_present_state <= crc_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        out_vld_int     <= '0';
        app_crc         <= '0';
        inp_next_int    <= out_next;
        crc_high_tgl    <= '0';
        
        case crc_present_state is
            
            
            --===============--
            when fwd_data =>
            --===============--
                
                if out_next = '1' then
                    out_vld_int <= inp_vld;
                    if eop = '1' then
                        crc_next_state <= append_crc;
                    else
                        crc_next_state <= fwd_data;
                    end if;
                else
                    crc_next_state <= fwd_data;
                end if;
            
            --===============--
            when append_crc =>
            --===============--
            
                app_crc         <= '1';
                inp_next_int    <= '0';
                
                if out_next = '1' then
                    crc_high_tgl <= '1';
                    out_vld_int  <= '1';
                    if crc_high = '1' then
                        inp_next_int   <= '0';
                        crc_next_state <= append_crc;
                    else
                        crc_next_state <= fwd_data;
                    end if;                     
                else
                    inp_next_int    <= '0';
                    crc_next_state  <= append_crc;
                end if;
            
        end case;
    end process;
  
    
    
end architecture struct;
