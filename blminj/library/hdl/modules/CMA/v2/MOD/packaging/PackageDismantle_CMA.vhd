------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/12/2014
Module Name:    PackageDismantle_CMA

-----------------
Short Description
-----------------

    This chain removes the CRC from a package and afterwards unpackages it.

------------
Dependencies
------------

    remCRC32_CMA
    unpackage_CMA
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the package input data (e.g. 18 bits, inkl sop/eop)
    out_data_size       := size of the non-packaged output data (e.g. 16 bit)
    data_slice_n        := number of data slices inside the package
    header_size         := full size of the header (not the number of slices)
    
--------------
Implementation
--------------
    
    detailed description of the actual implementation
    

-----------
Limitations
-----------
    
   
----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity PackageDismantle_CMA is

generic
(
    inp_data_size:      integer;
    out_data_size:      integer;
    data_slice_n:       integer;
    header_size:        integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(out_data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity PackageDismantle_CMA;


architecture struct of PackageDismantle_CMA is
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    constant remCRC32_data_size:  integer := inp_data_size;
    constant remCRC32_rx_number:  integer := 1;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- remCRC32 CMI
    signal remCRC32_tx_data:    std_logic_vector(inp_data_size-1 downto 0);
    signal remCRC32_tx_vld:     std_logic;
    signal remCRC32_tx_next:    std_logic;
    signal remCRC32_rx_data:    slv_array(0 downto 0)(inp_data_size-1 downto 0);
    signal remCRC32_rx_vld:     std_logic_vector(0 downto 0);
    signal remCRC32_rx_next:    std_logic_vector(0 downto 0);
    
    
    
BEGIN
    
    
    --==============--
    -- remCRC32 CMA --
    --==============--
    
    remCRC32_CMA_inst : entity work.remCRC32_CMA 
    generic map
    (
        data_size       => inp_data_size,
        header_size     => header_size,
        data_slice_n    => data_slice_n
    )
    port map 
    (
        clk             => clk,
        inp_data        => inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => remCRC32_tx_data,
        out_vld         => remCRC32_tx_vld,
        out_next        => remCRC32_tx_next
    );
    
    --==============--
    -- remCRC32 CMI --
    --==============--
    
    remCRC32_CMI : entity work.CMI 
    generic map
    (
        data_size   => remCRC32_data_size,
        rx_number   => remCRC32_rx_number
    )
    port map 
    (
        clk         => clk,
        tx_data     => remCRC32_tx_data,
        tx_vld      => remCRC32_tx_vld,
        tx_next     => remCRC32_tx_next,
        rx_data     => remCRC32_rx_data,
        rx_vld      => remCRC32_rx_vld,
        rx_next     => remCRC32_rx_next
    );
    
    --===============--
    -- UNPACKAGE_CMA --
    --===============--
    
    unpackage_CMA_inst : entity work.unpackage_CMA
    generic map
    (
        inp_data_size   => inp_data_size,
        out_data_size   => out_data_size,
        header_size     => header_size
    )
    port map 
    (
        clk             => clk,
        inp_data        => remCRC32_rx_data(0),
        inp_vld         => remCRC32_rx_vld(0),
        inp_next        => remCRC32_rx_next(0),
        out_data        => out_data,
        out_vld         => out_vld,
        out_next        => out_next
    );
    

end architecture struct;
