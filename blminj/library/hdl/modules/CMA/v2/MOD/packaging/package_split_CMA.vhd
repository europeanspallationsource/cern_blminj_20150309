------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    package_split_CMA

-----------------
Short Description
-----------------

    This module splits the package into a header part and a body part.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the input (must have 2 upppermost bits (sop,eop))
    header_data_size    := size of the header output (has to be inp_data_size-2)
    body_data_size      := size of the body output (has to be inp_data_size-2)
    header_size         := total size of the header (in bit)

    
--------------
Implementation
--------------
    

    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------

    external parameter update 

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity package_split_CMA is

generic
(
    inp_data_size:      integer;
    header_data_size:   integer;
    body_data_size:     integer;
    header_size:        integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    body_data:      out std_logic_vector(body_data_size-1 downto 0);
    body_vld:       out std_logic := '0';
    body_next:      in  std_logic;
    header_data:    out std_logic_vector(header_data_size-1 downto 0);
    header_vld:     out std_logic := '0';
    header_next:    in  std_logic
    
);

end entity package_split_CMA;


architecture struct of package_split_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- number of header slices
    constant hslice_n:          integer := INTEGER(CEIL(REAL(header_size)/REAL(header_data_size)));
    constant hslice_n_vsize:    integer := get_vsize(hslice_n);
    
    --======--
    -- FSMs --
    --======--
    
    type state_t is (fwd_header, fwd_data);
    signal present_state, next_state: state_t := fwd_data;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Header Slice Counter
    signal hslice_cnt:      UNSIGNED(hslice_n_vsize-1 downto 0) := TO_UNSIGNED(hslice_n-1, hslice_n_vsize);
    signal hslice_rst:      std_logic;
    signal hslice_dec:      std_logic;
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal sop:             std_logic;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal body_vld_int:    std_logic;
    signal header_vld_int:  std_logic;
    
BEGIN

    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    sop <= inp_data(inp_data_size-1);
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if hslice_rst= '1' then
                hslice_cnt <= TO_UNSIGNED(hslice_n-1, hslice_n_vsize);
            elsif hslice_dec = '1' then
                hslice_cnt <= hslice_cnt - 1;
            end if;
            
        end if;
    end process;
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if body_next = '1' then
                body_data   <= inp_data(body_data_size-1 downto 0);
                body_vld    <= body_vld_int;
            end if;
            
            if header_next = '1' then
                header_data <= inp_data(header_data_size-1 downto 0);
                header_vld  <= header_vld_int;
            end if;
                     
        end if;
    end process;
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        inp_next        <= body_next OR NOT inp_vld;
        body_vld_int    <= inp_vld;
        header_vld_int  <= '0';
        -- Counter
        hslice_dec      <= '0';
        hslice_rst      <= '0';
        
        case present_state is
            
            --===============--
            when fwd_data =>
            --===============--
                
                next_state <= fwd_data;
                
                if inp_vld = '1' AND sop = '1' then
                    inp_next      <= header_next OR NOT inp_vld;
                    body_vld_int  <= '0';
                    next_state    <= fwd_header;
                    
                    if header_next = '1' then
                        header_vld_int <= '1';
                        if hslice_cnt = 0 then
                            hslice_rst <= '1';
                            next_state <= fwd_data;
                        else
                            hslice_dec <= '1';
                            next_state <= fwd_header;
                        end if;
                    end if;
                end if;
                           
            --===============--
            when fwd_header =>
            --===============--
                
                inp_next      <= header_next OR NOT inp_vld;
                body_vld_int  <= '0';
                next_state    <= fwd_header;
                if header_next = '1' then
                    header_vld_int <= '1';
                    if hslice_cnt = 0 then
                        hslice_rst <= '1';
                        next_state <= fwd_data;
                    else
                        hslice_dec <= '1';
                        next_state <= fwd_header;
                    end if;
                end if;
                    
        end case;
    end process;
  

end architecture struct;
