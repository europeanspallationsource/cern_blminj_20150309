------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    package_append_CMA

-----------------
Short Description
-----------------

    This module transforms a package format (header + body) with a smaller data size (e.g. 16+2 bits) into
    one with a bigger data size (e.g. 64+2).
    
------------
Dependencies
------------

    vhdl_func_pkg
    append
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the input package (incl. 2 upppermost bits (sop,eop))
    out_data_size       := size of the output package (incl. 2 upppermost bits (sop,eop))
    header_size         := total size of the header (in bit)
    
--------------
Implementation
--------------
    

    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------

    external parameter update 

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity package_append_CMA is

generic
(
    inp_data_size:      integer;
    out_data_size:      integer;
    header_size:        integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(out_data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic 
);

end entity package_append_CMA;


architecture struct of package_append_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- internal data size (without sop/eop)
    constant inp_data_size_int:     integer := inp_data_size-2;
    constant out_data_size_int:     integer := out_data_size-2;
     
    -- number of header slices
    constant hslice_n:              integer := INTEGER(CEIL(REAL(header_size)/REAL(inp_data_size)));
    constant hslice_n_vsize:        integer := get_vsize(hslice_n);
    
    --======--
    -- FSMs --
    --======--
    
    type state_t is (send_header, send_body);
    signal present_state, next_state: state_t := send_header;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Header Slice Counter
    signal hslice_cnt:              UNSIGNED(hslice_n_vsize-1 downto 0) := TO_UNSIGNED(hslice_n-1, hslice_n_vsize);
    signal hslice_rst:              std_logic;
    signal hslice_dec:              std_logic;
    
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal sop_reg:                 std_logic := '0';
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal sop:                     std_logic;
    signal eop:                     std_logic;
    signal pkg_bits:                std_logic_vector(1 downto 0);
    signal sop_rst:                 std_logic;
    
    signal headapp_out_data:        std_logic_vector(out_data_size_int-1 downto 0);
    signal headapp_sync_rst:        std_logic;
    signal headapp_full_out_rdy:    std_logic;
    signal headapp_smpl:            std_logic;
    
    signal bodyapp_out_data:        std_logic_vector(out_data_size_int-1 downto 0);
    signal bodyapp_sync_rst:        std_logic;
    signal bodyapp_full_out_rdy:    std_logic;
    signal bodyapp_smpl:            std_logic;
    
    signal header_sel:              std_logic;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal out_vld_int:             std_logic;
    
BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    sop <= inp_data(inp_data_size-1);
    eop <= inp_data(inp_data_size-2);

    pkg_bits <= sop_reg & eop;
    
    --==========--
    -- REGISTER --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if sop_rst = '1' then
                sop_reg <= '0';
            elsif sop = '1' then
                sop_reg <= '1';
            end if;
        end if;
    end process;
    
    
    --================--
    -- APPEND MODULES --
    --================--
    
    header_append_inst: entity work.append
    GENERIC MAP
    (
        inp_data_size   => inp_data_size_int,
        out_data_size   => out_data_size_int
    )
    PORT MAP
    (
        clk             => clk,
        inp_data        => inp_data(inp_data_size_int-1 downto 0),
        out_data        => headapp_out_data,
        sync_rst        => headapp_sync_rst,
        full_out_rdy    => headapp_full_out_rdy,
        smpl            => headapp_smpl
    );
    
    body_append_inst: entity work.append
    GENERIC MAP
    (
        inp_data_size   => inp_data_size_int,
        out_data_size   => out_data_size_int
    )
    PORT MAP
    (
        clk             => clk,
        inp_data        => inp_data(inp_data_size_int-1 downto 0),
        out_data        => bodyapp_out_data,
        sync_rst        => bodyapp_sync_rst,
        full_out_rdy    => bodyapp_full_out_rdy,
        smpl            => bodyapp_smpl
    );
    
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if hslice_rst= '1' then
                hslice_cnt <= TO_UNSIGNED(hslice_n-1, hslice_n_vsize);
            elsif hslice_dec = '1' then
                hslice_cnt <= hslice_cnt - 1;
            end if;
            
        end if;
    end process;
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                if header_sel = '1' then
                    out_data <= pkg_bits & headapp_out_data;
                else
                    out_data <= pkg_bits & bodyapp_out_data; 
                end if;
                out_vld <= out_vld_int;
            end if;
                     
        end if;
    end process;
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        inp_next            <= '1';
        out_vld_int         <= '0';
        -- Others
        header_sel          <= '0';
        -- Append Modules
        headapp_smpl        <= '0';
        headapp_sync_rst    <= '0';
        bodyapp_smpl        <= '0';
        bodyapp_sync_rst    <= '0';
        -- Package
        sop_rst             <= '0';
        -- Counter
        hslice_dec          <= '0';
        hslice_rst          <= '0';
        
        case present_state is
            
            --===============--
            when send_header =>
            --===============--
                
                next_state  <= send_header;
                header_sel  <= '1';
                
                if inp_vld = '1' then
                    headapp_smpl <= '1';    
                    -- Option 1: append-submodule is "full"
                    if headapp_full_out_rdy = '1' then
                        if out_next = '1' then
                            out_vld_int         <= '1';
                            headapp_sync_rst    <= '1';
                            sop_rst             <= '1';
                            if hslice_cnt = 0 then
                                hslice_rst <= '0';
                                next_state <= send_body;
                            else
                                hslice_dec <= '1';
                            end if;                        
                        end if;
                    -- Option 2: no more header slices left
                    else
                        if hslice_cnt = 0 then
                            if out_next = '1' then
                                out_vld_int         <= '1';
                                headapp_sync_rst    <= '1';
                                hslice_rst          <= '0';
                                sop_rst             <= '1';
                                next_state          <= send_body;
                            end if;
                        else
                            hslice_dec <= '1';
                        end if;                        
                    end if;   
                end if;
                           
            --===============--
            when send_body =>
            --===============--
            
                next_state  <= send_body;
                
                if inp_vld = '1' then
                    bodyapp_smpl <= '1';
                    -- Either End of Package reached ...
                    if eop = '1' then
                        if out_next = '1' then
                            out_vld_int         <= '1';
                            bodyapp_sync_rst    <= '1';
                            next_state          <= send_header;
                        end if;
                    -- or append-submodule is "full"
                    elsif bodyapp_full_out_rdy = '1' then
                        if out_next = '1' then
                            out_vld_int         <= '1';
                            bodyapp_sync_rst    <= '1';                  
                        end if;
                    end if;  
                end if;
        end case;
    end process;
  

end architecture struct;
