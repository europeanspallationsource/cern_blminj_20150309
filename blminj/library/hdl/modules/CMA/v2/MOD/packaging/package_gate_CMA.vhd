------------------------------
/*
Company:       CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/02/2014
Module Name:    package_gate_CMA

    
-----------------
Short Description
-----------------

    This CMA module forwards a package, as long as stop is not 1. When stop goes to 1, it finishes forwarding the current package,
    until a next package arrives.


------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    data_size     := size of input and output bus 
    
--------------
Implementation
--------------
    
     
-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/      
------------------------


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

entity package_gate_CMA is

generic
(
    data_size:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Control 
    stop:           in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity package_gate_CMA;


architecture struct of package_gate_CMA is
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal sop:             std_logic;
    
    --=============--
    -- CMA SIGNALS --
    --=============--

    signal out_vld_int:     std_logic;
    
BEGIN
    
    sop <= inp_data(data_size-1);
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if out_next = '1' then
                out_data    <= inp_data;
                out_vld     <= out_vld_int;
            end if;
        end if;
    end process;
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        inp_next    <= out_next OR NOT inp_vld;
        out_vld_int <= inp_vld;
        
        if stop = '1' AND inp_vld = '1' AND sop = '1' then
            inp_next    <= '0';
            out_vld_int <= '0';
        end if;
      
    end process;
  

    
end architecture struct;