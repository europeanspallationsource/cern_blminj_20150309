------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    unpackage_CMA

-----------------
Short Description
-----------------

    This module removes the header from a package (determined by the sop and size of header) and removes the sop/eop bits from the data.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    inp_data_size       := out_data_size + 2 bits (sop,eop)
    out_data_size       := size of output data
    header_size         := total size of the header (in bit)

    
--------------
Implementation
--------------

-----------
Limitations
-----------

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity unpackage_CMA is

generic
(
    inp_data_size:      integer;
    out_data_size:      integer;
    header_size:        integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(out_data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
    
);

end entity unpackage_CMA;


architecture struct of unpackage_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- number of header slices
    constant hslice_n:          integer := INTEGER(CEIL(REAL(header_size)/REAL(out_data_size)));
    constant hslice_n_vsize:    integer := get_vsize(hslice_n);
    
    --======--
    -- FSMs --
    --======--
    
    type unpkg_state_t is (del_header, fwd_data);
    signal unpkg_present_state, unpkg_next_state: unpkg_state_t := fwd_data;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Header Slice Counter
    signal hslice_cnt:      UNSIGNED(hslice_n_vsize-1 downto 0) := TO_UNSIGNED(hslice_n-1, hslice_n_vsize);
    signal hslice_rst:      std_logic;
    signal hslice_dec:      std_logic;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal sop:             std_logic;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal out_vld_int:     std_logic;
    
    
BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    sop <= inp_data(inp_data_size-1);
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if hslice_rst= '1' then
                hslice_cnt <= TO_UNSIGNED(hslice_n-1, hslice_n_vsize);
            elsif hslice_dec = '1' then
                hslice_cnt <= hslice_cnt - 1;
            end if;
            
        end if;
    end process;

    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_data    <= inp_data(out_data_size-1 downto 0);
                out_vld     <= out_vld_int;
            end if;
                    
        end if;
    end process;
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            unpkg_present_state <= unpkg_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        inp_next        <= out_next OR NOT inp_vld;
        out_vld_int     <= inp_vld;
        -- Counter
        hslice_dec      <= '0';
        hslice_rst      <= '0';
        
        case unpkg_present_state is
            
            --===============--
            when fwd_data =>
            --===============--
                
                unpkg_next_state <= fwd_data;
                if inp_vld = '1' AND sop = '1' then
                    inp_next        <= '1';
                    out_vld_int     <= '0';
                    if hslice_cnt = 0 then
                        hslice_rst          <= '1';
                        unpkg_next_state    <= fwd_data;
                    else
                        hslice_dec          <= '1';
                        unpkg_next_state    <= del_header;
                    end if;
                end if;
                           
            --===============--
            when del_header =>
            --===============--
                
                inp_next        <= '1';
                out_vld_int     <= '0';
                if hslice_cnt = 0 then
                    hslice_rst          <= '1';
                    unpkg_next_state    <= fwd_data;
                else
                    hslice_dec          <= '1';
                    unpkg_next_state    <= del_header;
                end if;
                    
        end case;
    end process;
  
    
    
end architecture struct;
