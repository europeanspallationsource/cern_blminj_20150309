------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    19/11/2013
Module Name:    PackageCreator64_CMA

-----------------
Short Description
-----------------

    Takes 64-bit wide data, packages and adds a CRC to it.

------------
Dependencies
------------

    package_CMA
    appCRC32_DATA64_CMA
    
------------------
Generics/Constants
------------------

    pkg_size            := number of input data per package (without header/CRC)
    header_size         := total size of the header (in bit)
    
    inp_data_size       := size of the input data (64 bit)
    out_data_size       := size of the package output data (66 bits, inkl sop/eop)
   
--------------
Implementation
--------------
    
    detailed description of the actual implementation
    

-----------
Limitations
-----------
    

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity PackageCreator64_CMA is

generic
(
    pkg_size:           integer;
    header_size:        integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Parameters
    PARAM_header:   in  std_logic_vector(header_size-1 downto 0);
    -- Status
    pkg_done:       out std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(63 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(65 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity PackageCreator64_CMA;


architecture struct of PackageCreator64_CMA is
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    constant inp_data_size:     integer := 64;
    constant out_data_size:     integer := 66;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal package_tx_data:     std_logic_vector(out_data_size-1 downto 0);
    signal package_tx_vld:      std_logic;
    signal package_tx_next:     std_logic;
    signal package_rx_data:     slv_array(0 downto 0)(out_data_size-1 downto 0);
    signal package_rx_vld:      std_logic_vector(0 downto 0);
    signal package_rx_next:     std_logic_vector(0 downto 0);
    
    
BEGIN
    
    
    --=============--
    -- PACKAGE_CMA --
    --=============--
    
    package_CMA_inst : entity work.package_CMA 
    generic map
    (
        inp_data_size   => inp_data_size,
        pkg_size        => pkg_size,
        header_size     => header_size
    )
    port map 
    (
        clk             => clk,
        PARAM_header    => PARAM_header,
        pkg_done        => pkg_done,
        inp_data        => inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => package_tx_data,
        out_vld         => package_tx_vld,
        out_next        => package_tx_next
    );
    
    --=====--
    -- CMI --
    --=====--
    
    package_CMI: entity work.CMI
    generic map
    (
        rx_number   => 1,
        data_size   => out_data_size
    )
    port map 
    (
        clk         => clk,
        tx_data     => package_tx_data,
        tx_vld      => package_tx_vld,
        tx_next     => package_tx_next,
        rx_data     => package_rx_data,
        rx_vld      => package_rx_vld,
        rx_next     => package_rx_next
    );
    
    --=========--
    -- CRC_CMA --
    --=========--
    
    appCRC32_DATA64_CMA_inst : entity work.appCRC32_DATA64_CMA 
    port map 
    (
        clk             => clk,
        inp_data        => package_rx_data(0),
        inp_vld         => package_rx_vld(0),
        inp_next        => package_rx_next(0),
        out_data        => out_data,
        out_vld         => out_vld,
        out_next        => out_next
    );
    

end architecture struct;
