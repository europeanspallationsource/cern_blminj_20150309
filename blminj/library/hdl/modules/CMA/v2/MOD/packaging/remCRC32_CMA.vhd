------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    remCRC32_CMA

-----------------
Short Description
-----------------

    This module removes the header from a package (determined by the sop and size of header) and removes the sop/eop bits from the data.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    data_size           := size of input/output data
    header_size         := total size of the header (in bit)
    data_slice_n        := number of data slices in the package
    
    act_data_size       := size of the actual data part (without the 2 extra bits)
    header_slice_n      := number of header slices in the package
    cslice_n            := number of header and data slices combined
    cslice_n_vsize      := slv vector size for the number of combined slices
    
--------------
Implementation
--------------
    

    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity remCRC32_CMA is

generic
(
    data_size:          integer;
    header_size:        integer;
    data_slice_n:       integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(data_size-1 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity remCRC32_CMA;


architecture struct of remCRC32_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
  
    constant act_data_size:         integer := data_size - 2; -- without control bits
    constant header_slice_n:        integer := INTEGER(CEIL(REAL(header_size)/REAL(act_data_size)));
    
    constant cslice_n:              integer := header_slice_n + data_slice_n;
    constant cslice_n_vsize:        integer := get_vsize(cslice_n); -- everything without CRC
    
    --======--
    -- FSMs --
    --======--
    
    type remCRC_state_t is (idle, fwd_data, del_crc);
    signal remCRC_present_state, remCRC_next_state: remCRC_state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Combined Slice Counter
    signal cslice_cnt:  UNSIGNED(cslice_n_vsize-1 downto 0) := TO_UNSIGNED(cslice_n-1, cslice_n_vsize);
    signal cslice_rst:  std_logic;
    signal cslice_dec:  std_logic;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal sop:             std_logic;
    signal eop:             std_logic;
    signal new_eop:         std_logic;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal out_vld_int:     std_logic;
    
    
    
BEGIN

    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    sop <= inp_data(data_size-1);
    eop <= inp_data(data_size-2);
    

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if cslice_rst= '1' then
                cslice_cnt <= TO_UNSIGNED(cslice_n-1, cslice_n_vsize);
            elsif cslice_dec = '1' then
                cslice_cnt <= cslice_cnt - 1;
            end if;
            
        end if;
    end process;
    

    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_vld <= out_vld_int;
                if new_eop = '1' then
                    out_data <= "01" & inp_data(15 downto 0);
                else
                    out_data <= inp_data;
                end if;
                
            end if;
                    
        end if;
    end process;
    
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            remCRC_present_state <= remCRC_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        inp_next        <= out_next OR NOT inp_vld;
        out_vld_int     <= '0';
        -- Counter
        cslice_dec      <= '0';
        cslice_rst      <= '0';
        new_eop         <= '0';
        
        
        case remCRC_present_state is
            
            
            --===============--
            when idle =>
            --===============--
                
                remCRC_next_state <= idle;
                if inp_vld = '1' AND sop = '1' AND out_next = '1' then
                    out_vld_int         <= '1';
                    cslice_dec          <= '1';
                    remCRC_next_state   <= fwd_data;
                end if;
            
            --===============--
            when fwd_data =>
            --===============--
                
                remCRC_next_state <= fwd_data;
                if inp_vld = '1' AND out_next = '1' then
                    out_vld_int <= '1';
                    if cslice_cnt = 0 then
                        new_eop             <= '1';
                        cslice_rst          <= '1';
                        remCRC_next_state   <= del_crc;
                    else
                        cslice_dec          <= '1';
                        remCRC_next_state   <= fwd_data;
                    end if;
                end if;
                                    
            --===============--
            when del_crc =>
            --===============--
                
                inp_next <= '1';
                if inp_vld = '1' AND eop = '1' then
                    remCRC_next_state <= idle;
                else
                    remCRC_next_state <= del_crc;
                end if;

        end case;
    end process;
  
    
    
end architecture struct;
