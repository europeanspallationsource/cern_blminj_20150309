------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    23/09/2013
Module Name:    package_CMA

-----------------
Short Description
-----------------

    This module creates from a predefined amount of input data (pkg_size) a package and adds a header in front.
    Additional the package boundaries are defined by two additional bits (start-of-package (sop), end-of-package(eop)).
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of input/output data
    pkg_size            := number of input data per package
    header_size         := total size of the header (in bit)
   
    pkg_vsize           := size of the pkg_cnt
    hslice_n            := # of header slices (header_size/inp_data_size)
    hslice_n_vsize      := size of the hslice_cnt
    
--------------
Implementation
--------------
    
    For the header the amount of header slices are calculated and the header is stored in a register. It is composed of possible leading zeros,
    the header_content and takes the optional timestamp as LSB.
    
    The package control waits for the first word on the input, then sends out inp_next = 0 for the duration in which it publishes the header (hslice_cnt) 
    and afterwards it forwards one by one the input data until it added a number of input words equal to the predefined size of the package (pkg_size).
    
    Additionally the first published header slice gets a leading "10" (start-of-package) and the last data word gets a leading "01" (end-of-package) attached.
    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity package_CMA is

generic
(
    inp_data_size:      integer;
    pkg_size:           integer;
    header_size:        integer
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Parameters
    PARAM_header:       in  std_logic_vector(header_size-1 downto 0);
    -- Status
    pkg_done:           out std_logic;
    -- CMI Input
    inp_data:           in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(inp_data_size+1 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic
);

end entity package_CMA;


architecture struct of package_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- size of pkg cnt
    constant pkg_vsize:         integer := get_vsize(pkg_size);
    
    -- number of header slices
    constant hslice_n:          integer := INTEGER(CEIL(REAL(header_size)/REAL(inp_data_size)));
    constant hslice_n_vsize:    integer := get_vsize(hslice_n);
   
    --======--
    -- FSMs --
    --======--
    
    type pkg_state_t is (idle, add_header, fwd_data);
    signal pkg_present_state, pkg_next_state: pkg_state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Package Counter
    signal pkg_cnt:         UNSIGNED(pkg_vsize-1 downto 0) := TO_UNSIGNED(pkg_size-1, pkg_vsize);
    signal pkg_rst:         std_logic;
    signal pkg_dec:         std_logic;
    
    -- Header Slice Counter
    signal hslice_cnt:      UNSIGNED(hslice_n_vsize-1 downto 0) := TO_UNSIGNED(hslice_n-1, hslice_n_vsize);
    signal hslice_rst:      std_logic;
    signal hslice_dec:      std_logic;

    --===========--
    -- REGISTERS --
    --===========--
    
    signal full_header:     std_logic_vector(hslice_n*inp_data_size-1 downto 0) := (others => '0');
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal sel_header:      std_logic;
    
BEGIN
    
    --========--
    -- STATUS --
    --========--
    
    pkg_done <= pkg_rst;
    
    --===========--
    -- PARAMETER --
    --===========--

    full_header <= (inp_data_size*hslice_n-1 downto header_size => '0') & PARAM_header;

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if pkg_rst= '1' then
                pkg_cnt <= TO_UNSIGNED(pkg_size-1, pkg_vsize);
            elsif pkg_dec = '1' then
                pkg_cnt <= pkg_cnt - 1;
            end if;
            
            if hslice_rst= '1' then
                hslice_cnt <= TO_UNSIGNED(hslice_n-1, hslice_n_vsize);
            elsif hslice_dec = '1' then
                hslice_cnt <= hslice_cnt - 1;
            end if;
            
        end if;
    end process;


    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_vld <= inp_vld;
                if sel_header = '1' then
                    if hslice_cnt = TO_UNSIGNED(hslice_n-1, hslice_n_vsize) then
                        out_data <= "10" & full_header((TO_INTEGER(hslice_cnt)+1)*inp_data_size-1 downto TO_INTEGER(hslice_cnt)*inp_data_size); -- add sop
                    else
                        out_data <= "00" & full_header((TO_INTEGER(hslice_cnt)+1)*inp_data_size-1 downto TO_INTEGER(hslice_cnt)*inp_data_size);
                    end if;
                elsif inp_vld = '1' then
                    if pkg_cnt = 0 then
                        out_data <= "01" & inp_data; -- add eop
                    else
                        out_data <= "00" & inp_data;
                    end if;
                end if;
            end if;
                    
        end if;
    end process;
    
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            pkg_present_state <= pkg_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        inp_next        <= '1';
        -- Counter
        pkg_rst         <= '0';
        pkg_dec         <= '0';
        hslice_dec      <= '0';
        hslice_rst      <= '0';
        -- data output
        sel_header      <= '1';
        
        case pkg_present_state is
            
            --===============--
            when idle =>
            --===============--
            
                if inp_vld = '1' then
                    inp_next <= '0';
                    if out_next = '1' then
                        if hslice_cnt = 0 then
                            hslice_rst      <= '1';
                            pkg_next_state  <= fwd_data;
                        else
                            hslice_dec      <= '1';
                            pkg_next_state  <= add_header;
                        end if;
                    else
                        pkg_next_state <= add_header;
                    end if;
                else
                    pkg_next_state <= idle;
                end if;  
            
            --===============--
            when add_header =>
            --===============--
            
                inp_next <= '0';
                if out_next = '1' then
                    if hslice_cnt = 0 then
                        hslice_rst      <= '1';
                        pkg_next_state  <= fwd_data;
                    else
                        hslice_dec      <= '1';
                        pkg_next_state  <= add_header;
                    end if;
                else
                    pkg_next_state <= add_header;
                end if;
            
            --===============--
            when fwd_data =>
            --===============--
                
                sel_header <= '0';
                if inp_vld = '1' then
                    if out_next = '1' then
                        if pkg_cnt = 0 then
                            pkg_rst         <= '1';
                            pkg_next_state  <= idle;
                        else
                            pkg_dec         <= '1';
                            pkg_next_state  <= fwd_data;
                        end if;
                    else
                        inp_next        <= '0';
                        pkg_next_state  <= fwd_data;
                    end if;
                else
                    pkg_next_state <= fwd_data;
                end if;
            
        end case;
    end process;
  
    
    
end architecture struct;
