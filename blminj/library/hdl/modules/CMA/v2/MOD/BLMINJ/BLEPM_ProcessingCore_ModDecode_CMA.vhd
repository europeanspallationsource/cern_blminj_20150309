------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/11/2013
Module Name:    BLEPM_ProcessingCore_ModDecode_CMA

-----------------
Short Description
-----------------

    Top Module of the Processing Structure. Instantiates a certain number of Processing Channels.
    
    Connectivity checks (CH, 2015/02/20)
        + Instantiates one Processing Channel with Modulation Decode features

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the input data (includes full data for all channels, e.g. 8x20bit = 160 bit)
    RS_number_per_ch    := number of Running Sums per Channel
    channel_max         := number of channels (maximum: 8)
    PM_active           := Post Mortem used in all channels (1) or not (0)
   

--------------
Implementation
--------------
    
    detailed description of the actual implementation
    

-----------
Limitations
-----------
    

----------------
Missing Features
----------------


    
    

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    use work.BLMINJ_pkg.all;
   
entity BLEPM_ProcessingCore_ModDecode_CMA is


generic
(
    inp_data_size:          integer := 160;
    RS_number_per_ch:       integer := 3;
    channel_max:            integer := 8;
    PM_active:              integer := 1
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Timing Pulses
    BP_pulse:           in  std_logic;
    BIn_pulse:          in  std_logic;
    BOut_pulse:         in  std_logic;
    EVOstart_pulse:     in  std_logic;
    EVOstop_pulse:      in  std_logic;
    EVO_pulse:          in  std_logic;
    CAPstart_pulse:     in  std_logic;
    CAPstop_pulse:      in  std_logic;
    CAP_pulse:          in  std_logic;
    -- Timing Status
    BEAM_ACT:           in  std_logic;
    -- Timing Counter
    ACQ_CNT:            in  std_logic_vector(31 downto 0);
    BP_CNT:             in  std_logic_vector(31 downto 0);
    EVO_CNT:            in  std_logic_vector(31 downto 0);
    CAP_CNT:            in  std_logic_vector(31 downto 0);
    -- Timing Output
    obs_sample:         out std_logic;
    -- Paramaters
    PARAM_LoC_TH:       in  slv_array(0 to channel_max-1)(63 downto 0);
    PARAM_LoBP_TH:      in  slv_array(0 to channel_max-1)(63 downto 0);
    PARAM_RS_TH:        in  slv_array_array(0 to channel_max-1)(0 to RS_number_per_ch-1)(63 downto 0);   
    -- CMI Input
    inp_data:           in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(65 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic;
    -- Std TH Output
    TC_results:         out slv_array(0 to channel_max-1)(2+RS_number_per_ch-1 downto 0);
    
    -- Signals from ModDecode to signaltap
    out_error:          out std_logic_vector(1 downto 0);
    FFTin_or_out_valid: out std_logic
);

end entity BLEPM_ProcessingCore_ModDecode_CMA;


architecture struct of BLEPM_ProcessingCore_ModDecode_CMA is
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- mux_CMI
    constant mux_data_size:             integer := 66;
    constant mux_tx_number:             integer := channel_max;
    constant mux_rx_number:             integer := 1;

    -- split_CMI
    constant split_tx_data_size:        integer := inp_data_size;
    constant split_rx_data_size:        integer := inp_data_size/channel_max;
    constant split_rx_number:           integer := channel_max;
    
    --=============--
    -- CMA SIGNALS --
    --=============--

    -- mux_CMI
    signal mux_tx_data:                 slv_array(mux_tx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_tx_vld:                  std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_tx_next:                 std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_rx_data:                 slv_array(mux_rx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_rx_vld:                  std_logic_vector(mux_rx_number-1 downto 0);
    signal mux_rx_next:                 std_logic_vector(mux_rx_number-1 downto 0);
    
    signal ARB_mux_tx_vld:              std_logic_vector(mux_tx_number-1 downto 0);
    signal ARB_mux_tx_next:             std_logic_vector(mux_tx_number-1 downto 0);
    signal ARB_mux_rx_vld:              std_logic;
    signal ARB_mux_rx_next:             std_logic;
    signal ARB_mux_sel:                 std_logic_vector(get_vsize_addr(mux_tx_number)-1 downto 0);
    
    -- split_CMI
    signal split_tx_data:               std_logic_vector(split_tx_data_size-1 downto 0);
    signal split_tx_vld:                std_logic;
    signal split_tx_next:               std_logic;
    signal split_rx_data:               slv_array(split_rx_number-1 downto 0)(split_rx_data_size-1 downto 0);
    signal split_rx_vld:                std_logic_vector(split_rx_number-1 downto 0);
    signal split_rx_next:               std_logic_vector(split_rx_number-1 downto 0);
    
    signal DM_split_tx_data:            std_logic_vector(split_tx_data_size-1 downto 0);
    signal DM_split_tx_vld:             std_logic;
    signal DM_split_rx_data:            slv_array(split_rx_number-1 downto 0)(split_rx_data_size-1 downto 0);
    signal DM_split_rx_vld:             std_logic_vector(split_rx_number-1 downto 0);
    
    
    -- Signal to allow connecting all TC_results channels to Channel 0. CH
    signal TC_results_int:              std_logic_vector(2+RS_number_per_ch-1 downto 0);
    

BEGIN

    --===============--
    -- Data Splitter --
    --===============--
    
    -- Sampling --
    smpl_detect_CMA_inst: entity work.smpl_detect_CMA
    generic map
    (
        data_size   => inp_data_size
    )
    port map 
    (
        clk         => clk,
        smpl_det    => obs_sample,
        inp_data    => inp_data,
        inp_vld     => inp_vld,
        inp_next    => inp_next,
        out_data    => split_tx_data,
        out_vld     => split_tx_vld,
        out_next    => split_tx_next
    );
    
    -- split_CMI
    split_CMI: entity work.DM_CMI 
    generic map
    (
        tx_data_size    => split_tx_data_size,
        rx_data_size    => split_rx_data_size,
        rx_number       => split_rx_number
    )
    port map 
    (
        clk         => clk,
        tx_data     => split_tx_data,
        tx_vld      => split_tx_vld,
        tx_next     => split_tx_next,
        rx_data     => split_rx_data,
        rx_vld      => split_rx_vld,
        rx_next     => split_rx_next,
        DM_tx_data  => DM_split_tx_data,
        DM_tx_vld   => DM_split_tx_vld,
        DM_rx_data  => DM_split_rx_data,
        DM_rx_vld   => DM_split_rx_vld
    );
    
    -- split_CMI_DM
    split_CMI_DM: entity work.data_splitter_DM
    generic map
    (
        tx_data_size    => split_tx_data_size,
        rx_data_size    => split_rx_data_size,
        rx_number       => split_rx_number
    )
    port map
    (
        clk             => clk,
        DM_tx_data      => DM_split_tx_data,
        DM_tx_vld       => DM_split_tx_vld,
        DM_rx_data      => DM_split_rx_data,
        DM_rx_vld       => DM_split_rx_vld
    );
    

    --====================--
    -- Processing Channel --
    --====================--

    -- Channel 0 with Modulation Decode features (CH)
    PrCH0: entity work.BLEPM_ProcessingChannel_ModDecode_CMA
    generic map
    (
        inp_data_size   => split_rx_data_size,
        channel_ID      => 0,
        PM_active       => PM_active,
        RS_number       => RS_number_per_ch
    )
    port map 
    (
        clk                 => clk,
        BP_pulse            => BP_pulse,
        BIn_pulse           => BIn_pulse,
        BOut_pulse          => BOut_pulse,
        EVOstart_pulse      => EVOstart_pulse,
        EVOstop_pulse       => EVOstop_pulse,
        EVO_pulse           => EVO_pulse,
        CAPstart_pulse      => CAPstart_pulse,
        CAPstop_pulse       => CAPstop_pulse,
        CAP_pulse           => CAP_pulse,
        BEAM_ACT            => BEAM_ACT,
        ACQ_CNT             => ACQ_CNT,
        BP_CNT              => BP_CNT,
        EVO_CNT             => EVO_CNT,
        CAP_CNT             => CAP_CNT,
        PARAM_LoC_TH        => PARAM_LoC_TH(0),
        PARAM_LoBP_TH       => PARAM_LoBP_TH(0),
        PARAM_RS_TH         => PARAM_RS_TH(0),
        inp_data            => split_rx_data(0),
        inp_vld             => split_rx_vld(0),
        inp_next            => split_rx_next(0),
        out_data            => mux_tx_data(0),
        out_vld             => mux_tx_vld(0),
        out_next            => mux_tx_next(0),
        TC_results          => TC_results_int,          -- MODIFIED! Was: TC_results(0). CH
        out_error           => out_error,
        FFTin_or_out_valid  => FFTin_or_out_valid
    );
    
    
    -- Connect outputs of the single module to every other input to avoid problems
    TC_results(0) <= TC_results_int;
    
    ProcessingChannel_output_gen:
    for i in 1 to channel_max-1 generate
        split_rx_next(i) <= split_rx_next(0);
        mux_tx_data(i)   <= mux_tx_data(0);
        mux_tx_vld(i)    <= mux_tx_vld(0);
        TC_results(i)    <= TC_results_int;
    end generate;


/*    
    -------------------------------------------------------------------------------
    -- All other channels commented out to save space and speed up compilation!!!
    -------------------------------------------------------------------------------
    ProcessingChannel_CMA_gen:
    for i in 1 to channel_max-1 generate
    
        PrCH: entity work.BLEPM_ProcessingChannel_CMA
        generic map
        (
            inp_data_size   => split_rx_data_size,
            channel_ID      => i,
            PM_active       => PM_active,
            RS_number       => RS_number_per_ch
        )
        port map 
        (
            clk                 => clk,
            BP_pulse            => BP_pulse,
            BIn_pulse           => BIn_pulse,
            BOut_pulse          => BOut_pulse,
            EVOstart_pulse      => EVOstart_pulse,
            EVOstop_pulse       => EVOstop_pulse,
            EVO_pulse           => EVO_pulse,
            CAPstart_pulse      => CAPstart_pulse,
            CAPstop_pulse       => CAPstop_pulse,
            CAP_pulse           => CAP_pulse,
            BEAM_ACT            => BEAM_ACT,
            ACQ_CNT             => ACQ_CNT,
            BP_CNT              => BP_CNT,
            EVO_CNT             => EVO_CNT,
            CAP_CNT             => CAP_CNT,
            PARAM_LoC_TH        => PARAM_LoC_TH(i),
            PARAM_LoBP_TH       => PARAM_LoBP_TH(i),
            PARAM_RS_TH         => PARAM_RS_TH(i),
            inp_data            => split_rx_data(i),
            inp_vld             => split_rx_vld(i),
            inp_next            => split_rx_next(i),
            out_data            => mux_tx_data(i),
            out_vld             => mux_tx_vld(i),
            out_next            => mux_tx_next(i),
            TC_results          => TC_results(i)
        );

    end generate;
*/

    --================--
    -- Storage Output --
    --================--
    
    -- mux_CMI
    mux_CMI: entity work.MM_CMI 
    generic map
    (
        tx_number       => mux_tx_number,
        rx_number       => mux_rx_number,
        data_size       => mux_data_size
    )
    port map 
    (
        clk         => clk,
        tx_data     => mux_tx_data,
        tx_vld      => mux_tx_vld,
        tx_next     => mux_tx_next,
        rx_data     => mux_rx_data,
        rx_vld      => mux_rx_vld,
        rx_next     => mux_rx_next,
        ARB_tx_vld  => ARB_mux_tx_vld,
        ARB_tx_next => ARB_mux_tx_next,
        ARB_rx_vld  => ARB_mux_rx_vld,
        ARB_rx_next => ARB_mux_rx_next,
        ARB_sel     => ARB_mux_sel
        
    );

    mux_CMI_ARB: entity work.priority_pkg_mux_ARB
    generic map
    (
        tx_number       => mux_tx_number,
        data_size       => mux_data_size
    )
    port map
    (
        clk             => clk,
        full_data       => mux_tx_data,
        ARB_tx_vld      => ARB_mux_tx_vld,
        ARB_tx_next     => ARB_mux_tx_next,
        ARB_rx_vld      => ARB_mux_rx_vld,
        ARB_rx_next     => ARB_mux_rx_next,
        ARB_sel         => ARB_mux_sel
    );
    
    -- delay
    appCRC32_CMA_inst: entity work.appCRC32_DATA64_CMA
    port map 
    (
        clk         => clk,
        inp_data    => mux_rx_data(0),
        inp_vld     => mux_rx_vld(0),
        inp_next    => mux_rx_next(0),
        out_data    => out_data,
        out_vld     => out_vld,
        out_next    => out_next
    );
    
   
end architecture struct;
