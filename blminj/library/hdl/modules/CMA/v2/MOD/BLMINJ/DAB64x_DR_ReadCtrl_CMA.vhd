------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/09/2014
Module Name:    DAB64x_DR_ReadCtrl_CMA

-----------------
Short Description
-----------------

    This module request the diagnsotic data on the right side of the DR_RAM. When triggered, it starts requesting
    all diagnostic data.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    out_data_size   := size of the output data


    
--------------
Implementation
--------------
    
    
-----------
Limitations
-----------


----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DAB64x_DR_ReadCtrl_CMA is

generic
(
    out_data_size:      integer
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Trigger
    start_req:          in  std_logic;
    -- CMI Input
    out_data:           in  std_logic_vector(out_data_size-1 downto 0);
    out_vld:            in  std_logic;
    out_next:           out std_logic
);

end entity DAB64x_DR_ReadCtrl_CMA;


architecture struct of DAB64x_DR_ReadCtrl_CMA is
    
    
BEGIN

 
    
end architecture struct;
