------------------------------
/*
Company:       CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/02/2014
Module Name:    DAB64x_StorageWriteCtrl_CMA


-----------------
Short Description
-----------------

    This module creates an address from a package header and creates subsequent addresses, if needed.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    header_data_size    := size of the header CMI input data port
    body_data_size      := size of the body CMI input data port
    out_data_size       := size of the output CMI data port

--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    use work.BLMINJ_pkg.all;
   
entity DAB64x_StorageWriteCtrl_CMA is

generic
(
    header_data_size:   integer;
    body_data_size:     integer;
    out_data_size:      integer
    
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- CMI Input
    header_data:        in  std_logic_vector(header_data_size-1 downto 0);
    header_vld:         in  std_logic;
    header_next:        out std_logic;
    body_data:          in  std_logic_vector(body_data_size-1 downto 0);
    body_vld:           in  std_logic;
    body_next:          out std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(out_data_size-1 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic
);

end entity DAB64x_StorageWriteCtrl_CMA;


architecture struct of DAB64x_StorageWriteCtrl_CMA is
    
    --======--
    -- FSMS --
    --======--
    
    type state_t is (send_data, send_page_no);
    signal present_state, next_state: state_t := send_data;
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant addr_size:         integer := 32;
    constant ID_size:           integer := 4;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Address Counter
    signal addr_cnt:            UNSIGNED(addr_size-1 downto 0) := (others => '0');
    signal addr_rst:            std_logic;
    signal addr_inc:            std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal header:              std_logic_vector(header_data_size-1 downto 0) := (others => '0');
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- Header
    signal store_header:        std_logic;
    signal CARD_NO:             std_logic_vector(BLMINJ_CARD_NO_size-1 downto 0);
    signal PATH:                std_logic_vector(BLMINJ_TYPE_size-1 downto 0);
    signal ACQ_CH:              std_logic_vector(BLMINJ_SPC1_size-1 downto 0);
    signal PROC_TYPE:           std_logic_vector(BLMINJ_SPC2_size-1 downto 0);
    signal PROC_TYPE_early:     std_logic_vector(BLMINJ_SPC2_size-1 downto 0);
    signal PAGE_NO:             UNSIGNED(BLMINJ_PAGE_NO_size-1 downto 0);
    
    signal RAM_ID:              std_logic_vector(ID_size-1 downto 0);
    signal addr_gen:            UNSIGNED(addr_size-1 downto 0);
    signal addr_page_no:        std_logic_vector(addr_size-1 downto 0);
    signal page_no_write:       std_logic;
    signal out_vld_int:         std_logic;
    
BEGIN   
    
    --========--
    -- HEADER --
    --========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if store_header = '1' then
                header <= header_data;
            end if;
        end if;
    end process;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if addr_rst = '1' then
                addr_cnt <= (others => '0');
            elsif addr_inc = '1' then
                addr_cnt <= addr_cnt + 1;
            end if;
            
        end if;
    end process;
    

    --================--
    -- HEADER TO ADDR --
    --================--
    
    CARD_NO         <= header(63 downto 60); 
    PATH            <= header(59 downto 56);
    ACQ_CH          <= header(55 downto 48);
    PROC_TYPE       <= header(47 downto 40);
    PROC_TYPE_early <= header_data(47 downto 40);
    PAGE_NO         <= UNSIGNED(header(39 downto 16));

    process(all) is
    begin
        -- Defaults
        addr_gen        <= (others => '1');
        addr_page_no    <= (others => '1');
        RAM_ID          <= "1111";

        if PATH = "0010" then -- Processing
        
            case PROC_TYPE is
                -- Evolution
                when "00001001" =>     
                    RAM_ID <= "0000";
                    case ACQ_CH is
                        when "00000000" => addr_gen <= 32UX"0000" + PAGE_NO; addr_page_no <= 32UX"2800";
                        when "00000001" => addr_gen <= 32UX"0500" + PAGE_NO; addr_page_no <= 32UX"2801";
                        when "00000010" => addr_gen <= 32UX"0A00" + PAGE_NO; addr_page_no <= 32UX"2802";
                        when "00000011" => addr_gen <= 32UX"0F00" + PAGE_NO; addr_page_no <= 32UX"2803";
                        when "00000100" => addr_gen <= 32UX"1400" + PAGE_NO; addr_page_no <= 32UX"2804";
                        when "00000101" => addr_gen <= 32UX"1900" + PAGE_NO; addr_page_no <= 32UX"2805";
                        when "00000110" => addr_gen <= 32UX"1E00" + PAGE_NO; addr_page_no <= 32UX"2806";
                        when "00000111" => addr_gen <= 32UX"2300" + PAGE_NO; addr_page_no <= 32UX"2807";
                        when others => addr_gen <= (others => '1');
                    end case;
                -- Capture
                when "00001010" =>
                    RAM_ID <= "0001";    
                    case ACQ_CH is
                        when "00000000" => addr_gen <= 32UX"0000" + PAGE_NO; addr_page_no <= 32UX"1000";
                        when "00000001" => addr_gen <= 32UX"0200" + PAGE_NO; addr_page_no <= 32UX"1001";
                        when "00000010" => addr_gen <= 32UX"0400" + PAGE_NO; addr_page_no <= 32UX"1002";
                        when "00000011" => addr_gen <= 32UX"0600" + PAGE_NO; addr_page_no <= 32UX"1003";
                        when "00000100" => addr_gen <= 32UX"0800" + PAGE_NO; addr_page_no <= 32UX"1004";
                        when "00000101" => addr_gen <= 32UX"0A00" + PAGE_NO; addr_page_no <= 32UX"1005";
                        when "00000110" => addr_gen <= 32UX"0C00" + PAGE_NO; addr_page_no <= 32UX"1006";
                        when "00000111" => addr_gen <= 32UX"0E00" + PAGE_NO; addr_page_no <= 32UX"1007";
                        when others => addr_gen <= (others => '1');
                    end case;
               -- LoC
               when "00000010" =>
                    RAM_ID <= "0010";
                    case ACQ_CH is
                        when "00000000" => addr_gen <= 32UX"0000";
                        when "00000001" => addr_gen <= 32UX"0003";
                        when "00000010" => addr_gen <= 32UX"0006";
                        when "00000011" => addr_gen <= 32UX"0009";
                        when "00000100" => addr_gen <= 32UX"000C";
                        when "00000101" => addr_gen <= 32UX"000F";
                        when "00000110" => addr_gen <= 32UX"0012";
                        when "00000111" => addr_gen <= 32UX"0015";
                        when others => addr_gen <= (others => '1');
                    end case;
               -- LoBP 
               when "00000011" =>
                    RAM_ID <= "0010";       
                    case ACQ_CH is
                        when "00000000" => addr_gen <= 32UX"0018";
                        when "00000001" => addr_gen <= 32UX"001B";
                        when "00000010" => addr_gen <= 32UX"001E";
                        when "00000011" => addr_gen <= 32UX"0021";
                        when "00000100" => addr_gen <= 32UX"0024";
                        when "00000101" => addr_gen <= 32UX"0027";
                        when "00000110" => addr_gen <= 32UX"002A";
                        when "00000111" => addr_gen <= 32UX"002D";
                        when others => addr_gen <= (others => '1');
                    end case;
               -- RS #1    
               when "00010000" =>
                    RAM_ID <= "0010";
                    case ACQ_CH is
                        when "00000000" => addr_gen <= 32UX"0030";
                        when "00000001" => addr_gen <= 32UX"0032";
                        when "00000010" => addr_gen <= 32UX"0034";
                        when "00000011" => addr_gen <= 32UX"0036";
                        when "00000100" => addr_gen <= 32UX"0038";
                        when "00000101" => addr_gen <= 32UX"003A";
                        when "00000110" => addr_gen <= 32UX"003C";
                        when "00000111" => addr_gen <= 32UX"003E";
                        when others => addr_gen <= (others => '1');
                    end case;
               -- RS #2     
               when "00010001" =>
                    RAM_ID <= "0010";
                    case ACQ_CH is
                        when "00000000" => addr_gen <= 32UX"0040";
                        when "00000001" => addr_gen <= 32UX"0042";
                        when "00000010" => addr_gen <= 32UX"0044";
                        when "00000011" => addr_gen <= 32UX"0046";
                        when "00000100" => addr_gen <= 32UX"0048";
                        when "00000101" => addr_gen <= 32UX"004A";
                        when "00000110" => addr_gen <= 32UX"004C";
                        when "00000111" => addr_gen <= 32UX"004E";
                        when others => addr_gen <= (others => '1');
                    end case;
               -- RS #3     
               when "00010010" =>
                    RAM_ID <= "0010";
                    case ACQ_CH is
                        when "00000000" => addr_gen <= 32UX"0050";
                        when "00000001" => addr_gen <= 32UX"0052";
                        when "00000010" => addr_gen <= 32UX"0054";
                        when "00000011" => addr_gen <= 32UX"0056";
                        when "00000100" => addr_gen <= 32UX"0058";
                        when "00000101" => addr_gen <= 32UX"005A";
                        when "00000110" => addr_gen <= 32UX"005C";
                        when "00000111" => addr_gen <= 32UX"005E";
                        when others => addr_gen <= (others => '1');
                    end case;
                 
                when others => addr_gen <= (others => '1');
            end case;
        
        elsif PATH = "1000" then -- Diagnostics
            -- fill in later
        end if;

    end process;
    
    
    --============--
    -- CMI OUTREG --
    --============--
               
	process (clk) is
	begin
		if rising_edge(clk) then
			
            if out_next = '1' then
                
                out_vld <= out_vld_int;
                if page_no_write = '1' then
                    out_data <= RAM_ID & addr_page_no & (39 downto 0 => '0') & STD_LOGIC_VECTOR(PAGE_NO);
                else
                    out_data <= RAM_ID & STD_LOGIC_VECTOR(addr_gen + addr_cnt) & body_data;
                end if;
                
			end if;
			
		end if;
	end process;

    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        header_next     <= NOT header_vld;
        body_next       <= NOT body_vld;
        out_vld_int     <= '0';
        -- Counter
        addr_rst        <= '0';
        addr_inc        <= '0';
        -- Header
        store_header    <= '0';
        -- Page No
        page_no_write   <= '0';
        
        case present_state is
            
            --===============--
            when send_data =>
            --===============--
                    
                next_state <= send_data;
                    
                if header_vld = '1' then
                    store_header    <= '1';
                    addr_rst        <= '1';
                    header_next     <= '1';
                    if PROC_TYPE_early = "00001001" OR PROC_TYPE_early = "00001010" then
                        next_state <= send_page_no;
                    end if;
                elsif body_vld = '1' then
                    if out_next = '1' then
                        addr_inc        <= '1';
                        out_vld_int     <= '1';
                        body_next       <= '1';
                    end if;
                end if;

            
            --===============--
            when send_page_no =>
            --===============--
                
                if out_next = '1' then
                    page_no_write   <= '1';
                    out_vld_int     <= '1';
                    next_state      <= send_data;
                else
                    next_state      <= send_page_no;
                end if;
    
        end case;
        
    end process;
    
    
end architecture struct;
