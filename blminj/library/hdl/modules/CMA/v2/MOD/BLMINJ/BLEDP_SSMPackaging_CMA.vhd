------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    20/10/2013
Module Name:    BLEDP_SSMPackaging_CMA

-----------------
Short Description
-----------------

    This module creates the packages from the SSM on the BLEDP card.

------------
Dependencies
------------

    vhdl_func_pkg
    BLMINJ_pkg
    PackageCreator16_CMA
    fifo_CMA
    
------------------
Generics/Constants
------------------
    
    inp_data_size       := size of the CMI input data bus
    out_data_size       := size of the CMI output data bus
       
    pkged_data_size     := size of the pkged_CMI data bus
    pkged_rx_number     := number of the pkged_CMI receiver modules

--------------
Implementation
--------------
    

    

-----------
Limitations
-----------
    

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    use work.BLMINJ_pkg.all;
   
entity BLEDP_SSMPackaging_CMA is

port
(
    -- Clock
    clk:            in  std_logic;
    -- Timing Information
    ACQ_CNT:        in  std_logic_vector(31 downto 0);
    -- CMI Input
    inp_data:       in  std_logic_vector(15 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(17 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity BLEDP_SSMPackaging_CMA;


architecture struct of BLEDP_SSMPackaging_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant fifo_depth:        integer := 16;
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    constant inp_data_size:     integer := 16;
    constant out_data_size:     integer := 18;
       
    -- PKGED
    constant pkged_data_size:   integer := out_data_size;
    constant pkged_rx_number:   integer := 1;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal header:                  std_logic_vector(BLMINJ_header_size-1 downto 0);
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- pkged
    signal pkged_tx_data:     std_logic_vector(pkged_data_size-1 downto 0);
    signal pkged_tx_vld:      std_logic;
    signal pkged_tx_next:     std_logic;
    signal pkged_rx_data:     slv_array(pkged_rx_number-1 downto 0)(pkged_data_size-1 downto 0);
    signal pkged_rx_vld:      std_logic_vector(pkged_rx_number-1 downto 0);
    signal pkged_rx_next:     std_logic_vector(pkged_rx_number-1 downto 0);
      
    
BEGIN
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    header <= BLEDP_CARD_NO & SSM_TYPE & SSM_SPC1 & SSM_SPC2 & SSM_PAGE_NO & ACQ_CNT;
    
    --====================--
    -- PACKAGE CREATOR 16 --
    --====================--
    
    PackageCreator16_CMA_inst : entity work.PackageCreator16_CMA 
    generic map
    (
        pkg_size            => SSM_PKG_SIZE,
        header_size         => BLMINJ_header_size   
    )
    port map 
    (
        clk             => clk,
        PARAM_header    => header,
        inp_data        => inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => pkged_tx_data,
        out_vld         => pkged_tx_vld,
        out_next        => pkged_tx_next
    );
    
    --===========--
    -- PKGED CMI --
    --===========--
    
    pkged_CMI : entity work.CMI 
    generic map
    (
        rx_number   => pkged_rx_number,
        data_size   => pkged_data_size
    )
    port map 
    (
        clk         => clk,
        tx_data     => pkged_tx_data,
        tx_vld      => pkged_tx_vld,
        tx_next     => pkged_tx_next,
        rx_data     => pkged_rx_data,
        rx_vld      => pkged_rx_vld,
        rx_next     => pkged_rx_next
    );
    
    --======--
    -- FIFO --
    --======--
    
    fifo_CMA_inst: entity work.fifo_CMA
    GENERIC MAP
    (
        data_size   => out_data_size,
        fifo_depth  => fifo_depth
    )
    PORT MAP
    (
        clk                 => clk,
        inp_data            => pkged_rx_data(0),
        inp_vld             => pkged_rx_vld(0),
        inp_next            => pkged_rx_next(0),
        out_data            => out_data,
        out_vld             => out_vld,
        out_next            => out_next
    );
    
    

end architecture struct;
