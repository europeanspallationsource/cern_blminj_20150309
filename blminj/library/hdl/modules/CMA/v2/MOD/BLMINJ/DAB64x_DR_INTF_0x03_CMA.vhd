------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/09/2014
Module Name:    DAB64x_DR_INTF_0x03_CMA


-----------------
Short Description
-----------------

    Interface 0x03 (DS2411) of the DAB64x card.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    header_data_size    := size of the header CMI input data port
    body_data_size      := size of the body CMI input data port
    out_data_size       := size of the output CMI data port

--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DAB64x_DR_INTF_0x03_CMA is

generic
(
    Tclk_ns:            integer;
    tag_n:              integer
);

port
(   
    -- Clock
    clk:                in  std_logic;
    -- Timing Pulses
    BP_pulse:           in  std_logic;
    -- Parameter
    PARAM_INTF_0x03:    in  std_logic_vector(63 downto 0);
    -- Interface
    ID:                 inout std_logic;
    -- CMA Output
    out_data:           out std_logic_vector(17 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic;
    -- Comparator Results
    comp_results:       out std_logic_vector(tag_n-1 downto 0)
);

end entity DAB64x_DR_INTF_0x03_CMA;


architecture struct of DAB64x_DR_INTF_0x03_CMA is
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- rdout_CMA
    constant rdout_data_size:       integer := 64;
    -- comp_CMA
    constant comp_data_size:        integer := rdout_data_size;
   
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant param_data_size:       integer := 64;
    constant comp_type:             integer := 2; -- check on equal
    constant header_size:           integer := 16;
    constant pkg_size:              integer := integer(CEIL(real(rdout_data_size)/16.0));
    constant headers:               slv_array(tag_n-1 downto 0)(header_size-1 downto 0) := (0       => 16X"0300", 
                                                                                            others  => "0000"); --INTF+TAG
   
    --=============--
    -- CMA SIGNALS --
    --=============--
   
    -- rdout_CMA
    signal rdout_data:              std_logic_vector(rdout_data_size-1 downto 0);
    signal rdout_vld:               std_logic;
    signal rdout_next:              std_logic;
    
     -- comp_CMA
    signal comp_data:               slv_array(tag_n-1 downto 0)(comp_data_size-1 downto 0);
    signal comp_vld:                std_logic_vector(tag_n-1 downto 0);
    signal comp_next:               std_logic_vector(tag_n-1 downto 0);
    
    
    
BEGIN
    
    -- Interface --
    DS2411_CMA_inst: entity work.DS2411_CMA
    GENERIC MAP
    (
        Tclk_ns             => Tclk_ns
    )
    PORT MAP
    (
        clk                 => clk,
        DATA                => ID,
        rdreq_data          => '1',
        rdreq_vld           => BP_pulse,
        rdreq_next          => open,
        rdout_data          => rdout_data,
        rdout_vld           => rdout_vld,
        rdout_next          => rdout_next
    );
    
    
    -- Comparator
    compare_pass_CMA_inst: entity work.compare_pass_CMA
    GENERIC MAP
    (
        data_size           => rdout_data_size,
        param_data_size     => param_data_size,
        comp_type           => comp_type
    )
    PORT MAP
    (
        clk                 => clk,
        PARAM_comp_value    => PARAM_INTF_0x03,
        inp_data            => rdout_data,
        inp_vld             => rdout_vld,
        inp_next            => rdout_next,
        out_data            => comp_data(0),
        out_vld             => comp_vld(0),
        out_next            => comp_next(0),
        vectorize(result)   => comp_results
    );
    
   
    
    -- DR Processing --
    DR_Processing_CMA_inst: entity work.DR_Processing_CMA
    GENERIC MAP
    (
        inp_data_size       => rdout_data_size,
        header_size         => header_size,
        pkg_size            => pkg_size,
        tag_n               => tag_n
    )
    PORT MAP
    (
        clk                 => clk,
        UPDATE_pulse        => BP_pulse,
        PARAM_headers       => headers,
        inp_data            => comp_data,
        inp_vld             => comp_vld,
        inp_next            => comp_next,
        out_data            => out_data,
        out_vld             => out_vld,
        out_next            => out_next
    );
    
    
end architecture struct;
