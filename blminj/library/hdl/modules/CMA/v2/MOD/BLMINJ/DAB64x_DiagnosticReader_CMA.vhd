------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/09/2014
Module Name:    DAB64x_DiagnosticReader_CMA


-----------------
Short Description
-----------------

    Diagnostic Reader for the DAVB64x card.
    
------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    header_data_size    := size of the header CMI input data port
    body_data_size      := size of the body CMI input data port
    out_data_size       := size of the output CMI data port

--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DAB64x_DiagnosticReader_CMA is

generic
(
    Tclk_ns:            integer
);

port
(   
    -- Clock
    clk:                in  std_logic;
    -- Timing Pulse
    BP_pulse:           in  std_logic;
    -- Parameters
    PARAM_INTF_0x00:    in  std_logic_vector(31 downto 0);
    PARAM_INTF_0x01:    in  std_logic_vector(15 downto 0);
    PARAM_INTF_0x03:    in  std_logic_vector(63 downto 0);
    -- INTF 0x00
    DAB64x_FW:          in  std_logic_vector(31 downto 0);
    -- INTF 0x01
    GEO_ADDRESS:        in  std_logic_vector(4 downto 0);
    -- INTF 0x03
    ID:                 inout std_logic;
    -- CMA Output
    out_data:           out std_logic_vector(17 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic;
    -- SSM Results
    results:            out std_logic_vector(2 downto 0)

);

end entity DAB64x_DiagnosticReader_CMA;


architecture struct of DAB64x_DiagnosticReader_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant intf_n:                integer := 3;
    
    -- Tag_N
    constant INTF_0x00_tag_n:       integer := 1;
    constant INTF_0x01_tag_n:       integer := 1;
    constant INTF_0x03_tag_n:       integer := 1;
    
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- mux_CMA
    constant mux_data_size:         integer := 18;
    constant mux_tx_number:         integer := intf_n;
    constant mux_rx_number:         integer := 1;
    
    
    --=============--
    -- CMA SIGNALS --
    --=============--

    -- mux_CMA
    signal mux_tx_data:             slv_array(mux_tx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_tx_vld:              std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_tx_next:             std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_rx_data:             slv_array(mux_rx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_rx_vld:              std_logic_vector(mux_rx_number-1 downto 0);
    signal mux_rx_next:             std_logic_vector(mux_rx_number-1 downto 0);
    
    signal ARB_mux_tx_vld:          std_logic_vector(mux_tx_number-1 downto 0);
    signal ARB_mux_tx_next:         std_logic_vector(mux_tx_number-1 downto 0);
    signal ARB_mux_rx_vld:          std_logic;
    signal ARB_mux_rx_next:         std_logic;
    signal ARB_mux_sel:             std_logic_vector(get_vsize_addr(mux_tx_number)-1 downto 0);
    
    --=============--
    -- GEN SIGNALS --
    --=============--
    
    signal INTF_0x00_results:       std_logic_vector(INTF_0x00_tag_n-1 downto 0);
    signal INTF_0x01_results:       std_logic_vector(INTF_0x01_tag_n-1 downto 0);
    signal INTF_0x03_results:       std_logic_vector(INTF_0x03_tag_n-1 downto 0);
    
    
BEGIN

    --=============--
    -- SSM Results --
    --=============--
    
    results <= INTF_0x03_results & INTF_0x01_results & INTF_0x00_results;
    

    --===========--
    -- INTF 0x00 --
    --===========--
    
    INTF_0x00: entity work.DAB64x_DR_INTF_0x00_CMA
    generic map
    (
        Tclk_ns             => Tclk_ns,
        tag_n               => INTF_0x00_tag_n
    )
    port map 
    (
        clk                 => clk,
        BP_pulse            => BP_pulse,
        PARAM_INTF_0x00     => PARAM_INTF_0x00,
        DAB64x_FW           => DAB64x_FW,
        out_data            => mux_tx_data(0),
        out_vld             => mux_tx_vld(0),
        out_next            => mux_tx_next(0),
        comp_results        => INTF_0x00_results
        
    );

    --===========--
    -- INTF 0x01 --
    --===========--
    
    INTF_0x01: entity work.DAB64x_DR_INTF_0x01_CMA
    generic map
    (
        Tclk_ns             => Tclk_ns,
        tag_n               => INTF_0x01_tag_n
    )
    port map 
    (
        clk                 => clk,
        BP_pulse            => BP_pulse,
        PARAM_INTF_0x01     => PARAM_INTF_0x01,
        GEO_ADDRESS         => GEO_ADDRESS,
        out_data            => mux_tx_data(1),
        out_vld             => mux_tx_vld(1),
        out_next            => mux_tx_next(1),
        comp_results        => INTF_0x01_results
        
    );
    
    --===========--
    -- INTF 0x03 --
    --===========--
    
    INTF_0x03: entity work.DAB64x_DR_INTF_0x03_CMA
    generic map
    (
        Tclk_ns             => Tclk_ns,
        tag_n               => INTF_0x03_tag_n
    )
    port map 
    (
        clk                 => clk,
        BP_pulse            => BP_pulse,
        PARAM_INTF_0x03     => PARAM_INTF_0x03,
        ID                  => ID,
        out_data            => mux_tx_data(2),
        out_vld             => mux_tx_vld(2),
        out_next            => mux_tx_next(2),
        comp_results        => INTF_0x03_results
        
    );

    
    --=============--
    -- MULTIPLEXER --
    --=============--
    
    -- mux_CMI --
    mux_CMI: entity work.MM_CMI 
    generic map
    (
        tx_number       => mux_tx_number,
        rx_number       => mux_rx_number,
        data_size       => mux_data_size
    )
    port map 
    (
        clk             => clk,
        tx_data         => mux_tx_data,
        tx_vld          => mux_tx_vld,
        tx_next         => mux_tx_next,
        rx_data         => mux_rx_data,
        rx_vld          => mux_rx_vld,
        rx_next         => mux_rx_next,
        ARB_tx_vld      => ARB_mux_tx_vld,
        ARB_tx_next     => ARB_mux_tx_next,
        ARB_rx_vld      => ARB_mux_rx_vld,
        ARB_rx_next     => ARB_mux_rx_next,
        ARB_sel         => ARB_mux_sel
        
    );

    mux_CMI_ARB: entity work.priority_pkg_mux_ARB
    generic map
    (
        tx_number       => mux_tx_number,
        data_size       => mux_data_size
    )
    port map
    (
        clk             => clk,
        full_data       => mux_tx_data,
        ARB_tx_vld      => ARB_mux_tx_vld,
        ARB_tx_next     => ARB_mux_tx_next,
        ARB_rx_vld      => ARB_mux_rx_vld,
        ARB_rx_next     => ARB_mux_rx_next,
        ARB_sel         => ARB_mux_sel
    );


        
    
end architecture struct;
