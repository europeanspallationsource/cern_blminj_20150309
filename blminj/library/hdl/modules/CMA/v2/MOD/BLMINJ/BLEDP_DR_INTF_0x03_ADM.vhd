------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/02/2014
Module Name:    BLEDP_DR_INTF_0x03_ADM

-----------------
Short Description
-----------------

    ADM for BLEDP INTF 0x03.
    
------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    tx_data_size        := size with tag
    rx_data_size        := size without tag
    rx_number           := number of receivers

--------------
Implementation
--------------

     
-----------
Limitations
-----------
    
    

----------------
Missing Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity BLEDP_DR_INTF_0x03_ADM is

generic
(
    tx_data_size:       integer;
    rx_data_size:       integer;
    rx_number:          integer
);

port
(
    ADM_tx_data:    in  std_logic_vector(tx_data_size-1 downto 0);
    ADM_rx_data:    out std_logic_vector(rx_data_size-1 downto 0);
    ADM_sel:        out std_logic_vector(get_vsize_addr(rx_number)-1 downto 0)
);

end entity BLEDP_DR_INTF_0x03_ADM;


architecture struct of BLEDP_DR_INTF_0x03_ADM is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant ID_LSB:    integer := rx_data_size;
    constant ID_MSB:    integer := rx_data_size + get_vsize_addr(rx_number)-1;
     
              
    
BEGIN
    
    
    process(all) is
    begin
        ADM_rx_data <= ADM_tx_data(rx_data_size-1 downto 0);
        
        case ADM_tx_data(ID_MSB downto ID_LSB) is
            when "00011"    => ADM_sel <= "0";
            when "00101"    => ADM_sel <= "1";
            when others     => ADM_sel <= "0";
        end case;
    
    end process;
    
    
   
    
end architecture struct;
