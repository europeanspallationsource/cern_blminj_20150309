------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    DAB64x_DownstreamCore_CMA

-----------------
Short Description
-----------------


    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------



    
--------------
Implementation
--------------
    

    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DAB64x_DownstreamCore_CMA is

generic
(
    pkg_data_size:          integer := 66;
    VME_rdreq_data_size:    integer := 61;
    VME_wrreq_data_size:    integer := 133;
    VME_rdout_data_size:    integer := 64;
    VME_wrreq_addr_MSB:     integer := 132;
    VME_wrreq_byteena_MSB:  integer := 71;
    VME_wrreq_d_MSB:        integer := 63;
    num_rams:               integer := 4
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Control
    FINAL_PBL_pulse:    in  std_logic;
    INP_BLOCK:          in  std_logic;
    -- Package CMI Input
    pkg_data:           in  std_logic_vector(pkg_data_size-1 downto 0);
    pkg_vld:            in  std_logic;
    pkg_next:           out std_logic;
    -- RAM CMI Input
    VME_rdreq_data:     in  slv_array(num_rams-1 downto 0)(VME_rdreq_data_size-1 downto 0);
    VME_rdreq_vld:      in  std_logic_vector(num_rams-1 downto 0);
    VME_rdreq_next:     out std_logic_vector(num_rams-1 downto 0);
    
    VME_wrreq_data:     in  slv_array(num_rams-1 downto 0)(VME_wrreq_data_size-1 downto 0);
    VME_wrreq_vld:      in  std_logic_vector(num_rams-1 downto 0);
    VME_wrreq_next:     out std_logic_vector(num_rams-1 downto 0);
    -- RAM CMI Output
    VME_rdout_data:     out slv_array(num_rams-1 downto 0)(VME_rdout_data_size-1 downto 0);
    VME_rdout_vld:      out std_logic_vector(num_rams-1 downto 0) := (others => '0');
    VME_rdout_next:     in  std_logic_vector(num_rams-1 downto 0)
);

end entity DAB64x_DownstreamCore_CMA;


architecture struct of DAB64x_DownstreamCore_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- General
    constant header_size:               integer := 64;
    constant wr_addr_size:              integer := 32;
    
    -- RAMs
    constant RAM_data_size:             integer := 64;
    constant RAM_byteena_size:          integer := 8;
    
    constant RAM_EVO_numwords:          integer := 10496;
    constant RAM_EVO_data_size:         integer := RAM_data_size;
    constant RAM_EVO_addr_size:         integer := get_vsize_addr(RAM_EVO_numwords);
    constant RAM_EVO_byteena_size:      integer := RAM_byteena_size;
    constant RAM_EVO_data_MSB:          integer := RAM_EVO_data_size-1;
    constant RAM_EVO_addr_MSB:          integer := RAM_EVO_data_MSB+RAM_EVO_addr_size;
    constant RAM_EVO_byteena_MSB:       integer := 0;
    constant RAM_EVO_init_file:         string  := "UNUSED";
    constant RAM_EVO_type:              string  := "M-RAM";
    
    constant RAM_CAP_numwords:          integer := 4352;
    constant RAM_CAP_data_size:         integer := RAM_data_size;
    constant RAM_CAP_addr_size:         integer := get_vsize_addr(RAM_CAP_numwords);
    constant RAM_CAP_byteena_size:      integer := RAM_byteena_size;
    constant RAM_CAP_data_MSB:          integer := RAM_CAP_data_size-1;
    constant RAM_CAP_addr_MSB:          integer := RAM_CAP_data_MSB+RAM_CAP_addr_size;
    constant RAM_CAP_byteena_MSB:       integer := 0;
    constant RAM_CAP_init_file:         string  := "UNUSED";
    constant RAM_CAP_type:              string  := "M-RAM";
    
    constant RAM_LOSS_numwords:         integer := 512;
    constant RAM_LOSS_data_size:        integer := RAM_data_size;
    constant RAM_LOSS_addr_size:        integer := get_vsize_addr(RAM_LOSS_numwords);
    constant RAM_LOSS_byteena_size:     integer := RAM_byteena_size;
    constant RAM_LOSS_data_MSB:         integer := RAM_LOSS_data_size-1;
    constant RAM_LOSS_addr_MSB:         integer := RAM_LOSS_data_MSB+RAM_LOSS_addr_size;
    constant RAM_LOSS_byteena_MSB:      integer := 0;
    constant RAM_LOSS_init_file:        string  := "UNUSED";--"LOSS.mif";
    constant RAM_LOSS_type:             string  := "M4K";
    
    constant RAM_DIAG_numwords:         integer := 512;
    constant RAM_DIAG_data_size:        integer := RAM_data_size;
    constant RAM_DIAG_addr_size:        integer := get_vsize_addr(RAM_DIAG_numwords);
    constant RAM_DIAG_byteena_size:     integer := RAM_byteena_size;
    constant RAM_DIAG_data_MSB:         integer := RAM_DIAG_data_size-1;
    constant RAM_DIAG_addr_MSB:         integer := RAM_DIAG_data_MSB+RAM_DIAG_addr_size;
    constant RAM_DIAG_byteena_MSB:      integer := 0;
    constant RAM_DIAG_init_file:        string  := "DIAG.mif";
    constant RAM_DIAG_type:             string  := "M4K";
    
    -- VME Address MSBs
    constant VME_wrreq_EVO_addr_MSB:    integer := VME_wrreq_byteena_MSB+RAM_EVO_addr_size;
    constant VME_wrreq_CAP_addr_MSB:    integer := VME_wrreq_byteena_MSB+RAM_CAP_addr_size;
    constant VME_wrreq_LOSS_addr_MSB:   integer := VME_wrreq_byteena_MSB+RAM_LOSS_addr_size;
    constant VME_wrreq_DIAG_addr_MSB:   integer := VME_wrreq_byteena_MSB+RAM_DIAG_addr_size;
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- REM CRC
    constant remCRC_data_size:          integer := pkg_data_size;
    -- PKG GATE
    constant pkg_gate_data_size:        integer := pkg_data_size;
    
    -- PKG HEADER
    constant pkg_header_data_size:      integer := header_size;
    
    -- PKG BODY
    constant pkg_body_data_size:        integer := pkg_data_size-2;
    
    -- WRCtrl
    constant wrctrl_tx_data_size:       integer := pkg_body_data_size + wr_addr_size + 4; -- with ID
    constant wrctrl_rx_data_size:       integer := pkg_body_data_size + wr_addr_size; -- without ID
    constant wrctrl_rx_d_MSB:           integer := pkg_body_data_size-1;
    constant wrctrl_rx_addr_MSB:        integer := wrctrl_rx_data_size-1;
    
    constant wrctrl_rx_number:          integer := num_rams;
    
    -- DIAG SB
    constant DIAG_SB_data_size:         integer := wrctrl_rx_data_size;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- RAM Connections
    signal RAM_EVO_portA_addr:          std_logic_vector(RAM_EVO_addr_size-1 downto 0);
    signal RAM_EVO_portA_data:          std_logic_vector(RAM_EVO_data_size-1 downto 0);
    signal RAM_EVO_portA_byteena:       std_logic_vector(RAM_EVO_byteena_size-1 downto 0);
    signal RAM_EVO_portA_wren:          std_logic;
    signal RAM_EVO_portA_q:             std_logic_vector(RAM_EVO_data_size-1 downto 0);
    
    signal RAM_EVO_portB_addr:          std_logic_vector(RAM_EVO_addr_size-1 downto 0);
    signal RAM_EVO_portB_data:          std_logic_vector(RAM_EVO_data_size-1 downto 0);
    signal RAM_EVO_portB_byteena:       std_logic_vector(RAM_EVO_byteena_size-1 downto 0);
    signal RAM_EVO_portB_wren:          std_logic;
    signal RAM_EVO_portB_q:             std_logic_vector(RAM_EVO_data_size-1 downto 0);
    
    signal RAM_CAP_portA_addr:          std_logic_vector(RAM_CAP_addr_size-1 downto 0);
    signal RAM_CAP_portA_data:          std_logic_vector(RAM_CAP_data_size-1 downto 0);
    signal RAM_CAP_portA_byteena:       std_logic_vector(RAM_CAP_byteena_size-1 downto 0);
    signal RAM_CAP_portA_wren:          std_logic;
    signal RAM_CAP_portA_q:             std_logic_vector(RAM_CAP_data_size-1 downto 0);
    
    signal RAM_CAP_portB_addr:          std_logic_vector(RAM_CAP_addr_size-1 downto 0);
    signal RAM_CAP_portB_data:          std_logic_vector(RAM_CAP_data_size-1 downto 0);
    signal RAM_CAP_portB_byteena:       std_logic_vector(RAM_CAP_byteena_size-1 downto 0);
    signal RAM_CAP_portB_wren:          std_logic;
    signal RAM_CAP_portB_q:             std_logic_vector(RAM_CAP_data_size-1 downto 0);
    
    signal RAM_LOSS_portA_addr:         std_logic_vector(RAM_LOSS_addr_size-1 downto 0);
    signal RAM_LOSS_portA_data:         std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    signal RAM_LOSS_portA_byteena:      std_logic_vector(RAM_LOSS_byteena_size-1 downto 0);
    signal RAM_LOSS_portA_wren:         std_logic;
    signal RAM_LOSS_portA_q:            std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    
    signal RAM_LOSS_portB_addr:         std_logic_vector(RAM_LOSS_addr_size-1 downto 0);
    signal RAM_LOSS_portB_data:         std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    signal RAM_LOSS_portB_byteena:      std_logic_vector(RAM_LOSS_byteena_size-1 downto 0);
    signal RAM_LOSS_portB_wren:         std_logic;
    signal RAM_LOSS_portB_q:            std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    
    signal RAM_DIAG_portA_addr:         std_logic_vector(RAM_DIAG_addr_size-1 downto 0);
    signal RAM_DIAG_portA_data:         std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    signal RAM_DIAG_portA_byteena:      std_logic_vector(RAM_DIAG_byteena_size-1 downto 0);
    signal RAM_DIAG_portA_wren:         std_logic;
    signal RAM_DIAG_portA_q:            std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    
    signal RAM_DIAG_portB_addr:         std_logic_vector(RAM_DIAG_addr_size-1 downto 0);
    signal RAM_DIAG_portB_data:         std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    signal RAM_DIAG_portB_byteena:      std_logic_vector(RAM_DIAG_byteena_size-1 downto 0);
    signal RAM_DIAG_portB_wren:         std_logic;
    signal RAM_DIAG_portB_q:            std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- remCRC
    signal remCRC_data:                 std_logic_vector(remCRC_data_size-1 downto 0);
    signal remCRC_vld:                  std_logic;
    signal remCRC_next:                 std_logic;
    
    -- pkg_gate
    signal pkg_gate_data:               std_logic_vector(pkg_gate_data_size-1 downto 0);
    signal pkg_gate_vld:                std_logic;
    signal pkg_gate_next:               std_logic;
    
    -- pkg_header
    signal pkg_header_data:             std_logic_vector(pkg_header_data_size-1 downto 0);
    signal pkg_header_vld:              std_logic;
    signal pkg_header_next:             std_logic;
    
    -- pkg_body
    signal pkg_body_data:               std_logic_vector(pkg_body_data_size-1 downto 0);
    signal pkg_body_vld:                std_logic;
    signal pkg_body_next:               std_logic;
    
    -- wrctrl
    signal wrctrl_tx_data:              std_logic_vector(wrctrl_tx_data_size-1 downto 0);
    signal wrctrl_tx_vld:               std_logic;
    signal wrctrl_tx_next:              std_logic;
    signal wrctrl_rx_data:              slv_array(wrctrl_rx_number-1 downto 0)(wrctrl_rx_data_size-1 downto 0);
    signal wrctrl_rx_vld:               std_logic_vector(wrctrl_rx_number-1 downto 0);
    signal wrctrl_rx_next:              std_logic_vector(wrctrl_rx_number-1 downto 0);
    
    signal wrctrl_ADM_tx_data:          std_logic_vector(wrctrl_tx_data_size-1 downto 0);
    signal wrctrl_ADM_rx_data:          std_logic_vector(wrctrl_rx_data_size-1 downto 0);
    signal wrctrl_ADM_sel:              std_logic_vector(get_vsize_addr(wrctrl_rx_number)-1 downto 0);
    
    -- DIAG SB
    signal DIAG_SB_data:                std_logic_vector(DIAG_SB_data_size-1 downto 0);
    signal DIAG_SB_vld:                 std_logic;
    signal DIAG_SB_next:                std_logic;
    
BEGIN
    
    --===================--
    -- PACKAGE HANDLING  --
    --===================--
    
    -- remCRC32 --
    remCRC_CMA_inst: entity work.remCRC32_DATA64_CMA
    PORT MAP
    (
        clk             => clk,
        inp_data        => pkg_data,
        inp_vld         => pkg_vld,
        inp_next        => pkg_next,
        out_data        => remCRC_data,
        out_vld         => remCRC_vld,
        out_next        => remCRC_next
    );
    
    
    -- pkg gate --
    pkg_gate_CMA_inst: entity work.package_gate_CMA
    GENERIC MAP
    (
        data_size       => pkg_gate_data_size
    )
    PORT MAP
    (
        clk             => clk,
        stop            => INP_BLOCK,
        inp_data        => remCRC_data,
        inp_vld         => remCRC_vld,
        inp_next        => remCRC_next,
        out_data        => pkg_gate_data,
        out_vld         => pkg_gate_vld,
        out_next        => pkg_gate_next
    );
    
    
    -- package split --
    pkg_split_CMA_inst: entity work.package_split_CMA
    GENERIC MAP
    (
        inp_data_size       => pkg_gate_data_size,
        body_data_size      => pkg_body_data_size,
        header_data_size    => pkg_header_data_size,
        header_size         => header_size
    )
    PORT MAP
    (
        clk                 => clk,
        inp_data            => pkg_gate_data,
        inp_vld             => pkg_gate_vld,
        inp_next            => pkg_gate_next,
        body_data           => pkg_body_data,
        body_vld            => pkg_body_vld,
        body_next           => pkg_body_next,
        header_data         => pkg_header_data,
        header_vld          => pkg_header_vld,
        header_next         => pkg_header_next
    );
    
    --================--
    -- WRITE CONTROL  --
    --================--
    
    -- Write Control --
    StorageWRCtrl: entity work.DAB64x_StorageWriteCtrl_CMA
    GENERIC MAP
    (
        header_data_size    => pkg_header_data_size,
        body_data_size      => pkg_body_data_size,
        out_data_size       => wrctrl_tx_data_size
    )
    PORT MAP
    (
        clk                 => clk,
        body_data           => pkg_body_data,
        body_vld            => pkg_body_vld,
        body_next           => pkg_body_next,
        header_data         => pkg_header_data,
        header_vld          => pkg_header_vld,
        header_next         => pkg_header_next,
        out_data            => wrctrl_tx_data,
        out_vld             => wrctrl_tx_vld,
        out_next            => wrctrl_tx_next
    );
    
    -- wrctrl T-CMI -- 
    wrctrl_CMI: entity work.T_CMI
    GENERIC MAP
    (
        rx_number       => wrctrl_rx_number,
        tx_data_size    => wrctrl_tx_data_size,
        rx_data_size    => wrctrl_rx_data_size
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => wrctrl_tx_data,
        tx_vld          => wrctrl_tx_vld,
        tx_next         => wrctrl_tx_next,
        rx_data         => wrctrl_rx_data,
        rx_vld          => wrctrl_rx_vld,
        rx_next         => wrctrl_rx_next,
        ADM_tx_data     => wrctrl_ADM_tx_data,
        ADM_rx_data     => wrctrl_ADM_rx_data,
        ADM_sel         => wrctrl_ADM_sel
    );
    
    -- ADM vme_wrreq --
    wrctrl_ADM: entity work.ID_ADM
    GENERIC MAP
    (
        rx_number       => wrctrl_rx_number,
        tx_data_size    => wrctrl_tx_data_size,
        rx_data_size    => wrctrl_rx_data_size
    )
    PORT MAP
    (
        ADM_tx_data     => wrctrl_ADM_tx_data,
        ADM_rx_data     => wrctrl_ADM_rx_data,
        ADM_sel         => wrctrl_ADM_sel
    );
    
    
    --================--
    -- EVO STRUCTURES --
    --================--

    -- RAM Access Port DSCore --
    EVO_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => wrctrl_rx_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => RAM_EVO_byteena_MSB,
        wrreq_data_MSB      => RAM_EVO_data_MSB,
        wrreq_addr_MSB      => RAM_EVO_addr_MSB,
        RAM_addr_size       => RAM_EVO_addr_size,
        RAM_data_size       => RAM_EVO_data_size,
        RAM_byteena_size    => RAM_EVO_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => wrctrl_rx_data(0),
        wrreq_vld       => wrctrl_rx_vld(0),
        wrreq_next      => wrctrl_rx_next(0),
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_EVO_portB_addr,
        RAM_data        => RAM_EVO_portB_data,
        RAM_byteena     => RAM_EVO_portB_byteena,
        RAM_wren        => RAM_EVO_portB_wren,
        RAM_q           => RAM_EVO_portB_q
    );
    
    -- EVO RAM --
    EVO_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_EVO_numwords,
        portA_data_size     => RAM_EVO_data_size,
        portA_addr_size     => RAM_EVO_addr_size,
        portA_byteena_size  => RAM_EVO_byteena_size,
        portB_numwords      => RAM_EVO_numwords,
        portB_data_size     => RAM_EVO_data_size,
        portB_addr_size     => RAM_EVO_addr_size,
        portB_byteena_size  => RAM_EVO_byteena_size,
        init_file           => RAM_EVO_init_file,
        ram_type            => RAM_EVO_type,
        read_during_write   => "DONT_CARE",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_EVO_portA_addr,
        portA_data      => RAM_EVO_portA_data,
        portA_byteena   => RAM_EVO_portA_byteena,
        portA_wren      => RAM_EVO_portA_wren,
        portA_q         => RAM_EVO_portA_q,
        portB_addr      => RAM_EVO_portB_addr,
        portB_data      => RAM_EVO_portB_data,
        portB_byteena   => RAM_EVO_portB_byteena,
        portB_wren      => RAM_EVO_portB_wren,
        portB_q         => RAM_EVO_portB_q
    );
    
    -- EVO RAM Access Port VME --
    EVO_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_EVO_addr_MSB,
        RAM_addr_size       => RAM_EVO_addr_size,
        RAM_data_size       => RAM_EVO_data_size,
        RAM_byteena_size    => RAM_EVO_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_data(0),
        rdreq_vld       => VME_rdreq_vld(0),
        rdreq_next      => VME_rdreq_next(0),
        wrreq_data      => VME_wrreq_data(0),
        wrreq_vld       => VME_wrreq_vld(0),
        wrreq_next      => VME_wrreq_next(0),
        rdout_data      => VME_rdout_data(0),
        rdout_vld       => VME_rdout_vld(0),
        rdout_next      => VME_rdout_next(0),
        RAM_addr        => RAM_EVO_portA_addr,
        RAM_data        => RAM_EVO_portA_data,
        RAM_byteena     => RAM_EVO_portA_byteena,
        RAM_wren        => RAM_EVO_portA_wren,
        RAM_q           => RAM_EVO_portA_q
    );
    
    
    --================--
    -- CAP STRUCTURES --
    --================--
    
    -- CAP RAM Access Port DS Core --
    CAP_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => wrctrl_rx_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => RAM_CAP_byteena_MSB,
        wrreq_data_MSB      => RAM_CAP_data_MSB,
        wrreq_addr_MSB      => RAM_CAP_addr_MSB,
        RAM_addr_size       => RAM_CAP_addr_size,
        RAM_data_size       => RAM_CAP_data_size,
        RAM_byteena_size    => RAM_CAP_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => wrctrl_rx_data(1),
        wrreq_vld       => wrctrl_rx_vld(1),
        wrreq_next      => wrctrl_rx_next(1),
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_CAP_portB_addr,
        RAM_data        => RAM_CAP_portB_data,
        RAM_byteena     => RAM_CAP_portB_byteena,
        RAM_wren        => RAM_CAP_portB_wren,
        RAM_q           => RAM_CAP_portB_q
    );
    
    -- CAP RAM --
    CAP_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_CAP_numwords,
        portA_data_size     => RAM_CAP_data_size,
        portA_addr_size     => RAM_CAP_addr_size,
        portA_byteena_size  => RAM_CAP_byteena_size,
        portB_numwords      => RAM_CAP_numwords,
        portB_data_size     => RAM_CAP_data_size,
        portB_addr_size     => RAM_CAP_addr_size,
        portB_byteena_size  => RAM_CAP_byteena_size,
        init_file           => RAM_CAP_init_file,
        ram_type            => RAM_CAP_type,
        read_during_write   => "DONT_CARE",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_CAP_portA_addr,
        portA_data      => RAM_CAP_portA_data,
        portA_byteena   => RAM_CAP_portA_byteena,
        portA_wren      => RAM_CAP_portA_wren,
        portA_q         => RAM_CAP_portA_q,
        portB_addr      => RAM_CAP_portB_addr,
        portB_data      => RAM_CAP_portB_data,
        portB_byteena   => RAM_CAP_portB_byteena,
        portB_wren      => RAM_CAP_portB_wren,
        portB_q         => RAM_CAP_portB_q
    );
    
    -- CAP RAM Access Port VME --
    CAP_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_CAP_addr_MSB,
        RAM_addr_size       => RAM_CAP_addr_size,
        RAM_data_size       => RAM_CAP_data_size,
        RAM_byteena_size    => RAM_CAP_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_data(1),
        rdreq_vld       => VME_rdreq_vld(1),
        rdreq_next      => VME_rdreq_next(1),
        wrreq_data      => VME_wrreq_data(1),
        wrreq_vld       => VME_wrreq_vld(1),
        wrreq_next      => VME_wrreq_next(1),
        rdout_data      => VME_rdout_data(1),
        rdout_vld       => VME_rdout_vld(1),
        rdout_next      => VME_rdout_next(1),
        RAM_addr        => RAM_CAP_portA_addr,
        RAM_data        => RAM_CAP_portA_data,
        RAM_byteena     => RAM_CAP_portA_byteena,
        RAM_wren        => RAM_CAP_portA_wren,
        RAM_q           => RAM_CAP_portA_q
    );
    
    
   
    --=================--
    -- LOSS STRUCTURES --
    --=================--
    
    -- LOSS RAM Access Port DS Core --
    LOSS_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => wrctrl_rx_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => RAM_LOSS_byteena_MSB,
        wrreq_data_MSB      => RAM_LOSS_data_MSB,
        wrreq_addr_MSB      => RAM_LOSS_addr_MSB,
        RAM_addr_size       => RAM_LOSS_addr_size,
        RAM_data_size       => RAM_LOSS_data_size,
        RAM_byteena_size    => RAM_LOSS_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => wrctrl_rx_data(2),
        wrreq_vld       => wrctrl_rx_vld(2),
        wrreq_next      => wrctrl_rx_next(2),
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_LOSS_portB_addr,
        RAM_data        => RAM_LOSS_portB_data,
        RAM_byteena     => RAM_LOSS_portB_byteena,
        RAM_wren        => RAM_LOSS_portB_wren,
        RAM_q           => RAM_LOSS_portB_q
    );
    
    -- LOSS RAM --
    LOSS_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_LOSS_numwords,
        portA_data_size     => RAM_LOSS_data_size,
        portA_addr_size     => RAM_LOSS_addr_size,
        portA_byteena_size  => RAM_LOSS_byteena_size,
        portB_numwords      => RAM_LOSS_numwords,
        portB_data_size     => RAM_LOSS_data_size,
        portB_addr_size     => RAM_LOSS_addr_size,
        portB_byteena_size  => RAM_LOSS_byteena_size,
        init_file           => RAM_LOSS_init_file,
        ram_type            => RAM_LOSS_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_LOSS_portA_addr,
        portA_data      => RAM_LOSS_portA_data,
        portA_byteena   => RAM_LOSS_portA_byteena,
        portA_wren      => RAM_LOSS_portA_wren,
        portA_q         => RAM_LOSS_portA_q,
        portB_addr      => RAM_LOSS_portB_addr,
        portB_data      => RAM_LOSS_portB_data,
        portB_byteena   => RAM_LOSS_portB_byteena,
        portB_wren      => RAM_LOSS_portB_wren,
        portB_q         => RAM_LOSS_portB_q
    );
    
    -- LOSS RAM Access Port VME --
    LOSS_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_LOSS_addr_MSB,
        RAM_addr_size       => RAM_LOSS_addr_size,
        RAM_data_size       => RAM_LOSS_data_size,
        RAM_byteena_size    => RAM_LOSS_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_data(2),
        rdreq_vld       => VME_rdreq_vld(2),
        rdreq_next      => VME_rdreq_next(2),
        wrreq_data      => VME_wrreq_data(2),
        wrreq_vld       => VME_wrreq_vld(2),
        wrreq_next      => VME_wrreq_next(2),
        rdout_data      => VME_rdout_data(2),
        rdout_vld       => VME_rdout_vld(2),
        rdout_next      => VME_rdout_next(2),
        RAM_addr        => RAM_LOSS_portA_addr,
        RAM_data        => RAM_LOSS_portA_data,
        RAM_byteena     => RAM_LOSS_portA_byteena,
        RAM_wren        => RAM_LOSS_portA_wren,
        RAM_q           => RAM_LOSS_portA_q
    );
    
    
    --=================--
    -- DIAG STRUCTURES --
    --=================--
    
    DIAG_SB_data        <= wrctrl_rx_data(3)(DIAG_SB_data_size-1 downto 0);
    DIAG_SB_vld         <= wrctrl_rx_vld(3);
    wrctrl_rx_next(3)   <= DIAG_SB_next; 
    
    
    -- Scoreboard --
--    DIAG_SB_CMA: entity work.scoreboard_CMA
--    GENERIC MAP
--    (
--        inp_data_size       => wrctrl_rx_data_size,
--        inp_addr_MSB        => DIAG_SB_addr_MSB,
--        inp_d_MSB           => DIAG_SB_d_MSB,
--        out_data_size       => DIAG_SB_data_size,
--        out_addr_size       => RAM_DIAG_addr_size,
--        out_d_size          => RAM_DIAG_data_size
--    )
--    PORT MAP
--    (
--        clk                 => clk,
--        pbl                 => FINAL_PBL_pulse,
--        PARAM_SB_base_addr  => DIAG_SB_base_addr,
--        inp_data            => wrctrl_rx_data(3),
--        inp_vld             => wrctrl_rx_vld(3),
--        inp_next            => wrctrl_rx_next(3),
--        out_data            => DIAG_SB_data,
--        out_vld             => DIAG_SB_vld,
--        out_next            => DIAG_SB_next
--    );

    -- DIAG RAM Access Port DS Core --
    DIAG_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => DIAG_SB_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => RAM_DIAG_byteena_MSB,
        wrreq_data_MSB      => RAM_DIAG_data_MSB,
        wrreq_addr_MSB      => RAM_DIAG_addr_MSB,
        RAM_addr_size       => RAM_DIAG_addr_size,
        RAM_data_size       => RAM_DIAG_data_size,
        RAM_byteena_size    => RAM_DIAG_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => DIAG_SB_data,
        wrreq_vld       => DIAG_SB_vld,
        wrreq_next      => DIAG_SB_next,
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_DIAG_portB_addr,
        RAM_data        => RAM_DIAG_portB_data,
        RAM_byteena     => RAM_DIAG_portB_byteena,
        RAM_wren        => RAM_DIAG_portB_wren,
        RAM_q           => RAM_DIAG_portB_q
    );
    
    -- DIAG RAM --
    DIAG_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_DIAG_numwords,
        portA_data_size     => RAM_DIAG_data_size,
        portA_addr_size     => RAM_DIAG_addr_size,
        portA_byteena_size  => RAM_DIAG_byteena_size,
        portB_numwords      => RAM_DIAG_numwords,
        portB_data_size     => RAM_DIAG_data_size,
        portB_addr_size     => RAM_DIAG_addr_size,
        portB_byteena_size  => RAM_DIAG_byteena_size,
        init_file           => RAM_DIAG_init_file,
        ram_type            => RAM_DIAG_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_DIAG_portA_addr,
        portA_data      => RAM_DIAG_portA_data,
        portA_byteena   => RAM_DIAG_portA_byteena,
        portA_wren      => RAM_DIAG_portA_wren,
        portA_q         => RAM_DIAG_portA_q,
        portB_addr      => RAM_DIAG_portB_addr,
        portB_data      => RAM_DIAG_portB_data,
        portB_byteena   => RAM_DIAG_portB_byteena,
        portB_wren      => RAM_DIAG_portB_wren,
        portB_q         => RAM_DIAG_portB_q
    );
    
    -- DIAG RAM Access Port VME --
    DIAG_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_DIAG_addr_MSB,
        RAM_addr_size       => RAM_DIAG_addr_size,
        RAM_data_size       => RAM_DIAG_data_size,
        RAM_byteena_size    => RAM_DIAG_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_data(3),
        rdreq_vld       => VME_rdreq_vld(3),
        rdreq_next      => VME_rdreq_next(3),
        wrreq_data      => VME_wrreq_data(3),
        wrreq_vld       => VME_wrreq_vld(3),
        wrreq_next      => VME_wrreq_next(3),
        rdout_data      => VME_rdout_data(3),
        rdout_vld       => VME_rdout_vld(3),
        rdout_next      => VME_rdout_next(3),
        RAM_addr        => RAM_DIAG_portA_addr,
        RAM_data        => RAM_DIAG_portA_data,
        RAM_byteena     => RAM_DIAG_portA_byteena,
        RAM_wren        => RAM_DIAG_portA_wren,
        RAM_q           => RAM_DIAG_portA_q
    );
    
end architecture struct;
