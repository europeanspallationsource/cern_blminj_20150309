------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2014
Module Name:    BLEPM_TIMING_TABLE


-----------------
Short Description
-----------------

    Parameter Table for the Timing Controller of the BLEPM card.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    PARAM_CAP_START     := number of CAP cycles 
    
    
--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity BLEPM_TIMING_TABLE is
port
(
    -- Clock
    clk:                in  std_logic;
    -- CMI Input
    inp_data:           in  std_logic_vector(15 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- Parameters
    PARAM_CAP_START:    out std_logic_vector(31 downto 0);
    PARAM_CAP_STOP:     out std_logic_vector(31 downto 0);
    PARAM_CAP_SMPLRATE: out std_logic_vector(31 downto 0);
    PARAM_EVO_SMPLRATE: out std_logic_vector(31 downto 0);
    PARAM_MS_SMPLRATE:  out std_logic_vector(31 downto 0);
    PARAM_MACH_TYPE:    out std_logic_vector(31 downto 0)
    
);

end entity BLEPM_TIMING_TABLE;


architecture struct of BLEPM_TIMING_TABLE is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant param_data_size:   integer := 16;
    constant param_table_size:  integer := 12;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Table Output
    signal param_vector:        slv_array(0 to param_table_size-1)(param_data_size-1 downto 0);
    
BEGIN
    
    --===================--
    -- BASIC PARAM TABLE --
    --===================--  
    
    param_table_CMA_inst: entity work.param_table_CMA
    generic map
    (
        inp_data_size   => param_data_size,
        table_size      => param_table_size
    )
    port map 
    (
        clk         => clk,
        inp_data    => inp_data,
        inp_vld     => inp_vld,
        inp_next    => inp_next,
        params      => param_vector
    );
    
    
    --================--
    -- VECTOR MAPPING --
    --================-- 
        
    --PARAM_CAP_START       <= param_vector(0) & param_vector(1);
    --PARAM_CAP_STOP        <= param_vector(2) & param_vector(3);
    --PARAM_CAP_SMPLRATE    <= param_vector(4) & param_vector(5);
    --PARAM_EVO_SMPLRATE    <= param_vector(6) & param_vector(7);
    --PARAM_MS_SMPLRATE     <= param_vector(8) & param_vector(9);
    --PARAM_MACH_TYPE       <= param_vector(10) & param_vector(11);
    
    
    PARAM_CAP_START     <= 32UX"12C";
    PARAM_CAP_STOP      <= 32UX"2BC";
    PARAM_CAP_SMPLRATE  <= 32UX"18F";
    PARAM_EVO_SMPLRATE  <= 32UX"1F3";
    PARAM_MS_SMPLRATE   <= 32UX"1F3";
    PARAM_MACH_TYPE     <= 32UX"2"; -- PSB
    
    
     
 
end architecture struct;