------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/09/2014
Module Name:    BLEDP_DR_INTF_0x03_CMA


-----------------
Short Description
-----------------

    Interface 0x03 (SHT15 Sensor) of the BLEDP card.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    header_data_size    := size of the header CMI input data port
    body_data_size      := size of the body CMI input data port
    out_data_size       := size of the output CMI data port

--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity BLEDP_DR_INTF_0x03_CMA is

generic
(
    Tclk_ns:            integer := 8;
    tag_n:              integer := 2
);

port
(   
    -- Clock
    clk:                in  std_logic;
    -- Timing Pulses
    BP_pulse:           in  std_logic;
    -- Parameter
    PARAM_INTF_0x03:    in  std_logic_vector(15 downto 0);
    -- Interface
    SHT15_SCK:          out std_logic;
    SHT15_DATA:         inout std_logic;
    -- CMA Output
    out_data:           out std_logic_vector(17 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic;
    -- Comparator Results
    comp_results:       out std_logic
);

end entity BLEDP_DR_INTF_0x03_CMA;


architecture struct of BLEDP_DR_INTF_0x03_CMA is
    
   
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- rdreq_CMA
    constant rdreq_data_size:       integer := 8;

    -- rdout_CMA
    constant rdout_tx_data_size:    integer := 21;
    constant rdout_rx_data_size:    integer := 16;
    constant rdout_rx_number:       integer := tag_n;
    -- comp_CMA
    constant comp_data_size:        integer := rdout_rx_data_size;
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant device_family:         string := "Cyclone IV GX";
    constant init_file:             string := "sgen_sht15.mif";
    constant param_data_size:       integer := 16;
    constant comp_type:             integer := 1; -- check overTH
    constant header_size:           integer := 16;
    constant pkg_size:              integer := integer(CEIL(real(rdout_rx_data_size)/16.0));
    constant headers:               slv_array(tag_n-1 downto 0)(header_size-1 downto 0) := (0       => 16X"0303",
                                                                                            1       => 16X"0305",
                                                                                            others  => "0000"); --INTF+TAG
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- rdreq_CMA
    signal rdreq_data:              std_logic_vector(rdreq_data_size-1 downto 0);
    signal rdreq_vld:               std_logic;
    signal rdreq_next:              std_logic;
    
    -- rdout_CMA
    signal rdout_tx_data:           std_logic_vector(rdout_tx_data_size-1 downto 0);
    signal rdout_tx_vld:            std_logic;
    signal rdout_tx_next:           std_logic;
    signal rdout_rx_data:           slv_array(rdout_rx_number-1 downto 0)(rdout_rx_data_size-1 downto 0);
    signal rdout_rx_vld:            std_logic_vector(rdout_rx_number-1 downto 0);
    signal rdout_rx_next:           std_logic_vector(rdout_rx_number-1 downto 0);
    
    signal rdout_ADM_tx_data:       std_logic_vector(rdout_tx_data_size-1 downto 0);
    signal rdout_ADM_rx_data:       std_logic_vector(rdout_rx_data_size-1 downto 0);
    signal rdout_ADM_sel:           std_logic_vector(get_vsize_addr(rdout_rx_number)-1 downto 0);
    
    -- comp_CMA
    signal comp_data:               slv_array(tag_n-1 downto 0)(comp_data_size-1 downto 0);
    signal comp_vld:                std_logic_vector(tag_n-1 downto 0);
    signal comp_next:               std_logic_vector(tag_n-1 downto 0);
    
    
    
BEGIN
    
    -- Sequence Gen 
    seq_generator_CMA_inst: entity work.seq_generator_CMA
    GENERIC MAP
    (
        data_size       => rdreq_data_size,
        init_file       => init_file,
        numwords        => tag_n,
        device_family   => device_family
    )
    PORT MAP
    (
        clk             => clk,
        trigger         => BP_pulse,
        out_data        => rdreq_data,
        out_vld         => rdreq_vld,
        out_next        => rdreq_next
        
    );
    
    
    -- Interface --
    SHT15_CMA_inst: entity work.SHT15_CMA
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 4000
    )
    PORT MAP
    (
        clk             => clk,
        DATA            => SHT15_DATA,
        SCK             => SHT15_SCK,
        rdreq_data      => rdreq_data,
        rdreq_vld       => rdreq_vld,
        rdreq_next      => rdreq_next,
        rdout_data      => rdout_tx_data,
        rdout_vld       => rdout_tx_vld,
        rdout_next      => rdout_tx_next
    );
    
    
    -- rdout_CMA --
    rdout_CMI: entity work.T_CMI 
    generic map
    (
        rx_number       => rdout_rx_number,
        tx_data_size    => rdout_tx_data_size,
        rx_data_size    => rdout_rx_data_size
    )
    port map 
    (
        clk             => clk,
        tx_data         => rdout_tx_data,
        tx_vld          => rdout_tx_vld,
        tx_next         => rdout_tx_next,
        rx_data         => rdout_rx_data,
        rx_vld          => rdout_rx_vld,
        rx_next         => rdout_rx_next,
        ADM_tx_data     => rdout_ADM_tx_data,
        ADM_rx_data     => rdout_ADM_rx_data,
        ADM_sel         => rdout_ADM_sel
    );

    rdout_ADM: entity work.BLEDP_DR_INTF_0x03_ADM
    generic map
    (
        rx_number       => rdout_rx_number,
        tx_data_size    => rdout_tx_data_size,
        rx_data_size    => rdout_rx_data_size
    )
    port map
    (
        ADM_tx_data     => rdout_ADM_tx_data,
        ADM_rx_data     => rdout_ADM_rx_data,
        ADM_sel         => rdout_ADM_sel
    );
    

        
    -- Comparator
    compare_pass_CMA_inst: entity work.compare_pass_CMA
    GENERIC MAP
    (
        data_size           => rdout_rx_data_size,
        param_data_size     => param_data_size,
        comp_type           => comp_type
    )
    PORT MAP
    (
        clk                 => clk,
        PARAM_comp_value    => PARAM_INTF_0x03,
        inp_data            => rdout_rx_data(0),
        inp_vld             => rdout_rx_vld(0),
        inp_next            => rdout_rx_next(0),
        out_data            => comp_data(0),
        out_vld             => comp_vld(0),
        out_next            => comp_next(0),
        result              => comp_results
    );
    
    -- No Comparator Mapping --
    comp_data(1)        <= rdout_rx_data(1);
    comp_vld(1)         <= rdout_rx_vld(1);
    rdout_rx_next(1)    <= comp_next(1);

    
    -- DR Processing --
    DR_Processing_CMA_inst: entity work.DR_Processing_CMA
    GENERIC MAP
    (
        inp_data_size       => rdout_rx_data_size,
        header_size         => header_size,
        pkg_size            => pkg_size,
        tag_n               => tag_n
    )
    PORT MAP
    (
        clk                 => clk,
        UPDATE_pulse        => BP_pulse,
        PARAM_headers       => headers,
        inp_data            => comp_data,
        inp_vld             => comp_vld,
        inp_next            => comp_next,
        out_data            => out_data,
        out_vld             => out_vld,
        out_next            => out_next
    );
    
    
end architecture struct;
