------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/05/2014
Module Name:    BLEPM_DiagnosticPackaging_CMA

-----------------
Short Description
-----------------

    This module creates the packages from the Diagnostic Reader on the BLEPM card.

------------
Dependencies
------------

    vhdl_func_pkg
    BLMINJ_pkg
    combine_CMA
    max_inc_cnt
    PackageCreator64_CMA
 
    
------------------
Generics/Constants
------------------
    
    inp_data_size       := size of the CMI input data bus
    out_data_size       := size of the CMI output data bus
    
    combined_data_size  := size of the combined_CMI data bus
    combined_rx_number  := number of the combined_CMI receiver modules
    
--------------
Implementation
--------------
    

    

-----------
Limitations
-----------
    

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    use work.BLMINJ_pkg.all;
   
entity BLEPM_DiagnosticPackaging_CMA is

port
(
    -- Clock
    clk:            in  std_logic;
    -- Timing Information
    BP_CNT:         in  std_logic_vector(BLMINJ_TIMESTAMP_size-1 downto 0);
    -- CMI Input
    inp_data:       in  std_logic_vector(15 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(65 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity BLEPM_DiagnosticPackaging_CMA;


architecture struct of BLEPM_DiagnosticPackaging_CMA is
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    constant inp_data_size:         integer := 16;
    constant out_data_size:         integer := 66;
    
    -- combined
    constant combined_data_size:    integer := out_data_size-2;
    constant combined_rx_number:    integer := 1;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal header:                  std_logic_vector(BLMINJ_header_size-1 downto 0);
    signal pkg_done:                std_logic;
    signal DIAG_PAGE_CNT:           std_logic_vector(BLMINJ_PAGE_NO_size-1 downto 0);
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    --combined
    signal combined_tx_data:        std_logic_vector(combined_data_size-1 downto 0);
    signal combined_tx_vld:         std_logic;
    signal combined_tx_next:        std_logic;
    signal combined_rx_data:        slv_array(combined_rx_number-1 downto 0)(combined_data_size-1 downto 0);
    signal combined_rx_vld:         std_logic_vector(combined_rx_number-1 downto 0);
    signal combined_rx_next:        std_logic_vector(combined_rx_number-1 downto 0);
 
BEGIN
    
    --==============--
    -- PAGE COUNTER --
    --==============--
    
    pageCNT_inst : entity work.max_inc_cnt 
    generic map
    (
        data_size   => BLMINJ_PAGE_NO_size,
        max         => DIAG_PAGES_BLEPM     
    )
    port map 
    (
        clk             => clk,
        inc             => pkg_done,
        cnt             => DIAG_PAGE_CNT       
    );
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    header <= BLEPM_CARD_NO & DIAG_TYPE & DIAG_SPC1 & DIAG_SPC2 & DIAG_PAGE_CNT & BP_CNT;
    
    --=========--
    -- COMBINE --
    --=========--
    
    combine_CMA_inst : entity work.combine_CMA 
    generic map
    (
        inp_data_size   => inp_data_size,
        out_data_size   => combined_data_size      
    )
    port map 
    (
        clk             => clk,
        inp_data        => inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => combined_tx_data,
        out_vld         => combined_tx_vld,
        out_next        => combined_tx_next
    );
    
    --==============--
    -- COMBINED CMI --
    --==============--
    
    combined_CMI : entity work.CMI 
    generic map
    (
        rx_number   => combined_rx_number,
        data_size   => combined_data_size
    )
    port map 
    (
        clk         => clk,
        tx_data     => combined_tx_data,
        tx_vld      => combined_tx_vld,
        tx_next     => combined_tx_next,
        rx_data     => combined_rx_data,
        rx_vld      => combined_rx_vld,
        rx_next     => combined_rx_next
    );
    
    --====================--
    -- PACKAGE CREATOR 64 --
    --====================--
    
    PackageCreator64_CMA_inst : entity work.PackageCreator64_CMA 
    generic map
    (
        pkg_size            => DIAG_PKG_SIZE_64,
        header_size         => BLMINJ_header_size       
    )
    port map 
    (
        clk             => clk,
        PARAM_header    => header,
        pkg_done        => pkg_done,
        inp_data        => combined_rx_data(0),
        inp_vld         => combined_rx_vld(0),
        inp_next        => combined_rx_next(0),
        out_data        => out_data,
        out_vld         => out_vld,
        out_next        => out_next
    );
    
    
end architecture struct;
