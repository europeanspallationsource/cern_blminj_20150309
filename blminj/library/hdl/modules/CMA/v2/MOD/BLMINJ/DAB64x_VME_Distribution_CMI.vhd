------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    DAB64x_VME_Distribution_CMI

-----------------
Short Description
-----------------

    This module implements the CMI connections between the CMA VME Core and the connected RAM blocks.
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------



    
--------------
Implementation
--------------
    

    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DAB64x_VME_Distribution_CMI is

generic
(
    VME_rdreq_data_size:    integer := 61;
    VME_wrreq_data_size:    integer := 133;
    VME_rdout_data_size:    integer := 64;
    num_rams:               integer
    
);

port
(
    -- Clock
    clk:                    in  std_logic;
    -- CMI IOs
    VME_rdreq_tx_data:      in  std_logic_vector(VME_rdreq_data_size-1 downto 0);
    VME_rdreq_tx_vld:       in  std_logic;
    VME_rdreq_tx_next:      out std_logic;
    VME_rdreq_rx_data:      out slv_array(num_rams-1 downto 0)(VME_rdreq_data_size-1 downto 0);
    VME_rdreq_rx_vld:       out std_logic_vector(num_rams-1 downto 0);
    VME_rdreq_rx_next:      in  std_logic_vector(num_rams-1 downto 0);
    
    VME_wrreq_tx_data:      in  std_logic_vector(VME_wrreq_data_size-1 downto 0);
    VME_wrreq_tx_vld:       in  std_logic;
    VME_wrreq_tx_next:      out std_logic;
    VME_wrreq_rx_data:      out slv_array(num_rams-1 downto 0)(VME_wrreq_data_size-1 downto 0);
    VME_wrreq_rx_vld:       out std_logic_vector(num_rams-1 downto 0);
    VME_wrreq_rx_next:      in  std_logic_vector(num_rams-1 downto 0);
    
    VME_rdout_tx_data:      in  slv_array(num_rams-1 downto 0)(VME_rdout_data_size-1 downto 0);
    VME_rdout_tx_vld:       in  std_logic_vector(num_rams-1 downto 0);
    VME_rdout_tx_next:      out std_logic_vector(num_rams-1 downto 0);
    VME_rdout_rx_data:      out slv_array(0 downto 0)(VME_rdout_data_size-1 downto 0);
    VME_rdout_rx_vld:       out std_logic_vector(0 downto 0);
    VME_rdout_rx_next:      in  std_logic_vector(0 downto 0)
);

end entity DAB64x_VME_Distribution_CMI;


architecture struct of DAB64x_VME_Distribution_CMI is

    --===============--
    -- CMA CONSTANTS --
    --===============--  

    -- VME wrreq
    constant VME_wrreq_rx_number:       integer := num_rams;    
    constant VME_wrreq_addr_size:       integer := 61;
    
    -- VME rdreq
    constant VME_rdreq_rx_number:       integer := num_rams;
    constant VME_rdreq_addr_size:       integer := 61;
    
    -- VME rdout
    constant VME_rdout_tx_number:       integer := num_rams;
    constant VME_rdout_rx_number:       integer := 1;

    --=============--
    -- CMA SIGNALS --
    --=============--

    -- VME_rdreq
    signal VME_rdreq_ADM_tx_data:       std_logic_vector(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_ADM_rx_data:       std_logic_vector(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_ADM_sel:           std_logic_vector(get_vsize_addr(VME_rdreq_rx_number)-1 downto 0);
    
    -- VME_wrreq
    signal VME_wrreq_ADM_tx_data:       std_logic_vector(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_ADM_rx_data:       std_logic_vector(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_ADM_sel:           std_logic_vector(get_vsize_addr(VME_wrreq_rx_number)-1 downto 0);
    
    -- VME_rdout
    signal VME_rdout_ARB_tx_vld:        std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_ARB_tx_next:       std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_ARB_rx_vld:        std_logic;
    signal VME_rdout_ARB_rx_next:       std_logic;
    signal VME_rdout_ARB_sel:           std_logic_vector(get_vsize_addr(VME_rdout_tx_number)-1 downto 0);
    
BEGIN

    
    
    --================--
    -- VME Connection --
    --================--
    
    -- VME_wrreq --
    wrreq_CMI: entity work.T_CMI
    GENERIC MAP
    (
        rx_number       => VME_wrreq_rx_number,
        tx_data_size    => VME_wrreq_data_size,
        rx_data_size    => VME_wrreq_data_size
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => VME_wrreq_tx_data,
        tx_vld          => VME_wrreq_tx_vld,
        tx_next         => VME_wrreq_tx_next,
        rx_data         => VME_wrreq_rx_data,
        rx_vld          => VME_wrreq_rx_vld,
        rx_next         => VME_wrreq_rx_next,
        ADM_tx_data     => VME_wrreq_ADM_tx_data,
        ADM_rx_data     => VME_wrreq_ADM_rx_data,
        ADM_sel         => VME_wrreq_ADM_sel
    );
    
    -- ADM VME_wrreq --
    wrreq_ADM: entity work.DAB64x_VME_ADM
    GENERIC MAP
    (
        rx_number       => VME_wrreq_rx_number,
        data_size       => VME_wrreq_data_size,
        addr_size       => VME_wrreq_addr_size
    )
    PORT MAP
    (
        ADM_tx_data     => VME_wrreq_ADM_tx_data,
        ADM_rx_data     => VME_wrreq_ADM_rx_data,
        ADM_sel         => VME_wrreq_ADM_sel
    );
    
    -- VME_rdreq --
    rdreq_CMI: entity work.T_CMI
    GENERIC MAP
    (
        rx_number       => VME_rdreq_rx_number,
        tx_data_size    => VME_rdreq_data_size,
        rx_data_size    => VME_rdreq_data_size
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => VME_rdreq_tx_data,
        tx_vld          => VME_rdreq_tx_vld,
        tx_next         => VME_rdreq_tx_next,
        rx_data         => VME_rdreq_rx_data,
        rx_vld          => VME_rdreq_rx_vld,
        rx_next         => VME_rdreq_rx_next,
        ADM_tx_data     => VME_rdreq_ADM_tx_data,
        ADM_rx_data     => VME_rdreq_ADM_rx_data,
        ADM_sel         => VME_rdreq_ADM_sel
    );
    
    -- ADM VME_rdreq --
    rdreq_ADM: entity work.DAB64x_VME_ADM
    GENERIC MAP
    (
        rx_number       => VME_rdreq_rx_number,
        data_size       => VME_rdreq_data_size, 
        addr_size       => VME_rdreq_addr_size
    )
    PORT MAP
    (
        ADM_tx_data     => VME_rdreq_ADM_tx_data,
        ADM_rx_data     => VME_rdreq_ADM_rx_data,
        ADM_sel         => VME_rdreq_ADM_sel
    );
    
    -- VME_rdout --
    rdout_CMI: entity work.MM_CMI
    GENERIC MAP
    (
        tx_number       => VME_rdout_tx_number,
        rx_number       => VME_rdout_rx_number,
        data_size       => VME_rdout_data_size
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => VME_rdout_tx_data,
        tx_vld          => VME_rdout_tx_vld,
        tx_next         => VME_rdout_tx_next,
        rx_data         => VME_rdout_rx_data,
        rx_vld          => VME_rdout_rx_vld,
        rx_next         => VME_rdout_rx_next,
        ARB_tx_vld      => VME_rdout_ARB_tx_vld,
        ARB_tx_next     => VME_rdout_ARB_tx_next,
        ARB_rx_vld      => VME_rdout_ARB_rx_vld,
        ARB_rx_next     => VME_rdout_ARB_rx_next,
        ARB_sel         => VME_rdout_ARB_sel
    );
    
    -- ARB VME_rdout --
    rdout_ARB: entity work.DAB64x_VME_ARB
    GENERIC MAP
    (
        tx_number       => VME_rdout_tx_number
    )
    PORT MAP
    (
        clk             => clk,
        ARB_tx_vld      => VME_rdout_ARB_tx_vld,
        ARB_tx_next     => VME_rdout_ARB_tx_next,
        ARB_rx_vld      => VME_rdout_ARB_rx_vld,
        ARB_rx_next     => VME_rdout_ARB_rx_next,
        ARB_sel         => VME_rdout_ARB_sel
    );
    

end architecture struct;
