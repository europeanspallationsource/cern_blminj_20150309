------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/11/2013
Module Name:    RunningSum_ModDecode_CMA

-----------------
Short Description
-----------------

    Creates Running Sums, checks them with Thresholds and packages the maximum Running Sum during a certain
    timeframe and sends it.
    Furthermore it forwards the usedTH and the result of the threshold comparison.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the incoming data samples
    header_size         := size of the utilized header (in bit)
    TH_type             := the type of the TH comparison (overTH = 1, underTH = 0)
    window_length       := length of the RS window (Type: Time)
    gran                := granularity/occurence of the input samples (Type: Time)
    pkg_size            := number of 64-bit information blocks that need are packaged
   
    mux_data_size       := data size of the mux_CMI 
    mux_tx_number       := number of inputs of the mux_CMI
    mux_rx_number       := number of outputs of the mux_CMI
    
    waccu_data_size     := data size of the waccu_CMI
    waccu_rx_number     := number of outputs of the waccu_CMI
    

--------------
Implementation
--------------
    
    detailed description of the actual implementation
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity RunningSum_ModDecode_CMA is


generic
(
    inp_data_size:      integer;
    header_size:        integer;
    pkg_size:           integer;
    TH_type:            integer;
    window_length:      time;   
    gran:               time
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Timing Pulses
    max_start:          in  std_logic;
    max_stop:           in  std_logic;
    max_pbl:            in  std_logic;
    max_rst:            in  std_logic;
    -- Parameters
    PARAM_header:       in  std_logic_vector(header_size-1 downto 0);
    PARAM_threshold:    in  std_logic_vector(63 downto 0);
    -- CMI Input
    inp_data:           in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(65 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic;
    -- Std TH Output
    TC_result:          out std_logic;
    
    -- Signals from ModDecode to signaltap
    out_error:          out std_logic_vector(1 downto 0);
    FFTin_or_out_valid: out std_logic
);

end entity RunningSum_ModDecode_CMA;


architecture struct of RunningSum_ModDecode_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant out_data_size:     integer := 64;
    
    constant param_data_size:   integer := 64;
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- mux_CMA
    constant mux_data_size:     integer := 64;
    constant mux_tx_number:     integer := 2;
    constant mux_rx_number:     integer := 1;
    
    -- waccu_CMA
    constant waccu_data_size:   integer := 64;
    constant waccu_rx_number:   integer := 2;       -- The output of the Accumulator (the last sample for RS0) needs to be routed to the Modulation Decode logic

    -- THpass_CMA
    constant THpass_data_size:  integer := 64;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- waccu_CMA
    signal waccu_tx_data:       std_logic_vector(waccu_data_size-1 downto 0);
    signal waccu_tx_vld:        std_logic;
    signal waccu_tx_next:       std_logic;
    signal waccu_rx_data:       slv_array(waccu_rx_number-1 downto 0)(waccu_data_size-1 downto 0);
    signal waccu_rx_vld:        std_logic_vector(waccu_rx_number-1 downto 0);
    signal waccu_rx_next:       std_logic_vector(waccu_rx_number-1 downto 0);
    
    -- THpass_CMA
    signal THpass_data:         std_logic_vector(THpass_data_size-1 downto 0);
    signal THpass_vld:          std_logic;
    signal THpass_next:         std_logic;
    
    -- mux_CMA
    signal mux_tx_data:         slv_array(mux_tx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_tx_vld:          std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_tx_next:         std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_rx_data:         slv_array(mux_rx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_rx_vld:          std_logic_vector(mux_rx_number-1 downto 0);
    signal mux_rx_next:         std_logic_vector(mux_rx_number-1 downto 0);
    
    signal mux_ARB_tx_vld:      std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_ARB_tx_next:     std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_ARB_rx_vld:      std_logic;
    signal mux_ARB_rx_next:     std_logic;
    signal mux_ARB_sel:         std_logic_vector(get_vsize_addr(mux_tx_number)-1 downto 0);
    

BEGIN

    --=============--
    -- Window Accu --
    --=============--

    waccu_CMA_inst: entity work.window_accu_CMA
    generic map
    (
        inp_data_size   => inp_data_size,
        out_data_size   => waccu_data_size,
        window_length   => window_length,
        gran            => gran
    )
    port map 
    (
        clk             => clk,
        inp_data        => inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => waccu_tx_data,
        out_vld         => waccu_tx_vld,
        out_next        => waccu_tx_next
    );

    
    --===========--
    -- WACCU CMI --
    --===========--
    
    waccu_CMI: entity work.CMI
    generic map
    (
        rx_number   => waccu_rx_number,
        data_size   => waccu_data_size
    )
    port map 
    (
        clk         => clk,
        tx_data     => waccu_tx_data,
        tx_vld      => waccu_tx_vld,
        tx_next     => waccu_tx_next,
        rx_data     => waccu_rx_data,
        rx_vld      => waccu_rx_vld,
        rx_next     => waccu_rx_next
    );
    
    
    --===================--
    -- Threshold Compare --
    --===================--
    
    compare_pass_CMA_inst: entity work.compare_pass_CMA
    generic map
    (
        data_size           => waccu_data_size,
        param_data_size     => param_data_size,
        comp_type           => TH_type
    )
    port map 
    (
        clk                 => clk,
        PARAM_comp_value    => PARAM_threshold,
        inp_data            => waccu_rx_data(0),
        inp_vld             => waccu_rx_vld(0),
        inp_next            => waccu_rx_next(0),
        out_data            => THpass_data,
        out_vld             => THpass_vld,
        out_next            => THpass_next,
        result              => TC_result
    );
    
    --=====--
    -- max --
    --=====--
    
    -- MUX Input 1
    max_CMA_inst: entity work.max_CMA 
    generic map
    (
        data_size   => waccu_data_size
    )
    port map 
    (
        clk         => clk,
        start       => max_start,
        stop        => max_stop,
        pbl         => max_pbl,
        rst         => max_rst,
        inp_data    => THpass_data,
        inp_vld     => THpass_vld,
        inp_next    => THpass_next,
        out_data    => mux_tx_data(0),
        out_vld     => mux_tx_vld(0),
        out_next    => mux_tx_next(0)
    );
    
    --=======--
    -- Const --
    --=======--
    
    -- MUX Input 2
    const_CMA_inst: entity work.const_CMA
    generic map
    (
        data_size   => waccu_data_size
    )
    port map 
    (
        clk         => clk,
        PARAM_const => PARAM_threshold,
        out_data    => mux_tx_data(1),
        out_vld     => mux_tx_vld(1),
        out_next    => mux_tx_next(1)
    );
    
    
    --=========--
    -- mux CMI --
    --=========--
    
    mux_CMI: entity work.MM_CMI 
    generic map
    (
        tx_number       => mux_tx_number,
        rx_number       => mux_rx_number,
        data_size       => mux_data_size
    )
    port map 
    (
        clk             => clk,
        tx_data         => mux_tx_data,
        tx_vld          => mux_tx_vld,
        tx_next         => mux_tx_next,
        rx_data         => mux_rx_data,
        rx_vld          => mux_rx_vld,
        rx_next         => mux_rx_next,
        ARB_tx_vld      => mux_ARB_tx_vld,
        ARB_tx_next     => mux_ARB_tx_next,
        ARB_rx_vld      => mux_ARB_rx_vld,
        ARB_rx_next     => mux_ARB_rx_next,
        ARB_sel         => mux_ARB_sel
        
    );

    mux_CMI_ARB: entity work.RR_wait_mux_ARB
    generic map
    (
        tx_number       => mux_tx_number
    )
    port map
    (
        clk             => clk,
        ARB_tx_vld      => mux_ARB_tx_vld,
        ARB_tx_next     => mux_ARB_tx_next,
        ARB_rx_vld      => mux_ARB_rx_vld,
        ARB_rx_next     => mux_ARB_rx_next,
        ARB_sel         => mux_ARB_sel
    );
    
    --===========--
    -- Packaging --
    --===========--
    
    package_CMA_inst: entity work.package_CMA
    generic map
    (
        inp_data_size   => mux_data_size,
        pkg_size        => pkg_size,
        header_size     => header_size
    )
    port map 
    (
        clk             => clk,
        PARAM_header    => PARAM_header,
        pkg_done        => open,
        inp_data        => mux_rx_data(0),
        inp_vld         => mux_rx_vld(0),
        inp_next        => mux_rx_next(0),
        out_data        => out_data,
        out_vld         => out_vld,
        out_next        => out_next
    );

  
    --====================--
    -- Modulation Decoder --
    --====================--  
    ModDecode_CMA_inst: entity work.ModDecode_CMA
    generic map
    (
        RS_window_length    => window_length,
        RS_gran             => gran
    )    
    port map 
    (       
        clk                 => clk,
        inp_data            => waccu_rx_data(1),
        inp_vld             => waccu_rx_vld(1),
        inp_next            => waccu_rx_next(1),
        out_real            => open,
        out_imag            => open,
        out_exp             => open,
        out_sop             => open,
        out_eop             => open,
        out_vld             => open,
        out_next            => '1',
        out_error           => out_error,
        FFTin_or_out_valid  => FFTin_or_out_valid
    );  
    
    
end architecture struct;


