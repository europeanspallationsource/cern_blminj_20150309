------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2013
Module Name:    DAB64x_VME_ADM

-----------
Description
-----------

    The Address Decoder and Mapper for the vme_dab64x access to the connected RAMs.

------------------
Generics/Constants
------------------

    rx_number       := number of connected RAMs/slaves
    data_size       := size of input and output (this ADM has the same input and output size)
    addr_size       := size of the address part
    
----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------
    
    none

---------------
Necessary Cores
---------------

    none 
    
--------------
Implementation
--------------
    
------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DAB64x_VME_ADM is

generic
(
    rx_number:      integer := 2;
    data_size:      integer;
    addr_size:      integer
);

port
(
    ADM_tx_data:    in  std_logic_vector(data_size-1 downto 0);
    ADM_rx_data:    out std_logic_vector(data_size-1 downto 0);
    ADM_sel:        out std_logic_vector(get_vsize_addr(rx_number)-1 downto 0)
);

end entity DAB64x_VME_ADM;


architecture struct of DAB64x_VME_ADM is
    
    
    constant rest_data_size:    integer := data_size - addr_size;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal addr_int:            UNSIGNED(addr_size-1 downto 0);
    signal addr_mapped:         std_logic_vector(addr_size-1 downto 0);    
    
BEGIN
    
    -- Cast
    addr_int <= UNSIGNED(ADM_tx_data(data_size-1 downto rest_data_size));
    

        -- mapping
        process(all)
        begin
        -- defaults
        ADM_sel     <= STD_LOGIC_VECTOR(TO_UNSIGNED(0, get_vsize_addr(rx_number)));
        addr_mapped <= (others => '0');

            if      addr_int(19 downto 0) >= X"00000" AND addr_int(19 downto 0) < X"02900" then
                        addr_mapped     <= (addr_size-1 downto 20 => '0') & STD_LOGIC_VECTOR(addr_int(19 downto 0) - X"00000");
                        ADM_sel         <= STD_LOGIC_VECTOR(TO_UNSIGNED(0, get_vsize_addr(rx_number)));
            elsif   addr_int(19 downto 0) >= X"02900" AND addr_int(19 downto 0) < X"03A00" then
                        addr_mapped     <= (addr_size-1 downto 20 => '0') & STD_LOGIC_VECTOR(addr_int(19 downto 0) - X"02900");
                        ADM_sel         <= STD_LOGIC_VECTOR(TO_UNSIGNED(1, get_vsize_addr(rx_number)));
            elsif   addr_int(19 downto 0) >= X"03A00" AND addr_int(19 downto 0) < X"03C00" then
                        addr_mapped     <= (addr_size-1 downto 20 => '0') & STD_LOGIC_VECTOR(addr_int(19 downto 0) - X"03A00");
                        ADM_sel         <= STD_LOGIC_VECTOR(TO_UNSIGNED(2, get_vsize_addr(rx_number)));
            elsif   addr_int(19 downto 0) >= X"03C00" AND addr_int(19 downto 0) < X"03E00" then
                        addr_mapped     <= (addr_size-1 downto 20 => '0') & STD_LOGIC_VECTOR(addr_int(19 downto 0) - X"03C00");
                        ADM_sel         <= STD_LOGIC_VECTOR(TO_UNSIGNED(3, get_vsize_addr(rx_number)));
            elsif   addr_int(19 downto 0) >= X"03E00" AND addr_int(19 downto 0) < X"04000" then
                        addr_mapped     <= (addr_size-1 downto 20 => '0') & STD_LOGIC_VECTOR(addr_int(19 downto 0) - X"03E00");
                        ADM_sel         <= STD_LOGIC_VECTOR(TO_UNSIGNED(4, get_vsize_addr(rx_number)));
            elsif   addr_int(19 downto 0) >= X"04000" AND addr_int(19 downto 0) < X"04400" then
                        addr_mapped     <= (addr_size-1 downto 20 => '0') & STD_LOGIC_VECTOR(addr_int(19 downto 0) - X"04000");
                        ADM_sel         <= STD_LOGIC_VECTOR(TO_UNSIGNED(5, get_vsize_addr(rx_number)));
            elsif   addr_int(19 downto 0) >= X"04400" AND addr_int(19 downto 0) < X"04500" then
                        addr_mapped     <= (addr_size-1 downto 20 => '0') & STD_LOGIC_VECTOR(addr_int(19 downto 0) - X"04400");
                        ADM_sel         <= STD_LOGIC_VECTOR(TO_UNSIGNED(6, get_vsize_addr(rx_number)));
            end if;
      
        end process;
     
     -- OUTPUT
     
     ADM_rx_data <= addr_mapped & ADM_tx_data(rest_data_size-1 downto 0);
    
end architecture struct;
