------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/10/2013
Module Name:    AccuLoss_CMA

-----------------
Short Description
-----------------

    Accumulates data over a certain time and packages the results including the number of data summed up into
    a 64-bit package and the used threshold. In addition, it compares the result with a threshold, forwards 
    the result of the comparison.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the incoming data samples
    header_size         := size of the utilized header (in bit)
    pkg_size            := number of 64-bit data words in the package
    TH_type             := the type of the TH comparison (overTH = 1, underTH = 0)

    accu_tx_data_size   := input data size of the accu_CMI with 1-bit tag (65 bit)
    accu_rx_data_size   := output data size of accu_CMI without tag (64 bit)
    accu_rx_number      := number of receivers of the accu_CMI
   
    mux_data_size       := data size of the mux_CMI 
    mux_tx_number       := number of inputs of the mux_CMI
    mux_rx_number       := number of outputs of the mux_CMI

--------------
Implementation
--------------
    
    top module

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity AccuLoss_CMA is

generic
(
    inp_data_size:      integer;
    header_size:        integer;
    pkg_size:           integer;
    TH_type:            integer
    
);
port
(
    -- Clock
    clk:                in  std_logic;
    -- Timing Pulses
    accu_start:         in  std_logic;
    accu_stop:          in  std_logic;
    accu_pbl:           in  std_logic;
    accu_rst:           in  std_logic;
    -- Parameters
    PARAM_header:       in  std_logic_vector(header_size-1 downto 0);
    PARAM_threshold:    in  std_logic_vector(63 downto 0);
    -- CMI Input
    inp_data:           in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(65 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic;
    -- Std TH Output
    TC_result:          out std_logic
);

end entity AccuLoss_CMA;


architecture struct of AccuLoss_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant out_data_size:         integer := 66;
    
    constant param_data_size:       integer := 64;
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- accu CMA
    constant accu_tx_data_size:     integer := 65;
    constant accu_rx_data_size:     integer := 64;
    constant accu_rx_number:        integer := 2;
    
    -- mux CMA
    constant mux_data_size:         integer := 64;
    constant mux_tx_number:         integer := 3;
    constant mux_rx_number:         integer := 1;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- accu CMA
    signal accu_tx_data:            std_logic_vector(accu_tx_data_size-1 downto 0);
    signal accu_tx_vld:             std_logic;
    signal accu_tx_next:            std_logic;
    signal accu_rx_data:            slv_array(accu_rx_number-1 downto 0)(accu_rx_data_size-1 downto 0);
    signal accu_rx_vld:             std_logic_vector(accu_rx_number-1 downto 0);
    signal accu_rx_next:            std_logic_vector(accu_rx_number-1 downto 0);
    
    signal accu_ADM_tx_data:        std_logic_vector(accu_tx_data_size-1 downto 0);
    signal accu_ADM_rx_data:        std_logic_vector(accu_rx_data_size-1 downto 0);
    signal accu_ADM_sel:            std_logic_vector(get_vsize_addr(accu_rx_number)-1 downto 0);
    
    -- mux CMA
    signal mux_tx_data:             slv_array(mux_tx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_tx_vld:              std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_tx_next:             std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_rx_data:             slv_array(mux_rx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_rx_vld:              std_logic_vector(mux_rx_number-1 downto 0);
    signal mux_rx_next:             std_logic_vector(mux_rx_number-1 downto 0);
    
    signal mux_ARB_tx_vld:          std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_ARB_tx_next:         std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_ARB_rx_vld:          std_logic;
    signal mux_ARB_rx_next:         std_logic;
    signal mux_ARB_sel:             std_logic_vector(get_vsize_addr(mux_tx_number)-1 downto 0);
    
    
BEGIN
    
    
    --=============--
    -- Accumulator --
    --=============--
    
    accu_CMA_inst: entity work.accu_CMA 
    generic map
    (
        inp_data_size   => inp_data_size,
        out_data_size   => accu_tx_data_size
    )
    port map 
    (
        clk             => clk,
        start           => accu_start,
        stop            => accu_stop,
        pbl             => accu_pbl,
        rst             => accu_rst,
        inp_data        => inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => accu_tx_data,
        out_vld         => accu_tx_vld,
        out_next        => accu_tx_next
    );
    
    
    --==========--
    -- ACCU CMI --
    --==========--
    
    accu_CMI: entity work.T_CMI 
    generic map
    (
        rx_number       => accu_rx_number,
        tx_data_size    => accu_tx_data_size,
        rx_data_size    => accu_rx_data_size
    )
    port map 
    (
        clk             => clk,
        tx_data         => accu_tx_data,
        tx_vld          => accu_tx_vld,
        tx_next         => accu_tx_next,
        rx_data         => accu_rx_data,
        rx_vld          => accu_rx_vld,
        rx_next         => accu_rx_next,
        ADM_tx_data     => accu_ADM_tx_data,
        ADM_rx_data     => accu_ADM_rx_data,
        ADM_sel         => accu_ADM_sel
    );

    accu_CMI_ADM: entity work.ID_ADM
    generic map
    (
        rx_number       => accu_rx_number,
        tx_data_size    => accu_tx_data_size,
        rx_data_size    => accu_rx_data_size
    )
    port map
    (
        ADM_tx_data     => accu_ADM_tx_data,
        ADM_rx_data     => accu_ADM_rx_data,
        ADM_sel         => accu_ADM_sel
    );
    
    --===================--
    -- Threshold Compare --
    --===================--

    -- MUX Input 1
    compare_pass_CMA_inst: entity work.compare_pass_CMA
    generic map
    (
        data_size           => accu_rx_data_size,
        param_data_size     => param_data_size,
        comp_type           => TH_type
    )
    port map 
    (
        clk                 => clk,
        PARAM_comp_value    => PARAM_threshold,
        inp_data            => accu_rx_data(0),
        inp_vld             => accu_rx_vld(0),
        inp_next            => accu_rx_next(0),
        out_data            => mux_tx_data(0),
        out_vld             => mux_tx_vld(0),
        out_next            => mux_tx_next(0),
        result              => TC_result
    );   
    
    --=======--
    -- Const --
    --=======--
    
    -- MUX Input 2
    const_CMA_inst: entity work.const_CMA
    generic map
    (
        data_size   => accu_rx_data_size
    )
    port map 
    (
        clk         => clk,
        PARAM_const => PARAM_threshold,
        out_data    => mux_tx_data(1),
        out_vld     => mux_tx_vld(1),
        out_next    => mux_tx_next(1)
    );
    
    
    --=======--
    -- Delay --
    --=======--
    
    -- MUX Input 3
    delay_CMA_inst: entity work.delay_CMA
    generic map
    (
        delay_len   => 1,
        data_size   => accu_rx_data_size
    )
    port map 
    (
        clk         => clk,
        inp_data    => accu_rx_data(1),
        inp_vld     => accu_rx_vld(1),
        inp_next    => accu_rx_next(1),
        out_data    => mux_tx_data(2),
        out_vld     => mux_tx_vld(2),
        out_next    => mux_tx_next(2)
    );
      
    
    --=========--
    -- MUX CMI --
    --=========--
    
    mux_CMI: entity work.MM_CMI 
    generic map
    (
        tx_number       => mux_tx_number,
        rx_number       => mux_rx_number,
        data_size       => mux_data_size
    )
    port map 
    (
        clk         => clk,
        tx_data     => mux_tx_data,
        tx_vld      => mux_tx_vld,
        tx_next     => mux_tx_next,
        rx_data     => mux_rx_data,
        rx_vld      => mux_rx_vld,
        rx_next     => mux_rx_next,
        ARB_tx_vld  => mux_ARB_tx_vld,
        ARB_tx_next => mux_ARB_tx_next,
        ARB_rx_vld  => mux_ARB_rx_vld,
        ARB_rx_next => mux_ARB_rx_next,
        ARB_sel     => mux_ARB_sel
        
    );

    mus_CMI_ARB: entity work.RR_wait_mux_ARB
    generic map
    (
        tx_number       => mux_tx_number
    )
    port map
    (
        clk             => clk,
        ARB_tx_vld      => mux_ARB_tx_vld,
        ARB_tx_next     => mux_ARB_tx_next,
        ARB_rx_vld      => mux_ARB_rx_vld,
        ARB_rx_next     => mux_ARB_rx_next,
        ARB_sel         => mux_ARB_sel
    );
    
    
    --===============--
    -- Packaging/CRC --
    --===============--
    
    package_CMA_inst: entity work.package_CMA
    generic map
    (
        inp_data_size   => mux_data_size,
        pkg_size        => pkg_size,
        header_size     => header_size
    )
    port map 
    (
        clk             => clk,
        PARAM_header    => PARAM_header,
        pkg_done        => open,
        inp_data        => mux_rx_data(0),
        inp_vld         => mux_rx_vld(0),
        inp_next        => mux_rx_next(0),
        out_data        => out_data,
        out_vld         => out_vld,
        out_next        => out_next
    );

    
end architecture struct;
