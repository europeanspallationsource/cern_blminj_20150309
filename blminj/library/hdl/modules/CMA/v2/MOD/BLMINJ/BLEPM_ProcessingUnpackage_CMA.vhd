------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/05/2014
Module Name:    BLEPM_ProcessingUnpackage_CMA

-----------------
Short Description
-----------------

    This module creates the packages from the Diagnostic Reader on the BLEPM card.

------------
Dependencies
------------

    vhdl_func_pkg
    BLMINJ_pkg
    combine_CMA
    max_inc_cnt
    PackageCreator64_CMA
 
    
------------------
Generics/Constants
------------------
    
    inp_data_size       := size of the CMI input data bus
    out_data_size       := size of the CMI output data bus
    
    dismantled_data_size  := size of the dismantled_CMI data bus
    dismantled_rx_number  := number of the dismantled_CMI receiver modules
    
--------------
Implementation
--------------
    

    

-----------
Limitations
-----------
    

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    use work.BLMINJ_pkg.all;
   
entity BLEPM_ProcessingUnpackage_CMA is

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(17 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(159 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity BLEPM_ProcessingUnpackage_CMA;


architecture struct of BLEPM_ProcessingUnpackage_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant inp_data_size:         integer := 18;
    constant out_data_size:         integer := 160;
    
    constant header_size:           integer := 64;
    constant data_slice_n:          integer := 10;
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- dismantled_CMI
    constant dismantled_data_size:  integer := inp_data_size-2;
    constant dismantled_rx_number:  integer := 1;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- dismantle_CMI
    signal dismantled_tx_data:      std_logic_vector(dismantled_data_size-1 downto 0);
    signal dismantled_tx_vld:       std_logic;
    signal dismantled_tx_next:      std_logic;
    signal dismantled_rx_data:      slv_array(dismantled_rx_number-1 downto 0)(dismantled_data_size-1 downto 0);
    signal dismantled_rx_vld:       std_logic_vector(dismantled_rx_number-1 downto 0);
    signal dismantled_rx_next:      std_logic_vector(dismantled_rx_number-1 downto 0);
 
BEGIN
    
    
    --===============--
    -- PKG DISMANTLE --
    --===============--
    
    
    PackageDismantle_CMA_inst : entity work.PackageDismantle_CMA 
    generic map
    (
        inp_data_size   => inp_data_size,
        out_data_size   => dismantled_data_size,
        header_size     => header_size,
        data_slice_n    => data_slice_n
    )
    port map 
    (
        clk             => clk,
        inp_data        => inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => dismantled_tx_data,
        out_vld         => dismantled_tx_vld,
        out_next        => dismantled_tx_next
    );
    
    
    --================--
    -- DISMANTLED CMI --
    --================--
    
    dismantled_CMI : entity work.CMI 
    generic map
    (
        rx_number   => dismantled_rx_number,
        data_size   => dismantled_data_size
    )
    port map 
    (
        clk         => clk,
        tx_data     => dismantled_tx_data,
        tx_vld      => dismantled_tx_vld,
        tx_next     => dismantled_tx_next,
        rx_data     => dismantled_rx_data,
        rx_vld      => dismantled_rx_vld,
        rx_next     => dismantled_rx_next
    );
     
    --=========--
    -- COMBINE --
    --=========--
    
    combine_CMA_inst : entity work.combine_CMA 
    generic map
    (
        inp_data_size   => dismantled_data_size,
        out_data_size   => out_data_size      
    )
    port map 
    (
        clk             => clk,
        inp_data        => dismantled_rx_data(0),
        inp_vld         => dismantled_rx_vld(0),
        inp_next        => dismantled_rx_next(0),
        out_data        => out_data,
        out_vld         => out_vld,
        out_next        => out_next
    );
    
    
end architecture struct;