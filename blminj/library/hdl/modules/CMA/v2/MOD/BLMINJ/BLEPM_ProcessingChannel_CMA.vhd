------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/11/2013
Module Name:    BLEPM_ProcessingChannel_CMA

-----------------
Short Description
-----------------

    Top Module for one Processing Channel. It includes instantiation of the following modules:
        + Loss over Cycle
        + Loss over Beam Presence
        + Running Sums #1-#n
        + Capture
        + Post Mortem (optional)

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    inp_data_size   := size of the input, e.g. 20-bit data point
    channel_ID      := ID number of this channel (0-7)
    PM_active       := indicates if the Post Mortem module should be instantiated (1) or not (0)
    RS_number       := number of RunningSum Modules to be instantiated
    

--------------
Implementation
--------------
    
    detailed description of the actual implementation

-----------
Limitations
-----------
    
  

----------------
Missing Features
----------------
    
    

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    use work.BLMINJ_pkg.all;
   
entity BLEPM_ProcessingChannel_CMA is


generic
(
    inp_data_size:      integer;
    channel_ID:         integer;
    PM_active:          integer;
    RS_number:          integer
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Timing Pulses
    BP_pulse:           in  std_logic;
    BIn_pulse:          in  std_logic;
    BOut_pulse:         in  std_logic;
    EVOstart_pulse:     in  std_logic;
    EVOstop_pulse:      in  std_logic;
    EVO_pulse:          in  std_logic;
    CAPstart_pulse:     in  std_logic;
    CAPstop_pulse:      in  std_logic;
    CAP_pulse:          in  std_logic;
    -- Timing Status
    BEAM_ACT:           in  std_logic;
    -- Timing Counter
    ACQ_CNT:            in  std_logic_vector(31 downto 0);
    BP_CNT:             in  std_logic_vector(31 downto 0);
    EVO_CNT:            in  std_logic_vector(31 downto 0);
    CAP_CNT:            in  std_logic_vector(31 downto 0);
    -- Parameter
    PARAM_LoC_TH:       in  std_logic_vector(63 downto 0);
    PARAM_LoBP_TH:      in  std_logic_vector(63 downto 0);
    PARAM_RS_TH:        in  slv_array(0 to RS_number-1)(63 downto 0);
    -- CMI Input
    inp_data:           in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(65 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic;
    -- Std TH Output
    TC_results:         out std_logic_vector(2+RS_number-1 downto 0)
);

end entity BLEPM_ProcessingChannel_CMA;


architecture struct of BLEPM_ProcessingChannel_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- Channel Number (slv)
    constant PROC_CHAN_NO:          std_logic_vector(BLMINJ_SPC1_size-1 downto 0) := STD_LOGIC_VECTOR(TO_UNSIGNED(channel_ID, BLMINJ_SPC1_size));
    
    -- I/Os --
    -- MUX Position
    constant LoC_pos:               integer := 0;
    constant LoBP_pos:              integer := 1;
    constant EVO_pos:               integer := 2;
    constant RS_pos_base:           integer := 3;
    constant PM_pos:                integer := 3+RS_number;
    constant CAP_pos:               integer := 3+RS_number+PM_active;
    -- Numbers
    constant proc_number:           integer := 4+RS_number+PM_active; 
    constant TC_number:             integer := 2+RS_number;

    -- THRESHOLDs --
    -- TH vector positions
    constant LoC_TH_pos:            integer := 0;
    constant LoBP_TH_pos:           integer := 1;
    constant RS_TH_pos_base:        integer := 2;
    -- Loss over Cycle
    constant LoC_TH_type:           integer := 1;
    -- Loss over Beam Presence
    constant LoBP_TH_type:          integer := 1;
    
    -- Running Sums --
    constant RS_TH_type:            int_array(0 to RS_number-1)     := (1,1,1);
    constant RS_window_length:      time_array(0 to RS_number-1)    := (2 us,40 us, 1 ms);
    constant RS_gran:               time_array(0 to RS_number-1)    := (2 us,2 us, 2 us);    
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- broadcast_CMA
    constant broadcast_data_size:   integer := inp_data_size;
    constant broadcast_rx_number:   integer := proc_number;
    
    -- mux_CMA
    constant mux_data_size:         integer := 66;
    constant mux_tx_number:         integer := proc_number;
    constant mux_rx_number:         integer := 1;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- broadcast_CMA
    signal broadcast_tx_data:       std_logic_vector(broadcast_data_size-1 downto 0);
    signal broadcast_tx_vld:        std_logic;
    signal broadcast_tx_next:       std_logic;
    signal broadcast_rx_data:       slv_array(broadcast_rx_number-1 downto 0)(broadcast_data_size-1 downto 0);
    signal broadcast_rx_vld:        std_logic_vector(broadcast_rx_number-1 downto 0);
    signal broadcast_rx_next:       std_logic_vector(broadcast_rx_number-1 downto 0);
    
    -- mux_CMA
    signal mux_tx_data:             slv_array(mux_tx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_tx_vld:              std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_tx_next:             std_logic_vector(mux_tx_number-1 downto 0);
    signal mux_rx_data:             slv_array(mux_rx_number-1 downto 0)(mux_data_size-1 downto 0);
    signal mux_rx_vld:              std_logic_vector(mux_rx_number-1 downto 0);
    signal mux_rx_next:             std_logic_vector(mux_rx_number-1 downto 0);
    
    signal ARB_mux_tx_vld:          std_logic_vector(mux_tx_number-1 downto 0);
    signal ARB_mux_tx_next:         std_logic_vector(mux_tx_number-1 downto 0);
    signal ARB_mux_rx_vld:          std_logic;
    signal ARB_mux_rx_next:         std_logic;
    signal ARB_mux_sel:             std_logic_vector(get_vsize_addr(mux_tx_number)-1 downto 0);
    
     
    --=================--
    -- GENERAL SIGNALS --
    --=================--

    -- Timing Controls
    signal LoC_start:               std_logic;
    signal LoC_stop:                std_logic;
    signal LoC_pbl:                 std_logic;
    signal LoC_rst:                 std_logic;
    
    signal LoBP_start:              std_logic;
    signal LoBP_stop:               std_logic;
    signal LoBP_pbl:                std_logic;
    signal LoBP_rst:                std_logic;
    
    signal EVO_start:               std_logic;
    signal EVO_stop:                std_logic;
    signal EVO_pbl:                 std_logic;
    signal EVO_rst:                 std_logic;
    
    signal RS_start:                std_logic;
    signal RS_stop:                 std_logic;
    signal RS_pbl:                  std_logic;
    signal RS_rst:                  std_logic;
    
    signal CAP_start:               std_logic;
    signal CAP_stop:                std_logic;
    signal CAP_pbl:                 std_logic;
    signal CAP_rst:                 std_logic;
    
    -- Internal Redirected Counters
    signal EVO_PN:                  std_logic_vector(BLMINJ_PAGE_NO_size-1 downto 0);
    signal CAP_PN:                  std_logic_vector(BLMINJ_PAGE_NO_size-1 downto 0);
    signal PM_PN:                   std_logic_vector(BLMINJ_PAGE_NO_size-1 downto 0);
    signal ZERO_PN:                 std_logic_vector(BLMINJ_PAGE_NO_size-1 downto 0);
    
    -- Timestamp
    signal PROC_TIMESTAMP:          std_logic_vector(BLMINJ_TIMESTAMP_size-1 downto 0);
    
    -- Headers
    signal LoC_header:              std_logic_vector(BLMINJ_header_size-1 downto 0);
    signal LoBP_header:             std_logic_vector(BLMINJ_header_size-1 downto 0);
    signal EVO_header:              std_logic_vector(BLMINJ_header_size-1 downto 0);
    signal CAP_header:              std_logic_vector(BLMINJ_header_size-1 downto 0);
    signal PM_header:               std_logic_vector(BLMINJ_header_size-1 downto 0);
    signal RS_header:               slv_array(RS_number-1 downto 0)(BLMINJ_header_size-1 downto 0);

BEGIN

    --============--
    -- Signalling --
    --============--
    
    -- Timing Pulse Urilisation
    LoC_start       <= '1';
    LoC_stop        <= '0';
    LoC_pbl         <= BP_pulse;
    LoC_rst         <= BP_pulse;
    
    LoBP_start      <= BIn_pulse;
    LoBP_stop       <= BOut_pulse;
    LoBP_pbl        <= BOut_pulse OR (BP_pulse AND BEAM_ACT);
    LoBP_rst        <= BOut_pulse OR (BP_pulse AND BEAM_ACT);
    
    EVO_start       <= EVOstart_pulse;
    EVO_stop        <= EVOstop_pulse;
    EVO_pbl         <= EVO_pulse;
    EVO_rst         <= EVO_pulse;
    
    RS_start        <= '1';
    RS_stop         <= '0';
    RS_pbl          <= BP_pulse;
    RS_rst          <= BP_pulse;
    
    CAP_start       <= CAPstart_pulse;
    CAP_stop        <= CAPstop_pulse;
    CAP_pbl         <= CAP_pulse;
    CAP_rst         <= CAP_pulse;

    -- Page Numbers
    PM_PN           <= "0000000000000" & ACQ_CNT(10 downto 0);
    EVO_PN          <= EVO_CNT(BLMINJ_PAGE_NO_size-1 downto 0);
    CAP_PN          <= CAP_CNT(BLMINJ_PAGE_NO_size-1 downto 0);
    ZERO_PN         <= (BLMINJ_PAGE_NO_size-1 downto 0 => '0');
    
    -- Timestamp
    PROC_TIMESTAMP  <= BP_CNT(BLMINJ_TIMESTAMP_size-1 downto 0);
    
    -- Headers
    LoC_header      <= BLEPM_CARD_NO & PROC_TYPE & PROC_CHAN_NO & PROC_SPC2_LOC  & ZERO_PN & PROC_TIMESTAMP;
    LoBP_header     <= BLEPM_CARD_NO & PROC_TYPE & PROC_CHAN_NO & PROC_SPC2_LOBP & ZERO_PN & PROC_TIMESTAMP;
    EVO_header      <= BLEPM_CARD_NO & PROC_TYPE & PROC_CHAN_NO & PROC_SPC2_EVO  & EVO_PN  & PROC_TIMESTAMP;
    CAP_header      <= BLEPM_CARD_NO & PROC_TYPE & PROC_CHAN_NO & PROC_SPC2_CAP  & CAP_PN  & PROC_TIMESTAMP;
    PM_header       <= BLEPM_CARD_NO & PROC_TYPE & PROC_CHAN_NO & PROC_SPC2_PM   & PM_PN   & PROC_TIMESTAMP;
   
    RS_header_gen:
    for i in 0 to RS_number-1 generate
        RS_header(i) <= BLEPM_CARD_NO & PROC_TYPE & PROC_CHAN_NO & PROC_SPC2_RS(i) & ZERO_PN & PROC_TIMESTAMP;
    end generate;
    
    --==================--
    -- Input Connection --
    --==================--
    
    broadcast_tx_data   <= inp_data;
    broadcast_tx_vld    <= inp_vld;
    inp_next            <= broadcast_tx_next;
    
    --===============--
    -- Broadcast CMI --
    --===============--
    
    broadcast_CMI: entity work.CMI
    generic map
    (
        rx_number       => broadcast_rx_number,
        data_size       => broadcast_data_size
    )
    port map 
    (
        clk             => clk,
        tx_data         => broadcast_tx_data,
        tx_vld          => broadcast_tx_vld,
        tx_next         => broadcast_tx_next,
        rx_data         => broadcast_rx_data,
        rx_vld          => broadcast_rx_vld,
        rx_next         => broadcast_rx_next
    );
    
    --=================--
    -- Loss over Cycle --
    --=================--
    
    LoC_inst: entity work.AccuLoss_CMA
    generic map
    (
        inp_data_size   => broadcast_data_size,
        header_size     => BLMINJ_HEADER_size,
        pkg_size        => PROC_LoC_PKG_SIZE,
        TH_type         => LoC_TH_type
    )
    port map 
    (
        clk             => clk,
        accu_start      => LoC_start,
        accu_stop       => LoC_stop,
        accu_pbl        => LoC_pbl,
        accu_rst        => LoC_rst,
        PARAM_header    => LoC_header,
        PARAM_threshold => PARAM_LoC_TH,
        inp_data        => broadcast_rx_data(LoC_pos),
        inp_vld         => broadcast_rx_vld(LoC_pos),
        inp_next        => broadcast_rx_next(LoC_pos),
        out_data        => mux_tx_data(LoC_pos),
        out_vld         => mux_tx_vld(LoC_pos),
        out_next        => mux_tx_next(LoC_pos),
        TC_result       => TC_results(LoC_TH_pos)
    );
    
    --==============--
    -- Loss over BP --
    --==============--
    
    LoBP_inst: entity work.AccuLoss_CMA
    generic map
    (
        inp_data_size   => broadcast_data_size,
        header_size     => BLMINJ_HEADER_size,
        pkg_size        => PROC_LoBP_PKG_SIZE,
        TH_type         => LoBP_TH_type
    )
    port map 
    (
        clk             => clk,
        accu_start      => LoBP_start,
        accu_stop       => LoBP_stop,
        accu_pbl        => LoBP_pbl,
        accu_rst        => LoBP_rst,
        PARAM_header    => LoBP_header,
        PARAM_threshold => PARAM_LoBP_TH,
        inp_data        => broadcast_rx_data(LoBP_pos),
        inp_vld         => broadcast_rx_vld(LoBP_pos),
        inp_next        => broadcast_rx_next(LoBP_pos),
        out_data        => mux_tx_data(LoBP_pos),
        out_vld         => mux_tx_vld(LoBP_pos),
        out_next        => mux_tx_next(LoBP_pos),
        TC_result       => TC_results(LoBP_TH_pos)
    );
    
    --===========--
    -- Evolution --
    --===========--
    
    Evolution_inst: entity work.AccuLoss_noTC_CMA
    generic map
    (
        inp_data_size   => broadcast_data_size,
        pkg_size        => PROC_EVO_PKG_SIZE,
        header_size     => BLMINJ_HEADER_size
    )
    port map 
    (
        clk             => clk,
        pkg_done        => open,
        accu_start      => EVO_start,
        accu_stop       => EVO_stop,
        accu_pbl        => EVO_pbl,
        accu_rst        => EVO_rst,
        PARAM_header    => EVO_header,
        inp_data        => broadcast_rx_data(EVO_pos),
        inp_vld         => broadcast_rx_vld(EVO_pos),
        inp_next        => broadcast_rx_next(EVO_pos),
        out_data        => mux_tx_data(EVO_pos),
        out_vld         => mux_tx_vld(EVO_pos),
        out_next        => mux_tx_next(EVO_pos)
    );
    
        
    --==============--
    -- Running Sums --
    --==============--
    
    RunnningSum_CMA_gen:
    for i in 0 to RS_number-1 generate
    
        RS: entity work.RunningSum_CMA
        generic map
        (
            inp_data_size   => broadcast_data_size,
            header_size     => BLMINJ_HEADER_size,
            pkg_size        => PROC_RS_PKG_SIZE,
            TH_type         => RS_TH_type(i),
            window_length   => RS_window_length(i),
            gran            => RS_gran(i)
            
        )
        port map 
        (
            clk             => clk,
            max_start       => RS_start,
            max_stop        => RS_stop,
            max_pbl         => RS_pbl,
            max_rst         => RS_rst,
            PARAM_header    => RS_header(i),
            PARAM_threshold => PARAM_RS_TH(i),
            inp_data        => broadcast_rx_data(RS_pos_base+i),
            inp_vld         => broadcast_rx_vld(RS_pos_base+i),
            inp_next        => broadcast_rx_next(RS_pos_base+i),
            out_data        => mux_tx_data(RS_pos_base+i),
            out_vld         => mux_tx_vld(RS_pos_base+i),
            out_next        => mux_tx_next(RS_pos_base+i),
            TC_result       => TC_results(RS_TH_pos_base+i)
        );

    end generate;

    --=========--
    -- Capture --
    --=========--

    Capture_inst: entity work.AccuLoss_noTC_CMA
    generic map
    (
        inp_data_size   => broadcast_data_size,
        pkg_size        => PROC_CAP_PKG_SIZE,
        header_size     => BLMINJ_HEADER_size
    )
    port map 
    (
        clk             => clk,
        pkg_done        => open,
        accu_start      => CAP_start,
        accu_stop       => CAP_stop,
        accu_pbl        => CAP_pbl,
        accu_rst        => CAP_rst,
        PARAM_header    => CAP_header,
        inp_data        => broadcast_rx_data(CAP_pos),
        inp_vld         => broadcast_rx_vld(CAP_pos),
        inp_next        => broadcast_rx_next(CAP_pos),
        out_data        => mux_tx_data(CAP_pos),
        out_vld         => mux_tx_vld(CAP_pos),
        out_next        => mux_tx_next(CAP_pos)
    );

    --=============--
    -- Post Mortem --
    --=============--
    
    PM_gen: 
    if PM_active = 1 generate
    
        -- Packaging --
        PostMortem_CMA_inst: entity work.PostMortem_CMA
        generic map
        (
            inp_data_size   => broadcast_data_size,
            pkg_size        => PROC_PM_PKG_SIZE,
            header_size     => BLMINJ_HEADER_size
        )
        port map 
        (
            clk             => clk,
            pkg_done        => open,
            PARAM_header    => PM_header,
            inp_data        => broadcast_rx_data(PM_pos),
            inp_vld         => broadcast_rx_vld(PM_pos),
            inp_next        => broadcast_rx_next(PM_pos),
            out_data        => mux_tx_data(PM_pos),
            out_vld         => mux_tx_vld(PM_pos),
            out_next        => mux_tx_next(PM_pos)
        );
        
    end generate;
    
    --========--
    -- Output --
    --========--
    
    -- mux_CMI --
    mux_CMI: entity work.MM_CMI 
    generic map
    (
        tx_number       => mux_tx_number,
        rx_number       => mux_rx_number,
        data_size       => mux_data_size
    )
    port map 
    (
        clk             => clk,
        tx_data         => mux_tx_data,
        tx_vld          => mux_tx_vld,
        tx_next         => mux_tx_next,
        rx_data         => mux_rx_data,
        rx_vld          => mux_rx_vld,
        rx_next         => mux_rx_next,
        ARB_tx_vld      => ARB_mux_tx_vld,
        ARB_tx_next     => ARB_mux_tx_next,
        ARB_rx_vld      => ARB_mux_rx_vld,
        ARB_rx_next     => ARB_mux_rx_next,
        ARB_sel         => ARB_mux_sel
        
    );

    mux_CMI_ARB: entity work.priority_pkg_mux_ARB
    generic map
    (
        tx_number       => mux_tx_number,
        data_size       => mux_data_size
    )
    port map
    (
        clk             => clk,
        full_data       => mux_tx_data,
        ARB_tx_vld      => ARB_mux_tx_vld,
        ARB_tx_next     => ARB_mux_tx_next,
        ARB_rx_vld      => ARB_mux_rx_vld,
        ARB_rx_next     => ARB_mux_rx_next,
        ARB_sel         => ARB_mux_sel
    );
    
    --==================--
    -- Output Connection --
    --==================--
    
    out_data        <= mux_rx_data(0);
    out_vld         <= mux_rx_vld(0);
    mux_rx_next(0)  <= out_next;
    
    
end architecture struct;
