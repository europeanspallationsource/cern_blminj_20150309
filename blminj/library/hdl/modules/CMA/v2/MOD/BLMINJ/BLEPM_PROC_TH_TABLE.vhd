------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2014
Module Name:    BLEPM_PROC_TH_TABLE


-----------------
Short Description
-----------------

    Parameter Table for the Processing Core of the BLEPM card.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    
--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity BLEPM_PROC_TH_TABLE is
port
(
    -- Clock
    clk:                in  std_logic;
    -- CMI Input
    inp_data:           in  std_logic_vector(15 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- Parameters
    PARAM_LoC_TH:       out slv_array(0 to 7)(63 downto 0);
    PARAM_LoBP_TH:      out slv_array(0 to 7)(63 downto 0);
    PARAM_RS_TH:        out slv_array_array(0 to 7)(0 to 2)(63 downto 0) 
);

end entity BLEPM_PROC_TH_TABLE;


architecture struct of BLEPM_PROC_TH_TABLE is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant param_data_size:   integer := 16;
    constant param_table_size:  integer := 576;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Table Output
    signal param_vector:        slv_array(0 to param_table_size-1)(param_data_size-1 downto 0);
    
BEGIN   
  
      
    --===================--
    -- BASIC PARAM TABLE --
    --===================--  
    
    param_table_CMA_inst: entity work.param_table_CMA
    generic map
    (
        inp_data_size   => param_data_size,
        table_size      => param_table_size
    )
    port map 
    (
        clk         => clk,
        inp_data    => inp_data,
        inp_vld     => inp_vld,
        inp_next    => inp_next,
        params      => param_vector
    );
    
    
    --================--
    -- VECTOR MAPPING --
    --================-- 
    
    all_params: 
    for i in 0 to 7 generate
        
        PARAM_LoC_TH(i)     <= param_vector(0+4*i) & param_vector(1+4*i) & param_vector(2+4*i) & param_vector(3+4*i); 
        PARAM_LoBP_TH(i)    <= param_vector(32+4*i) & param_vector(33+4*i) & param_vector(34+4*i) & param_vector(35+4*i);
    
        RS_channels: 
        for j in 0 to 2 generate
            PARAM_RS_TH(i)(j)    <= param_vector(64+4*i+32*j) & param_vector(65+4*i+32*j) & param_vector(66+4*i+32*j) & param_vector(67+4*i+32*j);
        end generate;
        
    end generate;
        
    
    
end architecture struct;
