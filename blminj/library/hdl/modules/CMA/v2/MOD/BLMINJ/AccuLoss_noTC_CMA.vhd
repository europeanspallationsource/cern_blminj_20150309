------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/10/2013
Module Name:    AccuLoss_noTC_CMA

-----------------
Short Description
-----------------

    Accumulates data over a certain time and packages the result (without #samples) into a 64-bit package.

------------
Dependencies
------------

    vhdl_func_pkg
    accu_CMA
    PackageCreator64_CMA
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the incoming data samples
    header_size         := size of the utilized header (in bit)
    pkg_size            := number of 64-bit information blocks that need are packaged
   
    accu_tx_data_size   := internal data size with 1-bit tag (65 bit)
    accu_rx_data_size   := internal data size without tag (64 bit)
    accu_rx_number      := number of receivers for the accu_CMI
    
--------------
Implementation
--------------
    
    Top Module
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity AccuLoss_noTC_CMA is

generic
(
    inp_data_size:      integer;
    pkg_size:           integer;
    header_size:        integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Timing Pulses
    accu_start:     in  std_logic;
    accu_stop:      in  std_logic;
    accu_pbl:       in  std_logic;
    accu_rst:       in  std_logic;
    -- Status
    pkg_done:       out std_logic;
    -- Parameters
    PARAM_header:   in  std_logic_vector(header_size-1 downto 0);
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(65 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity AccuLoss_noTC_CMA;


architecture struct of AccuLoss_noTC_CMA is
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- accu_CMA
    constant accu_tx_data_size: integer := 65;
    constant accu_rx_data_size: integer := 64;
    constant accu_rx_number:    integer := 1;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- accu_CMA
    signal accu_tx_data:        std_logic_vector(accu_tx_data_size-1 downto 0);
    signal accu_tx_vld:         std_logic;
    signal accu_tx_next:        std_logic;
    signal accu_rx_data:        slv_array(accu_rx_number-1 downto 0)(accu_rx_data_size-1 downto 0);
    signal accu_rx_vld:         std_logic_vector(accu_rx_number-1 downto 0);
    signal accu_rx_next:        std_logic_vector(accu_rx_number-1 downto 0);
    
    signal accu_DM_tx_data:     std_logic_vector(accu_tx_data_size-1 downto 0);
    signal accu_DM_tx_vld:      std_logic;
    signal accu_DM_rx_data:     slv_array(accu_rx_number-1 downto 0)(accu_rx_data_size-1 downto 0);
    signal accu_DM_rx_vld:      std_logic_vector(accu_rx_number-1 downto 0);
    
BEGIN
    
    
    --=============--
    -- Accumulator --
    --=============--
    
    accu_CMA_inst: entity work.accu_CMA 
    generic map
    (
        inp_data_size   => inp_data_size,
        out_data_size   => accu_tx_data_size
    )
    port map 
    (
        clk         => clk,
        start       => accu_start,
        stop        => accu_stop,
        pbl         => accu_pbl,
        rst         => accu_rst,
        inp_data    => inp_data,
        inp_vld     => inp_vld,
        inp_next    => inp_next,
        out_data    => accu_tx_data,
        out_vld     => accu_tx_vld,
        out_next    => accu_tx_next
    );
    
    
    --=================--
    -- Accu Connection --
    --=================--
    
    accu_CMI: entity work.DM_CMI
    generic map
    (
        rx_number       => accu_rx_number,
        tx_data_size    => accu_tx_data_size,
        rx_data_size    => accu_rx_data_size
    )
    port map 
    (
        clk         => clk,
        tx_data     => accu_tx_data,
        tx_vld      => accu_tx_vld,
        tx_next     => accu_tx_next,
        rx_data     => accu_rx_data,
        rx_vld      => accu_rx_vld,
        rx_next     => accu_rx_next,
        DM_tx_data  => accu_DM_tx_data,
        DM_tx_vld   => accu_DM_tx_vld,
        DM_rx_data  => accu_DM_rx_data,
        DM_rx_vld   => accu_DM_rx_vld
    );

    accu_CMI_DM: entity work.accu_smpl_filter_DM
    generic map
    (
        rx_number       => accu_rx_number,
        tx_data_size    => accu_tx_data_size,
        rx_data_size    => accu_rx_data_size
        
    )
    port map
    (
        DM_tx_data      => accu_DM_tx_data,
        DM_tx_vld       => accu_DM_tx_vld,
        DM_rx_data      => accu_DM_rx_data,
        DM_rx_vld       => accu_DM_rx_vld
    );
    
    --===========--
    -- Packaging --
    --===========--
    
    
    package_CMA_inst: entity work.package_CMA
    generic map
    (
        inp_data_size   => accu_rx_data_size,
        pkg_size        => pkg_size,
        header_size     => header_size
    )
    port map 
    (
        clk             => clk,
        PARAM_header    => PARAM_header,
        pkg_done        => pkg_done,
        inp_data        => accu_rx_data(0),
        inp_vld         => accu_rx_vld(0),
        inp_next        => accu_rx_next(0),
        out_data        => out_data,
        out_vld         => out_vld,
        out_next        => out_next
    );
    
    

end architecture struct;