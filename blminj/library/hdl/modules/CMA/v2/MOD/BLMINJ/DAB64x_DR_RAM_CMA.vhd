------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    DAB64x_DR_RAM_CMA

-----------------
Short Description
-----------------

    
    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    
--------------
Implementation
--------------
    
    
-----------
Limitations
-----------


----------------
Missing Features
----------------

   

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DAB64x_DR_RAM_CMA is

generic
(
    inp_data_size:      integer;
    out_data_size:      integer
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Timing Pulses
    BP_pulse:           in  std_logic;
    DR_INP_BLOCK:       in  std_logic;
    -- CMI Input
    inp_data:           in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(out_data_size-1 downto 0); -- including protocol bit
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic
);

end entity DAB64x_DR_RAM_CMA;


architecture struct of DAB64x_DR_RAM_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- General
    constant fifo_depth:                integer := 128;
    constant header_size:               integer := 16;
    
    -- RAM    
    constant DR_RAM_numwords:           integer := 128;
    constant DR_RAM_data_size:          integer := 16;
    constant DR_RAM_byteena_size:       integer := 2;
    constant DR_RAM_addr_size:          integer := get_vsize_addr(DR_RAM_numwords);
    
    constant DR_RAM_data_MSB:           integer := DR_RAM_data_size-1;
    constant DR_RAM_addr_MSB:           integer := DR_RAM_data_MSB+DR_RAM_addr_size;
    constant DR_RAM_byteena_MSB:        integer := 0;
    constant DR_RAM_init_file:          string  := "UNUSED";
    constant DR_RAM_type:               string  := "M4K";
    
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- stored_CMA
    constant stored_data_size:          integer := inp_data_size;
    
    -- pkg_gate_CMA
    constant pkg_gate_data_size:        integer := inp_data_size;
    
    -- PKG HEADER
    constant pkg_header_data_size:      integer := header_size;
    
    -- PKG BODY
    constant pkg_body_data_size:        integer := inp_data_size-2;
    
    -- WRCtrl
    constant wrctrl_addr_size:          integer := DR_RAM_addr_size;
    constant wrctrl_data_size:          integer := pkg_body_data_size + wrctrl_addr_size;
    constant wrctrl_d_MSB:              integer := pkg_body_data_size-1;
    constant wrctrl_addr_MSB:           integer := wrctrl_data_size-1;
    
    -- RDCtrl
    constant rdctrl_data_size:          integer := DR_RAM_addr_size;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- RAM Connections
    signal DR_RAM_portA_addr:           std_logic_vector(DR_RAM_addr_size-1 downto 0);
    signal DR_RAM_portA_data:           std_logic_vector(DR_RAM_data_size-1 downto 0);
    signal DR_RAM_portA_byteena:        std_logic_vector(DR_RAM_byteena_size-1 downto 0);
    signal DR_RAM_portA_wren:           std_logic;
    signal DR_RAM_portA_q:              std_logic_vector(DR_RAM_data_size-1 downto 0);
    
    signal DR_RAM_portB_addr:           std_logic_vector(DR_RAM_addr_size-1 downto 0);
    signal DR_RAM_portB_data:           std_logic_vector(DR_RAM_data_size-1 downto 0);
    signal DR_RAM_portB_byteena:        std_logic_vector(DR_RAM_byteena_size-1 downto 0);
    signal DR_RAM_portB_wren:           std_logic;
    signal DR_RAM_portB_q:              std_logic_vector(DR_RAM_data_size-1 downto 0);
    
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- stored
    signal stored_data:                 std_logic_vector(stored_data_size-1 downto 0);
    signal stored_vld:                  std_logic;
    signal stored_next:                 std_logic;
    
    -- pkg_gate
    signal pkg_gate_data:               std_logic_vector(pkg_gate_data_size-1 downto 0);
    signal pkg_gate_vld:                std_logic;
    signal pkg_gate_next:               std_logic;
    
    -- pkg_header
    signal pkg_header_data:             std_logic_vector(pkg_header_data_size-1 downto 0);
    signal pkg_header_vld:              std_logic;
    signal pkg_header_next:             std_logic;
    
    -- pkg_body
    signal pkg_body_data:               std_logic_vector(pkg_body_data_size-1 downto 0);
    signal pkg_body_vld:                std_logic;
    signal pkg_body_next:               std_logic;
    
    -- wrctrl
    signal wrctrl_data:                 std_logic_vector(wrctrl_data_size-1 downto 0);
    signal wrctrl_vld:                  std_logic;
    signal wrctrl_next:                 std_logic;
    
    -- wrctrl
    signal rdctrl_data:                 std_logic_vector(rdctrl_data_size-1 downto 0);
    signal rdctrl_vld:                  std_logic;
    signal rdctrl_next:                 std_logic;
    
BEGIN


    
    --==================--
    -- PACKAGE HANDLING --
    --==================--
    
    -- FIFO
    fifo_CMA_inst: entity work.fifo_CMA
    GENERIC MAP
    (
        data_size       => inp_data_size, 
        fifo_depth      => fifo_depth
    )
    PORT MAP
    (
        clk             => clk,
        inp_data        => inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => stored_data,
        out_vld         => stored_vld,
        out_next        => stored_next
    );
    
    
    
    -- pkg gate --
    pkg_gate_CMA_inst: entity work.package_gate_CMA
    GENERIC MAP
    (
        data_size       => pkg_gate_data_size
    )
    PORT MAP
    (
        clk             => clk,
        stop            => DR_INP_BLOCK,
        inp_data        => stored_data,
        inp_vld         => stored_vld,
        inp_next        => stored_next,
        out_data        => pkg_gate_data,
        out_vld         => pkg_gate_vld,
        out_next        => pkg_gate_next
    );
    
    
    -- package split --
    pkg_split_CMA_inst: entity work.package_split_CMA
    GENERIC MAP
    (
        inp_data_size       => pkg_gate_data_size,
        body_data_size      => pkg_body_data_size,
        header_data_size    => pkg_header_data_size,
        header_size         => header_size
    )
    PORT MAP
    (
        clk                 => clk,
        inp_data            => pkg_gate_data,
        inp_vld             => pkg_gate_vld,
        inp_next            => pkg_gate_next,
        body_data           => pkg_body_data,
        body_vld            => pkg_body_vld,
        body_next           => pkg_body_next,
        header_data         => pkg_header_data,
        header_vld          => pkg_header_vld,
        header_next         => pkg_header_next
    );
    
    --================--
    -- WRITE CONTROL  --
    --================--
    
    -- Write Control --
    DRWriteCtrl_inst: entity work.DAB64x_DR_WriteCtrl_CMA
    GENERIC MAP
    (
        header_data_size    => pkg_header_data_size,
        body_data_size      => pkg_body_data_size,
        out_data_size       => wrctrl_data_size
    )
    PORT MAP
    (
        clk                 => clk,
        body_data           => pkg_body_data,
        body_vld            => pkg_body_vld,
        body_next           => pkg_body_next,
        header_data         => pkg_header_data,
        header_vld          => pkg_header_vld,
        header_next         => pkg_header_next,
        out_data            => wrctrl_data,
        out_vld             => wrctrl_vld,
        out_next            => wrctrl_next
    );
    

    
    --===============--
    -- RAM STRUCTURE --
    --===============--

    -- RAM Access Control Write
    EVO_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => wrctrl_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => DR_RAM_byteena_MSB,
        wrreq_data_MSB      => DR_RAM_data_MSB,
        wrreq_addr_MSB      => DR_RAM_addr_MSB,
        RAM_addr_size       => DR_RAM_addr_size,
        RAM_data_size       => DR_RAM_data_size,
        RAM_byteena_size    => DR_RAM_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => wrctrl_data,
        wrreq_vld       => wrctrl_vld,
        wrreq_next      => wrctrl_next,
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => DR_RAM_portB_addr,
        RAM_data        => DR_RAM_portB_data,
        RAM_byteena     => DR_RAM_portB_byteena,
        RAM_wren        => DR_RAM_portB_wren,
        RAM_q           => DR_RAM_portB_q
    );
    
    -- DR RAM --
    EVO_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => DR_RAM_numwords,
        portA_data_size     => DR_RAM_data_size,
        portA_addr_size     => DR_RAM_addr_size,
        portA_byteena_size  => DR_RAM_byteena_size,
        portB_numwords      => DR_RAM_numwords,
        portB_data_size     => DR_RAM_data_size,
        portB_addr_size     => DR_RAM_addr_size,
        portB_byteena_size  => DR_RAM_byteena_size,
        init_file           => DR_RAM_init_file,
        ram_type            => DR_RAM_type,
        read_during_write   => "DONT_CARE",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => DR_RAM_portA_addr,
        portA_data      => DR_RAM_portA_data,
        portA_byteena   => DR_RAM_portA_byteena,
        portA_wren      => DR_RAM_portA_wren,
        portA_q         => DR_RAM_portA_q,
        portB_addr      => DR_RAM_portB_addr,
        portB_data      => DR_RAM_portB_data,
        portB_byteena   => DR_RAM_portB_byteena,
        portB_wren      => DR_RAM_portB_wren,
        portB_q         => DR_RAM_portB_q
    );
    
    -- RAM Access Control Read
    EVO_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => 64,
        rdreq_data_size     => rdctrl_data_size,
        rdout_data_size     => out_data_size,
        wrreq_byteena_MSB   => 64,
        wrreq_data_MSB      => 64,
        wrreq_addr_MSB      => 64,
        RAM_addr_size       => DR_RAM_addr_size,
        RAM_data_size       => DR_RAM_data_size,
        RAM_byteena_size    => DR_RAM_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => rdctrl_data,
        rdreq_vld       => rdctrl_vld,
        rdreq_next      => rdctrl_next,
        wrreq_data      => (others => '0'),
        wrreq_vld       => '0',
        wrreq_next      => open,
        rdout_data      => out_data,
        rdout_vld       => out_vld,
        rdout_next      => out_next,
        RAM_addr        => DR_RAM_portA_addr,
        RAM_data        => DR_RAM_portA_data,
        RAM_byteena     => DR_RAM_portA_byteena,
        RAM_wren        => DR_RAM_portA_wren,
        RAM_q           => DR_RAM_portA_q
    );
    
    
    --==============--
    -- READ CONTROL --
    --==============--
    
    -- Write Control --
    DRReadCtrl_inst: entity work.DAB64x_DR_ReadCtrl_CMA
    GENERIC MAP
    (
        out_data_size       => rdctrl_data_size
    )
    PORT MAP
    (
        clk                 => clk,
        start_req           => BP_pulse,
        out_data            => rdctrl_data,
        out_vld             => rdctrl_vld,
        out_next            => rdctrl_next
    );
    
    
end architecture struct;
