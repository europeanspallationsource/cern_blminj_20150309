------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/05/2014
Module Name:    BLEDP_DiagnosticPackaging_CMA

-----------------
Short Description
-----------------

    This module creates the packages from the Diagnostic Reader on the BLEDP card.

------------
Dependencies
------------

    vhdl_func_pkg
    BLMINJ_pkg
    max_inc_cnt
    PackageCreator64_CMA
 
    
------------------
Generics/Constants
------------------
    
   

--------------
Implementation
--------------
    

    

-----------
Limitations
-----------
    

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    use work.BLMINJ_pkg.all;
   
entity BLEDP_DiagnosticPackaging_CMA is

port
(
    -- Clock
    clk:            in  std_logic;
    -- Timing Information
    BP_CNT:         in  std_logic_vector(BLMINJ_timestamp_size-1 downto 0);
    -- CMI Input
    inp_data:       in  std_logic_vector(15 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(65 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity BLEDP_DiagnosticPackaging_CMA;


architecture struct of BLEDP_DiagnosticPackaging_CMA is
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal header:                  std_logic_vector(BLMINJ_header_size-1 downto 0);
    signal pkg_done:                std_logic;
    signal DIAG_PAGE_CNT:           std_logic_vector(BLMINJ_PAGE_NO_size-1 downto 0);
      
    
BEGIN
    
    --==============--
    -- PAGE COUNTER --
    --==============--
    
    pageCNT_inst : entity work.max_inc_cnt 
    generic map
    (
        data_size   => BLMINJ_PAGE_NO_size,
        max         => DIAG_PAGES_BLEDP    
    )
    port map 
    (
        clk             => clk,
        inc             => pkg_done,
        cnt             => DIAG_PAGE_CNT
        
    );
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    header <= BLEDP_CARD_NO & DIAG_TYPE & DIAG_SPC1 & DIAG_SPC2 & DIAG_PAGE_CNT & BP_CNT;
    
  
    
    --====================--
    -- PACKAGE CREATOR 64 --
    --====================--
    
    PackageCreator16_CMA_inst : entity work.PackageCreator16_CMA 
    generic map
    (
        pkg_size            => DIAG_PKG_SIZE_16,
        header_size         => BLMINJ_header_size       
    )
    port map 
    (
        clk             => clk,
        PARAM_header    => header,
        pkg_done        => pkg_done,
        inp_data        => inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => out_data,
        out_vld         => out_vld,
        out_next        => out_next
    );
    
    
end architecture struct;