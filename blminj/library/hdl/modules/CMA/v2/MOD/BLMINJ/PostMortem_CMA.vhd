------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/10/2013
Module Name:    PostMortem_CMA

-----------------
Short Description
-----------------

    Packages all data

------------
Dependencies
------------

    vhdl_func_pkg
    PackageCreator64_CMA
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the incoming data samples
    header_size         := size of the utilized header (in bit)
    pkg_size            := number of 64-bit information blocks that need are packaged
   

--------------
Implementation
--------------
    
    Top Module
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity PostMortem_CMA is

generic
(
    inp_data_size:      integer;
    pkg_size:           integer;
    header_size:        integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Status
    pkg_done:       out std_logic;
    -- Parameters
    PARAM_header:   in  std_logic_vector(header_size-1 downto 0);
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(65 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity PostMortem_CMA;


architecture struct of PostMortem_CMA is
    
BEGIN
    
    --===========--
    -- Packaging --
    --===========--
    
    package_CMA_inst: entity work.package_CMA
    generic map
    (
        inp_data_size   => 64,
        pkg_size        => pkg_size,
        header_size     => header_size
    )
    port map 
    (
        clk             => clk,
        PARAM_header    => PARAM_header,
        pkg_done        => pkg_done,
        inp_data        => (63 downto inp_data_size => '0') & inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => out_data,
        out_vld         => out_vld,
        out_next        => out_next
    );
    
    

end architecture struct;