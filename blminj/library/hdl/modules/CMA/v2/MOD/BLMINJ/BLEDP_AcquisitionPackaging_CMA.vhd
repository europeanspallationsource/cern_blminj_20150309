--------------------------------
--
--Company:        CERN - BE/BI/BL
--Engineer:       Marcel Alsdorf
--Create Date:    20/10/2013
--Module Name:    BLEDP_AcquisitionPackaging_CMA
--
-------------------
--Short Description
-------------------
--
--    This module creates the packages from the Acquisition on the BLEDP card.
--
--------------
--Dependencies
--------------
--
--    vhdl_func_pkg
--    BLMINJ_pkg
--    PackageCreator16_CMA
--    fifo_CMA
--    slice_CMA
-- 
--    
--------------------
--Generics/Constants
--------------------
--    
--    fifo_depth          := depth of the FIFO (at the end)
--    inp_data_size       := size of the CMI input data bus
--    out_data_size       := size of the CMI output data bus
--    
--    sliced_data_size    := size of the sliced_CMI data bus
--    sliced_rx_number    := number of the sliced_CMI receiver modules
--    
--    pkged_data_size     := size of the pkged_CMI data bus
--    pkged_rx_number     := number of the pkged_CMI receiver modules
--   
--
----------------
--Implementation
----------------
--    
--
--    
--
-------------
--Limitations
-------------
--    
--
------------------
--Missing Features
------------------
--
--      redo the header:
--      - add BLEDP slot number
--      - add chip ID or CRC value of the chip ID
--      - extend counter to 32 bits if some space is left
--
--
--
------------------
--Change log
------------------
--  Date            Author      Change
--  12/11/2014      MK          Fixed CRC calculation. It was calculated onlu over first 16 bits of the packet. 
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    use work.BLMINJ_pkg.all;
   
entity BLEDP_AcquisitionPackaging_CMA is
generic
(
    fifo_depth:     integer := 16
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Timing Information
    ACQ_CNT:        in  std_logic_vector(31 downto 0);
    -- CMI Input
    inp_data:       in  std_logic_vector(159 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    out_data:       out std_logic_vector(17 downto 0);
    out_vld:        out std_logic := '0';
    out_next:       in  std_logic
);

end entity BLEDP_AcquisitionPackaging_CMA;


architecture struct of BLEDP_AcquisitionPackaging_CMA is
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    constant inp_data_size:     integer := 160;
    constant out_data_size:     integer := 18;
    
    -- SLICED
    constant sliced_data_size:  integer := out_data_size-2;
    constant sliced_rx_number:  integer := 1;
    
    -- PKGED
    constant pkged_data_size:   integer := out_data_size;
    constant pkged_rx_number:   integer := 1;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal header:                  std_logic_vector(BLMINJ_header_size-1 downto 0);
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    --sliced
    signal sliced_tx_data:    std_logic_vector(sliced_data_size-1 downto 0);
    signal sliced_tx_vld:     std_logic;
    signal sliced_tx_next:    std_logic;
    signal sliced_rx_data:    slv_array(sliced_rx_number-1 downto 0)(sliced_data_size-1 downto 0);
    signal sliced_rx_vld:     std_logic_vector(sliced_rx_number-1 downto 0);
    signal sliced_rx_next:    std_logic_vector(sliced_rx_number-1 downto 0);
    
    -- pkged
    signal pkged_tx_data:     std_logic_vector(pkged_data_size-1 downto 0);
    signal pkged_tx_vld:      std_logic;
    signal pkged_tx_next:     std_logic;
    signal pkged_rx_data:     slv_array(pkged_rx_number-1 downto 0)(pkged_data_size-1 downto 0);
    signal pkged_rx_vld:      std_logic_vector(pkged_rx_number-1 downto 0);
    signal pkged_rx_next:     std_logic_vector(pkged_rx_number-1 downto 0);
      
    
BEGIN
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    header <= BLEDP_CARD_NO & ACQ_TYPE & ACQ_SPC1 & ACQ_SPC2 & ACQ_CNT(23 downto 0) & (15 downto 0 => '0');
    
    --=======--
    -- SLICE --
    --=======--
    
    slice_CMA_inst : entity work.slice_CMA 
    generic map
    (
        inp_data_size   => inp_data_size,
        out_data_size   => sliced_data_size
        
    )
    port map 
    (
        clk             => clk,
        inp_data        => inp_data,
        inp_vld         => inp_vld,
        inp_next        => inp_next,
        out_data        => sliced_tx_data,
        out_vld         => sliced_tx_vld,
        out_next        => sliced_tx_next
    );
    
    
    
    --============--
    -- SLICED CMI --
    --============--
    
    sliced_CMI : entity work.CMI 
    generic map
    (
        rx_number   => sliced_rx_number,
        data_size   => sliced_data_size
    )
    port map 
    (
        clk         => clk,
        tx_data     => sliced_tx_data,
        tx_vld      => sliced_tx_vld,
        tx_next     => sliced_tx_next,
        rx_data     => sliced_rx_data,
        rx_vld      => sliced_rx_vld,
        rx_next     => sliced_rx_next
    );
    
    
    
    --====================--
    -- PACKAGE CREATOR 16 --
    --====================--
    
    PackageCreator16_CMA_inst : entity work.PackageCreator16_CMA 
    generic map
    (
        pkg_size            => ACQ_PKG_SIZE,
        header_size         => BLMINJ_header_size
        
    )
    port map 
    (
        clk             => clk,
        PARAM_header    => header,
        pkg_done        => open,
        inp_data        => sliced_rx_data(0),
        inp_vld         => sliced_rx_vld(0),
        inp_next        => sliced_rx_next(0),
        out_data        => pkged_tx_data,
        out_vld         => pkged_tx_vld,
        out_next        => pkged_tx_next
    );
    
    --===========--
    -- PKGED CMI --
    --===========--
    
    pkged_CMI : entity work.CMI 
    generic map
    (
        rx_number   => pkged_rx_number,
        data_size   => pkged_data_size
    )
    port map 
    (
        clk         => clk,
        tx_data     => pkged_tx_data,
        tx_vld      => pkged_tx_vld,
        tx_next     => pkged_tx_next,
        rx_data     => pkged_rx_data,
        rx_vld      => pkged_rx_vld,
        rx_next     => pkged_rx_next
    );
    
    --======--
    -- FIFO --
    --======--
    
    fifo_CMA_inst: entity work.fifo_CMA
    GENERIC MAP
    (
        data_size   => out_data_size,
        fifo_depth  => fifo_depth
    )
    PORT MAP
    (
        clk                 => clk,
        inp_data            => pkged_rx_data(0),
        inp_vld             => pkged_rx_vld(0),
        inp_next            => pkged_rx_next(0),
        out_data            => out_data,
        out_vld             => out_vld,
        out_next            => out_next
    );
    
    

end architecture struct;
