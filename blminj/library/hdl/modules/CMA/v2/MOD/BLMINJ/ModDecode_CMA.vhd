------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Csaba Hajdu
Create Date:    2015/02/20
Module Name:    ModDecode_CMA

-----------------
Short Description
-----------------

    Connectivity check modulation decoder. Instantiates the FFT core required for decoding

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------
  

--------------
Implementation
--------------
       

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity ModDecode_CMA is


    generic
    (
        inp_data_size           :   integer := 64;
        RS_window_length        :   time;
        RS_gran                 :   time
    );

    port
    (
        -- Clock
        clk                     :   in  std_logic;

        -- CMI Input: tapped signals from accumulator
        inp_data                :   in  std_logic_vector(inp_data_size-1 downto 0);
        inp_vld                 :   in  std_logic;
        inp_next                :   out std_logic;
    
        -- CMI Output: FFT data
        out_real                :   out std_logic_vector(17 downto 0);
        out_imag                :   out std_logic_vector(17 downto 0);
        out_exp                 :   out std_logic_vector(5 downto 0);
        out_sop                 :   out std_logic;
        out_eop                 :   out std_logic;
        out_vld                 :   out std_logic := '0';
        out_next                :   in  std_logic;
        out_error               :   out std_logic_vector(1 downto 0);       -- For debugging, probably not extremely useful
        FFTin_or_out_valid      :   out std_logic                           -- For debugging, FFT input or output valid signal
    );

end entity ModDecode_CMA;


architecture struct of ModDecode_CMA is

    -- Number of raw samples per running sum sample (for decimation)
    constant num_samples        :   integer := RS_window_length / RS_gran;
    -- The bit number required for counting to this number
    constant num_samples_cbits  :   integer := get_vsize(num_samples);

    
    -- FFT core input/output signals
    signal FFTin_data           :   std_logic_vector(17 downto 0);
    signal FFTin_sop            :   std_logic;
    signal FFTin_eop	        :   std_logic;
    signal FFTin_valid	        :   std_logic;
    signal FFTin_ready	        :   std_logic;

    signal FFTout_real          :   std_logic_vector(17 downto 0);
    signal FFTout_imag          :   std_logic_vector(17 downto 0);
    signal FFTout_exp           :   std_logic_vector(5 downto 0);
    signal FFTout_sop           :   std_logic;
    signal FFTout_eop           :   std_logic;
    signal FFTout_valid         :   std_logic;
    signal FFTout_ready         :   std_logic;
    signal FFTout_error         :   std_logic_vector(1 downto 0);
      
    -- FFT sample counter
    signal FFT_sample_cnt       :   unsigned(9 downto 0) := (others => '0');
    
    -- Internal ready signal to accumulator
    signal inp_next_int         :   std_logic;

    --===============--
    -- Debug signals --
    --===============--    
    signal FFT_overflow         :   std_logic := '0';                           -- FFT overflow flag
    signal sysclk_cnt           :   unsigned(31 downto 0) := (others => '0');   -- System clock cycle counter as absolute timer
    
    
    --==================--
    -- Debug attributes --
    --==================--    
    attribute noprune: boolean;
    attribute keep: boolean;
    attribute preserve: boolean;

    attribute noprune of ModDecode_CMA:entity is true;
    attribute preserve of sysclk_cnt:signal is true;
    attribute keep of sysclk_cnt:signal is true;

    
begin

    --=======--
    -- Debug --
    --=======--
    
    -- Input or output valid signal
    FFTin_or_out_valid <= FFTin_valid OR FFTout_valid;
    
    
    --=====================--
    -- Clock cycle counter --
    --=====================--    
    process(clk) is
    begin
        if rising_edge(clk) then
            sysclk_cnt <= sysclk_cnt + 1;
        end if;
    end process;

    
    --=======================--
    -- Data flow accu => FFT --
    --=======================--       
    process(clk) is

        -- Input sample counter: we only want to keep one sample per integration window length (decimation)
        variable in_sample_cnt        :   unsigned(num_samples_cbits-1 downto 0) := (others => '0');
    
    begin
        if rising_edge(clk) then           
            -- Default values
            FFTin_valid <= '0';
            FFTin_sop   <= '0';
            FFTin_eop   <= '0';
            
            -- New data available from accumulator
            if inp_vld = '1' then
                -- Count samples
                if in_sample_cnt < num_samples then
                    in_sample_cnt := in_sample_cnt + 1;
                else
                    in_sample_cnt := (others => '0');
                end if;
                
                -- Data corresponding to one integration window length received: take data and pass it to FFT core
                if in_sample_cnt = 0 then
                
                    -- Register data (tacitly discard many MSBs)
                    FFTin_data <= inp_data(17 downto 0);
                
                    -- Block new data from accumulator if FFT core is not ready
                    inp_next_int <= '0';
                
                    -- FFT core ready for data
                    if FFTin_ready = '1' then
                        -- Assert data valid signal to FFT core (Avalon spec: "The source may only assert valid and transfer data during ready cycles.")
                        FFTin_valid <= '1';

                        -- Sample counter
                        FFT_sample_cnt <= FFT_sample_cnt + 1;
                
                        -- Start of packet when counter = 0
                        if FFT_sample_cnt = 0 then
                            FFTin_sop <= '1';
                        end if;

                        -- End of packet when counter = 2^10 - 1
                        if FFT_sample_cnt = B"11_1111_1111" then
                            FFTin_eop <= '1';
                        end if;
                    
                        -- Request new data from accumulator
                        inp_next_int <= '1';
                    end if;               
                end if;     -- Otherwise, do nothing                
            end if;
        end if;
    end process;

    -- CMA invalid filter for ready signal to accumulator
    -- - Asserted when inp_vld = 0 to avoid invalid state
    -- - Asserted when FFT input ready for next packet
    inp_next <= inp_next_int OR NOT inp_vld;
    

    --=========================--
    -- Data flow FFT => output --
    --=========================--
    -- Actually transparent...    
    out_real <= FFTout_real;
    out_imag <= FFTout_imag;
    out_exp  <= FFTout_exp;
    out_sop  <= FFTout_sop;
    out_eop  <= FFTout_eop;
    out_vld  <= FFTout_valid;
    out_error <= FFTout_error;
    
    FFTout_ready <= out_next;   

    
    --==========--
    -- FFT core --
    --==========--
    FFT_inst: entity work.fft
	port map (
		clk	            => clk,
		reset_n	        => '1',
		inverse         => '0',             -- Inverse FFT is calculated if asserted
		
        -- Input side
		sink_real	    => FFTin_data,
		sink_imag       => (others => '0'),
		sink_sop	    => FFTin_sop,
		sink_eop	    => FFTin_eop,
        sink_valid      => FFTin_valid,     -- module -> FFT core
		sink_ready      => FFTin_ready,     -- FFT core -> module
		sink_error      => (others => '0'),

        -- Output side
		source_real	    => FFTout_real,
		source_imag	    => FFTout_imag,
		source_exp	    => FFTout_exp,
		source_sop	    => FFTout_sop,
		source_eop	    => FFTout_eop,
		source_valid	=> FFTout_valid,    -- FFT core -> module
		source_ready	=> FFTout_ready,    -- module -> FFT core
		source_error	=> FFTout_error
	);
    
end architecture struct;