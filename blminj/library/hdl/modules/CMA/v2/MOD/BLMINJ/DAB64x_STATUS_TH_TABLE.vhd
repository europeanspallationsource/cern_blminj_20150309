------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2014
Module Name:    DAB64x_STATUS_TH_TABLE


-----------------
Short Description
-----------------

    Parameter Table for the Diagnsotic Reader of the DAB64x card.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    
    
--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DAB64x_STATUS_TH_TABLE is
port
(
    -- Clock
    clk:                in  std_logic;
    -- CMI Input
    inp_data:           in  std_logic_vector(15 downto 0);
    inp_vld:            in  std_logic;
    inp_next:           out std_logic;
    -- Parameters
    PARAM_INTF_0x00:    out std_logic_vector(31 downto 0);
    PARAM_INTF_0x01:    out std_logic_vector(15 downto 0);
    PARAM_INTF_0x02:    out std_logic_vector(15 downto 0);
    PARAM_INTF_0x03:    out std_logic_vector(63 downto 0)
    
);

end entity DAB64x_STATUS_TH_TABLE;


architecture struct of DAB64x_STATUS_TH_TABLE is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant param_data_size:   integer := 16;
    constant param_table_size:  integer := 8;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Table Output
    signal param_vector:        slv_array(0 to param_table_size-1)(param_data_size-1 downto 0);
    
BEGIN
    
    --===================--
    -- BASIC PARAM TABLE --
    --===================--  
    
    param_table_CMA_inst: entity work.param_table_CMA
    generic map
    (
        inp_data_size   => param_data_size,
        table_size      => param_table_size
    )
    port map 
    (
        clk         => clk,
        inp_data    => inp_data,
        inp_vld     => inp_vld,
        inp_next    => inp_next,
        params      => param_vector
    );
    
    
    --================--
    -- VECTOR MAPPING --
    --================-- 
    
    PARAM_INTF_0x00     <= param_vector(0) & param_vector(1);
    PARAM_INTF_0x01     <= param_vector(2);
    PARAM_INTF_0x02     <= param_vector(3);
    PARAM_INTF_0x03     <= param_vector(4) & param_vector(5) & param_vector(7) & param_vector(8);
     
 
end architecture struct;