------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        01/05/2014
Module Name:    DAB64x_VME_BusCtrl


-----------------
Short Description
-----------------

    This module is mapping some of the standard VME Interface signals to the signals used/renamed on the DAB64x card.
    Additionally it controls the VME I/O Signals and handles the access to the VME_DATA lines between the VME Core and 
    the Flash Interface.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    

--------------
Implementation
--------------


     
     
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------
    
*/
------------------------------


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;


entity DAB64x_VME_BusCtrl is
port
(
    -- Clock
    clk:                    in std_logic;
    -- DAB64x Board Inputs
    Stratix_IRQ_VECTORn:    out std_logic;
    VME_IRQ_Level:          in std_logic_vector(2 downto 0);
    IRQ_Vector_Enable:      in std_logic;
    Board_Rst_ONOFFn:       in std_logic;
    -- DAB64x Board Outputs
    Stratix_VME_ACCn:       out std_logic;
    VME_ForceRetry:         out std_logic;  
    VME_2e_Cycle:           out std_logic;  
    Stratix_245_OEn:        out std_logic;   
    Stratix_dtackN:         out std_logic;
    Stratix_SYSFAILn:       out std_logic;
    -- DAB64x Board IOs
    VME_ADDR:               inout std_logic_vector(31 downto 1);
    VME_LWORDn:             inout std_logic;
    VME_DATA:               inout std_logic_vector(31 downto 0);
    VME_DTACK_EN:           inout std_logic;  
    -- VME_Interface Inputs
    VME_LWORDn_OUT:         in  std_logic;
    VME_DATA_OUT:           in  std_logic_vector(31 downto 0);
    VME_ADDR_OUT:           in  std_logic_vector(31 downto 1);
    
    VME_DATA_BUF_OEn:       in  std_logic;
    VME_ADDR_BUF_OEn:       in  std_logic;
    VME_DTACK_BUF_OEn:      in  std_logic;
    VME_RETRY_BUF_OEn:      in  std_logic;
    VME_BERR_BUF_OEn:       in  std_logic;
    VME_DATA_BUF_DIR:       in  std_logic;
    VME_ADDR_BUF_DIR:       in  std_logic;
    
    VME_ACC:                in  std_logic;
    VME_DTACKn:             in  std_logic;
    VME_SYSFAILn:           in  std_logic;
    -- VME_Interface Outputs
    VME_LWORDn_IN:          out std_logic;
    VME_DATA_IN:            out std_logic_vector(31 downto 0);
    VME_ADDR_IN:            out std_logic_vector(31 downto 1);
    VME_BUS_BUSY:           out std_logic;
    -- Flash_Interface
    FLASH_DATA_IN:          out std_logic_vector(7 downto 0);
    FLASH_DATA_OUT:         in  std_logic_vector(7 downto 0);
    FLASH_DATA_BUS_DIR:     in  std_logic;
    FLASH_BUS_REQ:          in  std_logic;
    FLASH_BUS_ACK:          out std_logic
);
    
end entity DAB64x_VME_BusCtrl;



architecture struct of DAB64x_VME_BusCtrl is

    --=====--
    -- FSM --
    --=====--
    
    type state is (VME_INTF_ACC, FLASH_INTF_ACC);
    
    signal present_state, next_state: state := VME_INTF_ACC;
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal FLASH_ACC:   std_logic;
    
begin
    --================--
    -- Stratix to MAX --
    --================--
     
    -- Stratix VME Access
    Stratix_VME_ACCn    <= NOT VME_ACC OR FLASH_ACC;
    
    -- IRQ
    Stratix_IRQ_VECTORn <= '1';
    
    -- mapping
    Stratix_dtackN      <= VME_DTACKn;
    Stratix_SYSFAILn    <= VME_SYSFAILn;
    
    
    --================--
    -- BUFFER CONTROL --
    --================--
    
    -- Addr Bus Buffer Direction
    VME_2e_Cycle        <= VME_ADDR_BUF_DIR;
    
    -- Data Bus Buffer Output Enable
    Stratix_245_OEn     <= '1' when FLASH_ACC ='1' else -- disconnect when Flash Access
                           VME_DATA_BUF_OEn;
    
    -- RETRY_BUF_OEn = 0 -> Buffer set to output, otherwise buffer is set to input
    VME_ForceRetry      <= NOT VME_RETRY_BUF_OEn;
    
    -- handled by the MAX device (through DTACKn), disconnect here
    VME_DTACK_EN        <= 'Z'; 
    
    --=========--
    -- VME IOs --
    --=========--
    
    -- only accessed by VME_INTERFACE
    VME_ADDR                <= VME_ADDR_OUT                 when VME_ADDR_BUF_DIR = '1'  else (others => 'Z');   
    VME_LWORDn              <= VME_LWORDn_OUT               when VME_ADDR_BUF_DIR = '1'  else 'Z';
    VME_DATA(31 downto 8)   <= VME_DATA_OUT (31 downto 8)   when VME_DATA_BUF_DIR = '1'  else (others => 'Z'); 
    
    
    -- accessed by both
    VME_DATA(7 downto 0)    <= FLASH_DATA_OUT               when FLASH_ACC = '1' AND FLASH_DATA_BUS_DIR = '1' else
                               VME_DATA_OUT (7 downto 0)    when FLASH_ACC = '0' AND VME_DATA_BUF_DIR = '1'  else (others => 'Z'); 
    
    -- Inputs
    VME_ADDR_IN     <= VME_ADDR;
    VME_DATA_IN     <= VME_DATA;
    VME_LWORDn_IN   <= VME_LWORDn;
    FLASH_DATA_IN   <= VME_DATA(7 downto 0);
    
    
    --=====--
    -- FSM --
    --=====--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    
    process (all)
    begin
        -- DEFAULTS
        VME_BUS_BUSY    <= '0';
        FLASH_BUS_ACK   <= '0';
        FLASH_ACC       <= '0';
            
        case present_state is
            
            --===============--
            when VME_INTF_ACC =>
            --===============--
                
                if VME_ACC = '0' AND FLASH_BUS_req = '1' then
                    next_state <= FLASH_INTF_ACC;
                else
                    next_state <= VME_INTF_ACC;
                end if;
                 
            --===============--
            when FLASH_INTF_ACC =>
            --===============--
            
                VME_BUS_BUSY    <= '1';
                FLASH_BUS_ACK   <= '1';
                FLASH_ACC       <= '1';
                
                if FLASH_BUS_req = '0' then
                    next_state <= VME_INTF_ACC;
                else
                    next_state <= FLASH_INTF_ACC;
                end if;
            
            --===============--
            when others => 
            --===============--
            
                next_state <= VME_INTF_ACC;
            
        end case;
    end process;
    
    
end architecture struct;




