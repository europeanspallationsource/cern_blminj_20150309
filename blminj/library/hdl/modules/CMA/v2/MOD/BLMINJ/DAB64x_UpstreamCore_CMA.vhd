------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    18/11/2013
Module Name:    DAB64x_UpstreamCore_CMA

-----------------
Short Description
-----------------


    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------



    
--------------
Implementation
--------------
    

    
-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity DAB64x_UpstreamCore_CMA is

generic
(
    pkg_data_size:          integer := 66;
    VME_rdreq_data_size:    integer := 64;
    VME_wrreq_data_size:    integer := 136;
    VME_rdout_data_size:    integer := 64;
    VME_wrreq_addr_MSB:     integer := 135;
    VME_wrreq_byteena_MSB:  integer := 71;
    VME_wrreq_d_MSB:        integer := 63
);

port
(
    -- Clock
    clk:                in  std_logic;
    -- Control
    SB_pbl:             in  std_logic;
    lock_RAM:           in  std_logic;
    -- Package CMI Input
    pkg_data:           in  std_logic_vector(pkg_data_size-1 downto 0);
    pkg_vld:            in  std_logic;
    pkg_next:           out std_logic;
    -- RAM CMI Input
    VME_rdreq_data:     in  std_logic_vector(VME_rdreq_data_size-1 downto 0);
    VME_rdreq_vld:      in  std_logic;
    VME_rdreq_next:     out std_logic;
    VME_wrreq_data:     in  std_logic_vector(VME_wrreq_data_size-1 downto 0);
    VME_wrreq_vld:      in  std_logic;
    VME_wrreq_next:     out std_logic;
    -- RAM CMI Output
    VME_rdout_data:     out std_logic_vector(VME_rdout_data_size-1 downto 0);
    VME_rdout_vld:      out std_logic := '0';
    VME_rdout_next:     in  std_logic
    
);

end entity DAB64x_UpstreamCore_CMA;


architecture struct of DAB64x_UpstreamCore_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- General
    constant header_size:               integer := 64;
    constant wr_addr_size:              integer := 32;
    
    -- RAMs
    constant num_rams:                  integer := 7;
    constant num_wr_rams:               integer := 4;
    constant RAM_data_size:             integer := 64;
    constant RAM_byteena_size:          integer := 8;
    
    constant RAM_EVO_numwords:          integer := 10496;
    constant RAM_EVO_addr_size:         integer := get_vsize_addr(RAM_EVO_numwords);
    constant RAM_EVO_data_size:         integer := RAM_data_size;
    constant RAM_EVO_byteena_size:      integer := RAM_byteena_size;
    constant RAM_EVO_init_file:         string  := "UNUSED";
    constant RAM_EVO_type:              string  := "M-RAM";
    
    constant RAM_CAP_numwords:          integer := 4352;
    constant RAM_CAP_addr_size:         integer := get_vsize_addr(RAM_CAP_numwords);
    constant RAM_CAP_data_size:         integer := RAM_data_size;
    constant RAM_CAP_byteena_size:      integer := RAM_byteena_size;
    constant RAM_CAP_init_file:         string  := "UNUSED";
    constant RAM_CAP_type:              string  := "M-RAM";
    
    constant RAM_LOSS_numwords:         integer := 512;
    constant RAM_LOSS_addr_size:        integer := get_vsize_addr(RAM_LOSS_numwords);
    constant RAM_LOSS_data_size:        integer := RAM_data_size;
    constant RAM_LOSS_byteena_size:     integer := RAM_byteena_size;
    constant RAM_LOSS_init_file:        string  := "LOSS.mif";
    constant RAM_LOSS_type:             string  := "M4K";
    
    constant RAM_DIAG_numwords:         integer := 512;
    constant RAM_DIAG_addr_size:        integer := get_vsize_addr(RAM_DIAG_numwords);
    constant RAM_DIAG_data_size:        integer := RAM_data_size;
    constant RAM_DIAG_byteena_size:     integer := RAM_byteena_size;
    constant RAM_DIAG_init_file:        string  := "DIAG.mif";
    constant RAM_DIAG_type:             string  := "M4K";
    
    constant RAM_PARAM_numwords:        integer := 512;
    constant RAM_PARAM_addr_size:       integer := get_vsize_addr(RAM_PARAM_numwords);
    constant RAM_PARAM_data_size:       integer := RAM_data_size;
    constant RAM_PARAM_byteena_size:    integer := RAM_byteena_size;
    constant RAM_PARAM_init_file:       string  := "PARAM.mif";
    constant RAM_PARAM_type:            string  := "M4K";
    
    constant RAM_PROG_numwords:         integer := 1024;
    constant RAM_PROG_addr_size:        integer := get_vsize_addr(RAM_PROG_numwords);
    constant RAM_PROG_data_size:        integer := RAM_data_size;
    constant RAM_PROG_byteena_size:     integer := RAM_byteena_size;
    constant RAM_PROG_init_file:        string  := "PROG.mif";
    constant RAM_PROG_type:             string  := "M4K";
    
    constant RAM_CTRL_numwords:         integer := 256;
    constant RAM_CTRL_addr_size:        integer := get_vsize_addr(RAM_CTRL_numwords);
    constant RAM_CTRL_data_size:        integer := RAM_data_size;
    constant RAM_CTRL_byteena_size:     integer := RAM_byteena_size;
    constant RAM_CTRL_init_file:        string  := "CTRL.mif";
    constant RAM_CTRL_type:             string  := "M4K";
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- PKG GATE
    constant pkg_gate_data_size:        integer := pkg_data_size;
    constant pkg_gate_rx_number:        integer := 1;
    
    -- PKG HEADER
    constant pkg_header_data_size:      integer := header_size;
    constant pkg_header_rx_number:      integer := 1;
    
    -- PKG BODY
    constant pkg_body_data_size:        integer := pkg_data_size-2;
    constant pkg_body_rx_number:        integer := 1;
    
    -- WRCtrl
    constant wrctrl_tx_data_size:       integer := pkg_body_data_size + wr_addr_size + 4; -- with ID
    constant wrctrl_rx_data_size:       integer := pkg_body_data_size + wr_addr_size; -- without ID
    constant wrctrl_rx_number:          integer := num_wr_rams;
    
    -- VME wrreq
    constant VME_wrreq_rx_number:       integer := num_rams;    
    constant VME_wrreq_addr_size:       integer := 64;
    
    -- VME rdreq
    constant VME_rdreq_rx_number:       integer := num_rams;
    constant VME_rdreq_addr_size:       integer := 64;
    
    -- VME rdout
    constant VME_rdout_tx_number:       integer := num_rams;
    constant VME_rdout_rx_number:       integer := 1;
    
    
    -- Scoreboard CMIs
    constant EVO_SB_data_size:          integer := RAM_EVO_data_size + RAM_EVO_addr_size;
    constant EVO_SB_d_MSB:              integer := RAM_EVO_data_size-1;
    constant EVO_SB_addr_MSB:           integer := EVO_SB_data_size-1;
    constant EVO_SB_rx_number:          integer := 1;
    constant EVO_SB_base_addr_full:     std_logic_vector(63 downto 0) := 64UX"285C";
    constant EVO_SB_base_addr:          std_logic_vector(RAM_EVO_addr_size-1 downto 0) := EVO_SB_base_addr_full(RAM_EVO_addr_size-1 downto 0);
    
    constant CAP_SB_data_size:          integer := RAM_CAP_data_size + RAM_CAP_addr_size;
    constant CAP_SB_d_MSB:              integer := RAM_CAP_data_size-1;
    constant CAP_SB_addr_MSB:           integer := CAP_SB_data_size-1;
    constant CAP_SB_rx_number:          integer := 1;
    constant CAP_SB_base_addr_full:     std_logic_vector(63 downto 0) := 64UX"10BC";
    constant CAP_SB_base_addr:          std_logic_vector(RAM_CAP_addr_size-1 downto 0) := CAP_SB_base_addr_full(RAM_CAP_addr_size-1 downto 0);
    
    constant LOSS_SB_data_size:         integer := RAM_LOSS_data_size + RAM_LOSS_addr_size;
    constant LOSS_SB_d_MSB:             integer := RAM_LOSS_data_size-1;
    constant LOSS_SB_addr_MSB:          integer := LOSS_SB_data_size-1;
    constant LOSS_SB_rx_number:         integer := 1;
    constant LOSS_SB_base_addr_full:    std_logic_vector(63 downto 0) := 64UX"01F8";
    constant LOSS_SB_base_addr:         std_logic_vector(RAM_LOSS_addr_size-1 downto 0) := LOSS_SB_base_addr_full(RAM_LOSS_addr_size-1 downto 0);
    
    constant DIAG_SB_data_size:         integer := RAM_DIAG_data_size + RAM_DIAG_addr_size;
    constant DIAG_SB_d_MSB:             integer := RAM_DIAG_data_size-1;
    constant DIAG_SB_addr_MSB:          integer := DIAG_SB_data_size-1;
    constant DIAG_SB_rx_number:         integer := 1;
    constant DIAG_SB_base_addr:         std_logic_vector(RAM_DIAG_addr_size-1 downto 0) := (others => '0'); -- HAS TO BE CHANGE, most likely no SB for DIAG
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- RAM Connections
    signal RAM_EVO_portA_addr:          std_logic_vector(RAM_EVO_addr_size-1 downto 0);
    signal RAM_EVO_portA_data:          std_logic_vector(RAM_EVO_data_size-1 downto 0);
    signal RAM_EVO_portA_byteena:       std_logic_vector(RAM_EVO_byteena_size-1 downto 0);
    signal RAM_EVO_portA_wren:          std_logic;
    signal RAM_EVO_portA_q:             std_logic_vector(RAM_EVO_data_size-1 downto 0);
    
    signal RAM_EVO_portB_addr:          std_logic_vector(RAM_EVO_addr_size-1 downto 0);
    signal RAM_EVO_portB_data:          std_logic_vector(RAM_EVO_data_size-1 downto 0);
    signal RAM_EVO_portB_byteena:       std_logic_vector(RAM_EVO_byteena_size-1 downto 0);
    signal RAM_EVO_portB_wren:          std_logic;
    signal RAM_EVO_portB_q:             std_logic_vector(RAM_EVO_data_size-1 downto 0);
    
    signal RAM_CAP_portA_addr:          std_logic_vector(RAM_CAP_addr_size-1 downto 0);
    signal RAM_CAP_portA_data:          std_logic_vector(RAM_CAP_data_size-1 downto 0);
    signal RAM_CAP_portA_byteena:       std_logic_vector(RAM_CAP_byteena_size-1 downto 0);
    signal RAM_CAP_portA_wren:          std_logic;
    signal RAM_CAP_portA_q:             std_logic_vector(RAM_CAP_data_size-1 downto 0);
    
    signal RAM_CAP_portB_addr:          std_logic_vector(RAM_CAP_addr_size-1 downto 0);
    signal RAM_CAP_portB_data:          std_logic_vector(RAM_CAP_data_size-1 downto 0);
    signal RAM_CAP_portB_byteena:       std_logic_vector(RAM_CAP_byteena_size-1 downto 0);
    signal RAM_CAP_portB_wren:          std_logic;
    signal RAM_CAP_portB_q:             std_logic_vector(RAM_CAP_data_size-1 downto 0);
    
    signal RAM_LOSS_portA_addr:         std_logic_vector(RAM_LOSS_addr_size-1 downto 0);
    signal RAM_LOSS_portA_data:         std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    signal RAM_LOSS_portA_byteena:      std_logic_vector(RAM_LOSS_byteena_size-1 downto 0);
    signal RAM_LOSS_portA_wren:         std_logic;
    signal RAM_LOSS_portA_q:            std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    
    signal RAM_LOSS_portB_addr:         std_logic_vector(RAM_LOSS_addr_size-1 downto 0);
    signal RAM_LOSS_portB_data:         std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    signal RAM_LOSS_portB_byteena:      std_logic_vector(RAM_LOSS_byteena_size-1 downto 0);
    signal RAM_LOSS_portB_wren:         std_logic;
    signal RAM_LOSS_portB_q:            std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    
    signal RAM_DIAG_portA_addr:         std_logic_vector(RAM_DIAG_addr_size-1 downto 0);
    signal RAM_DIAG_portA_data:         std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    signal RAM_DIAG_portA_byteena:      std_logic_vector(RAM_DIAG_byteena_size-1 downto 0);
    signal RAM_DIAG_portA_wren:         std_logic;
    signal RAM_DIAG_portA_q:            std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    
    signal RAM_DIAG_portB_addr:         std_logic_vector(RAM_DIAG_addr_size-1 downto 0);
    signal RAM_DIAG_portB_data:         std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    signal RAM_DIAG_portB_byteena:      std_logic_vector(RAM_DIAG_byteena_size-1 downto 0);
    signal RAM_DIAG_portB_wren:         std_logic;
    signal RAM_DIAG_portB_q:            std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    
    signal RAM_PARAM_portA_addr:        std_logic_vector(RAM_PARAM_addr_size-1 downto 0);
    signal RAM_PARAM_portA_data:        std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    signal RAM_PARAM_portA_byteena:     std_logic_vector(RAM_PARAM_byteena_size-1 downto 0);
    signal RAM_PARAM_portA_wren:        std_logic;
    signal RAM_PARAM_portA_q:           std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    
    signal RAM_PARAM_portB_addr:        std_logic_vector(RAM_PARAM_addr_size-1 downto 0);
    signal RAM_PARAM_portB_data:        std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    signal RAM_PARAM_portB_byteena:     std_logic_vector(RAM_PARAM_byteena_size-1 downto 0);
    signal RAM_PARAM_portB_wren:        std_logic;
    signal RAM_PARAM_portB_q:           std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    
    signal RAM_PROG_portA_addr:         std_logic_vector(RAM_PROG_addr_size-1 downto 0);
    signal RAM_PROG_portA_data:         std_logic_vector(RAM_PROG_data_size-1 downto 0);
    signal RAM_PROG_portA_byteena:      std_logic_vector(RAM_PROG_byteena_size-1 downto 0);
    signal RAM_PROG_portA_wren:         std_logic;
    signal RAM_PROG_portA_q:            std_logic_vector(RAM_PROG_data_size-1 downto 0);
    
    signal RAM_PROG_portB_addr:         std_logic_vector(RAM_PROG_addr_size-1 downto 0);
    signal RAM_PROG_portB_data:         std_logic_vector(RAM_PROG_data_size-1 downto 0);
    signal RAM_PROG_portB_byteena:      std_logic_vector(RAM_PROG_byteena_size-1 downto 0);
    signal RAM_PROG_portB_wren:         std_logic;
    signal RAM_PROG_portB_q:            std_logic_vector(RAM_PROG_data_size-1 downto 0);
    
    signal RAM_CTRL_portA_addr:         std_logic_vector(RAM_CTRL_addr_size-1 downto 0);
    signal RAM_CTRL_portA_data:         std_logic_vector(RAM_CTRL_data_size-1 downto 0);
    signal RAM_CTRL_portA_byteena:      std_logic_vector(RAM_CTRL_byteena_size-1 downto 0);
    signal RAM_CTRL_portA_wren:         std_logic;
    signal RAM_CTRL_portA_q:            std_logic_vector(RAM_CTRL_data_size-1 downto 0);
    
    signal RAM_CTRL_portB_addr:         std_logic_vector(RAM_CTRL_addr_size-1 downto 0);
    signal RAM_CTRL_portB_data:         std_logic_vector(RAM_CTRL_data_size-1 downto 0);
    signal RAM_CTRL_portB_byteena:      std_logic_vector(RAM_CTRL_byteena_size-1 downto 0);
    signal RAM_CTRL_portB_wren:         std_logic;
    signal RAM_CTRL_portB_q:            std_logic_vector(RAM_CTRL_data_size-1 downto 0);
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- pkg_gate
    signal pkg_gate_tx_data:            std_logic_vector(pkg_gate_data_size-1 downto 0);
    signal pkg_gate_tx_vld:             std_logic;
    signal pkg_gate_tx_next:            std_logic;
    signal pkg_gate_rx_data:            slv_array(pkg_gate_rx_number-1 downto 0)(pkg_gate_data_size-1 downto 0);
    signal pkg_gate_rx_vld:             std_logic_vector(pkg_gate_rx_number-1 downto 0);
    signal pkg_gate_rx_next:            std_logic_vector(pkg_gate_rx_number-1 downto 0);
    
    -- pkg_header
    signal pkg_header_tx_data:          std_logic_vector(pkg_header_data_size-1 downto 0);
    signal pkg_header_tx_vld:           std_logic;
    signal pkg_header_tx_next:          std_logic;
    signal pkg_header_rx_data:          slv_array(pkg_header_rx_number-1 downto 0)(pkg_header_data_size-1 downto 0);
    signal pkg_header_rx_vld:           std_logic_vector(pkg_header_rx_number-1 downto 0);
    signal pkg_header_rx_next:          std_logic_vector(pkg_header_rx_number-1 downto 0);
    
    -- pkg_body
    signal pkg_body_tx_data:            std_logic_vector(pkg_body_data_size-1 downto 0);
    signal pkg_body_tx_vld:             std_logic;
    signal pkg_body_tx_next:            std_logic;
    signal pkg_body_rx_data:            slv_array(pkg_body_rx_number-1 downto 0)(pkg_body_data_size-1 downto 0);
    signal pkg_body_rx_vld:             std_logic_vector(pkg_body_rx_number-1 downto 0);
    signal pkg_body_rx_next:            std_logic_vector(pkg_body_rx_number-1 downto 0);
    
    -- wrctrl
    signal wrctrl_tx_data:              std_logic_vector(wrctrl_tx_data_size-1 downto 0);
    signal wrctrl_tx_vld:               std_logic;
    signal wrctrl_tx_next:              std_logic;
    signal wrctrl_rx_data:              slv_array(wrctrl_rx_number-1 downto 0)(wrctrl_rx_data_size-1 downto 0);
    signal wrctrl_rx_vld:               std_logic_vector(wrctrl_rx_number-1 downto 0);
    signal wrctrl_rx_next:              std_logic_vector(wrctrl_rx_number-1 downto 0);
    
    signal wrctrl_ADM_tx_data:          std_logic_vector(wrctrl_tx_data_size-1 downto 0);
    signal wrctrl_ADM_rx_data:          std_logic_vector(wrctrl_rx_data_size-1 downto 0);
    signal wrctrl_ADM_sel:              std_logic_vector(get_vsize_addr(wrctrl_rx_number)-1 downto 0);
    
    -- EVO SB
    signal EVO_SB_tx_data:              std_logic_vector(EVO_SB_data_size-1 downto 0);
    signal EVO_SB_tx_vld:               std_logic;
    signal EVO_SB_tx_next:              std_logic;
    signal EVO_SB_rx_data:              slv_array(EVO_SB_rx_number-1 downto 0)(EVO_SB_data_size-1 downto 0);
    signal EVO_SB_rx_vld:               std_logic_vector(EVO_SB_rx_number-1 downto 0);
    signal EVO_SB_rx_next:              std_logic_vector(EVO_SB_rx_number-1 downto 0);
    
    -- CAP SB
    signal CAP_SB_tx_data:              std_logic_vector(CAP_SB_data_size-1 downto 0);
    signal CAP_SB_tx_vld:               std_logic;
    signal CAP_SB_tx_next:              std_logic;
    signal CAP_SB_rx_data:              slv_array(CAP_SB_rx_number-1 downto 0)(CAP_SB_data_size-1 downto 0);
    signal CAP_SB_rx_vld:               std_logic_vector(CAP_SB_rx_number-1 downto 0);
    signal CAP_SB_rx_next:              std_logic_vector(CAP_SB_rx_number-1 downto 0);
    
    -- LOSS SB
    signal LOSS_SB_tx_data:             std_logic_vector(LOSS_SB_data_size-1 downto 0);
    signal LOSS_SB_tx_vld:              std_logic;
    signal LOSS_SB_tx_next:             std_logic;
    signal LOSS_SB_rx_data:             slv_array(LOSS_SB_rx_number-1 downto 0)(LOSS_SB_data_size-1 downto 0);
    signal LOSS_SB_rx_vld:              std_logic_vector(LOSS_SB_rx_number-1 downto 0);
    signal LOSS_SB_rx_next:             std_logic_vector(LOSS_SB_rx_number-1 downto 0);
    
    -- DIAG SB
    signal DIAG_SB_tx_data:             std_logic_vector(DIAG_SB_data_size-1 downto 0);
    signal DIAG_SB_tx_vld:              std_logic;
    signal DIAG_SB_tx_next:             std_logic;
    signal DIAG_SB_rx_data:             slv_array(DIAG_SB_rx_number-1 downto 0)(DIAG_SB_data_size-1 downto 0);
    signal DIAG_SB_rx_vld:              std_logic_vector(DIAG_SB_rx_number-1 downto 0);
    signal DIAG_SB_rx_next:             std_logic_vector(DIAG_SB_rx_number-1 downto 0);
    
    -- VME_rdreq 
    signal VME_rdreq_tx_data:           std_logic_vector(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_tx_vld:            std_logic;
    signal VME_rdreq_tx_next:           std_logic;
    signal VME_rdreq_rx_data:           slv_array(VME_rdreq_rx_number-1 downto 0)(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_rx_vld:            std_logic_vector(VME_rdreq_rx_number-1 downto 0);
    signal VME_rdreq_rx_next:           std_logic_vector(VME_rdreq_rx_number-1 downto 0);
    
    signal VME_rdreq_ADM_tx_data:       std_logic_vector(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_ADM_rx_data:       std_logic_vector(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_ADM_sel:           std_logic_vector(get_vsize_addr(VME_rdreq_rx_number)-1 downto 0);
    
    -- VME_wrreq
    signal VME_wrreq_tx_data:           std_logic_vector(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_tx_vld:            std_logic;
    signal VME_wrreq_tx_next:           std_logic;
    signal VME_wrreq_rx_data:           slv_array(VME_wrreq_rx_number-1 downto 0)(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_rx_vld:            std_logic_vector(VME_wrreq_rx_number-1 downto 0);
    signal VME_wrreq_rx_next:           std_logic_vector(VME_wrreq_rx_number-1 downto 0);
    
    signal VME_wrreq_ADM_tx_data:       std_logic_vector(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_ADM_rx_data:       std_logic_vector(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_ADM_sel:           std_logic_vector(get_vsize_addr(VME_wrreq_rx_number)-1 downto 0);
    
    -- VME_rdout
    signal VME_rdout_tx_data:           slv_array(VME_rdout_tx_number-1 downto 0)(VME_rdout_data_size-1 downto 0);
    signal VME_rdout_tx_vld:            std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_tx_next:           std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_rx_data:           slv_array(VME_rdout_rx_number-1 downto 0)(VME_rdout_data_size-1 downto 0);
    signal VME_rdout_rx_vld:            std_logic_vector(VME_rdout_rx_number-1 downto 0);
    signal VME_rdout_rx_next:           std_logic_vector(VME_rdout_rx_number-1 downto 0);
 
    signal VME_rdout_ARB_tx_vld:        std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_ARB_tx_next:       std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_ARB_rx_vld:        std_logic;
    signal VME_rdout_ARB_rx_next:       std_logic;
    signal VME_rdout_ARB_sel:           std_logic_vector(get_vsize_addr(VME_rdout_tx_number)-1 downto 0);
    
BEGIN

    --===================--
    -- WRITE CONTROLLER  --
    --===================--
        
    -- pkg gate --
    pkg_gate_CMA_inst: entity work.package_gate_CMA
    GENERIC MAP
    (
        data_size       => pkg_gate_data_size
    )
    PORT MAP
    (
        clk             => clk,
        stop            => lock_RAM,
        inp_data        => pkg_data,
        inp_vld         => pkg_vld,
        inp_next        => pkg_next,
        out_data        => pkg_gate_tx_data,
        out_vld         => pkg_gate_tx_vld,
        out_next        => pkg_gate_tx_next
    );
    
    
    -- pkg gate CMI --
    pkg_gate_CMI: entity work.CMI
    GENERIC MAP
    (
        data_size       => pkg_gate_data_size,
        rx_number       => pkg_gate_rx_number
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => pkg_gate_tx_data,
        tx_vld          => pkg_gate_tx_vld,
        tx_next         => pkg_gate_tx_next,
        rx_data         => pkg_gate_rx_data,
        rx_vld          => pkg_gate_rx_vld,
        rx_next         => pkg_gate_rx_next
    );
    
    -- package split --
    pkg_split_CMA_inst: entity work.package_split_CMA
    GENERIC MAP
    (
        inp_data_size       => pkg_gate_data_size,
        body_data_size      => pkg_body_data_size,
        header_data_size    => pkg_header_data_size,
        header_size         => header_size
    )
    PORT MAP
    (
        clk                 => clk,
        inp_data            => pkg_gate_rx_data(0),
        inp_vld             => pkg_gate_rx_vld(0),
        inp_next            => pkg_gate_rx_next(0),
        body_data           => pkg_body_tx_data,
        body_vld            => pkg_body_tx_vld,
        body_next           => pkg_body_tx_next,
        header_data         => pkg_header_tx_data,
        header_vld          => pkg_header_tx_vld,
        header_next         => pkg_header_tx_next
    );
    
    -- pkg_header CMI --
    pkg_header_CMI: entity work.CMI
    GENERIC MAP
    (
        data_size       => pkg_header_data_size,
        rx_number       => pkg_header_rx_number
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => pkg_header_tx_data,
        tx_vld          => pkg_header_tx_vld,
        tx_next         => pkg_header_tx_next,
        rx_data         => pkg_header_rx_data,
        rx_vld          => pkg_header_rx_vld,
        rx_next         => pkg_header_rx_next
    );
    
    
    -- pkg_body CMI --
    pkg_body_CMI: entity work.CMI
    GENERIC MAP
    (
        data_size       => pkg_body_data_size,
        rx_number       => pkg_body_rx_number
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => pkg_body_tx_data,
        tx_vld          => pkg_body_tx_vld,
        tx_next         => pkg_body_tx_next,
        rx_data         => pkg_body_rx_data,
        rx_vld          => pkg_body_rx_vld,
        rx_next         => pkg_body_rx_next
    );
    
    
    -- Write Control --
    StorageWRCtrl: entity work.DAB64x_StorageWriteCtrl_CMA
    GENERIC MAP
    (
        header_data_size    => pkg_header_data_size,
        body_data_size      => pkg_body_data_size,
        out_data_size       => wrctrl_tx_data_size
    )
    PORT MAP
    (
        clk                 => clk,
        body_data           => pkg_body_rx_data(0),
        body_vld            => pkg_body_rx_vld(0),
        body_next           => pkg_body_rx_next(0),
        header_data         => pkg_header_rx_data(0),
        header_vld          => pkg_header_rx_vld(0),
        header_next         => pkg_header_rx_next(0),
        out_data            => wrctrl_tx_data,
        out_vld             => wrctrl_tx_vld,
        out_next            => wrctrl_tx_next
    );
    
    -- wrctrl T-CMI -- 
    wrctrl_CMI: entity work.T_CMI
    GENERIC MAP
    (
        rx_number       => wrctrl_rx_number,
        tx_data_size    => wrctrl_tx_data_size,
        rx_data_size    => wrctrl_rx_data_size
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => wrctrl_tx_data,
        tx_vld          => wrctrl_tx_vld,
        tx_next         => wrctrl_tx_next,
        rx_data         => wrctrl_rx_data,
        rx_vld          => wrctrl_rx_vld,
        rx_next         => wrctrl_rx_next,
        ADM_tx_data     => wrctrl_ADM_tx_data,
        ADM_rx_data     => wrctrl_ADM_rx_data,
        ADM_sel         => wrctrl_ADM_sel
    );
    
    -- ADM vme_wrreq --
    wrctrl_ADM: entity work.ID_ADM
    GENERIC MAP
    (
        rx_number       => wrctrl_rx_number,
        tx_data_size    => wrctrl_tx_data_size,
        rx_data_size    => wrctrl_rx_data_size
    )
    PORT MAP
    (
        ADM_tx_data     => wrctrl_ADM_tx_data,
        ADM_rx_data     => wrctrl_ADM_rx_data,
        ADM_sel         => wrctrl_ADM_sel
    );
    
    
    --================--
    -- EVO STRUCTURES --
    --================--
    
    -- Scoreboard --
    EVO_SB_CMA: entity work.scoreboard_CMA
    GENERIC MAP
    (
        inp_data_size       => wrctrl_rx_data_size,
        inp_addr_MSB        => EVO_SB_addr_MSB,
        inp_d_MSB           => EVO_SB_d_MSB,
        out_data_size       => EVO_SB_data_size,
        out_addr_size       => RAM_EVO_addr_size,
        out_d_size          => RAM_EVO_data_size
    )
    PORT MAP
    (
        clk                 => clk,
        pbl                 => SB_pbl,
        PARAM_SB_base_addr  => EVO_SB_base_addr,
        inp_data            => wrctrl_rx_data(0),
        inp_vld             => wrctrl_rx_vld(0),
        inp_next            => wrctrl_rx_next(0),
        out_data            => EVO_SB_tx_data,
        out_vld             => EVO_SB_tx_vld,
        out_next            => EVO_SB_tx_next
    );
    
    -- EVO SB CMI --
    EVO_SB_CMI: entity work.CMI
    GENERIC MAP
    (
        data_size       => EVO_SB_data_size,
        rx_number       => EVO_SB_rx_number
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => EVO_SB_tx_data,
        tx_vld          => EVO_SB_tx_vld,
        tx_next         => EVO_SB_tx_next,
        rx_data         => EVO_SB_rx_data,
        rx_vld          => EVO_SB_rx_vld,
        rx_next         => EVO_SB_rx_next
    );

    -- RAM Access Port B --
    EVO_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => EVO_SB_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => 0,
        wrreq_data_MSB      => EVO_SB_d_MSB,
        wrreq_addr_MSB      => EVO_SB_addr_MSB,
        RAM_addr_size       => RAM_EVO_addr_size,
        RAM_data_size       => RAM_EVO_data_size,
        RAM_byteena_size    => RAM_EVO_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => EVO_SB_rx_data(0),
        wrreq_vld       => EVO_SB_rx_vld(0),
        wrreq_next      => EVO_SB_rx_next(0),
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_EVO_portB_addr,
        RAM_data        => RAM_EVO_portB_data,
        RAM_byteena     => RAM_EVO_portB_byteena,
        RAM_wren        => RAM_EVO_portB_wren,
        RAM_q           => RAM_EVO_portB_q
    );
    
    -- EVO RAM --
    EVO_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_EVO_numwords,
        portA_data_size     => RAM_EVO_data_size,
        portA_addr_size     => RAM_EVO_addr_size,
        portA_byteena_size  => RAM_EVO_byteena_size,
        portB_numwords      => RAM_EVO_numwords,
        portB_data_size     => RAM_EVO_data_size,
        portB_addr_size     => RAM_EVO_addr_size,
        portB_byteena_size  => RAM_EVO_byteena_size,
        init_file           => RAM_EVO_init_file,
        ram_type            => RAM_EVO_type,
        read_during_write   => "DONT_CARE",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_EVO_portA_addr,
        portA_data      => RAM_EVO_portA_data,
        portA_byteena   => RAM_EVO_portA_byteena,
        portA_wren      => RAM_EVO_portA_wren,
        portA_q         => RAM_EVO_portA_q,
        portB_addr      => RAM_EVO_portB_addr,
        portB_data      => RAM_EVO_portB_data,
        portB_byteena   => RAM_EVO_portB_byteena,
        portB_wren      => RAM_EVO_portB_wren,
        portB_q         => RAM_EVO_portB_q
    );
    
    -- EVO RAM Access Port A --
    EVO_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_EVO_addr_size,
        RAM_data_size       => RAM_EVO_data_size,
        RAM_byteena_size    => RAM_EVO_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_rx_data(0),
        rdreq_vld       => VME_rdreq_rx_vld(0),
        rdreq_next      => VME_rdreq_rx_next(0),
        wrreq_data      => VME_wrreq_rx_data(0),
        wrreq_vld       => VME_wrreq_rx_vld(0),
        wrreq_next      => VME_wrreq_rx_next(0),
        rdout_data      => VME_rdout_tx_data(0),
        rdout_vld       => VME_rdout_tx_vld(0),
        rdout_next      => VME_rdout_tx_next(0),
        RAM_addr        => RAM_EVO_portA_addr,
        RAM_data        => RAM_EVO_portA_data,
        RAM_byteena     => RAM_EVO_portA_byteena,
        RAM_wren        => RAM_EVO_portA_wren,
        RAM_q           => RAM_EVO_portA_q
    );
    
    
    --================--
    -- CAP STRUCTURES --
    --================--
    
    -- Scoreboard --
    CAP_SB_CMA: entity work.scoreboard_CMA
    GENERIC MAP
    (
        inp_data_size       => wrctrl_rx_data_size,
        inp_addr_MSB        => CAP_SB_addr_MSB,
        inp_d_MSB           => CAP_SB_d_MSB,
        out_data_size       => CAP_SB_data_size,
        out_addr_size       => RAM_CAP_addr_size,
        out_d_size          => RAM_CAP_data_size
    )
    PORT MAP
    (
        clk                 => clk,
        pbl                 => SB_pbl,
        PARAM_SB_base_addr  => CAP_SB_base_addr,
        inp_data            => wrctrl_rx_data(1),
        inp_vld             => wrctrl_rx_vld(1),
        inp_next            => wrctrl_rx_next(1),
        out_data            => CAP_SB_tx_data,
        out_vld             => CAP_SB_tx_vld,
        out_next            => CAP_SB_tx_next
    );
    
    -- CAP SB CMI --
    CAP_SB_CMI: entity work.CMI
    GENERIC MAP
    (
        data_size       => CAP_SB_data_size,
        rx_number       => CAP_SB_rx_number
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => CAP_SB_tx_data,
        tx_vld          => CAP_SB_tx_vld,
        tx_next         => CAP_SB_tx_next,
        rx_data         => CAP_SB_rx_data,
        rx_vld          => CAP_SB_rx_vld,
        rx_next         => CAP_SB_rx_next
    );
    
    -- CAP RAM Access Port B --
    CAP_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => CAP_SB_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => 0,
        wrreq_data_MSB      => CAP_SB_d_MSB,
        wrreq_addr_MSB      => CAP_SB_addr_MSB,
        RAM_addr_size       => RAM_CAP_addr_size,
        RAM_data_size       => RAM_CAP_data_size,
        RAM_byteena_size    => RAM_CAP_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => CAP_SB_rx_data(0),
        wrreq_vld       => CAP_SB_rx_vld(0),
        wrreq_next      => CAP_SB_rx_next(0),
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_CAP_portB_addr,
        RAM_data        => RAM_CAP_portB_data,
        RAM_byteena     => RAM_CAP_portB_byteena,
        RAM_wren        => RAM_CAP_portB_wren,
        RAM_q           => RAM_CAP_portB_q
    );
    
    -- CAP RAM --
    CAP_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_CAP_numwords,
        portA_data_size     => RAM_CAP_data_size,
        portA_addr_size     => RAM_CAP_addr_size,
        portA_byteena_size  => RAM_CAP_byteena_size,
        portB_numwords      => RAM_CAP_numwords,
        portB_data_size     => RAM_CAP_data_size,
        portB_addr_size     => RAM_CAP_addr_size,
        portB_byteena_size  => RAM_CAP_byteena_size,
        init_file           => RAM_CAP_init_file,
        ram_type            => RAM_CAP_type,
        read_during_write   => "DONT_CARE",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_CAP_portA_addr,
        portA_data      => RAM_CAP_portA_data,
        portA_byteena   => RAM_CAP_portA_byteena,
        portA_wren      => RAM_CAP_portA_wren,
        portA_q         => RAM_CAP_portA_q,
        portB_addr      => RAM_CAP_portB_addr,
        portB_data      => RAM_CAP_portB_data,
        portB_byteena   => RAM_CAP_portB_byteena,
        portB_wren      => RAM_CAP_portB_wren,
        portB_q         => RAM_CAP_portB_q
    );
    
    -- CAP RAM Access Port A --
    CAP_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_CAP_addr_size,
        RAM_data_size       => RAM_CAP_data_size,
        RAM_byteena_size    => RAM_CAP_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_rx_data(1),
        rdreq_vld       => VME_rdreq_rx_vld(1),
        rdreq_next      => VME_rdreq_rx_next(1),
        wrreq_data      => VME_wrreq_rx_data(1),
        wrreq_vld       => VME_wrreq_rx_vld(1),
        wrreq_next      => VME_wrreq_rx_next(1),
        rdout_data      => VME_rdout_tx_data(1),
        rdout_vld       => VME_rdout_tx_vld(1),
        rdout_next      => VME_rdout_tx_next(1),
        RAM_addr        => RAM_CAP_portA_addr,
        RAM_data        => RAM_CAP_portA_data,
        RAM_byteena     => RAM_CAP_portA_byteena,
        RAM_wren        => RAM_CAP_portA_wren,
        RAM_q           => RAM_CAP_portA_q
    );
    
    
   
    --=================--
    -- LOSS STRUCTURES --
    --=================--
    
    -- Scoreboard --
     LOSS_SB_CMA: entity work.scoreboard_CMA
    GENERIC MAP
    (
        inp_data_size       => wrctrl_rx_data_size,
        inp_addr_MSB        => LOSS_SB_addr_MSB,
        inp_d_MSB           => LOSS_SB_d_MSB,
        out_data_size       => LOSS_SB_data_size,
        out_addr_size       => RAM_LOSS_addr_size,
        out_d_size          => RAM_LOSS_data_size
    )
    PORT MAP
    (
        clk                 => clk,
        pbl                 => SB_pbl,
        PARAM_SB_base_addr  => LOSS_SB_base_addr,
        inp_data            => wrctrl_rx_data(2),
        inp_vld             => wrctrl_rx_vld(2),
        inp_next            => wrctrl_rx_next(2),
        out_data            => LOSS_SB_tx_data,
        out_vld             => LOSS_SB_tx_vld,
        out_next            => LOSS_SB_tx_next
    );
    
    -- LOSS SB CMI --
    LOSS_SB_CMI: entity work.CMI
    GENERIC MAP
    (
        data_size       => LOSS_SB_data_size,
        rx_number       => LOSS_SB_rx_number
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => LOSS_SB_tx_data,
        tx_vld          => LOSS_SB_tx_vld,
        tx_next         => LOSS_SB_tx_next,
        rx_data         => LOSS_SB_rx_data,
        rx_vld          => LOSS_SB_rx_vld,
        rx_next         => LOSS_SB_rx_next
    );
    
    -- LOSS RAM Access Port B --
    LOSS_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => LOSS_SB_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => 0,
        wrreq_data_MSB      => LOSS_SB_d_MSB,
        wrreq_addr_MSB      => LOSS_SB_addr_MSB,
        RAM_addr_size       => RAM_LOSS_addr_size,
        RAM_data_size       => RAM_LOSS_data_size,
        RAM_byteena_size    => RAM_LOSS_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => LOSS_SB_rx_data(0),
        wrreq_vld       => LOSS_SB_rx_vld(0),
        wrreq_next      => LOSS_SB_rx_next(0),
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_LOSS_portB_addr,
        RAM_data        => RAM_LOSS_portB_data,
        RAM_byteena     => RAM_LOSS_portB_byteena,
        RAM_wren        => RAM_LOSS_portB_wren,
        RAM_q           => RAM_LOSS_portB_q
    );
    
    -- LOSS RAM --
    LOSS_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_LOSS_numwords,
        portA_data_size     => RAM_LOSS_data_size,
        portA_addr_size     => RAM_LOSS_addr_size,
        portA_byteena_size  => RAM_LOSS_byteena_size,
        portB_numwords      => RAM_LOSS_numwords,
        portB_data_size     => RAM_LOSS_data_size,
        portB_addr_size     => RAM_LOSS_addr_size,
        portB_byteena_size  => RAM_LOSS_byteena_size,
        init_file           => RAM_LOSS_init_file,
        ram_type            => RAM_LOSS_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_LOSS_portA_addr,
        portA_data      => RAM_LOSS_portA_data,
        portA_byteena   => RAM_LOSS_portA_byteena,
        portA_wren      => RAM_LOSS_portA_wren,
        portA_q         => RAM_LOSS_portA_q,
        portB_addr      => RAM_LOSS_portB_addr,
        portB_data      => RAM_LOSS_portB_data,
        portB_byteena   => RAM_LOSS_portB_byteena,
        portB_wren      => RAM_LOSS_portB_wren,
        portB_q         => RAM_LOSS_portB_q
    );
    
    -- LOSS RAM Access Port A --
    LOSS_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_LOSS_addr_size,
        RAM_data_size       => RAM_LOSS_data_size,
        RAM_byteena_size    => RAM_LOSS_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_rx_data(2),
        rdreq_vld       => VME_rdreq_rx_vld(2),
        rdreq_next      => VME_rdreq_rx_next(2),
        wrreq_data      => VME_wrreq_rx_data(2),
        wrreq_vld       => VME_wrreq_rx_vld(2),
        wrreq_next      => VME_wrreq_rx_next(2),
        rdout_data      => VME_rdout_tx_data(2),
        rdout_vld       => VME_rdout_tx_vld(2),
        rdout_next      => VME_rdout_tx_next(2),
        RAM_addr        => RAM_LOSS_portA_addr,
        RAM_data        => RAM_LOSS_portA_data,
        RAM_byteena     => RAM_LOSS_portA_byteena,
        RAM_wren        => RAM_LOSS_portA_wren,
        RAM_q           => RAM_LOSS_portA_q
    );
    
    
    --=================--
    -- DIAG STRUCTURES --
    --=================--
    
    -- Scoreboard --
    DIAG_SB_CMA: entity work.scoreboard_CMA
    GENERIC MAP
    (
        inp_data_size       => wrctrl_rx_data_size,
        inp_addr_MSB        => DIAG_SB_addr_MSB,
        inp_d_MSB           => DIAG_SB_d_MSB,
        out_data_size       => DIAG_SB_data_size,
        out_addr_size       => RAM_DIAG_addr_size,
        out_d_size          => RAM_DIAG_data_size
    )
    PORT MAP
    (
        clk                 => clk,
        pbl                 => SB_pbl,
        PARAM_SB_base_addr  => DIAG_SB_base_addr,
        inp_data            => wrctrl_rx_data(3),
        inp_vld             => wrctrl_rx_vld(3),
        inp_next            => wrctrl_rx_next(3),
        out_data            => DIAG_SB_tx_data,
        out_vld             => DIAG_SB_tx_vld,
        out_next            => DIAG_SB_tx_next
    );
    
    -- LOSS SB CMI --
    DIAG_SB_CMI: entity work.CMI
    GENERIC MAP
    (
        data_size       => DIAG_SB_data_size,
        rx_number       => DIAG_SB_rx_number
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => DIAG_SB_tx_data,
        tx_vld          => DIAG_SB_tx_vld,
        tx_next         => DIAG_SB_tx_next,
        rx_data         => DIAG_SB_rx_data,
        rx_vld          => DIAG_SB_rx_vld,
        rx_next         => DIAG_SB_rx_next
    );

    -- DIAG RAM Access Port B --
    DIAG_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => DIAG_SB_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => 0,
        wrreq_data_MSB      => DIAG_SB_d_MSB,
        wrreq_addr_MSB      => DIAG_SB_addr_MSB,
        RAM_addr_size       => RAM_DIAG_addr_size,
        RAM_data_size       => RAM_DIAG_data_size,
        RAM_byteena_size    => RAM_DIAG_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => DIAG_SB_rx_data(0),
        wrreq_vld       => DIAG_SB_rx_vld(0),
        wrreq_next      => DIAG_SB_rx_next(0),
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_DIAG_portB_addr,
        RAM_data        => RAM_DIAG_portB_data,
        RAM_byteena     => RAM_DIAG_portB_byteena,
        RAM_wren        => RAM_DIAG_portB_wren,
        RAM_q           => RAM_DIAG_portB_q
    );
    
    -- DIAG RAM --
    DIAG_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_DIAG_numwords,
        portA_data_size     => RAM_DIAG_data_size,
        portA_addr_size     => RAM_DIAG_addr_size,
        portA_byteena_size  => RAM_DIAG_byteena_size,
        portB_numwords      => RAM_DIAG_numwords,
        portB_data_size     => RAM_DIAG_data_size,
        portB_addr_size     => RAM_DIAG_addr_size,
        portB_byteena_size  => RAM_DIAG_byteena_size,
        init_file           => RAM_DIAG_init_file,
        ram_type            => RAM_DIAG_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_DIAG_portA_addr,
        portA_data      => RAM_DIAG_portA_data,
        portA_byteena   => RAM_DIAG_portA_byteena,
        portA_wren      => RAM_DIAG_portA_wren,
        portA_q         => RAM_DIAG_portA_q,
        portB_addr      => RAM_DIAG_portB_addr,
        portB_data      => RAM_DIAG_portB_data,
        portB_byteena   => RAM_DIAG_portB_byteena,
        portB_wren      => RAM_DIAG_portB_wren,
        portB_q         => RAM_DIAG_portB_q
    );
    
    -- DIAG RAM Access Port A --
    DIAG_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_DIAG_addr_size,
        RAM_data_size       => RAM_DIAG_data_size,
        RAM_byteena_size    => RAM_DIAG_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_rx_data(3),
        rdreq_vld       => VME_rdreq_rx_vld(3),
        rdreq_next      => VME_rdreq_rx_next(3),
        wrreq_data      => VME_wrreq_rx_data(3),
        wrreq_vld       => VME_wrreq_rx_vld(3),
        wrreq_next      => VME_wrreq_rx_next(3),
        rdout_data      => VME_rdout_tx_data(3),
        rdout_vld       => VME_rdout_tx_vld(3),
        rdout_next      => VME_rdout_tx_next(3),
        RAM_addr        => RAM_DIAG_portA_addr,
        RAM_data        => RAM_DIAG_portA_data,
        RAM_byteena     => RAM_DIAG_portA_byteena,
        RAM_wren        => RAM_DIAG_portA_wren,
        RAM_q           => RAM_DIAG_portA_q
    );

    
    --==================--
    -- PARAM STRUCTURES --
    --==================--
    
    -- PARAM RAM --
    PARAM_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_PARAM_numwords,
        portA_data_size     => RAM_PARAM_data_size,
        portA_addr_size     => RAM_PARAM_addr_size,
        portA_byteena_size  => RAM_PARAM_byteena_size,
        portB_numwords      => RAM_PARAM_numwords,
        portB_data_size     => RAM_PARAM_data_size,
        portB_addr_size     => RAM_PARAM_addr_size,
        portB_byteena_size  => RAM_PARAM_byteena_size,
        init_file           => RAM_PARAM_init_file,
        ram_type            => RAM_PARAM_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_PARAM_portA_addr,
        portA_data      => RAM_PARAM_portA_data,
        portA_byteena   => RAM_PARAM_portA_byteena,
        portA_wren      => RAM_PARAM_portA_wren,
        portA_q         => RAM_PARAM_portA_q,
        portB_addr      => (others => '0'),
        portB_data      => (others => '0'),
        portB_byteena   => (others => '0'),
        portB_wren      => '0',
        portB_q         => open
    );
    
    -- PARAM RAM Access Port A --
    PARAM_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_PARAM_addr_size,
        RAM_data_size       => RAM_PARAM_data_size,
        RAM_byteena_size    => RAM_PARAM_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_rx_data(4),
        rdreq_vld       => VME_rdreq_rx_vld(4),
        rdreq_next      => VME_rdreq_rx_next(4),
        wrreq_data      => VME_wrreq_rx_data(4),
        wrreq_vld       => VME_wrreq_rx_vld(4),
        wrreq_next      => VME_wrreq_rx_next(4),
        rdout_data      => VME_rdout_tx_data(4),
        rdout_vld       => VME_rdout_tx_vld(4),
        rdout_next      => VME_rdout_tx_next(4),
        RAM_addr        => RAM_PARAM_portA_addr,
        RAM_data        => RAM_PARAM_portA_data,
        RAM_byteena     => RAM_PARAM_portA_byteena,
        RAM_wren        => RAM_PARAM_portA_wren,
        RAM_q           => RAM_PARAM_portA_q
    );
    
        
    --=================--
    -- PROG STRUCTURES --
    --=================--
    
    -- PROG RAM --
    PROG_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_PROG_numwords,
        portA_data_size     => RAM_PROG_data_size,
        portA_addr_size     => RAM_PROG_addr_size,
        portA_byteena_size  => RAM_PROG_byteena_size,
        portB_numwords      => RAM_PROG_numwords,
        portB_data_size     => RAM_PROG_data_size,
        portB_addr_size     => RAM_PROG_addr_size,
        portB_byteena_size  => RAM_PROG_byteena_size,
        init_file           => RAM_PROG_init_file,
        ram_type            => RAM_PROG_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_PROG_portA_addr,
        portA_data      => RAM_PROG_portA_data,
        portA_byteena   => RAM_PROG_portA_byteena,
        portA_wren      => RAM_PROG_portA_wren,
        portA_q         => RAM_PROG_portA_q,
        portB_addr      => (others => '0'),
        portB_data      => (others => '0'),
        portB_byteena   => (others => '0'),
        portB_wren      => '0',
        portB_q         => open
    );
    
    -- PROG RAM Access Port B --
    PROG_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_PROG_addr_size,
        RAM_data_size       => RAM_PROG_data_size,
        RAM_byteena_size    => RAM_PROG_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_rx_data(5),
        rdreq_vld       => VME_rdreq_rx_vld(5),
        rdreq_next      => VME_rdreq_rx_next(5),
        wrreq_data      => VME_wrreq_rx_data(5),
        wrreq_vld       => VME_wrreq_rx_vld(5),
        wrreq_next      => VME_wrreq_rx_next(5),
        rdout_data      => VME_rdout_tx_data(5),
        rdout_vld       => VME_rdout_tx_vld(5),
        rdout_next      => VME_rdout_tx_next(5),
        RAM_addr        => RAM_PROG_portA_addr,
        RAM_data        => RAM_PROG_portA_data,
        RAM_byteena     => RAM_PROG_portA_byteena,
        RAM_wren        => RAM_PROG_portA_wren,
        RAM_q           => RAM_PROG_portA_q
    );

    
    --=================--
    -- CTRL STRUCTURES --
    --=================--
    
    -- CTRL RAM --
    CTRL_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_CTRL_numwords,
        portA_data_size     => RAM_CTRL_data_size,
        portA_addr_size     => RAM_CTRL_addr_size,
        portA_byteena_size  => RAM_CTRL_byteena_size,
        portB_numwords      => RAM_CTRL_numwords,
        portB_data_size     => RAM_CTRL_data_size,
        portB_addr_size     => RAM_CTRL_addr_size,
        portB_byteena_size  => RAM_CTRL_byteena_size,
        init_file           => RAM_CTRL_init_file,
        ram_type            => RAM_CTRL_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk,
        portA_addr      => RAM_CTRL_portA_addr,
        portA_data      => RAM_CTRL_portA_data,
        portA_byteena   => RAM_CTRL_portA_byteena,
        portA_wren      => RAM_CTRL_portA_wren,
        portA_q         => RAM_CTRL_portA_q,
        portB_addr      => (others => '0'),
        portB_data      => (others => '0'),
        portB_byteena   => (others => '0'),
        portB_wren      => '0',
        portB_q         => open
    );
    
    -- CTRL RAM Access Port A --
    CTRL_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_CTRL_addr_size,
        RAM_data_size       => RAM_CTRL_data_size,
        RAM_byteena_size    => RAM_CTRL_byteena_size

    )
    PORT MAP
    (
        clk             => clk,
        rdreq_data      => VME_rdreq_rx_data(6),
        rdreq_vld       => VME_rdreq_rx_vld(6),
        rdreq_next      => VME_rdreq_rx_next(6),
        wrreq_data      => VME_wrreq_rx_data(6),
        wrreq_vld       => VME_wrreq_rx_vld(6),
        wrreq_next      => VME_wrreq_rx_next(6),
        rdout_data      => VME_rdout_tx_data(6),
        rdout_vld       => VME_rdout_tx_vld(6),
        rdout_next      => VME_rdout_tx_next(6),
        RAM_addr        => RAM_CTRL_portA_addr,
        RAM_data        => RAM_CTRL_portA_data,
        RAM_byteena     => RAM_CTRL_portA_byteena,
        RAM_wren        => RAM_CTRL_portA_wren,
        RAM_q           => RAM_CTRL_portA_q
    );
    
    
    --================--
    -- VME Connection --
    --================--
    
    -- Port Connections
    VME_rdreq_tx_data   <= VME_rdreq_data;
    VME_rdreq_tx_vld    <= VME_rdreq_vld;
    VME_rdreq_next      <= VME_rdreq_tx_next;
    
    VME_wrreq_tx_data   <= VME_wrreq_data;
    VME_wrreq_tx_vld    <= VME_wrreq_vld;
    VME_wrreq_next      <= VME_wrreq_tx_next;
    
    VME_rdout_data      <= VME_rdout_rx_data(0);
    VME_rdout_vld       <= VME_rdout_rx_vld(0);
    VME_rdout_rx_next(0)<= VME_rdout_next;
    
    
    -- VME_wrreq --
    wrreq_CMI: entity work.T_CMI
    GENERIC MAP
    (
        rx_number       => VME_wrreq_rx_number,
        tx_data_size    => VME_wrreq_data_size,
        rx_data_size    => VME_wrreq_data_size
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => VME_wrreq_tx_data,
        tx_vld          => VME_wrreq_tx_vld,
        tx_next         => VME_wrreq_tx_next,
        rx_data         => VME_wrreq_rx_data,
        rx_vld          => VME_wrreq_rx_vld,
        rx_next         => VME_wrreq_rx_next,
        ADM_tx_data     => VME_wrreq_ADM_tx_data,
        ADM_rx_data     => VME_wrreq_ADM_rx_data,
        ADM_sel         => VME_wrreq_ADM_sel
    );
    
    -- ADM VME_wrreq --
    wrreq_ADM: entity work.DAB64x_VME_ADM
    GENERIC MAP
    (
        rx_number       => VME_wrreq_rx_number,
        data_size       => VME_wrreq_data_size,
        addr_size       => VME_wrreq_addr_size
    )
    PORT MAP
    (
        ADM_tx_data     => VME_wrreq_ADM_tx_data,
        ADM_rx_data     => VME_wrreq_ADM_rx_data,
        ADM_sel         => VME_wrreq_ADM_sel
    );
    
    -- VME_rdreq --
    rdreq_CMI: entity work.T_CMI
    GENERIC MAP
    (
        rx_number       => VME_rdreq_rx_number,
        tx_data_size    => VME_rdreq_data_size,
        rx_data_size    => VME_rdreq_data_size
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => VME_rdreq_tx_data,
        tx_vld          => VME_rdreq_tx_vld,
        tx_next         => VME_rdreq_tx_next,
        rx_data         => VME_rdreq_rx_data,
        rx_vld          => VME_rdreq_rx_vld,
        rx_next         => VME_rdreq_rx_next,
        ADM_tx_data     => VME_rdreq_ADM_tx_data,
        ADM_rx_data     => VME_rdreq_ADM_rx_data,
        ADM_sel         => VME_rdreq_ADM_sel
    );
    
    -- ADM VME_rdreq --
    rdreq_ADM: entity work.DAB64x_VME_ADM
    GENERIC MAP
    (
        rx_number       => VME_rdreq_rx_number,
        data_size       => VME_rdreq_data_size, 
        addr_size       => VME_rdreq_addr_size
    )
    PORT MAP
    (
        ADM_tx_data     => VME_rdreq_ADM_tx_data,
        ADM_rx_data     => VME_rdreq_ADM_rx_data,
        ADM_sel         => VME_rdreq_ADM_sel
    );
    
    -- VME_rdout --
    rdout_CMI: entity work.MM_CMI
    GENERIC MAP
    (
        tx_number       => VME_rdout_tx_number,
        rx_number       => VME_rdout_rx_number,
        data_size       => VME_rdout_data_size
    )
    PORT MAP
    (
        clk             => clk,
        tx_data         => VME_rdout_tx_data,
        tx_vld          => VME_rdout_tx_vld,
        tx_next         => VME_rdout_tx_next,
        rx_data         => VME_rdout_rx_data,
        rx_vld          => VME_rdout_rx_vld,
        rx_next         => VME_rdout_rx_next,
        ARB_tx_vld      => VME_rdout_ARB_tx_vld,
        ARB_tx_next     => VME_rdout_ARB_tx_next,
        ARB_rx_vld      => VME_rdout_ARB_rx_vld,
        ARB_rx_next     => VME_rdout_ARB_rx_next,
        ARB_sel         => VME_rdout_ARB_sel
    );
    
    -- ARB VME_rdout --
    rdout_ARB: entity work.DAB64x_VME_ARB
    GENERIC MAP
    (
        tx_number       => VME_rdout_tx_number
    )
    PORT MAP
    (
        clk             => clk,
        ARB_tx_vld      => VME_rdout_ARB_tx_vld,
        ARB_tx_next     => VME_rdout_ARB_tx_next,
        ARB_rx_vld      => VME_rdout_ARB_rx_vld,
        ARB_rx_next     => VME_rdout_ARB_rx_next,
        ARB_sel         => VME_rdout_ARB_sel
    );
    

end architecture struct;
