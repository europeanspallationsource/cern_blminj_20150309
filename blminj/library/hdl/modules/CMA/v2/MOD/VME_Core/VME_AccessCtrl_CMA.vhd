------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        20/11/2012
Module Name:    VME_AccessCtrl_CMA

-----------------
Short Description
-----------------

    This module processes the incoming access requests from the VME Interface and sends appropriate read/write requests 
    to the connected storage units. The output addresses 64-bit words. As the original address of the VME bus is a Byte-Address,
    the address coming out of this module is shifted by 3 to the right and indicates with a byte_wr_en, which bytes are actually valid
    data.

------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    none

--------------
Implementation
--------------

    The main feature is a byte_pos_cnt, which is used as an input for the byte_we circuit and the address offset counter.
    This byte_pos_cnt determines which bytes of the 64-bit vector should be written on and detects a switch to the next long word (next_lw)
    for the address offset.
    The increase of the byte_pos_cnt is given by the data size (dsize) of the VME transfer and the start position is given by the lower 
    bits of the access address (acc_data(2:0)).
 
-----------
Limitations
-----------
    
     CS/CSR is not implemented
    
----------------
Missing Features
----------------

    CS/CSR 

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity VME_AccessCtrl_CMA is
port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Inputs
    acc_data:       in  std_logic_vector(68 downto 0);
    acc_vld:        in  std_logic;
    acc_next:       out std_logic;
    wrfifo_data:    in  std_logic_vector(63 downto 0);
    wrfifo_vld:     in  std_logic;
    wrfifo_next:    out std_logic;
    -- CMI Outputs
    rdreq_data:     out std_logic_vector(60 downto 0);
    rdreq_vld:      out std_logic;
    rdreq_next:     in  std_logic;
    wrreq_data:     out std_logic_vector(132 downto 0);
    wrreq_vld:      out std_logic;
    wrreq_next:     in  std_logic;
    -- Commands
    VME_ACC:        in  std_logic
);

end entity VME_AccessCtrl_CMA;


architecture struct of VME_AccessCtrl_CMA is
    
    --======--
    -- FSMS --
    --======--
    
    type state_t is (idle, decode, SCT, BLT);
    signal present_state, next_state: state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Byte Position
    signal byte_pos_cnt:    UNSIGNED(3 downto 0) := (others => '0');
    signal byte_pos_inc:    std_logic; 
    signal byte_pos:        UNSIGNED(2 downto 0);
        
    -- Offset Counter
    signal offset_cnt:      UNSIGNED(9 downto 0) := (others => '0');
    signal offset_rst:      std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    -- stored access vector
    signal WRn:             std_logic;
    signal dsize:           std_logic_vector(1 downto 0);
    signal mode:            std_logic;
    signal csr:             std_logic;
    signal base_addr:       UNSIGNED(60 downto 0);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- Address Adder
    signal addr_offset:     UNSIGNED(9 downto 0);
    signal addr_sum:        UNSIGNED(60 downto 0);

    -- Register Enable
    signal new_acc_set:     std_logic;
    
    -- Write Enable
    signal byte_we:         std_logic_vector(7 downto 0);
    
    -- Next Longword
    signal last_bytegrp:    std_logic := '0';
    signal current_bytegrp: std_logic;
    signal next_lw:         std_logic;
    
    -- Byte Position
    signal byte_pos_start:  UNSIGNED(2 downto 0);
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal wrreq_vld_int:   std_logic;
    signal rdreq_vld_int:   std_logic;
    
BEGIN
    
    --===============--
    -- ACC REGISTERS --
    --===============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if new_acc_set = '1' then
                WRn         <= acc_data(68);
                dsize       <= acc_data(67 downto 66);
                mode        <= acc_data(65);
                csr         <= acc_data(64);
                base_addr   <= UNSIGNED(acc_data(63 downto 3));
            end if;
            
        end if;
    end process;

    --==========--
    -- BYTE POS --
    --==========--
    
    -- Start Position
    byte_pos_start <= UNSIGNED(acc_data(2 downto 0));
    
    process(clk) is
    begin
        if rising_edge(clk) then
            -- DEFAULTS
            last_bytegrp <= byte_pos_cnt(3); -- buffer last "bytegrp-tag"
            
            if new_acc_set = '1' then
                byte_pos_cnt    <= '0' & byte_pos_start;
                last_bytegrp    <= '0';
            elsif byte_pos_inc = '1' then 
                case dsize is
                    when "00"   => byte_pos_cnt <= byte_pos_cnt + 1;
                    when "01"   => byte_pos_cnt <= byte_pos_cnt + 2;
                    when "10"   => byte_pos_cnt <= byte_pos_cnt + 4;
                    when "11"   => byte_pos_cnt <= byte_pos_cnt + 8;
                    when others => byte_pos_cnt <= byte_pos_cnt;
                end case;
            end if;
                  
        end if;
    end process;
    
    -- actual byte pos (lower 3 bits)
    byte_pos <= byte_pos_cnt(2 downto 0);
    
    -- check for bytegroup switch
    current_bytegrp <= STD_LOGIC(byte_pos_cnt(3));
    next_lw         <= last_bytegrp XOR current_bytegrp;
    
    --===================--
    -- BYTE WRITE ENABLE --
    --===================--

    process(all) is
    begin
        -- DEFAULT
        byte_we <= "00000000";
        
        if dsize = "00" then
            case byte_pos is
                when "000" => byte_we(7) <= '1';
                when "001" => byte_we(6) <= '1';
                when "010" => byte_we(5) <= '1';
                when "011" => byte_we(4) <= '1';
                when "100" => byte_we(3) <= '1';
                when "101" => byte_we(2) <= '1';
                when "110" => byte_we(1) <= '1';
                when "111" => byte_we(0) <= '1';
                when others => byte_we <= "00000000";
            end case;   
        elsif dsize = "01" then
             case byte_pos is
                when "000" => byte_we(7 downto 6) <= "11";
                when "010" => byte_we(5 downto 4) <= "11";
                when "100" => byte_we(3 downto 2) <= "11";
                when "110" => byte_we(1 downto 0) <= "11";
                when others => byte_we <= "00000000";
            end case; 
        elsif dsize = "10" then
            case byte_pos is
                when "000" => byte_we(7 downto 4) <= "1111";
                when "100" => byte_we(3 downto 0) <= "1111";
                when others => byte_we <= "00000000";
            end case; 
        else
            byte_we <= "11111111";
        end if;
        
    end process;
     
    --================--
    -- ADDRESS OFFSET --
    --================--
    
    -- set mapper offset
    addr_offset <=  offset_cnt + 1 when next_lw = '1' else
                    offset_cnt;
    
    -- Offset Counter
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if offset_rst = '1' then
                offset_cnt <= (others => '0');
            elsif next_lw = '1' then
                offset_cnt <= addr_offset;
            end if;
                  
        end if;
    end process;
    
    -- Address Adder
    addr_sum <= base_addr + addr_offset;
    
    --=============--
    -- CMI OUTREGS --
    --=============--
        
    -- read request
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if rdreq_next = '1' then
                rdreq_data  <= STD_LOGIC_VECTOR(addr_sum);
                rdreq_vld   <= rdreq_vld_int;
            end if;
                  
        end if;
    end process;
    
    -- write request
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if wrreq_next = '1' then
                wrreq_data  <= STD_LOGIC_VECTOR(addr_sum) & byte_we & wrfifo_data;
                wrreq_vld   <= wrreq_vld_int;
            end if;
                  
        end if;
    end process;

    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        wrfifo_next     <= NOT wrfifo_vld;
        acc_next        <= '1';
        wrreq_vld_int   <= '0';
        rdreq_vld_int   <= '0';
        -- Enables
        new_acc_set     <= '0';
        byte_pos_inc    <= '0';
        offset_rst      <= '0';
        
        case present_state is
            
            --===============--
            when idle =>
            --===============--
                offset_rst <= '1';
                if acc_vld = '1' then
                    new_acc_set <= '1';
                    next_state  <= decode;
                else
                    next_state  <= idle;
                end if;
            
            --===============--
            when decode =>
            --===============--
            
                acc_next <= '0';
                if mode = '0' then
                    next_state <= SCT;
                else
                    next_state <= BLT;
                end if;
            
            --===============--
            when SCT =>
            --===============--
            
                acc_next    <= '0';
                next_state  <= SCT;
                
                if WRn = '1' then
                    if rdreq_next = '1' then
                        rdreq_vld_int   <= '1';
                        next_state      <= idle;
                    end if;
                else
                    if wrreq_next = '1' AND wrfifo_vld = '1' then
                        wrreq_vld_int   <= '1';
                        wrfifo_next     <= '1';
                        next_state      <= idle;
                    end if;
                end if;
            
            --===============--
            when BLT =>
            --===============--
            
                acc_next    <= '0';
                
                if VME_ACC = '1' then
                    next_state <= BLT;
                    if WRn = '1' then
                        if rdreq_next = '1' then
                            rdreq_vld_int   <= '1';
                            byte_pos_inc    <= '1';
                        end if;
                    else
                        if wrreq_next = '1' AND wrfifo_vld = '1' then
                            wrreq_vld_int   <= '1';
                            wrfifo_next     <= '1';
                            byte_pos_inc    <= '1';
                        end if;
                    end if;
                else
                    next_state <= idle;
                end if;
                
                
            --===============--
            when others =>
            --===============--  
            
                next_state <= idle;
            
        end case;
    end process;
    
end architecture struct;
