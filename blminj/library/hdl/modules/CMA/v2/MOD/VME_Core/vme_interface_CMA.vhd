------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2013
Module Name:    VME_Interface_CMA


-----------------
Short Description
-----------------

    This module implements most parts of the VME Interface Specifications. It is setup for an internal data size of 64 bit and an
    internal address size of 64 bit.

------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    
    Tclk_ns             := clock in ns
    
    sync_stages         := number of syncronisation stages for the VME strobes (DS, AS)
    dtack_fb_delay      := calculated minimum number of delay CCs until DTACK can be triggered (depending on the internal clk frequency)
    dtack_fb_min_delay  := actual DTACK latency (including switching of states, signal synchronisation steps)
    dtack_resc_delay    := maximum time for DTACK to rescind (check specs, depending on the internal frequency)

--------------
Implementation
--------------

 
-----------
Limitations
-----------
    
    The core should be clocked by a clock with a minimum clock frequency of 40 MHz (T = 25ns).
    
----------------
Missing Features
----------------

    CS/CSR mode and slave selection is not implemented 
*/
------------------------------

library ieee;
    use ieee.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

entity VME_Interface_CMA is
generic
(
    Tclk_ns:            real    := 8.0
);
port
(
    clk:                in  std_logic;
    -- VME Inputs
    VME_GA:             in  std_logic_vector(4 downto 0);
    VME_GAP:            in  std_logic;
    VME_WRn:            in  std_logic;
    VME_AM:             in  std_logic_vector(5 downto 0);
    VME_ASn:            in  std_logic;
    VME_DS0n:           in  std_logic;
    VME_DS1n:           in  std_logic;
    VME_IACKn:          in  std_logic;
    VME_IACKINn:        in  std_logic;
    VME_SYSCLK:         in  std_logic;
    VME_SYSRESETn:      in  std_logic;
    -- VME IOs
    VME_LWORDn_OUT:     out std_logic;
    VME_DATA_OUT:       out std_logic_vector(31 downto 0);
    VME_ADDR_OUT:       out std_logic_vector(31 downto 1);
    VME_LWORDn_IN:      in  std_logic;
    VME_DATA_IN:        in  std_logic_vector(31 downto 0);
    VME_ADDR_IN:        in  std_logic_vector(31 downto 1);
    -- VME Outputs
    VME_DTACKn:         out std_logic;
    VME_BERRn:          out std_logic;
    VME_RETRYn:         out std_logic;
    VME_IACKOUTn:       out std_logic;
    VME_IRQn:           out std_logic_vector(7 downto 1);
    VME_SYSFAILn:       out std_logic; 
    -- Internal SigNALS
    VME_BUS_BUSY:       in  std_logic;
    VME_ACC:            out std_logic;
    -- Board Buffer Controls
    VME_DATA_BUF_OEn:   out std_logic;
    VME_ADDR_BUF_OEn:   out std_logic;
    VME_DTACK_BUF_OEn:  out std_logic;
    VME_RETRY_BUF_OEn:  out std_logic;
    VME_BERR_BUF_OEn:   out std_logic;
    VME_DATA_BUF_DIR:   out std_logic;
    VME_ADDR_BUF_DIR:   out std_logic;
    -- CMI Interfaces
    write_data:         out std_logic_vector(63 downto 0);
    write_vld:          out std_logic;
    write_next:         in  std_logic;
   
    acc_data:           out std_logic_vector(68 downto 0);
    acc_vld:            out std_logic;
    acc_next:           in  std_logic;
    
    read_data:          in  std_logic_vector(63 downto 0);
    read_vld:           in  std_logic;
    read_next:          out std_logic
);
    
end entity VME_Interface_CMA;



architecture struct of VME_Interface_CMA is

    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant sync_stages:           integer := 2;
    
    -- DTACK Latency
    constant dtack_fb_delay:        integer := INTEGER(CEIL(30.0/Tclk_ns)); -- min 30 ns dtack latency
    constant dtack_fb_min_delay:    integer := floor_at_zero(dtack_fb_delay - sync_stages - 2); -- 2 additional CCs = 1 cycle state change, 1 cycle forcelat state
    
    -- Rescinding DTACK
    constant dtack_resc_delay:      integer := INTEGER(FLOOR(25.0/Tclk_ns)); -- max 30 ns dtack latency (better 25 ns)

    --=====--
    -- FSM --
    --=====--
    
    type state is (IDLE, GAPerr, CHK_SELECT, NO_SELECT,
                   MP_DS_ADDR_CYCLE_START, MP_FWD_ACC, MP_ADDR_DT_FORCELAT, MP_DS_ADDR_CYCLE_END, MP_DS_DATA_CYCLE_START,
                   nMP_DS_DATA_FIRST_CYCLE_START, nMP_FWD_ACC, nMP_DS_DATA_BLT_CYCLE_START,
                   RD_DT_FORCELAT, RD_TRANSFER, RD_DS_CYCLE_END,
                   WR_TRANSFER, WR_DT_FORCELAT, WR_DS_CYCLE_END,
                   RESC_DTACK, ADDR_PIPE, CLNUP_READ);
    
    signal present_state, next_state: state := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- DTACK delay counter
    signal dt_cnt:          UNSIGNED(3 downto 0) := (others => '0');
    signal dt_rst:          std_logic;
    signal dt_inc:          std_logic;
    -- DTACK rescinding counter
    signal resc_cnt:        UNSIGNED(3 downto 0) := (others => '0');
    signal resc_rst:        std_logic;
    signal resc_inc:        std_logic;
    
    -- Byte Position
    signal byte_pos_cnt:    UNSIGNED(2 downto 0) := (others => '0');
    signal byte_pos_inc:    std_logic; 
    signal byte_pos_set:    std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    -- Synced Triggers
    signal ASn:             std_logic;
    signal DS0n:            std_logic;
    signal DS1n:            std_logic;
    
    -- AS triggered 
    signal AM:              std_logic_vector(5 downto 0) := 6X"10";
    signal ADDR:            std_logic_vector(63 downto 1) := (others => '0');
    signal LWORDn:          std_logic := '0';
    
    -- DS triggered
    signal DATA:            std_logic_vector(63 downto 0) := (others => '0');
    signal WRn:             std_logic := '0';

    --===============--
    -- SIGNAL TYPING --
    --===============--
    
    type a_mode  is (A64, A40, A32, A24, A16, undef);
    type tr_mode is (MBLT, BLT, SCT, undef);
    type tr_type is (MP, nMP);
    type dt_mode is (D64, D32_BLT, D32, D16_BLT, D16, D08_ODD_BLT, D08_ODD, D08_EVEN_BLT, D08_EVEN, undef);
    
    signal ADDR_MODE:       a_mode;
    signal TRANSFER_MODE:   tr_mode;
    signal TRANSFER_TYPE:   tr_type; 
    signal DATA_MODE:       dt_mode;
    signal CSR_MODE:        std_logic;
    signal D08_odd_acc:     std_logic;
    signal D08_even_acc:    std_logic;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- Geographical Addressing
    signal GAn:                 std_logic_vector(4 downto 0);
    signal GAP_error:           std_logic;
    signal SLAVE_sel:           std_logic;
    -- Enable Signals
    signal AS_en:               std_logic;
    signal DS_en:               std_logic;
    -- Internal 3-state OEs/DIR
    signal VME_ADDR_DIR:        std_logic;
    signal VME_DATA_DIR:        std_logic;
    signal VME_INOUT_DIR:       std_logic;
    signal VME_ADDR_OE:         std_logic;
    signal VME_DATA_OE:         std_logic;
    signal VME_BERR_OE:         std_logic;
    signal VME_DTACK_OE:        std_logic;
    signal VME_RETRY_OE:        std_logic;
    -- 3-state bus signals
    signal DTACKn:              std_logic;
    signal RETRYn:              std_logic;
    signal BERRn:               std_logic;
    -- Access Details
    signal acc_WRn:             std_logic;
    signal acc_dsize:           std_logic_vector(1 downto 0);
    signal acc_mode:            std_logic;
    signal acc_csr:             std_logic;
    signal acc_addr_upper:      std_logic_vector(63 downto 3);
    signal acc_addr:            std_logic_vector(63 downto 0);
    -- Data
    signal WRDATA:              std_logic_vector(63 downto 0);
    signal RDDATA:              std_logic_vector(63 downto 0);
    
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    signal write_vld_int:       std_logic;
    signal acc_vld_int:         std_logic;
    
begin
    
    --=================--
    -- SYNCHRONISATION --
    --=================--
   
    sync_AS : entity work.sync
    generic map
    (
        data_width  => 1,
        stages      => sync_stages,
        init_value  => '1'
    )
    port map
    (
        dest_clk                => clk,
        async_data              => vectorize(VME_ASn),
        scalarize(sync_data)    => ASn
    );

    sync_DS0 : entity work.sync
    generic map
    (
        data_width  => 1,
        stages      => sync_stages,
        init_value  => '1'
    )
    port map
    (
        dest_clk                => clk,
        async_data              => vectorize(VME_DS0n),
        scalarize(sync_data)    => DS0n
    );

    sync_DS1 : entity work.sync
    generic map
    (
        data_width  => 1,
        stages      => sync_stages,
        init_value  => '1'
    )
    port map
    (
        dest_clk                => clk,
        async_data              => vectorize(VME_DS1n),
        scalarize(sync_data)    => DS1n
    );

    --============--
    -- MISC PORTS --
    --============--
        
    -- AS/DS trigger 
    process(clk) is
    begin
        if rising_edge(clk) then
            if AS_en = '1' then
                ADDR    <= VME_DATA_IN & VME_ADDR_IN;
                AM      <= VME_AM;
                LWORDn  <= VME_LWORDn_IN;
            end if;
            if DS_en = '1' then
                DATA    <= VME_ADDR_IN & VME_LWORDn_IN & VME_DATA_IN;
                WRn     <= VME_WRn;
            end if;
        end if;
    end process;
    
    
    --================--
    -- MAIN VME PORTS --
    --================--
    
    -- VME IO Direction
    VME_ADDR_DIR    <=  '1' when VME_INOUT_DIR = '1' AND DATA_MODE = D64 else    
                        '0';
    
    VME_DATA_DIR    <= VME_INOUT_DIR;
        
    -- VME IO Output
    VME_ADDR_OUT    <= RDDATA(63 downto 33); 
    VME_LWORDn_OUT  <= RDDATA(32);           
    VME_DATA_OUT    <= RDDATA(31 downto 0);   
     
    -- Mapping
    VME_DTACKn  <= DTACKn;                  
    VME_RETRYn  <= RETRYn;
    VME_BERRn   <= BERRn;
    
    -- Buffer Settings
    VME_DATA_BUF_OEn    <= NOT VME_DATA_OE;
    VME_ADDR_BUF_OEn    <= NOT VME_ADDR_OE;
    VME_DTACK_BUF_OEn   <= NOT VME_DTACK_OE;
    VME_RETRY_BUF_OEn   <= NOT VME_RETRY_OE;
    VME_BERR_BUF_OEn    <= NOT VME_BERR_OE;
    VME_DATA_BUF_DIR    <= VME_DATA_DIR;
    VME_ADDR_BUF_DIR    <= VME_ADDR_DIR;
    
    --============--
    -- INTERRUPTS --
    --============--
    
    VME_IACKOUTn <= '0' when VME_IACKINn = '0' AND ASn = '0' else
                    '1';
    
    VME_IRQn     <= "1111111";

    --================--
    -- DECODING CYCLE --
    --================--
    
    --== FIRST PHASE ==--
    
    -- Addressing Mode    
    ADDR_MODE   <=  A64 when AM = 6X"00" OR AM = 6X"01" OR AM = 6X"03" OR AM = 6X"04" else 
                    A40 when AM = 6X"34" OR AM = 6X"35" OR AM = 6X"37" else
                    A32 when AM = 6X"08" OR AM = 6X"09" OR AM = 6X"0A" OR AM = 6X"0B" OR AM = 6X"0C" OR AM = 6X"0D" OR AM = 6X"0E" OR AM = 6X"0F" else    
                    A24 when AM = 6X"2F" OR AM = 6X"38" OR AM = 6X"39" OR AM = 6X"3A" OR 
                             AM = 6X"3B" OR AM = 6X"3C" OR AM = 6X"3D" OR AM = 6X"3E" OR AM = 6X"3F" else
                    A16 when AM = 6X"29" OR AM = 6X"2C" OR AM = 6X"2D" else
                    undef;
    
    -- !!! WARNING NOT VME SPECIFICATION CONFORM !!! NO CR/CSR SLAVE SELECT --
    -- Slave selection
    SLAVE_sel   <=  '1' when (ADDR_MODE = A64 AND GAn = acc_addr_upper(63 downto 59)) OR
                             (ADDR_MODE = A40 AND GAn = acc_addr_upper(39 downto 35)) OR
                             (ADDR_MODE = A32 AND GAn = acc_addr_upper(28 downto 24)) OR -- for BLM system atm (0x1000000 per Slot)
                             (ADDR_MODE = A24 AND GAn = acc_addr_upper(23 downto 19) AND CSR_MODE = '0') OR
                             (ADDR_MODE = A16 AND GAn = acc_addr_upper(15 downto 11)) else 
                    '0';
    
    CSR_MODE    <=  '1' when AM = 6X"2F" else
                    '0';
    
    -- Transfer Mode
    TRANSFER_MODE   <=  MBLT when AM = 6X"00" OR AM = 6X"08" OR AM = 6X"0C" OR AM = 6X"38" OR AM = 6X"3C" else
                        BLT  when AM = 6X"03" OR AM = 6X"0B" OR AM = 6X"0F" OR AM = 6X"3B" OR AM = 6X"3F" OR AM = 6X"37" else
                        SCT  when AM = 6X"01" OR AM = 6X"09" OR AM = 6X"0D" OR AM = 6X"39" OR AM = 6X"3D" OR AM = 6X"29" OR AM = 6X"2D" OR AM = 6X"2F" else
                        undef;
    
    TRANSFER_TYPE   <=  MP  when ADDR_MODE = A64 OR ADDR_MODE = A40 OR TRANSFER_MODE = MBLT else
                        nMP;
    
    --== SECOND PHASE ==--
    
    DATA_MODE       <=  D64             when TRANSFER_MODE = MBLT AND LWORDn = '0' else
                        D32_BLT         when TRANSFER_MODE = BLT  AND LWORDn = '0' else
                        D32             when TRANSFER_MODE = SCT  AND LWORDn = '0' else
                        D16_BLT         when TRANSFER_MODE = BLT  AND LWORDn = '1' else
                        D16             when TRANSFER_MODE = SCT  AND LWORDn = '1' else
                        D08_ODD_BLT     when TRANSFER_MODE = BLT  AND LWORDn = '1' AND D08_odd_acc = '1' else
                        D08_ODD         when TRANSFER_MODE = SCT  AND LWORDn = '1' AND D08_odd_acc = '1' else
                        D08_EVEN_BLT    when TRANSFER_MODE = BLT  AND LWORDn = '1' AND D08_even_acc = '1' else
                        D08_EVEN        when TRANSFER_MODE = SCT  AND LWORDn = '1' AND D08_even_acc = '1' else
                        undef;
    
    
    --================--
    -- GEO ADDRESSING --
    --================--
    
    -- Inversion 
    GAn <= NOT VME_GA;
    
    -- Parity Check
    GAP_error <= VME_GA(0) XNOR VME_GA(1) XNOR VME_GA(2) XNOR VME_GA(3) XNOR VME_GA(4) XNOR VME_GAP;
   
    
    --==============--
    -- ACC SETTINGS --
    --==============--
    
    acc_WRn         <= WRn;
    
    acc_dsize       <= "11" when DATA_MODE = D64 else
                       "10" when DATA_MODE = D32 OR DATA_MODE = D32_BLT else
                       "01" when DATA_MODE = D16 OR DATA_MODE = D16_BLT else
                       "00" when DATA_MODE = D08_EVEN OR DATA_MODE = D08_EVEN_BLT OR DATA_MODE = D08_ODD OR DATA_MODE = D08_ODD_BLT else
                       "00";
                   
    acc_mode        <= '1' when TRANSFER_MODE = MBLT OR TRANSFER_MODE = BLT else
                       '0';
                   
    acc_csr         <= CSR_MODE;
    
    acc_addr_upper  <= ADDR(63 downto 3)                                                                   when ADDR_MODE = A64 else
                       (63 downto 40 => '0') & ADDR(39 downto 32) & ADDR(47 downto 40) & ADDR(23 downto 3) when ADDR_MODE = A40 else
                       (63 downto 32 => '0') & ADDR(31 downto 3)                                           when ADDR_MODE = A32 else
                       (63 downto 24 => '0') & ADDR(23 downto 3)                                           when ADDR_MODE = A24 else
                       (63 downto 16 => '0') & ADDR(15 downto 3);
    
    acc_addr        <= acc_addr_upper & "000"                   when DATA_MODE = D64 else
                       acc_addr_upper & ADDR(2) & "00"          when DATA_MODE = D32 OR DATA_MODE = D32_BLT else
                       acc_addr_upper & ADDR(2 downto 1) & '0'  when DATA_MODE = D16 OR DATA_MODE = D16_BLT else
                       acc_addr_upper & ADDR(2 downto 1) & DS1n when DATA_MODE = D08_EVEN OR DATA_MODE = D08_EVEN_BLT OR
                                                                     DATA_MODE = D08_ODD OR DATA_MODE = D08_ODD_BLT else
                       (others => '0'); 

    --=============--
    -- OUTPUT REGs --
    --=============--
    
    -- WRITE CMI
    process(clk) is
    begin
        if rising_edge(clk) then
            if write_next = '1' then
                write_data  <= WRDATA;
                write_vld   <= write_vld_int;
            end if;
        end if;
    end process;
    
    -- ACCESS CMI
    process(clk) is
    begin
        if rising_edge(clk) then
            if acc_next = '1' then
                acc_data    <= acc_WRn & acc_dsize & acc_mode & acc_csr & acc_addr;
                acc_vld     <= acc_vld_int;
            end if;
        end if;
    end process;
    
    --==========--
    -- BYTE POS --
    --==========--
    
    -- for Block Transfer --
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if byte_pos_set = '1' then
                byte_pos_cnt    <= UNSIGNED(acc_addr(2 downto 0));
            elsif byte_pos_inc = '1' then 
                case acc_dsize is
                    when "00"   => byte_pos_cnt <= byte_pos_cnt + 1;
                    when "01"   => byte_pos_cnt <= byte_pos_cnt + 2;
                    when "10"   => byte_pos_cnt <= byte_pos_cnt + 4;
                    when "11"   => byte_pos_cnt <= byte_pos_cnt + 8;
                    when others => byte_pos_cnt <= byte_pos_cnt;
                end case;
            end if;
                  
        end if;
    end process;
    
    
    --===========--
    -- DATA PREP --
    --===========--
    
    -- WRITE DATA
    process(all) is
    begin
        -- Defaults
        WRDATA <= (others => '0');
        
        if DATA_MODE = D64 then
            WRDATA <= DATA;
            
        elsif DATA_MODE = D32 OR DATA_MODE = D32_BLT then
        
            if byte_pos_cnt(2) = '0' then
                WRDATA(63 downto 32) <= DATA(31 downto 0);
            else
                WRDATA(31 downto 0) <= DATA(31 downto 0);
            end if;
        
        elsif DATA_MODE = D16 OR DATA_MODE = D16_BLT then
        
            case byte_pos_cnt(2 downto 1) is
                when "00" => WRDATA(63 downto 48) <= DATA(15 downto 0);
                when "01" => WRDATA(47 downto 32) <= DATA(15 downto 0); 
                when "10" => WRDATA(31 downto 16) <= DATA(15 downto 0);
                when "11" => WRDATA(15 downto 0) <= DATA(15 downto 0);
                when others => null;
            end case;
        
        else
        
            case byte_pos_cnt is
                when "000" => WRDATA(63 downto 56) <= DATA(15 downto 8);
                when "001" => WRDATA(55 downto 48) <= DATA(7 downto 0);
                when "010" => WRDATA(47 downto 40) <= DATA(15 downto 8);
                when "011" => WRDATA(39 downto 32) <= DATA(7 downto 0);
                when "100" => WRDATA(31 downto 24) <= DATA(15 downto 8);
                when "101" => WRDATA(23 downto 16) <= DATA(7 downto 0);
                when "110" => WRDATA(15 downto 8)  <= DATA(15 downto 8);
                when "111" => WRDATA(7 downto 0)   <= DATA(7 downto 0);
                when others => null;
            end case;
            
        end if;
        
    end process;
    
    
    -- READ DATA
    process(all) is
    begin
        if DATA_MODE = D64 then
            RDDATA <= read_data;
        elsif DATA_MODE = D32 OR DATA_MODE = D32_BLT then
            if byte_pos_cnt(2) = '0' then
                RDDATA <= (63 downto 32 => '0') & read_data(63 downto 32);
            else
                RDDATA <= (63 downto 32 => '0') & read_data(31 downto 0);
            end if;
        elsif DATA_MODE = D16 OR DATA_MODE = D16_BLT then
            case byte_pos_cnt(2 downto 1) is
                when "00" => RDDATA <= (63 downto 16 => '0') & read_data(63 downto 48);
                when "01" => RDDATA <= (63 downto 16 => '0') & read_data(47 downto 32);
                when "10" => RDDATA <= (63 downto 16 => '0') & read_data(31 downto 16);
                when "11" => RDDATA <= (63 downto 16 => '0') & read_data(15 downto 0);
                when others => RDDATA <= (others => '0');
            end case;
        else
            case byte_pos_cnt is
                when "000" =>   RDDATA(15 downto 8)     <= read_data(63 downto 56);
                                RDDATA(7 downto 0)      <= (others => '0');
                                RDDATA(63 downto 16)    <= (others => '0');
                when "001" =>   RDDATA(7 downto 0)      <= read_data(55 downto 48);
                                RDDATA(63 downto 8)     <= (others => '0');
                when "010" =>   RDDATA(15 downto 8)     <= read_data(47 downto 40);
                                RDDATA(7 downto 0)      <= (others => '0');
                                RDDATA(63 downto 16)    <= (others => '0');
                when "011" =>   RDDATA(7 downto 0)      <= read_data(39 downto 32);
                                RDDATA(63 downto 8)     <= (others => '0');
                when "100" =>   RDDATA(15 downto 8)     <= read_data(31 downto 24);
                                RDDATA(7 downto 0)      <= (others => '0');
                                RDDATA(63 downto 16)    <= (others => '0');
                when "101" =>   RDDATA(7 downto 0)      <= read_data(23 downto 16);
                                RDDATA(63 downto 8)     <= (others => '0');
                when "110" =>   RDDATA(15 downto 8)     <= read_data(15 downto 8);
                                RDDATA(7 downto 0)      <= (others => '0');
                                RDDATA(63 downto 16)    <= (others => '0');
                when "111" =>   RDDATA(7 downto 0)      <= read_data(7 downto 0);
                                RDDATA(63 downto 8)     <= (others => '0');
                when others =>  RDDATA <= (others => '0');
            end case;
        end if;
        
    end process;

    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if resc_rst = '1' then
                resc_cnt <= (others => '0');
            elsif resc_inc = '1' then
                resc_cnt <= resc_cnt + 1;
            end if;
            if dt_rst = '1' then
                dt_cnt <= (others => '0');
            elsif dt_inc = '1' then
                dt_cnt <= dt_cnt + 1;
            end if;
        end if;
    end process;

    --=====--
    -- FSM --
    --=====--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    
    process (all)
    begin
        -- DEFAULTS
        -- VME Control Signals
        DTACKn              <= '1';
        BERRn               <= '1';
        RETRYn              <= '1';
        -- DIR 
        VME_INOUT_DIR       <= '0';
        -- OE
        VME_DATA_OE         <= '1';
        VME_ADDR_OE         <= '1';
        VME_DTACK_OE        <= '0';
        VME_RETRY_OE        <= '0';
        VME_BERR_OE         <= '0';
        -- VME Access
        VME_ACC             <= '1';
        -- CMA
        write_vld_int       <= '0';
        acc_vld_int         <= '0';
        read_next           <= NOT read_vld;
        -- Counters
        resc_rst            <= '0';
        resc_inc            <= '0';
        dt_rst              <= '0';
        dt_inc              <= '0';
        -- Internal RegEn
        AS_en               <= '0'; 
        DS_en               <= '0'; 
        -- Byte Access
        D08_odd_acc         <= '0';
        D08_even_acc        <= '0';
        -- Byte Position Counter (BLT)
        byte_pos_set        <= '0';
        byte_pos_inc        <= '0';
        -- Ignored
        VME_SYSFAILn        <= '1';
            
        case present_state is
            
            --===============--
            when IDLE =>
            --===============--
                
                VME_DATA_OE <= '0';  
                VME_ADDR_OE <= '0';  
                VME_ACC     <= '0';
                
                if VME_BUS_BUSY = '0' then
                    if GAP_error = '1' then -- electrical error
                        next_state <= GAPerr;
                    else
                        if ASn = '0' AND VME_IACKn = '1' then 
                            AS_en       <= '1';
                            next_state  <= CHK_SELECT;
                        else
                            next_state  <= IDLE;
                        end if;
                    end if;
                else
                    next_state <= IDLE;
                end if;
            
            --===============-- 
            when GAPerr =>
            --===============--
                
                VME_DATA_OE <= '0'; 
                VME_ADDR_OE <= '0'; 
                VME_ACC     <= '0';
                next_state  <= GAPerr;
                
            --===============-- 
            when CHK_SELECT =>
            --===============--
                
                VME_DATA_OE <= '0'; 
                VME_ADDR_OE <= '0'; 
                VME_ACC     <= '0';
                
                if SLAVE_sel = '0' then
                    next_state <= NO_SELECT;
                else
                    if TRANSFER_TYPE = MP then
                        next_state <= MP_DS_ADDR_CYCLE_START;
                    else
                        next_state <= nMP_DS_DATA_FIRST_CYCLE_START;
                    end if;
                end if;
                
            --===============-- 
            when NO_SELECT =>
            --===============--
                
                VME_DATA_OE <= '0'; 
                VME_ADDR_OE <= '0'; 
                VME_ACC     <= '0';
                
                if ASn = '1' then
                    next_state <= IDLE;
                else
                    next_state <= NO_SELECT;
                end if;
            
          
          
            ------------------------
            --== MP CYCLE BEGIN ==--
            ------------------------
            
            --===============-- 
            when MP_DS_ADDR_CYCLE_START =>
            --===============--
            
                if NOT DS0n OR NOT DS1n then
                    DS_en           <= '1';
                    next_state      <= MP_FWD_ACC;
                    if DS0n = '1' AND DS1n = '0' then
                        D08_even_acc    <= '1';
                    end if;
                    if DS0n = '0' AND DS1n = '1' then
                        D08_odd_acc     <= '1';
                    end if;
                else
                    next_state <= MP_DS_ADDR_CYCLE_START;
                end if;
            
            --===============-- 
            when MP_FWD_ACC =>
            --===============--
            
                if acc_next = '1' then
                    byte_pos_set    <= '1';
                    acc_vld_int     <= '1';
                    next_state      <= MP_ADDR_DT_FORCELAT;
                else
                    next_state      <= MP_FWD_ACC;
                end if;
            
            --===============-- 
            when MP_ADDR_DT_FORCELAT =>
            --===============--
            
                if dt_cnt = dtack_fb_min_delay then
                    dt_rst          <= '1';
                    VME_DTACK_OE    <= '1';
                    DTACKn          <= '0';
                    next_state      <= MP_DS_ADDR_CYCLE_END;
                else
                    dt_inc          <= '1';
                    next_state      <= MP_ADDR_DT_FORCELAT;
                end if;
            
            --===============-- 
            when MP_DS_ADDR_CYCLE_END =>
            --===============--
            
                VME_DTACK_OE    <= '1';
                DTACKn          <= '0';
                
                if DS0n = '1' AND DS1n = '1' then
                    next_state <= RESC_DTACK;
                else
                    next_state <= MP_DS_ADDR_CYCLE_END;
                end if;
                
            --===============-- 
            when MP_DS_DATA_CYCLE_START =>
            --===============--
            
                if ASn = '1' then
                    next_state <= CLNUP_READ;
                else
                    if NOT DS0n OR NOT DS1n then
                        if WRn = '1' then
                            next_state  <= RD_DT_FORCELAT;
                        else
                            DS_en       <= '1';
                            next_state  <= WR_TRANSFER;
                        end if;
                    else 
                        next_state <= MP_DS_DATA_CYCLE_START;
                    end if;
                end if;
            
            ----------------------
            --== MP CYCLE END ==--
            ----------------------
            
            
            
            -------------------------
            --== nMP CYCLE START ==--
            -------------------------
            
            --===============-- 
            when nMP_DS_DATA_FIRST_CYCLE_START =>
            --===============--

                if NOT DS0n OR NOT DS1n then
                    DS_en       <= '1';
                    next_state  <= nMP_FWD_ACC;
                    if DS0n = '1' AND DS1n = '0' then
                        D08_even_acc    <= '1';
                    end if;
                    if DS0n = '0' AND DS1n = '1' then
                        D08_odd_acc     <= '1';
                    end if;
                else
                    next_state <= nMP_DS_DATA_FIRST_CYCLE_START;
                end if;
            
            --===============-- 
            when nMP_FWD_ACC =>
            --===============--
            
                if acc_next = '1' then
                    byte_pos_set    <= '1';
                    acc_vld_int     <= '1';
                    if WRn = '1' then
                        next_state  <= RD_DT_FORCELAT;
                    else
                        DS_en       <= '1';
                        next_state  <= WR_TRANSFER;
                    end if; 
                else
                    next_state <= nMP_FWD_ACC;
                end if;
            
            --===============-- 
            when nMP_DS_DATA_BLT_CYCLE_START =>
            --===============--
                
                if ASn = '1' then
                    next_state <= CLNUP_READ;
                else
                   if NOT DS0n OR NOT DS1n then
                        
                        if DS0n = '1' AND DS1n = '0' then
                            D08_even_acc    <= '1';
                        end if;
                        if DS0n = '0' AND DS1n = '1' then
                            D08_odd_acc     <= '1';
                        end if;
                        if WRn = '1' then
                            next_state <= RD_DT_FORCELAT;
                        else
                            DS_en      <= '1';
                            next_state <= WR_TRANSFER;
                        end if;
                    else
                        next_state <= nMP_DS_DATA_BLT_CYCLE_START;
                    end if;
                end if;
            
            -----------------------
            --== nMP CYCLE END ==--
            -----------------------
            
            
            
            --------------------------------
            --== RESCINDING DTACK START ==--
            --------------------------------
            
            --===============-- 
            when RESC_DTACK =>
            --===============--
            
                if resc_cnt = dtack_resc_delay then
                    resc_rst        <= '1';
                    byte_pos_inc    <= '1';
                    if TRANSFER_TYPE = MP then
                        next_state <= MP_DS_DATA_CYCLE_START;
                    else
                        next_state <= nMP_DS_DATA_BLT_CYCLE_START;
                    end if;
                else
                    resc_inc        <= '1';
                    VME_DTACK_OE    <= '1';
                    DTACKn          <= '1';
                    next_state      <= RESC_DTACK;
                end if;
            
            ------------------------------
            --== RESCINDING DTACK END ==--
            ------------------------------
            
            
            
            ---------------------
            --== WRITE START ==--
            ---------------------
            
            --===============-- 
            when WR_TRANSFER =>
            --===============--
            
                if write_next = '1' then
                    write_vld_int   <= '1';
                    next_state      <= WR_DT_FORCELAT;
                else
                    next_state      <= WR_TRANSFER;
                end if;
            
            --===============-- 
            when WR_DT_FORCELAT =>
            --===============--
            
                if dt_cnt = dtack_fb_min_delay then
                    dt_rst          <= '1';
                    VME_DTACK_OE    <= '1';
                    DTACKn          <= '0';
                    next_state      <= WR_DS_CYCLE_END;
                else
                    dt_inc          <= '1';
                    next_state      <= WR_DT_FORCELAT;
                end if;
            
            --===============-- 
            when WR_DS_CYCLE_END =>
            --===============--
            
                VME_DTACK_OE    <= '1';
                DTACKn          <= '0';
                
                if DS0n = '1' AND DS1n = '1' then
                    DTACKn  <= '1';
                    if TRANSFER_TYPE = nMP AND TRANSFER_MODE = SCT then
                        if ASn = '1' then
                            next_state <= IDLE;
                        else
                            next_state <= ADDR_PIPE;
                        end if;
                    else
                        next_state  <= RESC_DTACK;
                    end if;
                else
                    next_state  <= WR_DS_CYCLE_END;
                end if;
            
            -------------------
            --== WRITE END ==--
            -------------------
            
            
            
            --------------------
            --== READ START ==--
            --------------------
            
            --===============-- 
            when RD_DT_FORCELAT =>
            --===============--
            
                if dt_cnt = dtack_fb_min_delay then
                    dt_rst      <= '1';
                    next_state  <= RD_TRANSFER;
                else
                    dt_inc      <= '1';
                    next_state  <= RD_DT_FORCELAT;
                end if;
            
            --===============-- 
            when RD_TRANSFER =>
            --===============--
            
                if read_vld = '1' then
                    VME_INOUT_DIR   <= '1';  -- set Bus as output
                    VME_DTACK_OE    <= '1';  -- acknowledge read access
                    DTACKn          <= '0';
                    next_state      <= RD_DS_CYCLE_END;
                else
                    next_state      <= RD_TRANSFER;
                end if;
            
            --===============--
            when RD_DS_CYCLE_END =>
            --===============--
            
                VME_INOUT_DIR   <= '1'; -- set Bus as output
                VME_DTACK_OE    <= '1'; -- hold acknowledge read access
                DTACKn          <= '0';

                if DS0n = '1' AND DS1n = '1' then
                    DTACKn      <= '1';
                    read_next   <= '1'; -- next read data
                    if TRANSFER_TYPE = nMP AND TRANSFER_MODE = SCT then
                        if ASn = '1' then
                            next_state <= IDLE;
                        else
                            next_state <= ADDR_PIPE;
                        end if;
                    else
                        next_state <= RESC_DTACK;
                    end if;
                else
                    next_state <= RD_DS_CYCLE_END;
                end if;
            
            ------------------
            --== READ END ==--
            ------------------
            
            
            --===============-- 
            when ADDR_PIPE =>
            --===============--
            
                if ASn = '1' then 
                    next_state <= IDLE;
                else 
                    next_state <= ADDR_PIPE;
                end if;
            
            --===============--
            when CLNUP_READ =>
            --===============--
                
                VME_DATA_OE <= '0';  
                VME_ADDR_OE <= '0';
                VME_ACC     <= '0';
                
                if read_vld = '1' then
                    read_next   <= '1';
                    next_state  <= CLNUP_READ;
                else
                    next_state  <= IDLE;
                end if;
                
                        
         end case;
   end process;

   
end architecture struct;