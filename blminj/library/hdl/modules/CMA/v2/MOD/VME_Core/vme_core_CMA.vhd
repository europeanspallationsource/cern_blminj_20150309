------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2013
Module Name:    VME_Core_CMA


-----------------
Short Description
-----------------

    This is the Core Module for VME access utilizing CMA Interconnections.

------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    
    Tclk_ns             := clock in ns
    wrreq_data_size     := size of the wrreq CMI data port
    rdreq_data_size     := size of the rdreq CMI data port
    rdout_data_size     := size of the rdout CMI data port
     
    VME_data_size       := general size of the data transferred in the core(equals rdout_data_size)
    VME_addr_size       := general size of the addresses transferred in the core(equals rdreq_data_size)
    wrfifo_depth        := depth of the write FIFO
    rdfifo_depth        := depth of the read FIFO
    

--------------
Implementation
--------------

 
-----------
Limitations
-----------
    
    
----------------
Missing Features
----------------

    CS/CSR is not implemented (needs to be added inside the vme_interface and as a new module inside the core)

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

entity VME_Core_CMA is
generic
(

    Tclk_ns:            real    := 8.0;
    rdreq_data_size:    integer := 61;
    wrreq_data_size:    integer := 133;
    rdout_data_size:    integer := 64
    
);

port
(   
    -- Clock
    clk:                in  std_logic;
    -- VME Interface
    VME_GA:             in  std_logic_vector(4 downto 0);
    VME_GAP:            in  std_logic;
    VME_WRn:            in  std_logic;
    VME_AM:             in  std_logic_vector(5 downto 0);
    VME_ASn:            in  std_logic;
    VME_DS0n:           in  std_logic;
    VME_DS1n:           in  std_logic;
    VME_IACKn:          in  std_logic;
    VME_IACKINn:        in  std_logic;
    VME_SYSCLK:         in  std_logic;
    VME_SYSRESETn:      in  std_logic;

    VME_LWORDn_OUT:     out std_logic;
    VME_DATA_OUT:       out std_logic_vector(31 downto 0);
    VME_ADDR_OUT:       out std_logic_vector(31 downto 1);
    VME_LWORDn_IN:      in  std_logic;
    VME_DATA_IN:        in  std_logic_vector(31 downto 0);
    VME_ADDR_IN:        in  std_logic_vector(31 downto 1);

    VME_DTACKn:         out std_logic;
    VME_BERRn:          out std_logic;
    VME_RETRYn:         out std_logic;
    VME_IACKOUTn:       out std_logic;
    VME_IRQn:           out std_logic_vector(7 downto 1);
    VME_SYSFAILn:       out std_logic; 

    VME_ACC:            out std_logic;
    VME_BUS_BUSY:       in  std_logic;
    VME_DATA_BUF_OEn:   out std_logic;
    VME_ADDR_BUF_OEn:   out std_logic;
    VME_DTACK_BUF_OEn:  out std_logic;
    VME_RETRY_BUF_OEn:  out std_logic;
    VME_BERR_BUF_OEn:   out std_logic;
    
    VME_DATA_BUF_DIR:   out std_logic;
    VME_ADDR_BUF_DIR:   out std_logic;
    -- VME Access Controller
    rdreq_data:         out std_logic_vector(rdreq_data_size-1 downto 0);
    rdreq_vld:          out std_logic;
    rdreq_next:         in  std_logic;
    
    wrreq_data:         out std_logic_vector(wrreq_data_size-1 downto 0);
    wrreq_vld:          out std_logic;
    wrreq_next:         in  std_logic;
    -- Readout FIFO
    rdout_data:         in  std_logic_vector(rdout_data_size-1 downto 0);
    rdout_vld:          in  std_logic;
    rdout_next:         out std_logic
);
    
end entity VME_Core_CMA;





architecture struct of VME_Core_CMA is   
    
    --===========--
    -- CONSTANTS --
    --===========--

    constant VME_data_size:         integer := rdout_data_size;
    constant VME_addr_size:         integer := rdreq_data_size;
    constant wrfifo_depth:          integer := 256;
    constant rdfifo_depth:          integer := 256;
    
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- vmeif_acc
    constant vmeif_acc_data_size:   integer := 69;
    -- vmeif_wr
    constant vmeif_wr_data_size:    integer := VME_data_size;
    -- wrfifo
    constant wrfifo_data_size:      integer := VME_data_size;
    -- rdfifo
    constant rdfifo_data_size:      integer := VME_data_size;

    --=================--
    -- GENERAL SIGNALS --
    --=================--
        
    signal VME_ACC_int:             std_logic;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- vmeif_wr
    signal vmeif_wr_data:           std_logic_vector(vmeif_wr_data_size-1 downto 0);
    signal vmeif_wr_vld:            std_logic;
    signal vmeif_wr_next:           std_logic;
    -- wrfifo
    signal wrfifo_data:             std_logic_vector(wrfifo_data_size-1 downto 0);
    signal wrfifo_vld:              std_logic;
    signal wrfifo_next:             std_logic;
    -- vmeif_acc
    signal vmeif_acc_data:          std_logic_vector(vmeif_acc_data_size-1 downto 0);
    signal vmeif_acc_vld:           std_logic;
    signal vmeif_acc_next:          std_logic;
    -- rdfifo
    signal rdfifo_data:             std_logic_vector(rdfifo_data_size-1 downto 0);
    signal rdfifo_vld:              std_logic;
    signal rdfifo_next:             std_logic;

begin
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    
    VME_ACC <= VME_ACC_int;
    
    --===============--
    -- VME INTERFACE --
    --===============--
    
    VME_Interface_CMA_inst: entity work.VME_Interface_CMA
    generic map
    (
        Tclk_ns             => Tclk_ns
    )
    port map
    (   
        clk                 => clk,
        VME_GA              => VME_GA,
        VME_GAP             => VME_GAP,
        VME_WRn             => VME_WRn,
        VME_AM              => VME_AM,
        VME_ASn             => VME_ASn,
        VME_DS0n            => VME_DS0n,
        VME_DS1n            => VME_DS1n,
        VME_IACKn           => VME_IACKn,
        VME_IACKINn         => VME_IACKINn,
        VME_SYSCLK          => VME_SYSCLK,
        VME_SYSRESETn       => VME_SYSRESETn,
        VME_ADDR_IN         => VME_ADDR_IN,
        VME_LWORDn_IN       => VME_LWORDn_IN,
        VME_DATA_IN         => VME_DATA_IN,
        VME_ADDR_OUT        => VME_ADDR_OUT,
        VME_LWORDn_OUT      => VME_LWORDn_OUT,
        VME_DATA_OUT        => VME_DATA_OUT,
        VME_DTACKn          => VME_DTACKn,
        VME_BERRn           => VME_BERRn,
        VME_RETRYn          => VME_RETRYn,
        VME_IACKOUTn        => VME_IACKOUTn,
        VME_IRQn            => VME_IRQn,
        VME_SYSFAILn        => VME_SYSFAILn,
        VME_ACC             => VME_ACC_int,
        VME_BUS_BUSY        => VME_BUS_BUSY,
        VME_DATA_BUF_OEn    => VME_DATA_BUF_OEn,
        VME_ADDR_BUF_OEn    => VME_ADDR_BUF_OEn,  
        VME_DTACK_BUF_OEn   => VME_DTACK_BUF_OEn,  
        VME_RETRY_BUF_OEn   => VME_RETRY_BUF_OEn,  
        VME_BERR_BUF_OEn    => VME_BERR_BUF_OEn,   
        VME_DATA_BUF_DIR    => VME_DATA_BUF_DIR,   
        VME_ADDR_BUF_DIR    => VME_ADDR_BUF_DIR,     
        -- to wrfifo
        write_data          => vmeif_wr_data,
        write_vld           => vmeif_wr_vld,
        write_next          => vmeif_wr_next,
        -- to acc_controller
        acc_data            => vmeif_acc_data,
        acc_vld             => vmeif_acc_vld,
        acc_next            => vmeif_acc_next,
        -- from rdfifo
        read_data           => rdfifo_data,
        read_vld            => rdfifo_vld,
        read_next           => rdfifo_next
    );
    
    
    --========--
    -- wrFIFO --
    --========--
    
    wrfifo: entity work.fifo_CMA
    GENERIC MAP
    (
        data_size   => wrfifo_data_size,
        fifo_depth  => wrfifo_depth
    )
    PORT MAP
    (
        clk                 => clk,
        inp_data            => vmeif_wr_data,
        inp_vld             => vmeif_wr_vld,
        inp_next            => vmeif_wr_next,
        out_data            => wrfifo_data,
        out_vld             => wrfifo_vld,
        out_next            => wrfifo_next
    );
    
    --========--
    -- rdFIFO --
    --========--
    
    rdfifo: entity work.fifo_CMA
    GENERIC MAP
    (
        data_size   => rdfifo_data_size,
        fifo_depth  => rdfifo_depth
    )
    PORT MAP
    (
        clk                 => clk,
        inp_data            => rdout_data,
        inp_vld             => rdout_vld,
        inp_next            => rdout_next,
        out_data            => rdfifo_data,
        out_vld             => rdfifo_vld,
        out_next            => rdfifo_next
    );
    
    --================--
    -- ACC CONTROLLER --
    --================--
    
    
    VME_AccessCtrl_CMA_inst: entity work.VME_AccessCtrl_CMA
    PORT MAP
    (
        clk             => clk,
        acc_data        => vmeif_acc_data,
        acc_vld         => vmeif_acc_vld,
        acc_next        => vmeif_acc_next,
        wrfifo_data     => wrfifo_data,
        wrfifo_vld      => wrfifo_vld,
        wrfifo_next     => wrfifo_next,
        rdreq_data      => rdreq_data,
        rdreq_vld       => rdreq_vld,
        rdreq_next      => rdreq_next,
        wrreq_data      => wrreq_data,
        wrreq_vld       => wrreq_vld,
        wrreq_next      => wrreq_next,
        VME_ACC         => VME_ACC_int   
    );

end architecture struct;
