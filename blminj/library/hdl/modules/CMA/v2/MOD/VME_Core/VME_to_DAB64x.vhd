-- -------------------------------------------
-- Company:        CERN - BE/BI/BL
-- Engineer:       Marcel Alsdorf
-- Created:        01/11/2011
-- Module Name:    VME_to_DAB64x

-- Updates
-- ------------------------------------------
-- 23/10/2014: Christos
--             [minor] clean-up for readability

-- Description
-- ------------------------------------------
-- This module is mapping some of the standard VME Interface signals to the signals used/renamed on the DAB64x card.

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;

entity VME_to_DAB64x is
port
(
    -- DAB64x Inputs
    Stratix_IRQ_VECTORn:    out std_logic;
    VME_IRQ_Level:          in std_logic_vector(2 downto 0);
    IRQ_Vec_OEn:            in std_logic;
    Board_Rst_ONOFFn:       in std_logic;
    -- DAB64x Outputs
    Stratix_VME_ACCn:       out std_logic;
    FP_VME_ACC_LEDn:        out std_logic;
    VME_ForceRetry:         out std_logic;  
    VME_2e_Cycle:           out std_logic;  
    Stratix_245_OEn:        out std_logic;   
    Stratix_dtackN:         out std_logic;
    Stratix_SYSFAILn:       out std_logic;
    -- DAB64x IOs
    VME_ADDR:               inout std_logic_vector(31 downto 1);
    VME_LWORDn:             inout std_logic;
    VME_DATA:               inout std_logic_vector(31 downto 0);
    VME_DTACK_EN:           inout std_logic;  
    -- VME_Interface Inputs
    VME_LWORDn_OUT:         in  std_logic;
    VME_DATA_OUT:           in  std_logic_vector(31 downto 0);
    VME_ADDR_OUT:           in  std_logic_vector(31 downto 1);
    
    VME_DATA_BUF_OEn:       in  std_logic;
    VME_ADDR_BUF_OEn:       in  std_logic;
    VME_DTACK_BUF_OEn:      in  std_logic;
    VME_RETRY_BUF_OEn:      in  std_logic;
    VME_BERR_BUF_OEn:       in  std_logic;
    
    VME_DATA_BUF_DIR:       in  std_logic;
    VME_ADDR_BUF_DIR:       in  std_logic;
    
    VME_ACC:                in  std_logic;
    VME_DTACKn:             in  std_logic;
    VME_SYSFAILn:           in  std_logic;
    
    -- VME_Interface Outputs
    VME_LWORDn_IN:          out std_logic;
    VME_DATA_IN:            out std_logic_vector(31 downto 0);
    VME_ADDR_IN:            out std_logic_vector(31 downto 1)
);
    
end entity VME_to_DAB64x;

architecture struct of VME_to_DAB64x is

begin
    
    --================--
    -- BUFFER CONTROL --
    --================--

    VME_2e_Cycle        <= VME_ADDR_BUF_DIR;        -- Addr Bus Buffer Direction
    Stratix_245_OEn     <= VME_DATA_BUF_OEn;        -- Data Bus Buffer Output Enable
    VME_ForceRetry      <= NOT VME_RETRY_BUF_OEn;   -- Retry Buffer Direction used as output enable
    VME_DTACK_EN        <= 'Z';                     -- DTACK Buffer Direction used as output enable -- here it is handled by the CPLD (MAX device).
    
    -- IOs
    VME_ADDR    <= VME_ADDR_OUT    when VME_ADDR_BUF_DIR = '1'  else (others => 'Z');   
    VME_LWORDn  <= VME_LWORDn_OUT  when VME_ADDR_BUF_DIR = '1'  else 'Z';
    VME_DATA    <= VME_DATA_OUT    when VME_DATA_BUF_DIR = '1'  else (others => 'Z'); 
    
    VME_ADDR_IN     <= VME_ADDR;
    VME_DATA_IN     <= VME_DATA;
    VME_LWORDn_IN   <= VME_LWORDn;
    
    --===============--
    -- DAB64 Signals --
    --===============--
     
    Stratix_VME_ACCn    <= NOT VME_ACC;    -- Stratix VME Access
    FP_VME_ACC_LEDn     <= NOT VME_ACC;    -- Access LED
    Stratix_IRQ_VECTORn <= '1';            -- IRQ
    Stratix_dtackN      <= VME_DTACKn;     -- mapping
    Stratix_SYSFAILn    <= VME_SYSFAILn;
    
end architecture struct;




