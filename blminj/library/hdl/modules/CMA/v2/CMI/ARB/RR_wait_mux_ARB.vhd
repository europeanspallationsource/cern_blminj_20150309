------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    15/11/2013
Module Name:    RR_wait_mux_ARB

-----------
Description
-----------

    This Multiplexer takes from each output the next valid data and goes to the next.
    As long as there is no valid input at a certain place, it holds there until there is.


------------
Dependencies
------------

    vhdl_func_pkg

------------------
Generics/Constants
------------------

    tx_number   := number of inputs 
    
    sel_vsize   := vectorsize for the selection counter for the n inputs
    
-----------
Limitations
-----------

    

--------------
Implementation
--------------
    
-----------
Limitations
-----------


----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity RR_wait_mux_ARB is

generic
(
    tx_number:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Inputs
    ARB_tx_vld:     in  std_logic_vector(tx_number-1 downto 0);
    ARB_tx_next:    out std_logic_vector(tx_number-1 downto 0);
    -- CMI Output
    ARB_rx_vld:     out std_logic;
    ARB_rx_next:    in  std_logic;
    ARB_sel:        out std_logic_vector(get_vsize_addr(tx_number)-1 downto 0)
);

end entity RR_wait_mux_ARB;


architecture struct of RR_wait_mux_ARB is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant sel_vsize:     integer := get_vsize_addr(tx_number);
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- Input Counter
    signal sel_cnt:         UNSIGNED(sel_vsize-1 downto 0) := (others => '0');
    signal sel_rst:         std_logic;
    signal sel_inc:         std_logic;

BEGIN
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if sel_rst = '1' then
                sel_cnt <= (others => '0');
            elsif sel_inc = '1' then
                sel_cnt <= sel_cnt + 1;
            end if;
        end if;
    end process;
    
    --==============--
    -- COMBINATORIC --
    --==============--
    
    -- set ARB_sel output
    ARB_sel <= STD_LOGIC_VECTOR(sel_cnt);

    process (all) is
    begin
        -- DEFAULTS
        sel_inc         <= '0';
        sel_rst         <= '0';
        -- Next Defaults
        for i in tx_number-1 downto 0 loop
            ARB_tx_next(i) <= NOT ARB_tx_vld(i);
        end loop;
        
        
        -- connect selected input with output
        ARB_rx_vld                          <= ARB_tx_vld(TO_INTEGER(sel_cnt));
        ARB_tx_next(TO_INTEGER(sel_cnt))    <= ARB_rx_next;
        
        -- switch input
        if ARB_tx_vld(TO_INTEGER(sel_cnt)) = '1' AND ARB_rx_next = '1' then
            if sel_cnt = tx_number-1 then
                sel_rst <= '1';
            else
                sel_inc <= '1';
            end if;
        end if;        

    end process;
    
end architecture struct;
