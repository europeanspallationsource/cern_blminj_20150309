------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    15/11/2013
Module Name:    locked_conn_ARB

-----------
Description
-----------

    Depending on a changeable parameter one of the input and the output are directly connected
    and all others inputs are ignored (lost).


------------
Dependencies
------------

    vhdl_func_pkg

------------------
Generics/Constants
------------------

    tx_number   := number of inputs 
    
    sel_vsize   := vectorsize for the selector
    
-----------
Limitations
-----------

    

--------------
Implementation
--------------
    
-----------
Limitations
-----------


----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity locked_conn_ARB is

generic
(
    tx_number:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- PARAMETER
    PARAM_select:   in  std_logic_vector(get_vsize_addr(tx_number)-1 downto 0);
    -- CMI Inputs
    ARB_tx_vld:     in  std_logic_vector(tx_number-1 downto 0);
    ARB_tx_next:    out std_logic_vector(tx_number-1 downto 0);
    -- CMI Output
    ARB_rx_vld:     out std_logic;
    ARB_rx_next:    in  std_logic;
    ARB_sel:        out std_logic_vector(get_vsize_addr(tx_number)-1 downto 0)
);

end entity locked_conn_ARB;


architecture struct of locked_conn_ARB is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant sel_vsize:     integer := get_vsize_addr(tx_number);
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal int_select:      UNSIGNED(sel_vsize-1 downto 0) := (others => '0');
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
        
BEGIN
    
    --=======--
    -- PARAM --
    --=======--

    int_select <= UNSIGNED(PARAM_select);
    
    
    --==============--
    -- COMBINATORIC --
    --==============--
    
    ARB_sel <= STD_LOGIC_VECTOR(int_select);
    
    process(all) is
    begin
        --DEFAULTS
        for i in tx_number-1 downto 0 loop
            ARB_tx_next(i) <= '1';
        end loop;
        
        ARB_tx_next(TO_INTEGER(int_select)) <= ARB_rx_next;
        ARB_rx_vld <= ARB_tx_vld(TO_INTEGER(int_select));
        
    end process;
    
   
    
    

    
    
end architecture struct;
