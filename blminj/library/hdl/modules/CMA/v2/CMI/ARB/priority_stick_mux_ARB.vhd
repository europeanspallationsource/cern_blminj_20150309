------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/08/2012
Module Name:    priority_stick_mux_ARB

-----------
Description
-----------

    Priority Multiplexer to give priority in order of the inputs. It always sticks with
    the last input that had pririty until there is no valid data available.
--------
Generics
--------

    tx_number   := number of inputs 
    
    sel_vsize   := vectorsize for the selection counter for the n inputs
    
-----------
Limitations
-----------

    

----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg

-----------------
Necessary Modules
-----------------

    CMI
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity priority_stick_mux_ARB is

generic
(
    tx_number:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Inputs
    ARB_tx_vld:     in  std_logic_vector(tx_number-1 downto 0);
    ARB_tx_next:    out std_logic_vector(tx_number-1 downto 0);
    -- CMI Output
    ARB_rx_vld:     out std_logic;
    ARB_rx_next:    in  std_logic;
    ARB_sel:        out std_logic_vector(get_vsize_addr(tx_number)-1 downto 0)
);

end entity priority_stick_mux_ARB;


architecture struct of priority_stick_mux_ARB is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant sel_vsize:     integer := get_vsize_addr(tx_number);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal ARB_sel_int:     UNSIGNED(sel_vsize-1 downto 0);
    
    --==========--
    -- REGISTER --
    --==========--
    
    signal ARB_sel_last:    UNSIGNED(sel_vsize-1 downto 0) := (sel_vsize-1 downto 0 => '0');
    
BEGIN

    --==========--
    -- INP LOCK --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            ARB_sel_last <= ARB_sel_int;
        end if;
    end process;
    
    
    --==============--
    -- COMBINATORIC --
    --==============--
    
    -- choose input --
    process(all) is
    begin
        --DEFAULTS
        ARB_sel_int     <= (sel_vsize-1 downto 0 => '0');
        ARB_rx_vld      <= '0';
            
            if ARB_tx_vld(TO_INTEGER(ARB_sel_last)) = '1' then
                ARB_sel_int     <= ARB_sel_last;
                ARB_rx_vld      <= '1';
            else
                for k in tx_number-1 downto 0 loop
                    if ARB_tx_vld(k) = '1' then
                        ARB_sel_int   <= TO_UNSIGNED(k, sel_vsize);
                        ARB_rx_vld    <= '1';
                    end if;
                end loop;
            end if;
    end process;
    
    -- setting ARB_tx_next --
    
    process(all) is
    begin
        --DEFAULTS
        for i in tx_number-1 downto 0 loop
            ARB_tx_next(i) <= NOT ARB_tx_vld(i);
        end loop;
        
        ARB_tx_next(TO_INTEGER(ARB_sel_int)) <= ARB_rx_next OR NOT ARB_tx_vld(TO_INTEGER(ARB_sel_int));
        
    end process;

    
    -- set ARB_sel output

    ARB_sel <= STD_LOGIC_VECTOR(ARB_sel_int);

    
    
end architecture struct;
