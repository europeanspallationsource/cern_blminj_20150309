------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    15/11/2013
Module Name:    priority_pkg_mux_ARB

-----------
Description
-----------

    Priority Package Multiplexer gives priority in order of channels for complete packages.


------------
Dependencies
------------

    vhdl_func_pkg

------------------
Generics/Constants
------------------

    tx_number   := number of inputs 
    
    sel_vsize   := vectorsize for the selection counter for the n inputs
    
-----------
Limitations
-----------

    

--------------
Implementation
--------------
    
-----------
Limitations
-----------


----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use IEEE.std_logic_misc.all;
    use work.vhdl_func_pkg.all;
   
entity priority_pkg_mux_ARB is

generic
(
    tx_number:      integer;
    data_size:      integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Package Information
    full_data:      in  slv_array(tx_number-1 downto 0)(data_size-1 downto 0); 
    -- ARB Inputs
    ARB_tx_vld:     in  std_logic_vector(tx_number-1 downto 0);
    ARB_tx_next:    out std_logic_vector(tx_number-1 downto 0);
    -- ARB Output/Select
    ARB_rx_vld:     out std_logic;
    ARB_rx_next:    in  std_logic;
    ARB_sel:        out std_logic_vector(get_vsize_addr(tx_number)-1 downto 0)
);

end entity priority_pkg_mux_ARB;


architecture struct of priority_pkg_mux_ARB is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant sel_vsize:     integer := get_vsize_addr(tx_number);
    
    --======--
    -- FSMs --
    --======--
    
    type mux_state_t is (choose, fwd_pkg);
    signal mux_present_state, mux_next_state: mux_state_t := choose;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal ARB_sel_int:     UNSIGNED(sel_vsize-1 downto 0);

    --==========--
    -- REGISTER --
    --==========--
    
    signal ARB_sel_last:    UNSIGNED(sel_vsize-1 downto 0) := (others => '0');
    
BEGIN
    
    --==============--
    -- COMBINATORIC --
    --==============--
    
    -- set ARB_sel output
    ARB_sel <= STD_LOGIC_VECTOR(ARB_sel_int);

    --==========--
    -- INP LOCK --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            ARB_sel_last <= ARB_sel_int;
        end if;
    end process;
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            mux_present_state <= mux_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        ARB_rx_vld      <= '0';
        ARB_sel_int     <= ARB_sel_last;
        -- Next Defaults
        for i in tx_number-1 downto 0 loop
            ARB_tx_next(i) <= NOT ARB_tx_vld(i);
        end loop;
        
        case mux_present_state is
            
            --===============--
            when choose =>
            --===============--
                
                mux_next_state <= choose;
                
                for k in tx_number-1 downto 0 loop
                    if ARB_tx_vld(k) = '1' then
                        ARB_sel_int   <= TO_UNSIGNED(k, sel_vsize);
                        ARB_rx_vld    <= '1';
                    end if;
                end loop;
                
                if OR_REDUCE(ARB_tx_vld) = '1' then
                        mux_next_state <= fwd_pkg;
                    end if;
                
                ARB_tx_next(TO_INTEGER(ARB_sel_int)) <= ARB_rx_next OR NOT ARB_tx_vld(TO_INTEGER(ARB_sel_int));
                
            --===============--
            when fwd_pkg =>
            --===============--
                
                mux_next_state <= fwd_pkg;
                if ARB_rx_next = '1' AND ARB_tx_vld(TO_INTEGER(ARB_sel_last)) = '1' then
                    ARB_tx_next(TO_INTEGER(ARB_sel_last))   <= '1';
                    ARB_rx_vld                              <= '1';
                    if full_data(TO_INTEGER(ARB_sel_last))(64) = '1' then -- Pkg Bits / eop
                        mux_next_state <= choose;
                    end if;
                end if;
  
        end case;
        
    end process;
    
    
end architecture struct;
