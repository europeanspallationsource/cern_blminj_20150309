------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    15/11/2013
Module Name:    priority_mux_ARB

-----------
Description
-----------

    Priority Multiplexer to give priority in order of the inputs. It is an ARB extension for the MM_CMI.


------------
Dependencies
------------

    vhdl_func_pkg

------------------
Generics/Constants
------------------

    tx_number   := number of inputs 
    
    sel_vsize   := vectorsize for the selection counter for the n inputs
    
-----------
Limitations
-----------

    

--------------
Implementation
--------------
    
-----------
Limitations
-----------


----------------
Missing Features
----------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity priority_mux_ARB is

generic
(
    tx_number:      integer
);

port
(
    -- CMI Inputs
    ARB_tx_vld:     in  std_logic_vector(tx_number-1 downto 0);
    ARB_tx_next:    out std_logic_vector(tx_number-1 downto 0);
    -- CMI Output
    ARB_rx_vld:     out std_logic;
    ARB_rx_next:    in  std_logic;
    ARB_sel:        out std_logic_vector(get_vsize_addr(tx_number)-1 downto 0)
);

end entity priority_mux_ARB;


architecture struct of priority_mux_ARB is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant sel_vsize:     integer := get_vsize_addr(tx_number);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal ARB_sel_int:     UNSIGNED(sel_vsize-1 downto 0);
        
BEGIN

    
    --==============--
    -- COMBINATORIC --
    --==============--
    
    -- choose input --
    process(all) is
    begin
            
        for k in tx_number-1 downto 0 loop
            if ARB_tx_vld(k) = '1' then
                ARB_sel_int   <= TO_UNSIGNED(k, sel_vsize);
                ARB_rx_vld    <= '1';
            end if;
        end loop;
      
    end process;
    
    -- setting ARB_tx_next --  
    process(all) is
    begin
        --DEFAULTS
        for i in tx_number-1 downto 0 loop
            ARB_tx_next(i) <= NOT ARB_tx_vld(i);
        end loop;
        
        ARB_tx_next(TO_INTEGER(ARB_sel_int)) <= ARB_rx_next OR NOT ARB_tx_vld(TO_INTEGER(ARB_sel_int));
        
    end process;

    
    -- set ARB_sel output

    ARB_sel <= STD_LOGIC_VECTOR(ARB_sel_int);

    
    
end architecture struct;
