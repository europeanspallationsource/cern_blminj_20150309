------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/02/2014
Module Name:    ID_ADM

-----------------
Short Description
-----------------

    This module forwards data to the appropriate receiver depending on the ID. ID = 0 -> Receiver = 0 etc.
    
------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    tx_data_size        := size with ID
    rx_data_size        := size without ID
    rx_number           := number of receivers

--------------
Implementation
--------------

     
-----------
Limitations
-----------
    
    

----------------
Missing Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity ID_ADM is

generic
(
    tx_data_size:       integer;
    rx_data_size:       integer;
    rx_number:          integer
);

port
(
    ADM_tx_data:    in  std_logic_vector(tx_data_size-1 downto 0);
    ADM_rx_data:    out std_logic_vector(rx_data_size-1 downto 0);
    ADM_sel:        out std_logic_vector(get_vsize_addr(rx_number)-1 downto 0)
);

end entity ID_ADM;


architecture struct of ID_ADM is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant ID_LSB:    integer := rx_data_size;
    constant ID_MSB:    integer := rx_data_size + get_vsize_addr(rx_number)-1;
     
              
    
BEGIN
    
    ADM_sel     <= ADM_tx_data(ID_MSB downto ID_LSB);
    ADM_rx_data <= ADM_tx_data(rx_data_size-1 downto 0);
    
   
    
end architecture struct;
