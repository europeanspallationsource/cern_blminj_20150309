------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/10/2013
Module Name:    filter_1toN_DM_tmpl

-----------------
Short Description
-----------------

    TEMPLATE: Filters out data words with a certain tags and ignores the others (can be used for each output individually)

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    inp_data_size       := size of the input data with tag
    out_data_size       := size of the output data without tag
    rx_number           := number of receivers/outputs
    
    tag_size            := size of the tag inside the data bus

--------------
Implementation
--------------
    
    see short description
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity filter_1toN_DM_tmpl is

generic
(
    inp_data_size:      integer;
    out_data_size:      integer;
    rx_number:          integer
);

port
(
    -- Input
    DM_inp_data:    in  std_logic_vector(inp_data_size-1 downto 0);
    DM_inp_vld:     in  std_logic;
    -- Output
    DM_out_data:    in  slv_array(rx_number-1 downto 0)(out_data_size-1 downto 0);
    DM_out_vld:     in  std_logic_vector(rx_number-1 downto 0)
);

end entity filter_1toN_DM_tmpl;


architecture struct of filter_1toN_DM_tmpl is

    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal tag:     std_logic_vector(tag_size-1 downto 0);
    
BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    
    tag         <= DM_inp_data(inp_data_size-1 downto out_data_size);
    DM_out_data <= DM_inp_data(out_data_size-1 downto 0);
    
    process(all) is
    begin
    
        case tag is
            when "1"     =>  DM_out_vld(0) <= DM_inp_vld;
                             DM_out_vld(1) <= '0';
            when others  =>  DM_out_vld(0) <= DM_inp_vld;
                             DM_out_vld(1) <= DM_inp_vld;
        end case;
    
    end process;
    
    
    
end architecture struct;
