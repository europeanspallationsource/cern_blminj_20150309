------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/10/2013
Module Name:    filter_1to1_DM_tmpl

-----------------
Short Description
-----------------

    does something

------------
Dependencies
------------

    Filters out data words with a certain tags and ignores the others
    
------------------
Generics/Constants
------------------

    data_size           := size of the actual data
    tag_size            := size of the tag inside the data bus
  

--------------
Implementation
--------------
    
    see short description
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity filter_1to1_DM_tmpl is

generic
(
    data_size:      integer;
    tag_size:       integer
);

port
(
    -- Clock
    clk:            in  std_logic;

    -- Input
    DM_inp_data:    in  std_logic_vector(data_size+tag_size-1 downto 0);
    DM_inp_vld:     in  std_logic;
    -- Output
    DM_out_data:    in  std_logic_vector(data_size-1 downto 0);
    DM_out_vld:     in  std_logic
);

end entity filter_1to1_DM_tmpl;


architecture struct of filter_1to1_DM_tmpl is

    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal tag:     std_logic_vector(tag_size-1 downto 0);
    
BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    
    tag         <= DM_inp_data(data_size+tag_size-1 downto data_size);
    DM_out_data    <= DM_inp_data(data_size-1 downto 0);
    
    process(all) is
    begin
    
        case tag is
            when "0"     =>  DM_out_vld <= '1';
            when others  =>  DM_out_vld <= '0';
        end case;
    
    end process;
    
    
    
end architecture struct;
