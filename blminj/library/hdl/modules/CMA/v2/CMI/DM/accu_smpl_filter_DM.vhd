------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/10/2013
Module Name:    accu_smpl_filter_DM

-----------------
Short Description
-----------------

    Filters out the number of samples for the Threshold Comparator

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    tx_data_size       := size of the input data with tag
    rx_data_size       := size of the output data without tag
    rx_number           := number of receivers/outputs
    
    tag_size            := size of the tag inside the data bus

--------------
Implementation
--------------
    
    see short description
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity accu_smpl_filter_DM is

generic
(
    tx_data_size:      integer;
    rx_data_size:      integer;
    rx_number:         integer
);

port
(
    -- Input
    DM_tx_data:     in  std_logic_vector(tx_data_size-1 downto 0);
    DM_tx_vld:      in  std_logic;
    -- Output
    DM_rx_data:     out slv_array(rx_number-1 downto 0)(rx_data_size-1 downto 0);
    DM_rx_vld:      out std_logic_vector(rx_number-1 downto 0)
);

end entity accu_smpl_filter_DM;


architecture struct of accu_smpl_filter_DM is

    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal tag:     std_logic;
    
BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    
    tag             <= scalarize(DM_tx_data(tx_data_size-1 downto tx_data_size-1));
    
    -- forward data portion
    DM_rx_data(0)   <= DM_tx_data(rx_data_size-1 downto 0);
    DM_rx_vld(0)    <= '0' when tag = '1' else
                       DM_tx_vld;
    
    
end architecture struct;
