------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/10/2013
Module Name:    time_filter_full_DM

-----------------
Short Description
-----------------

    From a certain start point in time, passes a defined amount of data through.

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    data_size           := size of the input and output data
    data_n              := number of passed data values from triggering start
    
    data_vsize          := size of the data_cnt    

--------------
Implementation
--------------
    
    The state machine waits for the start flag. Afterwards for the next data_n inputs, it lets
    the input pass to the output. If there is a valid input available in the same cycle as the start flag,
    it is filtered out as well.
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity time_filter_full_DM is

generic
(
    rx_number:      integer;
    tx_data_size:   integer;
    rx_data_size:   integer;
    cnt_size:       integer;
    cnt_max:        integer
    
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Timing Pulse
    start:          in  std_logic;
    -- Data Used    
    used:           in  std_logic_vector(rx_number-1 downto 0);
    -- CNT
    CNT:            out std_logic_vector(cnt_size-1 downto 0);
    -- Input
    DM_tx_data:     in  std_logic_vector(tx_data_size-1 downto 0);
    DM_tx_vld:      in  std_logic;
    -- Output
    DM_rx_data:     out slv_array(rx_number-1 downto 0)(rx_data_size-1 downto 0);
    DM_rx_vld:      out std_logic_vector(rx_number-1 downto 0)
);

end entity time_filter_full_DM;


architecture struct of time_filter_full_DM is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    --======--
    -- FSMS --
    --======--
    
    type tf_state_t is (filtering, fwd);
    signal tf_present_state, tf_next_state: tf_state_t := filtering;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Slice Counter
    signal data_cnt:   UNSIGNED(cnt_size-1 downto 0) := TO_UNSIGNED(0, cnt_size);
    signal data_rst:   std_logic;
    signal data_inc:   std_logic;


BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    DM_rx_data(0) <= DM_tx_data;
    
    CNT <= STD_LOGIC_VECTOR(data_cnt);
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if data_rst= '1' then
                data_cnt <= TO_UNSIGNED(0, cnt_size);
            elsif data_inc = '1' then
                data_cnt <= data_cnt + 1;
            end if;
            
        end if;
    end process;
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            tf_present_state <= tf_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        DM_rx_vld(0)<= '0';
        data_inc    <= '0';
        data_rst    <= '0';
        
        case tf_present_state is
            
            --===============--
            when filtering =>
            --===============--
            
                if start = '1' then
                    tf_next_state <= fwd;
                else
                    tf_next_state <= filtering;
                end if;
           
            --===============--
            when fwd =>
            --===============--
            
                DM_rx_vld(0) <= DM_tx_vld;
                if DM_tx_vld = '1' AND used(0) = '1' then
                    if data_cnt = cnt_max-1 then
                        data_rst        <= '1';
                        tf_next_state   <= filtering;
                    else
                        data_inc        <= '1';
                        tf_next_state   <= fwd;
                    end if;
                else
                    tf_next_state <= fwd;
                end if;
            
            
        end case;
    end process;
    
    
    
    
end architecture struct;

