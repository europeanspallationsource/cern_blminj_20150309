------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    20/11/2013
Module Name:    truncate_DM

-----------------
Short Description
-----------------

    Truncates the input vector in some way.

------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

    tx_data_size    := size of the actual data
    rx_data_size    := size of the truncated output
    

--------------
Implementation
--------------
    
    see short description
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity truncate_DM is

generic
(
    rx_number:      integer;  
    tx_data_size:   integer;
    rx_data_size:   integer
);

port
(
    -- Input
    DM_tx_data:     in  std_logic_vector(tx_data_size-1 downto 0);
    DM_tx_vld:      in  std_logic;
    -- Output
    DM_rx_data:     out slv_array(rx_number-1 downto 0)(rx_data_size-1 downto 0);
    DM_rx_vld:      out std_logic_vector(rx_number-1 downto 0)
);

end entity truncate_DM;


architecture struct of truncate_DM is

    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    
BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    DM_rx_vld(0)  <= DM_tx_vld;
    DM_rx_data(0) <= DM_tx_data(rx_data_size-1 downto 0);
   
    
    
    
end architecture struct;
