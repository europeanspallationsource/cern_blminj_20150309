------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/10/2013
Module Name:    time_filter_DM

-----------------
Short Description
-----------------

    Depending on an input called "blocked" the input data is not forwarded (lost) during that time.
    
------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    rx_number           := number of CMI receivers
    tx_data_size        := input data size
    rx_data_size        := output data size
    
--------------
Implementation
--------------
    

-----------
Limitations
-----------
    
    Possibly filters the last valid data before switching to blocked, if rx_next was 0 beforehand.

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity time_filter_DM is

generic
(
    rx_number:      integer;
    tx_data_size:   integer;
    rx_data_size:   integer
);

port
(
    -- Timing
    blocked:        in  std_logic;
    -- Input
    DM_tx_data:     in  std_logic_vector(tx_data_size-1 downto 0);
    DM_tx_vld:      in  std_logic;
    -- Output
    DM_rx_data:     out slv_array(rx_number-1 downto 0)(rx_data_size-1 downto 0);
    DM_rx_vld:      out std_logic_vector(rx_number-1 downto 0)
);

end entity time_filter_DM;


architecture struct of time_filter_DM is

BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    DM_rx_data(0)   <= DM_tx_data(rx_data_size-1 downto 0);
    DM_rx_vld(0)    <= DM_tx_vld when blocked = '0' else '0';
    
    
end architecture struct;

