------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    15/10/2013
Module Name:    data_splitter_DM

-----------------
Short Description
-----------------

    splits the one data word into parts for the different outputs

------------
Dependencies
------------

    none
    
------------------
Generics/Constants
------------------

    tx_data_size        := size of the input data
    rx_data_size        := size of the multiple output data
    rx_number           := number of receivers/outputs
   

--------------
Implementation
--------------
    
    see short description
    

-----------
Limitations
-----------
    
    none

----------------
Missing Features
----------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity data_splitter_DM is

generic
(
    tx_data_size:       integer;
    rx_data_size:       integer;
    rx_number:          integer
    
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- Std Input
    DM_tx_data:     in  std_logic_vector(tx_data_size-1 downto 0);
    DM_tx_vld:      in  std_logic;
    -- Std Output
    DM_rx_data:     out slv_array(rx_number-1 downto 0)(rx_data_size-1 downto 0);
    DM_rx_vld:      out std_logic_vector(rx_number-1 downto 0)
);

end entity data_splitter_DM;


architecture struct of data_splitter_DM is
    
    
BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    gen_outputs: 
    for i in 0 to rx_number-1 generate
        
        DM_rx_data(i)   <= DM_tx_data(rx_data_size*(i+1)-1 downto rx_data_size*i);
        DM_rx_vld(i)    <= DM_tx_vld;
        
    end generate;

    
    
end architecture struct;

