------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        01/04/2013
Module Name:    MM_CMI


-----------
Description
-----------

    This is the "MultiMaster" version of the CMA Interconnect. It can be used for m:1 and m:n broadcast setups. An external module has to be connected,
    that implements the requested arbitration scheme.

------------------
Generics/Constants
------------------

    
    rx_number       := # of connected receiver modules
    tx_number       := # of connected transmitter(Master) modules
    data_size       := size of transported data (incl. optional tags)

----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
 
-----------------
Necessary Modules
-----------------

    none

---------------
Necessary Cores
---------------

    none 
  
  
--------------
Implementation
--------------

    This IC uses the Invalid Filter(IF), the Buffer (BUF), the Arbiter (ARB), the Delayed Next (DNEXT) and optionally the Broadcast (BC) extension.


------------------------
Not implemented Features
------------------------

    none
    
    
*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
	use work.vhdl_func_pkg.all;
    
entity MM_CMI is

generic
(
    data_size:      integer;
    tx_number:      integer;
    rx_number:      integer
	 
);

port
(
    -- Clocks
    clk:            in  std_logic;
    -- CMI Transmitter
    tx_data:        in  slv_array (tx_number-1 downto 0)(data_size-1 downto 0);
    tx_vld:         in  std_logic_vector(tx_number-1 downto 0);
    tx_next:        out std_logic_vector(tx_number-1 downto 0);
    -- CMI Receivers
    rx_data:        out slv_array (rx_number-1 downto 0)(data_size-1 downto 0);
    rx_vld:         out std_logic_vector(rx_number-1 downto 0);
    rx_next:        in  std_logic_vector(rx_number-1 downto 0);
	-- ARB Connections
    ARB_tx_vld:     out std_logic_vector(tx_number-1 downto 0);
    ARB_tx_next:    in  std_logic_vector(tx_number-1 downto 0);
    ARB_rx_vld:     in  std_logic;
    ARB_rx_next:    out std_logic;
    ARB_sel:        in  std_logic_vector(get_vsize_addr(tx_number)-1 downto 0)
	
);

end entity MM_CMI;


architecture struct of MM_CMI is
    
    --==================--
    -- INTERNAL SIGNALS --
    --==================--
    
    -- Internal Signals
    signal data_int:        std_logic_vector(data_size-1 downto 0);
    signal next_int:        std_logic_vector(tx_number-1 downto 0);
    
    -- ARB
    signal ARB_rx_data:     std_logic_vector(data_size-1 downto 0);
    
    -- BUF
    signal vld_bufd:        std_logic := '0';
    signal data_bufd:       std_logic_vector(data_size-1 downto 0);
    
    -- BC
    signal vld_int:         std_logic;
    signal vld_appr:        std_logic_vector(rx_number-1 downto 0);
    
    -- DNEXT
    signal next_reg:        std_logic_vector(rx_number-1 downto 0) := (others => '1');
    signal ARB_rx_next_int: std_logic;
    
    
    signal ARB_sel_int:     UNSIGNED(get_vsize_addr(tx_number)-1 downto 0);
BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    RXDATA:
    for i in 0 to rx_number-1 generate   
        rx_data(i) <= data_int;
    end generate;
    
    ARB_rx_next <= ARB_rx_next_int;
    
    --================--
    -- INVALID FILTER --
    --================--
    
    InvFilter: 
    for i in 0 to tx_number-1 generate
        tx_next(i) <= NOT tx_vld(i) OR next_int(i);
    end generate;
    
    --=========--
    -- ARBITER --
    --=========--
    
    -- ARB I/O
    next_int    <= ARB_tx_next;
    ARB_tx_vld  <= tx_vld;
    
    -- next output (MUX)
    
    --ARB_rx_data <= tx_data(TO_INTEGER(UNSIGNED(ARB_sel)));

    
--    process (all) is
--      variable tmp : std_logic_vector(ARB_rx_data'range);
--    begin
--      tmp := (others => '0');
--      for I in 0 to tx_number-1 loop
--        if I = TO_INTEGER(UNSIGNED(ARB_sel)) then
--          tmp := tmp or tx_data(I);
--        end if;
--      end loop;
--      ARB_rx_data <= tmp;
--    end process;
--        
    
    ARB_rx_data <= eff_mux(tx_data, tx_number, ARB_sel, data_size);
    
    
    --========--
    -- BUFFER --
    --========--
    

    process(clk) is
    begin
        if rising_edge(clk) then
            if ARB_rx_next_int = '1' then
                vld_bufd  <= ARB_rx_vld;
                data_bufd <= ARB_rx_data;
            end if;
        end if;
    end process;
            
    vld_int     <= ARB_rx_vld   when ARB_rx_next_int ='1' else vld_bufd;
    data_int    <= ARB_rx_data  when ARB_rx_next_int ='1' else data_bufd;
        
    
    --===========--
    -- BROADCAST --
    --===========--
    
    multiple_recv: 
    if rx_number > 1 generate
    
        ARB_rx_next_int <= AND_REDUCE(next_reg); -- combine next signals
            
        rx_vld_appr: 
        for i in 0 to rx_number-1 generate
            vld_appr(i) <= NOT next_reg(i) OR ARB_rx_next_int;
            rx_vld(i)   <= vld_appr(i) AND vld_int;
        end generate;
        
    end generate;
        
    one_recv: 
    if rx_number = 1 generate
        rx_vld(0)       <= vld_int;
        ARB_rx_next_int <= next_reg(0);
    end generate;
    
    
    --==============--
    -- DELAYED NEXT --
    --==============--

    DNEXT: 
    for i in 0 to rx_number-1 generate
        process(clk) is
        begin
            if rising_edge(clk) then
                next_reg(i) <= rx_next(i);
            end if;
        end process;
    end generate;
        

    
end architecture struct;
