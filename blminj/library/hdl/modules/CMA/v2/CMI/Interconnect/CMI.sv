/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/04/2012
Interface Name:   CMI
Description:

(Synchronized) Common Modular Interconnect

Parameter Description:

rx_number 		:= # of connected receiver modules
data_size 		:= bit size of the transported data
outreg_incl 	:= include the TX output register inside the Interconnect

Use Case:

This Interconnect delays the next-signal by one CC (thats the reason for the buffer).
This leads in most of the cases to a better clock timing(no combinatorial next-signal), but cost more space in terms of the buffer.

*/


interface CMI
#(
	parameter rx_number 	= 1,
	parameter data_size 	= 32,
	parameter outreg_incl	= 1,
)
(
	input clk
);
	
	///////////////
	// Interface //
	///////////////
	
	// CMI Transmitter
	logic 					tx_next;
	logic 					tx_vld;
	logic [data_size-1:0]	tx_data;
	// CMI Receiver
	logic [rx_number-1:0]	rx_next;
	logic [rx_number-1:0]	rx_vld;
	logic [data_size-1:0]	rx_data;
	
	///////////////////
	// TX Output Reg //
	///////////////////
	
	logic 					tx_vld_reg;
	logic [data_size-1:0]	tx_data_reg;
	
	///////////////////
	// Basic Signals //
	///////////////////
	
	logic [rx_number-1:0]	next_reg;
	logic					next_comb;
	
	logic					vld_int;
	logic [rx_number-1:0]	vld_appr;
	
	logic 					vld_bufd;
	logic [data_size-1:0]	data_bufd;

	
	// Generator Variable
	genvar i;
	
	generate
				
		/////////////////////
		// OUTPUT REGISTER //
		/////////////////////

		if (outreg_incl)
		begin
		
			always @(posedge clk)
				if (tx_next)
				begin
					tx_vld_reg 	<= tx_vld;
					tx_data_reg	<= tx_data;
				end
		end else
		begin
			
			assign tx_vld_reg 	= tx_vld;
			assign tx_data_reg	= tx_data;
		
		end
			
		////////////////////
		// INVALID FILTER //
		////////////////////
		
		assign tx_next = ~tx_vld | next_comb;
		
		////////////////////////
		// BUFFER/MULTIPLEXER //
		////////////////////////
			
		always @(posedge clk)
			if (next_comb)
			begin
				vld_bufd 	<= tx_vld_reg;
				data_bufd 	<= tx_data_reg;
			end
			
		assign vld_int 	= next_comb ? tx_vld_reg 	: vld_bufd;
		assign rx_data  = next_comb ? tx_data_reg 	: data_bufd;
		
		////////////////////
		// Next Registers //
		////////////////////
		
		for (i=0; i<rx_number; i++)
		begin : NextRegs 
			always @(posedge clk)
					next_reg[i] <= rx_next[i];
		end
		
		///////////////
		// RECEIVERS //
		///////////////
		
		if (rx_number > 1)
		begin
		
			assign next_comb = & next_reg; // combine next signals
			
			for (i=0; i<rx_number; i++) // generate vld approval solution
			begin : rx_vld_appr
				assign vld_appr[i] = ~next_reg[i] | next_comb;
				assign rx_vld[i] = vld_appr & vld_int;
			end
			
		end	else 
		begin
		
			assign rx_vld[0] = vld_int;
			assign next_comb = next_reg[0];
		
		end
			
				
	endgenerate
	
	////////////////
	// TX modport //
	////////////////
	
	// external modport name: inst.TX
	modport TX (input tx_next, output tx_vld, tx_data); 
	
	////////////////
	// RX modport //
	////////////////
	
	// external modport name: inst.Recv[i].RX
	generate
		for (i=0; i<rx_number; i++)
		begin : Recv
			modport RX(input rx_data, input .rx_vld(rx_vld[i]), output .rx_next(rx_next[i])); 
		end : Recv
	endgenerate
	
endinterface
