------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        01/04/2013
Module Name:    T_CMI


-----------
Description
-----------

    This is the "Target" Version of the CMA Interconnect. It can be used for 1:n addressed setups. An external module has to be connected,
    that implements the requested address decoder/mapper.

------------------
Generics/Constants
------------------

    
    rx_number       := # of connected receiver modules
    tx_data_size    := size of the tx_data (going into the ADM)
    rx_data_size    := size of the rx_data (coming out of the ADM)
    

----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg

    
-----------------
Necessary Modules
-----------------

    none

---------------
Necessary Cores
---------------

    none 
  
  
--------------
Implementation
--------------

    This IC uses the Invalid Filter(IF), the Buffer (BUF), the Address Decoder (AD) and the Delayed Next (DNEXT) extension.


------------------------
Not implemented Features
------------------------

    none
    
    
*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
	use work.vhdl_func_pkg.all;
    
entity T_CMI is

generic
(
    tx_data_size:       integer;
    rx_data_size:       integer;
    rx_number:          integer
	 
);

port
(
    -- Clocks
    clk:            in  std_logic;
    -- CMI Transmitter
    tx_data:        in  std_logic_vector(tx_data_size-1 downto 0);
    tx_vld:         in  std_logic;
    tx_next:        out std_logic;
    -- CMI Receivers
    rx_data:        out slv_array (rx_number-1 downto 0)(rx_data_size-1 downto 0);
    rx_vld:         out std_logic_vector(rx_number-1 downto 0);
    rx_next:        in  std_logic_vector(rx_number-1 downto 0);
	-- ADM Decoder
	ADM_tx_data:    out std_logic_vector(tx_data_size-1 downto 0);
    ADM_rx_data:    in  std_logic_vector(rx_data_size-1 downto 0);
    ADM_sel:        in  std_logic_vector(get_vsize_addr(rx_number)-1 downto 0)
);

end entity T_CMI;


architecture struct of T_CMI is
    
    --==================--
    -- INTERNAL SIGNALS --
    --==================--

    
    -- Internal Signals
    signal vld_int:         std_logic_vector(rx_number-1 downto 0);
    signal next_int:        std_logic;
    
    -- BUF
    signal vld_bufd:        std_logic_vector(rx_number-1 downto 0) := (rx_number-1 downto 0 => '0');
    signal data_bufd:       slv_array (rx_number-1 downto 0)(rx_data_size-1 downto 0);
    
    -- DNEXT
    signal next_reg:        std_logic_vector(rx_number-1 downto 0) := (others => '1');
    
    
BEGIN
    
    --================--
    -- INVALID FILTER --
    --================--
    
    tx_next     <= NOT tx_vld OR next_int;
    
    --=======================--
    -- ADDRESS DECODER MAPPER--
    --=======================--
    
    -- ADM Input
    ADM_tx_data <= tx_data;
    
    -- next output (MUX)
    next_int <= next_reg(TO_INTEGER(UNSIGNED(ADM_sel)));

    -- vld outputs (DEMUX)
    process(all)
    begin
        for k in rx_number-1 downto 0 loop
            vld_int(k) <= '0';
        end loop;
        
        vld_int(TO_INTEGER(UNSIGNED(ADM_sel))) <= tx_vld;
        
    end process;
    
    --========--
    -- BUFFER --
    --========--
    
    BUF: 
    for i in 0 to rx_number-1 generate
        
        process(clk) is
        begin
            if rising_edge(clk) then
                if next_reg(i) = '1' then
                    vld_bufd(i)  <= vld_int(i);
                    data_bufd(i) <= ADM_rx_data;
                end if;
            end if;
        end process;
            
        rx_vld(i)   <= vld_int(i)   when next_reg(i) ='1' else vld_bufd(i);
        rx_data(i)  <= ADM_rx_data  when next_reg(i) ='1' else data_bufd(i);
        
    end generate;
    
    
    --==============--
    -- DELAYED NEXT --
    --==============--

    DNEXT: 
    for i in 0 to rx_number-1 generate
        process(clk) is
        begin
            if rising_edge(clk) then
                next_reg(i) <= rx_next(i);
            end if;
        end process;
    end generate;
        

    
end architecture struct;
