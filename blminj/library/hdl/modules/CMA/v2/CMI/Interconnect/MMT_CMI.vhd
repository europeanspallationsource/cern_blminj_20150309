------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Updated::       01/04/2013
Module Name:    MMT_CMI


-----------
Description
-----------

    This is the "MultiMaster Target" Version of the CMI Interconnect. It can be used for m:n addressed setups. Two external module have to be connected,
    one that implements the requested arbitration scheme and another that implements the requested address decoder/mapper.

--------
Generics
--------

    
    rx_number       := # of connected receiver modules
    tx_number       := # of connected transmitter(Master) modules
    data_size       := the size of the data part ONLY (incl. optional tags)
    ADM_addr_size   := the size of the ADM_addr part that determines the requested slave (have to be the topmost bits of the tx_data lines)

----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
    
--------------
Implementation
--------------

    This IC uses the Invalid Filter(IF), the Buffer (BUF), the Arbiter (ARB), the Address Decoder (AD) and the Delayed Next (DNEXT) extension.



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
	use work.vhdl_func_pkg.all;
    
entity MMT_CMI is

generic
(
    tx_data_size:       integer;
    rx_data_size:       integer;
    tx_number:          integer;
    rx_number:          integer
    
	 
);

port
(
    -- Clocks
    clk:            in  std_logic;
    -- CMI Transmitter
    tx_data:        in  slv_array (tx_number-1 downto 0)(tx_data_size-1 downto 0);
    tx_vld:         in  std_logic_vector(tx_number-1 downto 0);
    tx_next:        out std_logic_vector(tx_number-1 downto 0);
    -- CMI Receivers
    rx_data:        out slv_array (rx_number-1 downto 0)(rx_data_size-1 downto 0);
    rx_vld:         out std_logic_vector(rx_number-1 downto 0);
    rx_next:        in  std_logic_vector(rx_number-1 downto 0);
	-- ARB Connections
    ARB_tx_next:    in  std_logic_vector(tx_number-1 downto 0);
    ARB_tx_vld:     out std_logic_vector(tx_number-1 downto 0);
	ARB_rx_next:    out std_logic;
    ARB_rx_vld:     in  std_logic;
    ARB_sel:        in  std_logic_vector(get_vsize_addr(tx_number)-1 downto 0);
    -- AD Decoder
	ADM_tx_data:    out std_logic_vector(tx_data_size-1 downto 0);
    ADM_rx_data:    in  std_logic_vector(rx_data_size-1 downto 0);
    ADM_sel:        in  std_logic_vector(get_vsize_addr(rx_number)-1 downto 0)
	
);

end entity MMT_CMI;


architecture struct of MMT_CMI is
    
    --==================--
    -- INTERNAL SIGNALS --
    --==================--
    
    -- Internal Signals
    signal vld_int:         std_logic_vector(rx_number-1 downto 0);
    signal next_int:        std_logic_vector(tx_number-1 downto 0);
    
    -- ARB
    signal ARB_rx_data:     std_logic_vector(tx_data_size-1 downto 0);

    -- BUF
    signal vld_bufd:        std_logic_vector(rx_number-1 downto 0) := (rx_number-1 downto 0 => '0');
    signal data_bufd:       slv_array (rx_number-1 downto 0)(rx_data_size-1 downto 0);
    
    -- DNEXT
    signal next_reg:        std_logic_vector(rx_number-1 downto 0) := (others => '1');
    
BEGIN

    
    --================--
    -- INVALID FILTER --
    --================--
    
    InvFilter: 
    for i in 0 to tx_number-1 generate
        tx_next(i) <= NOT tx_vld(i) OR next_int(i);
    end generate;
    
    --=========--
    -- ARBITER --
    --=========--
    
    -- ARB I/O
    tx_next     <= ARB_tx_next;
    ARB_tx_vld  <= tx_vld;
    
    -- next output (MUX)
    
    ARB_rx_data <= tx_data(TO_INTEGER(UNSIGNED(ARB_sel)));

    
    --=======================--
    -- ADDRESS DECODER MAPPER--
    --=======================--
    
    -- ADM Input
    ADM_tx_data <= ARB_rx_data;
    
    -- next output (MUX)
    
    ARB_rx_next <= next_reg(TO_INTEGER(UNSIGNED(ADM_sel)));

    -- vld outputs (DEMUX)
    
    process(all)
    begin
        for k in rx_number-1 downto 0 loop
            vld_int(k) <= '0';
        end loop;
        
        vld_int(TO_INTEGER(UNSIGNED(ADM_sel))) <= ARB_rx_vld;
        
    end process;
    
    --========--
    -- BUFFER --
    --========--
    
    BUF: 
    for i in 0 to rx_number-1 generate
        
        buf: process(clk) is
        begin
            if rising_edge(clk) then
                if next_reg(i) = '1' then
                    vld_bufd(i)  <= vld_int(i);
                    data_bufd(i) <= ADM_rx_data;
                end if;
            end if;
        end process;
            
        rx_vld(i)   <= vld_int(i)   when next_reg(i) ='1' else vld_bufd(i);
        rx_data(i)  <= ADM_rx_data  when next_reg(i) ='1' else data_bufd(i);
        
    end generate;
    
    
    --==============--
    -- DELAYED NEXT --
    --==============--

    DNEXT: 
    for i in 0 to rx_number-1 generate
        process(clk) is
        begin
            if rising_edge(clk) then
                next_reg(i) <= rx_next(i);
            end if;
        end process;
    end generate;

    
end architecture struct;
