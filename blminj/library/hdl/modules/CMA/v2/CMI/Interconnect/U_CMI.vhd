------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Updated:        01/04/2013
Module Name:    U_CMI

-----------
Description
-----------

    This is the "Undelayed" version of the CMA Interconnect. It can be used for 1:1 and 1:n broadcast setups.

--------
Generics
--------

    
    rx_number       := # of connected receiver modules
    data_size       := size of transported data (incl. optional tags)

----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
 
    
--------------
Implementation
--------------

    This IC uses the Invalid Filter(IF) and optionally the Broadcast (BC) extension.

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
    
entity U_CMI is

generic
(
    data_size:      integer;
    rx_number:      integer := 1
);

port(
    -- Clocks
    clk:            in  std_logic;
    -- CMI Transmitter
    tx_data:        in  std_logic_vector(data_size-1 downto 0);
    tx_vld:         in  std_logic;
    tx_next:        out std_logic;
    -- CMI Receivers
    rx_data:        out slv_array (rx_number-1 downto 0)(data_size-1 downto 0);
    rx_vld:         out std_logic_vector(rx_number-1 downto 0);
    rx_next:        in  std_logic_vector(rx_number-1 downto 0)
);

end entity U_CMI;


architecture struct of U_CMI is
    
    
    
    --===============--
    -- Basic Signals --
    --===============--
    
    signal next_int:       std_logic;
    
    signal vld_appr:        std_logic_vector(rx_number-1 downto 0);
    signal vld_appr_reg:    std_logic_vector(rx_number-1 downto 0);
    
    
BEGIN
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    RXDATA:
    for i in 0 to rx_number-1 generate   
        rx_data(i) <= tx_data;
    end generate;
    
    
    --================--
    -- INVALID FILTER --
    --================--
    
    tx_next <= NOT tx_vld OR next_int;

    
    --===========--
    -- RECEIVERS --
    --===========--
    
    multiple_recv: 
    if rx_number > 1 generate
    
        next_int <= AND_REDUCE(rx_next); -- combine next signals
            
        rx_vld_appr: 
        for i in 0 to rx_number-1 generate
            vld_appr(i) <= NOT rx_next(i) OR next_int;
            rx_vld(i)   <= vld_appr_reg(i) AND tx_vld;
        end generate;
        
        process(clk) is
        begin
            if rising_edge(clk) then
                vld_appr_reg <= vld_appr;
            end if;
        end process;
        
        
    end generate;
        
    one_recv: 
    if rx_number = 1 generate
        rx_vld(0) <= tx_vld;
        next_int <= rx_next(0);
    end generate;
    
    
end architecture struct;