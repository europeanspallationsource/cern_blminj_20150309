------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Updated:        01/04/2013
Module Name:    S_CMI

-----------
Description
-----------

    This is the "Synchronized" version of the CMA Interconnect. It can be used for 1:1 clock-domain crossing. The tx_clk is the clock region of the transmitter
    and the rx_clk is the clock region of the receiver.


--------
Generics
--------

    data_size       := bit size of the transported data
    sync_steps      := # of synchronisation registers


----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
    
--------------
Implementation
--------------




*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

   
entity S_CMI is

generic
(
    data_size:      integer;
    sync_steps:     integer := 2
);

port(
    -- Clocks
    tx_clk:         in std_logic;
    rx_clk:         in std_logic;
    -- CMI Transmitter
    tx_data:        in std_logic_vector(data_size-1 downto 0);
    tx_vld:         in std_logic;
    tx_next:        out std_logic;
    -- CMI Receivers
    rx_data:        out std_logic_vector(data_size-1 downto 0);
    rx_vld:         out std_logic;
    rx_next:        in std_logic
);

end entity S_CMI;


architecture struct of S_CMI is
    
    --===============--
    -- Basic Signals --
    --===============--
    
    signal data_bufd:       std_logic_vector(data_size-1 downto 0);
    
    --==============--
    -- Sync Signals --
    --==============--
    
    signal tx_tag:          std_logic := '0';
    signal tx_tag_en:       std_logic;
    signal tx_tag_comp:     std_logic;
    signal tx_tag_synced:   std_logic_vector(sync_steps-1 downto 0) := (others => '0');
    
    signal rx_tag:          std_logic := '0';
    signal rx_tag_en:       std_logic;
    signal rx_tag_comp:     std_logic;
    signal rx_tag_synced:   std_logic_vector(sync_steps-1 downto 0) := (others => '0');
    
    signal tx_next_sig:     std_logic;
    
BEGIN
    
    --================--
    -- INVALID FILTER --
    --================--

    tx_next <= NOT tx_vld OR tx_tag_comp;
    
    --===============--
    -- Sync Protocol --
    --===============--

    -- Buffer
    process(tx_clk) is
    begin
        if rising_edge(tx_clk) then
            if tx_tag_comp = '1' then
                data_bufd <= tx_data;
            end if;
        end if;
    end process;

    -- RX Assignments
    rx_data     <= data_bufd;
    rx_vld      <= rx_tag_comp;
        
    -- TX Tag
    tx_tag_comp <= rx_tag_synced(sync_steps-1) XNOR tx_tag;
    tx_tag_en   <= tx_vld AND tx_tag_comp;
        
    process(tx_clk) is
    begin
        if rising_edge(tx_clk) then
            if tx_tag_en = '1' then
                tx_tag <= NOT tx_tag;
            end if;
        end if;
    end process;
        
    -- RX Tag
    rx_tag_en   <= rx_next AND rx_tag_comp;
    rx_tag_comp <= tx_tag_synced(sync_steps-1) XOR rx_tag;
    
    process(rx_clk) is
    begin
        if rising_edge(rx_clk) then
            if rx_tag_en = '1' then
                rx_tag <= NOT rx_tag;
            end if;
        end if;
    end process;
        
        
    -- Synchronisation (both ways)
        
    process(tx_clk) is
    begin
        if rising_edge(tx_clk) then
            rx_tag_synced <= rx_tag_synced(sync_steps-2 downto 0) & rx_tag;
        end if;
    end process;
        
    process(rx_clk) is
    begin
        if rising_edge(rx_clk) then
            tx_tag_synced <= tx_tag_synced(sync_steps-2 downto 0) & tx_tag;
        end if;
    end process;
    
    
    
    
end architecture struct;