------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Updated:        01/04/2013
Module Name:    S_CMI_TX

-----------
Description
-----------

    This is the TX side of the synchronisation CMI.


--------
Generics
--------

    data_size       := bit size of the transported data
    sync_steps      := # of synchronisation registers for the rx_tag


----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
    
--------------
Implementation
--------------




*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

   
entity S_CMI_TX is

generic(
    data_size:      integer := 32;
    sync_steps:     integer := 2
);

port(
    -- Clocks
    tx_clk:         in std_logic;
    -- CMI Transmitter
    tx_data:        in std_logic_vector(data_size-1 downto 0);
    tx_vld:         in std_logic;
    tx_next:        out std_logic;
    -- Connections
    scmi_tx_data:   out std_logic_vector(data_size-1 downto 0);
    scmi_tx_tag:    out std_logic;
    scmi_rx_tag:    in  std_logic
);

end entity S_CMI_TX;


architecture struct of S_CMI_TX is
    
    --===============--
    -- Basic Signals --
    --===============--
    
    signal data_bufd:       std_logic_vector(data_size-1 downto 0);
    
    --==============--
    -- Sync Signals --
    --==============--
    
    signal tx_tag:          std_logic := '0';
    signal tx_tag_en:       std_logic;
    signal tx_tag_comp:     std_logic;
    signal rx_tag_synced:   std_logic_vector(sync_steps-1 downto 0) := (others => '0');
    
    
BEGIN
    
    --================--
    -- INVALID FILTER --
    --================--

    tx_next <= NOT tx_vld OR tx_tag_comp;
    
    --===============--
    -- Sync Protocol --
    --===============--

    -- Buffer
    process(tx_clk) is
    begin
        if rising_edge(tx_clk) then
            if tx_tag_comp = '1' then
                data_bufd <= tx_data;
            end if;
        end if;
    end process;

    -- RX Assignments
    scmi_tx_data    <= data_bufd;
    scmi_tx_tag     <= tx_tag;
    -- TX Tag
    tx_tag_comp     <= rx_tag_synced(sync_steps-1) XNOR tx_tag;
    tx_tag_en       <= tx_vld AND tx_tag_comp;
        
    process(tx_clk) is
    begin
        if rising_edge(tx_clk) then
            if tx_tag_en = '1' then
                tx_tag <= NOT tx_tag;
            end if;
        end if;
    end process;
        
        
    -- Synchronisation
        
    process(tx_clk) is
    begin
        if rising_edge(tx_clk) then
            rx_tag_synced <= rx_tag_synced(sync_steps-2 downto 0) & scmi_rx_tag;
        end if;
    end process;
        
  
    
    
end architecture struct;