------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/10/2013
Module Name:    DM_CMI


-----------
Description
-----------

    This is the "Data Manipulation" version of the CMA Interconnect. It can be used for 1:1 and 1:n broadcast setups.
    It includes the connection to an external Data Manipulation module. This should be used for primarily combinatorial changes
    of the data itself or the validity of the data.

------------------
Generics/Constants
------------------

    
    rx_number       := # of connected receiver modules
    data_size       := size of transported data (incl. optional tags)

----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
 
-----------------
Necessary Modules
-----------------

    none

---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

    This IC uses the Invalid Filter(IF), the Buffer (BUF), the Delayed Next (DNEXT) and optionally the Broadcast (BC) extension.
    
    
------------------------
Not implemented Features
------------------------

    none

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
	use work.vhdl_func_pkg.all;
    
entity DM_CMI is

generic
(
    tx_data_size:   integer := 34;
    rx_data_size:   integer := 32;
    rx_number:      integer := 1
);

port
(
    -- Clocks
    clk:            in  std_logic;
    -- CMI Transmitter
    tx_data:        in  std_logic_vector(tx_data_size-1 downto 0);
    tx_vld:         in  std_logic;
    tx_next:        out std_logic;
    -- CMI Receivers
    rx_data:        out slv_array (rx_number-1 downto 0)(rx_data_size-1 downto 0);
    rx_vld:         out std_logic_vector(rx_number-1 downto 0);
    rx_next:        in  std_logic_vector(rx_number-1 downto 0);
    -- DM Connection
    DM_tx_data:     out std_logic_vector(tx_data_size-1 downto 0);
    DM_tx_vld:      out std_logic;
    DM_rx_data:     in  slv_array(rx_number-1 downto 0)(rx_data_size-1 downto 0);
    DM_rx_vld:      in  std_logic_vector(rx_number-1 downto 0)
);

end entity DM_CMI;


architecture struct of DM_CMI is
    
    --==================--
    -- INTERNAL SIGNALS --
    --==================--
    
    -- Internal Signals
    signal next_int:        std_logic;
    
    -- BUF
    signal vld_bufd:        std_logic := '0';
    signal data_bufd:       std_logic_vector(tx_data_size-1 downto 0);
    
    -- BC
    signal vld_appr:        std_logic_vector(rx_number-1 downto 0);
    
    -- DNEXT
    signal next_reg:        std_logic_vector(rx_number-1 downto 0) := (others => '1');
    
BEGIN

    --===============--
    -- COMBINATORICS --
    --===============--
    
    RXDATA:
    for i in 0 to rx_number-1 generate   
        rx_data(i) <= DM_rx_data(i);
    end generate;
      
    --================--
    -- INVALID FILTER --
    --================--
    
    tx_next <= NOT tx_vld OR next_int;

    
    --========--
    -- BUFFER --
    --========--
    

        process(clk) is
        begin
            if rising_edge(clk) then
                if next_int = '1' then
                    vld_bufd  <= tx_vld;
                    data_bufd <= tx_data;
                end if;
            end if;
        end process;
            
        DM_tx_vld     <= tx_vld   when next_int ='1' else vld_bufd;
        DM_tx_data    <= tx_data  when next_int ='1' else data_bufd;
        
    
    --===========--
    -- BROADCAST --
    --===========--
    
    multiple_recv: 
    if rx_number > 1 generate
    
        next_int <= AND_REDUCE(next_reg); -- combine next signals
            
        rx_vld_appr: 
        for i in 0 to rx_number-1 generate
            vld_appr(i) <= NOT next_reg(i) OR next_int;
            rx_vld(i)   <= vld_appr(i) AND DM_rx_vld(i);
        end generate;
        
    end generate;
        
    one_recv: 
    if rx_number = 1 generate
        rx_vld(0)   <= DM_rx_vld(0);
        next_int    <= next_reg(0);
    end generate;
    
    
    --==============--
    -- DELAYED NEXT --
    --==============--

    DNEXT: 
    for i in 0 to rx_number-1 generate
        process(clk) is
        begin
            if rising_edge(clk) then
                next_reg(i) <= rx_next(i);
            end if;
        end process;
    end generate;
        

    
end architecture struct;