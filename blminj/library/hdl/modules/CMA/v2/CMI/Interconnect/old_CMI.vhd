------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    15/05/2012
Module Name:    old_CMI


-----------
Description
-----------

    This is a general interconnect usable between all kinds of modules, as long as they obey the handshake rules for the old_CMI protocol.

--------
Generics
--------

    
    rx_number       := # of connected receiver modules
    data_size       := bit size of the transported data
    outreg_incl     := include the TX output register inside the Interconnect

 
-----------
Limitations
-----------

    none
    
----------------------------
Necessary Packages/Libraries
----------------------------
    
    none
 
-----------------
Necessary Modules
-----------------

    none

---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    Notes: 
    
    This Interconnect delays the next-signal by one CC (thats the reason for the buffer).
    This leads in most of the cases to a better clock timing(no combinatorial next-signal), but cost more space in terms of the buffer.

------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    
entity old_CMI is

generic
(
    rx_number:      integer := 1;
    data_size:      integer := 32;
    outreg_incl:    integer := 0
);

port
(
    -- Clocks
    clk:        in std_logic;
    -- CMI Transmitter
    tx_next:    out std_logic;
    tx_vld:     in std_logic;
    tx_data:    in std_logic_vector(data_size-1 downto 0);
    -- CMI Receivers
    rx_next:    in std_logic_vector(rx_number-1 downto 0);
    rx_vld:     out std_logic_vector(rx_number-1 downto 0);
    rx_data:    out std_logic_vector(data_size-1 downto 0)
);

end entity old_CMI;


architecture struct of old_CMI is
    
    
    --===============--
    -- TX Output Reg --
    --===============--
    
    signal tx_vld_reg:      std_logic := '0';
    signal tx_data_reg:     std_logic_vector(data_size-1 downto 0);
    
    --===============--
    -- Basic Signals --
    --===============--
    
    signal next_reg:        std_logic_vector(rx_number-1 downto 0) := (others => '1');
    signal next_comb:       std_logic;
    
    signal vld_int:         std_logic;
    signal vld_appr:        std_logic_vector(rx_number-1 downto 0);
    
    signal vld_bufd:        std_logic := '0';
    signal data_bufd:       std_logic_vector(data_size-1 downto 0);
    
    signal tx_next_sig:     std_logic;
    
BEGIN

    --=================--
    -- OUTPUT REGISTER --
    --=================--
    
    incl_outputreg:
    if outreg_incl = 1 generate
        
        process(clk) is
        begin
            if rising_edge(clk) then
                if tx_next_sig = '1' then
                    tx_vld_reg  <= tx_vld;
                    tx_data_reg <= tx_data;
                end if;
            end if;
        end process;
        
    end generate;

    
    notincl_outputreg:
    if outreg_incl = 0 generate
        
        tx_vld_reg  <= tx_vld;
        tx_data_reg <= tx_data;
        
    end generate;
    
    --================--
    -- INVALID FILTER --
    --================--
    
    tx_next_sig <= NOT tx_vld OR next_comb;
    tx_next     <= tx_next_sig;
    
    --====================--
    -- BUFFER/MULTIPLEXER --
    --====================--
        
    process(clk) is
    begin
        if rising_edge(clk) then
            if next_comb = '1' then
                vld_bufd  <= tx_vld_reg;
                data_bufd <= tx_data_reg;
            end if;
        end if;
    end process;
        
    vld_int <= tx_vld_reg when next_comb ='1' else vld_bufd;
    rx_data <= tx_data_reg when next_comb ='1' else data_bufd;
        
    
    --================--
    -- Next Registers --
    --================--

    nextregs: 
    for i in 0 to rx_number-1 generate
        process(clk) is
        begin
            if rising_edge(clk) then
                next_reg(i) <= rx_next(i);
            end if;
        end process;
    end generate;
        
    --===========--
    -- RECEIVERS --
    --===========--
    
    multiple_recv: 
    if rx_number > 1 generate
    
        next_comb <= AND_REDUCE(next_reg); -- combine next signals
            
        rx_vld_appr: 
        for i in 0 to rx_number-1 generate
            vld_appr(i) <= NOT next_reg(i) OR next_comb;
            rx_vld(i)   <= vld_appr(i) AND vld_int;
        end generate;
        
    end generate;
        
    one_recv: 
    if rx_number = 1 generate
        rx_vld(0) <= vld_int;
        next_comb <= next_reg(0);
    end generate;
    
    
end architecture struct;
