------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Updated:        01/04/2013
Module Name:    S_CMI_RX

-----------
Description
-----------

   This is the RX side of the synchronisation CMI.


--------
Generics
--------

    data_size       := bit size of the transported data
    sync_steps      := # of synchronisation registers for the tx_tag


----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
    
--------------
Implementation
--------------




*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

   
entity S_CMI_RX is

generic
(
    data_size:      integer := 32;
    sync_steps:     integer := 2
);

port(
    -- Clocks
    rx_clk:         in std_logic;
    -- CMI Transmitter
    scmi_rx_tag:    out std_logic;
    scmi_tx_tag:    in  std_logic;
    scmi_tx_data:   in  std_logic_vector(data_size-1 downto 0);
    -- CMI Receivers
    rx_data:        out std_logic_vector(data_size-1 downto 0);
    rx_vld:         out std_logic;
    rx_next:        in std_logic
);

end entity S_CMI_RX;


architecture struct of S_CMI_RX is
    
    --===============--
    -- Basic Signals --
    --===============--
    
    
    --==============--
    -- Sync Signals --
    --==============--
    
    signal tx_tag_synced:   std_logic_vector(sync_steps-1 downto 0) := (others => '0');
    signal rx_tag:          std_logic := '0';
    signal rx_tag_en:       std_logic;
    signal rx_tag_comp:     std_logic;
    
BEGIN
    
 
    --===============--
    -- Sync Protocol --
    --===============--
    
    -- Tag Forward
    scmi_rx_tag <= rx_tag;
    
    -- RX Assignments
    rx_data     <= scmi_tx_data;
    rx_vld      <= rx_tag_comp;
        
    -- RX Tag
    rx_tag_en   <= rx_next AND rx_tag_comp;
    rx_tag_comp <= tx_tag_synced(sync_steps-1) XOR rx_tag;
    
    process(rx_clk) is
    begin
        if rising_edge(rx_clk) then
            if rx_tag_en = '1' then
                rx_tag <= NOT rx_tag;
            end if;
        end if;
    end process;
        
        
    -- Synchronisation 
        
    process(rx_clk) is
    begin
        if rising_edge(rx_clk) then
            tx_tag_synced <= tx_tag_synced(sync_steps-2 downto 0) & scmi_tx_tag;
        end if;
    end process;
    
    
    
    
end architecture struct;