
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/04/2012
Interface Name:   CMI
Description:

Synchronized Common Modular Interconnect

Parameter Description:

data_size 		:= bit size of the transported data
sync_steps		:= # of synchronisation registers
outreg_incl 	:= include the TX output register inside the Interconnect

Input Description:

tx_clk	:= clock region for the transmitter module
rx_clk	:= clock region for the receiver module 

Use Case:

This Interconnect is able to send data between  2 different clock domains.
But every data point/sample needs 2 TX_CCs and 2 RX_CCs to cross this Interconnect.

*/


interface SCMI
#(
	parameter data_size 	= 32,
	parameter sync_steps	= 2,
	parameter outreg_incl	= 1
)
(
	input tx_clk,
	input rx_clk
);
	
	///////////////
	// Interface //
	///////////////
	
	logic 					tx_next;
	logic 					tx_vld;
	logic [data_size-1:0]	tx_data;
	
	logic 					rx_next;
	logic 					rx_vld;
	logic [data_size-1:0]	rx_data;
	
	///////////////////
	// TX Output Reg //
	///////////////////
	
	logic 					tx_vld_reg;
	logic [data_size-1:0]	tx_data_reg;
	
	///////////////////
	// Basic Signals //
	///////////////////
	
	logic [data_size-1:0]	data_bufd;
	
	//////////////////
	// Sync Signals //
	//////////////////
	
	logic					tx_tag;
	logic 					tx_tag_en;
	logic					tx_tag_comp;
	logic [sync_steps-1:0]	tx_tag_synced;
	
	logic					rx_tag;
	logic 					rx_tag_en;
	logic					rx_tag_comp;
	logic [sync_steps-1:0]	rx_tag_synced;
	
	
	
	// Generator Variable
	genvar i;
	
	generate
				
		/////////////////////
		// OUTPUT REGISTER //
		/////////////////////

		if (outreg_incl)
		begin
		
			always @(posedge clk)
				if (tx_next)
				begin
					tx_vld_reg 	<= tx_vld;
					tx_data_reg	<= tx_data;
				end
		end else
		begin
			
			assign tx_vld_reg 	= tx_vld;
			assign tx_data_reg	= tx_data;
		
		end
			
		////////////////////
		// INVALID FILTER //
		////////////////////
		
		assign tx_next = ~tx_vld | next_comb;
		

		///////////////////
		// Sync Protocol //
		///////////////////
			
		// Buffer
		always @(posedge tx_clk)
			if (tx_tag_comp)
				data_bufd <= tx_data_reg;
		
					
		// RX Assignments
		assign rx_data 	 = data_bufd;
		assign rx_vld	 = rx_tag_comp;
			
		// TX Tag
		assign tx_tag_comp = rx_tag_synced[sync_steps-1] ~^ tx_tag;
		assign tx_tag_en = tx_vld_reg & tx_tag_comp;
			
			
		always @(posedge tx_clk)
			if (tx_tag_en)
				tx_tag <= ~tx_tag;
			
		// RX Tag
		assign rx_tag_en = rx_next & rx_tag_comp;
		assign rx_tag_comp = tx_tag_synced[sync_steps-1] ^ rx_tag;
			
		always @(posedge rx_clk)
			if (rx_tag_en)
				rx_tag <= ~rx_tag;
			
		// Synchronisation (both ways)
			
		always @(posedge tx_clk)
			rx_tag_synced <= {rx_tag_synced[sync_steps-2:0], rx_tag};
			
		always @(posedge rx_clk)
			tx_tag_synced <= {tx_tag_synced[sync_steps-2:0], tx_tag};
			
	
	endgenerate
	
	////////////////
	// TX modport //
	////////////////
	
	// external modport name: inst.TX
	modport TX (input tx_next, output tx_vld, tx_data); 
	
	////////////////
	// RX modport //
	////////////////
	
	// external modport name: inst.RX

	modport RX (input rx_data, input rx_vld, output rx_next); 

	
endinterface
