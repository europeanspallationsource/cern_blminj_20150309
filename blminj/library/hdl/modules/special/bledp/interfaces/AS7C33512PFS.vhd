------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Oliver Bitterling
Create Date:    4/09/2012
Module Name:    AS7C33512PFS

-----------
Description
-----------

    CMI Interfac for the AS7C33512PFS SRAM

--------
Generics
--------

    ram_data_size       := size of the RAM data bus.
    ram_addr_size       := size of the RAM addr bus.
    cmi_data_size       := size of the CMI data bus. Must be smaller or equal to ram_data_size.
    cmi_addr_size       := size of the CMI addr bus. Must be smaller or equal to ram_addr_size.

-----------
Limitations
-----------

    Maximum clock frequency of 166 MHz.

    cmi_data_size must be smaller or equal to ram_data_size.
    cmi_addr_size must be smaller or equal to ram_addr_size.

----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

    altera_mf.scfifo
    
--------------
Implementation
--------------
    
    The interface can handle one read or write request per clock cycle.
	Switching from writing to reading takes two clock cycles.
    The interface needs 4 clock cycles to complete a request . 
    In the first cycle the received data is written into a 3 register FIFO where data propagates one register per clock cycle.
	If this data is a read request the address is immediately written into the SRAM.
	If during the third clock cycle the third register of the FIFO contains a read request the data from the SRAM and the address from 
	the request are written into another FIFO. This is necessary to store data of already submitted read requests if the rdout_next signal is zero.
	If the third register contains a write request both address and data are send to the SRAM together completing the request.
	The last clock cycle is used by the output FIFO. 
    All data to the SRAM is written on the falling edge and data from the SRAM is read on the rising edge.
	
	Combinatorics: Manages all handshake signals. Blocks read request for 2 cycls after last write request.
	
	FIFO Register: Writes outside data in FIFO according to commands from Combinatorics.
	
	Rising Edge: Propagates data inside the FIFO.
	
	Falling Edge: All data for the SRAM is send on the falling edge. Furthermore some handshake signals for the SRAM and some Switch signals are set.

	Ports: According to Switch signals data is send from the FIFO register to the SRAM.
	
	Output FIFO: Data from the SRAM is read on the rising edge and conserved until rdout_next is one. 

*/

library IEEE;
	use IEEE.std_logic_1164.all;
	use IEEE.numeric_std.all;
	use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
LIBRARY altera_mf;
	USE altera_mf.all;
	

entity AS7C33512PFS is

Generic(
    ram_data_size:      Integer := 32;
    ram_addr_size:      Integer := 21;
    cmi_data_size:      Integer := 16;
    cmi_addr_size:      Integer := 16
    
    );
port(
	-- Clock
	signal clk:			in std_logic;
	
	-- RAM Interface
	signal data:        inout std_logic_vector(ram_data_size-1 downto 0);
	signal addr:		out std_logic_vector(ram_addr_size-1 downto 0);
    signal ce:          out std_logic_vector(2 downto 0);               --Chip select
    signal gwe:         out std_logic;                                  --Global write enable
    signal bwe:         out std_logic;                                  --Byte write enable
    signal oe:          out std_logic:= '0';                            --Direction of data
    
        	
	-- CMI Request Input
	signal req_data:	in std_logic_vector(cmi_addr_size-1 downto 0); --contain address of requested data
	signal req_vld:		in std_logic;
	signal req_next:	out std_logic := '1';

	-- CMI Write Input
	signal wr_data:		in std_logic_vector(cmi_addr_size + cmi_data_size-1 downto 0); -- contains data to be written
	signal wr_vld:		in std_logic;
	signal wr_next:		out std_logic := '1';
	
	-- CMI Output
	signal rdout_data:	out std_logic_vector(cmi_addr_size + cmi_data_size-1 downto 0); -- contains read data
	signal rdout_vld:	out std_logic;
	signal rdout_next:	in std_logic
	
);
	
end entity AS7C33512PFS;

architecture struct of AS7C33512PFS is
	
    ---------------
    -- Constants --
    ---------------
    
    Constant fifo_depth:    natural := 3;
    Constant lpm_widthu:	natural := natural(ceil(LOG2 (real(fifo_depth))));
    
    ---------------
    -- Registers --
    ---------------
    
    signal conveyor:                slv_array(2 downto 0)(cmi_data_size + cmi_addr_size +1 downto 0) := (others => (cmi_data_size + cmi_addr_size => '1', others => '0'));
    signal FIFO_data_int:           std_logic_vector(cmi_data_size + cmi_addr_size-1 downto 0) := (others => '0');
    signal FIFO_vld_int:            std_logic := '0';
    signal addr_int1:               std_logic_vector(ram_addr_size-1 downto 0)      := (others => '0');
	signal addr_int2:               std_logic_vector(ram_addr_size-1 downto 0)      := (others => '0');
    signal gwe_int:                 std_logic := '1';
    signal data_int:                std_logic_vector(ram_data_size-1 downto 0)    := (others => '0');
	signal addr_switch:				std_logic := '1';
    
    
    --=================--
	-- GENERAL SIGNALS --
	--=================--
	
    signal vld:                     std_logic_vector(1 downto 0);
    signal req_next_int:            std_logic;
    signal RdReq:                   std_logic;
    signal empty:                   std_logic;
	signal no_wr_in_que:			std_logic;
	signal wr_switch:				std_logic;
	
	signal data_temp:				std_logic_vector(ram_data_size-1 downto 0):= (others => '0');

	
    
	COMPONENT scfifo
	GENERIC (
		add_ram_output_register		: STRING;
		intended_device_family		: STRING;
		lpm_numwords		: NATURAL;
		lpm_showahead		: STRING;
		lpm_type		: STRING;
		lpm_width		: NATURAL;
		lpm_widthu		: NATURAL;
		overflow_checking		: STRING;
		underflow_checking		: STRING;
		use_eab		: STRING
	);
	PORT (
			clock	: IN STD_LOGIC ;
			data	: IN STD_LOGIC_VECTOR (7 DOWNTO 0);
			rdreq	: IN STD_LOGIC ;
			usedw	: OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
			empty	: OUT STD_LOGIC ;
			full	: OUT STD_LOGIC ;
			q	: OUT STD_LOGIC_VECTOR (7 DOWNTO 0);
			wrreq	: IN STD_LOGIC 
	);
	END COMPONENT;
	
Begin
    
    --===============--
	-- Combinatorics --
	--===============--
	wr_next <= '1'; --Write priority
    req_next_int <= (Not req_vld) OR ((not wr_vld) And (rdout_next and no_wr_in_que)); 
    req_next <= req_next_int;
	no_wr_in_que <= conveyor(1)(cmi_data_size + cmi_addr_size) and conveyor(2)(cmi_data_size + cmi_addr_size);
    
	vld <= wr_vld & (req_vld and req_next_int);
    
	
	
	
	--===============--
	-- FIFO Register --
	--===============--
	-- Filling FIFO with data from both CMI interfaces
	WITH vld SELECT 
        conveyor(0) <= 	'1' & '0' & wr_data 										when "10",
						'1' & '0' & wr_data 										when "11",
						'1' & '1' & req_data & (cmi_data_size-1 downto 0 => '0') 	when "01",
						(cmi_data_size + cmi_addr_size => '1', others => '0') 		when "00",
						(cmi_data_size + cmi_addr_size => '1', others => '0') 		when others;
	
	

    
    
	--=============--
	-- Rising Edge --
	--=============--
	
	Process(clk)
    Begin
        if rising_edge(clk) then        
            --Basic FIFO
            conveyor(2) <= conveyor(1);
            conveyor(1) <= conveyor(0);
        end if;
    End process;

    
    --==============--
	-- Falling Edge --
	--==============--
	
	Process(clk) 
    Begin   
        if falling_edge(clk) then
			
			wr_switch <= not conveyor(2)(cmi_data_size + cmi_addr_size);
			oe <= not conveyor(2)(cmi_data_size + cmi_addr_size);
			
			data_int <= (ram_data_size-cmi_data_size-1 downto 0 => '0') & conveyor(2)(cmi_data_size-1 downto 0);
            
			addr_int1 <= (ram_addr_size-cmi_addr_size-1 downto 0 => '0') & conveyor(0)(cmi_data_size + cmi_addr_size-1 downto cmi_data_size);
            addr_int2 <= (ram_addr_size-cmi_addr_size-1 downto 0 => '0') & conveyor(2)(cmi_data_size + cmi_addr_size-1 downto cmi_data_size);
			addr_switch <= conveyor(2)(cmi_data_size + cmi_addr_size);
			
			gwe_int <= not (not conveyor(2)(cmi_data_size + cmi_addr_size) and conveyor(2)(cmi_data_size + cmi_addr_size+1));
            data_temp <= data;
           
        end if;
    end process;
            
    --=======--
	-- PORTS --
	--=======--
					
	with wr_switch select 
		data 	<= 	data_int			when '1',
					(others => 'Z') 	when others;
    
    
	with addr_switch select
		addr <= addr_int1 when '1',
				addr_int2 when '0',
				addr_int2 when others;
	
	gwe <= 	gwe_int;
    
    -- permament configuration signals to RAM
	bwe<= '1';
    ce(2 downto 0) <= "010"; 
	
	
	--=============--
	-- Output FIFO --
	--=============--
   
   -- Transfere read data to output FIFO
    FIFO_vld_int <= conveyor(2)(cmi_data_size + cmi_addr_size) And conveyor(2)(cmi_data_size + cmi_addr_size+1); -- Is package valid and result of a read request?
    WITH conveyor(2)(cmi_data_size + cmi_addr_size) SELECT
        FIFO_data_int <=    conveyor(2)(cmi_data_size + cmi_addr_size-1 downto cmi_data_size) & data(cmi_data_size-1 downto 0) when '1',
                            (others => '1') when others;
    
    
    -- CMI FIFO
    RdReq       <= rdout_next AND NOT(Empty);
    Process(clk)
	Begin
		if rising_edge(clk) then
			if rdout_next = '1' then
				rdout_vld <= NOT(Empty);
			end if;
		end if;
	End process;
    
	data_io_inst: entity work.data_io
	PORT map
	(
		probe => data_temp
	);

    -- Output FIFO to prevent data loss due to data congestion
    Fifo	:	scfifo
		generic map(		
		add_ram_output_register => "OFF",
		intended_device_family => "Stratix",
		lpm_numwords => fifo_depth,
		lpm_showahead => "OFF",
		lpm_type => "scfifo",
		lpm_width => cmi_data_size + cmi_addr_size,
		lpm_widthu => lpm_widthu,
		overflow_checking => "ON",
		underflow_checking => "ON",
		use_eab => "ON"
		)
		port map(
		clock			=>clk,
		data			=>FIFO_data_int,
		empty			=>empty,
		full			=>open,
		q 				=>rdout_data,
		rdreq			=>RdReq,
		usedw			=>open,
		wrreq			=>FIFO_vld_int
		);
		
	
        
end architecture struct;