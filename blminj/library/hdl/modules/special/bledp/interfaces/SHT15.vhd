------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Update Date:    20/06/2012
Module Name:    SHT15


-----------
Description
-----------

    Reads out the Temperature and Humidity values from a SHT15 Chip and presents them in case of a request on the readout CMI.

------------------
Generics/Constants
------------------

    Tclk_ns         := system clock period in ns, no restrictions
    Tintfclk_ns     := local clock period in ns for the I2C Interface
    
    clkdom_diff     := timing difference between the system clock and the necessary interface timing
    intfclk_vsize   := size of the timer needed for the given timing difference
    T_quiet         := idle time between readouts (1 s)
    quiet_vsize     := vectorsize for the quiet timer
    
-----------
Limitations
-----------

    Tintfclk_ns     := max. frequency of 2 MHz (respectively 10 MHz, when powered with more than 4.5 V) 
                       The actual SCK frequency is half of the frequency of the clk.


----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
 
-----------------
Necessary Modules
-----------------

    CMI

---------------
Necessary Cores
---------------

    none  
    
    
--------------
Implementation
--------------

    To request the values, the following instructions are necessary:

    Temp :  "00000011"
    Hum :   "00000101"

    The Implementation consist mainly of the following controls:

    1. CMI Control

    - manages the internal CMI interfaces
    - is triggered by a vld on the rd_req CMI
    - after a request, it triggers the INTF control with said request and afterwards presents the requested value on the rdout-CMI

    2. INTF Control

    - implements one read access to the connected chip
    - is triggered by intf_en


    Add. Information: State changes of the interface control happen at the falling_edge of SCK_int.

    -----

    How to do a measurement?

    1. Reset Sequence
              _____________________________________________________         ________
        DATA:                                                      |_______|
                 _    _    _    _    _    _    _    _    _        ___     ___
        SCK : __| |__| |__| |__| |__| |__| |__| |__| |__| |______|   |___|   |______


    2. Send Transmission Start
             _____         ________
       DATA:      |_______|
                 ___     ___
       SCK : ___|   |___|   |______

    3. Send Command

    4. Wait for Measurement

    5. Readout Data

    6. End Transfer

    7. Wait  min. 1s between measurements

    -----

    Local Clock Generator

    The fast clock (clk, period: Tclk_ns) is used in all clocked processes, but is only enabled (enable_clk) every clkdom_diff cycles,
    so that it simulates a local clock with a period of Tintfclk_ns.

------------------------
Not implemented Features
------------------------

    - "Strange Conditions" states / ERROR HANDLING$

*/
------------------------------


library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

entity SHT15 is

generic
(
    Tclk_ns:        integer := 8; 
    Tintfclk_ns:    integer := 4000 -- 250 kHz (default)
);

port
(   
    -- Clock
    clk:            in  std_logic;
    -- SHT15 Interface
    DATA:           inout std_logic;
    SCK:            out std_logic;
    -- CMI Rd_Request Input
    rd_req_data:    in  std_logic_vector(7 downto 0);
    rd_req_vld:     in  std_logic;
    rd_req_next:    out std_logic;
    -- CMI Readout Output
    rdout_data:     out std_logic_vector(20 downto 0); -- tag(5-bits) + data
    rdout_vld:      out std_logic;
    rdout_next:     in  std_logic
);

end entity SHT15;



architecture struct of SHT15 is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- DATA #sync regs
    constant sync_stages: integer := 2;
    
    -- Local <-> General Clock Timing/Difference
    constant clkdom_diff:   integer := Tintfclk_ns/Tclk_ns;
    constant intfclk_vsize: integer := get_vsize(clkdom_diff);
    
    -- Quiet Timer
    constant T_quiet:       integer := integer(CEIL(real(1000000000)/real(Tclk_ns)));  -- quiet time of 1s
    constant quiet_vsize:   integer := get_vsize(T_quiet);
    
    -- Measurement Timeout
    constant T_meas_timeout:    integer := integer(CEIL(real(1000000000)/real(Tclk_ns)));  --timeout time of 1s
    constant meas_to_vsize:     integer := get_vsize(T_meas_timeout);
    
    --=====--
    -- FSM --
    --=====--

    type cmi_state_t is (idle, read_req, send_rdout, quiet_phase);
    signal cmi_present_state, cmi_next_state: cmi_state_t := idle;

    type intf_state_t is (idle, sync_with_timer, reset_seq, reset_seq_start_symbol, start_symbol, start_symbol_end, idle_instr, send_instr, wait_for_meas, read_data, ack, error);
    signal intf_present_state, intf_next_state: intf_state_t := idle;

    --=========--
    -- COUNTER --
    --=========--
    
    -- Bit Counter
    signal bit_cnt:         UNSIGNED(3 downto 0) := "0111";
    signal bit_rst1:        std_logic;
    signal bit_rst2:        std_logic;
    signal bit_rst3:        std_logic;
    signal bit_dec:         std_logic;
    
    -- Byte Counter (for read/write access)
    signal byte_cnt:        UNSIGNED(2 downto 0) := "000";
    signal byte_rst:        std_logic;
    signal byte_inc:        std_logic;

    -- Start Symbol Counter
    signal CC_cnt:          std_logic;
    signal CC_tgl:          std_logic;
    
    -- Local Clock
    signal intfclk_timer:   UNSIGNED(intfclk_vsize-1 downto 0) := (intfclk_vsize-1 downto 0 => '0');
    signal intfclk_rst:     std_logic;
    
    -- Quiet Timer
    signal quiet_timer:     UNSIGNED(quiet_vsize-1 downto 0) := (quiet_vsize-1 downto 0 => '0');
    signal quiet_rst:       std_logic;
    signal quiet_inc:       std_logic;
    
    -- Meas Timeout Timer
    signal meas_to_timer:   UNSIGNED(meas_to_vsize-1 downto 0) := (meas_to_vsize-1 downto 0 => '0');
    signal meas_to_rst:     std_logic;
    signal meas_to_dec:     std_logic;
    
    
    

    --==========--
    -- REGISTER --
    --==========--
    
    -- I2C
    signal readout:         std_logic_vector(15 downto 0);
    signal instr:           std_logic_vector(7 downto 0);

    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- CMI Control
    signal rdout_vld_int:   std_logic;
    
    -- Access
    signal upd_instr:       std_logic;
    
    -- CMI <-> INTF 
    signal intf_en:         std_logic;
    signal intf_busy:       std_logic;
    
    -- INTF Control
    signal DATA_int:        std_logic;
    signal DATA_int_reg:    std_logic;
    signal DATA_sel:        UNSIGNED(1 downto 0);
    signal readout_shift:   std_logic;  

    -- Generated Clock
    signal SCK_int:         std_logic;
    signal toggle_SCK:      std_logic;
    
    -- Interface Clock Control
    signal intfclk_re:      std_logic;
    
    -- sync DATA
    signal DATA_synced:     std_logic;
    
begin
    
    --=======--
    -- PORTS --
    --=======--
    
    SCK     <= SCK_int;
    DATA    <= 'Z' when DATA_int = '1' else '0';
    
    
    --=================--
	-- SYNCHRONISATION --
	--=================--
   
	sync_AS : entity work.sync
	generic map
	(
		data_width 	=> 1,
		stages 		=> sync_stages,
        init_value  => '1'
	)
	port map
	(
		dest_clk 				=> clk,
		async_data 				=> vectorize(DATA),
		scalarize(sync_data) 	=> DATA_synced
	);
    --======================--
    -- CREATING LOCAL CLOCK --
    --======================--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if intfclk_rst = '1' then
                intfclk_timer <= (intfclk_vsize-1 downto 0 => '0');
            else
                intfclk_timer <= intfclk_timer + 1;
            end if;
        end if;
    end process;
    
    process(all) is
    begin
        -- DEFAULTS
        intfclk_rst <= '0';
        intfclk_re  <= '0';
        
        if intfclk_timer = clkdom_diff - 1 then
            intfclk_rst     <= '1';
            intfclk_re      <= '1';
        end if;
        
    end process;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if bit_rst1= '1' then
                bit_cnt <= "1000"; -- 9 Reset Cycles
            elsif bit_rst2 = '1' then
                bit_cnt <= "0111"; -- 8 Instruction Cycles
            elsif bit_rst3 = '1' then
                bit_cnt <= "1111"; -- 16 Readout Cycles
            elsif bit_dec = '1' then
                bit_cnt <= bit_cnt - 1;
            end if;
                
            if byte_rst = '1' then
                byte_cnt <= "000";
            elsif byte_inc = '1' then
                byte_cnt <= byte_cnt + 1;
            end if;
                
            if CC_tgl = '1' then
                CC_cnt <= NOT CC_cnt;
            end if;
            
            if quiet_rst = '1' then
                quiet_timer <= (quiet_vsize-1 downto 0 => '0');
            elsif quiet_inc = '1' then
                quiet_timer <= quiet_timer + 1;
            end if;
            
            if meas_to_rst = '1' then
                meas_to_timer <= TO_UNSIGNED(T_meas_timeout, meas_to_vsize);
            elsif meas_to_dec = '1' then  
                meas_to_timer <= meas_to_timer - 1;
            end if;
            
        end if;
    end process;
    
    
    --=============--
    -- CMI CONTROL --
    --=============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            cmi_present_state <= cmi_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        rdout_vld_int   <= '0';
        rd_req_next     <= '1';
        -- Instruction update
        upd_instr       <= '0';
        -- INTF ctrl
        intf_en         <= '0';
        -- Quiet Timer
        quiet_rst       <= '0';
        quiet_inc       <= '0';
        
        
        case cmi_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if rd_req_vld = '1' then
                    rd_req_next     <= '0';
                    upd_instr       <= '1';
                    intf_en         <= '1';
                    cmi_next_state  <= read_req;
                else
                    cmi_next_state  <= idle;
                end if;
            
            --===============--
            when read_req =>
            --===============--
                
                rd_req_next <= '0';
                if intf_busy = '1' then
                    cmi_next_state  <= read_req;
                else
                    cmi_next_state  <= send_rdout;
                end if;
                
            --===============--
            when send_rdout =>
            --===============--
                
                rd_req_next <= '0';
                if rdout_next = '1' then
                    rdout_vld_int   <= '1';
                    cmi_next_state  <= quiet_phase;
                else
                    cmi_next_state  <= send_rdout;
                end if;
            
            --===============--
            when quiet_phase =>
            --===============--
            
                if quiet_timer = T_quiet-1 then
                    quiet_rst       <= '1';
                    rd_req_next     <= '1';
                    cmi_next_state  <= idle;
                else
                    quiet_inc       <= '1';
                    rd_req_next     <= '0';
                    cmi_next_state  <= quiet_phase;
                end if; 
            
            --===============--
            when others =>
            --===============--
            
                cmi_next_state <= idle;
            
        end case;
    end process;
    
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if rdout_next = '1' then
                rdout_data  <= instr(4 downto 0) & readout;
                rdout_vld   <= rdout_vld_int;
            end if;
     
        end if;
    end process;
    
    --==============--
    -- UPDATE INSTR --
    --==============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
        
            if upd_instr = '1' then
                instr <= rd_req_data; 
            end if;
            
        end if;
    end process;

    
    --============--
    -- R/W ACCESS --
    --============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if readout_shift = '1' then
                readout <= readout(14 downto 0) & DATA_synced;
            end if;
                
        end if;
    end process;
    
    DATA_int <= '0'                         when DATA_sel = "00" else
                instr(TO_INTEGER(bit_cnt))  when DATA_sel = "10" else
                '1'                         when DATA_sel = "11" else
                '1';
                
    
    --===========--
    -- CLOCK GEN --
    --===========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            if intfclk_re = '1' then
            
                if toggle_SCK = '1' then
                    SCK_int <= NOT SCK_int;
                else
                    SCK_int <= '0';
                end if;
                
            end if;
        end if;
    end process;
    
    
    --==============--
    -- INTF CONTROL --
    --==============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            intf_present_state <= intf_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- Counters
        bit_rst1        <= '0';
        bit_rst2        <= '0';
        bit_rst3        <= '0';
        bit_dec         <= '0';
        byte_inc        <= '0';
        byte_rst        <= '0';
        meas_to_rst     <= '0';
        meas_to_dec     <= '0';
        CC_tgl          <= '0';
        -- Readout Reg
        readout_shift   <= '0';
        -- I2C control
        DATA_sel        <= "11"; -- Z
        toggle_SCK      <= '1';
        -- Busy
        intf_busy       <= '1';
        
        
        case intf_present_state is
            
            --===============--
            when idle =>
            --===============--
            
                intf_busy       <= '0';
                byte_rst        <= '1';
                toggle_SCK      <= '0';
                
                if intf_en = '1' then
                    bit_rst1        <= '1';
                    intf_next_state <= sync_with_timer;
                else
                    intf_next_state <= idle;
                end if;
                
            --===============--
            when sync_with_timer =>
            --===============--
            
                toggle_SCK      <= '0';
                if intfclk_re = '1' then
                    intf_next_state <= reset_seq;
                else
                    intf_next_state <= sync_with_timer;
                end if;
                
            --===============--
            when reset_seq =>
            --===============--
                
                if intfclk_re = '1' then
                    if SCK_int = '0' then -- XXXXX
                        intf_next_state <= reset_seq;
                    else
                        if bit_cnt = 0 then
                            bit_rst2        <= '1';
                            intf_next_state <= reset_seq_start_symbol;
                        else
                            bit_dec         <= '1';
                            intf_next_state <= reset_seq;
                        end if;
                    end if;
                else
                    intf_next_state <= reset_seq;
                end if;
                
            --===============--
            when reset_seq_start_symbol =>
            --===============--
                

                if intfclk_re = '1' then
                    if CC_cnt = '0' then
                        intf_next_state <= reset_seq_start_symbol;
                        if SCK_int = '1' then
                            CC_tgl <= '1';
                        end if;
                    else
                        if SCK_int = '0' then
                            intf_next_state <= reset_seq_start_symbol;
                        else
                            CC_tgl          <= '1';
                            intf_next_state <= start_symbol;
                        end if;
                    end if;
                else
                    if CC_cnt = '0' AND SCK_int = '1' then
                        DATA_sel <= "00";
                    end if;
                    
                    if CC_cnt = '1' AND SCK_int = '0' then
                        DATA_sel <= "00";
                    end if;
                    
                    intf_next_state <= reset_seq_start_symbol;
                end if;
                
            --===============--
            when start_symbol =>
            --===============--

                if intfclk_re = '1' then
                    if CC_cnt = '0' then
                        intf_next_state <= start_symbol;
                        if SCK_int = '1' then
                            CC_tgl <= '1';
                        end if;
                    else
                        if SCK_int = '0' then
                            intf_next_state <= start_symbol;
                        else
                            CC_tgl          <= '1';
                            intf_next_state <= start_symbol_end;
                        end if;
                    end if;
                else
                    if CC_cnt = '0' AND SCK_int = '1' then
                        DATA_sel <= "00";
                    end if;
                    
                    if CC_cnt = '1' AND SCK_int = '0' then
                        DATA_sel <= "00";
                    end if;
                    
                    intf_next_state <= start_symbol;
                end if;
                
            --===============--
            when start_symbol_end =>
            --===============--

                toggle_SCK      <= '0';
                intf_next_state <= idle_instr;
                
            --===============--
            when idle_instr =>
            --===============--

                toggle_SCK      <= '0';
                DATA_sel        <= "00";
                intf_next_state <= send_instr;
                
            
            --===============--
            when send_instr =>
            --===============--

                DATA_sel <= "10";
                if intfclk_re = '1' then
                    if SCK_int = '1' then
                        if bit_cnt = "0000" then 
                            bit_rst3        <= '1';
                            intf_next_state <= ack;
                        else
                            bit_dec         <= '1';
                            intf_next_state <= send_instr;
                        end if;
                    else
                        intf_next_state <= send_instr;
                    end if;
                else
                    intf_next_state <= send_instr;
                end if;
            
            --===============--
            when wait_for_meas =>
            --===============--
            
                toggle_SCK      <= '0';
                meas_to_dec     <= '1';
                
                if intfclk_re = '1' then
                    if DATA_synced = '0' then
                        meas_to_rst     <= '1';
                        intf_next_state <= read_data;
                    else
                        if meas_to_timer = 0 then -- ERROR
                            bit_rst1        <= '1';
                            byte_rst        <= '1';
                            meas_to_rst     <= '1';
                            intf_next_state <= reset_seq;
                        else
                            intf_next_state <= wait_for_meas;
                        end if;
                    end if;
                else
                    intf_next_state <= wait_for_meas;
                end if;
            
            --===============--
            when read_data =>
            --===============--
                
                if intfclk_re = '1' then
                    if SCK_int = '1' then
                        readout_shift <= '1';
                        if bit_cnt = "0000" then
                            bit_rst1        <= '1';
                            intf_next_state <= idle; -- DATA = Z, no ack, no CRC, end transmission
                        elsif bit_cnt = "1000" then 
                            bit_dec         <= '1';
                            intf_next_state <= ack;
                        else
                            bit_dec         <= '1';
                            intf_next_state <= read_data;
                        end if;
                    else
                        intf_next_state <= read_data;
                    end if;
                else
                    intf_next_state <= read_data;
                end if;
        
            --===============--
            when ack =>
            --===============--

                intf_next_state <= ack;
                
                if intfclk_re = '1' then
                    if SCK_int = '1' then -- SCK high
                        byte_inc <= '1'; -- next byte
                        if byte_cnt = "000" then
                            if DATA_synced = '0' then -- got ack
                                toggle_SCK      <= '0'; -- pull the clock low during measurement
                                intf_next_state <= wait_for_meas;
                            else -- ERROR
                                toggle_SCK      <= '0';
                                bit_rst1        <= '1';
                                byte_rst        <= '1';
                                intf_next_state <= reset_seq;
                            end if;
                        elsif byte_cnt = "001" then
                            intf_next_state <= read_data;
                        end if;
                    end if;
                else
                    if byte_cnt = "001" then
                        DATA_sel <= "00"; -- ack for first byte
                    end if;
                end if;
                
            
            
            --===============--
            when error =>
            --===============--
                
                intf_next_state <= error;
                
            --===============--
            when others =>
            --===============--
            
                intf_next_state <= error;
                
        end case;
    end process;
    
    
end architecture struct;
