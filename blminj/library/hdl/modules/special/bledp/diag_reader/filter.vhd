------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/08/2012
Module Name:    filter

----------
Description
-----------

    Filters out certain data depending on the defined tag (with deleting the tag)

------------------
Generics/Constants
------------------

    tag_size        := size of the tag (MSBs of the input data)
    data_size       := size of the real data (LSBs of the input data)
    filtered_tag    := accepted tag number, which is going to pass the filter
    
    filter_value    := filtered tag in std_logic_vector form
    
-----------
Limitations
-----------

    < limitations for the generics and/or the module itself >
 
----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

    CMI

---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    < detailed descriptions of the internal functionality of the module including port usage >

    
------------------------
Not implemented Features
------------------------

    < informations about missing features or nice-to-have addons >


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity filter is

generic(
    tag_size:       integer;
    data_size:      integer;
    filtered_tag:   integer
    
);

port(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    tagged_data:    in  std_logic_vector(tag_size+data_size-1 downto 0);
    tagged_vld:     in  std_logic;
    tagged_next:    out std_logic;
    -- CMI Output
    filtered_data:  out std_logic_vector(data_size-1 downto 0);
    filtered_vld:   out std_logic := '0';
    filtered_next:  in  std_logic
);

end entity filter;


architecture struct of filter is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant filter_value:  UNSIGNED(tag_size-1 downto 0) := TO_UNSIGNED(filtered_tag,tag_size);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal tag: UNSIGNED(tag_size+data_size-1 downto data_size);
    
BEGIN

    --==============--
    -- COMBINATORIC --
    --==============--
    
    -- invalid filter
    tagged_next <= filtered_next OR NOT tagged_vld; 
    
    -- renaming
    tag <= UNSIGNED(tagged_data(tag_size+data_size-1 downto data_size));
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if filtered_next = '1' then
                filtered_data <= tagged_data(data_size-1 downto 0);
                if tag = filter_value then
                    filtered_vld <= tagged_vld;
                else
                    filtered_vld <= '0';
                end if;
            end if;
                    
        end if;
    end process;
    

    
    
end architecture struct;
