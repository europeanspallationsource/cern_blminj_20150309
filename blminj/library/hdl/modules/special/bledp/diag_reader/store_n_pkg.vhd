------------------------------
/*
Company:        CERN
Department:     BE/BI/BL
Engineer:       Marcel Alsdorf
Created on:     16/08/2012
Module Name:    store_n_pkg

-----------
Description
-----------

    converts incoming readout data (that includes different tags) into a package format including a control bit which devides the data part into header 
    (intf_number + tag_number) and normal data.

------------------
Generics/Constants
------------------

    intf_number     := defines the interface number in the package header
    rdout_data_size := size of the incoming data
    rdout_tag_size  := size of the incoming tag
    pkg_data_size   := size of the packaged output (including the control bit)
    pkg_tag_size    := size of the tag part in the header
    tag_n           := number of different tags for this module
    tag_array       := values of the different tags (start from LSB) 

-----------
Limitations
-----------

    < limitations for the generics and/or the module itself >
  
----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg    
    
-----------------
Necessary Modules
-----------------

    CMI
    sync_storage
    tag_n_slice
    filter
    priority_mux
    

---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    < detailed descriptions of the internal functionality of the module including port usage >

    
------------------------
Not implemented Features
------------------------

    < informations about missing features or nice-to-have addons >

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.std_logic_misc.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity store_n_pkg is

generic
(
    intf_number:        integer;
    rdout_tag_size:     integer;
    rdout_data_size:    integer;
    pkg_data_size:      integer;
    pkg_tag_size:       integer;
    tag_n:              integer;
    tag_array:          int_array(63 downto 0)
);

port
(
    -- Clock
    clk:        in  std_logic;
    -- CMI Input
    rdout_data: in  std_logic_vector(rdout_data_size+rdout_tag_size-1 downto 0);
    rdout_vld:  in  std_logic;
    rdout_next: out std_logic;
    -- CMI Output
    pkg_data:   out std_logic_vector(pkg_data_size-1 downto 0); -- including protocol bit
    pkg_vld:    out std_logic := '0';
    pkg_next:   in  std_logic;
    -- Update
    upd:        in  std_logic
);

end entity store_n_pkg;


architecture struct of store_n_pkg is
    
   

    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- RDOUT Level    
    signal rdout_rx_data:       std_logic_vector(rdout_data_size+rdout_tag_size-1 downto 0);
    signal rdout_rx_vld:        std_logic_vector(tag_n-1 downto 0);
    signal rdout_rx_next:       std_logic_vector(tag_n-1 downto 0);
    
    -- filtered level
    signal filtered_tx_data:    slv_array(tag_n-1 downto 0) (rdout_data_size-1 downto 0);
    signal filtered_tx_vld:     std_logic_vector(tag_n-1 downto 0);
    signal filtered_tx_next:    std_logic_vector(tag_n-1 downto 0);
    
    signal filtered_rx_data:    slv_array(tag_n-1 downto 0) (rdout_data_size-1 downto 0);
    signal filtered_rx_vld:     std_logic_vector(tag_n-1 downto 0);
    signal filtered_rx_next:    std_logic_vector(tag_n-1 downto 0);
    
    -- sync storage level
    signal stored_tx_data:      slv_array(tag_n-1 downto 0) (rdout_data_size-1 downto 0);
    signal stored_tx_vld:       std_logic_vector(tag_n-1 downto 0);
    signal stored_tx_next:      std_logic_vector(tag_n-1 downto 0);
    
    signal stored_rx_data:      slv_array(tag_n-1 downto 0) (rdout_data_size-1 downto 0);
    signal stored_rx_vld:       std_logic_vector(tag_n-1 downto 0);
    signal stored_rx_next:      std_logic_vector(tag_n-1 downto 0);
    
    -- tns level
    signal tns_tx_data:         slv_array(tag_n-1 downto 0) (pkg_data_size-1 downto 0);
    signal tns_tx_vld:          std_logic_vector(tag_n-1 downto 0);
    signal tns_tx_next:         std_logic_vector(tag_n-1 downto 0);
    
    signal tns_rx_data:         slv_array(tag_n-1 downto 0) (pkg_data_size-1 downto 0);
    signal tns_rx_vld:          std_logic_vector(tag_n-1 downto 0);
    signal tns_rx_next:         std_logic_vector(tag_n-1 downto 0);
    
    -- sync Storage Update Ctrl
    signal stored_data_av:      std_logic_vector(tag_n-1 downto 0);
    signal upd_int:             std_logic;
    
BEGIN

    
    -- sync storage update
        
    upd_int <=  upd when AND_REDUCE(stored_data_av) = '1' else
                '0';
    
    
    --=========--
    -- ONE TAG --
    --=========--
    
    one_tag: if tag_n = 1 generate
        
        
        
        -- CMI RDOUT --
        rdout_CMI: entity work.old_CMI
        GENERIC MAP
        (
            rx_number   => 1,
            data_size   => rdout_data_size + rdout_tag_size,
            outreg_incl => 0
        )
        PORT MAP
        (
            clk         => clk,
            tx_data     => rdout_data,
            tx_vld      => rdout_vld,
            tx_next     => rdout_next,
            rx_data     => rdout_rx_data,
            rx_vld      => rdout_rx_vld,
            rx_next     => rdout_rx_next
        );
        
        -- sync Storage --
        loc_store: entity work.sync_storage
        GENERIC MAP
        (
            data_size       => rdout_data_size
        )
        PORT MAP
        (
            clk             => clk,
            upd_data        => rdout_rx_data,
            upd_vld         => rdout_rx_vld(0),
            upd_next        => rdout_rx_next(0),
            stored_data     => stored_tx_data(0),
            stored_vld      => stored_tx_vld(0),
            stored_next     => stored_tx_next(0),
            upd_storage     => upd_int,
            new_data_av     => stored_data_av(0)
        );
        
        -- CMI Stored --
        stored_CMI: entity work.old_CMI
        GENERIC MAP
        (
            rx_number   => 1,
            data_size   => rdout_data_size,
            outreg_incl => 0
        )
        PORT MAP
        (
            clk                 => clk,
            tx_data             => stored_tx_data(0),
            tx_vld              => stored_tx_vld(0),
            tx_next             => stored_tx_next(0),
            rx_data             => stored_rx_data(0),
            scalarize(rx_vld)   => stored_rx_vld(0),
            rx_next             => vectorize(stored_rx_next(0))
        );
        
        -- TNS --
        tns: entity work.tag_n_slice
        GENERIC MAP
        (
            inp_data_size       => rdout_data_size,
            sliced_data_size    => pkg_data_size,
            tag_size            => pkg_tag_size,
            tag_n               => 0,
            chan_n              => intf_number
        )
        PORT MAP
        (
            clk             => clk,
            inp_data        => stored_rx_data(0),
            inp_vld         => stored_rx_vld(0),
            inp_next        => stored_rx_next(0),
            sliced_data     => pkg_data,
            sliced_vld      => pkg_vld,
            sliced_next     => pkg_next
        );
        
    
    end generate;

    
    --===============--
    -- MULTIPLE TAGS --
    --===============--
    
    mult_tags: if tag_n > 1 generate
    
    
        
        -- CMI RDOUT --
        rdout_CMI: entity work.old_CMI
        GENERIC MAP
        (
            rx_number   => tag_n,
            data_size   => rdout_data_size + rdout_tag_size,
            outreg_incl => 0
        )
        PORT MAP
        (
            clk         => clk,
            tx_data     => rdout_data,
            tx_vld      => rdout_vld,
            tx_next     => rdout_next,
            rx_data     => rdout_rx_data,
            rx_vld      => rdout_rx_vld,
            rx_next     => rdout_rx_next
        );
        
        
        
        per_tag: for i in 0 to tag_n-1 generate
            
            
            -- Filter --
            filter: entity work.filter
            GENERIC MAP
            (
                tag_size        => rdout_tag_size,
                data_size       => rdout_data_size,
                filtered_tag    => tag_array(i)
            )
            PORT MAP
            (
                clk             => clk,
                tagged_data     => rdout_rx_data,
                tagged_vld      => rdout_rx_vld(i),
                tagged_next     => rdout_rx_next(i),
                filtered_data   => filtered_tx_data(i),
                filtered_vld    => filtered_tx_vld(i),
                filtered_next   => filtered_tx_next(i)
            );
            
            -- CMI filtered --
            rdout_CMI: entity work.old_CMI
            GENERIC MAP
            (
                rx_number   => 1,
                data_size   => rdout_data_size,
                outreg_incl => 0
            )
            PORT MAP
            (
                clk                 => clk,
                tx_data             => filtered_tx_data(i),
                tx_vld              => filtered_tx_vld(i),
                tx_next             => filtered_tx_next(i),
                rx_data             => filtered_rx_data(i),
                scalarize(rx_vld)   => filtered_rx_vld(i),
                rx_next             => vectorize(filtered_rx_next(i))
            );
            
            -- sync Storage --
            loc_store: entity work.sync_storage
            GENERIC MAP
            (
                data_size   => rdout_data_size
            )
            PORT MAP
            (
                clk             => clk,
                upd_data        => filtered_rx_data(i),
                upd_vld         => filtered_rx_vld(i),
                upd_next        => filtered_rx_next(i),
                stored_data     => stored_tx_data(i),
                stored_vld      => stored_tx_vld(i),
                stored_next     => stored_tx_next(i),
                upd_storage     => upd_int,
                new_data_av     => stored_data_av(i)
            );
            
            -- CMI Stored --
            stored_CMI: entity work.old_CMI
            GENERIC MAP
            (
                rx_number   => 1,
                data_size   => rdout_data_size,
                outreg_incl => 0
            )
            PORT MAP
            (
                clk                 => clk,
                tx_data             => stored_tx_data(i),
                tx_vld              => stored_tx_vld(i),
                tx_next             => stored_tx_next(i),
                rx_data             => stored_rx_data(i),
                scalarize(rx_vld)   => stored_rx_vld(i),
                rx_next             => vectorize(stored_rx_next(i))
            );
            
            -- TNS --
            tns: entity work.tag_n_slice
            GENERIC MAP
            (
                inp_data_size       => rdout_data_size,
                sliced_data_size    => pkg_data_size,
                tag_size            => pkg_tag_size,
                tag_n               => tag_array(i),
                chan_n              => intf_number
            )
            PORT MAP
            (
                clk             => clk,
                inp_data        => stored_rx_data(i),
                inp_vld         => stored_rx_vld(i),
                inp_next        => stored_rx_next(i),
                sliced_data     => tns_tx_data(i),
                sliced_vld      => tns_tx_vld(i),
                sliced_next     => tns_tx_next(i)
            );
            
            -- CMI TNS --
            tns_CMI: entity work.old_CMI
            GENERIC MAP
            (
                rx_number   => 1,
                data_size   => pkg_data_size,
                outreg_incl => 0
            )
            PORT MAP
            (
                clk                 => clk,
                tx_data             => tns_tx_data(i),
                tx_vld              => tns_tx_vld(i),
                tx_next             => tns_tx_next(i),
                rx_data             => tns_rx_data(i),
                scalarize(rx_vld)   => tns_rx_vld(i),
                rx_next             => vectorize(tns_rx_next(i))
            );
            
        
        end generate;

        
        -- final priority_mux
        pMUX: entity work.priority_mux
        GENERIC MAP
        (
            data_size   => pkg_data_size,
            n           => tag_n
        )
        PORT MAP
        (
            clk         => clk,
            inp_data    => tns_rx_data,
            inp_vld     => tns_rx_vld,
            inp_next    => tns_rx_next,
            muxxed_data => pkg_data,
            muxxed_vld  => pkg_vld,
            muxxed_next => pkg_next
        );
        

    end generate;
    
    
    
end architecture struct;
