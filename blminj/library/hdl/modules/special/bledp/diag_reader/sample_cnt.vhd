------------------------------
/*
Company:    CERN
Department: BE/BI/BL
Engineer:   Marcel Alsdorf
Created on:     17/08/2012
Module Name:    sample_cnt

-----------
Description
-----------

< short description of the module >

------------------
Generics/Constants
------------------

< list of generics/constants including descriptions >

-----------
Limitations
-----------

< limitations for the generics and/or the module itself >

--------------
Implementation
--------------

< detailed descriptions of the internal functionality of the module including port usage >


------------------------
Not implemented Features
------------------------

< informations about missing features or nice-to-have addons >

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity sample_cnt is

generic
(
    data_size: integer
);

port
(

    -- Clock
    clk:        in  std_logic;
    -- Standard Input
    inp:        in  std_logic;
    -- CMI Output
    cmi_data:   out std_logic_vector(data_size-1 downto 0);
    cmi_vld:    out std_logic := '0';
    cmi_next:   in  std_logic;
    -- Commands
    sample:     in  std_logic;
    upd_output: in  std_logic
);

end entity sample_cnt;


architecture struct of sample_cnt is
    
    --==========--
    -- CONSTANTS--
    --==========--
    
    constant max_event_cnt:     UNSIGNED(data_size-1 downto 0) := (others => '1');

    --======--
    -- FSMS --
    --======--
    
    type state_t is (idle, update);
    signal present_state, next_state: state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Bit Counter
    signal event_cnt:       UNSIGNED(data_size-1 downto 0) := (others => '0');
    signal event_rst:       std_logic;
    signal event_flag:      std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal storage:         std_logic_vector(data_size-1 downto 0);
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--

    signal storage_en:      std_logic;
    signal cmi_vld_int:     std_logic;
    
    
BEGIN
    
    
    --==============--
    -- COMBINATORIC --
    --==============--


    --==========--
    -- COUNTERS --
    --==========--
    
    
    process(clk) is
    begin
        if rising_edge(clk) then
            
            if inp = '1' then
                event_flag <= '1';
            elsif sample = '1' then -- reset
                event_flag <= '0';
            end if;
            
            if event_rst= '1' then
                event_cnt <= (others => '0');
            elsif event_cnt < max_event_cnt then
                if sample = '1' then
                    if event_flag = '1' then
                        event_cnt <= event_cnt + 1;
                    end if;
                end if;
            end if;
            
            if storage_en = '1' then
                storage <= STD_LOGIC_VECTOR(event_cnt);
            end if;
        end if;
    end process;

    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        event_rst   <= '0';
        storage_en  <= '0';
        cmi_vld_int <= '0';
        
        case present_state is
            
            --===============--
            when idle =>
            --===============--
            
                if upd_output = '1' then
                    next_state  <= update;
                    storage_en  <= '1';
                    event_rst  <= '1';
                else
                    next_state  <= idle;
                end if;
                
            --===============--
            when update =>
            --===============--  
                
                if cmi_next = '1' then
                    cmi_vld_int <= '1';
                    next_state  <= idle;
                else
                    next_state  <= update;
                end if;
            
            --===============--
            when others =>
            --===============-- 
                
                next_state <= idle;
                
        end case;
    end process;
    
    
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if cmi_next = '1' then
                cmi_data    <= storage;
                cmi_vld     <= cmi_vld_int;
            end if;
                    
        end if;
    end process;
    
end architecture struct;
