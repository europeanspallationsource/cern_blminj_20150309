------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/08/2012
Module Name:    sync_storage 

-----------
Description
-----------

    Locally stores data on a trigger (upd_storage) and pre-stores a the first incoming data after said trigger, 
    while holding the stored data (2 layer storing).
    Signals to the outside world (new_data_av), if a new data has been pre-stored.

------------------
Generics/Constants
------------------

    data_size := size of the data input/output 

-----------
Limitations
-----------

  
----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg

-----------------
Necessary Modules
-----------------

    CMI
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity sync_storage is

generic(
    data_size: integer
);

port(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    upd_data:       in  std_logic_vector(data_size-1 downto 0);
    upd_vld:        in  std_logic;
    upd_next:       out std_logic;
    -- CMI Output
    stored_data:    out std_logic_vector(data_size-1 downto 0);
    stored_vld:     out std_logic := '0';
    stored_next:    in  std_logic;
    -- Command
    upd_storage:    in  std_logic;
    new_data_av:    out std_logic
);

end entity sync_storage;


architecture struct of sync_storage is
    
    --======--
    -- FSMs --
    --======--
    
    type state_t is (idle, got_upd);
    signal cmi_present_state, cmi_next_state: state_t := idle;
    
    --==========--
    -- REGISTER --
    --==========--
    
    signal pre_storage:     std_logic_vector(data_size-1 downto 0);
    signal storage:         std_logic_vector(data_size-1 downto 0);
    signal marked:          std_logic := '0';
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- CMI
    signal stored_vld_int:  std_logic;
    -- Enables
    signal pre_store_en:    std_logic;
    signal mark_en:         std_logic;
    
BEGIN

    --=======--
    -- PORTS --
    --=======--
    
    new_data_av <= marked;
    
    upd_next    <= '1'; -- only the first vld data per cycle (upd_storage) is stored 
    
    --=========--
    -- MARKING --
    --=========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if pre_store_en OR upd_storage then -- if new data or reset through an update
                marked <= NOT marked;
            end if;
        end if;
    end process;

    --============--
    -- STORE DATA --
    --============--
    
    pre_store_en <= upd_vld AND NOT marked;
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if pre_store_en = '1' then -- only saves the first vld input per cycle defined by the upd_storage signal
                pre_storage <= upd_data;
            end if;
            if upd_storage = '1' then
                storage <= pre_storage;
            end if;
        end if;
    end process;

    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if stored_next = '1' then
                stored_data <= storage;
                stored_vld <= stored_vld_int;
            end if;
                    
        end if;
    end process;
    
    --=============--
    -- CMI CONTROL --
    --=============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            cmi_present_state <= cmi_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        stored_vld_int <= '0';
        
        case cmi_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if upd_storage = '1' then
                    cmi_next_state <= got_upd;
                else
                    cmi_next_state <= idle;
                end if;
                
            --===============--
            when got_upd =>
            --===============--
            
                if stored_next = '1' then
                    stored_vld_int  <= '1';
                    cmi_next_state  <= idle;
                else
                    cmi_next_state  <= got_upd;
                end if;
            
            --===============--
            when others =>
            --===============--
                
                cmi_next_state  <= idle;
                
        end case;
    end process;
    
    
end architecture struct;
