------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/02/2012
Module Name:    dreader_pkg_mapper 

-----------
Description
-----------

    
    
------------------
Generics/Constants
------------------

    none
    
-----------
Limitations
-----------
    
    none
    
----------------------------
Necessary Packages/Libraries
----------------------------

    none

-----------------
Necessary Modules
-----------------

    none
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

------------------------
Not implemented Features
------------------------

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
   
entity dreader_pkg_mapper is

port
(
    map_in:     in std_logic_vector(15 downto 0);
    map_out:    out std_logic_vector(8 downto 0)
);

end entity dreader_pkg_mapper;


architecture struct of dreader_pkg_mapper is
    
BEGIN

    --======--
    -- CASE --
    --======--

    process(all) is
    begin
        case map_in is
            
            when X"0000" => map_out <= 9X"000";
            
            when X"0100" => map_out <= 9X"002";
            
            when X"0200" => map_out <= 9X"005";
            
            when X"0300" => map_out <= 9X"00A";
            
            when X"0403" => map_out <= 9X"00C";
            when X"0405" => map_out <= 9X"00E";
            
            when X"0500" => map_out <= 9X"010";
            
            when X"0600" => map_out <= 9X"012";
            when X"0700" => map_out <= 9X"015";
            when X"0800" => map_out <= 9X"018";
            when X"0900" => map_out <= 9X"01B";
            when X"0A00" => map_out <= 9X"01E";
            when X"0B00" => map_out <= 9X"021";
            when X"0C00" => map_out <= 9X"024";
            when X"0D00" => map_out <= 9X"027";
            
            when X"0E00" => map_out <= 9X"02A";
            when X"0E01" => map_out <= 9X"02C";
            when X"0E02" => map_out <= 9X"02E";
            when X"0E03" => map_out <= 9X"030";
            when X"0E04" => map_out <= 9X"032";
            
            when X"0F01" => map_out <= 9X"034";
            when X"0F03" => map_out <= 9X"036";
            when X"1001" => map_out <= 9X"038";
            when X"1003" => map_out <= 9X"03A";
            when X"1101" => map_out <= 9X"03C";
            when X"1103" => map_out <= 9X"03E";
            when X"1201" => map_out <= 9X"040";
            when X"1203" => map_out <= 9X"042";
            when X"1301" => map_out <= 9X"044";
            when X"1303" => map_out <= 9X"046";
            when X"1401" => map_out <= 9X"048";
            when X"1403" => map_out <= 9X"04A";
            when X"1501" => map_out <= 9X"04C";
            when X"1503" => map_out <= 9X"04E";
            when X"1601" => map_out <= 9X"050";
            when X"1603" => map_out <= 9X"052";
            
            when X"1744" => map_out <= 9X"054";
            when X"1745" => map_out <= 9X"056";
            when X"1746" => map_out <= 9X"058";
            when X"1747" => map_out <= 9X"05A";
            when X"1748" => map_out <= 9X"05C";
            when X"1749" => map_out <= 9X"05E";
            when X"174A" => map_out <= 9X"060";
            when X"174B" => map_out <= 9X"062";
            when X"174C" => map_out <= 9X"064";
            when X"174D" => map_out <= 9X"066";
            when X"174E" => map_out <= 9X"068";
            when X"174F" => map_out <= 9X"06A";
            when X"1750" => map_out <= 9X"06C";
            when X"1751" => map_out <= 9X"06E";
            when X"1752" => map_out <= 9X"070";
            when X"1753" => map_out <= 9X"072";
            when X"1728" => map_out <= 9X"074";
            when X"1729" => map_out <= 9X"076";
            when X"172A" => map_out <= 9X"078";
            when X"172B" => map_out <= 9X"07A";
            when X"172C" => map_out <= 9X"07C";
            when X"172D" => map_out <= 9X"07E";
            when X"172E" => map_out <= 9X"080";
            when X"172F" => map_out <= 9X"082";
            when X"1730" => map_out <= 9X"084";
            when X"1731" => map_out <= 9X"086";
            when X"1732" => map_out <= 9X"088";
            when X"1733" => map_out <= 9X"08A";
            when X"1734" => map_out <= 9X"08C";
            when X"1735" => map_out <= 9X"08E";
            when X"1736" => map_out <= 9X"090";
            when X"1737" => map_out <= 9X"092";
            when X"1738" => map_out <= 9X"094";
            when X"1739" => map_out <= 9X"096";
            when X"173A" => map_out <= 9X"098";
            when X"173B" => map_out <= 9X"09A";
            when X"17E0" => map_out <= 9X"09C";
            when X"17E1" => map_out <= 9X"09E";
            when X"17E2" => map_out <= 9X"0A0";
            when X"17E3" => map_out <= 9X"0A2";
            when X"17E4" => map_out <= 9X"0A4";
            when X"17E5" => map_out <= 9X"0A6";
            when X"17E6" => map_out <= 9X"0A8";
            when X"17E7" => map_out <= 9X"0AA";
            when X"17E8" => map_out <= 9X"0AC";
            when X"17E9" => map_out <= 9X"0AE";
            
            when X"1800" => map_out <= 9X"0B0";
            
            when X"1944" => map_out <= 9X"0B2";
            when X"1945" => map_out <= 9X"0B4";
            when X"1946" => map_out <= 9X"0B6";
            when X"1947" => map_out <= 9X"0B8";
            when X"1948" => map_out <= 9X"0BA";
            when X"1949" => map_out <= 9X"0BC";
            when X"194A" => map_out <= 9X"0BE";
            when X"194B" => map_out <= 9X"0C0";
            when X"194C" => map_out <= 9X"0C2";
            when X"194D" => map_out <= 9X"0C4";
            when X"194E" => map_out <= 9X"0C6";
            when X"194F" => map_out <= 9X"0C8";
            when X"1950" => map_out <= 9X"0CA";
            when X"1951" => map_out <= 9X"0CC";
            when X"1952" => map_out <= 9X"0CE";
            when X"1953" => map_out <= 9X"0D0";
            when X"1928" => map_out <= 9X"0D2";
            when X"1929" => map_out <= 9X"0D4";
            when X"192A" => map_out <= 9X"0D6";
            when X"192B" => map_out <= 9X"0D8";
            when X"192C" => map_out <= 9X"0DA";
            when X"192D" => map_out <= 9X"0DC";
            when X"192E" => map_out <= 9X"0DE";
            when X"192F" => map_out <= 9X"0E0";
            when X"1930" => map_out <= 9X"0E2";
            when X"1931" => map_out <= 9X"0E4";
            when X"1932" => map_out <= 9X"0E6";
            when X"1933" => map_out <= 9X"0E8";
            when X"1934" => map_out <= 9X"0EA";
            when X"1935" => map_out <= 9X"0EC";
            when X"1936" => map_out <= 9X"0EE";
            when X"1937" => map_out <= 9X"0F0";
            when X"1938" => map_out <= 9X"0F2";
            when X"1939" => map_out <= 9X"0F4";
            when X"193A" => map_out <= 9X"0F6";
            when X"193B" => map_out <= 9X"0F8";
            when X"19E0" => map_out <= 9X"0FA";
            when X"19E1" => map_out <= 9X"0FC";
            when X"19E2" => map_out <= 9X"0FE";
            when X"19E3" => map_out <= 9X"100";
            when X"19E4" => map_out <= 9X"102";
            when X"19E5" => map_out <= 9X"104";
            when X"19E6" => map_out <= 9X"106";
            when X"19E7" => map_out <= 9X"108";
            when X"19E8" => map_out <= 9X"10A";
            when X"19E9" => map_out <= 9X"10C";
            
            when X"1A00" => map_out <= 9X"10E";
            
            when X"1B00" => map_out <= 9X"110";
            when X"1C00" => map_out <= 9X"113";
            when X"1D00" => map_out <= 9X"116";
            when X"1E00" => map_out <= 9X"119";
            when X"1F00" => map_out <= 9X"11C";
            when X"2000" => map_out <= 9X"11F";
            when X"2100" => map_out <= 9X"122";
            when X"2200" => map_out <= 9X"125";
            
            when X"FFFF" => map_out <= 9X"128";
            
            when others  => map_out <= 9X"1FF";
            
        end case;
    end process;
    
    
end architecture struct;
