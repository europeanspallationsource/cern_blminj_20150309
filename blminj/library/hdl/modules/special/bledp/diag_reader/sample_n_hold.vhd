------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/08/2012
Module Name:    sample_n_hold  

-----------
Description
-----------

    Samples the input on a given trigger, holds it there for the sample period and presents it once to the CMI output.


------------------
Generics/Constants
------------------

    data_size       := size of input/output data 

-----------
Limitations
-----------
    
    none
    
----------------------------
Necessary Packages/Libraries
----------------------------

    none

-----------------
Necessary Modules
-----------------

    CMI
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
   
entity sample_n_hold is

generic
(
    data_size: integer
);

port
(
    -- Clock
    clk:        in  std_logic;
    -- Standard Input
    inp:        in  std_logic_vector(data_size-1 downto 0);
    -- CMI Output
    cmi_data:   out std_logic_vector(data_size-1 downto 0);
    cmi_vld:    out std_logic := '0';
    cmi_next:   in  std_logic;
    -- Commands
    trigger:    in  std_logic
);

end entity sample_n_hold;


architecture struct of sample_n_hold is
    
    --======--
    -- FSMs --
    --======--
    
    type state_t is (idle, triggered);
    signal present_state, next_state: state_t := idle;
    
    --=================--
    -- GENERAL SIGNALs --
    --=================--

    signal cmi_vld_int:     std_logic;
    
BEGIN
    
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if cmi_next = '1' then
                cmi_data    <= inp;
                cmi_vld     <= cmi_vld_int;
            end if;
        end if;
    end process;
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process(all)
    begin
        -- DEFAULT
        cmi_vld_int <= '0';
        
        case present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if trigger = '1' then
                    if cmi_next = '1' then
                        cmi_vld_int <= '1';
                        next_state <= idle;
                    else
                        next_state <= triggered;
                    end if;
                else
                    next_state <= idle;
                end if;
                
            --===============--
            when triggered =>
            --===============--
                
                if cmi_next = '1' then
                    cmi_vld_int <= '1';
                    next_state <= idle;
                else
                    next_state <= triggered;
                end if;
            
         
            --===============--
            when others =>
            --===============--
                
                next_state <= idle;
                
        end case;   
            
    end process;
    
end architecture struct;
