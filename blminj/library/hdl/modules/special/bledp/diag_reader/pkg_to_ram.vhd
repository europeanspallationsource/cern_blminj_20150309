------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/02/2012
Module Name:    pkg_to_ram

-----------
Description
-----------

    Prepares the package data to be written to a RAM with via a standard RAM interface

------------------
Generics/Constants
------------------

    pkg_data_size       := size of the data part (the pkg input itself includes one more bit, the ctrl_bit)
    ram_data_size       := size of the memory data port equal to pkg_data_size - 1
    ram_addr_size       := size of the memory address

    
-----------
Limitations
-----------
    
    none
    
----------------------------
Necessary Packages/Libraries
----------------------------

    none

-----------------
Necessary Modules
-----------------

    CMI
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity pkg_to_ram is

generic
(
    pkg_data_size:  integer;
    ram_data_size:  integer;
    ram_addr_size:  integer
);

port
(
    -- Clock
    clk:        in  std_logic;
    -- CMI Input
    pkg_data:   in  std_logic_vector(pkg_data_size-1 downto 0);
    pkg_vld:    in  std_logic;
    pkg_next:   out std_logic;
    -- NIOS RAM Connection
    addr:       out std_logic_vector(ram_addr_size-1 downto 0);
    data:       out std_logic_vector(ram_data_size-1 downto 0);
    wr_en:      out std_logic;
    busy:       in  std_logic
);

end entity pkg_to_ram;


architecture struct of pkg_to_ram is
    
    --===========--
    -- REGISTERs --
    --===========--
    
    signal wraddr_reg:      UNSIGNED(ram_addr_size-1 downto 0) := (others => '0');
    
    --=================--
    -- GENERAL SIGNALs --
    --=================--
    
    signal wrdata:          UNSIGNED(pkg_data_size-2 downto 0);
    signal wraddr:          UNSIGNED(ram_addr_size-1 downto 0);
    signal ctrl_bit:        std_logic;
    signal pkg_addr:        std_logic_vector(pkg_data_size-2 downto 0);
    signal ram_addr:        std_logic_vector(ram_addr_size-1 downto 0);
    
    
BEGIN
    
    --=======--
    -- PORTS --
    --=======--
    
    pkg_next <= NOT busy when pkg_vld = '1' else
                '1';
    
    --================--
    -- ADDRESS MAPPER --
    --================--
    
    pkg_addr <= pkg_data(pkg_data_size-2 downto 0);
    
    addr_map: entity work.dreader_pkg_mapper
    PORT MAP
    (
        map_in => pkg_addr,
        map_out => ram_addr
    );
    
    
    --==============--
    -- COMBINATORIC --
    --==============--
    
    -- Data --
    wrdata <= UNSIGNED(pkg_data(pkg_data_size-2 downto 0));
    
    -- Address Select --
    ctrl_bit <= pkg_data(pkg_data_size-1);
    
    wraddr <=   UNSIGNED(ram_addr) when ctrl_bit = '1' else
                wraddr_reg + 1;
    
    
    --============--
    -- WRITE ADDR --
    --============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            wraddr_reg <= wraddr;
        end if;
    end process;
    
    
    --============--
    -- RAM ACCESS --
    --============--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if busy = '0' then
                data        <= STD_LOGIC_VECTOR(wrdata);
                addr        <= STD_LOGIC_VECTOR(wraddr);
                wr_en       <= pkg_vld;
            end if; 
        end if;
    end process;
    
    
end architecture struct;
