------------------------------
/*
Company:        CERN
Department:     BE/BI/BL
Engineer:       Marcel Alsdorf
Created on:     21/08/2012
Module Name:    scoreboard

-----------
Description
-----------

    This module adds a scoreboard at the end of the incoming package, that includes a mark for every included subpackage.

------------------
Generics/Constants
------------------
    
    pkg_data_size   := size of the package data slices    
    word_n          := number of scoreboard words (final size depends on the pkg_data_size)
    
    word_n_vsize    := vectorsize for the word counter
    
-----------
Limitations
-----------
    
    none
    
----------------------------
Necessary Packages/Libraries
----------------------------

    none

-----------------
Necessary Modules
-----------------

    mapper_16_7
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

------------------------
Not implemented Features
------------------------

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity scoreboard is

generic
(
    pkg_data_size:  integer := 17;
    word_n:         integer := 8
);

port
(
    -- Clock
    clk:                in std_logic;
    -- CMI Input
    pkg_data:           in  std_logic_vector(pkg_data_size-1 downto 0);
    pkg_vld:            in  std_logic;
    pkg_next:           out std_logic;
    -- CMI Output
    pkg_scored_data:    out std_logic_vector(pkg_data_size-1 downto 0);
    pkg_scored_vld:     out std_logic := '0';
    pkg_scored_next:    in  std_logic;
    -- Commands
    trigger:            in  std_logic
);

end entity scoreboard;


architecture struct of scoreboard is
    
    --===========--
    -- CONSTANTS --
    --===========--

    constant word_n_vsize:      integer := get_vsize(word_n);
    
    constant map_out_vsize:     integer := get_vsize_addr(word_n*16);
    --======--
    -- FSMS --
    --======--
    
    type state_t is (idle, wait_for_start, pass_data, send_sb_data, send_sb_header);
    signal present_state, next_state: state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Bit Counter
    signal word_cnt:        UNSIGNED(word_n_vsize-1 downto 0) := (others => '0');
    signal word_rst:        std_logic;
    signal word_dec:        std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal sb:              std_logic_vector(word_n*16-1 downto 0) := (others =>'0');
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal map_out:                     std_logic_vector(map_out_vsize-1 downto 0);
    signal sb_addr:                     UNSIGNED(map_out_vsize-1 downto 0);
    signal out_sel:                     std_logic_vector(1 downto 0);
    signal sb_set:                      std_logic;
    signal sb_rst:                      std_logic;
    signal ctrl_bit:                    std_logic;
    signal pkg_scored_data_int:         std_logic_vector(pkg_data_size-1 downto 0);
    signal pkg_scored_vld_int:          std_logic;
    
BEGIN

    --==========--
    -- DATAPATH --
    --==========--
    
    pkg_scored_data_int <=  pkg_data                                                                    when out_sel = "00" else
                            '1' & X"FFFF"                                                               when out_sel = "10" else
                            '0' & sb((TO_INTEGER(word_cnt)+1)*16 - 1 downto TO_INTEGER(word_cnt)*16)    when out_sel = "11" else
                            (others => '0');
    
    ctrl_bit <= pkg_data(16);
    
    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if word_rst= '1' then
                word_cnt <= TO_UNSIGNED(word_n-1, word_n_vsize);
            elsif word_dec  = '1' then
                word_cnt <= word_cnt -1;
            end if;
            
        end if;
    end process;
    
    
    --========--
    -- MAPPER --
    --========--
    
    addr_map: entity work.dreader_sb_mapper
	PORT MAP
	(
		map_in => pkg_data(15 downto 0),
		map_out => map_out
	);
    
    --============--
    -- SCOREBOARD --
    --============--
    
    sb_addr <= UNSIGNED(map_out);
    
    sb_set  <= ctrl_bit;
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if sb_rst = '1' then
                sb <= (others => '0');
            elsif sb_set = '1' then
                sb(TO_INTEGER(sb_addr)) <= '1';
            end if;
            
        end if;
    end process;
    
    
    --============--
    -- CMI OUTREG --
    --============--
    
	process (clk) is
	begin
		if rising_edge(clk) then
			
            if pkg_scored_next = '1' then
				pkg_scored_data 	<= pkg_scored_data_int;
				pkg_scored_vld	    <= pkg_scored_vld_int;
			end if;
			
		end if;
	end process;
    

    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        pkg_next            <= '1';
        pkg_scored_vld_int  <= '0';
        -- select output 
        out_sel             <= "00";
        -- Scoreboard Reset
        sb_rst              <= '0';
        -- Word Cnt
        word_rst            <= '0';
        word_dec            <= '0';
        
        
        case present_state is
            
            --===============--
            when idle =>
            --===============--
            
                if trigger = '1' then
                    next_state <= wait_for_start;
                else
                    next_state <= idle;
                end if;
                
            --===============--
            when wait_for_start =>
            --===============--
            
                if pkg_vld = '1' then
                    pkg_next            <= pkg_scored_next;
                    pkg_scored_vld_int  <= '1';
                    next_state          <= pass_data;
                else
                    pkg_next            <= '1';
                    next_state          <= wait_for_start;
                end if;
            
            --===============--
            when pass_data =>
            --===============--
                
                if pkg_vld = '0' then -- end of package
                    pkg_next            <= '1';
                    next_state          <= send_sb_header;
                else
                    pkg_next            <= pkg_scored_next;
                    pkg_scored_vld_int  <= '1';
                    next_state          <= pass_data;
                end if;
                
            --===============--
            when send_sb_header =>
            --===============--
                    
                if pkg_scored_next = '1' then
                    out_sel             <= "10";
                    pkg_scored_vld_int  <= '1';
                    next_state          <= send_sb_data;
                else
                    next_state          <= send_sb_header;
                end if;
                
            --===============--
            when send_sb_data =>
            --===============--
                
                if pkg_scored_next = '1' then
                    out_sel             <= "11";
                    pkg_scored_vld_int  <= '1';
                    if word_cnt = 0 then
                        word_rst        <= '1';
                        sb_rst          <= '1';
                        next_state      <= idle;
                    else
                        word_dec         <= '1';
                        next_state      <= send_sb_data;
                    end if;
                else
                    next_state <= send_sb_data;
                end if;
                
            --===============--
            when others =>
            --===============--
                    
                next_state <= idle;
            
        end case;
    end process;
    
end architecture struct;
