------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/08/2012
Module Name:    priority_mux

-----------
Description
-----------

    Priority Multiplexer to give priority in order of the inputs. It leaves the input, if there is no incoming data anymore.

--------
Generics
--------

    data_size   := size of the input and output data
    n           := number of inputs 
    
    sel_vsize   := vectorsize for the selection counter for the n inputs
    
-----------
Limitations
-----------

    

----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg

-----------------
Necessary Modules
-----------------

    CMI
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity priority_mux is

generic
(
    data_size:  integer;
    n:          integer
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Inputs
    inp_data:       in  slv_array(n-1 downto 0) (data_size-1 downto 0);
    inp_vld:        in  std_logic_vector(n-1 downto 0);
    inp_next:       out std_logic_vector(n-1 downto 0);
    -- CMI Output
    muxxed_data:    out std_logic_vector(data_size-1 downto 0);
    muxxed_vld:     out std_logic := '0';
    muxxed_next:    in  std_logic
);

end entity priority_mux;


architecture struct of priority_mux is
    
    --===========--
    -- CONSTANTS --
    --===========--

    constant sel_vsize:     integer := get_vsize(n);
    
    --===========--
    -- REGISTERs --
    --===========--
    
    signal inp_lock:        UNSIGNED(sel_vsize-1 downto 0) := (sel_vsize-1 downto 0 => '0');
    
    --=================--
    -- GENERAL SIGNALs --
    --=================--

    signal inp_sel:         UNSIGNED(sel_vsize-1 downto 0) := (sel_vsize-1 downto 0 => '0');
    signal muxxed_vld_int:  std_logic;
    
BEGIN

    --==========--
    -- INP LOCK --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            inp_lock <= inp_sel;
        end if;
    end process;
    
    
    --==============--
    -- COMBINATORIC --
    --==============--
    
    -- choose input --
    process(all) is
    begin
        --DEFAULTS
        inp_sel         <= (sel_vsize-1 downto 0 => '0');
        muxxed_vld_int  <= '0';
            
            if inp_vld(TO_INTEGER(inp_lock)) = '1' then
                inp_sel         <= inp_lock;
                muxxed_vld_int  <= '1';
            else
                for k in n-1 downto 0 loop
                    if inp_vld(k) = '1' then
                        inp_sel         <= TO_UNSIGNED(k, sel_vsize);
                        muxxed_vld_int  <= '1';
                    end if;
                end loop;
            end if;
    end process;
    
    -- setting input next --
    process(all) is
    begin
        --DEFAULTS
        for i in n-1 downto 0 loop
            inp_next(i) <= NOT inp_vld(i);
        end loop;
        
        inp_next(TO_INTEGER(inp_sel)) <= muxxed_next OR NOT inp_vld(TO_INTEGER(inp_sel));
        
    end process;
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if muxxed_next = '1' then
                muxxed_vld  <= muxxed_vld_int;
                muxxed_data <= inp_data(TO_INTEGER(inp_sel));
            end if;
                    
        end if;
    end process;
    
    
end architecture struct;
