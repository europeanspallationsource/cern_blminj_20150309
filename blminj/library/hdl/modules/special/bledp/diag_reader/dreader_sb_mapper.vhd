------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/02/2012
Module Name:    dreader_sb_mapper


-----------
Description
-----------

    
    
------------------
Generics/Constants
------------------

    none
    
-----------
Limitations
-----------
    
    none
    
----------------------------
Necessary Packages/Libraries
----------------------------

    none

-----------------
Necessary Modules
-----------------

    none
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

------------------------
Not implemented Features
------------------------

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity dreader_sb_mapper is

port
(
    map_in:     in std_logic_vector(15 downto 0);
    map_out:    out std_logic_vector(7 downto 0)
);

end entity dreader_sb_mapper;


architecture struct of dreader_sb_mapper is
    
BEGIN

    --======--
    -- CASE --
    --======--

    process(all) is
    begin
        case map_in is
            
            when X"0000" => map_out <= X"00";
            
            when X"0100" => map_out <= X"01";
            
            when X"0200" => map_out <= X"02";
            
            when X"0300" => map_out <= X"03";
            
            when X"0403" => map_out <= X"04";
            when X"0405" => map_out <= X"05";
            
            when X"0500" => map_out <= X"06";
            when X"0600" => map_out <= X"07";
            when X"0700" => map_out <= X"08";
            when X"0800" => map_out <= X"09";
            when X"0900" => map_out <= X"0A";
            when X"0A00" => map_out <= X"0B";
            when X"0B00" => map_out <= X"0C";
            when X"0C00" => map_out <= X"0D";
            when X"0D00" => map_out <= X"0E";
            
            when X"0E00" => map_out <= X"0F";
            when X"0E01" => map_out <= X"10";
            when X"0E02" => map_out <= X"11";
            when X"0E03" => map_out <= X"12";
            when X"0E04" => map_out <= X"13";
            
            when X"0F01" => map_out <= X"14";
            when X"0F03" => map_out <= X"15";
            when X"1001" => map_out <= X"16";
            when X"1003" => map_out <= X"17";
            when X"1101" => map_out <= X"18";
            when X"1103" => map_out <= X"19";
            when X"1201" => map_out <= X"1A";
            when X"1203" => map_out <= X"1B";
            when X"1301" => map_out <= X"1C";
            when X"1303" => map_out <= X"1D";
            when X"1401" => map_out <= X"1E";
            when X"1403" => map_out <= X"1F";
            when X"1501" => map_out <= X"20";
            when X"1503" => map_out <= X"21";
            when X"1601" => map_out <= X"22";
            when X"1603" => map_out <= X"23";
            
            when X"1744" => map_out <= X"24";
            when X"1745" => map_out <= X"25";
            when X"1746" => map_out <= X"26";
            when X"1747" => map_out <= X"27";
            when X"1748" => map_out <= X"28";
            when X"1749" => map_out <= X"29";
            when X"174A" => map_out <= X"2A";
            when X"174B" => map_out <= X"2B";
            when X"174C" => map_out <= X"2C";
            when X"174D" => map_out <= X"2D";
            when X"174E" => map_out <= X"2E";
            when X"174F" => map_out <= X"2F";
            when X"1750" => map_out <= X"30";
            when X"1751" => map_out <= X"31";
            when X"1752" => map_out <= X"32";
            when X"1753" => map_out <= X"33";
            when X"1728" => map_out <= X"34";
            when X"1729" => map_out <= X"35";
            when X"172A" => map_out <= X"36";
            when X"172B" => map_out <= X"37";
            when X"172C" => map_out <= X"38";
            when X"172D" => map_out <= X"39";
            when X"172E" => map_out <= X"3A";
            when X"172F" => map_out <= X"3B";
            when X"1730" => map_out <= X"3C";
            when X"1731" => map_out <= X"3D";
            when X"1732" => map_out <= X"3E";
            when X"1733" => map_out <= X"3F";
            when X"1734" => map_out <= X"40";
            when X"1735" => map_out <= X"41";
            when X"1736" => map_out <= X"42";
            when X"1737" => map_out <= X"43";
            when X"1738" => map_out <= X"44";
            when X"1739" => map_out <= X"45";
            when X"173A" => map_out <= X"46";
            when X"173B" => map_out <= X"47";
            when X"17E0" => map_out <= X"48";
            when X"17E1" => map_out <= X"49";
            when X"17E2" => map_out <= X"4A";
            when X"17E3" => map_out <= X"4B";
            when X"17E4" => map_out <= X"4C";
            when X"17E5" => map_out <= X"4D";
            when X"17E6" => map_out <= X"4E";
            when X"17E7" => map_out <= X"4F";
            when X"17E8" => map_out <= X"50";
            when X"17E9" => map_out <= X"51";
            
            when X"1800" => map_out <= X"52";
            
            when X"1944" => map_out <= X"53";
            when X"1945" => map_out <= X"54";
            when X"1946" => map_out <= X"55";
            when X"1947" => map_out <= X"56";
            when X"1948" => map_out <= X"57";
            when X"1949" => map_out <= X"58";
            when X"194A" => map_out <= X"59";
            when X"194B" => map_out <= X"5A";
            when X"194C" => map_out <= X"5B";
            when X"194D" => map_out <= X"5C";
            when X"194E" => map_out <= X"5D";
            when X"194F" => map_out <= X"5E";
            when X"1950" => map_out <= X"5F";
            when X"1951" => map_out <= X"60";
            when X"1952" => map_out <= X"61";
            when X"1953" => map_out <= X"62";
            when X"1928" => map_out <= X"63";
            when X"1929" => map_out <= X"64";
            when X"192A" => map_out <= X"65";
            when X"192B" => map_out <= X"66";
            when X"192C" => map_out <= X"67";
            when X"192D" => map_out <= X"68";
            when X"192E" => map_out <= X"69";
            when X"192F" => map_out <= X"6A";
            when X"1930" => map_out <= X"6B";
            when X"1931" => map_out <= X"6C";
            when X"1932" => map_out <= X"6D";
            when X"1933" => map_out <= X"6E";
            when X"1934" => map_out <= X"6F";
            when X"1935" => map_out <= X"70";
            when X"1936" => map_out <= X"71";
            when X"1937" => map_out <= X"72";
            when X"1938" => map_out <= X"73";
            when X"1939" => map_out <= X"74";
            when X"193A" => map_out <= X"75";
            when X"193B" => map_out <= X"76";
            when X"19E0" => map_out <= X"77";
            when X"19E1" => map_out <= X"78";
            when X"19E2" => map_out <= X"79";
            when X"19E3" => map_out <= X"7A";
            when X"19E4" => map_out <= X"7B";
            when X"19E5" => map_out <= X"7C";
            when X"19E6" => map_out <= X"7D";
            when X"19E7" => map_out <= X"7E";
            when X"19E8" => map_out <= X"7F";
            when X"19E9" => map_out <= X"80";
            
            when X"1A00" => map_out <= X"81";
            
            when X"1B00" => map_out <= X"82";
            when X"1C00" => map_out <= X"83";
            when X"1D00" => map_out <= X"84";
            when X"1E00" => map_out <= X"85";
            when X"1F00" => map_out <= X"86";
            when X"2000" => map_out <= X"87";
            when X"2100" => map_out <= X"88";
            when X"2200" => map_out <= X"89";
           
            when others  => map_out <= X"FF";
            
        end case;
    end process;
    
    
end architecture struct;
