------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/08/2012
Module Name:    bledp_diag_reader
Part of:        bledp_op

-----------
Description
-----------

    Topmodule that connects all modules that are part of the diagnostic reader for the BLEDP.


------------------
Generics/Constants
------------------

    pkg_data_size   := size of the packaged output (including the control bit)
    pkg_tag_size    := size of the tag part in the header
    
    intf_n          := number of incorporated interfaces 
    ifXX_rdreq_size := size of the interface rdreq vector
    ifXX_rdout_size := size of the interface rdout vector
    ifXX_tag_size   := size of the tag part in the rdout vector
    ifXX_n_tags     := number of different possible tags of this interface
    ifXX_list_tags  := list of these specific tag (starting at vectorposition 0)
    ifXX_init_file  := init file for the seq_generator memory (all read requests)

-----------
Limitations
-----------

    < limitations for the generics and/or the module itself >

    
----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

    CMI
    DS2411
    AD7927
    AD5252
    SHT15
    SFP_EEMEM
    sample_n_hold
    store_n_pkg
    seq_generator
    sample_cnt
    priority_mux
    

---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    < detailed descriptions of the internal functionality of the module including port usage >

    
------------------------
Not implemented Features
------------------------

    < informations about missing features or nice-to-have addons >


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity bledp_diag_reader is

generic
(
    Tclk_ns:        integer := 8;
    pkg_data_size:  integer := 17;
    pkg_tag_size:   integer := 8;
    sfp1_n_tags:    integer := 46;                  -- 46 for Optical SFP, 36 for Ethernet SFP
    sfp1_mif_file:  string  := "sgen_sfp_opt.mif";  -- _opt for optical, _eth for ethernet
    sfp2_n_tags:    integer := 36;                  -- 46 for Optical SFP, 36 for Ethernet SFP
    sfp2_mif_file:  string  := "sgen_sfp_eth.mif"   -- _opt for optical, _eth for ethernet
    
);

port
(   
    -- Clock
    clk:                in  std_logic;
    
    -- Reader Input
    -- INTF 0x00
    POWERGOOD_1_2:      in  std_logic;
    POWERGOOD_CB1:      in  std_logic;
    POWERGOOD_CB2:      in  std_logic;
    CB1_ACT:            in  std_logic;
    CB2_ACT:            in  std_logic;
    DCDC_EN:            in  std_logic;
    -- INTF 0x01
    bledp_fw:           in  std_logic_vector(31 downto 0);
    -- INTF 0x02
    ID:                 inout std_logic;
    -- INTF 0x03
    global_addr:        in  std_logic_vector(3 downto 0);
    -- INTF 0x04
    SHT15_SCK:          out std_logic;
    SHT15_DATA:         inout std_logic;
    -- INTF 0x05
    RELAY_ACT:          in std_logic_vector(8 downto 1);
    RELAYS_STATUS:      in std_logic;
    -- INTF 0x06 - 0x0D
    STOPOUT:            in std_logic_vector(8 downto 1);
    STOPOUT_smpl:       in std_logic_vector(8 downto 1);
    -- INTF 0x0E
    AD_I_CLK:           out std_logic;
    AD_I_DIN:           out std_logic;
    AD_I_CS_N:          out std_logic;
    AD_I_DOUT:          in std_logic;
    -- INTF 0x0F - 0x16
    SDA:                inout std_logic_vector(8 downto 1);
    SCL:                out std_logic_vector(8 downto 1);
    WP:                 out std_logic_vector(8 downto 1);
    CB1_wakeup:         in  std_logic;
    CB2_wakeup:         in  std_logic;
    DP_wr_req_data:     in  slv_array (8 downto 1) (17 downto 0);
    DP_wr_req_vld:      in  std_logic_vector(8 downto 1);
    DP_wr_req_next:     out std_logic_vector(8 downto 1);
    
    -- INTF 0x17
    SDA_OPT_1:          inout std_logic;
    SCL_OPT_1:          out std_logic;
    SFP1_wr_req_data:   in  std_logic_vector (16 downto 0);
    SFP1_wr_req_vld:    in  std_logic;
    SFP1_wr_req_next:   out std_logic;
    -- INTF 0x18
    LOSS_OF_SIGNAL_1:   in  std_logic;
    MODULE_DETECT_1:    in  std_logic;
    TX_FAULT_1:         in  std_logic;
    RATE_SELECT_1:      in  std_logic;
    TX_DISABLE_1:       in  std_logic;
    -- INTF 0x19
    SDA_OPT_2:          inout std_logic;
    SCL_OPT_2:          out std_logic;
    SFP2_wr_req_data:   in  std_logic_vector (16 downto 0);
    SFP2_wr_req_vld:    in  std_logic;
    SFP2_wr_req_next:   out std_logic;
    -- INTF 0x1A
    LOSS_OF_SIGNAL_2:   in  std_logic;
    MODULE_DETECT_2:    in  std_logic;
    TX_FAULT_2:         in  std_logic;
    RATE_SELECT_2:      in  std_logic;
    TX_DISABLE_2:       in  std_logic;
    -- INTF 0x1B-0x22
    DAmode:             in std_logic_vector(8 downto 1);
    DAmode_smpl:        in std_logic_vector(8 downto 1);
    
    -- CMI Output
    diag_data:          out std_logic_vector(pkg_data_size-1 downto 0);
    diag_vld:           out std_logic := '0';
    diag_next:          in  std_logic;
    -- Timer
    upd:                in  std_logic
);

end entity bledp_diag_reader;


architecture struct of bledp_diag_reader is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- Monitoring (to be moved outside in the future)
    constant SFP1_Temp_H:           std_logic := '0';
    constant SFP1_Temp_L:           std_logic := '0';
    constant SFP1_RXPWR_H:          std_logic := '0';
    constant SFP1_RXPWR_L:          std_logic := '0';
    constant SFP1_TXPWR_H:          std_logic := '0';
    constant SFP1_TXPWR_L:          std_logic := '0';
    constant SFP1_VCC_H:            std_logic := '0';
    constant SFP1_VCC_L:            std_logic := '0';
    constant SFP1_TXBias_H:         std_logic := '0';
    constant SFP1_TXBias_L:         std_logic := '0';
    
    constant SFP2_Temp_H:           std_logic := '0';
    constant SFP2_Temp_L:           std_logic := '0';
    constant SFP2_RXPWR_H:          std_logic := '0';
    constant SFP2_RXPWR_L:          std_logic := '0';
    constant SFP2_TXPWR_H:          std_logic := '0';
    constant SFP2_TXPWR_L:          std_logic := '0';
    constant SFP2_VCC_H:            std_logic := '0';
    constant SFP2_VCC_L:            std_logic := '0';
    constant SFP2_TXBias_H:         std_logic := '0';
    constant SFP2_TXBias_L:         std_logic := '0';
    
    -- # of interfaces
    constant intf_n:                integer := 35;
    
    -- INTF RDREQ Sizes
    constant if04_rdreq_size:       integer := 8;
    constant if0E_rdreq_size:       integer := 3;
    constant if0F_rdreq_size:       integer := 10;
    constant if10_rdreq_size:       integer := 10;
    constant if11_rdreq_size:       integer := 10;
    constant if12_rdreq_size:       integer := 10;
    constant if13_rdreq_size:       integer := 10;
    constant if14_rdreq_size:       integer := 10;
    constant if15_rdreq_size:       integer := 10;
    constant if16_rdreq_size:       integer := 10;
    constant if17_rdreq_size:       integer := 9;
    constant if19_rdreq_size:       integer := 9;
    
    -- INTF Readout Sizes
    constant if00_rdout_size:       integer := 6;
    constant if01_rdout_size:       integer := 32;
    constant if02_rdout_size:       integer := 64;
    constant if03_rdout_size:       integer := 4;
    constant if04_rdout_size:       integer := 16;
    constant if05_rdout_size:       integer := 9;
    constant if06_rdout_size:       integer := 32;
    constant if07_rdout_size:       integer := 32;
    constant if08_rdout_size:       integer := 32;
    constant if09_rdout_size:       integer := 32;
    constant if0A_rdout_size:       integer := 32;
    constant if0B_rdout_size:       integer := 32;
    constant if0C_rdout_size:       integer := 32;
    constant if0D_rdout_size:       integer := 32;
    constant if0E_rdout_size:       integer := 12;
    constant if0F_rdout_size:       integer := 8;
    constant if10_rdout_size:       integer := 8;
    constant if11_rdout_size:       integer := 8;
    constant if12_rdout_size:       integer := 8;
    constant if13_rdout_size:       integer := 8;
    constant if14_rdout_size:       integer := 8;
    constant if15_rdout_size:       integer := 8;
    constant if16_rdout_size:       integer := 8;
    constant if17_rdout_size:       integer := 8;
    constant if18_rdout_size:       integer := 15;
    constant if19_rdout_size:       integer := 8;
    constant if1A_rdout_size:       integer := 15;
    constant if1B_rdout_size:       integer := 32;
    constant if1C_rdout_size:       integer := 32;
    constant if1D_rdout_size:       integer := 32;
    constant if1E_rdout_size:       integer := 32;
    constant if1F_rdout_size:       integer := 32;
    constant if20_rdout_size:       integer := 32;
    constant if21_rdout_size:       integer := 32;
    constant if22_rdout_size:       integer := 32;
    
    
    -- INTF Tag Sizes
    constant if00_tag_size:         integer := 0;
    constant if01_tag_size:         integer := 0;
    constant if02_tag_size:         integer := 0;
    constant if03_tag_size:         integer := 0;
    constant if04_tag_size:         integer := 5;
    constant if05_tag_size:         integer := 0;
    constant if06_tag_size:         integer := 0;
    constant if07_tag_size:         integer := 0;
    constant if08_tag_size:         integer := 0;
    constant if09_tag_size:         integer := 0;
    constant if0A_tag_size:         integer := 0;
    constant if0B_tag_size:         integer := 0;
    constant if0C_tag_size:         integer := 0;
    constant if0D_tag_size:         integer := 0;
    constant if0E_tag_size:         integer := 3;
    constant if0F_tag_size:         integer := 5;
    constant if10_tag_size:         integer := 5;
    constant if11_tag_size:         integer := 5;
    constant if12_tag_size:         integer := 5;
    constant if13_tag_size:         integer := 5;
    constant if14_tag_size:         integer := 5;
    constant if15_tag_size:         integer := 5;
    constant if16_tag_size:         integer := 5;
    constant if17_tag_size:         integer := 8;
    constant if18_tag_size:         integer := 0;
    constant if19_tag_size:         integer := 8;
    constant if1A_tag_size:         integer := 0;
    constant if1B_tag_size:         integer := 0;
    constant if1C_tag_size:         integer := 0;
    constant if1D_tag_size:         integer := 0;
    constant if1E_tag_size:         integer := 0;
    constant if1F_tag_size:         integer := 0;
    constant if20_tag_size:         integer := 0;
    constant if21_tag_size:         integer := 0;
    constant if22_tag_size:         integer := 0;
    
    
    -- INTF #Tags
    constant if00_n_tags:           integer := 1;
    constant if01_n_tags:           integer := 1;
    constant if02_n_tags:           integer := 1;
    constant if03_n_tags:           integer := 1;
    constant if04_n_tags:           integer := 2;
    constant if05_n_tags:           integer := 1;
    constant if06_n_tags:           integer := 1;
    constant if07_n_tags:           integer := 1;
    constant if08_n_tags:           integer := 1;
    constant if09_n_tags:           integer := 1;
    constant if0A_n_tags:           integer := 1;
    constant if0B_n_tags:           integer := 1;
    constant if0C_n_tags:           integer := 1;
    constant if0D_n_tags:           integer := 1;
    constant if0E_n_tags:           integer := 5;
    constant if0F_n_tags:           integer := 2;
    constant if10_n_tags:           integer := 2;
    constant if11_n_tags:           integer := 2;
    constant if12_n_tags:           integer := 2;
    constant if13_n_tags:           integer := 2;
    constant if14_n_tags:           integer := 2;
    constant if15_n_tags:           integer := 2;
    constant if16_n_tags:           integer := 2;
    constant if17_n_tags:           integer := sfp1_n_tags; 
    constant if18_n_tags:           integer := 1;
    constant if19_n_tags:           integer := sfp2_n_tags;
    constant if1A_n_tags:           integer := 1;
    constant if1B_n_tags:           integer := 1;
    constant if1C_n_tags:           integer := 1;
    constant if1D_n_tags:           integer := 1;
    constant if1E_n_tags:           integer := 1;
    constant if1F_n_tags:           integer := 1;
    constant if20_n_tags:           integer := 1;
    constant if21_n_tags:           integer := 1;
    constant if22_n_tags:           integer := 1;
    
    
    -- INTF List of tags
    constant if00_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if01_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if02_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if03_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if04_list_tags:        int_array(63 downto 0)  := (1 => 5, 0 => 3, others => 0);
    constant if05_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if06_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if07_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if08_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if09_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if0A_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if0B_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if0C_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if0D_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if0E_list_tags:        int_array(63 downto 0)  := (4 => 4, 3 => 3, 2 => 2, 1 => 1, 0 => 0, others => 0);
    constant if0F_list_tags:        int_array(63 downto 0)  := (1 => 3, 0 => 1, others => 0);
    constant if10_list_tags:        int_array(63 downto 0)  := (1 => 3, 0 => 1, others => 0);
    constant if11_list_tags:        int_array(63 downto 0)  := (1 => 3, 0 => 1, others => 0);
    constant if12_list_tags:        int_array(63 downto 0)  := (1 => 3, 0 => 1, others => 0);
    constant if13_list_tags:        int_array(63 downto 0)  := (1 => 3, 0 => 1, others => 0);
    constant if14_list_tags:        int_array(63 downto 0)  := (1 => 3, 0 => 1, others => 0);
    constant if15_list_tags:        int_array(63 downto 0)  := (1 => 3, 0 => 1, others => 0);
    constant if16_list_tags:        int_array(63 downto 0)  := (1 => 3, 0 => 1, others => 0);
    constant if17_list_tags:        int_array(63 downto 0)  := (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,233,232,231,230,229,228,227,226,225,224,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68);
    constant if18_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if19_list_tags:        int_array(63 downto 0)  := (0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,233,232,231,230,229,228,227,226,225,224,59,58,57,56,55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,83,82,81,80,79,78,77,76,75,74,73,72,71,70,69,68);
    constant if1A_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if1B_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if1C_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if1D_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if1E_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if1F_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if20_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if21_list_tags:        int_array(63 downto 0)  := (others => 0);
    constant if22_list_tags:        int_array(63 downto 0)  := (others => 0);

    
    
    -- INTF Init Files
    constant if04_init_file:        string := "sgen_sht.mif";
    constant if0E_init_file:        string := "sgen_ad7927.mif";
    constant if0F_init_file:        string := "sgen_ad5252.mif";
    constant if10_init_file:        string := "sgen_ad5252.mif";
    constant if11_init_file:        string := "sgen_ad5252.mif";
    constant if12_init_file:        string := "sgen_ad5252.mif";
    constant if13_init_file:        string := "sgen_ad5252.mif";
    constant if14_init_file:        string := "sgen_ad5252.mif";
    constant if15_init_file:        string := "sgen_ad5252.mif";
    constant if16_init_file:        string := "sgen_ad5252.mif";
    constant if17_init_file:        string := sfp1_mif_file;
    constant if19_init_file:        string := sfp2_mif_file;
    
    --=================--
    -- GENERAL SIGNALs --
    --=================--

    -- INTF 0x00
    signal if00_snp_tx_data:    std_logic_vector(if00_rdout_size+if00_tag_size-1 downto 0);
    signal if00_snp_tx_vld:     std_logic;
    signal if00_snp_tx_next:    std_logic;
    
    -- INTF 0x01
    signal if01_snp_tx_data:    std_logic_vector(if01_rdout_size+if01_tag_size-1 downto 0);
    signal if01_snp_tx_vld:     std_logic;
    signal if01_snp_tx_next:    std_logic;
    
    -- INTF 0x02
    signal if02_snp_tx_data:    std_logic_vector(if02_rdout_size+if02_tag_size-1 downto 0);
    signal if02_snp_tx_vld:     std_logic;
    signal if02_snp_tx_next:    std_logic;
    
    -- INTF 0x03
    signal if03_snp_tx_data:    std_logic_vector(if03_rdout_size+if03_tag_size-1 downto 0);
    signal if03_snp_tx_vld:     std_logic;
    signal if03_snp_tx_next:    std_logic;
    
    -- INTF 0x04
    signal if04_sgen_tx_data:   std_logic_vector(if04_rdreq_size-1 downto 0);
    signal if04_sgen_tx_vld:    std_logic;
    signal if04_sgen_tx_next:   std_logic;
    
    signal if04_sgen_rx_data:   std_logic_vector(if04_rdreq_size-1 downto 0);
    signal if04_sgen_rx_vld:    std_logic;
    signal if04_sgen_rx_next:   std_logic;
    
    signal if04_snp_tx_data:    std_logic_vector(if04_rdout_size+if04_tag_size-1 downto 0);
    signal if04_snp_tx_vld:     std_logic;
    signal if04_snp_tx_next:    std_logic;
    
    -- INTF 0x05
    signal if05_snp_tx_data:    std_logic_vector(if05_rdout_size+if05_tag_size-1 downto 0);
    signal if05_snp_tx_vld:     std_logic;
    signal if05_snp_tx_next:    std_logic;
    
    -- INTF 0x06
    signal if06_snp_tx_data:    std_logic_vector(if06_rdout_size+if06_tag_size-1 downto 0);
    signal if06_snp_tx_vld:     std_logic;
    signal if06_snp_tx_next:    std_logic;
    
    -- INTF 0x07
    signal if07_snp_tx_data:    std_logic_vector(if07_rdout_size+if07_tag_size-1 downto 0);
    signal if07_snp_tx_vld:     std_logic;
    signal if07_snp_tx_next:    std_logic;
    
    -- INTF 0x08
    signal if08_snp_tx_data:    std_logic_vector(if08_rdout_size+if08_tag_size-1 downto 0);
    signal if08_snp_tx_vld:     std_logic;
    signal if08_snp_tx_next:    std_logic;
    
    -- INTF 0x09
    signal if09_snp_tx_data:    std_logic_vector(if09_rdout_size+if09_tag_size-1 downto 0);
    signal if09_snp_tx_vld:     std_logic;
    signal if09_snp_tx_next:    std_logic;
    
    -- INTF 0x0A
    signal if0A_snp_tx_data:    std_logic_vector(if0A_rdout_size+if0A_tag_size-1 downto 0);
    signal if0A_snp_tx_vld:     std_logic;
    signal if0A_snp_tx_next:    std_logic;
    
    -- INTF 0x0B
    signal if0B_snp_tx_data:    std_logic_vector(if0B_rdout_size+if0B_tag_size-1 downto 0);
    signal if0B_snp_tx_vld:     std_logic;
    signal if0B_snp_tx_next:    std_logic;
    
    -- INTF 0x0C
    signal if0C_snp_tx_data:    std_logic_vector(if0C_rdout_size+if0C_tag_size-1 downto 0);
    signal if0C_snp_tx_vld:     std_logic;
    signal if0C_snp_tx_next:    std_logic;
    
    -- INTF 0x0D
    signal if0D_snp_tx_data:    std_logic_vector(if0D_rdout_size+if0D_tag_size-1 downto 0);
    signal if0D_snp_tx_vld:     std_logic;
    signal if0D_snp_tx_next:    std_logic;
    
    -- INTF 0x0E
    signal if0E_sgen_tx_data:   std_logic_vector(if0E_rdreq_size-1 downto 0);
    signal if0E_sgen_tx_vld:    std_logic;
    signal if0E_sgen_tx_next:   std_logic;
    
    signal if0E_sgen_rx_data:   std_logic_vector(if0E_rdreq_size-1 downto 0);
    signal if0E_sgen_rx_vld:    std_logic;
    signal if0E_sgen_rx_next:   std_logic;
    
    signal if0E_snp_tx_data:    std_logic_vector(if0E_rdout_size+if0E_tag_size-1 downto 0);
    signal if0E_snp_tx_vld:     std_logic;
    signal if0E_snp_tx_next:    std_logic;
    
    -- INTF 0x0F
    signal if0F_sgen_tx_data:   std_logic_vector(if0F_rdreq_size-1 downto 0);
    signal if0F_sgen_tx_vld:    std_logic;
    signal if0F_sgen_tx_next:   std_logic;
    
    signal if0F_sgen_rx_data:   std_logic_vector(if0F_rdreq_size-1 downto 0);
    signal if0F_sgen_rx_vld:    std_logic;
    signal if0F_sgen_rx_next:   std_logic;
    
    signal if0F_snp_tx_data:    std_logic_vector(if0F_rdout_size+if0F_tag_size-1 downto 0);
    signal if0F_snp_tx_vld:     std_logic;
    signal if0F_snp_tx_next:    std_logic;
    
    -- INTF 0x10
    signal if10_sgen_tx_data:   std_logic_vector(if10_rdreq_size-1 downto 0);
    signal if10_sgen_tx_vld:    std_logic;
    signal if10_sgen_tx_next:   std_logic;
    
    signal if10_sgen_rx_data:   std_logic_vector(if10_rdreq_size-1 downto 0);
    signal if10_sgen_rx_vld:    std_logic;
    signal if10_sgen_rx_next:   std_logic;
    
    signal if10_snp_tx_data:    std_logic_vector(if10_rdout_size+if10_tag_size-1 downto 0);
    signal if10_snp_tx_vld:     std_logic;
    signal if10_snp_tx_next:    std_logic;
    
    -- INTF 0x11
    signal if11_sgen_tx_data:   std_logic_vector(if11_rdreq_size-1 downto 0);
    signal if11_sgen_tx_vld:    std_logic;
    signal if11_sgen_tx_next:   std_logic;
    
    signal if11_sgen_rx_data:   std_logic_vector(if11_rdreq_size-1 downto 0);
    signal if11_sgen_rx_vld:    std_logic;
    signal if11_sgen_rx_next:   std_logic;
    
    signal if11_snp_tx_data:    std_logic_vector(if11_rdout_size+if11_tag_size-1 downto 0);
    signal if11_snp_tx_vld:     std_logic;
    signal if11_snp_tx_next:    std_logic;
    
    -- INTF 0x12
    signal if12_sgen_tx_data:   std_logic_vector(if12_rdreq_size-1 downto 0);
    signal if12_sgen_tx_vld:    std_logic;
    signal if12_sgen_tx_next:   std_logic;
    
    signal if12_sgen_rx_data:   std_logic_vector(if12_rdreq_size-1 downto 0);
    signal if12_sgen_rx_vld:    std_logic;
    signal if12_sgen_rx_next:   std_logic;
    
    signal if12_snp_tx_data:    std_logic_vector(if12_rdout_size+if12_tag_size-1 downto 0);
    signal if12_snp_tx_vld:     std_logic;
    signal if12_snp_tx_next:    std_logic;

    -- INTF 0x13
    signal if13_sgen_tx_data:   std_logic_vector(if13_rdreq_size-1 downto 0);
    signal if13_sgen_tx_vld:    std_logic;
    signal if13_sgen_tx_next:   std_logic;
    
    signal if13_sgen_rx_data:   std_logic_vector(if13_rdreq_size-1 downto 0);
    signal if13_sgen_rx_vld:    std_logic;
    signal if13_sgen_rx_next:   std_logic;
    
    signal if13_snp_tx_data:    std_logic_vector(if13_rdout_size+if13_tag_size-1 downto 0);
    signal if13_snp_tx_vld:     std_logic;
    signal if13_snp_tx_next:    std_logic;
    
    -- INTF 0x14
    signal if14_sgen_tx_data:   std_logic_vector(if14_rdreq_size-1 downto 0);
    signal if14_sgen_tx_vld:    std_logic;
    signal if14_sgen_tx_next:   std_logic;
    
    signal if14_sgen_rx_data:   std_logic_vector(if14_rdreq_size-1 downto 0);
    signal if14_sgen_rx_vld:    std_logic;
    signal if14_sgen_rx_next:   std_logic;
    
    signal if14_snp_tx_data:    std_logic_vector(if14_rdout_size+if14_tag_size-1 downto 0);
    signal if14_snp_tx_vld:     std_logic;
    signal if14_snp_tx_next:    std_logic;
    
    -- INTF 0x15
    signal if15_sgen_tx_data:   std_logic_vector(if15_rdreq_size-1 downto 0);
    signal if15_sgen_tx_vld:    std_logic;
    signal if15_sgen_tx_next:   std_logic;
    
    signal if15_sgen_rx_data:   std_logic_vector(if15_rdreq_size-1 downto 0);
    signal if15_sgen_rx_vld:    std_logic;
    signal if15_sgen_rx_next:   std_logic;
    
    signal if15_snp_tx_data:    std_logic_vector(if15_rdout_size+if15_tag_size-1 downto 0);
    signal if15_snp_tx_vld:     std_logic;
    signal if15_snp_tx_next:    std_logic;
    
    -- INTF 0x16
    signal if16_sgen_tx_data:   std_logic_vector(if16_rdreq_size-1 downto 0);
    signal if16_sgen_tx_vld:    std_logic;
    signal if16_sgen_tx_next:   std_logic;
    
    signal if16_sgen_rx_data:   std_logic_vector(if16_rdreq_size-1 downto 0);
    signal if16_sgen_rx_vld:    std_logic;
    signal if16_sgen_rx_next:   std_logic;
    
    signal if16_snp_tx_data:    std_logic_vector(if16_rdout_size+if16_tag_size-1 downto 0);
    signal if16_snp_tx_vld:     std_logic;
    signal if16_snp_tx_next:    std_logic;
    
    -- INTF 0x17
    signal if17_sgen_tx_data:   std_logic_vector(if17_rdreq_size-1 downto 0);
    signal if17_sgen_tx_vld:    std_logic;
    signal if17_sgen_tx_next:   std_logic;
    
    signal if17_sgen_rx_data:   std_logic_vector(if17_rdreq_size-1 downto 0);
    signal if17_sgen_rx_vld:    std_logic;
    signal if17_sgen_rx_next:   std_logic;
    
    signal if17_snp_tx_data_uc: std_logic_vector(if17_rdout_size+if17_tag_size+1-1 downto 0);
    signal if17_snp_tx_data:    std_logic_vector(if17_rdout_size+if17_tag_size-1 downto 0);
    signal if17_snp_tx_vld:     std_logic;
    signal if17_snp_tx_next:    std_logic;
    
    -- INTF 0x18
    signal if18_snp_tx_data:    std_logic_vector(if18_rdout_size+if18_tag_size-1 downto 0);
    signal if18_snp_tx_vld:     std_logic;
    signal if18_snp_tx_next:    std_logic;
    
    -- INTF 0x19
    signal if19_sgen_tx_data:   std_logic_vector(if19_rdreq_size-1 downto 0);
    signal if19_sgen_tx_vld:    std_logic;
    signal if19_sgen_tx_next:   std_logic;
    
    signal if19_sgen_rx_data:   std_logic_vector(if19_rdreq_size-1 downto 0);
    signal if19_sgen_rx_vld:    std_logic;
    signal if19_sgen_rx_next:   std_logic;
    
    signal if19_snp_tx_data_uc: std_logic_vector(if19_rdout_size+if19_tag_size+1-1 downto 0);
    signal if19_snp_tx_data:    std_logic_vector(if19_rdout_size+if19_tag_size-1 downto 0);
    signal if19_snp_tx_vld:     std_logic;
    signal if19_snp_tx_next:    std_logic;
    
    -- INTF 0x1A
    signal if1A_snp_tx_data:    std_logic_vector(if1A_rdout_size+if1A_tag_size-1 downto 0);
    signal if1A_snp_tx_vld:     std_logic;
    signal if1A_snp_tx_next:    std_logic;
    
    -- INTF 0x1B
    signal if1B_snp_tx_data:    std_logic_vector(if1B_rdout_size+if1B_tag_size-1 downto 0);
    signal if1B_snp_tx_vld:     std_logic;
    signal if1B_snp_tx_next:    std_logic;
    
    -- INTF 0x1C
    signal if1C_snp_tx_data:    std_logic_vector(if1C_rdout_size+if1C_tag_size-1 downto 0);
    signal if1C_snp_tx_vld:     std_logic;
    signal if1C_snp_tx_next:    std_logic;
    
    -- INTF 0x1D
    signal if1D_snp_tx_data:    std_logic_vector(if1D_rdout_size+if1D_tag_size-1 downto 0);
    signal if1D_snp_tx_vld:     std_logic;
    signal if1D_snp_tx_next:    std_logic;
    
    -- INTF 0x1E
    signal if1E_snp_tx_data:    std_logic_vector(if1E_rdout_size+if1E_tag_size-1 downto 0);
    signal if1E_snp_tx_vld:     std_logic;
    signal if1E_snp_tx_next:    std_logic;
    
    -- INTF 0x1F
    signal if1F_snp_tx_data:    std_logic_vector(if1F_rdout_size+if1F_tag_size-1 downto 0);
    signal if1F_snp_tx_vld:     std_logic;
    signal if1F_snp_tx_next:    std_logic;
    
    -- INTF 0x20
    signal if20_snp_tx_data:    std_logic_vector(if20_rdout_size+if20_tag_size-1 downto 0);
    signal if20_snp_tx_vld:     std_logic;
    signal if20_snp_tx_next:    std_logic;
    
    -- INTF 0x21
    signal if21_snp_tx_data:    std_logic_vector(if21_rdout_size+if21_tag_size-1 downto 0);
    signal if21_snp_tx_vld:     std_logic;
    signal if21_snp_tx_next:    std_logic;
    
    -- INTF 0x22
    signal if22_snp_tx_data:    std_logic_vector(if22_rdout_size+if22_tag_size-1 downto 0);
    signal if22_snp_tx_vld:     std_logic;
    signal if22_snp_tx_next:    std_logic;
    
    
    
    
    
    
    -- all INTF gloPM
    signal if_gloPM_tx_data:    slv_array (intf_n-1 downto 0) (pkg_data_size-1 downto 0);
    signal if_gloPM_tx_vld:     std_logic_vector(intf_n-1 downto 0);
    signal if_gloPM_tx_next:    std_logic_vector(intf_n-1 downto 0);
    signal if_gloPM_rx_data:    slv_array (intf_n-1 downto 0) (pkg_data_size-1 downto 0);
    signal if_gloPM_rx_vld:     std_logic_vector(intf_n-1 downto 0);
    signal if_gloPM_rx_next:    std_logic_vector(intf_n-1 downto 0);
    
    
BEGIN


    --===========--
    -- INTF 0x00 --
    --===========--
    
    -- Interface --
    if00_cmi_std: entity work.sample_n_hold
    GENERIC MAP
    (
        data_size => if00_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => POWERGOOD_1_2 & POWERGOOD_CB1 & POWERGOOD_CB2 & CB1_ACT & CB2_ACT & DCDC_EN,
        cmi_data    => if00_snp_tx_data,
        cmi_vld     => if00_snp_tx_vld,
        cmi_next    => if00_snp_tx_next,
        trigger     => upd
    );

    
    -- Store and Package --
    if00_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 0,
        rdout_tag_size      => if00_tag_size,
        rdout_data_size     => if00_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if00_n_tags,
        tag_array           => if00_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if00_snp_tx_data,
        rdout_vld   => if00_snp_tx_vld,
        rdout_next  => if00_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(0),
        pkg_vld     => if_gloPM_tx_vld(0),
        pkg_next    => if_gloPM_tx_next(0),
        upd         => upd
    );
    
    -- CMI GloPM --
    if00_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(0),
        tx_vld              => if_gloPM_tx_vld(0),
        tx_next             => if_gloPM_tx_next(0),
        rx_data             => if_gloPM_rx_data(0),
        scalarize(rx_vld)   => if_gloPM_rx_vld(0),
        rx_next             => vectorize(if_gloPM_rx_next(0))
    );
    
    
    --===========--
    -- INTF 0x01 --
    --===========--
    
    -- Interface --
    if01_cmi_std: entity work.sample_n_hold
    GENERIC MAP
    (
        data_size => if01_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => bledp_fw,
        cmi_data    => if01_snp_tx_data,
        cmi_vld     => if01_snp_tx_vld,
        cmi_next    => if01_snp_tx_next,
        trigger     => upd
    );

    -- Store and Package --
    if01_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 1,
        rdout_tag_size      => if01_tag_size,
        rdout_data_size     => if01_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if01_n_tags,
        tag_array           => if01_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if01_snp_tx_data,
        rdout_vld   => if01_snp_tx_vld,
        rdout_next  => if01_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(1),
        pkg_vld     => if_gloPM_tx_vld(1),
        pkg_next    => if_gloPM_tx_next(1),
        upd         => upd
    );
    
    -- CMI GloPM --
    if01_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(1),
        tx_vld              => if_gloPM_tx_vld(1),
        tx_next             => if_gloPM_tx_next(1),
        rx_data             => if_gloPM_rx_data(1),
        scalarize(rx_vld)   => if_gloPM_rx_vld(1),
        rx_next             => vectorize(if_gloPM_rx_next(1))
    );
    
    
    --===========--
    -- INTF 0x02 --
    --===========--
    
    -- Interface --
    if02_chipID: entity work.DS2411
    GENERIC MAP
    (
        Tclk_ns => Tclk_ns
    )
    PORT MAP
    (
        clk         => clk,
        DATA        => ID,
        rd_req_data => '1',
        rd_req_vld  => upd,         -- req once per update
        rd_req_next => open,
        rdout_data  => if02_snp_tx_data,
        rdout_vld   => if02_snp_tx_vld,
        rdout_next  => if02_snp_tx_next
    );
    
    -- Store and Package --
    if02_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 2,
        rdout_tag_size      => if02_tag_size,
        rdout_data_size     => if02_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if02_n_tags,
        tag_array           => if02_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if02_snp_tx_data,
        rdout_vld   => if02_snp_tx_vld,
        rdout_next  => if02_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(2),
        pkg_vld     => if_gloPM_tx_vld(2),
        pkg_next    => if_gloPM_tx_next(2),
        upd         => upd
    );
    
    -- CMI GloPM --
    if02_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(2),
        tx_vld              => if_gloPM_tx_vld(2),
        tx_next             => if_gloPM_tx_next(2),
        rx_data             => if_gloPM_rx_data(2),
        scalarize(rx_vld)   => if_gloPM_rx_vld(2),
        rx_next             => vectorize(if_gloPM_rx_next(2))
    );
    
    
    --===========--
    -- INTF 0x03 --
    --===========--
    
    -- Interface --
    if03_cmi_std: entity work.sample_n_hold
    GENERIC MAP
    (
        data_size => if03_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => global_addr,
        cmi_data    => if03_snp_tx_data,
        cmi_vld     => if03_snp_tx_vld,
        cmi_next    => if03_snp_tx_next,
        trigger     => upd
    );


    -- Store and Package --
    if03_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 3,
        rdout_tag_size      => if03_tag_size,
        rdout_data_size     => if03_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if03_n_tags,
        tag_array           => if03_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if03_snp_tx_data,
        rdout_vld   => if03_snp_tx_vld,
        rdout_next  => if03_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(3),
        pkg_vld     => if_gloPM_tx_vld(3),
        pkg_next    => if_gloPM_tx_next(3),
        upd         => upd
    );
    
    -- CMI GloPM --
    if03_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(3),
        tx_vld              => if_gloPM_tx_vld(3),
        tx_next             => if_gloPM_tx_next(3),
        rx_data             => if_gloPM_rx_data(3),
        scalarize(rx_vld)   => if_gloPM_rx_vld(3),
        rx_next             => vectorize(if_gloPM_rx_next(3))
    );
    
    
    --===========--
    -- INTF 0x04 --
    --===========--
    
    -- Sequence Gen 
    if04_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if04_rdreq_size,
        init_file       => if04_init_file,
        numb_words      => if04_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if04_sgen_tx_data,
        gen_vld     => if04_sgen_tx_vld,
        gen_next    => if04_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if04_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if04_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if04_sgen_tx_data,
        tx_vld                  => if04_sgen_tx_vld,
        tx_next                 => if04_sgen_tx_next,
        rx_data                 => if04_sgen_rx_data,
        scalarize(rx_vld)       => if04_sgen_rx_vld,
        rx_next                 => vectorize(if04_sgen_rx_next)
    );
    
    -- Interface --
    if04_SHT: entity work.SHT15
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 4000
    )
    PORT MAP
    (
        clk         => clk,
        DATA        => SHT15_DATA,
        SCK         => SHT15_SCK,
        rd_req_data => if04_sgen_rx_data,
        rd_req_vld  => if04_sgen_rx_vld,
        rd_req_next => if04_sgen_rx_next,
        rdout_data  => if04_snp_tx_data,
        rdout_vld   => if04_snp_tx_vld,
        rdout_next  => if04_snp_tx_next
    );

    -- Store and Package --
    if04_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 4,
        rdout_tag_size      => if04_tag_size,
        rdout_data_size     => if04_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if04_n_tags,
        tag_array           => if04_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if04_snp_tx_data,
        rdout_vld   => if04_snp_tx_vld,
        rdout_next  => if04_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(4),
        pkg_vld     => if_gloPM_tx_vld(4),
        pkg_next    => if_gloPM_tx_next(4),
        upd         => upd
    );
    
    -- CMI GloPM --
    if04_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(4),
        tx_vld              => if_gloPM_tx_vld(4),
        tx_next             => if_gloPM_tx_next(4),
        rx_data             => if_gloPM_rx_data(4),
        scalarize(rx_vld)   => if_gloPM_rx_vld(4),
        rx_next             => vectorize(if_gloPM_rx_next(4))
    );
    
    
    --===========--
    -- INTF 0x05 --
    --===========--
    
    -- Interface --
    if05_cmi_std: entity work.sample_n_hold
    GENERIC MAP
    (
        data_size => if05_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => RELAYS_STATUS & RELAY_ACT,
        cmi_data    => if05_snp_tx_data,
        cmi_vld     => if05_snp_tx_vld,
        cmi_next    => if05_snp_tx_next,
        trigger     => upd
    );


    -- Store and Package --
    if05_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 5,
        rdout_tag_size      => if05_tag_size,
        rdout_data_size     => if05_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if05_n_tags,
        tag_array           => if05_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if05_snp_tx_data,
        rdout_vld   => if05_snp_tx_vld,
        rdout_next  => if05_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(5),
        pkg_vld     => if_gloPM_tx_vld(5),
        pkg_next    => if_gloPM_tx_next(5),
        upd         => upd
    );
    
    -- CMI GloPM --
    if05_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(5),
        tx_vld              => if_gloPM_tx_vld(5),
        tx_next             => if_gloPM_tx_next(5),
        rx_data             => if_gloPM_rx_data(5),
        scalarize(rx_vld)   => if_gloPM_rx_vld(5),
        rx_next             => vectorize(if_gloPM_rx_next(5))
    );
    
    
    --===========--
    -- INTF 0x06 --
    --===========--
    
    -- Interface --
    if06_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if06_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => STOPOUT(1),
        cmi_data    => if06_snp_tx_data,
        cmi_vld     => if06_snp_tx_vld,
        cmi_next    => if06_snp_tx_next,
        sample      => STOPOUT_smpl(1),
        upd_output  => upd
    );


    -- Store and Package --
    if06_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 6,
        rdout_tag_size      => if06_tag_size,
        rdout_data_size     => if06_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if06_n_tags,
        tag_array           => if06_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if06_snp_tx_data,
        rdout_vld   => if06_snp_tx_vld,
        rdout_next  => if06_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(6),
        pkg_vld     => if_gloPM_tx_vld(6),
        pkg_next    => if_gloPM_tx_next(6),
        upd         => upd
    );
    
    -- CMI GloPM --
    if06_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(6),
        tx_vld              => if_gloPM_tx_vld(6),
        tx_next             => if_gloPM_tx_next(6),
        rx_data             => if_gloPM_rx_data(6),
        scalarize(rx_vld)   => if_gloPM_rx_vld(6),
        rx_next             => vectorize(if_gloPM_rx_next(6))
    );
    
    --===========--
    -- INTF 0x07 --
    --===========--
    
    -- Interface --
    if07_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if07_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => STOPOUT(2),
        cmi_data    => if07_snp_tx_data,
        cmi_vld     => if07_snp_tx_vld,
        cmi_next    => if07_snp_tx_next,
        sample      => STOPOUT_smpl(2),
        upd_output  => upd
    );


    -- Store and Package --
    if07_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 7,
        rdout_tag_size      => if07_tag_size,
        rdout_data_size     => if07_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if07_n_tags,
        tag_array           => if07_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if07_snp_tx_data,
        rdout_vld   => if07_snp_tx_vld,
        rdout_next  => if07_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(7),
        pkg_vld     => if_gloPM_tx_vld(7),
        pkg_next    => if_gloPM_tx_next(7),
        upd         => upd
    );
    
    -- CMI GloPM --
    if07_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(7),
        tx_vld              => if_gloPM_tx_vld(7),
        tx_next             => if_gloPM_tx_next(7),
        rx_data             => if_gloPM_rx_data(7),
        scalarize(rx_vld)   => if_gloPM_rx_vld(7),
        rx_next             => vectorize(if_gloPM_rx_next(7))
    );
    
    --===========--
    -- INTF 0x08 --
    --===========--
    
    -- Interface --
    if08_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if08_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => STOPOUT(3),
        cmi_data    => if08_snp_tx_data,
        cmi_vld     => if08_snp_tx_vld,
        cmi_next    => if08_snp_tx_next,
        sample      => STOPOUT_smpl(3),
        upd_output  => upd
    );


    -- Store and Package --
    if08_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 8,
        rdout_tag_size      => if08_tag_size,
        rdout_data_size     => if08_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if08_n_tags,
        tag_array           => if08_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if08_snp_tx_data,
        rdout_vld   => if08_snp_tx_vld,
        rdout_next  => if08_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(8),
        pkg_vld     => if_gloPM_tx_vld(8),
        pkg_next    => if_gloPM_tx_next(8),
        upd         => upd
    );
    
    -- CMI GloPM --
    if08_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(8),
        tx_vld              => if_gloPM_tx_vld(8),
        tx_next             => if_gloPM_tx_next(8),
        rx_data             => if_gloPM_rx_data(8),
        scalarize(rx_vld)   => if_gloPM_rx_vld(8),
        rx_next             => vectorize(if_gloPM_rx_next(8))
    );
    
    
    --===========--
    -- INTF 0x09 --
    --===========--
    
    -- Interface --
    if09_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if09_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => STOPOUT(4),
        cmi_data    => if09_snp_tx_data,
        cmi_vld     => if09_snp_tx_vld,
        cmi_next    => if09_snp_tx_next,
        sample      => STOPOUT_smpl(4),
        upd_output  => upd
    );


    -- Store and Package --
    if09_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 9,
        rdout_tag_size      => if09_tag_size,
        rdout_data_size     => if09_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if09_n_tags,
        tag_array           => if09_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if09_snp_tx_data,
        rdout_vld   => if09_snp_tx_vld,
        rdout_next  => if09_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(9),
        pkg_vld     => if_gloPM_tx_vld(9),
        pkg_next    => if_gloPM_tx_next(9),
        upd         => upd
    );
    
    -- CMI GloPM --
    if09_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(9),
        tx_vld              => if_gloPM_tx_vld(9),
        tx_next             => if_gloPM_tx_next(9),
        rx_data             => if_gloPM_rx_data(9),
        scalarize(rx_vld)   => if_gloPM_rx_vld(9),
        rx_next             => vectorize(if_gloPM_rx_next(9))
    );
    
    
    --===========--
    -- INTF 0x0A --
    --===========--
    
    -- Interface --
    if0A_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if0A_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => STOPOUT(5),
        cmi_data    => if0A_snp_tx_data,
        cmi_vld     => if0A_snp_tx_vld,
        cmi_next    => if0A_snp_tx_next,
        sample      => STOPOUT_smpl(5),
        upd_output  => upd
    );


    -- Store and Package --
    if0A_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 10,
        rdout_tag_size      => if0A_tag_size,
        rdout_data_size     => if0A_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if0A_n_tags,
        tag_array           => if0A_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if0A_snp_tx_data,
        rdout_vld   => if0A_snp_tx_vld,
        rdout_next  => if0A_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(10),
        pkg_vld     => if_gloPM_tx_vld(10),
        pkg_next    => if_gloPM_tx_next(10),
        upd         => upd
    );
    
    -- CMI GloPM --
    if0A_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(10),
        tx_vld              => if_gloPM_tx_vld(10),
        tx_next             => if_gloPM_tx_next(10),
        rx_data             => if_gloPM_rx_data(10),
        scalarize(rx_vld)   => if_gloPM_rx_vld(10),
        rx_next             => vectorize(if_gloPM_rx_next(10))
    );
    
    
    --===========--
    -- INTF 0x0B --
    --===========--
    
    -- Interface --
    if0B_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if0B_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => STOPOUT(6),
        cmi_data    => if0B_snp_tx_data,
        cmi_vld     => if0B_snp_tx_vld,
        cmi_next    => if0B_snp_tx_next,
        sample      => STOPOUT_smpl(6),
        upd_output  => upd
    );


    -- Store and Package --
    if0B_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 11,
        rdout_tag_size      => if0B_tag_size,
        rdout_data_size     => if0B_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if0B_n_tags,
        tag_array           => if0B_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if0B_snp_tx_data,
        rdout_vld   => if0B_snp_tx_vld,
        rdout_next  => if0B_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(11),
        pkg_vld     => if_gloPM_tx_vld(11),
        pkg_next    => if_gloPM_tx_next(11),
        upd         => upd
    );
    
    -- CMI GloPM --
    if0B_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(11),
        tx_vld              => if_gloPM_tx_vld(11),
        tx_next             => if_gloPM_tx_next(11),
        rx_data             => if_gloPM_rx_data(11),
        scalarize(rx_vld)   => if_gloPM_rx_vld(11),
        rx_next             => vectorize(if_gloPM_rx_next(11))
    );
    
    --===========--
    -- INTF 0x0C --
    --===========--
    
    -- Interface --
    if0C_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if0C_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => STOPOUT(7),
        cmi_data    => if0C_snp_tx_data,
        cmi_vld     => if0C_snp_tx_vld,
        cmi_next    => if0C_snp_tx_next,
        sample      => STOPOUT_smpl(7),
        upd_output  => upd
    );


    -- Store and Package --
    if0C_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 12,
        rdout_tag_size      => if0C_tag_size,
        rdout_data_size     => if0C_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if0C_n_tags,
        tag_array           => if0C_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if0C_snp_tx_data,
        rdout_vld   => if0C_snp_tx_vld,
        rdout_next  => if0C_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(12),
        pkg_vld     => if_gloPM_tx_vld(12),
        pkg_next    => if_gloPM_tx_next(12),
        upd         => upd
    );
    
    -- CMI GloPM --
    if0C_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(12),
        tx_vld              => if_gloPM_tx_vld(12),
        tx_next             => if_gloPM_tx_next(12),
        rx_data             => if_gloPM_rx_data(12),
        scalarize(rx_vld)   => if_gloPM_rx_vld(12),
        rx_next             => vectorize(if_gloPM_rx_next(12))
    );
    
    --===========--
    -- INTF 0x0D --
    --===========--
    
    -- Interface --
    if0D_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if0D_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => STOPOUT(8),
        cmi_data    => if0D_snp_tx_data,
        cmi_vld     => if0D_snp_tx_vld,
        cmi_next    => if0D_snp_tx_next,
        sample      => STOPOUT_smpl(8),
        upd_output  => upd
    );


    -- Store and Package --
    if0D_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 13,
        rdout_tag_size      => if0D_tag_size,
        rdout_data_size     => if0D_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if0D_n_tags,
        tag_array           => if0D_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if0D_snp_tx_data,
        rdout_vld   => if0D_snp_tx_vld,
        rdout_next  => if0D_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(13),
        pkg_vld     => if_gloPM_tx_vld(13),
        pkg_next    => if_gloPM_tx_next(13),
        upd         => upd
    );
    
    -- CMI GloPM --
    if0D_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(13),
        tx_vld              => if_gloPM_tx_vld(13),
        tx_next             => if_gloPM_tx_next(13),
        rx_data             => if_gloPM_rx_data(13),
        scalarize(rx_vld)   => if_gloPM_rx_vld(13),
        rx_next             => vectorize(if_gloPM_rx_next(13))
    );
    
    
    --===========--
    -- INTF 0x0E --
    --===========--
    
    -- Sequence Gen 
    if0E_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if0E_rdreq_size,
        init_file       => if0E_init_file,
        numb_words      => if0E_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if0E_sgen_tx_data,
        gen_vld     => if0E_sgen_tx_vld,
        gen_next    => if0E_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if0E_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if0E_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if0E_sgen_tx_data,
        tx_vld                  => if0E_sgen_tx_vld,
        tx_next                 => if0E_sgen_tx_next,
        rx_data                 => if0E_sgen_rx_data,
        scalarize(rx_vld)       => if0E_sgen_rx_vld,
        rx_next                 => vectorize(if0E_sgen_rx_next)
    );
    
    -- Interface --
    if0E_sadc: entity work.AD7927
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 2000
    )
    PORT MAP
    (
        clk         => clk,
        CSn         => AD_I_CS_N,
        SCLK        => AD_I_CLK,
        DOUT        => AD_I_DOUT,
        DIN         => AD_I_DIN,
        rd_req_data => if0E_sgen_rx_data,
        rd_req_vld  => if0E_sgen_rx_vld,
        rd_req_next => if0E_sgen_rx_next,
        rdout_data  => if0E_snp_tx_data,
        rdout_vld   => if0E_snp_tx_vld,
        rdout_next  => if0E_snp_tx_next
    );

    -- Store and Package --
    if0E_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 14,
        rdout_tag_size      => if0E_tag_size,
        rdout_data_size     => if0E_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if0E_n_tags,
        tag_array           => if0E_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if0E_snp_tx_data,
        rdout_vld   => if0E_snp_tx_vld,
        rdout_next  => if0E_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(14),
        pkg_vld     => if_gloPM_tx_vld(14),
        pkg_next    => if_gloPM_tx_next(14),
        upd         => upd
    );
    
    -- CMI GloPM --
    if0E_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(14),
        tx_vld              => if_gloPM_tx_vld(14),
        tx_next             => if_gloPM_tx_next(14),
        rx_data             => if_gloPM_rx_data(14),
        scalarize(rx_vld)   => if_gloPM_rx_vld(14),
        rx_next             => vectorize(if_gloPM_rx_next(14))
    );
    
    
    --===========--
    -- INTF 0x0F --
    --===========--
    
    -- Sequence Gen 
    if0F_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if0F_rdreq_size,
        init_file       => if0F_init_file,
        numb_words      => if0F_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if0F_sgen_tx_data,
        gen_vld     => if0F_sgen_tx_vld,
        gen_next    => if0F_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if0F_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if0F_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if0F_sgen_tx_data,
        tx_vld                  => if0F_sgen_tx_vld,
        tx_next                 => if0F_sgen_tx_next,
        rx_data                 => if0F_sgen_rx_data,
        scalarize(rx_vld)       => if0F_sgen_rx_vld,
        rx_next                 => vectorize(if0F_sgen_rx_next)
    );
    
    -- Interface --
    if0F_sadc: entity work.AD5252
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 4000
    )
    PORT MAP
    (
        clk         => clk,
        SDA         => SDA(1),
        SCL         => SCL(1),
        WPn         => WP(1),
        rd_req_data => if0F_sgen_rx_data,
        rd_req_vld  => if0F_sgen_rx_vld,
        rd_req_next => if0F_sgen_rx_next,
        rdout_data  => if0F_snp_tx_data,
        rdout_vld   => if0F_snp_tx_vld,
        rdout_next  => if0F_snp_tx_next,
        wakeup      => CB1_wakeup,
        wr_req_data => DP_wr_req_data(1),
        wr_req_vld  => DP_wr_req_vld(1),
        wr_req_next => DP_wr_req_next(1)
    );

    -- Store and Package --
    if0F_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 15,
        rdout_tag_size      => if0F_tag_size,
        rdout_data_size     => if0F_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if0F_n_tags,
        tag_array           => if0F_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if0F_snp_tx_data,
        rdout_vld   => if0F_snp_tx_vld,
        rdout_next  => if0F_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(15),
        pkg_vld     => if_gloPM_tx_vld(15),
        pkg_next    => if_gloPM_tx_next(15),
        upd         => upd
    );
    
    -- CMI GloPM --
    if0F_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(15),
        tx_vld              => if_gloPM_tx_vld(15),
        tx_next             => if_gloPM_tx_next(15),
        rx_data             => if_gloPM_rx_data(15),
        scalarize(rx_vld)   => if_gloPM_rx_vld(15),
        rx_next             => vectorize(if_gloPM_rx_next(15))
    );
    
    --===========--
    -- INTF 0x10 --
    --===========--
    
    -- Sequence Gen 
    if10_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if10_rdreq_size,
        init_file       => if10_init_file,
        numb_words      => if10_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if10_sgen_tx_data,
        gen_vld     => if10_sgen_tx_vld,
        gen_next    => if10_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if10_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if10_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if10_sgen_tx_data,
        tx_vld                  => if10_sgen_tx_vld,
        tx_next                 => if10_sgen_tx_next,
        rx_data                 => if10_sgen_rx_data,
        scalarize(rx_vld)       => if10_sgen_rx_vld,
        rx_next                 => vectorize(if10_sgen_rx_next)
    );
    
    -- Interface --
    if10_sadc: entity work.AD5252
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 4000
    )
    PORT MAP
    (
        clk         => clk,
        SDA         => SDA(2),
        SCL         => SCL(2),
        WPn         => WP(2),
        rd_req_data => if10_sgen_rx_data,
        rd_req_vld  => if10_sgen_rx_vld,
        rd_req_next => if10_sgen_rx_next,
        rdout_data  => if10_snp_tx_data,
        rdout_vld   => if10_snp_tx_vld,
        rdout_next  => if10_snp_tx_next,
        wakeup      => CB1_wakeup,
        wr_req_data => DP_wr_req_data(2),
        wr_req_vld  => DP_wr_req_vld(2),
        wr_req_next => DP_wr_req_next(2)
    );

    -- Store and Package --
    if10_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 16,
        rdout_tag_size      => if10_tag_size,
        rdout_data_size     => if10_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if10_n_tags,
        tag_array           => if10_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if10_snp_tx_data,
        rdout_vld   => if10_snp_tx_vld,
        rdout_next  => if10_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(16),
        pkg_vld     => if_gloPM_tx_vld(16),
        pkg_next    => if_gloPM_tx_next(16),
        upd         => upd
    );
    
    -- CMI GloPM --
    if10_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(16),
        tx_vld              => if_gloPM_tx_vld(16),
        tx_next             => if_gloPM_tx_next(16),
        rx_data             => if_gloPM_rx_data(16),
        scalarize(rx_vld)   => if_gloPM_rx_vld(16),
        rx_next             => vectorize(if_gloPM_rx_next(16))
    );
    
    --===========--
    -- INTF 0x11 --
    --===========--
    
    -- Sequence Gen 
    if11_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if11_rdreq_size,
        init_file       => if11_init_file,
        numb_words      => if11_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if11_sgen_tx_data,
        gen_vld     => if11_sgen_tx_vld,
        gen_next    => if11_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if11_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if11_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if11_sgen_tx_data,
        tx_vld                  => if11_sgen_tx_vld,
        tx_next                 => if11_sgen_tx_next,
        rx_data                 => if11_sgen_rx_data,
        scalarize(rx_vld)       => if11_sgen_rx_vld,
        rx_next                 => vectorize(if11_sgen_rx_next)
    );
    
    -- Interface --
    if11_sadc: entity work.AD5252
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 4000
    )
    PORT MAP
    (
        clk         => clk,
        SDA         => SDA(3),
        SCL         => SCL(3),
        WPn         => WP(3),
        rd_req_data => if11_sgen_rx_data,
        rd_req_vld  => if11_sgen_rx_vld,
        rd_req_next => if11_sgen_rx_next,
        rdout_data  => if11_snp_tx_data,
        rdout_vld   => if11_snp_tx_vld,
        rdout_next  => if11_snp_tx_next,
        wakeup      => CB1_wakeup,
        wr_req_data => DP_wr_req_data(3),
        wr_req_vld  => DP_wr_req_vld(3),
        wr_req_next => DP_wr_req_next(3)
    );

    -- Store and Package --
    if11_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 17,
        rdout_tag_size      => if11_tag_size,
        rdout_data_size     => if11_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if11_n_tags,
        tag_array           => if11_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if11_snp_tx_data,
        rdout_vld   => if11_snp_tx_vld,
        rdout_next  => if11_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(17),
        pkg_vld     => if_gloPM_tx_vld(17),
        pkg_next    => if_gloPM_tx_next(17),
        upd         => upd
    );
    
    -- CMI GloPM --
    if11_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(17),
        tx_vld              => if_gloPM_tx_vld(17),
        tx_next             => if_gloPM_tx_next(17),
        rx_data             => if_gloPM_rx_data(17),
        scalarize(rx_vld)   => if_gloPM_rx_vld(17),
        rx_next             => vectorize(if_gloPM_rx_next(17))
    );
    
    --===========--
    -- INTF 0x12 --
    --===========--
    
    -- Sequence Gen 
    if12_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if12_rdreq_size,
        init_file       => if12_init_file,
        numb_words      => if12_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if12_sgen_tx_data,
        gen_vld     => if12_sgen_tx_vld,
        gen_next    => if12_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if12_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if12_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if12_sgen_tx_data,
        tx_vld                  => if12_sgen_tx_vld,
        tx_next                 => if12_sgen_tx_next,
        rx_data                 => if12_sgen_rx_data,
        scalarize(rx_vld)       => if12_sgen_rx_vld,
        rx_next                 => vectorize(if12_sgen_rx_next)
    );
    
    -- Interface --
    if12_sadc: entity work.AD5252
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 4000
    )
    PORT MAP
    (
        clk         => clk,
        SDA         => SDA(4),
        SCL         => SCL(4),
        WPn         => WP(4),
        rd_req_data => if12_sgen_rx_data,
        rd_req_vld  => if12_sgen_rx_vld,
        rd_req_next => if12_sgen_rx_next,
        rdout_data  => if12_snp_tx_data,
        rdout_vld   => if12_snp_tx_vld,
        rdout_next  => if12_snp_tx_next,
        wakeup      => CB1_wakeup,
        wr_req_data => DP_wr_req_data(4),
        wr_req_vld  => DP_wr_req_vld(4),
        wr_req_next => DP_wr_req_next(4)
    );

    -- Store and Package --
    if12_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 18,
        rdout_tag_size      => if12_tag_size,
        rdout_data_size     => if12_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if12_n_tags,
        tag_array           => if12_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if12_snp_tx_data,
        rdout_vld   => if12_snp_tx_vld,
        rdout_next  => if12_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(18),
        pkg_vld     => if_gloPM_tx_vld(18),
        pkg_next    => if_gloPM_tx_next(18),
        upd         => upd
    );
    
    -- CMI GloPM --
    if12_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(18),
        tx_vld              => if_gloPM_tx_vld(18),
        tx_next             => if_gloPM_tx_next(18),
        rx_data             => if_gloPM_rx_data(18),
        scalarize(rx_vld)   => if_gloPM_rx_vld(18),
        rx_next             => vectorize(if_gloPM_rx_next(18))
    );
    
    --===========--
    -- INTF 0x13 --
    --===========--
    
    -- Sequence Gen 
    if13_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if13_rdreq_size,
        init_file       => if13_init_file,
        numb_words      => if13_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if13_sgen_tx_data,
        gen_vld     => if13_sgen_tx_vld,
        gen_next    => if13_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if13_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if13_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if13_sgen_tx_data,
        tx_vld                  => if13_sgen_tx_vld,
        tx_next                 => if13_sgen_tx_next,
        rx_data                 => if13_sgen_rx_data,
        scalarize(rx_vld)       => if13_sgen_rx_vld,
        rx_next                 => vectorize(if13_sgen_rx_next)
    );
    
    -- Interface --
    if13_sadc: entity work.AD5252
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 4000
    )
    PORT MAP
    (
        clk         => clk,
        SDA         => SDA(5),
        SCL         => SCL(5),
        WPn         => WP(5),
        rd_req_data => if13_sgen_rx_data,
        rd_req_vld  => if13_sgen_rx_vld,
        rd_req_next => if13_sgen_rx_next,
        rdout_data  => if13_snp_tx_data,
        rdout_vld   => if13_snp_tx_vld,
        rdout_next  => if13_snp_tx_next,
        wakeup      => CB2_wakeup,
        wr_req_data => DP_wr_req_data(5),
        wr_req_vld  => DP_wr_req_vld(5),
        wr_req_next => DP_wr_req_next(5)
    );

    -- Store and Package --
    if13_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 19,
        rdout_tag_size      => if13_tag_size,
        rdout_data_size     => if13_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if13_n_tags,
        tag_array           => if13_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if13_snp_tx_data,
        rdout_vld   => if13_snp_tx_vld,
        rdout_next  => if13_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(19),
        pkg_vld     => if_gloPM_tx_vld(19),
        pkg_next    => if_gloPM_tx_next(19),
        upd         => upd
    );
    
    -- CMI GloPM --
    if13_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(19),
        tx_vld              => if_gloPM_tx_vld(19),
        tx_next             => if_gloPM_tx_next(19),
        rx_data             => if_gloPM_rx_data(19),
        scalarize(rx_vld)   => if_gloPM_rx_vld(19),
        rx_next             => vectorize(if_gloPM_rx_next(19))
    );
    
    
    --===========--
    -- INTF 0x14 --
    --===========--
    
    -- Sequence Gen 
    if14_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if14_rdreq_size,
        init_file       => if14_init_file,
        numb_words      => if14_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if14_sgen_tx_data,
        gen_vld     => if14_sgen_tx_vld,
        gen_next    => if14_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if14_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if14_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if14_sgen_tx_data,
        tx_vld                  => if14_sgen_tx_vld,
        tx_next                 => if14_sgen_tx_next,
        rx_data                 => if14_sgen_rx_data,
        scalarize(rx_vld)       => if14_sgen_rx_vld,
        rx_next                 => vectorize(if14_sgen_rx_next)
    );
    
    -- Interface --
    if14_sadc: entity work.AD5252
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 4000
    )
    PORT MAP
    (
        clk         => clk,
        SDA         => SDA(6),
        SCL         => SCL(6),
        WPn         => WP(6),
        rd_req_data => if14_sgen_rx_data,
        rd_req_vld  => if14_sgen_rx_vld,
        rd_req_next => if14_sgen_rx_next,
        rdout_data  => if14_snp_tx_data,
        rdout_vld   => if14_snp_tx_vld,
        rdout_next  => if14_snp_tx_next,
        wakeup      => CB2_wakeup,
        wr_req_data => DP_wr_req_data(6),
        wr_req_vld  => DP_wr_req_vld(6),
        wr_req_next => DP_wr_req_next(6)
    );

    -- Store and Package --
    if14_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 20,
        rdout_tag_size      => if14_tag_size,
        rdout_data_size     => if14_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if14_n_tags,
        tag_array           => if14_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if14_snp_tx_data,
        rdout_vld   => if14_snp_tx_vld,
        rdout_next  => if14_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(20),
        pkg_vld     => if_gloPM_tx_vld(20),
        pkg_next    => if_gloPM_tx_next(20),
        upd         => upd
    );
    
    -- CMI GloPM --
    if14_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(20),
        tx_vld              => if_gloPM_tx_vld(20),
        tx_next             => if_gloPM_tx_next(20),
        rx_data             => if_gloPM_rx_data(20),
        scalarize(rx_vld)   => if_gloPM_rx_vld(20),
        rx_next             => vectorize(if_gloPM_rx_next(20))
    );
    
    --===========--
    -- INTF 0x15 --
    --===========--
    
    -- Sequence Gen 
    if15_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if15_rdreq_size,
        init_file       => if15_init_file,
        numb_words      => if15_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if15_sgen_tx_data,
        gen_vld     => if15_sgen_tx_vld,
        gen_next    => if15_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if15_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if15_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if15_sgen_tx_data,
        tx_vld                  => if15_sgen_tx_vld,
        tx_next                 => if15_sgen_tx_next,
        rx_data                 => if15_sgen_rx_data,
        scalarize(rx_vld)       => if15_sgen_rx_vld,
        rx_next                 => vectorize(if15_sgen_rx_next)
    );
    
    -- Interface --
    if15_sadc: entity work.AD5252
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 4000
    )
    PORT MAP
    (
        clk         => clk,
        SDA         => SDA(7),
        SCL         => SCL(7),
        WPn         => WP(7),
        rd_req_data => if15_sgen_rx_data,
        rd_req_vld  => if15_sgen_rx_vld,
        rd_req_next => if15_sgen_rx_next,
        rdout_data  => if15_snp_tx_data,
        rdout_vld   => if15_snp_tx_vld,
        rdout_next  => if15_snp_tx_next,
        wakeup      => CB2_wakeup,
        wr_req_data => DP_wr_req_data(7),
        wr_req_vld  => DP_wr_req_vld(7),
        wr_req_next => DP_wr_req_next(7)
    );

    -- Store and Package --
    if15_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 21,
        rdout_tag_size      => if15_tag_size,
        rdout_data_size     => if15_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if15_n_tags,
        tag_array           => if15_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if15_snp_tx_data,
        rdout_vld   => if15_snp_tx_vld,
        rdout_next  => if15_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(21),
        pkg_vld     => if_gloPM_tx_vld(21),
        pkg_next    => if_gloPM_tx_next(21),
        upd         => upd
    );
    
    -- CMI GloPM --
    if15_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(21),
        tx_vld              => if_gloPM_tx_vld(21),
        tx_next             => if_gloPM_tx_next(21),
        rx_data             => if_gloPM_rx_data(21),
        scalarize(rx_vld)   => if_gloPM_rx_vld(21),
        rx_next             => vectorize(if_gloPM_rx_next(21))
    );
    
    
    --===========--
    -- INTF 0x16 --
    --===========--
    
    -- Sequence Gen 
    if16_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if16_rdreq_size,
        init_file       => if16_init_file,
        numb_words      => if16_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if16_sgen_tx_data,
        gen_vld     => if16_sgen_tx_vld,
        gen_next    => if16_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if16_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if16_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if16_sgen_tx_data,
        tx_vld                  => if16_sgen_tx_vld,
        tx_next                 => if16_sgen_tx_next,
        rx_data                 => if16_sgen_rx_data,
        scalarize(rx_vld)       => if16_sgen_rx_vld,
        rx_next                 => vectorize(if16_sgen_rx_next)
    );
    
    -- Interface --
    if16_sadc: entity work.AD5252
    GENERIC MAP
    (
        Tclk_ns        => Tclk_ns,
        Tintfclk_ns    => 4000
    )
    PORT MAP
    (
        clk         => clk,
        SDA         => SDA(8),
        SCL         => SCL(8),
        WPn         => WP(8),
        rd_req_data => if16_sgen_rx_data,
        rd_req_vld  => if16_sgen_rx_vld,
        rd_req_next => if16_sgen_rx_next,
        rdout_data  => if16_snp_tx_data,
        rdout_vld   => if16_snp_tx_vld,
        rdout_next  => if16_snp_tx_next,
        wakeup      => CB2_wakeup,
        wr_req_data => DP_wr_req_data(8),
        wr_req_vld  => DP_wr_req_vld(8),
        wr_req_next => DP_wr_req_next(8)
    );

    -- Store and Package --
    if16_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 22,
        rdout_tag_size      => if16_tag_size,
        rdout_data_size     => if16_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if16_n_tags,
        tag_array           => if16_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if16_snp_tx_data,
        rdout_vld   => if16_snp_tx_vld,
        rdout_next  => if16_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(22),
        pkg_vld     => if_gloPM_tx_vld(22),
        pkg_next    => if_gloPM_tx_next(22),
        upd         => upd
    );
    
    -- CMI GloPM --
    if16_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(22),
        tx_vld              => if_gloPM_tx_vld(22),
        tx_next             => if_gloPM_tx_next(22),
        rx_data             => if_gloPM_rx_data(22),
        scalarize(rx_vld)   => if_gloPM_rx_vld(22),
        rx_next             => vectorize(if_gloPM_rx_next(22))
    );
    
    
    --===========--
    -- INTF 0x17 --
    --===========--
    
    -- Sequence Gen 
    if17_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if17_rdreq_size,
        init_file       => if17_init_file,
        numb_words      => if17_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if17_sgen_tx_data,
        gen_vld     => if17_sgen_tx_vld,
        gen_next    => if17_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if17_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if17_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if17_sgen_tx_data,
        tx_vld                  => if17_sgen_tx_vld,
        tx_next                 => if17_sgen_tx_next,
        rx_data                 => if17_sgen_rx_data,
        scalarize(rx_vld)       => if17_sgen_rx_vld,
        rx_next                 => vectorize(if17_sgen_rx_next)
    );
    
    -- Interface --
    if17_sfp1: entity work.SFP_EEMEM
    GENERIC MAP
    (
        Tclk_ns         => Tclk_ns,
        Tintfclk_ns     => 4000
    )
    PORT MAP
    (
        clk         => clk,
        SDA         => SDA_OPT_1,
        SCL         => SCL_OPT_1,
        rd_req_data => if17_sgen_rx_data,
        rd_req_vld  => if17_sgen_rx_vld,
        rd_req_next => if17_sgen_rx_next,
        rdout_data  => if17_snp_tx_data_uc,
        rdout_vld   => if17_snp_tx_vld,
        rdout_next  => if17_snp_tx_next,
        wr_req_data => SFP1_wr_req_data,
        wr_req_vld  => SFP1_wr_req_vld,
        wr_req_next => SFP1_wr_req_next
    );
    
    -- CUT the rdout tag to 8 bit --
    
    if17_snp_tx_data <= if17_snp_tx_data_uc(16) & if17_snp_tx_data_uc(14 downto 0); 
    
    -- Store and Package --
    if17_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 23,
        rdout_tag_size      => if17_tag_size,
        rdout_data_size     => if17_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if17_n_tags,
        tag_array           => if17_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if17_snp_tx_data,
        rdout_vld   => if17_snp_tx_vld,
        rdout_next  => if17_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(23),
        pkg_vld     => if_gloPM_tx_vld(23),
        pkg_next    => if_gloPM_tx_next(23),
        upd         => upd
    );
    
    -- CMI GloPM --
    if17_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(23),
        tx_vld              => if_gloPM_tx_vld(23),
        tx_next             => if_gloPM_tx_next(23),
        rx_data             => if_gloPM_rx_data(23),
        scalarize(rx_vld)   => if_gloPM_rx_vld(23),
        rx_next             => vectorize(if_gloPM_rx_next(23))
    );
    
    --===========--
    -- INTF 0x18 --
    --===========--
    
    -- Interface --
    if18_cmi_std: entity work.sample_n_hold
    GENERIC MAP
    (
        data_size => if18_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => TX_FAULT_1 & MODULE_DETECT_1 & LOSS_OF_SIGNAL_1 & RATE_SELECT_2 & TX_DISABLE_2 & SFP1_Temp_H & SFP1_Temp_L & SFP1_VCC_H & SFP1_VCC_L & SFP1_TXBias_H & SFP1_TXBias_L & SFP1_TXPWR_H & SFP1_TXPWR_L & SFP1_RXPWR_H & SFP1_RXPWR_L,
        cmi_data    => if18_snp_tx_data,
        cmi_vld     => if18_snp_tx_vld,
        cmi_next    => if18_snp_tx_next,
        trigger     => upd
    );

    
    -- Store and Package --
    if18_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 24,
        rdout_tag_size      => if18_tag_size,
        rdout_data_size     => if18_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if18_n_tags,
        tag_array           => if18_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if18_snp_tx_data,
        rdout_vld   => if18_snp_tx_vld,
        rdout_next  => if18_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(24),
        pkg_vld     => if_gloPM_tx_vld(24),
        pkg_next    => if_gloPM_tx_next(24),
        upd         => upd
    );
    
    -- CMI GloPM --
    if18_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(24),
        tx_vld              => if_gloPM_tx_vld(24),
        tx_next             => if_gloPM_tx_next(24),
        rx_data             => if_gloPM_rx_data(24),
        scalarize(rx_vld)   => if_gloPM_rx_vld(24),
        rx_next             => vectorize(if_gloPM_rx_next(24))
    );
    
    
    
    --===========--
    -- INTF 0x19 --
    --===========--
    
    -- Sequence Gen 
    if19_seqgen: entity work.seq_generator
    GENERIC MAP
    (
        data_size       => if19_rdreq_size,
        init_file       => if19_init_file,
        numb_words      => if19_n_tags
    )
    PORT MAP
    (
        clk         => clk,
        gen_data    => if19_sgen_tx_data,
        gen_vld     => if19_sgen_tx_vld,
        gen_next    => if19_sgen_tx_next,
        trigger_seq => upd
    );
    
    -- CMI SGEN --
    if19_sgen_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => if19_rdreq_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => clk,
        tx_data                 => if19_sgen_tx_data,
        tx_vld                  => if19_sgen_tx_vld,
        tx_next                 => if19_sgen_tx_next,
        rx_data                 => if19_sgen_rx_data,
        scalarize(rx_vld)       => if19_sgen_rx_vld,
        rx_next                 => vectorize(if19_sgen_rx_next)
    );
    
    -- Interface --
    if19_sfp2: entity work.SFP_EEMEM
    GENERIC MAP
    (
        Tclk_ns         => Tclk_ns,
        Tintfclk_ns     => 4000
    )
    PORT MAP
    (
        clk         => clk,
        SDA         => SDA_OPT_2,
        SCL         => SCL_OPT_2,
        rd_req_data => if19_sgen_rx_data,
        rd_req_vld  => if19_sgen_rx_vld,
        rd_req_next => if19_sgen_rx_next,
        rdout_data  => if19_snp_tx_data_uc,
        rdout_vld   => if19_snp_tx_vld,
        rdout_next  => if19_snp_tx_next,
        wr_req_data => SFP2_wr_req_data,
        wr_req_vld  => SFP2_wr_req_vld,
        wr_req_next => SFP2_wr_req_next
    );
    
    -- CUT the rdout tag to 8 bit --
    
    if19_snp_tx_data <= if19_snp_tx_data_uc(16) & if19_snp_tx_data_uc(14 downto 0); 
    
    -- Store and Package --
    if19_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 25,
        rdout_tag_size      => if19_tag_size,
        rdout_data_size     => if19_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if19_n_tags,
        tag_array           => if19_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if19_snp_tx_data,
        rdout_vld   => if19_snp_tx_vld,
        rdout_next  => if19_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(25),
        pkg_vld     => if_gloPM_tx_vld(25),
        pkg_next    => if_gloPM_tx_next(25),
        upd         => upd
    );
    
    -- CMI GloPM --
    if19_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(25),
        tx_vld              => if_gloPM_tx_vld(25),
        tx_next             => if_gloPM_tx_next(25),
        rx_data             => if_gloPM_rx_data(25),
        scalarize(rx_vld)   => if_gloPM_rx_vld(25),
        rx_next             => vectorize(if_gloPM_rx_next(25))
    );
    
    
    --===========--
    -- INTF 0x1A --
    --===========--
    
    -- Interface --
    if1A_cmi_std: entity work.sample_n_hold
    GENERIC MAP
    (
        data_size => if1A_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => TX_FAULT_2 & MODULE_DETECT_2 & LOSS_OF_SIGNAL_2 & RATE_SELECT_1 & TX_DISABLE_1 & SFP2_Temp_H & SFP2_Temp_L & SFP2_VCC_H & SFP2_VCC_L & SFP2_TXBias_H & SFP2_TXBias_L & SFP2_TXPWR_H & SFP2_TXPWR_L & SFP2_RXPWR_H & SFP2_RXPWR_L,
        cmi_data    => if1A_snp_tx_data,
        cmi_vld     => if1A_snp_tx_vld,
        cmi_next    => if1A_snp_tx_next,
        trigger     => upd
    );

    
    -- Store and Package --
    if1A_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 26,
        rdout_tag_size      => if1A_tag_size,
        rdout_data_size     => if1A_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if1A_n_tags,
        tag_array           => if1A_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if1A_snp_tx_data,
        rdout_vld   => if1A_snp_tx_vld,
        rdout_next  => if1A_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(26),
        pkg_vld     => if_gloPM_tx_vld(26),
        pkg_next    => if_gloPM_tx_next(26),
        upd         => upd
    );
    
    -- CMI GloPM --
    if1A_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(26),
        tx_vld              => if_gloPM_tx_vld(26),
        tx_next             => if_gloPM_tx_next(26),
        rx_data             => if_gloPM_rx_data(26),
        scalarize(rx_vld)   => if_gloPM_rx_vld(26),
        rx_next             => vectorize(if_gloPM_rx_next(26))
    );
    
    
    
    --===========--
    -- INTF 0x1B --
    --===========--
    
    -- Interface --
    if1B_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if1B_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => DAmode(1),
        cmi_data    => if1B_snp_tx_data,
        cmi_vld     => if1B_snp_tx_vld,
        cmi_next    => if1B_snp_tx_next,
        sample      => DAmode_smpl(1),
        upd_output  => upd
    );


    -- Store and Package --
    if1B_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 27,
        rdout_tag_size      => if1B_tag_size,
        rdout_data_size     => if1B_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if1B_n_tags,
        tag_array           => if1B_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if1B_snp_tx_data,
        rdout_vld   => if1B_snp_tx_vld,
        rdout_next  => if1B_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(27),
        pkg_vld     => if_gloPM_tx_vld(27),
        pkg_next    => if_gloPM_tx_next(27),
        upd         => upd
    );
    
    -- CMI GloPM --
    if1B_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(27),
        tx_vld              => if_gloPM_tx_vld(27),
        tx_next             => if_gloPM_tx_next(27),
        rx_data             => if_gloPM_rx_data(27),
        scalarize(rx_vld)   => if_gloPM_rx_vld(27),
        rx_next             => vectorize(if_gloPM_rx_next(27))
    );
    
    --===========--
    -- INTF 0x1C --
    --===========--
    
    -- Interface --
    if1C_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if1C_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => DAmode(2),
        cmi_data    => if1C_snp_tx_data,
        cmi_vld     => if1C_snp_tx_vld,
        cmi_next    => if1C_snp_tx_next,
        sample      => DAmode_smpl(2),
        upd_output  => upd
    );


    -- Store and Package --
    if1C_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 28,
        rdout_tag_size      => if1C_tag_size,
        rdout_data_size     => if1C_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if1C_n_tags,
        tag_array           => if1C_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if1C_snp_tx_data,
        rdout_vld   => if1C_snp_tx_vld,
        rdout_next  => if1C_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(28),
        pkg_vld     => if_gloPM_tx_vld(28),
        pkg_next    => if_gloPM_tx_next(28),
        upd         => upd
    );
    
    -- CMI GloPM --
    if1C_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(28),
        tx_vld              => if_gloPM_tx_vld(28),
        tx_next             => if_gloPM_tx_next(28),
        rx_data             => if_gloPM_rx_data(28),
        scalarize(rx_vld)   => if_gloPM_rx_vld(28),
        rx_next             => vectorize(if_gloPM_rx_next(28))
    );
    

    --===========--
    -- INTF 0x1D --
    --===========--
    
    -- Interface --
    if1D_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if1D_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => DAmode(3),
        cmi_data    => if1D_snp_tx_data,
        cmi_vld     => if1D_snp_tx_vld,
        cmi_next    => if1D_snp_tx_next,
        sample      => DAmode_smpl(3),
        upd_output  => upd
    );


    -- Store and Package --
    if1D_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 29,
        rdout_tag_size      => if1D_tag_size,
        rdout_data_size     => if1D_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if1D_n_tags,
        tag_array           => if1D_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if1D_snp_tx_data,
        rdout_vld   => if1D_snp_tx_vld,
        rdout_next  => if1D_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(29),
        pkg_vld     => if_gloPM_tx_vld(29),
        pkg_next    => if_gloPM_tx_next(29),
        upd         => upd
    );
    
    -- CMI GloPM --
    if1D_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(29),
        tx_vld              => if_gloPM_tx_vld(29),
        tx_next             => if_gloPM_tx_next(29),
        rx_data             => if_gloPM_rx_data(29),
        scalarize(rx_vld)   => if_gloPM_rx_vld(29),
        rx_next             => vectorize(if_gloPM_rx_next(29))
    );
    

    --===========--
    -- INTF 0x1E --
    --===========--
    
    -- Interface --
    if1E_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if1E_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => DAmode(4),
        cmi_data    => if1E_snp_tx_data,
        cmi_vld     => if1E_snp_tx_vld,
        cmi_next    => if1E_snp_tx_next,
        sample      => DAmode_smpl(4),
        upd_output  => upd
    );


    -- Store and Package --
    if1E_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 30,
        rdout_tag_size      => if1E_tag_size,
        rdout_data_size     => if1E_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if1E_n_tags,
        tag_array           => if1E_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if1E_snp_tx_data,
        rdout_vld   => if1E_snp_tx_vld,
        rdout_next  => if1E_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(30),
        pkg_vld     => if_gloPM_tx_vld(30),
        pkg_next    => if_gloPM_tx_next(30),
        upd         => upd
    );
    
    -- CMI GloPM --
    if1E_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(30),
        tx_vld              => if_gloPM_tx_vld(30),
        tx_next             => if_gloPM_tx_next(30),
        rx_data             => if_gloPM_rx_data(30),
        scalarize(rx_vld)   => if_gloPM_rx_vld(30),
        rx_next             => vectorize(if_gloPM_rx_next(30))
    );
    

    --===========--
    -- INTF 0x1F --
    --===========--
    
    -- Interface --
    if1F_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if1F_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => DAmode(5),
        cmi_data    => if1F_snp_tx_data,
        cmi_vld     => if1F_snp_tx_vld,
        cmi_next    => if1F_snp_tx_next,
        sample      => DAmode_smpl(5),
        upd_output  => upd
    );


    -- Store and Package --
    if1F_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 31,
        rdout_tag_size      => if1F_tag_size,
        rdout_data_size     => if1F_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if1F_n_tags,
        tag_array           => if1F_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if1F_snp_tx_data,
        rdout_vld   => if1F_snp_tx_vld,
        rdout_next  => if1F_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(31),
        pkg_vld     => if_gloPM_tx_vld(31),
        pkg_next    => if_gloPM_tx_next(31),
        upd         => upd
    );
    
    -- CMI GloPM --
    if1F_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(31),
        tx_vld              => if_gloPM_tx_vld(31),
        tx_next             => if_gloPM_tx_next(31),
        rx_data             => if_gloPM_rx_data(31),
        scalarize(rx_vld)   => if_gloPM_rx_vld(31),
        rx_next             => vectorize(if_gloPM_rx_next(31))
    );
    

    --===========--
    -- INTF 0x20 --
    --===========--
    
    -- Interface --
    if20_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if20_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => DAmode(6),
        cmi_data    => if20_snp_tx_data,
        cmi_vld     => if20_snp_tx_vld,
        cmi_next    => if20_snp_tx_next,
        sample      => DAmode_smpl(6),
        upd_output  => upd
    );


    -- Store and Package --
    if20_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 32,
        rdout_tag_size      => if20_tag_size,
        rdout_data_size     => if20_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if20_n_tags,
        tag_array           => if20_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if20_snp_tx_data,
        rdout_vld   => if20_snp_tx_vld,
        rdout_next  => if20_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(32),
        pkg_vld     => if_gloPM_tx_vld(32),
        pkg_next    => if_gloPM_tx_next(32),
        upd         => upd
    );
    
    -- CMI GloPM --
    if20_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(32),
        tx_vld              => if_gloPM_tx_vld(32),
        tx_next             => if_gloPM_tx_next(32),
        rx_data             => if_gloPM_rx_data(32),
        scalarize(rx_vld)   => if_gloPM_rx_vld(32),
        rx_next             => vectorize(if_gloPM_rx_next(32))
    );
    

    --===========--
    -- INTF 0x21 --
    --===========--
    
    -- Interface --
    if21_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if21_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => DAmode(7),
        cmi_data    => if21_snp_tx_data,
        cmi_vld     => if21_snp_tx_vld,
        cmi_next    => if21_snp_tx_next,
        sample      => DAmode_smpl(7),
        upd_output  => upd
    );


    -- Store and Package --
    if21_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 33,
        rdout_tag_size      => if21_tag_size,
        rdout_data_size     => if21_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if21_n_tags,
        tag_array           => if21_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if21_snp_tx_data,
        rdout_vld   => if21_snp_tx_vld,
        rdout_next  => if21_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(33),
        pkg_vld     => if_gloPM_tx_vld(33),
        pkg_next    => if_gloPM_tx_next(33),
        upd         => upd
    );
    
    -- CMI GloPM --
    if21_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(33),
        tx_vld              => if_gloPM_tx_vld(33),
        tx_next             => if_gloPM_tx_next(33),
        rx_data             => if_gloPM_rx_data(33),
        scalarize(rx_vld)   => if_gloPM_rx_vld(33),
        rx_next             => vectorize(if_gloPM_rx_next(33))
    );
    

    --===========--
    -- INTF 0x22 --
    --===========--
    
    -- Interface --
    if22_smp_cnt: entity work.sample_cnt
    GENERIC MAP
    (
        data_size => if22_rdout_size
    )
    PORT MAP
    (
        clk         => clk,
        inp         => DAmode(8),
        cmi_data    => if22_snp_tx_data,
        cmi_vld     => if22_snp_tx_vld,
        cmi_next    => if22_snp_tx_next,
        sample      => DAmode_smpl(8),
        upd_output  => upd
    );


    -- Store and Package --
    if22_store_n_pkg: entity work.store_n_pkg
    GENERIC MAP
    (
        intf_number         => 34,
        rdout_tag_size      => if22_tag_size,
        rdout_data_size     => if22_rdout_size,
        pkg_data_size       => pkg_data_size,
        pkg_tag_size        => pkg_tag_size,
        tag_n               => if22_n_tags,
        tag_array           => if22_list_tags
    )
    PORT MAP
    (
        clk         => clk,
        rdout_data  => if22_snp_tx_data,
        rdout_vld   => if22_snp_tx_vld,
        rdout_next  => if22_snp_tx_next,
        pkg_data    => if_gloPM_tx_data(34),
        pkg_vld     => if_gloPM_tx_vld(34),
        pkg_next    => if_gloPM_tx_next(34),
        upd         => upd
    );
    
    -- CMI GloPM --
    if22_gloPM_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                 => clk,
        tx_data             => if_gloPM_tx_data(34),
        tx_vld              => if_gloPM_tx_vld(34),
        tx_next             => if_gloPM_tx_next(34),
        rx_data             => if_gloPM_rx_data(34),
        scalarize(rx_vld)   => if_gloPM_rx_vld(34),
        rx_next             => vectorize(if_gloPM_rx_next(34))
    );
    

    --====================--
    -- global priorityMUX --
    --====================--
    
    pMUX: entity work.priority_mux
    GENERIC MAP
    (
        data_size   => pkg_data_size,
        n           => intf_n
    )
    PORT MAP
    (
        clk         => clk,
        inp_data    => if_gloPM_rx_data,
        inp_vld     => if_gloPM_rx_vld,
        inp_next    => if_gloPM_rx_next,
        muxxed_data => diag_data,
        muxxed_vld  => diag_vld,
        muxxed_next => diag_next
    );
        
    
end architecture struct;
