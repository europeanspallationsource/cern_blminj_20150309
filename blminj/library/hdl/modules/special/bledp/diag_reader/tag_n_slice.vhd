
------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/08/2012
Module Name:    tag_n_slice

-----------
Description
-----------

    The tag_n_slice module creates a packages with a header (distinguishable by a leading '1') containing input informations (tag)
    and additional informations (chan) and 1-n data slices (distinguishable by a leading '0'). The input data is sliced into 1-n parts 
    depending on the output size (sliced_data_size).


------------------
Generics/Constants
------------------

    inp_data_size       := size of the data-part in the input CMI data signal
    sliced_data_size    := size of the data-part in the output CMI data signal (including the control bit)
    tag_size            := size of the tag in the output header
    tag_n               := tag number
    chan_n              := channel number that is combined with the tag forming the header
    
    slice_size          := size of the slices without the control bit
    slice_n             := # of slices
    slice_n_vsize       := vectorsize for the slice counter
    chan_size           := size of the channel in the given header size
    chan                := chan number in bits
    tag                 := tag number in bits
    
-----------
Limitations
-----------
    
    none
    
----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg

-----------------
Necessary Modules
-----------------

    CMI
    
---------------
Necessary Cores
---------------

    none 

--------------
Implementation
--------------

    From a given input that looks like this:

        --------------------------
        ¦              data         ¦
        --------------------------

    the module adds a chan number and a header, resulting in the following output packages

        ----------------
        ¦1¦ chan ¦ tag ¦
        ----------------
        ¦0¦    data    ¦ (MSBs)
        ----------------
        ¦0¦    data    ¦
        ----------------
        ¦0¦    data    ¦ (LSBs)
        ----------------
        ......

    The number of data slices depends on the relationship between "inp_data_size" and "sliced_data_size".

------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity tag_n_slice is

generic
(
    inp_data_size:      integer;
    sliced_data_size:   integer;
    tag_size:           integer;
    tag_n:              integer;
    chan_n:             integer 
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    inp_data:       in  std_logic_vector(inp_data_size-1 downto 0);
    inp_vld:        in  std_logic;
    inp_next:       out std_logic;
    -- CMI Output
    sliced_data:    out std_logic_vector(sliced_data_size-1 downto 0); -- including protocol bit
    sliced_vld:     out std_logic := '0';
    sliced_next:    in  std_logic
);

end entity tag_n_slice;


architecture struct of tag_n_slice is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- slice size
    constant slice_size:        integer := sliced_data_size - 1;
    
    -- number of slices
    constant slice_n:           integer := INTEGER(CEIL(REAL(inp_data_size)/REAL(slice_size)));
    constant slice_n_vsize:     integer := get_vsize(slice_n);
    
    -- header informations
    constant chan_size:         integer := slice_size - tag_size;
    constant chan:              UNSIGNED(chan_size-1 downto 0) := TO_UNSIGNED(chan_n, chan_size);
    constant tag:               UNSIGNED(tag_size-1 downto 0)  := TO_UNSIGNED(tag_n, tag_size);
    
    --======--
    -- FSMS --
    --======--
    
    type cmi_state_t is (idle, wait_for_slice);
    signal cmi_present_state, cmi_next_state: cmi_state_t := idle;
    
    type slice_state_t is (idle, send_header, send_data);
    signal slice_present_state, slice_next_state: slice_state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Slice Counter
    signal slice_cnt:   UNSIGNED(slice_n_vsize-1 downto 0) := (others => '0');
    signal slice_rst:   std_logic;
    signal slice_dec:   std_logic;

    
    --===========--
    -- REGISTERS --
    --===========--
    
    signal header:      std_logic_vector(slice_size-1 downto 0);
    signal full_data:   std_logic_vector(slice_size*slice_n-1 downto 0);
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--

    signal sel_header:      std_logic;
    signal sliced_vld_int:  std_logic;
    signal slice_busy:      std_logic;
    signal slice_en:        std_logic;
    
BEGIN

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if slice_rst= '1' then
                slice_cnt <= TO_UNSIGNED(slice_n-1, slice_n_vsize);
            elsif slice_dec = '1' then
                slice_cnt <= slice_cnt - 1;
            end if;
            
        end if;
    end process;


    --============--
    -- RENAME INP --
    --============--
    
    process(all) is
    begin
        if inp_data_size < slice_size*slice_n then
            full_data(slice_size*slice_n-1 downto inp_data_size)    <= (others => '0');
            full_data(inp_data_size-1 downto 0)                     <= inp_data;
        else
            full_data <= inp_data;
        end if;
        
        header <= STD_LOGIC_VECTOR(chan) & STD_LOGIC_VECTOR(tag);
        
    end process;
    

    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if sliced_next = '1' then
                sliced_vld  <= sliced_vld_int;
                if sel_header = '1' then
                    sliced_data <= '1' & header;
                else
                    sliced_data <= '0' & full_data((TO_INTEGER(slice_cnt)+1)*slice_size -1 downto TO_INTEGER(slice_cnt)*slice_size);
                end if;
            end if;
                    
        end if;
    end process;
    
    
    
    --=============--
    -- CMI CONTROL --
    --=============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            cmi_present_state <= cmi_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        inp_next    <= '1';
        -- Enable Slice Ctrl
        slice_en    <= '0';
        
        case cmi_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if inp_vld = '1' then
                    inp_next        <= '0';
                    slice_en        <= '1';
                    cmi_next_state  <= wait_for_slice;
                else
                    cmi_next_state  <= idle;
                end if;
            
            --===============--
            when wait_for_slice =>
            --===============--
            
                if slice_busy = '0' then
                    cmi_next_state  <= idle;
                else
                    inp_next        <= '0';
                    cmi_next_state  <= wait_for_slice;
                end if;
                
            
        end case;
    end process;
    
    
    --===============--
    -- SLICE CONTROL --
    --===============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            slice_present_state <= slice_next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        sliced_vld_int  <= '0';
        -- selector
        sel_header      <= '0';
        -- Slice Counter
        slice_rst       <= '0';
        slice_dec       <= '0';
        -- Busy
        slice_busy      <= '1';
        
        case slice_present_state is
            
            --===============--
            when idle =>
            --===============--
                
                slice_busy  <= '0';
                if slice_en = '1' then
                    if sliced_next = '1' then
                        sel_header          <= '1';
                        sliced_vld_int      <= '1';
                        slice_next_state    <= send_data;
                    else
                        slice_next_state    <= send_header;
                    end if;
                else
                    slice_next_state <= idle;
                end if;
                
            --===============--
            when send_header =>
            --===============-- 
                
                if sliced_next = '1' then
                    sel_header          <= '1';
                    sliced_vld_int      <= '1';
                    slice_next_state    <= send_data;
                else
                    slice_next_state    <= send_header;
                end if;
                
            --===============--
            when send_data =>
            --===============-- 
                
                if sliced_next = '1' then
                    sliced_vld_int <= '1';
                    if slice_cnt = 0 then
                        slice_busy          <= '0';
                        slice_rst           <= '1';
                        slice_next_state    <= idle;
                    else
                        slice_dec           <= '1';
                        slice_next_state    <= send_data;
                    end if;
                else
                    slice_next_state    <= send_data;
                end if;
                
            
            
        end case;
    end process;
    
    
    
end architecture struct;
