------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/02/2012
Module Name:    startup_chan  

-----------
Description
-----------

Startup Procedure for the analog channels powered by Circuit Breaker 1 and 2 on the BLEDP card.

------------------
Generics/Constants
------------------

    Tclk_ns         := CC length

    cb_cnt_vsize    := vectorsize of the local counter/timer
    T_release       := point in time to after which the outputs are released
    
-----------
Limitations
-----------

    < limitations for the generics and/or the module itself >

----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

    none
    

---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    After CBx changes from 0 to 1 (after startup or while running), the PRE, CLR and STOPL outputs are hold to 1 for a minimum of 2 seconds.

------------------------
Not implemented Features
------------------------



*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity startup_chan is

generic
(
    Tclk_ns: integer := 8 -- 125 MHz (default)
);
port
(
    -- Clock
    clk:        in  std_logic;
    -- Internal Tap
    ON_CB:      in  std_logic;
    -- BLEDP Output
    CLR:        out std_logic;
    PRE:        out std_logic;
    STOPL:      out std_logic   
);

end entity startup_chan;


architecture struct of startup_chan is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- vectorsize of the timer
    constant cb_cnt_vsize:     integer := get_vsize(get_maxcnt(Tclk_ns, 2000000000)); -- max 5s
    
    
    constant T_release:     integer := integer(CEIL(real(2000000000)/real(Tclk_ns)));  -- 2s
    
    --=====--
    -- FSM --
    --=====--
    
    type cb_state is (idle, startup, ctrl, reset);
    signal present_state, next_state: cb_state := startup;
    
    --=========--
    -- COUNTER --
    --=========--
    
    signal cb_cnt:      UNSIGNED(cb_cnt_vsize-1 downto 0) := (others => '0');
    signal cb_rst:      std_logic;
    signal cb_inc:      std_logic;
    
BEGIN

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if cb_rst = '1' then
                cb_cnt <= (others => '0');
            elsif cb_inc = '1' then
                cb_cnt <= cb_cnt + 1;
            end if;
        end if;
    end process;

    --===============--
    -- STATE MACHINE --
    --===============--
    
    process(clk) is
   begin
      if rising_edge(clk) then
         present_state  <= next_state;
        end if;
   end process;
   
   
    process (all)
    begin
        --defaults
        cb_rst  <= '0';
        cb_inc  <= '0';
        PRE     <= '0';
        CLR     <= '0';
        STOPL   <= '0';
        
        case present_state is
            
            --===============--
            when startup =>
            --===============--
                -- hold them to 1 after FPGA startup
                PRE     <= '1';
                CLR     <= '1';
                STOPL   <= '1';
                if ON_CB = '1' then
                    next_state <= ctrl;
                else
                    next_state <= startup;
                end if;
            
            --===============-- 
            when idle =>
            --===============--
            
                if ON_CB = '1' then
                    next_state <= ctrl;
                else
                    next_state <= idle;
                end if;
            
            --===============--
            when ctrl =>
            --===============--
            
                PRE     <= '1';
                CLR     <= '1';
                STOPL   <= '1';
                if cb_cnt = T_release then -- later
                    cb_rst      <= '1';
                    next_state  <= reset;
                else
                    cb_inc      <= '1';
                    next_state  <= ctrl;
                end if;
            
            --===============-- 
            when reset =>
            --===============--
            
                if ON_CB = '0' then
                    next_state <= idle;
                else
                    next_state <= reset;
                end if;
                
            
            --===============-- 
            when others =>
            --===============--
            
                next_state <= idle;
                
        end case;
    end process;

    
end architecture struct;
