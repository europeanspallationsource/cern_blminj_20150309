------------------------------
/*
Company: CERN - BE/BI/BL
Engineer: Marcel Alsdorf
Create Date:    01/02/2012
Module Name:    sat_monitor 


-----------
Description
-----------

    Reacts on saturation of an BLEDP input channel.

------------------
Generics/Constants
------------------

    none
    
-----------
Limitations
-----------

    < limitations for the generics and/or the module itself >

----------------------------
Necessary Packages/Libraries
----------------------------

    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

    none
    
---------------
Necessary Cores
---------------

    none  
    
--------------
Implementation
--------------

    In case of saturation (STOPOUT = 1), CLR and PRE are being set until COUNT_M and COUNT_P are both 1.

------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity sat_monitor is

port
(
    -- Clock
    clk:            in  std_logic;
    -- BLEDP Input 
    STOPOUT:        in  std_logic;
    COUNT_P:        in  std_logic;
    COUNT_M:        in  std_logic;
    -- BLEDP Output
    PRE:            out std_logic;
    CLR:            out std_logic
);

end entity sat_monitor;


architecture struct of sat_monitor is
    
    --=====--
    -- FSM --
    --=====--
    
    type stop_state is (idle, sat);
    signal present_state, next_state: stop_state := idle;
    
    
BEGIN

    --===============--
    -- STATE MACHINE --
    --===============--
    
    process(clk) is
    begin
      if rising_edge(clk) then
         present_state  <= next_state;
        end if;
    end process;
   
    process (all)
    begin
        -- DEFAULTS
        PRE <= '0';
        CLR <= '0';
        
        case present_state is
        
            --===============--
            when idle =>
            --===============--
            
                if STOPOUT = '1' then
                    next_state <= sat;
                else
                    next_state <= idle;
                end if;
            
            --===============--
            when sat =>
            --===============--
            
                PRE <= '1';
                CLR <= '1';
                if COUNT_M = '1' AND COUNT_P = '1' then
                    next_state <= idle;
                else
                    next_state <= sat;
                end if;
                    
            --===============-- 
            when others =>
            --===============--
            
                next_state <= idle;
                
        end case;
    end process;

    
end architecture struct;
