--------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    25/09/2014
--Module Name:    fdfc_ss_controller  
--
-------------
--Description
-------------
--
--Startup and Saturation (SS) Procedure for the analog channels powered by Circuit Breaker 1 and 2 on the BLEDP card.
--
--------------------
--Generics/Constants
--------------------
--
--    Tclk_ns         := CC length
--
--    cb_cnt_vsize    := vectorsize of the local counter/timer
--    T_release       := point in time to after which the outputs are released
--    
-------------
--Limitations
-------------
--
--    < limitations for the generics and/or the module itself >
--
------------------------------
--Necessary Packages/Libraries
------------------------------
--
--    vhdl_func_pkg
--    
-------------------
--Necessary Modules
-------------------
--
--    none
--    
--
-----------------
--Necessary Cores
-----------------
--
--    none  
--    
----------------
--Implementation
----------------
--
--------------------------
--Not implemented Features
--------------------------
--
--
--
--
--------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity fdfc_ss_controller is

generic
(
    Tclk_ns: integer := 8 -- 125 MHz (default)
);
port
(
    -- Clock
    clk:        in  std_logic;
    nrst:       in  std_logic;
    -- Internal Tap
    ON_CB:      in  std_logic;
	-- BLEDP Input 
    STOPOUT:    in  std_logic;
    COUNT_P:    in  std_logic;
    COUNT_M:    in  std_logic;
    -- BLEDP Output
    CLR:        out std_logic;
    PRE:        out std_logic;
    STOPL:      out std_logic;
	-- control port
	-- this input decides to which branch of the integrator the current 
	-- is routed after the startup and after the saturation recovery
	startup_dir:in	std_logic;		
	-- status output
	saturation:	out std_logic;
	startup:	out std_logic
);

end entity fdfc_ss_controller;


architecture struct of fdfc_ss_controller is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- vectorsize of the timer
    constant cb_cnt_vsize:  integer := get_vsize(get_maxcnt(Tclk_ns, 2000000000)); -- max 5s
    
    
    constant T_release:     integer := integer(CEIL(real(2000000000)/real(Tclk_ns)));  -- 2s
    constant T_clr:     	integer := integer(CEIL(real(100000000)/real(Tclk_ns)));  -- 100ms
    
    --=====--
    -- FSM --
    --=====--
    
    type cb_state is (idle, start0, start1, start2, saturation0, saturation1, saturation2);
    signal present_state, next_state: cb_state;
	
	signal int_nrst : std_logic;
	signal stopout_sync : std_logic;
	signal count_p_sync : std_logic;
	signal count_p_sync_d : std_logic;
	signal count_m_sync : std_logic;
	signal count_m_sync_d : std_logic;
    
    --=========--
    -- COUNTER --
    --=========--
    
    signal cb_cnt:      UNSIGNED(cb_cnt_vsize-1 downto 0) := (others => '0');
    signal cb_rst:      std_logic;
    signal cb_inc:      std_logic;
    
BEGIN

	--===============--
    -- Inputs synchronization
    --===============--
	
	process(clk) is
    begin
        if rising_edge(clk) then
            stopout_sync <= STOPOUT;
            count_p_sync_d <= COUNT_P;
            count_p_sync <= count_p_sync_d;
            count_m_sync_d <= COUNT_M;
            count_m_sync <= count_m_sync_d;
        end if;
    end process;

	--===============--
    -- Reset controller
    --===============--
	
	process(clk) is
    begin
        if rising_edge(clk) then
            int_nrst <= nrst and ON_CB;
        end if;
    end process;
	

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if cb_rst = '1' or int_nrst = '0' then
                cb_cnt <= (others => '0');
            elsif cb_inc = '1' then
                cb_cnt <= cb_cnt + 1;
            end if;
        end if;
    end process;

    --===============--
    -- STATE MACHINE --
    --===============--
	
	process(clk) is
	begin
		if rising_edge(clk) then
			if int_nrst = '0' then
				present_state <= start0;
			else
				present_state <= next_state;
			end if;
		end if;
	end process;
   
   
    process (all)
    begin
        --defaults
        cb_rst  <= '0';
        cb_inc  <= '0';
        PRE     <= '0';
        CLR     <= '0';
        STOPL   <= '0';
        saturation   <= '0';
        startup   <= '0';
		next_state <= present_state;
        
        case present_state is
            
            --===============--
            when start0 =>
            --===============--
                PRE     <= '1';
                CLR     <= '1';
                STOPL   <= '1';
				startup <= '1';
                if cb_cnt = T_release then 
                    cb_rst      <= '1';
                    next_state  <= start1;
                else
                    cb_inc      <= '1';
                    next_state  <= start0;
                end if;
			
			--===============-- 
            when start1 =>
            --===============--
				PRE     <= '1';
                CLR     <= '1';
                STOPL   <= '0';
				startup <= '1';
                next_state  <= start2;
			
			--===============-- 
            when start2 =>
            --===============--
				if startup_dir = '0' then
					PRE     <= '0';
					CLR     <= '1';
				else
					PRE     <= '1';
					CLR     <= '0';
				end if;
                STOPL   <= '0';
				startup <= '1';
				if cb_cnt = T_clr then 
                    cb_rst      <= '1';
                    next_state  <= idle;
                else
                    cb_inc      <= '1';
                    next_state  <= start2;
                end if;
            
            --===============-- 
            when idle =>
            --===============--
				-- stay in idle until ON_CB toggled or satuturation detected
				if stopout_sync = '1' then
                    next_state <= saturation0;
                end if;
				
			--===============--
            when saturation0 =>
            --===============--
                PRE     <= '1';
                CLR     <= '1';
                saturation <= '1';
                if count_p_sync = '1' and count_m_sync = '1' then
                    next_state <= saturation1;
                end if;
			
			--===============-- 
            when saturation1 =>
            --===============--
				PRE     <= '1';
                CLR     <= '1';
				saturation <= '1';
                next_state  <= saturation2;
			
			--===============-- 
            when saturation2 =>
            --===============--
				if startup_dir = '0' then
					PRE     <= '0';
					CLR     <= '1';
				else
					PRE     <= '1';
					CLR     <= '0';
				end if;
				saturation <= '1';
                next_state  <= idle;
                
            
            --===============-- 
            when others =>
            --===============--
                next_state <= idle;
                
        end case;
    end process;

    
end architecture struct;
