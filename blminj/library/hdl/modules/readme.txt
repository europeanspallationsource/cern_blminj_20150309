he library is grouped into the following subfolders:
		cmi 				// modules with at least one CMI input or output
			interconnect 	// includes the CMI interconnect modules
			interfaces		// includes modules that are interfacing to external chips/sensors
			processing		// includes modules that manipulate data
			dataflow		// includes modules that are controlling/manipulating the dataflow, not the data itself
			sources			// includes modules that create data
		
		std 				// modules without CMI connections
			interfaces		// includes modules that are interfacing to external chips/sensors
			processing		// includes modules that manipulate data
			dataflow		// includes modules that are controlling/manipulating the dataflow, not the data itself
			sources			// includes modules that create data
			components		// includes components (combinatorical) that can be used in modules
		
		special				// very specific modules for a certain card/IP
			bledp			// special bledp modules
			
		packages			// general packages