------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/03/2014
Module Name:    LOSS_MEM_gen_CMA

-----------------
Short Description
-----------------

    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

--------------
Implementation
--------------

-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity LOSS_MEM_gen_CMA is

port
(
    -- Clock
    clk:                in  std_logic;
    -- Timing
    trigger:            in  std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(127 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic
);

end entity LOSS_MEM_gen_CMA;


architecture struct of LOSS_MEM_gen_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant addr_max:          integer := 96;
    constant data_max:          integer := 120;
    
    constant data_size:         integer := 64;
    constant addr_size:         integer := 64;
    
    --======--
    -- FSMs --
    --======--
    
    type state_t is (idle, set_mem, wait_for_trigger);
    signal present_state, next_state: state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Data Content Counter
    signal data_cnt:    UNSIGNED(data_size-1 downto 0) := (others => '0');
    signal data_rst:    std_logic;
    signal data_inc:    std_logic;
    
    -- Data Content Counter
    signal addr_cnt:    UNSIGNED(addr_size-1 downto 0) := (others => '0');
    signal addr_rst:    std_logic;
    signal addr_inc:    std_logic;
    
    
    --===========--
    -- REGISTERS --
    --===========--
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal data_int:        UNSIGNED(data_size-1 downto 0);
    signal addr_int:        UNSIGNED(addr_size-1 downto 0);
    signal out_vld_int:     std_logic;
    
BEGIN

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if data_rst= '1' then
                data_cnt <= (others => '0');
            elsif data_inc = '1' then
                data_cnt <= data_cnt + 1;
            end if;
            
            if addr_rst= '1' then
                addr_cnt <= (others => '0');
            elsif addr_inc = '1' then
                addr_cnt <= addr_cnt + 1;
            end if;
            
        end if;
    end process;
    
    
    --===============--
    -- COMBINATORICS --
    --===============--
    
    
    addr_int <= addr_cnt;
    
    -- Data
    process(all) is
    begin
        case addr_int is
            -- LoC
            when 64UX"00" => data_int <= data_cnt + 64UX"1100000000";
            when 64UX"01" => data_int <= 64UX"1100000100";
            when 64UX"02" => data_int <= 64UX"927C0";
            
            when 64UX"03" => data_int <= data_cnt + 64UX"1200000000";
            when 64UX"04" => data_int <= 64UX"1200000040";
            when 64UX"05" => data_int <= 64UX"927C1";
            
            when 64UX"06" => data_int <= data_cnt + 64UX"1300000000";
            when 64UX"07" => data_int <= 64UX"1300000100";
            when 64UX"08" => data_int <= 64UX"927BF";
            
            when 64UX"09" => data_int <= data_cnt + 64UX"1400000000";
            when 64UX"0A" => data_int <= 64UX"1400000040";
            when 64UX"0B" => data_int <= 64UX"927C0";
            
            when 64UX"0C" => data_int <= data_cnt + 64UX"1500000000";
            when 64UX"0D" => data_int <= 64UX"1500000100";
            when 64UX"0E" => data_int <= 64UX"927C1";
            
            when 64UX"0F" => data_int <= data_cnt + 64UX"1600000000";
            when 64UX"10" => data_int <= 64UX"1600000040";
            when 64UX"11" => data_int <= 64UX"927BF";
            
            when 64UX"12" => data_int <= data_cnt + 64UX"1700000000";
            when 64UX"13" => data_int <= 64UX"1700000100";
            when 64UX"14" => data_int <= 64UX"927C0";
            
            when 64UX"15" => data_int <= data_cnt + 64UX"1800000000";
            when 64UX"16" => data_int <= 64UX"1800000040";
            when 64UX"17" => data_int <= 64UX"927C1";
            
            -- LoBP
            when 64UX"18" => data_int <= data_cnt + 64UX"E10000000";
            when 64UX"19" => data_int <= 64UX"E10000100";
            when 64UX"1A" => data_int <= 64UX"7A120";
            
            when 64UX"1B" => data_int <= data_cnt + 64UX"E20000000";
            when 64UX"1C" => data_int <= 64UX"E20000040";
            when 64UX"1D" => data_int <= 64UX"7A121";
           
            when 64UX"1E" => data_int <= data_cnt + 64UX"E30000000";
            when 64UX"1F" => data_int <= 64UX"E30000100";
            when 64UX"20" => data_int <= 64UX"7A11F";
            
            when 64UX"21" => data_int <= data_cnt + 64UX"E40000000";
            when 64UX"22" => data_int <= 64UX"E40000040";
            when 64UX"23" => data_int <= 64UX"7A120";
            
            when 64UX"24" => data_int <= data_cnt + 64UX"E50000000";
            when 64UX"25" => data_int <= 64UX"E50000100";
            when 64UX"26" => data_int <= 64UX"7A121";
            
            when 64UX"27" => data_int <= data_cnt + 64UX"E60000000";
            when 64UX"28" => data_int <= 64UX"E60000040";
            when 64UX"29" => data_int <= 64UX"7A11F";
            
            when 64UX"2A" => data_int <= data_cnt + 64UX"E70000000";
            when 64UX"2B" => data_int <= 64UX"E70000100";
            when 64UX"2C" => data_int <= 64UX"7A120";
            
            when 64UX"2D" => data_int <= data_cnt + 64UX"E80000000";
            when 64UX"2E" => data_int <= 64UX"E80000040";
            when 64UX"2F" => data_int <= 64UX"7A121";
            
            -- RS #1
            when 64UX"30" => data_int <= data_cnt + 64UX"1100000";
            when 64UX"31" => data_int <= 64UX"1100100";
            
            when 64UX"32" => data_int <= data_cnt + 64UX"1200000";
            when 64UX"33" => data_int <= 64UX"1200040";
            
            when 64UX"34" => data_int <= data_cnt + 64UX"1300000";
            when 64UX"35" => data_int <= 64UX"1300100";
            
            when 64UX"36" => data_int <= data_cnt + 64UX"1400000";
            when 64UX"37" => data_int <= 64UX"1400040";
          
            when 64UX"38" => data_int <= data_cnt + 64UX"1500000";
            when 64UX"39" => data_int <= 64UX"1500100";
            
            when 64UX"3A" => data_int <= data_cnt + 64UX"1600000";
            when 64UX"3B" => data_int <= 64UX"1600040";
            
            when 64UX"3C" => data_int <= data_cnt + 64UX"1700000";
            when 64UX"3D" => data_int <= 64UX"1700100";
            
            when 64UX"3E" => data_int <= data_cnt + 64UX"1800000";
            when 64UX"3F" => data_int <= 64UX"1800040";
            
            -- RS #2
            when 64UX"40" => data_int <= data_cnt + 64UX"210000";
            when 64UX"41" => data_int <= 64UX"210010";
            
            when 64UX"42" => data_int <= data_cnt + 64UX"220000";
            when 64UX"43" => data_int <= 64UX"220040";
            
            when 64UX"44" => data_int <= data_cnt + 64UX"230000";
            when 64UX"45" => data_int <= 64UX"230100";
          
            when 64UX"46" => data_int <= data_cnt + 64UX"240000";
            when 64UX"47" => data_int <= 64UX"240040";
          
            when 64UX"48" => data_int <= data_cnt + 64UX"250000";
            when 64UX"49" => data_int <= 64UX"250100";
            
            when 64UX"4A" => data_int <= data_cnt + 64UX"260000";
            when 64UX"4B" => data_int <= 64UX"260040";
            
            when 64UX"4C" => data_int <= data_cnt + 64UX"270000";
            when 64UX"4D" => data_int <= 64UX"270100";
            
            when 64UX"4E" => data_int <= data_cnt + 64UX"280000";
            when 64UX"4F" => data_int <= 64UX"280040";
            
            -- RS #3
            when 64UX"50" => data_int <= data_cnt + 64UX"31000";
            when 64UX"51" => data_int <= 64UX"31100";
            
            when 64UX"52" => data_int <= data_cnt + 64UX"32000";
            when 64UX"53" => data_int <= 64UX"32040";
           
            when 64UX"54" => data_int <= data_cnt + 64UX"33000";
            when 64UX"55" => data_int <= 64UX"33100";
            
            when 64UX"56" => data_int <= data_cnt + 64UX"34000";
            when 64UX"57" => data_int <= 64UX"34040";
          
            when 64UX"58" => data_int <= data_cnt + 64UX"35000";
            when 64UX"59" => data_int <= 64UX"35100";
            
            when 64UX"5A" => data_int <= data_cnt + 64UX"36000";
            when 64UX"5B" => data_int <= 64UX"36040";
            
            when 64UX"5C" => data_int <= data_cnt + 64UX"37000";
            when 64UX"5D" => data_int <= 64UX"37100";
            
            when 64UX"5E" => data_int <= data_cnt + 64UX"38000";
            when 64UX"5F" => data_int <= 64UX"38040";
            
            when others => data_int <= data_cnt;
        end case;
    end process;
    
  
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_vld     <= out_vld_int;
                out_data    <= STD_LOGIC_VECTOR(data_int) & STD_LOGIC_VECTOR(addr_int);
            end if;
                    
        end if;
    end process;
    
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        out_vld_int <= '0';
        -- Counters
        addr_inc    <= '0';
        addr_rst    <= '0';
        data_inc    <= '0';
        data_rst    <= '0';

        
        case present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if out_next = '1' then
                    next_state <= set_mem;
                else
                    next_state <= idle;
                end if;

            
            --===============--
            when set_mem =>
            --===============--

                next_state <= set_mem;
                if out_next = '1' then
                    out_vld_int <= '1';
                    if addr_cnt = addr_max-1 then
                        addr_rst    <= '1';
                        next_state  <= wait_for_trigger;
                    else
                        addr_inc <= '1';
                    end if;
                end if;
            
            --===============--
            when wait_for_trigger =>
            --===============--
            
                if trigger = '1' then
                    next_state <= set_mem;
                    if data_cnt = data_max-1 then
                        data_rst <= '1';
                    else
                        data_inc <= '1';
                    end if;
                else
                    next_state <= wait_for_trigger;
                end if;
                
        end case;
    end process;
  
    
    
end architecture struct;
