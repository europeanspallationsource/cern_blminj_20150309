------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2013
Module Name:    dab64x_op


-----------
Description
-----------

    This is the topmodule for the DAB64 Operation Firmware.


------------------
Generics/Constants
------------------

    Tclk_ns             := clock in ns
    
    num_rams            := number of attached storage RAMs
    addr_size           := general address size
    be_size             := size of the write byte enable part
    ram_data_size       := size of the data inside the RAMs
    wrreq_data_size     := size of the data part of the wrreq CMI
    rdreq_data_size     := size of the data part of the rdreq CMI
    vme_wr_size         := data size of the write FIFO/CMIs
    vme_rd_size         := data size of the read FIFO/CMI
    
----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

    

---------------
Necessary Cores
---------------

    pll125 
    
--------------
Implementation
--------------
    
------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

entity dab64x_op is
port
(
    -- external clock
    STX_40MHZ_Osc:              in  std_logic;
    -- VME IOs
    VME_GA:                     in  std_logic_vector(4 downto 0);
    VME_GAP:                    in  std_logic;
    VME_WRn:                    in  std_logic;
    VME_AM:                     in  std_logic_vector(5 downto 0);
    VME_ASn:                    in  std_logic;
    VME_DS0n:                   in  std_logic;
    VME_DS1n:                   in  std_logic;
    VME_IACKn:                  in  std_logic;
    VME_IACKINn:                in  std_logic;
    VME_SYSCLK:                 in  std_logic;
    VME_SYSRESETn:              in  std_logic;
    VME_ADD:                    inout std_logic_vector(31 downto 1);
    VME_LWORDn:                 inout std_logic;
    VME_DATA:                   inout std_logic_vector(31 downto 0);
    VME_BERRn:                  out std_logic;
    VME_RETRYn:                 out std_logic;
    VME_IACKOUTn:               out std_logic;
    VME_IRQn:                   out std_logic_vector(6 downto 1);
    -- VME DAB64 IOs
    Stratix_IRQ_VECTORn:        out std_logic;
    VME_IRQ_Level:              in  std_logic_vector(2 downto 0);
    IRQ_Vector_Enable:          in  std_logic;
    Board_Rst_ONOFFn:           in  std_logic;
    Stratix_VME_ACCn:           out std_logic;
    VME_ForceRetry:             out std_logic;  
    VME_2e_Cycle:               out std_logic;  
    VME_DTACK_EN:               inout std_logic;  
    Stratix_245_OEn:            out std_logic;  
    Stratix_dtackN:             out std_logic;  
    Stratix_SYSFAILn:           out std_logic;
    -- PIM Header
    PIM_IO:                     in  std_logic_vector(64 downto 1);
    PIM_SIG_EVEN:               in  std_logic_vector(24 downto 1);
    PIM_SIG_ODD:                out std_logic_vector(24 downto 1);
    -- Flash
    FLASH_A:                    inout std_logic_vector(22 downto 0);
    FLASH_CEn:                  inout std_logic;
    FLASH_OEn:                  inout std_logic;
    FLASH_WEn:                  inout std_logic;
    Stratix_Flash_csN:          out std_logic;
    -- PM SRAM
--    PM_SRAM_DATA:               inout std_logic_vector(31 downto 0);
--    PM_SRAM_ADD:                out std_logic_vector(20 downto 0);
--    PM_SRAM_Byte_WEn:           out std_logic;
--    PM_SRAM_CE1n:               out std_logic;
--    PM_SRAM_CLK:                out std_logic;
--    PM_SRAM_WRT_LWORDn:         out std_logic;
--    PM_SRAM_OEn:                out std_logic;
--    -- Hor SRAM
--    Hor_SRAM_DATA:              inout std_logic_vector(31 downto 0);
--    Hor_SRAM_ADD:               out std_logic_vector(20 downto 0);
--    Hor_SRAM_Byte_WEn:          out std_logic;
--    Hor_SRAM_CE1n:              out std_logic;
--    Hor_SRAM_CLK:               out std_logic;
--    Hor_SRAM_WRT_LWORDn:        out std_logic;
--    Hor_SRAM_OEn:               out std_logic;
--    -- Vert SRAM
--    Vert_SRAM_DATA:             inout std_logic_vector(31 downto 0);
--    Vert_SRAM_ADD:              out std_logic_vector(20 downto 0);
--    Vert_SRAM_Byte_WEn:         out std_logic;
--    Vert_SRAM_CE1n:             out std_logic;
--    Vert_SRAM_CLK:              out std_logic;
--    Vert_SRAM_WRT_LWORDn:       out std_logic;
--    Vert_SRAM_OEn:              out std_logic;
--    -- SRAM Control 
--    SRAM_BWSAn:                 out std_logic;
--    SRAM_BWSBn:                 out std_logic;
--    SRAM_BWSCn:                 out std_logic;
--    SRAM_BWSDn:                 out std_logic;
--    SRAM_P_FTn:                 out std_logic;
    -- Chip ID
    ModuleID:                  in  std_logic;
    -- Temp Sensor
--    TEMP_CSn:                   out std_logic;                
--    TEMP_CLK:                   out std_logic;
--    TEMP_DATA:                  in  std_logic;
    -- P0 Connector
    P0_BLM_DCin_Ctrl1:          out std_logic;
    P0_BLM_DCin_Ctrl2:          out std_logic;
    P0_BLM_IN:                  in  std_logic_vector(8 downto 1);
    P0_AUX_PM_Start:            in  std_logic;
    P0_AUX_PM_Freeze:           in  std_logic;
    P0_AUX_D:                   in  std_logic_vector(8 downto 2);
    P0_AUX_E:                   in  std_logic_vector(8 downto 6);
    P0_BSCAN1:                  in  std_logic;
    P0_BSCAN2:                  in  std_logic;
    P0_BSCAN3:                  in  std_logic;
    P0_BS:                      in  std_logic_vector(7 downto 6);
    P0_Capture_Start:           in  std_logic;
    P0_LVDS_TCLK:               in  std_logic;
    P0_LVDS_40MHz:              in  std_logic;
    P0_Orbit_Start:             in  std_logic;
    P0_PM_Start:                in  std_logic;
    P0_PM_Freeze:               in  std_logic;
    -- LEDs
    FP_VME_ACC_LEDn:            out std_logic;
    FP_Calib_Mode_LEDn:         out std_logic;
    FP_Configure_OK_LEDn:       out std_logic;
    FP_Orbit_Running_LEDn:      out std_logic;
    FP_PM_Running_LEDn:         out std_logic;
    FP_Pwr_OK_LEDn:             out std_logic;
    FP_Tclk_Det_LEDn:           out std_logic;
    FP_Capture_Running_LEDn:    out std_logic;
    FP_40MHz_Det_LEDn:          out std_logic
);
    
end entity dab64x_op;



architecture struct of dab64x_op is
    
    
    --===========--
    -- CONSTANTS --
    --===========--
    -- Clock
    constant Tclk_ns:                   real := 25.0; -- 80 MHz
    constant T_CLK:                     time := 25000 ps;
    
    -- VME Core
    constant VME_core_data_size:        integer := 64;
    constant VME_core_addr_size:        integer := 64;
        
    -- Downstream/Upstream Core RAMs
    constant all_rams:                  integer := 7;
    constant US_rams:                   integer := 3;
    constant DS_rams:                   integer := 4;
    
    -- Downstream/Upstream Core MSBs
    constant VME_wrreq_byteena_MSB:     integer := 71;
    constant VME_wrreq_d_MSB:           integer := 63;
    
    -- Number of Sync Stages
    constant sync_stages:               integer := 2;
    
    -- Timing
    constant timestamp_size:            integer := 32;
    constant T_INP_BLOCK_START:         time    := 500 us;
    constant T_INP_BLOCK_STOP:          time    := 200 ms;
    constant T_SB_PBL:                  time    := 800 us;
    
    -- US RAMs
    
    constant RAM_data_size:             integer := 64;
    constant RAM_byteena_size:          integer := 8;
    
    constant RAM_PARAM_numwords:        integer := 512;
    constant RAM_PARAM_addr_size:       integer := get_vsize_addr(RAM_PARAM_numwords);
    constant RAM_PARAM_data_size:       integer := RAM_data_size;
    constant RAM_PARAM_byteena_size:    integer := RAM_byteena_size;
    constant RAM_PARAM_init_file:       string  := "PARAM.mif";
    constant RAM_PARAM_type:            string  := "M4K";
    
    constant RAM_PROG_numwords:         integer := 1024;
    constant RAM_PROG_addr_size:        integer := get_vsize_addr(RAM_PROG_numwords);
    constant RAM_PROG_data_size:        integer := RAM_data_size;
    constant RAM_PROG_byteena_size:     integer := RAM_byteena_size;
    constant RAM_PROG_init_file:        string  := "PROG.mif";
    constant RAM_PROG_type:             string  := "M4K";
    
    constant RAM_CTRL_numwords:         integer := 256;
    constant RAM_CTRL_addr_size:        integer := get_vsize_addr(RAM_CTRL_numwords);
    constant RAM_CTRL_data_size:        integer := RAM_data_size;
    constant RAM_CTRL_byteena_size:     integer := RAM_byteena_size;
    constant RAM_CTRL_init_file:        string  := "CTRL.mif";
    constant RAM_CTRL_type:             string  := "M4K";
    
    -- VME Address MSBs
    constant VME_wrreq_PARAM_addr_MSB:  integer := VME_wrreq_byteena_MSB+RAM_PARAM_addr_size;
    constant VME_wrreq_PROG_addr_MSB:   integer := VME_wrreq_byteena_MSB+RAM_PROG_addr_size;
    constant VME_wrreq_CTRL_addr_MSB:   integer := VME_wrreq_byteena_MSB+RAM_CTRL_addr_size;
    
    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- proc SCMI
    constant proc_data_size:        integer := 66;
    -- cmd SCMI
    constant cmd_data_size:         integer := 18;
    
    -- VME_wrreq 
    constant VME_wrreq_data_size:   integer := 133;
    constant VME_wrreq_rx_number:   integer := all_rams;
    
    -- VME_rdreq 
    constant VME_rdreq_data_size:   integer := 61;
    constant VME_rdreq_rx_number:   integer := all_rams;
    
    -- VME_rdout 
    constant VME_rdout_data_size:   integer := 64;
    constant VME_rdout_tx_number:   integer := all_rams;
    constant VME_rdout_rx_number:   integer := 1;
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- System Clock
    signal clk_main:                  std_logic;
    
    -- VME_Interface <-> VME_BusCtrl
    signal VME_ACC:                 std_logic;
    signal VME_BUS_BUSY:            std_logic;
    signal VME_DATA_BUF_OEn:        std_logic;
    signal VME_ADDR_BUF_OEn:        std_logic;
    signal VME_DTACK_BUF_OEn:       std_logic;
    signal VME_RETRY_BUF_OEn:       std_logic;
    signal VME_BERR_BUF_OEn:        std_logic;
    signal VME_DATA_BUF_DIR:        std_logic;
    signal VME_ADDR_BUF_DIR:        std_logic;
    signal VME_DTACKn:              std_logic;
    signal VME_SYSFAILn:            std_logic;
    signal VME_LWORDn_IN:           std_logic;
    signal VME_DATA_IN:             std_logic_vector(31 downto 0);
    signal VME_ADDR_IN:             std_logic_vector(31 downto 1);
    signal VME_LWORDn_OUT:          std_logic;
    signal VME_DATA_OUT:            std_logic_vector(31 downto 0);
    signal VME_ADDR_OUT:            std_logic_vector(31 downto 1);
    
    -- Interlock
    signal BIS_1:                   std_logic;
    signal BIS_2:                   std_logic;
    
    -- External Timing
    signal BP_TIM:                  std_logic;
    signal BEAM_IN_TIM:             std_logic;
    signal BEAM_OUT_TIM:            std_logic;
    
    -- Internal Timing
    signal BP_long_pulse:           std_logic;
    signal BEAM_IN_long_pulse:      std_logic;
    signal BEAM_OUT_long_pulse:     std_logic;
    
    -- Timing Controller --
    signal BP_pulse:                std_logic;
    signal INP_BLOCK:                std_logic;
    signal FINAL_PBL_pulse:            std_logic;
    signal BP_CNT:                  std_logic_vector(timestamp_size-1 downto 0);
    
    
    -- US RAMs
    signal RAM_PARAM_portA_addr:        std_logic_vector(RAM_PARAM_addr_size-1 downto 0);
    signal RAM_PARAM_portA_data:        std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    signal RAM_PARAM_portA_byteena:     std_logic_vector(RAM_PARAM_byteena_size-1 downto 0);
    signal RAM_PARAM_portA_wren:        std_logic;
    signal RAM_PARAM_portA_q:           std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    
    signal RAM_PARAM_portB_addr:        std_logic_vector(RAM_PARAM_addr_size-1 downto 0);
    signal RAM_PARAM_portB_data:        std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    signal RAM_PARAM_portB_byteena:     std_logic_vector(RAM_PARAM_byteena_size-1 downto 0);
    signal RAM_PARAM_portB_wren:        std_logic;
    signal RAM_PARAM_portB_q:           std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    
    signal RAM_PROG_portA_addr:         std_logic_vector(RAM_PROG_addr_size-1 downto 0);
    signal RAM_PROG_portA_data:         std_logic_vector(RAM_PROG_data_size-1 downto 0);
    signal RAM_PROG_portA_byteena:      std_logic_vector(RAM_PROG_byteena_size-1 downto 0);
    signal RAM_PROG_portA_wren:         std_logic;
    signal RAM_PROG_portA_q:            std_logic_vector(RAM_PROG_data_size-1 downto 0);
    
    signal RAM_PROG_portB_addr:         std_logic_vector(RAM_PROG_addr_size-1 downto 0);
    signal RAM_PROG_portB_data:         std_logic_vector(RAM_PROG_data_size-1 downto 0);
    signal RAM_PROG_portB_byteena:      std_logic_vector(RAM_PROG_byteena_size-1 downto 0);
    signal RAM_PROG_portB_wren:         std_logic;
    signal RAM_PROG_portB_q:            std_logic_vector(RAM_PROG_data_size-1 downto 0);
    
    signal RAM_CTRL_portA_addr:         std_logic_vector(RAM_CTRL_addr_size-1 downto 0);
    signal RAM_CTRL_portA_data:         std_logic_vector(RAM_CTRL_data_size-1 downto 0);
    signal RAM_CTRL_portA_byteena:      std_logic_vector(RAM_CTRL_byteena_size-1 downto 0);
    signal RAM_CTRL_portA_wren:         std_logic;
    signal RAM_CTRL_portA_q:            std_logic_vector(RAM_CTRL_data_size-1 downto 0);
    
    signal RAM_CTRL_portB_addr:         std_logic_vector(RAM_CTRL_addr_size-1 downto 0);
    signal RAM_CTRL_portB_data:         std_logic_vector(RAM_CTRL_data_size-1 downto 0);
    signal RAM_CTRL_portB_byteena:      std_logic_vector(RAM_CTRL_byteena_size-1 downto 0);
    signal RAM_CTRL_portB_wren:         std_logic;
    signal RAM_CTRL_portB_q:            std_logic_vector(RAM_CTRL_data_size-1 downto 0);

    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- proc SCMI
    signal proc_scmi_rx_tag:        std_logic;
    signal proc_scmi_tx_tag:        std_logic;
    signal proc_scmi_tx_data:       std_logic_vector(proc_data_size-1 downto 0);
    signal proc_rx_data:            std_logic_vector(proc_data_size-1 downto 0);
    signal proc_rx_vld:             std_logic;
    signal proc_rx_next:            std_logic;
    
    -- proc SCMI
    signal cmd_scmi_rx_tag:         std_logic;
    signal cmd_scmi_tx_tag:         std_logic;
    signal cmd_scmi_tx_data:        std_logic_vector(cmd_data_size-1 downto 0);
    signal cmd_tx_data:             std_logic_vector(cmd_data_size-1 downto 0);
    signal cmd_tx_vld:              std_logic;
    signal cmd_tx_next:             std_logic;
    
    -- VME_rdreq 
    signal VME_rdreq_tx_data:       std_logic_vector(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_tx_vld:        std_logic;
    signal VME_rdreq_tx_next:       std_logic;
    signal VME_rdreq_rx_data:       slv_array(VME_rdreq_rx_number-1 downto 0)(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_rx_vld:        std_logic_vector(VME_rdreq_rx_number-1 downto 0);
    signal VME_rdreq_rx_next:       std_logic_vector(VME_rdreq_rx_number-1 downto 0);

    -- VME_wrreq
    signal VME_wrreq_tx_data:       std_logic_vector(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_tx_vld:        std_logic;
    signal VME_wrreq_tx_next:       std_logic;
    signal VME_wrreq_rx_data:       slv_array(VME_wrreq_rx_number-1 downto 0)(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_rx_vld:        std_logic_vector(VME_wrreq_rx_number-1 downto 0);
    signal VME_wrreq_rx_next:       std_logic_vector(VME_wrreq_rx_number-1 downto 0);
    
    -- VME_rdout
    signal VME_rdout_tx_data:       slv_array(VME_rdout_tx_number-1 downto 0)(VME_rdout_data_size-1 downto 0);
    signal VME_rdout_tx_vld:        std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_tx_next:       std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_rx_data:       slv_array(VME_rdout_rx_number-1 downto 0)(VME_rdout_data_size-1 downto 0);
    signal VME_rdout_rx_vld:        std_logic_vector(VME_rdout_rx_number-1 downto 0);
    signal VME_rdout_rx_next:       std_logic_vector(VME_rdout_rx_number-1 downto 0);
 
begin
    
    --====================--
    -- PinOut Connections --
    --====================--
    
    -- PROC SCMI_RX
    proc_scmi_tx_data(63 downto 54) <= PIM_IO(64 downto 55);
    proc_scmi_tx_data(52 downto 0)  <= PIM_IO(53 downto 1);
    proc_scmi_tx_data(53)           <= PIM_SIG_EVEN(15);
    
    proc_scmi_tx_data(65 downto 64) <= PIM_SIG_EVEN(2 downto 1);
    proc_scmi_tx_tag                <= PIM_SIG_EVEN(3);
    PIM_SIG_ODD(24)                 <= proc_scmi_rx_tag;
    
    -- CMD SCMI_TX 
    PIM_SIG_ODD(18 downto 1)        <= cmd_scmi_tx_data;
    PIM_SIG_ODD(19)                 <= cmd_scmi_tx_tag;
    cmd_scmi_rx_tag                 <= PIM_SIG_EVEN(24);
    
    -- Interlock
    BIS_1                           <= PIM_SIG_EVEN(21);
    BIS_2                           <= PIM_SIG_EVEN(22);
    
    -- Timing
    PIM_SIG_ODD(21)                 <= BP_TIM;
    PIM_SIG_ODD(22)                 <= BEAM_IN_TIM;
    PIM_SIG_ODD(23)                 <= BEAM_OUT_TIM;
    
    
    --======--
    -- LEDS --
    --======--
    
    -- LEDs 1-4 (Status)
    FP_Configure_OK_LEDn            <= '0';
    FP_Orbit_Running_LEDn           <= '1';
    FP_PM_Running_LEDn              <= '0';
    FP_Capture_Running_LEDn         <= '1';
    -- LEDs 5-8 (Errors)
    FP_Calib_Mode_LEDn              <= '1';
    FP_TCLk_Det_LEDn                <= '0';
    FP_40MHz_Det_LEDn               <= '1';
    FP_PWR_OK_LEDn                  <= '0';
    -- VEM Access LED
    FP_VME_ACC_LEDn                 <= NOT VME_ACC;
    
    --===========--
    -- INTERLOCK --
    --===========--
    
    P0_BLM_DCin_Ctrl1               <= BIS_1;
    P0_BLM_DCin_Ctrl2               <= BIS_2;
    
    
    --=============--
    -- FLASH PORTS --
    --=============--
    
    FLASH_A             <= (others => 'Z');
    FLASH_CEn           <= 'Z';
    FLASH_OEn           <= 'Z';            
    FLASH_WEn           <= 'Z';
    Stratix_Flash_csN   <= '1';      
    
    --============--
    -- CLOCK PLLs --
    --============--
    
    vmepll: entity work.pll
    port map
    (
        inclk0  => STX_40MHZ_Osc,
        c0      => clk_main,
        locked  => OPEN
    );
    
    
    --==================--
    -- TIMING SYNC/EDGE --
    --==================--
    
    
    -- BP Timing Signal
    sync_BP: entity work.sync
    generic map
    (
        data_width              => 1,
        stages                  => sync_stages,
        init_value              => '0'
    )
    port map
    (
        dest_clk                => clk_main,
        async_data              => vectorize(P0_BLM_IN(1)),
        scalarize(sync_data)    => BP_long_pulse
    );
    
    edge_det_BP: entity work.edge_detect
    port map
    (
        clk         => clk_main,
        long_pulse  => BP_long_pulse,
        oneCC_pulse => BP_TIM
    );
    
    -- B-In Timing Signal
    sync_BIn: entity work.sync
    generic map
    (
        data_width              => 1,
        stages                  => sync_stages,
        init_value              => '0'
    )
    port map
    (
        dest_clk                => clk_main,
        async_data              => vectorize(P0_BLM_IN(2)),
        scalarize(sync_data)    => BEAM_IN_long_pulse
    );
    
    edge_det_BIn: entity work.edge_detect
    port map
    (
        clk         => clk_main,
        long_pulse  => BEAM_IN_long_pulse,
        oneCC_pulse => BEAM_IN_TIM
    );
    
    -- B-Out Timing Signal
    sync_BOut: entity work.sync
    generic map
    (
        data_width              => 1,
        stages                  => sync_stages,
        init_value              => '0'
    )
    port map
    (
        dest_clk                => clk_main,
        async_data              => vectorize(P0_BLM_IN(3)),
        scalarize(sync_data)    => BEAM_OUT_long_pulse
    );
    
    edge_det_BOut: entity work.edge_detect
    port map
    (
        clk         => clk_main,
        long_pulse  => BEAM_OUT_long_pulse,
        oneCC_pulse => BEAM_OUT_TIM
    );
    
    --===================--
    -- TIMING CONTROLLER --
    --===================--
    
    DAB64x_TimingControl_inst: entity work.DAB64x_TimingControl
	GENERIC MAP
    (
        cnt_size            => timestamp_size,
        T_CLK               => T_CLK,
        T_INP_BLOCK_START   => T_INP_BLOCK_START,
        T_INP_BLOCK_STOP    => T_INP_BLOCK_STOP,
        T_SB_PBL            => T_SB_PBL
	)
    PORT MAP
    (
        clk                 => clk_main,
        BP_pulse_sync       => BP_TIM,
        BP_pulse            => BP_pulse,
        INP_BLOCK           => INP_BLOCK,
        FINAL_PBL_pulse     => FINAL_PBL_pulse,
        BP_CNT              => BP_CNT
	);
    
    --=======================--
    -- Parallel Connector RX --
    --=======================--
    
     -- proc CMI --
    proc_SCMI_RX: entity work.S_CMI_RX
    GENERIC MAP
    (
        data_size       => proc_data_size
    )
    PORT MAP
    (
        rx_clk          => clk_main,
        scmi_tx_data    => proc_scmi_tx_data,
        scmi_tx_tag     => proc_scmi_tx_tag,
        scmi_rx_tag     => proc_scmi_rx_tag,
        rx_data         => proc_rx_data,
        rx_vld          => proc_rx_vld,
        rx_next         => proc_rx_next
    );
    
    
    
    --=================--
    -- Downstream Core --
    --=================--
    
    DownstreamCore_CMA_inst: entity work.DAB64x_DownstreamCore_CMA
    generic map
    (
        pkg_data_size           => proc_data_size,
        VME_rdreq_data_size     => VME_rdreq_data_size,
        VME_wrreq_data_size     => VME_wrreq_data_size,
        VME_rdout_data_size     => VME_rdout_data_size,
        VME_wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        VME_wrreq_d_MSB         => VME_wrreq_d_MSB
        
    )
    port map
    (   
        clk                 => clk_main,
        FINAL_PBL_pulse     => FINAL_PBL_pulse,
        INP_BLOCK            => INP_BLOCK,
        pkg_data            => proc_rx_data,
        pkg_vld             => proc_rx_vld,
        pkg_next            => proc_rx_next,
        VME_rdreq_data      => VME_rdreq_rx_data(DS_rams-1 downto 0),
        VME_rdreq_vld       => VME_rdreq_rx_vld(DS_rams-1 downto 0),
        VME_rdreq_next      => VME_rdreq_rx_next(DS_rams-1 downto 0),
        VME_wrreq_data      => VME_wrreq_rx_data(DS_rams-1 downto 0),
        VME_wrreq_vld       => VME_wrreq_rx_vld(DS_rams-1 downto 0),
        VME_wrreq_next      => VME_wrreq_rx_next(DS_rams-1 downto 0),
        VME_rdout_data      => VME_rdout_tx_data(DS_rams-1 downto 0),
        VME_rdout_vld       => VME_rdout_tx_vld(DS_rams-1 downto 0),
        VME_rdout_next      => VME_rdout_tx_next(DS_rams-1 downto 0)
    );
    
    
    --===============--
    -- Upstream Core --
    --===============--
    
    
    -- PARAM STRUCTURES --
    
    
    -- PARAM RAM --
    PARAM_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_PARAM_numwords,
        portA_data_size     => RAM_PARAM_data_size,
        portA_addr_size     => RAM_PARAM_addr_size,
        portA_byteena_size  => RAM_PARAM_byteena_size,
        portB_numwords      => RAM_PARAM_numwords,
        portB_data_size     => RAM_PARAM_data_size,
        portB_addr_size     => RAM_PARAM_addr_size,
        portB_byteena_size  => RAM_PARAM_byteena_size,
        init_file           => RAM_PARAM_init_file,
        ram_type            => RAM_PARAM_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk_main,
        portA_addr      => RAM_PARAM_portA_addr,
        portA_data      => RAM_PARAM_portA_data,
        portA_byteena   => RAM_PARAM_portA_byteena,
        portA_wren      => RAM_PARAM_portA_wren,
        portA_q         => RAM_PARAM_portA_q,
        portB_addr      => (others => '0'),
        portB_data      => (others => '0'),
        portB_byteena   => (others => '0'),
        portB_wren      => '0',
        portB_q         => open
    );
    
    -- PARAM RAM Access Port A --
    PARAM_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_PARAM_addr_MSB,
        RAM_addr_size       => RAM_PARAM_addr_size,
        RAM_data_size       => RAM_PARAM_data_size,
        RAM_byteena_size    => RAM_PARAM_byteena_size

    )
    PORT MAP
    (
        clk             => clk_main,
        rdreq_data      => VME_rdreq_rx_data(4),
        rdreq_vld       => VME_rdreq_rx_vld(4),
        rdreq_next      => VME_rdreq_rx_next(4),
        wrreq_data      => VME_wrreq_rx_data(4),
        wrreq_vld       => VME_wrreq_rx_vld(4),
        wrreq_next      => VME_wrreq_rx_next(4),
        rdout_data      => VME_rdout_tx_data(4),
        rdout_vld       => VME_rdout_tx_vld(4),
        rdout_next      => VME_rdout_tx_next(4),
        RAM_addr        => RAM_PARAM_portA_addr,
        RAM_data        => RAM_PARAM_portA_data,
        RAM_byteena     => RAM_PARAM_portA_byteena,
        RAM_wren        => RAM_PARAM_portA_wren,
        RAM_q           => RAM_PARAM_portA_q
    );
    
    
    -- PROG STRUCTURES --
    
    -- PROG RAM --
    PROG_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_PROG_numwords,
        portA_data_size     => RAM_PROG_data_size,
        portA_addr_size     => RAM_PROG_addr_size,
        portA_byteena_size  => RAM_PROG_byteena_size,
        portB_numwords      => RAM_PROG_numwords,
        portB_data_size     => RAM_PROG_data_size,
        portB_addr_size     => RAM_PROG_addr_size,
        portB_byteena_size  => RAM_PROG_byteena_size,
        init_file           => RAM_PROG_init_file,
        ram_type            => RAM_PROG_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk_main,
        portA_addr      => RAM_PROG_portA_addr,
        portA_data      => RAM_PROG_portA_data,
        portA_byteena   => RAM_PROG_portA_byteena,
        portA_wren      => RAM_PROG_portA_wren,
        portA_q         => RAM_PROG_portA_q,
        portB_addr      => (others => '0'),
        portB_data      => (others => '0'),
        portB_byteena   => (others => '0'),
        portB_wren      => '0',
        portB_q         => open
    );
    
    -- PROG RAM Access Port B --
    PROG_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_PROG_addr_MSB,
        RAM_addr_size       => RAM_PROG_addr_size,
        RAM_data_size       => RAM_PROG_data_size,
        RAM_byteena_size    => RAM_PROG_byteena_size

    )
    PORT MAP
    (
        clk             => clk_main,
        rdreq_data      => VME_rdreq_rx_data(5),
        rdreq_vld       => VME_rdreq_rx_vld(5),
        rdreq_next      => VME_rdreq_rx_next(5),
        wrreq_data      => VME_wrreq_rx_data(5),
        wrreq_vld       => VME_wrreq_rx_vld(5),
        wrreq_next      => VME_wrreq_rx_next(5),
        rdout_data      => VME_rdout_tx_data(5),
        rdout_vld       => VME_rdout_tx_vld(5),
        rdout_next      => VME_rdout_tx_next(5),
        RAM_addr        => RAM_PROG_portA_addr,
        RAM_data        => RAM_PROG_portA_data,
        RAM_byteena     => RAM_PROG_portA_byteena,
        RAM_wren        => RAM_PROG_portA_wren,
        RAM_q           => RAM_PROG_portA_q
    );

    

    -- CTRL STRUCTURES --
    
    -- CTRL RAM --
    CTRL_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_CTRL_numwords,
        portA_data_size     => RAM_CTRL_data_size,
        portA_addr_size     => RAM_CTRL_addr_size,
        portA_byteena_size  => RAM_CTRL_byteena_size,
        portB_numwords      => RAM_CTRL_numwords,
        portB_data_size     => RAM_CTRL_data_size,
        portB_addr_size     => RAM_CTRL_addr_size,
        portB_byteena_size  => RAM_CTRL_byteena_size,
        init_file           => RAM_CTRL_init_file,
        ram_type            => RAM_CTRL_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk_main,
        portA_addr      => RAM_CTRL_portA_addr,
        portA_data      => RAM_CTRL_portA_data,
        portA_byteena   => RAM_CTRL_portA_byteena,
        portA_wren      => RAM_CTRL_portA_wren,
        portA_q         => RAM_CTRL_portA_q,
        portB_addr      => (others => '0'),
        portB_data      => (others => '0'),
        portB_byteena   => (others => '0'),
        portB_wren      => '0',
        portB_q         => open
    );
    
    -- CTRL RAM Access Port A --
    CTRL_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_CTRL_addr_MSB,
        RAM_addr_size       => RAM_CTRL_addr_size,
        RAM_data_size       => RAM_CTRL_data_size,
        RAM_byteena_size    => RAM_CTRL_byteena_size

    )
    PORT MAP
    (
        clk             => clk_main,
        rdreq_data      => VME_rdreq_rx_data(6),
        rdreq_vld       => VME_rdreq_rx_vld(6),
        rdreq_next      => VME_rdreq_rx_next(6),
        wrreq_data      => VME_wrreq_rx_data(6),
        wrreq_vld       => VME_wrreq_rx_vld(6),
        wrreq_next      => VME_wrreq_rx_next(6),
        rdout_data      => VME_rdout_tx_data(6),
        rdout_vld       => VME_rdout_tx_vld(6),
        rdout_next      => VME_rdout_tx_next(6),
        RAM_addr        => RAM_CTRL_portA_addr,
        RAM_data        => RAM_CTRL_portA_data,
        RAM_byteena     => RAM_CTRL_portA_byteena,
        RAM_wren        => RAM_CTRL_portA_wren,
        RAM_q           => RAM_CTRL_portA_q
    );
    
    
    
    --==================--
    -- VME Distribution --
    --==================--
    
    VME_Distribution_CMI_inst: entity work.DAB64x_VME_Distribution_CMI
    generic map
    (
        VME_rdreq_data_size     => VME_rdreq_data_size,
        VME_wrreq_data_size     => VME_wrreq_data_size,
        VME_rdout_data_size     => VME_rdout_data_size,
        num_rams                => all_rams
        
    )
    port map
    (   
        clk                     => clk_main,
        VME_rdreq_tx_data       => VME_rdreq_tx_data,
        VME_rdreq_tx_vld        => VME_rdreq_tx_vld,
        VME_rdreq_tx_next       => VME_rdreq_tx_next,
        VME_rdreq_rx_data       => VME_rdreq_rx_data,
        VME_rdreq_rx_vld        => VME_rdreq_rx_vld,
        VME_rdreq_rx_next       => VME_rdreq_rx_next,
        VME_wrreq_tx_data       => VME_wrreq_tx_data,
        VME_wrreq_tx_vld        => VME_wrreq_tx_vld,
        VME_wrreq_tx_next       => VME_wrreq_tx_next,
        VME_wrreq_rx_data       => VME_wrreq_rx_data,
        VME_wrreq_rx_vld        => VME_wrreq_rx_vld,
        VME_wrreq_rx_next       => VME_wrreq_rx_next,
        VME_rdout_tx_data       => VME_rdout_tx_data,
        VME_rdout_tx_vld        => VME_rdout_tx_vld,
        VME_rdout_tx_next       => VME_rdout_tx_next,
        VME_rdout_rx_data       => VME_rdout_rx_data,
        VME_rdout_rx_vld        => VME_rdout_rx_vld,
        VME_rdout_rx_next       => VME_rdout_rx_next
    );
    
    
    
    --==========--
    -- VME Core --
    --==========--
    
    vmecore: entity work.vme_core_CMA
    generic map
    (
        Tclk_ns             => Tclk_ns,
        wrreq_data_size     => vme_wrreq_data_size,
        rdreq_data_size     => vme_rdreq_data_size,
        rdout_data_size     => vme_rdout_data_size
    )
    port map
    (   
        clk                 => clk_main,
        VME_GA              => VME_GA,
        VME_GAP             => VME_GAP,
        VME_WRn             => VME_WRn,
        VME_AM              => VME_AM,
        VME_ASn             => VME_ASn,
        VME_DS0n            => VME_DS0n,
        VME_DS1n            => VME_DS1n,
        VME_IACKn           => VME_IACKn,
        VME_IACKINn         => VME_IACKINn,
        VME_SYSCLK          => VME_SYSCLK,
        VME_SYSRESETn       => VME_SYSRESETn,
        VME_ADDR_IN         => VME_ADDR_IN,
        VME_LWORDn_IN       => VME_LWORDn_IN,
        VME_DATA_IN         => VME_DATA_IN,
        VME_ADDR_OUT        => VME_ADDR_OUT,
        VME_LWORDn_OUT      => VME_LWORDn_OUT,
        VME_DATA_OUT        => VME_DATA_OUT,
        VME_DTACKn          => VME_DTACKn,
        VME_BERRn           => VME_BERRn,
        VME_RETRYn          => VME_RETRYn,
        VME_IACKOUTn        => VME_IACKOUTn,
        VME_IRQn(6 downto 1)=> VME_IRQn,
        VME_SYSFAILn        => VME_SYSFAILn,
        VME_ACC             => VME_ACC,
        VME_BUS_BUSY        => VME_BUS_BUSY,
        VME_DATA_BUF_OEn    => VME_DATA_BUF_OEn,
        VME_ADDR_BUF_OEn    => VME_ADDR_BUF_OEn,  
        VME_DTACK_BUF_OEn   => VME_DTACK_BUF_OEn,  
        VME_RETRY_BUF_OEn   => VME_RETRY_BUF_OEn,  
        VME_BERR_BUF_OEn    => VME_BERR_BUF_OEn,   
        VME_DATA_BUF_DIR    => VME_DATA_BUF_DIR,   
        VME_ADDR_BUF_DIR    => VME_ADDR_BUF_DIR,   
        rdreq_data          => VME_rdreq_tx_data,
        rdreq_vld           => VME_rdreq_tx_vld,
        rdreq_next          => VME_rdreq_tx_next,
        wrreq_data          => VME_wrreq_tx_data,
        wrreq_vld           => VME_wrreq_tx_vld,
        wrreq_next          => VME_wrreq_tx_next,
        rdout_data          => VME_rdout_rx_data(0),
        rdout_vld           => VME_rdout_rx_vld(0),
        rdout_next          => VME_rdout_rx_next(0)
    );
    
    --===========================--
    -- DAB64x VME BUS CONTROLLER --
    --===========================--
    
    VME_BUS_Ctrl : entity work.DAB64x_VME_BusCtrl
    port map
    (   
        clk                 => clk_main,
        Stratix_IRQ_VECTORn => Stratix_IRQ_VECTORn,
        VME_IRQ_Level       => VME_IRQ_Level,
        IRQ_Vector_Enable   => IRQ_Vector_Enable,
        Board_Rst_ONOFFn    => Board_Rst_ONOFFn,
        Stratix_VME_ACCn    => Stratix_VME_ACCn,
        VME_ForceRetry      => VME_ForceRetry,
        VME_2e_Cycle        => VME_2e_Cycle,
        Stratix_245_OEn     => Stratix_245_OEn,
        Stratix_dtackN      => Stratix_dtackN,
        Stratix_SYSFAILn    => Stratix_SYSFAILn,
        VME_ADDR            => VME_ADD,
        VME_LWORDn          => VME_LWORDn,
        VME_DATA            => VME_DATA,
        VME_DTACK_EN        => VME_DTACK_EN,
        VME_ADDR_OUT        => VME_ADDR_OUT,
        VME_LWORDn_OUT      => VME_LWORDn_OUT,
        VME_DATA_OUT        => VME_DATA_OUT,
        VME_DATA_BUF_OEn    => VME_DATA_BUF_OEn,
        VME_ADDR_BUF_OEn    => VME_ADDR_BUF_OEn,  
        VME_DTACK_BUF_OEn   => VME_DTACK_BUF_OEn,  
        VME_RETRY_BUF_OEn   => VME_RETRY_BUF_OEn,  
        VME_BERR_BUF_OEn    => VME_BERR_BUF_OEn,   
        VME_DATA_BUF_DIR    => VME_DATA_BUF_DIR,   
        VME_ADDR_BUF_DIR    => VME_ADDR_BUF_DIR,   
        VME_ACC             => VME_ACC,
        VME_DTACKn          => VME_DTACKn,
        VME_SYSFAILn        => VME_SYSFAILn,
        VME_ADDR_IN         => VME_ADDR_IN,
        VME_LWORDn_IN       => VME_LWORDn_IN,
        VME_DATA_IN         => VME_DATA_IN,
        VME_BUS_BUSY        => VME_BUS_BUSY,
        FLASH_DATA_IN       => open, --FLASH_DATA_IN,
        FLASH_DATA_OUT      => "00000000", --FLASH_DATA_OUT,
        FLASH_BUS_REQ       => '0', -- FLASH_BUS_REQ,
        FLASH_BUS_ACK       => open, --FLASH_BUS_ACK,
        FLASH_DATA_BUS_DIR  => '0'
    );
    
   
    
    
end architecture struct;