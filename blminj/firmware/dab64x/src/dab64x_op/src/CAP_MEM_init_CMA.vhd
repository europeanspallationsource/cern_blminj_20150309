------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Create Date:    01/03/2014
Module Name:    CAP_MEM_init_CMA

-----------------
Short Description
-----------------

    
------------
Dependencies
------------

    vhdl_func_pkg
    
------------------
Generics/Constants
------------------

--------------
Implementation
--------------

-----------
Limitations
-----------
    
    

----------------
Missing Features
----------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity CAP_MEM_init_CMA is

port
(
    -- Clock
    clk:                in  std_logic;
    -- CMI Output
    out_data:           out std_logic_vector(127 downto 0);
    out_vld:            out std_logic := '0';
    out_next:           in  std_logic
);

end entity CAP_MEM_init_CMA;


architecture struct of CAP_MEM_init_CMA is
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant channel_num:       integer := 8;
    constant cnt_max:           integer := 512;
    
    constant data_size:         integer := 64;
    constant addr_size:         integer := 64;
    constant chan_cnt_vsize:    integer := 3;
    
    --======--
    -- FSMs --
    --======--
    
    type state_t is (idle, init_all, finish);
    signal present_state, next_state: state_t := idle;
    
    --==========--
    -- COUNTERS --
    --==========--
    
    -- Data Content Counter
    signal data_cnt:    UNSIGNED(data_size-1 downto 0) := (others => '0');
    signal data_rst:    std_logic;
    signal data_inc:    std_logic;
    
    -- Data Content Counter
    signal addr_cnt:    UNSIGNED(addr_size-1 downto 0) := (others => '0');
    signal addr_rst:    std_logic;
    signal addr_inc:    std_logic;
    
    -- Data Content Counter
    signal chan_cnt:    UNSIGNED(chan_cnt_vsize-1 downto 0) := (others => '0');
    signal chan_rst:    std_logic;
    signal chan_inc:    std_logic;
    
    --===========--
    -- REGISTERS --
    --===========--
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal data_int:        UNSIGNED(data_size-1 downto 0);
    signal addr_int:        UNSIGNED(addr_size-1 downto 0);
    signal out_vld_int:     std_logic;
    
BEGIN

    --==========--
    -- COUNTERS --
    --==========--
    
    process(clk) is
    begin
        if rising_edge(clk) then
        
            if data_rst= '1' then
                data_cnt <= (others => '0');
            elsif data_inc = '1' then
                data_cnt <= data_cnt + 1;
            end if;
            
            if addr_rst= '1' then
                addr_cnt <= (others => '0');
            elsif addr_inc = '1' then
                addr_cnt <= addr_cnt + 1;
            end if;
            
            if chan_rst= '1' then
                chan_cnt <= (others => '0');
            elsif chan_inc = '1' then
                chan_cnt <= chan_cnt + 1;
            end if;
            
        end if;
    end process;
    
    
    --===============--
    -- COMBINATORICS --
    --===============--

    -- Data
    process(all) is
    begin
        case chan_cnt is
            when "000" => data_int <= data_cnt + 64UX"10";
            when "001" => data_int <= data_cnt + 64UX"100";
            when "010" => data_int <= data_cnt + 64UX"1000";
            when "011" => data_int <= data_cnt + 64UX"10000";
            when "100" => data_int <= data_cnt + 64UX"100000";
            when "101" => data_int <= data_cnt + 64UX"1000000";
            when "110" => data_int <= data_cnt + 64UX"10000000";
            when "111" => data_int <= data_cnt + 64UX"100000000";
            when others => data_int <= data_cnt;
        end case;
    end process;
    
    -- Addr EVO
    process(all) is
    begin
        case chan_cnt is
            when "000" => addr_int <= addr_cnt;
            when "001" => addr_int <= addr_cnt + 64UX"0200";
            when "010" => addr_int <= addr_cnt + 64UX"0400";
            when "011" => addr_int <= addr_cnt + 64UX"0600";
            when "100" => addr_int <= addr_cnt + 64UX"0800";
            when "101" => addr_int <= addr_cnt + 64UX"0A00";
            when "110" => addr_int <= addr_cnt + 64UX"0C00";
            when "111" => addr_int <= addr_cnt + 64UX"0E00";
            when others => addr_int <= addr_cnt;
        end case;
    end process;
    
    --============--
    -- CMI OUTREG --
    --============--
    
    process (clk) is
    begin
        if rising_edge(clk) then
                
            if out_next = '1' then
                out_vld     <= out_vld_int;
                out_data    <= STD_LOGIC_VECTOR(data_int) & STD_LOGIC_VECTOR(addr_int);
            end if;
                    
        end if;
    end process;
    
    
    
    --=========--
    -- CONTROL --
    --=========--
    
    process (clk) is
    begin
        if rising_edge(clk) then
            present_state <= next_state;
        end if;
    end process;
    
    process (all) is
    begin
        -- DEFAULTS
        -- CMI
        out_vld_int <= '0';
        -- Counters
        addr_inc    <= '0';
        addr_rst    <= '0';
        chan_inc    <= '0';
        chan_rst    <= '0';
        data_inc    <= '0';
        data_rst    <= '0';

        
        case present_state is
            
            --===============--
            when idle =>
            --===============--
                
                if out_next = '1' then
                    next_state <= init_all;
                else
                    next_state <= idle;
                end if;

            
            --===============--
            when init_all =>
            --===============--

                next_state <= init_all;
                if out_next = '1' then
                    out_vld_int <= '1';
                    if addr_cnt = cnt_max-1 then
                        addr_rst <= '1';
                        data_rst <= '1';
                        if chan_cnt = channel_num-1 then
                            chan_rst    <= '1';
                            next_state  <= finish;
                        else
                            chan_inc    <= '1';
                        end if;
                    else
                        addr_inc <= '1';
                        data_inc <= '1';
                    end if;
                end if;
            
            --===============--
            when finish =>
            --===============--
            
                next_state <= finish;
                
        end case;
    end process;
  
    
    
end architecture struct;
