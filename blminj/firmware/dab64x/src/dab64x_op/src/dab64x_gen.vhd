------------------------------
/*
Company:        CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2013
Module Name:    dab64x_gen


-----------
Description
-----------

    This is the topmodule for the DAB64 VME Core including RAMs with generated/set MIFs.


------------------
Generics/Constants
------------------

    Tclk_ns             := clock in ns
    
    num_rams            := number of attached storage RAMs
    addr_size           := general address size
    be_size             := size of the write byte enable part
    ram_data_size       := size of the data inside the RAMs
    wrreq_data_size     := size of the data part of the wrreq CMI
    rdreq_data_size     := size of the data part of the rdreq CMI
    vme_wr_size         := data size of the write FIFO/CMIs
    vme_rd_size         := data size of the read FIFO/CMI
    
----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

    

---------------
Necessary Cores
---------------

    pll125 
    
--------------
Implementation
--------------
    
------------------------
Not implemented Features
------------------------


*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

entity dab64x_gen is
port
(
    -- external clock
    STX_40MHZ_Osc:          in  std_logic;
    -- VME Inputs
    VME_GA:                 in  std_logic_vector(4 downto 0);
    VME_GAP:                in  std_logic;
    VME_WRn:                in  std_logic;
    VME_AM:                 in  std_logic_vector(5 downto 0);
    VME_ASn:                in  std_logic;
    VME_DS0n:               in  std_logic;
    VME_DS1n:               in  std_logic;
    VME_IACKn:              in  std_logic;
    VME_IACKINn:            in  std_logic;
    VME_SYSCLK:             in  std_logic;
    VME_SYSRESETn:          in  std_logic;
    -- VME IOs
    VME_ADD:                inout std_logic_vector(31 downto 1);
    VME_LWORDn:             inout std_logic;
    VME_DATA:               inout std_logic_vector(31 downto 0);
    -- VME Outputs
    VME_BERRn:              out std_logic;
    VME_RETRYn:             out std_logic;
    VME_IACKOUTn:           out std_logic;
    VME_IRQn:               out std_logic_vector(6 downto 1);
    -- DAB64 IOs
    Stratix_IRQ_VECTORn:    out std_logic;
    VME_IRQ_Level:          in  std_logic_vector(2 downto 0);
    IRQ_Vector_Enable:      in  std_logic;
    FP_Configure_OK_LEDn:   out std_logic;
    Board_Rst_ONOFFn:       in  std_logic;
    Stratix_VME_ACCn:       out std_logic;
    VME_ForceRetry:         out std_logic;  -- activate with RETRY
    VME_2e_Cycle:           out std_logic;  -- use for A[31:1] & LWORDn direction
    VME_DTACK_EN:           inout std_logic;  -- for releasing the DTACK line
    Stratix_245_OEn:        out std_logic;  -- OE for data lines D[31:0]
    Stratix_dtackN:         out std_logic;  -- VME_DTACKn
    Stratix_SYSFAILn:       out std_logic;   -- VME_SYSFAILn
    
    -- LEDs
    FP_VME_ACC_LEDn:        out std_logic;

    
    -- Flash (as long as there is no DAB64x complete FW)
    FLASH_A:                inout std_logic_vector(22 downto 0);
    FLASH_CEn:              inout std_logic;
    FLASH_OEn:              inout std_logic;
    FLASH_WEn:              inout std_logic;
    Stratix_Flash_csN:      out std_logic
);
    
end entity dab64x_gen;



architecture struct of dab64x_gen is
    
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant Tclk_ns:               real := 12.5; -- 80 MHz
    constant timer_intv:            integer := 1000; -- ms
    
    constant VME_core_data_size:    integer := 64;
    constant VME_core_addr_size:    integer := 64;
    constant vme_wrreq_data_size:   integer := 133;
    constant vme_rdreq_data_size:   integer := 61;
    constant vme_rdout_data_size:   integer := 64;
    constant VME_wrreq_addr_MSB:    integer := 132;
    constant VME_wrreq_byteena_MSB: integer := 71;
    constant VME_wrreq_d_MSB:       integer := 63;
    

     -- General
    constant header_size:               integer := 64;
    constant wr_addr_size:              integer := 32;
    constant PARAM_update:              std_logic := '1';
    
    -- RAMs
    constant num_rams:                  integer := 7;
    constant num_wr_rams:               integer := 4;
    constant RAM_data_size:             integer := 64;
    constant RAM_byteena_size:          integer := 8;
    
    constant RAM_EVO_numwords:          integer := 10496;
    constant RAM_EVO_addr_size:         integer := get_vsize_addr(RAM_EVO_numwords);
    constant RAM_EVO_data_size:         integer := RAM_data_size;
    constant RAM_EVO_byteena_size:      integer := RAM_byteena_size;
    constant RAM_EVO_init_file:         string  := "UNUSED";
    constant RAM_EVO_type:              string  := "M-RAM";
    
    constant RAM_CAP_numwords:          integer := 4352;
    constant RAM_CAP_addr_size:         integer := get_vsize_addr(RAM_CAP_numwords);
    constant RAM_CAP_data_size:         integer := RAM_data_size;
    constant RAM_CAP_byteena_size:      integer := RAM_byteena_size;
    constant RAM_CAP_init_file:         string  := "UNUSED";
    constant RAM_CAP_type:              string  := "M-RAM";
    
    constant RAM_LOSS_numwords:         integer := 512;
    constant RAM_LOSS_addr_size:        integer := get_vsize_addr(RAM_LOSS_numwords);
    constant RAM_LOSS_data_size:        integer := RAM_data_size;
    constant RAM_LOSS_byteena_size:     integer := RAM_byteena_size;
    constant RAM_LOSS_init_file:        string  := "LOSS.mif";
    constant RAM_LOSS_type:             string  := "M4K";
    
    constant RAM_DIAG_numwords:         integer := 512;
    constant RAM_DIAG_addr_size:        integer := get_vsize_addr(RAM_DIAG_numwords);
    constant RAM_DIAG_data_size:        integer := RAM_data_size;
    constant RAM_DIAG_byteena_size:     integer := RAM_byteena_size;
    constant RAM_DIAG_init_file:        string  := "DIAG.mif";
    constant RAM_DIAG_type:             string  := "M4K";
    
    constant RAM_PARAM_numwords:        integer := 512;
    constant RAM_PARAM_addr_size:       integer := get_vsize_addr(RAM_PARAM_numwords);
    constant RAM_PARAM_data_size:       integer := RAM_data_size;
    constant RAM_PARAM_byteena_size:    integer := RAM_byteena_size;
    constant RAM_PARAM_init_file:       string  := "PARAM.mif";
    constant RAM_PARAM_type:            string  := "M4K";
    
    constant RAM_PROG_numwords:         integer := 1024;
    constant RAM_PROG_addr_size:        integer := get_vsize_addr(RAM_PROG_numwords);
    constant RAM_PROG_data_size:        integer := RAM_data_size;
    constant RAM_PROG_byteena_size:     integer := RAM_byteena_size;
    constant RAM_PROG_init_file:        string  := "PROG.mif";
    constant RAM_PROG_type:             string  := "M4K";
    
    constant RAM_CTRL_numwords:         integer := 256;
    constant RAM_CTRL_addr_size:        integer := get_vsize_addr(RAM_CTRL_numwords);
    constant RAM_CTRL_data_size:        integer := RAM_data_size;
    constant RAM_CTRL_byteena_size:     integer := RAM_byteena_size;
    constant RAM_CTRL_init_file:        string  := "CTRL.mif";
    constant RAM_CTRL_type:             string  := "M4K";
    
    
     -- VME wrreq
    constant VME_wrreq_rx_number:       integer := num_rams;    
    constant VME_wrreq_addr_size:       integer := 61;
    
    -- VME rdreq
    constant VME_rdreq_rx_number:       integer := num_rams;
    constant VME_rdreq_addr_size:       integer := 61;
    
    -- VME rdout
    constant VME_rdout_tx_number:       integer := num_rams;
    constant VME_rdout_rx_number:       integer := 1;
    
    
    -- MEM IniTs
    constant RAM_EVO_wrreq_data_size:   integer := 128;
    constant RAM_EVO_wrreq_data_MSB:    integer := 127;
    constant RAM_EVO_wrreq_addr_MSB:    integer := RAM_EVO_addr_size-1;
    
    constant RAM_CAP_wrreq_data_size:   integer := 128;
    constant RAM_CAP_wrreq_data_MSB:    integer := 127;
    constant RAM_CAP_wrreq_addr_MSB:    integer := RAM_CAP_addr_size-1;
    
    constant RAM_LOSS_wrreq_data_size:  integer := 128;
    constant RAM_LOSS_wrreq_data_MSB:   integer := 127;
    constant RAM_LOSS_wrreq_addr_MSB:   integer := RAM_LOSS_addr_size-1;
    
    
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- local clock
    signal clk_80:                  std_logic;
    
    -- between VME_Interface and DAB64x VME Bus Ctrl
    signal VME_ACC:                 std_logic;
    signal VME_BUS_BUSY:            std_logic;
    signal VME_DATA_BUF_OEn:        std_logic;
    signal VME_ADDR_BUF_OEn:        std_logic;
    signal VME_DTACK_BUF_OEn:       std_logic;
    signal VME_RETRY_BUF_OEn:       std_logic;
    signal VME_BERR_BUF_OEn:        std_logic;
    signal VME_DATA_BUF_DIR:        std_logic;
    signal VME_ADDR_BUF_DIR:        std_logic;
    signal VME_DTACKn:              std_logic;
    signal VME_SYSFAILn:            std_logic;
    
    signal VME_LWORDn_IN:           std_logic;
    signal VME_DATA_IN:             std_logic_vector(31 downto 0);
    signal VME_ADDR_IN:             std_logic_vector(31 downto 1);
    signal VME_LWORDn_OUT:          std_logic;
    signal VME_DATA_OUT:            std_logic_vector(31 downto 0);
    signal VME_ADDR_OUT:            std_logic_vector(31 downto 1);
    
    -- FLASH Interface to DAB64x VME Bus Ctrl
    signal FLASH_DATA_IN:           std_logic_vector(7 downto 0);       
    signal FLASH_DATA_OUT:          std_logic_vector(7 downto 0);
    signal FLASH_BUS_REQ:           std_logic;
    signal FLASH_BUS_ACK:           std_logic;
    
    -- RAM Connections
    signal RAM_EVO_portA_addr:          std_logic_vector(RAM_EVO_addr_size-1 downto 0);
    signal RAM_EVO_portA_data:          std_logic_vector(RAM_EVO_data_size-1 downto 0);
    signal RAM_EVO_portA_byteena:       std_logic_vector(RAM_EVO_byteena_size-1 downto 0);
    signal RAM_EVO_portA_wren:          std_logic;
    signal RAM_EVO_portA_q:             std_logic_vector(RAM_EVO_data_size-1 downto 0);
    
    signal RAM_EVO_portB_addr:          std_logic_vector(RAM_EVO_addr_size-1 downto 0);
    signal RAM_EVO_portB_data:          std_logic_vector(RAM_EVO_data_size-1 downto 0);
    signal RAM_EVO_portB_byteena:       std_logic_vector(RAM_EVO_byteena_size-1 downto 0);
    signal RAM_EVO_portB_wren:          std_logic;
    signal RAM_EVO_portB_q:             std_logic_vector(RAM_EVO_data_size-1 downto 0);
    
    signal RAM_CAP_portA_addr:          std_logic_vector(RAM_CAP_addr_size-1 downto 0);
    signal RAM_CAP_portA_data:          std_logic_vector(RAM_CAP_data_size-1 downto 0);
    signal RAM_CAP_portA_byteena:       std_logic_vector(RAM_CAP_byteena_size-1 downto 0);
    signal RAM_CAP_portA_wren:          std_logic;
    signal RAM_CAP_portA_q:             std_logic_vector(RAM_CAP_data_size-1 downto 0);
    
    signal RAM_CAP_portB_addr:          std_logic_vector(RAM_CAP_addr_size-1 downto 0);
    signal RAM_CAP_portB_data:          std_logic_vector(RAM_CAP_data_size-1 downto 0);
    signal RAM_CAP_portB_byteena:       std_logic_vector(RAM_CAP_byteena_size-1 downto 0);
    signal RAM_CAP_portB_wren:          std_logic;
    signal RAM_CAP_portB_q:             std_logic_vector(RAM_CAP_data_size-1 downto 0);
    
    signal RAM_LOSS_portA_addr:         std_logic_vector(RAM_LOSS_addr_size-1 downto 0);
    signal RAM_LOSS_portA_data:         std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    signal RAM_LOSS_portA_byteena:      std_logic_vector(RAM_LOSS_byteena_size-1 downto 0);
    signal RAM_LOSS_portA_wren:         std_logic;
    signal RAM_LOSS_portA_q:            std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    
    signal RAM_LOSS_portB_addr:         std_logic_vector(RAM_LOSS_addr_size-1 downto 0);
    signal RAM_LOSS_portB_data:         std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    signal RAM_LOSS_portB_byteena:      std_logic_vector(RAM_LOSS_byteena_size-1 downto 0);
    signal RAM_LOSS_portB_wren:         std_logic;
    signal RAM_LOSS_portB_q:            std_logic_vector(RAM_LOSS_data_size-1 downto 0);
    
    signal RAM_DIAG_portA_addr:         std_logic_vector(RAM_DIAG_addr_size-1 downto 0);
    signal RAM_DIAG_portA_data:         std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    signal RAM_DIAG_portA_byteena:      std_logic_vector(RAM_DIAG_byteena_size-1 downto 0);
    signal RAM_DIAG_portA_wren:         std_logic;
    signal RAM_DIAG_portA_q:            std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    
    signal RAM_DIAG_portB_addr:         std_logic_vector(RAM_DIAG_addr_size-1 downto 0);
    signal RAM_DIAG_portB_data:         std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    signal RAM_DIAG_portB_byteena:      std_logic_vector(RAM_DIAG_byteena_size-1 downto 0);
    signal RAM_DIAG_portB_wren:         std_logic;
    signal RAM_DIAG_portB_q:            std_logic_vector(RAM_DIAG_data_size-1 downto 0);
    
    signal RAM_PARAM_portA_addr:        std_logic_vector(RAM_PARAM_addr_size-1 downto 0);
    signal RAM_PARAM_portA_data:        std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    signal RAM_PARAM_portA_byteena:     std_logic_vector(RAM_PARAM_byteena_size-1 downto 0);
    signal RAM_PARAM_portA_wren:        std_logic;
    signal RAM_PARAM_portA_q:           std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    
    signal RAM_PARAM_portB_addr:        std_logic_vector(RAM_PARAM_addr_size-1 downto 0);
    signal RAM_PARAM_portB_data:        std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    signal RAM_PARAM_portB_byteena:     std_logic_vector(RAM_PARAM_byteena_size-1 downto 0);
    signal RAM_PARAM_portB_wren:        std_logic;
    signal RAM_PARAM_portB_q:           std_logic_vector(RAM_PARAM_data_size-1 downto 0);
    
    signal RAM_PROG_portA_addr:         std_logic_vector(RAM_PROG_addr_size-1 downto 0);
    signal RAM_PROG_portA_data:         std_logic_vector(RAM_PROG_data_size-1 downto 0);
    signal RAM_PROG_portA_byteena:      std_logic_vector(RAM_PROG_byteena_size-1 downto 0);
    signal RAM_PROG_portA_wren:         std_logic;
    signal RAM_PROG_portA_q:            std_logic_vector(RAM_PROG_data_size-1 downto 0);
    
    signal RAM_PROG_portB_addr:         std_logic_vector(RAM_PROG_addr_size-1 downto 0);
    signal RAM_PROG_portB_data:         std_logic_vector(RAM_PROG_data_size-1 downto 0);
    signal RAM_PROG_portB_byteena:      std_logic_vector(RAM_PROG_byteena_size-1 downto 0);
    signal RAM_PROG_portB_wren:         std_logic;
    signal RAM_PROG_portB_q:            std_logic_vector(RAM_PROG_data_size-1 downto 0);
    
    signal RAM_CTRL_portA_addr:         std_logic_vector(RAM_CTRL_addr_size-1 downto 0);
    signal RAM_CTRL_portA_data:         std_logic_vector(RAM_CTRL_data_size-1 downto 0);
    signal RAM_CTRL_portA_byteena:      std_logic_vector(RAM_CTRL_byteena_size-1 downto 0);
    signal RAM_CTRL_portA_wren:         std_logic;
    signal RAM_CTRL_portA_q:            std_logic_vector(RAM_CTRL_data_size-1 downto 0);
    
    signal RAM_CTRL_portB_addr:         std_logic_vector(RAM_CTRL_addr_size-1 downto 0);
    signal RAM_CTRL_portB_data:         std_logic_vector(RAM_CTRL_data_size-1 downto 0);
    signal RAM_CTRL_portB_byteena:      std_logic_vector(RAM_CTRL_byteena_size-1 downto 0);
    signal RAM_CTRL_portB_wren:         std_logic;
    signal RAM_CTRL_portB_q:            std_logic_vector(RAM_CTRL_data_size-1 downto 0);

    
    --=============--
    -- CMA SIGNALS --
    --=============--
 
    -- VME_rdreq 
    signal VME_rdreq_tx_data:           std_logic_vector(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_tx_vld:            std_logic;
    signal VME_rdreq_tx_next:           std_logic;
    signal VME_rdreq_rx_data:           slv_array(VME_rdreq_rx_number-1 downto 0)(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_rx_vld:            std_logic_vector(VME_rdreq_rx_number-1 downto 0);
    signal VME_rdreq_rx_next:           std_logic_vector(VME_rdreq_rx_number-1 downto 0);
    
    signal VME_rdreq_ADM_tx_data:       std_logic_vector(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_ADM_rx_data:       std_logic_vector(VME_rdreq_data_size-1 downto 0);
    signal VME_rdreq_ADM_sel:           std_logic_vector(get_vsize_addr(VME_rdreq_rx_number)-1 downto 0);
    
    -- VME_wrreq
    signal VME_wrreq_tx_data:           std_logic_vector(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_tx_vld:            std_logic;
    signal VME_wrreq_tx_next:           std_logic;
    signal VME_wrreq_rx_data:           slv_array(VME_wrreq_rx_number-1 downto 0)(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_rx_vld:            std_logic_vector(VME_wrreq_rx_number-1 downto 0);
    signal VME_wrreq_rx_next:           std_logic_vector(VME_wrreq_rx_number-1 downto 0);
    
    signal VME_wrreq_ADM_tx_data:       std_logic_vector(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_ADM_rx_data:       std_logic_vector(VME_wrreq_data_size-1 downto 0);
    signal VME_wrreq_ADM_sel:           std_logic_vector(get_vsize_addr(VME_wrreq_rx_number)-1 downto 0);
    
    -- VME_rdout
    signal VME_rdout_tx_data:           slv_array(VME_rdout_tx_number-1 downto 0)(VME_rdout_data_size-1 downto 0);
    signal VME_rdout_tx_vld:            std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_tx_next:           std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_rx_data:           slv_array(VME_rdout_rx_number-1 downto 0)(VME_rdout_data_size-1 downto 0);
    signal VME_rdout_rx_vld:            std_logic_vector(VME_rdout_rx_number-1 downto 0);
    signal VME_rdout_rx_next:           std_logic_vector(VME_rdout_rx_number-1 downto 0);
 
    signal VME_rdout_ARB_tx_vld:        std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_ARB_tx_next:       std_logic_vector(VME_rdout_tx_number-1 downto 0);
    signal VME_rdout_ARB_rx_vld:        std_logic;
    signal VME_rdout_ARB_rx_next:       std_logic;
    signal VME_rdout_ARB_sel:           std_logic_vector(get_vsize_addr(VME_rdout_tx_number)-1 downto 0);
    
    -- MEM INITs
    signal EVO_MEM_gen_data:            std_logic_vector(RAM_EVO_wrreq_data_size-1 downto 0);
    signal EVO_MEM_gen_vld:             std_logic;
    signal EVO_MEM_gen_next:            std_logic;
    
    signal CAP_MEM_gen_data:            std_logic_vector(RAM_CAP_wrreq_data_size-1 downto 0);
    signal CAP_MEM_gen_vld:             std_logic;
    signal CAP_MEM_gen_next:            std_logic;
    
    signal LOSS_MEM_gen_data:           std_logic_vector(RAM_LOSS_wrreq_data_size-1 downto 0);
    signal LOSS_MEM_gen_vld:            std_logic;
    signal LOSS_MEM_gen_next:           std_logic;
    
    
    signal timer_trigger:               std_logic;
    

begin

    --======--
    -- LEDS --
    --======--
    
    -- LEDs 1-4 (Status)
    FP_Configure_OK_LEDn            <= '0';
    -- VEM Access LED
    FP_VME_ACC_LEDn                 <= NOT VME_ACC;
    
    
    --=============--
    -- FLASH PORTS --
    --=============--
    
    FLASH_A             <= (others => 'Z');
    FLASH_CEn           <= 'Z';
    FLASH_OEn           <= 'Z';            
    FLASH_WEn           <= 'Z';
    Stratix_Flash_csN   <= '1';      
    
    
    --============--
    -- CLOCK PLLs --
    --============--
    
    vmepll: entity work.pll
    port map
    (
        inclk0  => STX_40MHZ_Osc,
        c0      => clk_80,
        locked  => OPEN
    );
    
    
    -- RAM Access Port B --
    timer_inst: entity work.timer
    GENERIC MAP
    (
        Tclk_ns     => Tclk_ns,
        upd_intv_ms => timer_intv

    )
    PORT MAP
    (
        clk         => clk_80,
        rst         => '0',
        upd         => timer_trigger
    );
    
    
    --================--
    -- EVO STRUCTURES --
    --================--
    
    -- EVO Gen --
    EVO_MEM_init_inst: entity work.EVO_MEM_init_CMA
    PORT MAP
    (
        clk         => clk_80,
        out_data    => EVO_MEM_gen_data,
        out_vld     => EVO_MEM_gen_vld,
        out_next    => EVO_MEM_gen_next
    );
    

    -- RAM Access Port B --
    EVO_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => RAM_EVO_wrreq_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => 0,
        wrreq_data_MSB      => RAM_EVO_wrreq_data_MSB,
        wrreq_addr_MSB      => RAM_EVO_wrreq_addr_MSB,
        RAM_addr_size       => RAM_EVO_addr_size,
        RAM_data_size       => RAM_EVO_data_size,
        RAM_byteena_size    => RAM_EVO_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk_80,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => EVO_MEM_gen_data,
        wrreq_vld       => EVO_MEM_gen_vld,
        wrreq_next      => EVO_MEM_gen_next,
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_EVO_portB_addr,
        RAM_data        => RAM_EVO_portB_data,
        RAM_byteena     => RAM_EVO_portB_byteena,
        RAM_wren        => RAM_EVO_portB_wren,
        RAM_q           => RAM_EVO_portB_q
    );
    
    -- EVO RAM --
    EVO_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_EVO_numwords,
        portA_data_size     => RAM_EVO_data_size,
        portA_addr_size     => RAM_EVO_addr_size,
        portA_byteena_size  => RAM_EVO_byteena_size,
        portB_numwords      => RAM_EVO_numwords,
        portB_data_size     => RAM_EVO_data_size,
        portB_addr_size     => RAM_EVO_addr_size,
        portB_byteena_size  => RAM_EVO_byteena_size,
        init_file           => RAM_EVO_init_file,
        ram_type            => RAM_EVO_type,
        read_during_write   => "DONT_CARE",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk_80,
        portA_addr      => RAM_EVO_portA_addr,
        portA_data      => RAM_EVO_portA_data,
        portA_byteena   => RAM_EVO_portA_byteena,
        portA_wren      => RAM_EVO_portA_wren,
        portA_q         => RAM_EVO_portA_q,
        portB_addr      => RAM_EVO_portB_addr,
        portB_data      => RAM_EVO_portB_data,
        portB_byteena   => RAM_EVO_portB_byteena,
        portB_wren      => RAM_EVO_portB_wren,
        portB_q         => RAM_EVO_portB_q
    );
    
    -- EVO RAM Access Port A --
    EVO_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_EVO_addr_size,
        RAM_data_size       => RAM_EVO_data_size,
        RAM_byteena_size    => RAM_EVO_byteena_size

    )
    PORT MAP
    (
        clk             => clk_80,
        rdreq_data      => VME_rdreq_rx_data(0),
        rdreq_vld       => VME_rdreq_rx_vld(0),
        rdreq_next      => VME_rdreq_rx_next(0),
        wrreq_data      => VME_wrreq_rx_data(0),
        wrreq_vld       => VME_wrreq_rx_vld(0),
        wrreq_next      => VME_wrreq_rx_next(0),
        rdout_data      => VME_rdout_tx_data(0),
        rdout_vld       => VME_rdout_tx_vld(0),
        rdout_next      => VME_rdout_tx_next(0),
        RAM_addr        => RAM_EVO_portA_addr,
        RAM_data        => RAM_EVO_portA_data,
        RAM_byteena     => RAM_EVO_portA_byteena,
        RAM_wren        => RAM_EVO_portA_wren,
        RAM_q           => RAM_EVO_portA_q
    );
    
    
    --================--
    -- CAP STRUCTURES --
    --================--
    
    -- CAP Gen --
    CAP_MEM_init_inst: entity work.CAP_MEM_init_CMA
    PORT MAP
    (
        clk         => clk_80,
        out_data    => CAP_MEM_gen_data,
        out_vld     => CAP_MEM_gen_vld,
        out_next    => CAP_MEM_gen_next
    );
    
    
    -- CAP RAM Access Port B --
    CAP_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => RAM_CAP_wrreq_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => 0,
        wrreq_data_MSB      => RAM_CAP_wrreq_data_MSB,
        wrreq_addr_MSB      => RAM_CAP_wrreq_addr_MSB,
        RAM_addr_size       => RAM_CAP_addr_size,
        RAM_data_size       => RAM_CAP_data_size,
        RAM_byteena_size    => RAM_CAP_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk_80,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => CAP_MEM_gen_data,
        wrreq_vld       => CAP_MEM_gen_vld,
        wrreq_next      => CAP_MEM_gen_next,
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_CAP_portB_addr,
        RAM_data        => RAM_CAP_portB_data,
        RAM_byteena     => RAM_CAP_portB_byteena,
        RAM_wren        => RAM_CAP_portB_wren,
        RAM_q           => RAM_CAP_portB_q
    );
    
    -- CAP RAM --
    CAP_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_CAP_numwords,
        portA_data_size     => RAM_CAP_data_size,
        portA_addr_size     => RAM_CAP_addr_size,
        portA_byteena_size  => RAM_CAP_byteena_size,
        portB_numwords      => RAM_CAP_numwords,
        portB_data_size     => RAM_CAP_data_size,
        portB_addr_size     => RAM_CAP_addr_size,
        portB_byteena_size  => RAM_CAP_byteena_size,
        init_file           => RAM_CAP_init_file,
        ram_type            => RAM_CAP_type,
        read_during_write   => "DONT_CARE",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk_80,
        portA_addr      => RAM_CAP_portA_addr,
        portA_data      => RAM_CAP_portA_data,
        portA_byteena   => RAM_CAP_portA_byteena,
        portA_wren      => RAM_CAP_portA_wren,
        portA_q         => RAM_CAP_portA_q,
        portB_addr      => RAM_CAP_portB_addr,
        portB_data      => RAM_CAP_portB_data,
        portB_byteena   => RAM_CAP_portB_byteena,
        portB_wren      => RAM_CAP_portB_wren,
        portB_q         => RAM_CAP_portB_q
    );
    
    -- CAP RAM Access Port A --
    CAP_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_CAP_addr_size,
        RAM_data_size       => RAM_CAP_data_size,
        RAM_byteena_size    => RAM_CAP_byteena_size

    )
    PORT MAP
    (
        clk             => clk_80,
        rdreq_data      => VME_rdreq_rx_data(1),
        rdreq_vld       => VME_rdreq_rx_vld(1),
        rdreq_next      => VME_rdreq_rx_next(1),
        wrreq_data      => VME_wrreq_rx_data(1),
        wrreq_vld       => VME_wrreq_rx_vld(1),
        wrreq_next      => VME_wrreq_rx_next(1),
        rdout_data      => VME_rdout_tx_data(1),
        rdout_vld       => VME_rdout_tx_vld(1),
        rdout_next      => VME_rdout_tx_next(1),
        RAM_addr        => RAM_CAP_portA_addr,
        RAM_data        => RAM_CAP_portA_data,
        RAM_byteena     => RAM_CAP_portA_byteena,
        RAM_wren        => RAM_CAP_portA_wren,
        RAM_q           => RAM_CAP_portA_q
    );
    
    
   
    --=================--
    -- LOSS STRUCTURES --
    --=================--
    
    
    -- LOSS Gen --
    LOSS_MEM_gen_inst: entity work.LOSS_MEM_gen_CMA
    PORT MAP
    (
        clk         => clk_80,
        trigger     => timer_trigger,
        out_data    => LOSS_MEM_gen_data,
        out_vld     => LOSS_MEM_gen_vld,
        out_next    => LOSS_MEM_gen_next
    );
    

    -- RAM Access Port B --
    LOSS_WC_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => RAM_LOSS_wrreq_data_size,
        rdreq_data_size     => 64,
        rdout_data_size     => 0,
        wrreq_byteena_MSB   => 0,
        wrreq_data_MSB      => RAM_LOSS_wrreq_data_MSB,
        wrreq_addr_MSB      => RAM_LOSS_wrreq_addr_MSB,
        RAM_addr_size       => RAM_LOSS_addr_size,
        RAM_data_size       => RAM_LOSS_data_size,
        RAM_byteena_size    => RAM_LOSS_byteena_size,
        full_byteena        => 1

    )
    PORT MAP
    (
        clk             => clk_80,
        rdreq_data      => (others => '0'),
        rdreq_vld       => '0',
        rdreq_next      => open,
        wrreq_data      => LOSS_MEM_gen_data,
        wrreq_vld       => LOSS_MEM_gen_vld,
        wrreq_next      => LOSS_MEM_gen_next,
        rdout_data      => open,
        rdout_vld       => open,
        rdout_next      => '1',
        RAM_addr        => RAM_LOSS_portB_addr,
        RAM_data        => RAM_LOSS_portB_data,
        RAM_byteena     => RAM_LOSS_portB_byteena,
        RAM_wren        => RAM_LOSS_portB_wren,
        RAM_q           => RAM_LOSS_portB_q
    );
    
    
    
    
    
    -- LOSS RAM --
    LOSS_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_LOSS_numwords,
        portA_data_size     => RAM_LOSS_data_size,
        portA_addr_size     => RAM_LOSS_addr_size,
        portA_byteena_size  => RAM_LOSS_byteena_size,
        portB_numwords      => RAM_LOSS_numwords,
        portB_data_size     => RAM_LOSS_data_size,
        portB_addr_size     => RAM_LOSS_addr_size,
        portB_byteena_size  => RAM_LOSS_byteena_size,
        init_file           => RAM_LOSS_init_file,
        ram_type            => RAM_LOSS_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk_80,
        portA_addr      => RAM_LOSS_portA_addr,
        portA_data      => RAM_LOSS_portA_data,
        portA_byteena   => RAM_LOSS_portA_byteena,
        portA_wren      => RAM_LOSS_portA_wren,
        portA_q         => RAM_LOSS_portA_q,
        portB_addr      => RAM_LOSS_portB_addr,
        portB_data      => RAM_LOSS_portB_data,
        portB_byteena   => RAM_LOSS_portB_byteena,
        portB_wren      => RAM_LOSS_portB_wren,
        portB_q         => RAM_LOSS_portB_q
    );
    
    -- LOSS RAM Access Port A --
    LOSS_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_LOSS_addr_size,
        RAM_data_size       => RAM_LOSS_data_size,
        RAM_byteena_size    => RAM_LOSS_byteena_size

    )
    PORT MAP
    (
        clk             => clk_80,
        rdreq_data      => VME_rdreq_rx_data(2),
        rdreq_vld       => VME_rdreq_rx_vld(2),
        rdreq_next      => VME_rdreq_rx_next(2),
        wrreq_data      => VME_wrreq_rx_data(2),
        wrreq_vld       => VME_wrreq_rx_vld(2),
        wrreq_next      => VME_wrreq_rx_next(2),
        rdout_data      => VME_rdout_tx_data(2),
        rdout_vld       => VME_rdout_tx_vld(2),
        rdout_next      => VME_rdout_tx_next(2),
        RAM_addr        => RAM_LOSS_portA_addr,
        RAM_data        => RAM_LOSS_portA_data,
        RAM_byteena     => RAM_LOSS_portA_byteena,
        RAM_wren        => RAM_LOSS_portA_wren,
        RAM_q           => RAM_LOSS_portA_q
    );
    
    
    --=================--
    -- DIAG STRUCTURES --
    --=================--

    -- DIAG RAM --
    DIAG_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_DIAG_numwords,
        portA_data_size     => RAM_DIAG_data_size,
        portA_addr_size     => RAM_DIAG_addr_size,
        portA_byteena_size  => RAM_DIAG_byteena_size,
        portB_numwords      => RAM_DIAG_numwords,
        portB_data_size     => RAM_DIAG_data_size,
        portB_addr_size     => RAM_DIAG_addr_size,
        portB_byteena_size  => RAM_DIAG_byteena_size,
        init_file           => RAM_DIAG_init_file,
        ram_type            => RAM_DIAG_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk_80,
        portA_addr      => RAM_DIAG_portA_addr,
        portA_data      => RAM_DIAG_portA_data,
        portA_byteena   => RAM_DIAG_portA_byteena,
        portA_wren      => RAM_DIAG_portA_wren,
        portA_q         => RAM_DIAG_portA_q,
        portB_addr      => (others => '0'),
        portB_data      => (others => '0'),
        portB_byteena   => (others => '0'),
        portB_wren      => '0',
        portB_q         => open
    );
    
    -- DIAG RAM Access Port A --
    DIAG_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_DIAG_addr_size,
        RAM_data_size       => RAM_DIAG_data_size,
        RAM_byteena_size    => RAM_DIAG_byteena_size

    )
    PORT MAP
    (
        clk             => clk_80,
        rdreq_data      => VME_rdreq_rx_data(3),
        rdreq_vld       => VME_rdreq_rx_vld(3),
        rdreq_next      => VME_rdreq_rx_next(3),
        wrreq_data      => VME_wrreq_rx_data(3),
        wrreq_vld       => VME_wrreq_rx_vld(3),
        wrreq_next      => VME_wrreq_rx_next(3),
        rdout_data      => VME_rdout_tx_data(3),
        rdout_vld       => VME_rdout_tx_vld(3),
        rdout_next      => VME_rdout_tx_next(3),
        RAM_addr        => RAM_DIAG_portA_addr,
        RAM_data        => RAM_DIAG_portA_data,
        RAM_byteena     => RAM_DIAG_portA_byteena,
        RAM_wren        => RAM_DIAG_portA_wren,
        RAM_q           => RAM_DIAG_portA_q
    );

    
    --==================--
    -- PARAM STRUCTURES --
    --==================--
    
    -- PARAM RAM --
    PARAM_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_PARAM_numwords,
        portA_data_size     => RAM_PARAM_data_size,
        portA_addr_size     => RAM_PARAM_addr_size,
        portA_byteena_size  => RAM_PARAM_byteena_size,
        portB_numwords      => RAM_PARAM_numwords,
        portB_data_size     => RAM_PARAM_data_size,
        portB_addr_size     => RAM_PARAM_addr_size,
        portB_byteena_size  => RAM_PARAM_byteena_size,
        init_file           => RAM_PARAM_init_file,
        ram_type            => RAM_PARAM_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk_80,
        portA_addr      => RAM_PARAM_portA_addr,
        portA_data      => RAM_PARAM_portA_data,
        portA_byteena   => RAM_PARAM_portA_byteena,
        portA_wren      => RAM_PARAM_portA_wren,
        portA_q         => RAM_PARAM_portA_q,
        portB_addr      => (others => '0'),
        portB_data      => (others => '0'),
        portB_byteena   => (others => '0'),
        portB_wren      => '0',
        portB_q         => open
    );
    
    -- PARAM RAM Access Port A --
    PARAM_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_PARAM_addr_size,
        RAM_data_size       => RAM_PARAM_data_size,
        RAM_byteena_size    => RAM_PARAM_byteena_size

    )
    PORT MAP
    (
        clk             => clk_80,
        rdreq_data      => VME_rdreq_rx_data(4),
        rdreq_vld       => VME_rdreq_rx_vld(4),
        rdreq_next      => VME_rdreq_rx_next(4),
        wrreq_data      => VME_wrreq_rx_data(4),
        wrreq_vld       => VME_wrreq_rx_vld(4),
        wrreq_next      => VME_wrreq_rx_next(4),
        rdout_data      => VME_rdout_tx_data(4),
        rdout_vld       => VME_rdout_tx_vld(4),
        rdout_next      => VME_rdout_tx_next(4),
        RAM_addr        => RAM_PARAM_portA_addr,
        RAM_data        => RAM_PARAM_portA_data,
        RAM_byteena     => RAM_PARAM_portA_byteena,
        RAM_wren        => RAM_PARAM_portA_wren,
        RAM_q           => RAM_PARAM_portA_q
    );
    
        
    --=================--
    -- PROG STRUCTURES --
    --=================--
    
    -- PROG RAM --
    PROG_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_PROG_numwords,
        portA_data_size     => RAM_PROG_data_size,
        portA_addr_size     => RAM_PROG_addr_size,
        portA_byteena_size  => RAM_PROG_byteena_size,
        portB_numwords      => RAM_PROG_numwords,
        portB_data_size     => RAM_PROG_data_size,
        portB_addr_size     => RAM_PROG_addr_size,
        portB_byteena_size  => RAM_PROG_byteena_size,
        init_file           => RAM_PROG_init_file,
        ram_type            => RAM_PROG_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk_80,
        portA_addr      => RAM_PROG_portA_addr,
        portA_data      => RAM_PROG_portA_data,
        portA_byteena   => RAM_PROG_portA_byteena,
        portA_wren      => RAM_PROG_portA_wren,
        portA_q         => RAM_PROG_portA_q,
        portB_addr      => (others => '0'),
        portB_data      => (others => '0'),
        portB_byteena   => (others => '0'),
        portB_wren      => '0',
        portB_q         => open
    );
    
    -- PROG RAM Access Port B --
    PROG_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_PROG_addr_size,
        RAM_data_size       => RAM_PROG_data_size,
        RAM_byteena_size    => RAM_PROG_byteena_size

    )
    PORT MAP
    (
        clk             => clk_80,
        rdreq_data      => VME_rdreq_rx_data(5),
        rdreq_vld       => VME_rdreq_rx_vld(5),
        rdreq_next      => VME_rdreq_rx_next(5),
        wrreq_data      => VME_wrreq_rx_data(5),
        wrreq_vld       => VME_wrreq_rx_vld(5),
        wrreq_next      => VME_wrreq_rx_next(5),
        rdout_data      => VME_rdout_tx_data(5),
        rdout_vld       => VME_rdout_tx_vld(5),
        rdout_next      => VME_rdout_tx_next(5),
        RAM_addr        => RAM_PROG_portA_addr,
        RAM_data        => RAM_PROG_portA_data,
        RAM_byteena     => RAM_PROG_portA_byteena,
        RAM_wren        => RAM_PROG_portA_wren,
        RAM_q           => RAM_PROG_portA_q
    );

    
    --=================--
    -- CTRL STRUCTURES --
    --=================--
    
    -- CTRL RAM --
    CTRL_DPRAM: entity work.ALTERA_DPRAM
    GENERIC MAP
    (
        portA_numwords      => RAM_CTRL_numwords,
        portA_data_size     => RAM_CTRL_data_size,
        portA_addr_size     => RAM_CTRL_addr_size,
        portA_byteena_size  => RAM_CTRL_byteena_size,
        portB_numwords      => RAM_CTRL_numwords,
        portB_data_size     => RAM_CTRL_data_size,
        portB_addr_size     => RAM_CTRL_addr_size,
        portB_byteena_size  => RAM_CTRL_byteena_size,
        init_file           => RAM_CTRL_init_file,
        ram_type            => RAM_CTRL_type,
        read_during_write   => "OLD_DATA",
        device_family       => "Stratix"

    )
    PORT MAP
    (
        clk             => clk_80,
        portA_addr      => RAM_CTRL_portA_addr,
        portA_data      => RAM_CTRL_portA_data,
        portA_byteena   => RAM_CTRL_portA_byteena,
        portA_wren      => RAM_CTRL_portA_wren,
        portA_q         => RAM_CTRL_portA_q,
        portB_addr      => (others => '0'),
        portB_data      => (others => '0'),
        portB_byteena   => (others => '0'),
        portB_wren      => '0',
        portB_q         => open
    );
    
    -- CTRL RAM Access Port A --
    CTRL_VME_ram_access_CMA: entity work.ram_access_CMA
    GENERIC MAP
    (
        wrreq_data_size     => VME_wrreq_data_size,
        rdreq_data_size     => VME_rdreq_data_size,
        rdout_data_size     => VME_rdout_data_size,
        wrreq_byteena_MSB   => VME_wrreq_byteena_MSB,
        wrreq_data_MSB      => VME_wrreq_d_MSB,
        wrreq_addr_MSB      => VME_wrreq_addr_MSB,
        RAM_addr_size       => RAM_CTRL_addr_size,
        RAM_data_size       => RAM_CTRL_data_size,
        RAM_byteena_size    => RAM_CTRL_byteena_size

    )
    PORT MAP
    (
        clk             => clk_80,
        rdreq_data      => VME_rdreq_rx_data(6),
        rdreq_vld       => VME_rdreq_rx_vld(6),
        rdreq_next      => VME_rdreq_rx_next(6),
        wrreq_data      => VME_wrreq_rx_data(6),
        wrreq_vld       => VME_wrreq_rx_vld(6),
        wrreq_next      => VME_wrreq_rx_next(6),
        rdout_data      => VME_rdout_tx_data(6),
        rdout_vld       => VME_rdout_tx_vld(6),
        rdout_next      => VME_rdout_tx_next(6),
        RAM_addr        => RAM_CTRL_portA_addr,
        RAM_data        => RAM_CTRL_portA_data,
        RAM_byteena     => RAM_CTRL_portA_byteena,
        RAM_wren        => RAM_CTRL_portA_wren,
        RAM_q           => RAM_CTRL_portA_q
    );
    
    
    --================--
    -- VME Connection --
    --================--
    
    -- VME_wrreq --
    wrreq_CMI: entity work.T_CMI
    GENERIC MAP
    (
        rx_number       => VME_wrreq_rx_number,
        tx_data_size    => VME_wrreq_data_size,
        rx_data_size    => VME_wrreq_data_size
    )
    PORT MAP
    (
        clk             => clk_80,
        tx_data         => VME_wrreq_tx_data,
        tx_vld          => VME_wrreq_tx_vld,
        tx_next         => VME_wrreq_tx_next,
        rx_data         => VME_wrreq_rx_data,
        rx_vld          => VME_wrreq_rx_vld,
        rx_next         => VME_wrreq_rx_next,
        ADM_tx_data     => VME_wrreq_ADM_tx_data,
        ADM_rx_data     => VME_wrreq_ADM_rx_data,
        ADM_sel         => VME_wrreq_ADM_sel
    );
    
    -- ADM VME_wrreq --
    wrreq_ADM: entity work.DAB64x_VME_ADM
    GENERIC MAP
    (
        rx_number       => VME_wrreq_rx_number,
        data_size       => VME_wrreq_data_size,
        addr_size       => VME_wrreq_addr_size
    )
    PORT MAP
    (
        ADM_tx_data     => VME_wrreq_ADM_tx_data,
        ADM_rx_data     => VME_wrreq_ADM_rx_data,
        ADM_sel         => VME_wrreq_ADM_sel
    );
    
    -- VME_rdreq --
    rdreq_CMI: entity work.T_CMI
    GENERIC MAP
    (
        rx_number       => VME_rdreq_rx_number,
        tx_data_size    => VME_rdreq_data_size,
        rx_data_size    => VME_rdreq_data_size
    )
    PORT MAP
    (
        clk             => clk_80,
        tx_data         => VME_rdreq_tx_data,
        tx_vld          => VME_rdreq_tx_vld,
        tx_next         => VME_rdreq_tx_next,
        rx_data         => VME_rdreq_rx_data,
        rx_vld          => VME_rdreq_rx_vld,
        rx_next         => VME_rdreq_rx_next,
        ADM_tx_data     => VME_rdreq_ADM_tx_data,
        ADM_rx_data     => VME_rdreq_ADM_rx_data,
        ADM_sel         => VME_rdreq_ADM_sel
    );
    
    -- ADM VME_rdreq --
    rdreq_ADM: entity work.DAB64x_VME_ADM
    GENERIC MAP
    (
        rx_number       => VME_rdreq_rx_number,
        data_size       => VME_rdreq_data_size, 
        addr_size       => VME_rdreq_addr_size
    )
    PORT MAP
    (
        ADM_tx_data     => VME_rdreq_ADM_tx_data,
        ADM_rx_data     => VME_rdreq_ADM_rx_data,
        ADM_sel         => VME_rdreq_ADM_sel
    );
    
    -- VME_rdout --
    rdout_CMI: entity work.MM_CMI
    GENERIC MAP
    (
        tx_number       => VME_rdout_tx_number,
        rx_number       => VME_rdout_rx_number,
        data_size       => VME_rdout_data_size
    )
    PORT MAP
    (
        clk             => clk_80,
        tx_data         => VME_rdout_tx_data,
        tx_vld          => VME_rdout_tx_vld,
        tx_next         => VME_rdout_tx_next,
        rx_data         => VME_rdout_rx_data,
        rx_vld          => VME_rdout_rx_vld,
        rx_next         => VME_rdout_rx_next,
        ARB_tx_vld      => VME_rdout_ARB_tx_vld,
        ARB_tx_next     => VME_rdout_ARB_tx_next,
        ARB_rx_vld      => VME_rdout_ARB_rx_vld,
        ARB_rx_next     => VME_rdout_ARB_rx_next,
        ARB_sel         => VME_rdout_ARB_sel
    );
    
    -- ARB VME_rdout --
    rdout_ARB: entity work.DAB64x_VME_ARB
    GENERIC MAP
    (
        tx_number       => VME_rdout_tx_number
    )
    PORT MAP
    (
        clk             => clk_80,
        ARB_tx_vld      => VME_rdout_ARB_tx_vld,
        ARB_tx_next     => VME_rdout_ARB_tx_next,
        ARB_rx_vld      => VME_rdout_ARB_rx_vld,
        ARB_rx_next     => VME_rdout_ARB_rx_next,
        ARB_sel         => VME_rdout_ARB_sel
    );
    
    
    
    

    --==========--
    -- VME Core --
    --==========--
    
    vmecore: entity work.vme_core_CMA
    generic map
    (
        Tclk_ns             => Tclk_ns,
        wrreq_data_size     => vme_wrreq_data_size,
        rdreq_data_size     => vme_rdreq_data_size,
        rdout_data_size     => vme_rdout_data_size
    )
    port map
    (   
        clk                 => clk_80,
        VME_GA              => VME_GA,
        VME_GAP             => VME_GAP,
        VME_WRn             => VME_WRn,
        VME_AM              => VME_AM,
        VME_ASn             => VME_ASn,
        VME_DS0n            => VME_DS0n,
        VME_DS1n            => VME_DS1n,
        VME_IACKn           => VME_IACKn,
        VME_IACKINn         => VME_IACKINn,
        VME_SYSCLK          => VME_SYSCLK,
        VME_SYSRESETn       => VME_SYSRESETn,
        VME_ADDR_IN         => VME_ADDR_IN,
        VME_LWORDn_IN       => VME_LWORDn_IN,
        VME_DATA_IN         => VME_DATA_IN,
        VME_ADDR_OUT        => VME_ADDR_OUT,
        VME_LWORDn_OUT      => VME_LWORDn_OUT,
        VME_DATA_OUT        => VME_DATA_OUT,
        VME_DTACKn          => VME_DTACKn,
        VME_BERRn           => VME_BERRn,
        VME_RETRYn          => VME_RETRYn,
        VME_IACKOUTn        => VME_IACKOUTn,
        VME_IRQn(6 downto 1)=> VME_IRQn,
        VME_SYSFAILn        => VME_SYSFAILn,
        VME_ACC             => VME_ACC,
        VME_BUS_BUSY        => VME_BUS_BUSY,
        VME_DATA_BUF_OEn    => VME_DATA_BUF_OEn,
        VME_ADDR_BUF_OEn    => VME_ADDR_BUF_OEn,  
        VME_DTACK_BUF_OEn   => VME_DTACK_BUF_OEn,  
        VME_RETRY_BUF_OEn   => VME_RETRY_BUF_OEn,  
        VME_BERR_BUF_OEn    => VME_BERR_BUF_OEn,   
        VME_DATA_BUF_DIR    => VME_DATA_BUF_DIR,   
        VME_ADDR_BUF_DIR    => VME_ADDR_BUF_DIR,   
        rdreq_data          => vme_rdreq_tx_data,
        rdreq_vld           => vme_rdreq_tx_vld,
        rdreq_next          => vme_rdreq_tx_next,
        wrreq_data          => vme_wrreq_tx_data,
        wrreq_vld           => vme_wrreq_tx_vld,
        wrreq_next          => vme_wrreq_tx_next,
        rdout_data          => vme_rdout_rx_data(0),
        rdout_vld           => vme_rdout_rx_vld(0),
        rdout_next          => vme_rdout_rx_next(0)
    );
    
    --===========================--
    -- DAB64x VME BUS CONTROLLER --
    --===========================--
    
    VME_BUS_Ctrl : entity work.DAB64x_VME_BusCtrl
    port map
    (   
        clk                 => clk_80,
        Stratix_IRQ_VECTORn => Stratix_IRQ_VECTORn,
        VME_IRQ_Level       => VME_IRQ_Level,
        IRQ_Vector_Enable   => IRQ_Vector_Enable,
        Board_Rst_ONOFFn    => Board_Rst_ONOFFn,
        Stratix_VME_ACCn    => Stratix_VME_ACCn,
        VME_ForceRetry      => VME_ForceRetry,
        VME_2e_Cycle        => VME_2e_Cycle,
        Stratix_245_OEn     => Stratix_245_OEn,
        Stratix_dtackN      => Stratix_dtackN,
        Stratix_SYSFAILn    => Stratix_SYSFAILn,
        VME_ADDR            => VME_ADD,
        VME_LWORDn          => VME_LWORDn,
        VME_DATA            => VME_DATA,
        VME_DTACK_EN        => VME_DTACK_EN,
        VME_ADDR_OUT        => VME_ADDR_OUT,
        VME_LWORDn_OUT      => VME_LWORDn_OUT,
        VME_DATA_OUT        => VME_DATA_OUT,
        VME_DATA_BUF_OEn    => VME_DATA_BUF_OEn,
        VME_ADDR_BUF_OEn    => VME_ADDR_BUF_OEn,  
        VME_DTACK_BUF_OEn   => VME_DTACK_BUF_OEn,  
        VME_RETRY_BUF_OEn   => VME_RETRY_BUF_OEn,  
        VME_BERR_BUF_OEn    => VME_BERR_BUF_OEn,   
        VME_DATA_BUF_DIR    => VME_DATA_BUF_DIR,   
        VME_ADDR_BUF_DIR    => VME_ADDR_BUF_DIR,   
        VME_ACC             => VME_ACC,
        VME_DTACKn          => VME_DTACKn,
        VME_SYSFAILn        => VME_SYSFAILn,
        VME_ADDR_IN         => VME_ADDR_IN,
        VME_LWORDn_IN       => VME_LWORDn_IN,
        VME_DATA_IN         => VME_DATA_IN,
        VME_BUS_BUSY        => VME_BUS_BUSY,
        FLASH_DATA_IN       => open, --FLASH_DATA_IN,
        FLASH_DATA_OUT      => "00000000", --FLASH_DATA_OUT,
        FLASH_BUS_REQ       => '0', -- FLASH_BUS_REQ,
        FLASH_BUS_ACK       => open, --FLASH_BUS_ACK,
        FLASH_DATA_BUS_DIR  => '0'
    );
    
   
    
    
end architecture struct;