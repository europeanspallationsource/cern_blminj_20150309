//
//  packet_mux
//
//  This component multiplexes 2 Avalon ST sink interfaces out one Avalon ST
//  source interface.  Once a pending channel is selected to transmit its data,
//  the mux allows the entire packet from that channel to transmit out the
//  source interface.  There is no arbitration applied to the multiplexing
//  schedule, it follows a simple round robin schedule of pending channels.
//
module packet_mux (    
    
      // Interface: clk
      input              csi_clock_clk,
      input              csi_clock_reset_n,
      // Interface: in0
      input              asi_in0_valid,
      output reg         asi_in0_ready,
      input      [31: 0] asi_in0_data,
      input              asi_in0_startofpacket,
      input              asi_in0_endofpacket,
      input      [ 1: 0] asi_in0_empty,
      // Interface: in1
      input              asi_in1_valid,
      output reg         asi_in1_ready,
      input      [31: 0] asi_in1_data,
      input              asi_in1_startofpacket,
      input              asi_in1_endofpacket,
      input      [ 1: 0] asi_in1_empty,
	  input              asi_in1_error,
      // Interface: out
      output reg         aso_out_valid,
      input              aso_out_ready,
      output reg [31: 0] aso_out_data,
      output reg         aso_out_startofpacket,
      output reg         aso_out_endofpacket,
      output reg [ 1: 0] aso_out_empty,
      output reg 		 aso_out_error
);

   // ---------------------------------------------------------------------
   //| Signal Declarations
   // ---------------------------------------------------------------------
   reg  [36: 0] asi_in0_payload;
   reg  [36: 0] asi_in1_payload;

   reg  		decision = 0;      
   reg  		select = 0;
   reg          selected_endofpacket = 0;
   reg          selected_startofpacket = 0;
   reg          selected_valid;
   wire         selected_ready;
   reg  [36: 0] selected_payload;
   reg  [ 1: 0] counter;

   reg          in_a_packet;
   wire         out_valid_wire;
   
   wire [36: 0] out_payload;

   // ---------------------------------------------------------------------
   //| Input Mapping
   // ---------------------------------------------------------------------
   always @* begin
     asi_in0_payload <= {asi_in0_data,asi_in0_startofpacket,asi_in0_endofpacket,asi_in0_empty,1'b0};
     asi_in1_payload <= {asi_in1_data,asi_in1_startofpacket,asi_in1_endofpacket,asi_in1_empty,asi_in1_error};
   end
   
   // ---------------------------------------------------------------------
   //| Scheduling Algorithm
   // ---------------------------------------------------------------------
   always @* begin
         
      decision <= 0;
      case(select) 
         0 : begin
            if (asi_in0_valid) decision <= 0;
            if (asi_in1_valid) decision <= 1;
         end  
         1 : begin
            if (asi_in1_valid) decision <= 1;
            if (asi_in0_valid) decision <= 0;
         end
         default : begin // Same as '0', should never get used.
            if (asi_in0_valid) decision <= 0;
            if (asi_in1_valid) decision <= 1;
         end  
      endcase   
   end

   // ---------------------------------------------------------------------
   //| Capture Decision
   // ---------------------------------------------------------------------
   always @ (negedge csi_clock_reset_n, posedge csi_clock_clk) begin
      if (!csi_clock_reset_n) begin
         select <= 0;
         in_a_packet <= 0;
      end else begin
         if (
                ((selected_valid == 0) && (in_a_packet == 0)) || 
                ((selected_valid == 1) && (selected_ready == 1) && (selected_endofpacket))
            ) begin
            select <= decision;
            in_a_packet <= 0;
         end
         else if ((selected_valid == 1) && (selected_ready == 1) && (selected_startofpacket)) begin
            in_a_packet <= 1;
         end
      end
   end

   // ---------------------------------------------------------------------
   //| Mux
   // ---------------------------------------------------------------------
   always @* begin
      case(select) 
         0 : begin
            selected_payload <= asi_in0_payload;         
            selected_valid   <= asi_in0_valid;
            selected_endofpacket <= asi_in0_endofpacket;
            selected_startofpacket <= asi_in0_startofpacket;
         end  
         1 : begin
            selected_payload <= asi_in1_payload;         
            selected_valid   <= asi_in1_valid;
            selected_endofpacket <= asi_in1_endofpacket;
            selected_startofpacket <= asi_in1_startofpacket;
         end
         default : begin
            selected_payload <= asi_in0_payload;         
            selected_valid <= asi_in0_valid;
            selected_endofpacket <= asi_in0_endofpacket;
            selected_startofpacket <= asi_in0_startofpacket;
         end
      endcase

   end

   // ---------------------------------------------------------------------
   //| Back Pressure
   // ---------------------------------------------------------------------
   always @* begin
      asi_in0_ready <= ~asi_in0_valid   ;
      asi_in1_ready <= ~asi_in1_valid   ;
      case(select) 
         0 : asi_in0_ready <= selected_ready;
         1 : asi_in1_ready <= selected_ready;
         default : asi_in0_ready <= selected_ready;
      endcase
   end

   // ---------------------------------------------------------------------
   //| output Pipeline
   // ---------------------------------------------------------------------
   packet_mux_1stage_pipeline #( .PAYLOAD_WIDTH( 37 ) ) outpipe (
        .clk            (csi_clock_clk ),
        .reset_n        (csi_clock_reset_n),
        .in_ready       (selected_ready),
        .in_valid       (selected_valid), 
        .in_payload     (selected_payload),
        .out_ready      (aso_out_ready), 
        .out_valid      (out_valid_wire), 
        .out_payload    (out_payload)
    );
   
   // ---------------------------------------------------------------------
   //| Output Mapping
   // ---------------------------------------------------------------------
   always @* begin
     aso_out_valid   <= out_valid_wire;
     {aso_out_data,aso_out_startofpacket,aso_out_endofpacket,aso_out_empty,aso_out_error} <= out_payload;
   end


endmodule

//  --------------------------------------------------------------------------------
// | single buffered pipeline stage
//  --------------------------------------------------------------------------------
module packet_mux_1stage_pipeline  
#( parameter PAYLOAD_WIDTH = 8 )
 ( input                          clk,
   input                          reset_n, 
   output reg                     in_ready,
   input                          in_valid,   
   input      [PAYLOAD_WIDTH-1:0] in_payload,
   input                          out_ready,   
   output reg                     out_valid,
   output reg [PAYLOAD_WIDTH-1:0] out_payload      
 );
      
   always @* begin
     in_ready <= out_ready || ~out_valid;
   end
   
   always @ (negedge reset_n, posedge clk) begin
      if (!reset_n) begin
         out_valid <= 0;
         out_payload <= 0;
      end else begin
         if (in_valid) begin
           out_valid <= 1;
         end else if (out_ready) begin
           out_valid <= 0;
         end
         
         if(in_valid && in_ready) begin
            out_payload <= in_payload;
         end
      end
   end

endmodule

