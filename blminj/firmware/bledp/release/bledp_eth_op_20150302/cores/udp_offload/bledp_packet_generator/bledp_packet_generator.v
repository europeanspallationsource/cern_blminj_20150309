//
// bledp_packet_generator
//


module bledp_packet_generator
(
    // clock interface
    input           csi_clock_clk,
    input           csi_clock_reset,
    
    // slave interface
    input           avs_s0_write,
    input           avs_s0_read,
    input   [1:0]   avs_s0_address,
    input   [3:0]   avs_s0_byteenable,
    input   [31:0]  avs_s0_writedata,
    output  [31:0]  avs_s0_readdata,
    
    // source interface
    output          aso_src0_valid,
    input           aso_src0_ready,
    output  [31:0]  aso_src0_data,
    output  [1:0]   aso_src0_empty,
    output          aso_src0_startofpacket,
    output          aso_src0_endofpacket,
	
	// conduit interface
	input	[31:0]	coe_bledp_data,
	input			coe_bledp_valid,
	output			coe_bledp_ready
);

localparam [1:0] IDLE_STATE = 2'h0;
localparam [1:0] SOP_STATE  = 2'h1;
localparam [1:0] DATA_STATE = 2'h2;
localparam [1:0] EOP_STATE  = 2'h3;

reg             go_bit;
reg             running_bit;
reg     [13:0]  bledp_dword_count;
reg     [13:0]  dword_count;
reg     [31:0]  coe_bledp_data_previous;
reg     [31:0]  bledp_chunk_count;
reg     [31:0]  bledp_chunk_count_request;
wire            bledp_chunks_transmitted;
wire            bledp_continous_transmit;
reg             clear_bledp_chunk_count;
reg     [1:0]   state;


//
// slave read mux
//
assign avs_s0_readdata =    (avs_s0_address == 2'h0) ?  ({{30{1'b0}}, running_bit, go_bit}) :
                            (avs_s0_address == 2'h1) ?  ({{18{1'b0}}, bledp_dword_count}) :
                            (avs_s0_address == 2'h2) ?  (bledp_chunk_count) :
                                                        (bledp_chunk_count_request);

//
// slave write demux
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        go_bit                  <= 0;
        bledp_dword_count		<= 0;
        clear_bledp_chunk_count      <= 0;
    end
    else
    begin
        if(avs_s0_write)
        begin
            case(avs_s0_address)
                2'h0:
                begin
                    if (avs_s0_byteenable[0] == 1'b1)
                        go_bit  <= avs_s0_writedata[0];
                end
                2'h1:
                begin
                    if (avs_s0_byteenable[0] == 1'b1)
                        bledp_dword_count[7:0]    <= avs_s0_writedata[7:0];
                    if (avs_s0_byteenable[1] == 1'b1)
                        bledp_dword_count[13:8]   <= avs_s0_writedata[13:8];
                end
                2'h2:
                begin
                    clear_bledp_chunk_count  <= 1;
                end
				2'h3:
                begin
                    if (avs_s0_byteenable[0] == 1'b1)
                        bledp_chunk_count_request[7:0]    <= avs_s0_writedata[7:0];
                    if (avs_s0_byteenable[1] == 1'b1)
                        bledp_chunk_count_request[15:8]   <= avs_s0_writedata[15:8];
					if (avs_s0_byteenable[2] == 1'b1)
                        bledp_chunk_count_request[23:16]   <= avs_s0_writedata[23:16];
					if (avs_s0_byteenable[3] == 1'b1)
                        bledp_chunk_count_request[31:24]   <= avs_s0_writedata[31:24];
                end
            endcase
        end
        else
        begin
            clear_bledp_chunk_count  <= 0;
			if(bledp_chunks_transmitted)
			begin
				go_bit <= 0;
			end
        end
    end
end

//
// bledp_chunk_count state machine
//
// count the packet when we send startofpacket, the first word of the packet
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        bledp_chunk_count <= 0;
    end
    else
    begin
        if(clear_bledp_chunk_count || bledp_chunks_transmitted)
        begin
            bledp_chunk_count <= 0;
        end
        else if(aso_src0_valid & aso_src0_ready & aso_src0_startofpacket)
        begin
            bledp_chunk_count <= bledp_chunk_count + 1;
        end
    end
end


//assign bledp_chunks_transmitted = (bledp_chunk_count == bledp_chunk_count_request) ? (1'b1) : (1'b0);

assign bledp_chunks_transmitted = 	(bledp_chunk_count != bledp_chunk_count_request) ? (1'b0) : 
									(bledp_continous_transmit) ? (1'b0) : (1'b1);
									
assign bledp_continous_transmit = (bledp_chunk_count_request == 0) ? (1'b1) : (1'b0);


//
// running_bit state machine
//
// we start immediately when go_bit is asserted
// we don't stop until we reach the end of the current packet that we're generating
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        running_bit <= 0;
    end
    else
    begin
        if(go_bit)
        begin
            running_bit <= 1;
        end
        else if(running_bit & !go_bit & aso_src0_valid & aso_src0_ready & aso_src0_endofpacket)
        begin
            running_bit <= 0;
        end
    end
end

//
// coe_bledp_data_previous state machine
//
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        coe_bledp_data_previous  <= 0;
    end
    else
    begin
        if(((state == SOP_STATE) || (state == DATA_STATE)) && aso_src0_valid && aso_src0_ready)
        begin
            coe_bledp_data_previous <= coe_bledp_data;
        end
    end
end

//
// dword_count state machine
//
// this state machine counts the number of bytes that have been transmitted in
// current packet.
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        dword_count  <= 0;
    end
    else
    begin
        case(state)
            IDLE_STATE:
            begin
                dword_count  <= 0;
            end
            SOP_STATE:
            begin
                dword_count  <= 1;
            end
            DATA_STATE:
            begin
                if(aso_src0_valid && aso_src0_ready)
                begin
                    dword_count  <= dword_count + 1;
                end
            end
            EOP_STATE:
            begin
                dword_count  <= 0;
            end
        endcase
    end
end


//
// source interface control
//
// these are combinatorial control equations for our source interface
//

assign coe_bledp_ready			=	(((state == SOP_STATE) && (bledp_dword_count >= 1)) || (state == DATA_STATE)) ? (aso_src0_ready & coe_bledp_valid) : (1'b0);

assign aso_src0_valid           =   (state == EOP_STATE) ? (running_bit) : (running_bit & coe_bledp_valid);

assign aso_src0_data            =   (state == SOP_STATE) ? ({bledp_dword_count, 2'b0, coe_bledp_data[31:16]}) :
                                    ({coe_bledp_data_previous[15:0], coe_bledp_data[31:16]});

assign aso_src0_empty           =   (state == EOP_STATE) ? (2'h2) : 
									((state == SOP_STATE) && (bledp_dword_count < 1)) ? (2'h2) : (2'h0);

assign aso_src0_startofpacket   =   (state == SOP_STATE) ? (1'b1) : (1'b0);

assign aso_src0_endofpacket     =   (state == EOP_STATE) ? (1'b1) : 
                                    ((state == SOP_STATE) && (bledp_dword_count < 1)) ? (1'b1) : (1'b0);

//
// source state machine
//
// this state machine provides synchronous sequencing for the control of the
// source interface
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        state <= IDLE_STATE;
    end
    else
    begin
        case(state)
            IDLE_STATE:
            begin
                if(go_bit)
                begin
                    state <= SOP_STATE;
                end
            end
            SOP_STATE:
            begin
                if((bledp_dword_count < 1) && go_bit && aso_src0_valid && aso_src0_ready)							//0
                begin
                    state <= SOP_STATE;
                end
                else if((bledp_dword_count > 1) && aso_src0_valid && aso_src0_ready)								//2, 3, 4, 5 ...
                begin
                    state <= DATA_STATE;
                end
                else if((bledp_dword_count == 1) && aso_src0_valid && aso_src0_ready)								//1
                begin
                    state <= EOP_STATE;
                end
                else if((bledp_dword_count < 1) && !go_bit && aso_src0_valid && aso_src0_ready)
                begin
                    state <= IDLE_STATE;
                end
            end
            DATA_STATE:
            begin
                if(((dword_count + 1) >= bledp_dword_count) && aso_src0_valid && aso_src0_ready)
                begin
                    state <= EOP_STATE;
                end
            end
            EOP_STATE:
            begin
                if(go_bit && aso_src0_valid && aso_src0_ready)
                begin
                    state <= SOP_STATE;
                end
                else if(!go_bit  && aso_src0_valid && aso_src0_ready)
                begin
                    state <= IDLE_STATE;
                end
            end
        endcase
    end
end

endmodule
