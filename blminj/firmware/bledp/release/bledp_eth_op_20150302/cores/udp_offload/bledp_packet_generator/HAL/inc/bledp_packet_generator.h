#ifndef BLEDP_PACKET_GENERATOR_H
#define BLEDP_PACKET_GENERATOR_H

#include "alt_types.h"

typedef struct {
    alt_u32 csr_state;      // csr value
    alt_u16 dword_count;    // dword (4*byte) length of generated packets
    alt_u32 packet_count;   // packet counter value
    alt_u32 packet_request; // packet requested value
} PKT_GEN_STATS;

int start_packet_generator(void *base, alt_u16 dword_count, alt_u32 chunks_requested);
int stop_packet_generator(void *base);
int is_packet_generator_running(void *base);
int clear_bledp_chunks_counter(void *base);
int wait_until_packet_generator_stops_running(void *base);
int get_packet_generator_stats(void *base, PKT_GEN_STATS *stats);

#endif /*BLEDP_PACKET_GENERATOR_H*/
