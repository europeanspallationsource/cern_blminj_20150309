##
## DEVICE  "EP4CGX150CF23C7"
##


#**************************************************************
# Time Information
#**************************************************************

set_time_format -unit ns -decimal_places 3



#**************************************************************
# Create Clock
#**************************************************************

#Base Clock
create_clock -name REFCLK2 -period 8.000 -waveform { 0.000 4.000 } [get_ports {REFCLK2}]
create_clock -name REFCLK1 -period 8.000 -waveform { 0.000 4.000 } [get_ports {REFCLK1}]

create_clock -name DCO[1] -period 4.000 -waveform { 0.000 2.000 } [get_ports {DCO[1]}]
create_clock -name DCO[2] -period 4.000 -waveform { 0.000 2.000 } [get_ports {DCO[2]}]
create_clock -name DCO[3] -period 4.000 -waveform { 0.000 2.000 } [get_ports {DCO[3]}]
create_clock -name DCO[4] -period 4.000 -waveform { 0.000 2.000 } [get_ports {DCO[4]}]
create_clock -name DCO[5] -period 4.000 -waveform { 0.000 2.000 } [get_ports {DCO[5]}]
create_clock -name DCO[6] -period 4.000 -waveform { 0.000 2.000 } [get_ports {DCO[6]}]
create_clock -name DCO[7] -period 4.000 -waveform { 0.000 2.000 } [get_ports {DCO[7]}]
create_clock -name DCO[8] -period 4.000 -waveform { 0.000 2.000 } [get_ports {DCO[8]}]

#Virtual Base Clock
#create_clock -name REFCLK2_virt -period 8.000 -waveform { 0.000 4.000 } 

# In-System Memory Clock
#create_clock -name {altera_reserved_tck} -period 100.000 -waveform { 0.000 50.000 } [get_ports {altera_reserved_tck}]


#**************************************************************
# Create Generated Clock
#**************************************************************

derive_pll_clocks

#create_generated_clock -name clk_125 -source [get_pins {buf|altpll_component|auto_generated|pll1|inclk[0]}] [get_pins {buf|altpll_component|auto_generated|pll1|clk[0]}]

#create_generated_clock -name clk_10 -source [get_pins {buf|altpll_component|auto_generated|pll1|inclk[0]}] -divide_by 25 -multiply_by 2 [get_pins {buf|altpll_component|auto_generated|pll1|clk[1]}]

#create_generated_clock -name I2C_clk -source [get_pins {buf|altpll_component|auto_generated|pll1|inclk[0]}] -divide_by 500 [get_pins {buf|altpll_component|auto_generated|pll1|clk[2]}]

#create_generated_clock -name clk_40 -source [get_pins {buf|altpll_component|auto_generated|pll1|inclk[0]}] -divide_by 25 -multiply_by 8 [get_pins {buf|altpll_component|auto_generated|pll1|clk[3]}]


#**************************************************************
# Set Clock Latency
#**************************************************************



#**************************************************************
# Set Clock Uncertainty
#**************************************************************

#set_clock_uncertainty -rise_from [get_clocks {altera_reserved_tck}] -rise_to [get_clocks {altera_reserved_tck}]  0.020 
#set_clock_uncertainty -rise_from [get_clocks {altera_reserved_tck}] -fall_to [get_clocks {altera_reserved_tck}]  0.020 
#set_clock_uncertainty -fall_from [get_clocks {altera_reserved_tck}] -rise_to [get_clocks {altera_reserved_tck}]  0.020 
#set_clock_uncertainty -fall_from [get_clocks {altera_reserved_tck}] -fall_to [get_clocks {altera_reserved_tck}]  0.020 


#**************************************************************
# Set Input Delay
#**************************************************************



#**************************************************************
# Set Output Delay
#**************************************************************



#**************************************************************
# Set Clock Groups
#**************************************************************

#set_clock_groups -asynchronous -group [get_clocks {altera_reserved_tck}] 


#**************************************************************
# Set False Path
#**************************************************************


set_false_path  -from  [get_clocks {DCO[1]}]  -to  [get_clocks {PLL1_inst|altpll_component|auto_generated|pll1|clk[2]}]
set_false_path  -from  [get_clocks {DCO[2]}]  -to  [get_clocks {PLL1_inst|altpll_component|auto_generated|pll1|clk[2]}]
set_false_path  -from  [get_clocks {DCO[3]}]  -to  [get_clocks {PLL1_inst|altpll_component|auto_generated|pll1|clk[2]}]
set_false_path  -from  [get_clocks {DCO[4]}]  -to  [get_clocks {PLL1_inst|altpll_component|auto_generated|pll1|clk[2]}]
set_false_path  -from  [get_clocks {DCO[5]}]  -to  [get_clocks {PLL1_inst|altpll_component|auto_generated|pll1|clk[2]}]
set_false_path  -from  [get_clocks {DCO[6]}]  -to  [get_clocks {PLL1_inst|altpll_component|auto_generated|pll1|clk[2]}]
set_false_path  -from  [get_clocks {DCO[7]}]  -to  [get_clocks {PLL1_inst|altpll_component|auto_generated|pll1|clk[2]}]
set_false_path  -from  [get_clocks {DCO[8]}]  -to  [get_clocks {PLL1_inst|altpll_component|auto_generated|pll1|clk[2]}]


#**************************************************************
# Set Multicycle Path
#**************************************************************



#**************************************************************
# Set Maximum Delay
#**************************************************************


set_max_delay -from [get_ports {DCO[1] D[1]}] -through [get_pins *] -to [all_registers] 1.250
set_max_delay -from [get_ports {DCO[2] D[2]}] -through [get_pins *] -to [all_registers] 1.250
set_max_delay -from [get_ports {DCO[3] D[3]}] -through [get_pins *] -to [all_registers] 1.250
set_max_delay -from [get_ports {DCO[4] D[4]}] -through [get_pins *] -to [all_registers] 1.250
set_max_delay -from [get_ports {DCO[5] D[5]}] -through [get_pins *] -to [all_registers] 1.250
set_max_delay -from [get_ports {DCO[6] D[6]}] -through [get_pins *] -to [all_registers] 1.250
set_max_delay -from [get_ports {DCO[7] D[7]}] -through [get_pins *] -to [all_registers] 1.250
set_max_delay -from [get_ports {DCO[8] D[8]}] -through [get_pins *] -to [all_registers] 1.250


#**************************************************************
# Set Minimum Delay
#**************************************************************



#**************************************************************
# Set Input Transition
#**************************************************************
