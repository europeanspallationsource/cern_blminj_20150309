-- Company:		CERN - BE/BI/BL
-- Engineer:	Maciej Kwiatkowski
-- Create Date: 03/07/2012
-- Module Name:	bledp_acquisition
-- Description:	module to acquire and combine the data in the BLEDP card


LIBRARY IEEE;
USE IEEE.STD_Logic_1164.ALL;
USE IEEE.Numeric_STD.ALL;


entity bledp_acquisition is
	generic (
		t_clk_period_ns 	: natural := 4;
		t_integration_ns	: natural := 2000;			-- must be >= 100 ns, default is 2us
		ADC_dynamic_range	: boolean := true			-- false - disable dynamic ADC range, true - enable dynamic ADC range
		);
	port (
		-- clock
		clk		: in std_logic;
		-- reset
		nrst	: in std_logic;
		-- ADC interface
		adc_run	: out std_logic;
		adc_next : out std_logic;
		adc_vld : in std_logic;
		adc_data : in std_logic_vector(15 downto 0); 
		-- integrator interface
		fdfc_pos : in std_logic;
		fdfc_neg : in std_logic;
		-- control status
		raw_data : in std_logic;
		adc_range_reset : in std_logic;
		adc_fixed_range_max : in std_logic_vector(15 downto 0);
		adc_fixed_range_min : in std_logic_vector(15 downto 0);
		adc_max : out std_logic_vector(15 downto 0);
		adc_min : out std_logic_vector(15 downto 0);
		saturation : out std_logic;
		period_pulse : out std_logic;
		-- output data
		data : out std_logic_vector(19 downto 0);
		data_debug : out std_logic_vector(19 downto 0);
		data_en : out std_logic
		);
end bledp_acquisition;

------------------------------------------------------------------
architecture rtl of bledp_acquisition is 
------------------------------------------------------------------

-- components
component multiplier is
	generic (
		OP1_LEN			: natural := 16;			-- operand 1 bits number
		OP2_LEN			: natural := 4;				-- operand 2 bits number
		RESET_ACTIVE	: std_logic	:= '0'			-- polarity of the reset
	);
	port(
		-- global signals
		clk				: in std_logic;
		rst				: in std_logic;
		-- input signals
		op1				: in unsigned(OP1_LEN-1 downto 0);
		op2				: in unsigned(OP2_LEN-1 downto 0);
		multiply		: in std_logic;
		-- output_signals
		result			: out unsigned(OP1_LEN+OP2_LEN-1 downto 0);
		result_en		: out std_logic;
		busy_flg		: out std_logic
	);
end component;

-- attribute to keep selected nodes names after synthesis
attribute keep: boolean;
-- period counter signals
constant p_cnt_max : natural :=  t_integration_ns/t_clk_period_ns - 1;
signal p_cnt : integer range 0 to p_cnt_max;
signal p_cnt_done : std_logic;
signal p_cnt_done_d : std_logic;
signal p_cnt_done_d1 : std_logic;
signal p_cnt_done_d2 : std_logic;
signal p_cnt_done_d3 : std_logic;
attribute keep of p_cnt_done: signal is true;
attribute keep of p_cnt_done_d: signal is true;
-- adc samples registers signals
signal adc_sample_h_1 : signed(15 downto 0);
signal adc_sample_h_2 : signed(15 downto 0);
signal adc_sample_1 : signed(15 downto 0);
signal adc_sample_2 : signed(15 downto 0);
signal adc_sample_en : std_logic;
attribute keep of adc_sample_1: signal is true;
attribute keep of adc_sample_2: signal is true;
-- adc sample hold register (follow min or max)
signal hold_rst_cnt : unsigned(0 downto 0);
signal hold_rst : std_logic;
signal adc_data_hold : signed(15 downto 0);
signal adc_data_hold_en : std_logic;
signal min_or_max_en : std_logic;
attribute keep of adc_data_hold: signal is true;
-- adc samples difference registers signals
signal adc_sample_diff_1 : signed(16 downto 0);
signal adc_sample_diff_2 : signed(16 downto 0);
signal adc_sample_diff : signed(16 downto 0);
signal adc_sample_diff_en : std_logic;
-- adc samples difference absolute values registers
signal adc_sample_diff_abs_sig : signed(16 downto 0);
signal adc_sample_diff_abs_1_sig : signed(16 downto 0);
signal adc_sample_diff_abs_2_sig : signed(16 downto 0);
signal adc_sample_diff_abs_1 : std_logic_vector(15 downto 0);
signal adc_sample_diff_abs_2 : std_logic_vector(15 downto 0);
signal adc_sample_diff_abs : std_logic_vector(15 downto 0);
signal adc_sample_diff_abs_en : std_logic;
-- sum of absolute values of the samples diffrences
signal adc_sample_diffs_abs_sum : unsigned(16 downto 0);
-- ADC range signals
signal full_adc_range : signed(16 downto 0);
signal full_adc_range_abs_sig : signed(16 downto 0);
signal full_adc_range_abs : std_logic_vector(15 downto 0);
-- FDFC pulses flip-flops and edge detector signals
signal fdfc_neg_ff1 : std_logic;
signal fdfc_neg_ff2 : std_logic;
signal fdfd_pos_ff1 : std_logic;
signal fdfd_pos_ff2 : std_logic;
signal fdfc_neg_fedge : std_logic;
signal fdfc_pos_fedge : std_logic;
signal fdfc_any_fedge : std_logic;
attribute keep of fdfc_neg_fedge: signal is true;
attribute keep of fdfc_pos_fedge: signal is true;
attribute keep of fdfc_any_fedge: signal is true;
-- FDFC slope signals
signal fdfc_slope : std_logic;
signal fdfc_slope_ff1 : std_logic;
signal fdfc_slope_ff2 : std_logic;
signal fdfc_slope_change : std_logic;
signal fdfc_slope_1 : std_logic;
signal fdfc_slope_2 : std_logic;
attribute keep of fdfc_slope: signal is true;
attribute keep of fdfc_slope_1: signal is true;
attribute keep of fdfc_slope_2: signal is true;
-- FDFC pulses counter signals
signal fdfc_cnt : unsigned(3 downto 0);
signal fdfc_cnt_din : unsigned(3 downto 0);
signal fdfc_cnt_en : std_logic;
attribute keep of fdfc_cnt: signal is true;
signal zero_counts : std_logic;
-- FDFC counts register signals
signal fdfc_count_reg : unsigned(3 downto 0);
signal fdfc_count_reg_en : std_logic;
attribute keep of fdfc_count_reg: signal is true;
attribute keep of fdfc_count_reg_en: signal is true;
signal fdfc_count_reg_m1 : unsigned(3 downto 0);
-- ADC scan signals
signal scan_min_reg : signed(15 downto 0);
signal scan_max_reg : signed(15 downto 0);
signal adc_max_reg : signed(15 downto 0);
signal adc_min_reg : signed(15 downto 0);
attribute keep of adc_max_reg: signal is true;
attribute keep of adc_min_reg: signal is true;

-- multiplier signals
signal adc_range_mul_counts : unsigned(19 downto 0);
signal adc_range_mul_counts_en : std_logic;
signal busy_flg : std_logic;

-- data selection signals
signal data_combined_case_2_3 : std_logic_vector(19 downto 0);
signal data_combined_case_1 : std_logic_vector(19 downto 0);
signal data_combined : std_logic_vector(19 downto 0);
signal data_raw : std_logic_vector(19 downto 0);
signal data_mux : std_logic_vector(19 downto 0);

begin

-- assertions
-- check if the integration period requested by the generic is greater than the minimum ADC sampling period
assert t_integration_ns >= 100 report "Integration period (t_integration_ns) must be more or equal to 100 ns" severity error;
-- simulation assertions
-- build a testbench which verifies the following cases
assert not (p_cnt_done_d = '1' and fdfc_neg_fedge = '1') report "TB case 1: Integration period ends when there is the FDFC bottom switch. Verify fdfc_cnt and its influence on the output data" severity warning;
assert not (p_cnt_done_d = '1' and fdfc_pos_fedge = '1') report "TB case 2: Integration period ends when there is the FDFC top switch. Verify fdfc_cnt and its influence on the output data" severity warning;
assert not (adc_vld = '1' and fdfc_neg_fedge = '1') report "TB case 3: ADC data sample is ready when there is the FDFC bottom switch. Verify adc_data_hold register and its influence on the output data" severity warning;
assert not (adc_vld = '1' and fdfc_pos_fedge = '1') report "TB case 4: ADC data sample is ready when there is the FDFC top switch. Verify adc_data_hold register and its influence on the output data" severity warning;


-- FDFC negative pulses falling edge detector
fdfc_neg_ff_p: process(clk, nrst)
begin
	if nrst = '0' then
		fdfc_neg_ff1 <= '0';
		fdfc_neg_ff2 <= '0';
	elsif rising_edge(clk) then
		fdfc_neg_ff1 <= fdfc_neg;
		fdfc_neg_ff2 <= fdfc_neg_ff1;
	end if;
end process;
fdfc_neg_fedge <= not fdfc_neg_ff1 and fdfc_neg_ff2;

-- FDFC positive pulses falling edge detector
fdfc_pos_ff_p: process(clk, nrst)
begin
	if nrst = '0' then
		fdfd_pos_ff1 <= '0';
		fdfd_pos_ff2 <= '0';
	elsif rising_edge(clk) then
		fdfd_pos_ff1 <= fdfc_pos;
		fdfd_pos_ff2 <= fdfd_pos_ff1;
	end if;
end process;
fdfc_pos_fedge <= not fdfd_pos_ff1 and fdfd_pos_ff2;

-- sum neg and pos FDFC edge detectors
fdfc_any_fedge <= fdfc_neg_fedge or fdfc_pos_fedge;


-- FDFC counter
-- TODO: consider using recommended altera style for a counter with synchronous load (set?) and clear
fdfc_cnt_p: process(clk, nrst)
begin
	if nrst = '0' then
		fdfc_cnt <= (others => '0');
	elsif rising_edge(clk) then
--		if p_cnt_done_d = '1' and fdfc_any_fedge = '0' then
--			fdfc_cnt <= (others => '0');
--		elsif p_cnt_done_d = '0' and fdfc_any_fedge = '1' then
--			fdfc_cnt <= fdfc_cnt + 1;
--		elsif p_cnt_done_d = '1' and fdfc_any_fedge = '1' then
--			fdfc_cnt <= "0001";
--		end if;
		if fdfc_cnt_en = '1' then
			fdfc_cnt <= fdfc_cnt_din;
		end if;
	end if;
end process;

fdfc_cnt_en <= p_cnt_done_d or fdfc_slope_change;
fdfc_cnt_din <= "0000" when p_cnt_done_d = '1' and fdfc_slope_change = '0' else
				"0001" when p_cnt_done_d = '1' and fdfc_slope_change = '1' else
				fdfc_cnt + 1;
--fdfc_cnt_en <= p_cnt_done_d or fdfc_any_fedge;
--fdfc_cnt_din <= "0000" when p_cnt_done_d = '1' and fdfc_any_fedge = '0' else
--				"0001" when p_cnt_done_d = '1' and fdfc_any_fedge = '1' else
--				fdfc_cnt + 1;


-- FDFC signal slope flag
-- '1' - signal is rising
-- '0' - signal is falling
-- TODO: data output shouldn't be valid until there is first top or bottom pulse and the fdfc_slope is initialized correctly
fdfc_slope_p: process(clk)
begin
	if rising_edge(clk) then
		if fdfc_pos_fedge = '1' or fdfc_neg_fedge = '1' then
			fdfc_slope_ff1 <= fdfc_neg_fedge;
		end if;
		fdfc_slope_ff2 <= fdfc_slope_ff1;
	end if;
end process;
fdfc_slope_change <= (fdfc_slope_ff1 and not fdfc_slope_ff2) or (not fdfc_slope_ff1 and fdfc_slope_ff2);
--fdfc_slope <= fdfc_slope_ff1;
fdfc_slope <= fdfc_slope_ff2;

--fdfc_slope_p: process(clk)
--begin
--	if rising_edge(clk) then
--		if fdfc_pos_fedge = '1' then
--			fdfc_slope <= '0';
--		elsif fdfc_neg_fedge = '1' then
--			fdfc_slope <= '1';
--		end if;
--	end if;
--end process;



-- measurement period free running counter
per_cnt_p: process(clk, nrst)
begin
	if nrst = '0' then
		p_cnt <= 0;
	elsif rising_edge(clk) then
		if p_cnt_done = '1' then
			p_cnt <= 0;
		else
			p_cnt <= p_cnt + 1;
		end if;
	end if;
end process;
p_cnt_done <= '1' when p_cnt = p_cnt_max else '0';
--ADC is started in advance to the storage (FDFC data)
adc_run <= p_cnt_done;
adc_next <= '1';

--delay p_cnt_done used for the FDFC storage
store_en_p: process(clk, nrst)
begin
	if nrst = '0' then
		p_cnt_done_d1 <= '0';
		p_cnt_done_d2 <= '0';
		p_cnt_done_d3 <= '0';
	elsif rising_edge(clk) then		
		p_cnt_done_d1 <= p_cnt_done;
		p_cnt_done_d2 <= p_cnt_done_d1;
		p_cnt_done_d3 <= p_cnt_done_d2;
	end if;
end process;
p_cnt_done_d <=	p_cnt_done_d3;
period_pulse <= p_cnt_done_d;

-- Hold minumum ADC value when on falling slope
-- Hold maximum ADC value when on rising slope
-- Reset when switching from rising to falling slope
-- Reset when switching from falling to rising slope
-- As a reset value take the ADC sample after switching the slope
-- Possibility to increase number of samples to be taken without 
-- min/max holding by changing hold_rst_cnt maximum count value

hold_rst_cnt_p: process(clk, nrst)
begin
	if nrst = '0' then
		hold_rst_cnt <= (others => '0');
	elsif rising_edge(clk) then
		if fdfc_any_fedge = '1' then
			hold_rst_cnt <= (others => '0');
		elsif adc_vld = '1' and hold_rst = '1' then
			hold_rst_cnt <= hold_rst_cnt + 1;
		end if;
	end if;
end process;

hold_rst <= '0' when hold_rst_cnt = 1 else '1';

adc_sample_hold_p: process(clk, nrst)
begin
	if nrst = '0' then
		adc_data_hold <= (others => '0');
	elsif rising_edge(clk) then
		if adc_vld = '1' and (min_or_max_en = '1' or hold_rst = '1') then
			adc_data_hold <= signed(adc_data);
		end if;
		
		adc_data_hold_en <= adc_vld;
		
	end if;
end process;

min_or_max_en <= '1' when (signed(adc_data) < adc_data_hold and fdfc_slope = '0') or (signed(adc_data) > adc_data_hold and fdfc_slope = '1') else '0';


-- ADC samples registers
-- stores data when the sample is ready
-- p_cnt_done (end/beginning of the measurement period) starts the ADC 
-- but the sample is ready when adc_vld arrive (after ADC processing and TX is finished)
-- adc_valid is delayed by the adc_data_hold register (adc_data_hold_en)
adc_sample_p: process(clk, nrst)
begin
	if nrst = '0' then
		adc_sample_2 <= (others => '0');
		adc_sample_1 <= (others => '0');
		adc_sample_h_2 <= (others => '0');
		adc_sample_h_1 <= (others => '0');
	elsif rising_edge(clk) then
		if adc_data_hold_en = '1' then
			adc_sample_h_2 <= adc_data_hold;
			adc_sample_h_1 <= adc_sample_h_2;
		end if;
		if adc_vld = '1' then
			adc_sample_2 <= signed(adc_data);
			adc_sample_1 <= adc_sample_2;
		end if;
		adc_sample_en <= adc_data_hold_en;
	end if;
end process;


-- FDFC counters registers
-- store the counters when the measurement period is finished
fdfc_regs_p: process(clk, nrst)
begin
	if nrst = '0' then
		fdfc_count_reg <= (others => '0');
	elsif rising_edge(clk) then
		if p_cnt_done_d = '1' then
			-- numbers of positive and negative counts
			fdfc_count_reg <= fdfc_cnt;
			
			-- edge flags of each ADC ample (at the beginning and at the end of the measurement period)
			fdfc_slope_2 <= fdfc_slope;
			fdfc_slope_1 <= fdfc_slope_2;
		end if;
		fdfc_count_reg_en <= p_cnt_done_d;
	end if;
end process;


-- optional ADC effective range scan
dynamic_ADC_range: if ADC_dynamic_range = true generate
		
	-- scan min ADC value
	-- reset every refresh time
	scan_adc_min_p: process(clk)
	begin
		if rising_edge(clk) then
			if adc_range_reset = '1' or nrst = '0' then
				scan_min_reg <= "0111111111111111";
			elsif adc_vld = '1' and scan_min_reg > signed(adc_data) then
				scan_min_reg <= signed(adc_data);
			end if;
		end if;
	end process;
	
	-- store last refresh min value 
	adc_min_reg_p: process(clk)
	begin
		if rising_edge(clk) then
			if nrst = '0' then
				adc_min_reg <= "1000000000000000";
			elsif adc_range_reset = '1' then
				adc_min_reg <= scan_min_reg;
			end if;
		end if;
	end process;
	
	-- scan max ADC value
	-- reset every refresh time
	scan_adc_max_p: process(clk)
	begin
		if rising_edge(clk) then
			if adc_range_reset = '1' or nrst = '0' then
				scan_max_reg <= "1000000000000000";
			elsif adc_vld = '1' and scan_max_reg < signed(adc_data) then
				scan_max_reg <= signed(adc_data);
			end if;
		end if;
	end process;
	
	-- store last refresh max value 
	adc_max_reg_p: process(clk)
	begin
		if rising_edge(clk) then
			if nrst = '0' then
				adc_max_reg <= "0111111111111111";
			elsif adc_range_reset = '1' then
				adc_max_reg <= scan_max_reg;
			end if;
		end if;
	end process;
	
end generate;

fixed_ADC_range: if ADC_dynamic_range = false generate
	
	adc_range_reg_p: process(clk)
	begin
		if rising_edge(clk) then
			--adc_max_reg <= adc_max_val;
			adc_max_reg <= signed(adc_fixed_range_max);
			--adc_min_reg <= adc_min_val;
			adc_min_reg <= signed(adc_fixed_range_min);
		end if;
	end process;

end generate;

adc_max <= std_logic_vector(adc_max_reg);
adc_min <= std_logic_vector(adc_min_reg);


-- difference registers
diff_regs_p: process(clk)
begin
	if rising_edge(clk) then
	
		-- case 1 (sum of fdfc counts = 0)
		-- 2 samples difference
		adc_sample_diff <= resize(adc_sample_h_1, 17) - resize(adc_sample_h_2, 17);
		
		-- case 2,3 (counts >= 1)
		-- two differences:
		-- sample1 - (min or max)
		-- (min or max) - sample2
		-- min or max is selected depending on the signal's slope where the sample was taken
		if fdfc_slope_1 = '1' then
			adc_sample_diff_1 <= resize(adc_sample_1, 17) - resize(adc_max_reg, 17);
		else
			adc_sample_diff_1 <= resize(adc_sample_1, 17) - resize(adc_min_reg, 17);
		end if;
	
		if fdfc_slope_2 = '1' then
			adc_sample_diff_2 <= resize(adc_min_reg, 17) - resize(adc_sample_2, 17);
		else
			adc_sample_diff_2 <= resize(adc_max_reg, 17) - resize(adc_sample_2, 17);
		end if;
		
		-- data strobe (all cases)
		adc_sample_diff_en <= adc_sample_en;
		
		-- full ADC range difference
		-- for both fixed or dynamic range versions
		full_adc_range <= resize(adc_max_reg, 17) - resize(adc_min_reg, 17);
		
	end if;
end process;
	
-- difference absolute value conversion
adc_sample_diff_abs_sig <= abs(adc_sample_diff);
adc_sample_diff_abs_1_sig <= abs(adc_sample_diff_1);
adc_sample_diff_abs_2_sig <= abs(adc_sample_diff_2);
full_adc_range_abs_sig <= abs(full_adc_range);

-- register absolute value without sign bit (always '0': positive)
diff_abs_regs_p: process(clk)
begin
	if rising_edge(clk) then
	
		-- case 1 (counts == 0)
		adc_sample_diff_abs <= std_logic_vector(adc_sample_diff_abs_sig(15 downto 0));
		
		-- case 2,3 (counts >= 1)
		adc_sample_diff_abs_1 <= std_logic_vector(adc_sample_diff_abs_1_sig(15 downto 0));
		adc_sample_diff_abs_2 <= std_logic_vector(adc_sample_diff_abs_2_sig(15 downto 0));
		
		-- data strobe (all cases)
		adc_sample_diff_abs_en <= adc_sample_diff_en;
		
		-- ADC full range absolute value
		full_adc_range_abs <= std_logic_vector(full_adc_range_abs_sig(15 downto 0));
		
	end if;
end process;

u1: multiplier
generic map (
	RESET_ACTIVE => '0',
	OP1_LEN => 16,
	OP2_LEN => 4
)
port map (
	clk => clk,
	rst	=> nrst,
	-- input signals
	op1			=> unsigned(full_adc_range_abs),
	op2			=> fdfc_count_reg_m1,
	multiply	=> fdfc_count_reg_en,
	-- output_signals
	result		=> adc_range_mul_counts,
	result_en	=> adc_range_mul_counts_en,
	busy_flg	=> busy_flg
);
fdfc_count_reg_m1 <= fdfc_count_reg - 1;

-- raw data is always sum of pos and neg counts at MSB and last ADC sample at LSB
-- use MSB of the sum of counts to transmit slope bit
data_raw <= fdfc_slope_2 & std_logic_vector(fdfc_count_reg(2 downto 0)) & std_logic_vector(adc_sample_2);

-- case 1 or case 2,3 selector
zero_counts <= '1' when fdfc_count_reg = 0 else '0';

-- case 1 calculations (zero counts + difference of 2 samples)
data_combined_case_1 <= "0000" & adc_sample_diff_abs;

-- sum of 2 differences of 2 samples for case 2 and 3
adc_sample_diffs_abs_sum <= unsigned('0' & adc_sample_diff_abs_1) + unsigned('0' & adc_sample_diff_abs_2);

-- case 2,3 calculation (1 or more counts - 1 + 2 differences of 2 samples)
data_combined_case_2_3 <= std_logic_vector(adc_range_mul_counts + adc_sample_diffs_abs_sum);

-- select sdata source depending on the current case (1 or 2,3)
data_combined <= data_combined_case_1 when zero_counts = '1' else data_combined_case_2_3;

--saturation bit when number of counts reach maximum
--this condition is detected without waiting to the end of the measurement period
--TODO: consider stopping the calculation and invalidating the output as soon as the saturation is detected
saturation <= '1' when fdfc_cnt = 2**fdfc_cnt'length-1 else '0';

-- raw/processed data selector
data_mux <= data_raw when raw_data = '1' else data_combined;

-- output register
result_reg_p: process(clk)
begin
	if rising_edge(clk) then
		if adc_sample_diff_abs_en = '1' then
			data <= data_mux;
			data_debug <= data_raw;
		end if;
		data_en <= adc_sample_diff_abs_en;
	end if;
end process;

end rtl;

