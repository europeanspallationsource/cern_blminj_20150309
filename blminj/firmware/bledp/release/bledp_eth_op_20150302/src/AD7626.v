// -----------------------------------------------------------------------------
//
// Copyright 2011(c) Analog Devices, Inc.
//
// All rights reserved.
//
// Redistribution and use in source and binary forms, with or without modification,
// are permitted provided that the following conditions are met:
//  - Redistributions of source code must retain the above copyright
//    notice, this list of conditions and the following disclaimer.
//  - Redistributions in binary form must reproduce the above copyright
//    notice, this list of conditions and the following disclaimer in
//    the documentation and/or other materials provided with the
//    distribution.
//  - Neither the name of Analog Devices, Inc. nor the names of its
//    contributors may be used to endorse or promote products derived
//    from this software without specific prior written permission.
//  - The use of this software may or may not infringe the patent rights
//    of one or more patent holders.  This license does not release you
//    from the requirement that you obtain separate licenses from these
//    patent holders to use this software.
//  - Use of the software either in source or binary form, must be run
//    on or directly connected to an Analog Devices Inc. component.
//
// THIS SOFTWARE IS PROVIDED BY ANALOG DEVICES "AS IS" AND ANY EXPRESS OR IMPLIED
// WARRANTIES, INCLUDING, BUT NOT LIMITED TO, NON-INFRINGEMENT, MERCHANTABILITY
// AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
// IN NO EVENT SHALL ANALOG DEVICES BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
// SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
// INTELLECTUAL PROPERTY RIGHTS, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
// LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
// ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
// (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
//
// -----------------------------------------------------------------------------
// FILE NAME : AD7626.v
// MODULE NAME : AD7626
// AUTHOR :
// AUTHOR�S EMAIL :
// -----------------------------------------------------------------------------
// SVN REVISION: $WCREV$
// -----------------------------------------------------------------------------
// KEYWORDS : ad7626
// -----------------------------------------------------------------------------
// PURPOSE : Module used to read data from the AD7626 and resync the data to
// the FPGA clock and send it to an upper module on a 16 bit bus
// -----------------------------------------------------------------------------
// REUSE ISSUES
// Reset Strategy      :
// Clock Domains       : clk_i 250MHz
//                       dco_i 250MHz, from the ADC
// Critical Timing     :
// Test Features       :
// Asynchronous I/F    :
// Instantiations      :
// Synthesizable (y/n) : y
// Target Device       :
// Other               :
// -----------------------------------------------------------------------------

`timescale 1 ns / 100 ps

//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------
module AD7626 # (
	parameter t_clk_period_ns = 4
)
(
    input           clk_i,     // 250MHz Clock phase 0
    input           adc_cnv_start_i,// ADC start of conversion
    input           dco_i,          // ECHOED CLK from ADC
    input           d_i,            // SERIAL data from ADC

    output          cnv_o,          // CNV to ADC
    output          clk_o,          // Serial Clk to ADC
    output  [15:0]  par_data_o,     // Parallel data out
    output          par_data_rdy_o  // Data ready flag out
    );

//------------------------------------------------------------------------------
//----------- Local Parameters -------------------------------------------------
//------------------------------------------------------------------------------
localparam   QUIET    = 2'b00;
localparam   CLOCKING = 2'b01;
localparam   SYNC     = 2'b11;
localparam   DATAFLAG = 2'b10;

localparam   TMSB    = 5'd20;//20+3 ( 4ns clks ) 92ns for AD7626
                            //30+3 ( 134ns for 7625 )
localparam   WAIT    = 2'b00;
localparam   COUNT   = 2'b01;
localparam   EXPIRE  = 2'b10;
localparam   HOLD    = 2'b11;

localparam   TCNVH   = 25/t_clk_period_ns;


//------------------------------------------------------------------------------
//----------- Registers Declarations -------------------------------------------
//------------------------------------------------------------------------------
reg [15:0]  echo_dadc;
reg [15:0]  echo_data;
reg [15:0]  sync_data;
reg        echo_rdy_s;
reg        sync_en_s;

reg         clk_en = 1'b0;
reg [1:0]   clk_state = QUIET;
reg [4:0]   clk_count = 5'd0;
reg [2:0]   sync_count;

reg [4:0]   busy_count;
reg [1:0]   timer_state = WAIT;
reg  rdy_o;

//integer tcnvh_count = 0;
reg [4:0]   tcnvh_count = 0;

//------------------------------------------------------------------------------
//----------- Wires Declarations -----------------------------------------------
//------------------------------------------------------------------------------
wire [4:0]  num_clks_s;
wire [2:0]  sync_width_s;

wire tcnvh_count_done;

//------------------------------------------------------------------------------
//----------- Assign/Always Blocks ---------------------------------------------
//------------------------------------------------------------------------------
assign par_data_o       = echo_data;
assign par_data_rdy_o   = echo_rdy_s;
assign num_clks_s   = 5'd15;    // 16 Clks for Echo mode
assign sync_width_s = 3'd5;      //Sync pulse for Echo mode

/*************************************************************/
/**********       Serial Clock Generation       **************/
/*************************************************************/
			  
/*** Generate Serial CLK to ADC ***/
//always @ (negedge clk_i or negedge reset_n_i)
always @ (negedge clk_i)
begin
/*
    if( ~reset_n_i )
    begin
        clk_en    <= 1'b0;                      // Stop Clks
        clk_count <= 5'd0;                      // Re-load Counter
        clk_state <= QUIET;
    end
    else 
    begin
*/
        case (clk_state)
            QUIET:                              // NO CLK
            begin
                clk_count   <= num_clks_s;      // Re-load Counter
                sync_count  <= sync_width_s;
                clk_en      <= 0;
                sync_en_s   <= 1'b0;
                echo_rdy_s  <= 1'b0;
                if (rdy_o)
                begin
                    clk_state <= CLOCKING;      // Begin Clocking
                end
                else
                begin
                    clk_state <= QUIET;         // Wait for adc_rdy_i
                end
            end
            CLOCKING:
            begin
                clk_en      <= 1'b1;
                sync_en_s   <= 1'b0;
                echo_rdy_s  <= 1'b0;
                if (clk_count == 5'd0)          // Stop Clks
                begin
                    clk_count   <= num_clks_s;
                    clk_state   <= SYNC;        // Re-load Counter
                end
                else                            // Continue Clks
                begin
                    clk_count   <= clk_count - 5'd1; // Decrement Counter
                    clk_state   <= CLOCKING;
                end
            end
            SYNC:
            begin
                clk_en      <= 0;
                sync_en_s   <= 1'b1;
                echo_rdy_s  <= 1'b0;
                if (sync_count == 3'd0)
                begin
                    sync_count   <= sync_width_s;
                    clk_state    <= DATAFLAG;
                end
                else
                begin
                    sync_count   <= sync_count - 3'd1;
                    clk_state    <= SYNC;
                end
            end
            DATAFLAG:
            begin
                clk_en      <= 0;
                sync_en_s   <= 1'b0;
                echo_rdy_s  <= 1'b1;
                clk_state   <= QUIET;
            end
        endcase
    //end
end

assign clk_o = clk_en & clk_i;         // CLK is gated from CLK_250

//------------------------------------------------------------------------------
//----------- TMSB counter (data ready for readout counter) --------------------
//------------------------------------------------------------------------------
always @ (posedge clk_i)
begin
    case(timer_state)
        WAIT:
        begin
            rdy_o       <= 1'b0;
            busy_count  <= TMSB;
            if (~adc_cnv_start_i)
            begin
                timer_state <= WAIT;
            end
            else
            begin
                timer_state <= COUNT;
            end
        end
        COUNT:
        begin
            rdy_o       <= 1'b0;
            busy_count  <= busy_count - 5'd1;
            if (busy_count == 5'd0)
            begin
                timer_state <= EXPIRE;
            end
            else
            begin
                timer_state <= COUNT;
            end
        end
        EXPIRE:
        begin
            rdy_o       <= 1'b1;
            busy_count  <= TMSB;
            timer_state <= HOLD;
        end
        HOLD:
        begin
            rdy_o       <= 1'b1;
            busy_count  <= TMSB;
            timer_state <= WAIT;
        end
    endcase
end

//------------------------------------------------------------------------------
//----------- Tcnvh counter ----------------------------------------------------
//------------------------------------------------------------------------------
always @(posedge clk_i)
begin
	if (adc_cnv_start_i)
		tcnvh_count <= TCNVH;
	else if (~tcnvh_count_done)
		tcnvh_count <= tcnvh_count - 5'd1;
end
assign tcnvh_count_done = (tcnvh_count == 0? 1'b1 : 1'b0);
assign cnv_o = ~tcnvh_count_done;

/*************************************************************/
/**********        ECHO-CLOCK OPERATION         **************/
/*************************************************************/

/******* Echo-mode Shift Register *******/
always @ (posedge dco_i)
begin                                   // Clock data on rising edge of DCO
   echo_dadc <= {echo_dadc[14:0],d_i};  // Shift and Concatenate Data
end


/** Optional for Syncronous Design: Re-sync data to FPGA clock **/
always @ (posedge clk_i)
begin                           // Any syncronous FPGA clk
   if (sync_en_s)
   begin
      sync_data <= echo_dadc;   // Cascaded registers recommended
      echo_data <= sync_data;   // to minimize metastable output
   end
end

endmodule
