// Company:		CERN - BE/BI/BL
// Engineer:	Maciej Kwiatkowski
// Create Date:	13/06/2012
// Module Name:	cross_clk_synch
// Description:	module to transfer synchronous data strobe (e.g. data request, data ready) between two clock domains


module cross_clk_synch (
	input	clk_1,
	input	clk_2,
	input	clk_1_strobe,
	output	clk_2_strobe
);

reg clk_1_toggle = 0;
reg clk_2_ff_1 = 0;
reg clk_2_ff_2 = 0;

//toggle bit at every strobe in clk_1 domain
always @(posedge clk_1)
begin
	if (clk_1_strobe)
		clk_1_toggle <= ~clk_1_toggle;
end

//clk_1_toggle routed via two flip flops in clk_2 domain
always @(posedge clk_2)
begin
	clk_2_ff_1 <= clk_1_toggle;
	clk_2_ff_2 <= clk_2_ff_1;
end

//detect rising or falling edge of clk_1_toggle
assign clk_2_strobe = (clk_2_ff_1 & ~clk_2_ff_2) | (~clk_2_ff_1 & clk_2_ff_2);

endmodule
