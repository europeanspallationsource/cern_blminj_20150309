------------------------------------------------------------------
--Design Units : 	The multiplexer writes data from selected channel (source) to external memory (destination) depending on the data availability
--					The FSM impelented shouldn't ignore any of the input channels when data is available constantly on the other channel
--					This could lead to the external FIFO overflow
--					Source write clock frequency should be 9 times lower to avoid eternal FIFO overflow @ maximum write clock (9 sources writes to 1 destination)
--					FIFO overflow condition should be monitored by the external logic!!!
--
--
--Author: 			Maciej Kwiatkowski
--		  	 		European Organisation for Nuclear Research
--		 	 		BE-BI-BL
--		 	 		CERN, Geneva, Switzerland,  CH-1211
--		 	 		865/R-A01
--
--Simulator:		Modelsim
------------------------------------------------------------------
--Vsn 	Author	Date			Changes
--
--0.1	MK		12.04.2012		First version
------------------------------------------------------------------

LIBRARY IEEE;
USE IEEE.STD_Logic_1164.ALL;
USE IEEE.Numeric_STD.ALL;

entity channel_mux is
	generic (
		ADDR_NBITS 	: natural := 13;			-- number of address bits
		TXBUFF_SIZE	: natural := 1400;			-- TX buffer size [bytes]
		RESET_ACTIVE: std_logic	:= '0'			-- polarity of the reset
		);
	port (
		-- global signals
		rst		: in std_logic; 				-- global reset
		clk		: in std_logic; 				-- global clock
		
		-- source read signals
   		ch0_data		: in std_logic_vector(31 downto 0); 	-- data in
		ch0_data_avail	: in std_logic; 								-- data available flag
		ch0_data_rd		: out std_logic; 								-- data read request
		ch1_data		: in std_logic_vector(31 downto 0); 	-- data in
		ch1_data_avail	: in std_logic; 								-- data available flag
		ch1_data_rd		: out std_logic; 								-- data read request
		ch2_data		: in std_logic_vector(31 downto 0); 	-- data in
		ch2_data_avail	: in std_logic; 								-- data available flag
		ch2_data_rd		: out std_logic; 								-- data read request
		ch3_data		: in std_logic_vector(31 downto 0); 	-- data in
		ch3_data_avail	: in std_logic; 								-- data available flag
		ch3_data_rd		: out std_logic; 								-- data read request
		ch4_data		: in std_logic_vector(31 downto 0); 	-- data in
		ch4_data_avail	: in std_logic; 								-- data available flag
		ch4_data_rd		: out std_logic; 								-- data read request
		ch5_data		: in std_logic_vector(31 downto 0); 	-- data in
		ch5_data_avail	: in std_logic; 								-- data available flag
		ch5_data_rd		: out std_logic; 								-- data read request
		ch6_data		: in std_logic_vector(31 downto 0); 	-- data in
		ch6_data_avail	: in std_logic; 								-- data available flag
		ch6_data_rd		: out std_logic; 								-- data read request
		ch7_data		: in std_logic_vector(31 downto 0); 	-- data in
		ch7_data_avail	: in std_logic; 								-- data available flag
		ch7_data_rd		: out std_logic; 								-- data read request
		
		-- destination write signals
		out_data		: out std_logic_vector(31 downto 0); 			-- data out
		out_data_wr		: out std_logic; 								-- data write request
		out_chksum_wr	: out std_logic;								-- check sum write request
		out_address		: out std_logic_vector(ADDR_NBITS-1 downto 0);	-- address
		
		-- buffer ready signals
		buff_rdy		: out std_logic;								-- buffer ready strobe
		buff_last_addr	: out std_logic_vector(ADDR_NBITS-1 downto 0)	-- address of the end of the last buffer
		
		);
end channel_mux;

------------------------------------------------------------------
architecture rtl of channel_mux is 
------------------------------------------------------------------

--log2 function to calculate required bit number for generic values
function log2 (x : positive) return natural is 
begin
  if x <= 1 then
    return 1;
  else
    return log2 (x / 2) + 1;
  end if;
end function log2;

--FSM signals
type CntStateType is (idle, channel0, channel1, channel2, channel3, channel4, channel5, channel6, channel7);
signal cnt_state, next_cnt_state : CntStateType;

--Channel selector
type SelType is (ch0, ch1, ch2, ch3, ch4, ch5, ch6, ch7, none);
signal sel, sel_d : SelType;

signal sig_ch0_data_rd : std_logic;
signal sig_ch1_data_rd : std_logic;
signal sig_ch2_data_rd : std_logic;
signal sig_ch3_data_rd : std_logic;
signal sig_ch4_data_rd : std_logic;
signal sig_ch5_data_rd : std_logic;
signal sig_ch6_data_rd : std_logic;
signal sig_ch7_data_rd : std_logic;

signal sig_out_data : std_logic_vector(31 downto 0);
signal sig_out_data_wr : std_logic;

signal sum_acc : unsigned(31 downto 0);

constant num_of_txbuffs : natural := 2**ADDR_NBITS/((TXBUFF_SIZE/4)+1);
constant max_address : natural := (num_of_txbuffs * ((TXBUFF_SIZE/4)+1))-1;
signal addr_cnt : unsigned(ADDR_NBITS-1 downto 0);

constant TXBUFF_NBITS : natural := log2(TXBUFF_SIZE/4);
signal txbuff_cnt : unsigned(TXBUFF_NBITS-1 downto 0);
signal txbuff_cnt_done : std_logic;
signal txbuff_cnt_done_en : std_logic;
signal txbuff_cnt_done_d_en : std_logic;
signal txbuff_cnt_done_d : std_logic;
signal txbuff_cnt_done_d2 : std_logic;


signal any_data_rd_en : std_logic;



begin

------------------------------------------------------------------
--	assertions to sanitize the generics
------------------------------------------------------------------
assert (TXBUFF_SIZE mod 4) = 0 report "TX buffer size must be multiple of 4 bytes" severity failure;
--check if tx buffsize + checksum size will fit into the memory
assert ((TXBUFF_SIZE/4)+1) <= 2**ADDR_NBITS report "Memory range is too small to fit " & integer'image(TXBUFF_SIZE) & " bytes of the TX buffer and 4 bytes of the checksum" severity failure;
assert TXBUFF_SIZE < 1460 report "TX buffer size of " & integer'image(TXBUFF_SIZE) & " bytes exceeds the Maximum Segment Size" severity warning;

------------------------------------------------------------------
--	counter FSM
--  it counts using numbers of available inputs
--  generates selection signal which drives data output multiplexer
------------------------------------------------------------------
fsm_cnt_cnt_p: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		cnt_state <= idle;
	elsif rising_edge(clk) then
		if txbuff_cnt_done_en = '0' then
			cnt_state <= next_cnt_state;
		end if;
	end if;
end process;

fsm_cnt_cl_p: process(cnt_state, ch0_data_avail, ch1_data_avail, ch2_data_avail, ch3_data_avail, ch4_data_avail, ch5_data_avail, ch6_data_avail, ch7_data_avail)
begin
	next_cnt_state <= cnt_state;
	sel <= ch0;
	
	case cnt_state is
		
		when idle =>
			sel <= none;
		
			if ch0_data_avail = '1' then
				next_cnt_state <= channel0;
			elsif ch1_data_avail = '1' then
				next_cnt_state <= channel1;
			elsif ch2_data_avail = '1' then
				next_cnt_state <= channel2;
			elsif ch3_data_avail = '1' then
				next_cnt_state <= channel3;
			elsif ch4_data_avail = '1' then
				next_cnt_state <= channel4;
			elsif ch5_data_avail = '1' then
				next_cnt_state <= channel5;
			elsif ch6_data_avail = '1' then
				next_cnt_state <= channel6;
			elsif ch7_data_avail = '1' then
				next_cnt_state <= channel7;
			else
				next_cnt_state <= idle;
			end if;

		when channel0 =>
			sel <= ch0;
			
			if ch1_data_avail = '1' then
				next_cnt_state <= channel1;
			elsif ch2_data_avail = '1' then
				next_cnt_state <= channel2;
			elsif ch3_data_avail = '1' then
				next_cnt_state <= channel3;
			elsif ch4_data_avail = '1' then
				next_cnt_state <= channel4;
			elsif ch5_data_avail = '1' then
				next_cnt_state <= channel5;
			elsif ch6_data_avail = '1' then
				next_cnt_state <= channel6;
			elsif ch7_data_avail = '1' then
				next_cnt_state <= channel7;
			elsif ch0_data_avail = '1' then
				next_cnt_state <= channel0;
			else
				next_cnt_state <= idle;
			end if;
		
		when channel1 =>
			sel <= ch1;
			
			if ch2_data_avail = '1' then
				next_cnt_state <= channel2;
			elsif ch3_data_avail = '1' then
				next_cnt_state <= channel3;
			elsif ch4_data_avail = '1' then
				next_cnt_state <= channel4;
			elsif ch5_data_avail = '1' then
				next_cnt_state <= channel5;
			elsif ch6_data_avail = '1' then
				next_cnt_state <= channel6;
			elsif ch7_data_avail = '1' then
				next_cnt_state <= channel7;
			elsif ch0_data_avail = '1' then
				next_cnt_state <= channel0;
			elsif ch1_data_avail = '1' then
				next_cnt_state <= channel1;
			else
				next_cnt_state <= idle;
			end if;
		
		when channel2 =>
			sel <= ch2;
			
			if ch3_data_avail = '1' then
				next_cnt_state <= channel3;
			elsif ch4_data_avail = '1' then
				next_cnt_state <= channel4;
			elsif ch5_data_avail = '1' then
				next_cnt_state <= channel5;
			elsif ch6_data_avail = '1' then
				next_cnt_state <= channel6;
			elsif ch7_data_avail = '1' then
				next_cnt_state <= channel7;
			elsif ch0_data_avail = '1' then
				next_cnt_state <= channel0;
			elsif ch1_data_avail = '1' then
				next_cnt_state <= channel1;
			elsif ch2_data_avail = '1' then
				next_cnt_state <= channel2;
			else
				next_cnt_state <= idle;
			end if;
		
		when channel3 =>
			sel <= ch3;
			
			if ch4_data_avail = '1' then
				next_cnt_state <= channel4;
			elsif ch5_data_avail = '1' then
				next_cnt_state <= channel5;
			elsif ch6_data_avail = '1' then
				next_cnt_state <= channel6;
			elsif ch7_data_avail = '1' then
				next_cnt_state <= channel7;
			elsif ch0_data_avail = '1' then
				next_cnt_state <= channel0;
			elsif ch1_data_avail = '1' then
				next_cnt_state <= channel1;
			elsif ch2_data_avail = '1' then
				next_cnt_state <= channel2;
			elsif ch3_data_avail = '1' then
				next_cnt_state <= channel3;
			else
				next_cnt_state <= idle;
			end if;
		
		when channel4 =>
			sel <= ch4;
			
			if ch5_data_avail = '1' then
				next_cnt_state <= channel5;
			elsif ch6_data_avail = '1' then
				next_cnt_state <= channel6;
			elsif ch7_data_avail = '1' then
				next_cnt_state <= channel7;
			elsif ch0_data_avail = '1' then
				next_cnt_state <= channel0;
			elsif ch1_data_avail = '1' then
				next_cnt_state <= channel1;
			elsif ch2_data_avail = '1' then
				next_cnt_state <= channel2;
			elsif ch3_data_avail = '1' then
				next_cnt_state <= channel3;
			elsif ch4_data_avail = '1' then
				next_cnt_state <= channel4;
			else
				next_cnt_state <= idle;
			end if;
		
		when channel5 =>
			sel <= ch5;
			
			if ch6_data_avail = '1' then
				next_cnt_state <= channel6;
			elsif ch7_data_avail = '1' then
				next_cnt_state <= channel7;
			elsif ch0_data_avail = '1' then
				next_cnt_state <= channel0;
			elsif ch1_data_avail = '1' then
				next_cnt_state <= channel1;
			elsif ch2_data_avail = '1' then
				next_cnt_state <= channel2;
			elsif ch3_data_avail = '1' then
				next_cnt_state <= channel3;
			elsif ch4_data_avail = '1' then
				next_cnt_state <= channel4;
			elsif ch5_data_avail = '1' then
				next_cnt_state <= channel5;
			else
				next_cnt_state <= idle;
			end if;
		
		when channel6 =>
			sel <= ch6;
			
			if ch7_data_avail = '1' then
				next_cnt_state <= channel7;
			elsif ch0_data_avail = '1' then
				next_cnt_state <= channel0;
			elsif ch1_data_avail = '1' then
				next_cnt_state <= channel1;
			elsif ch2_data_avail = '1' then
				next_cnt_state <= channel2;
			elsif ch3_data_avail = '1' then
				next_cnt_state <= channel3;
			elsif ch4_data_avail = '1' then
				next_cnt_state <= channel4;
			elsif ch5_data_avail = '1' then
				next_cnt_state <= channel5;
			elsif ch6_data_avail = '1' then
				next_cnt_state <= channel6;
			else
				next_cnt_state <= idle;
			end if;
		
		when channel7 =>
			sel <= ch7;
			
			if ch0_data_avail = '1' then
				next_cnt_state <= channel0;
			elsif ch1_data_avail = '1' then
				next_cnt_state <= channel1;
			elsif ch2_data_avail = '1' then
				next_cnt_state <= channel2;
			elsif ch3_data_avail = '1' then
				next_cnt_state <= channel3;
			elsif ch4_data_avail = '1' then
				next_cnt_state <= channel4;
			elsif ch5_data_avail = '1' then
				next_cnt_state <= channel5;
			elsif ch6_data_avail = '1' then
				next_cnt_state <= channel6;
			elsif ch7_data_avail = '1' then
				next_cnt_state <= channel7;
			else
				next_cnt_state <= idle;
			end if;
			
	end case;
	
end process;


-- first generate read strobes for selected channel
sig_ch0_data_rd <= '1' when sel = ch0 and ch0_data_avail = '1' and txbuff_cnt_done_en = '0' else '0';
sig_ch1_data_rd <= '1' when sel = ch1 and ch1_data_avail = '1' and txbuff_cnt_done_en = '0' else '0';
sig_ch2_data_rd <= '1' when sel = ch2 and ch2_data_avail = '1' and txbuff_cnt_done_en = '0' else '0';
sig_ch3_data_rd <= '1' when sel = ch3 and ch3_data_avail = '1' and txbuff_cnt_done_en = '0' else '0';
sig_ch4_data_rd <= '1' when sel = ch4 and ch4_data_avail = '1' and txbuff_cnt_done_en = '0' else '0';
sig_ch5_data_rd <= '1' when sel = ch5 and ch5_data_avail = '1' and txbuff_cnt_done_en = '0' else '0';
sig_ch6_data_rd <= '1' when sel = ch6 and ch6_data_avail = '1' and txbuff_cnt_done_en = '0' else '0';
sig_ch7_data_rd <= '1' when sel = ch7 and ch7_data_avail = '1' and txbuff_cnt_done_en = '0' else '0';


ch0_data_rd <= sig_ch0_data_rd;
ch1_data_rd <= sig_ch1_data_rd;
ch2_data_rd <= sig_ch2_data_rd;
ch3_data_rd <= sig_ch3_data_rd;
ch4_data_rd <= sig_ch4_data_rd;
ch5_data_rd <= sig_ch5_data_rd;
ch6_data_rd <= sig_ch6_data_rd;
ch7_data_rd <= sig_ch7_data_rd;

-- second generate write strobe (delayed one clock cycle to any read strobe)
dly_wr_p: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		sig_out_data_wr <= '0';
		sel_d <= ch0;
	elsif rising_edge(clk) then
		sig_out_data_wr <= any_data_rd_en;
		sel_d <= sel;
	end if;
end process;

any_data_rd_en <= sig_ch0_data_rd or sig_ch1_data_rd or sig_ch2_data_rd or sig_ch3_data_rd or sig_ch4_data_rd or sig_ch5_data_rd or sig_ch6_data_rd or sig_ch7_data_rd;

out_data_wr <= sig_out_data_wr;

-- output data multiplexer - delayed selector because write strobe is also delayed
sig_out_data <= ch0_data when sel_d = ch0 else
				ch1_data when sel_d = ch1 else
				ch2_data when sel_d = ch2 else
				ch3_data when sel_d = ch3 else
				ch4_data when sel_d = ch4 else
				ch5_data when sel_d = ch5 else
				ch6_data when sel_d = ch6 else
				ch7_data;

-- out data or check sum when the tx buffer is ready				
out_data <= sig_out_data when txbuff_cnt_done_d_en = '0' else std_logic_vector(sum_acc);
out_chksum_wr <= txbuff_cnt_done_d_en;
				
-- address counter
addr_cnt_p: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		addr_cnt <= (others=>'0');
	elsif rising_edge(clk) then
		if sig_out_data_wr = '1' or txbuff_cnt_done_d_en = '1' then
			if addr_cnt = max_address then
				addr_cnt <= (others=>'0');
			else
				addr_cnt <= addr_cnt + 1;
			end if;
		end if;
	end if;
end process;

out_address <= std_logic_vector(addr_cnt);

-- TX buffer counter
txbuff_cnt_p: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		txbuff_cnt <= to_unsigned((TXBUFF_SIZE/4)-1, TXBUFF_NBITS);
		--txbuff_cnt <= (others=>'0');
	elsif rising_edge(clk) then
		if any_data_rd_en = '1' then
			if txbuff_cnt_done = '0' then
				txbuff_cnt <= txbuff_cnt + 1;
			else
				txbuff_cnt <= (others=>'0');
			end if;
		end if;
	end if;
end process;

txbuff_cnt_done <= '1' when to_integer(txbuff_cnt) = (TXBUFF_SIZE/4)-1 else '0';

-- TX done rising edge detectors
tx_done_redge_p: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		txbuff_cnt_done_d <= '1';
		txbuff_cnt_done_d2 <= '1';
	elsif rising_edge(clk) then
		txbuff_cnt_done_d <= txbuff_cnt_done;
		txbuff_cnt_done_d2 <= txbuff_cnt_done_d;
	end if;
end process;
txbuff_cnt_done_en <= txbuff_cnt_done and not txbuff_cnt_done_d;
txbuff_cnt_done_d_en <= txbuff_cnt_done_d and not txbuff_cnt_done_d2;

-- Sum accumulator
-- it resets when a new tx buffer begins
sum_acc_p: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		sum_acc <= (others=>'0');
	elsif rising_edge(clk) then
		if sig_out_data_wr = '1' then			
			sum_acc <= sum_acc + unsigned(sig_out_data(31 downto 16)) + unsigned(sig_out_data(15 downto 0));
		elsif txbuff_cnt_done_d_en = '1' then
			sum_acc <= (others=>'0');
		end if;
	end if;
end process;

-- buffer ready signal (the same like chksum write strobe)
buff_rdy <= txbuff_cnt_done_d_en;

-- last buffer address register
last_addr_reg_p: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		buff_last_addr <= (others=>'0');
	elsif rising_edge(clk) then
		if txbuff_cnt_done_d_en = '1' then			
			buff_last_addr <= std_logic_vector(addr_cnt);
		end if;
	end if;
end process;

end rtl;


