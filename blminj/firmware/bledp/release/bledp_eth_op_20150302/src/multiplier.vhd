------------------------------------------------------------------
--Design Units : 	binary multiplier component
--
--Notes			:	Unsigned multiplier op1*op2
--
--Synthesis		:
--
--Implement 	:
--
--Notes			:	
--
--Author			:	Maciej Kwiatkowski
--		  	 			European Organisation for Nuclear Research
--		 	 			CERN, Geneva, Switzerland,  CH-1211
--
--Simulator		:	Modelsim SE/XE
--ISE				:	
------------------------------------------------------------------
--Vsn 	Author	Date			Changes
--
--0.1		MK		27.06.12		First version
------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;


entity multiplier is
	generic (
		OP1_LEN			: natural := 16;			-- operand 1 bits number
		OP2_LEN			: natural := 4;				-- operand 2 bits number
		RESET_ACTIVE	: std_logic	:= '0'			-- polarity of the reset
	);
	port(
		-- global signals
		clk				: in std_logic;
		rst				: in std_logic;
		-- input signals
		op1				: in unsigned(OP1_LEN-1 downto 0);
		op2				: in unsigned(OP2_LEN-1 downto 0);
		multiply		: in std_logic;
		-- output_signals
		result			: out unsigned(OP1_LEN+OP2_LEN-1 downto 0);
		result_en		: out std_logic;
		busy_flg		: out std_logic
	);
end multiplier;

architecture rtl of multiplier is


-- shifts counter
constant shift_cnt_max : natural := OP2_LEN-1;
signal shift_cnt : natural range 0 to shift_cnt_max;
signal shift_en : std_logic;
signal shift_cnt_done : std_logic;

--FSM signals
type StateType is (idle, shift, add);
signal state, next_state : StateType;
signal result_add_en : std_logic;
signal busy_flg_sig : std_logic;
signal busy_flg_sig_d : std_logic;

-- operands shift registers
signal op1_reg : unsigned(OP1_LEN+OP2_LEN-1 downto 0);
signal op2_reg : unsigned(OP2_LEN-1 downto 0);

--result register
signal result_reg : unsigned(OP1_LEN+OP2_LEN-1 downto 0);

begin

-- shift counter
sh_cnt: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		shift_cnt <= 0;
	elsif rising_edge(clk) then
		if multiply = '1' and shift_cnt_done = '1' then
			shift_cnt <= shift_cnt_max;
		elsif shift_en = '1' then
			shift_cnt <= shift_cnt - 1;
		end if;
	end if;
end process;
shift_cnt_done <= '1' when shift_cnt = 0 else '0';

------------------------------------------------------------------
-- multiplier FSM
-- add, shift
------------------------------------------------------------------
fsm_cnt_p: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		state <= idle;
	elsif rising_edge(clk) then
		state <= next_state;
	end if;
end process;

fsm_cl_p: process(state, shift_cnt_done)
begin
	next_state <= state;
	busy_flg_sig <= '1';
	result_add_en <= '0';
	shift_en <= '0';

	case state is
		
		when idle =>
			busy_flg_sig <= '0';
			if shift_cnt_done = '0' then
				next_state <= add;
			end if;
		
		when add =>
			result_add_en <= '1';
			if shift_cnt_done = '1' then
				next_state <= idle;
			else
				next_state <= shift;
			end if;
		
		when shift =>
			shift_en <= '1';
			next_state <= add;
			
	end case;
	
end process;

-- op1 left shift register
op1_shift_reg: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		op1_reg <= (others=>'0');
	elsif rising_edge(clk) then
		if multiply = '1' and shift_cnt_done = '1' then
			op1_reg <= resize(op1, OP1_LEN+OP2_LEN);
		elsif shift_en = '1' then
			op1_reg <= op1_reg(OP1_LEN+OP2_LEN-2 downto 0) & '0';
		end if;
	end if;
end process;

-- op2 right shift register
op2_shift_reg: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		op2_reg <= (others=>'0');
	elsif rising_edge(clk) then
		if multiply = '1' and shift_cnt_done = '1' then
			op2_reg <= op2;
		elsif shift_en = '1' then
			op2_reg <= '0' & op2_reg(OP2_LEN-1 downto 1);
		end if;
	end if;
end process;

-- result add products
res_add_reg: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		result_reg <= (others=>'0');
	elsif rising_edge(clk) then
		if multiply = '1' and shift_cnt_done = '1' then
			result_reg <= (others=>'0');
		elsif result_add_en = '1' and op2_reg(0) = '1' then
			result_reg <= result_reg + op1_reg;
		end if;
	end if;
end process;
result <= result_reg;


-- result ready strobe
ready_en: process(clk, rst)
begin
	if rst = RESET_ACTIVE then
		busy_flg_sig_d <= '1';
	elsif rising_edge(clk) then
		busy_flg_sig_d <= busy_flg_sig;
	end if;
end process;
result_en <= not busy_flg_sig and busy_flg_sig_d;
busy_flg <= busy_flg_sig;

end rtl;


