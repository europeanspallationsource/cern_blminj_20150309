// Company:		CERN - BE/BI/BL
// Engineer:	Maciej Kwiatkowski
// Create Date:	14/06/2012
// Module Name:	bledp_acquisition
// Description:	module to acquire and combine the data in the BLEDP card


module bledp_acquisition # (
	parameter t_clk_period_ns = 4,
	parameter t_integration_ns = 2000,	//must be >= 100 ns, default is 2us
	parameter ADC_dynamic_range = 1 	//0 - disable dynamic ADC range, 1 or other value - enable dynamic ADC range
)
(
	//clock
	input clk,
	//reset
	input nrst,
	//ADC interface
	output adc_run,
	output adc_next,
	input adc_vld,
	input [15:0] adc_data,
	//integrator interface
	input fdfc_pos,
	input fdfc_neg,
	//control status
	input raw_data,
	input adc_range_reset,
	output [15:0] adc_max,
	output [15:0] adc_min,
	output saturation,
	//output data
	output reg [19:0] data,
	output reg data_en
);

//measurement period counter parameters
localparam p_cnt_max = t_integration_ns/t_clk_period_ns;
localparam N_p_cnt = $clog2(p_cnt_max);

reg [N_p_cnt:0] p_cnt = 0;
reg [15:0] adc_sample_1 = 0 /* synthesis keep */;
reg [15:0] adc_sample_2 = 0 /* synthesis keep */;
reg [16:0] adc_sample_diff_1 = 0;
reg [16:0] adc_sample_diff_2 = 0;
reg [16:0] adc_sample_diff = 0;
reg [16:0] full_adc_range = 0;
reg [15:0] adc_sample_diff_abs_1 = 0;
reg [15:0] adc_sample_diff_abs_2 = 0;
reg [15:0] adc_sample_diff_abs = 0;
reg [15:0] full_adc_range_abs = 0;
reg fdfc_neg_ff1 = 0;
reg fdfc_neg_ff2 = 0;
reg fdfd_pos_ff1 = 0;
reg fdfd_pos_ff2 = 0;
reg fdfc_slope = 0 /* synthesis keep */;
reg fdfc_slope_1 = 0 /* synthesis keep */;
reg fdfc_slope_2 = 0 /* synthesis keep */;
reg [3:0] fdfc_neg_cnt = 0 /* synthesis keep */;
reg [3:0] fdfc_pos_cnt = 0 /* synthesis keep */;
reg [3:0] fdfc_neg_reg = 0 /* synthesis keep */;
reg [3:0] fdfc_pos_reg = 0 /* synthesis keep */;
reg fdfc_count_reg_en = 0 /* synthesis keep */;
reg [3:0] fdfc_counts_sum;
reg fdfc_counts_sum_en;
reg adc_sample_en = 0;
reg adc_sample_diff_en = 0;
reg adc_sample_diff_abs_en = 0;
reg [15:0] adc_max_reg = 16'h7FFF /* synthesis keep */;
reg [15:0] adc_min_reg = 16'h8000 /* synthesis keep */;

wire p_cnt_done /* synthesis keep */;
wire fdfc_neg_fedge /* synthesis keep */;
wire fdfc_pos_fedge /* synthesis keep */;
wire [16:0] adc_sample_diff_abs_1_t;
wire [16:0] adc_sample_diff_abs_2_t;
wire [16:0] adc_sample_diff_abs_t;
wire [16:0] full_adc_range_abs_t;
wire [16:0] adc_sample_sign_1;
wire [16:0] adc_sample_sign_2;
wire [16:0] adc_sample_sign;
wire [16:0] full_adc_range_sign;

wire [16:0] adc_sample_diffs_abs_sum;

wire zero_counts;
wire [19:0] adc_range_mul_counts;
wire adc_range_mul_counts_en;
wire busy_flg;
wire [20:0] data_combined_case_2_3;
wire [19:0] data_combined_case_1;
wire [19:0] data_combined;
wire [19:0] data_raw;


//FDFC negative pulses falling edge detector and counter
always @(posedge clk)
begin
	fdfc_neg_ff1 <= fdfc_neg;
	fdfc_neg_ff2 <= fdfc_neg_ff1;
end
assign fdfc_neg_fedge = ~fdfc_neg_ff1 & fdfc_neg_ff2;
//assign fdfc_neg_fedge = ~fdfc_neg & fdfc_neg_ff1;

always @(posedge clk)
begin
	if (p_cnt_done & ~fdfc_neg_fedge)
		fdfc_neg_cnt <= 0;
	else if (fdfc_neg_fedge & ~p_cnt_done)
		fdfc_neg_cnt <= fdfc_neg_cnt + 1'b1;
	else if (p_cnt_done & fdfc_neg_fedge)
		fdfc_neg_cnt <= 1;
end

//FDFC positive pulses falling edge detector and counter
always @(posedge clk)
begin
	fdfd_pos_ff1 <= fdfc_pos;
	fdfd_pos_ff2 <= fdfd_pos_ff1;
end
assign fdfc_pos_fedge = ~fdfd_pos_ff1 & fdfd_pos_ff2;
//assign fdfc_pos_fedge = ~fdfc_pos & fdfd_pos_ff1;

always @(posedge clk)
begin
	if (p_cnt_done & ~fdfc_pos_fedge)
		fdfc_pos_cnt <= 0;
	else if (fdfc_pos_fedge & ~p_cnt_done)
		fdfc_pos_cnt <= fdfc_pos_cnt + 1'b1;
	else if (p_cnt_done & fdfc_pos_fedge)
		fdfc_pos_cnt <= 1;
end

//FDFC signal slope flag
//1 - signal is rising, 0 - signal is falling
always @(posedge clk)
begin
	if (fdfc_pos_fedge)
		fdfc_slope <= 0;
	else if (fdfc_neg_fedge)
		fdfc_slope <= 1;
end

//measurement period free running counter
always @(posedge clk)
begin
	if (p_cnt_done)
		p_cnt <= 0;
	else
		p_cnt <= p_cnt + 1'b1;
end
assign p_cnt_done = (p_cnt==p_cnt_max-1? 1'b1: 1'b0);
assign adc_run = p_cnt_done;
assign adc_next = 1'b1;

//ADC samples registers
//stores data when the sample is ready
//p_cnt_done (end/beginning of the measurement period) starts the ADC 
//but the sample is ready when adc_vld arrive (after ADC processing and TX is finished)
always @(posedge clk)
begin
	if (adc_vld)
	begin
		adc_sample_2 <= adc_data;
		adc_sample_1 <= adc_sample_2;
	end
	adc_sample_en <= adc_vld;
end

//FDFC counters registers
//store the counters when the measurement period is finished
always @(posedge clk)
begin
	if (p_cnt_done)
	begin
		//numbers of positive and negative counts
		fdfc_neg_reg <= fdfc_neg_cnt;
		fdfc_pos_reg <= fdfc_pos_cnt;
		
		//edge flags of each ADC ample (at the beginning and at the end of the measurement period)
		fdfc_slope_2 <= fdfc_slope;
		fdfc_slope_1 <= fdfc_slope_2;
	end
	
	//fdfc count regs strobe
	fdfc_count_reg_en <= p_cnt_done;
end

//sum of FDFC counts register
always @(posedge clk)
begin
	if (fdfc_count_reg_en)
	begin
		fdfc_counts_sum <= fdfc_neg_reg + fdfc_pos_reg;
	end
	
	fdfc_counts_sum_en <= fdfc_count_reg_en;
	
end

//optional ADC effective range scan
generate
	if (ADC_dynamic_range != 0)
	begin: dynamic_ADC_range
		
		//parameters, wires and registers
		reg [15:0] scan_min_reg = 16'h7FFF;
		reg [15:0] scan_max_reg = 16'h8000;
		
		//scan min ADC value
		//reset every refresh time
		always @(posedge clk)
		begin
			/*
			if (adc_range_reset)
				scan_min_reg <= 16'h7FFF;
			else if (adc_vld & $signed(scan_min_reg) > $signed(adc_data))
				scan_min_reg <= adc_data;
			*/
			if (adc_range_reset | (adc_vld & $signed(scan_min_reg) > $signed(adc_data)))
				scan_min_reg <= (adc_range_reset? 16'h7FFF: adc_data);
		end
		//store last refresh min value 
		always @(posedge clk)
		begin
			if (adc_range_reset)
				adc_min_reg <= scan_min_reg;
		end
		
		//scan max ADC value
		//reset every refresh time
		always @(posedge clk)
		begin
		/*
			if (adc_range_reset)
				scan_max_reg <= 16'h8000;
			else if (adc_vld & $signed(scan_max_reg) < $signed(adc_data))
				scan_max_reg <= adc_data;*/
			if (adc_range_reset | (adc_vld & $signed(scan_max_reg) < $signed(adc_data)))
				scan_max_reg <= (adc_range_reset? 16'h8000: adc_data);
		end
		//store last refresh max value 
		always @(posedge clk)
		begin
			if (adc_range_reset)
				adc_max_reg <= scan_max_reg;
		end
		
	end
	else
	begin: fixed_ADC_range
	
		//parameters, wires and registers
		//consider moving them to user settable parameters
		localparam [15:0] adc_max_val = 16'h7FFF;
		localparam [15:0] adc_min_val = 16'h8000;
		
		//store fixed adc range 
		always @(posedge clk)
		begin
			adc_max_reg <= adc_max_val;
			adc_min_reg <= adc_min_val;
		end

	end
endgenerate

assign adc_max = adc_max_reg;
assign adc_min = adc_min_reg;


//difference registers
always @(posedge clk)
begin
	//case 1 (counts == 0)
	//2 samples difference
	adc_sample_diff <= {adc_sample_1[15],adc_sample_1} - {adc_sample_2[15],adc_sample_2};
	//case 2,3 (counts >= 1)
	//two differences:
	//sample1 - (min or max)
	//(min or max) - sample2
	//min or max is selected depending on the signal's slope where the sample was taken
	adc_sample_diff_1 <= {adc_sample_1[15],adc_sample_1} - (fdfc_slope_1? {adc_max_reg[15],adc_max_reg} : {adc_min_reg[15],adc_min_reg});
	adc_sample_diff_2 <= (fdfc_slope_2? {adc_min_reg[15],adc_min_reg}: {adc_max_reg[15],adc_max_reg}) - {adc_sample_2[15],adc_sample_2};
	
	//data strobe (all cases)
	adc_sample_diff_en <= adc_sample_en;
	
	//full ADC range difference
	//for both fixed or dynamic range versions
	full_adc_range <= {adc_max_reg[15],adc_max_reg} - {adc_min_reg[15],adc_min_reg};
	
end

//difference absolute value conversion
//expression to convert negative value to positive value - it changes nothing if the value is already positive
//value = (value xor values_extended_sign_bit) - values_extended_sign_bit
assign adc_sample_diff_abs_t = (adc_sample_diff ^ adc_sample_sign) - adc_sample_sign;
assign adc_sample_diff_abs_1_t = (adc_sample_diff_1 ^ adc_sample_sign_1) - adc_sample_sign_1;
assign adc_sample_diff_abs_2_t = (adc_sample_diff_2 ^ adc_sample_sign_2) - adc_sample_sign_2;
assign full_adc_range_abs_t = (full_adc_range ^ full_adc_range_sign) - full_adc_range_sign;
//register absolute value without sign bit (always '0': positive)
always @(posedge clk)
begin
	//case 1 (counts == 0)
	adc_sample_diff_abs <= adc_sample_diff_abs_t[15:0];
	//case 2,3 (counts >= 1)
	adc_sample_diff_abs_1 <= adc_sample_diff_abs_1_t[15:0];
	adc_sample_diff_abs_2 <= adc_sample_diff_abs_2_t[15:0];
	
	//data strobe (all cases)
	adc_sample_diff_abs_en <= adc_sample_diff_en;
	
	//ADC full range absolute value
	full_adc_range_abs <= full_adc_range_abs_t[15:0];
end
//abs calculation helper values containing sample's sign bit extended to all bits
assign adc_sample_sign = (adc_sample_diff[16]==1? 17'h1FFFF : 17'h00000);
assign adc_sample_sign_1 = (adc_sample_diff_1[16]==1? 17'h1FFFF : 17'h00000);
assign adc_sample_sign_2 = (adc_sample_diff_2[16]==1? 17'h1FFFF : 17'h00000);
assign full_adc_range_sign = (full_adc_range[16]==1? 17'h1FFFF : 17'h00000);

multiplier #(
	.OP1_LEN(16),
	.OP2_LEN(4),
	.RESET_ACTIVE(1'b0)
) u1(
	.clk(clk),
	.rst(nrst),
	.op1(full_adc_range_abs),
	.op2(fdfc_counts_sum - 1'b1),
	.multiply(fdfc_counts_sum_en),
	.result(adc_range_mul_counts),
	.result_en(adc_range_mul_counts_en),
	.busy_flg(busy_flg)
);

//result register + raw/processed data selector
always @(posedge clk)
begin
	if (raw_data)
		data <= data_raw;
	else
		data <= data_combined;
	data_en <= adc_sample_diff_abs_en;
end
//raw data is always sum of pos and neg counts at MSB and last ADC sample at LSB
assign data_raw = {fdfc_counts_sum, adc_sample_2};
//case 1 calculations (zero counts + difference of 2 samples)
assign data_combined_case_1 = {4'b0, adc_sample_diff_abs};
//case 2,3 calculation (1 or more counts - 1 + 2 differences of 2 samples)
//assign data_combined_case_2_3 = {fdfc_counts_sum - 1'b1, 16'd0} + adc_sample_diffs_abs_sum;
assign data_combined_case_2_3 = adc_range_mul_counts + adc_sample_diffs_abs_sum;
//select saturation bit and data source depending on the current case (1 or 2,3)
assign {saturation, data_combined} = (zero_counts? {1'b0, data_combined_case_1}: data_combined_case_2_3);

//case 1 or case 2,3 selector
assign zero_counts = (fdfc_counts_sum == 0? 1'b1: 1'b0);
//sum of 2 differences of 2 samples for case 2 and 3
assign adc_sample_diffs_abs_sum = adc_sample_diff_abs_1 + adc_sample_diff_abs_2;

endmodule
