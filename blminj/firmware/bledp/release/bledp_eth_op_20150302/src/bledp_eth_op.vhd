------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    12/03/2012
--Module Name:    bledp_eth_op
--Project Name:   bledp_eth_op
--Description:   
--
--
--  Date            Author      Change
--  10/11/2014      MK          Fixed byte re-ordering pattern in the protocol_tx. According to the manual the byte pattern should appear only in the least 
--  11/11/2014      MK          Added external trigger for the byte order module of the optical gigabit receiver (rx_enabyteord).
--                              When configured as the internal it was not retrigerring the byte reordering after the word resync
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use work.vhdl_func_pkg.all;

entity bledp_eth_op is
generic(
	-- generate test mode logic - 4 channels with combined data (ch 1 to 4) and the same data in raw mode (ch 5 to 8)
	raw_comb_test_mode:		boolean := false
);
port(
	-- Reference Clock 125 MHz
	REFCLK2:				in std_logic;
	-- Reference Clock 125 MHz
	REFCLK1:				in std_logic;
	
	-- I. Analog Channel ADC	
	CNV:					out std_logic_vector(8 downto 1);	
	CLK:					out std_logic_vector(8 downto 1);
	DCO:					in std_logic_vector(8 downto 1);
	D:						in std_logic_vector(8 downto 1);
	
	-- II. Analog Monitors
	CLR:					out std_logic_vector(8 downto 1);
	PRE:					out std_logic_vector(8 downto 1);
	STOPL:					out std_logic_vector(8 downto 1);
	STOPOUT:				in std_logic_vector(8 downto 1);
	
	COUNT_M:				in std_logic_vector(8 downto 1);
	COUNT_P:				in std_logic_vector(8 downto 1);	
	
	-- II.a. Digital Potentiometer
	SDA: 					inout std_logic_vector(8 downto 1);
	SCL:					out std_logic_vector(8 downto 1);
	WP:						out std_logic_vector(8 downto 1);

	-- III. Circuit Breaker
	ON_CB1:					out std_logic;
	ON_CB2:					out std_logic;
	POWERGOOD_CB1:			in std_logic;
	POWERGOOD_CB2:			in std_logic;
	
	-- IV. Power Supply
	DCDC_ENABLE:			out std_logic;
	POWERGOOD_1_2:		in std_logic;
	
	-- V. Service ADC
	AD_I_CLK:				out std_logic;
	AD_I_DIN:				out std_logic;
	AD_I_CS_N:				out std_logic;
	AD_I_DOUT:				in std_logic;
--	
	-- VI. Relays
	RELAY_ACTIVATION_FPGA_1: 	out std_logic;
	RELAY_ACTIVATION_FPGA_2: 	out std_logic;
	RELAY_ACTIVATION_FPGA_3: 	out std_logic;
	RELAY_ACTIVATION_FPGA_4: 	out std_logic;
	RELAY_ACTIVATION_FPGA_5: 	out std_logic;
	RELAY_ACTIVATION_FPGA_6:	out std_logic;
	RELAY_ACTIVATION_FPGA_7: 	out std_logic;
	RELAY_ACTIVATION_FPGA_8: 	out std_logic;
	RELAYS_STATUS_FPGA:			in std_logic;
	
	-- VII. Temperatur/Humidity
	SHT15_SCK:					out std_logic;
	SHT15_DATA:					inout std_logic;

	-- VIII. ID
	ID:							inout std_logic;
	
	-- IX. Optical/Network SPF Link
	SDA_OPT_1:					inout std_logic;
	SCL_OPT_1:					out std_logic;
	LOSS_OF_SIGNAL_1:			in std_logic;
	MODULE_DETECT_1:			in std_logic;
	TX_FAULT_1:					in std_logic;
	RATE_SELECT_1:				out std_logic;
	TX_DISABLE_1:				out std_logic;
	GXP_RX1:					in std_logic;
	GXP_TX1:					out std_logic;

	SDA_OPT_2:					inout std_logic;
	SCL_OPT_2:					out std_logic;
	LOSS_OF_SIGNAL_2:			in std_logic;
	MODULE_DETECT_2:			in std_logic;
	TX_FAULT_2:					in std_logic;
	RATE_SELECT_2:				out std_logic;
	TX_DISABLE_2:				out std_logic;
	GXP_RX2:					in std_logic;
	GXP_TX2:					out std_logic;
	
	-- X. Others
	LED_1:						out std_logic;
	LED_2:						out std_logic;
	LED_3:						out std_logic;
	POS1:						in std_logic;
	POS2:						in std_logic;
	POS3:						in std_logic;
	POS4:						in std_logic
--	TESTPOINT_1:				out std_logic;
--	TESTPOINT_2:				out std_logic;
--	TESTPOINT_3:				out std_logic;
--	TESTPOINT_4:				out std_logic;
--	TESTPOINT_5:				out std_logic;
--	TESTPOINT_6:				out std_logic;
--	TESTPOINT_7:				out std_logic;
--	TESTPOINT_8:				out std_logic

);
	
end entity bledp_eth_op;


architecture rtl of bledp_eth_op is

attribute keep: boolean;

constant sys_clk_freq : natural := 100000000;
--constant sys_clk_freq : natural := 50000000;
constant sys_clk_ns_period : natural := 1000000000/sys_clk_freq;

-- type definitions
type channel_sig_record is
	record
		ch_input 		: std_logic_vector(31 downto 0);
		ch_in_wr 		: std_logic;
		ch_data			: std_logic_vector(31 downto 0);
		ch_data_avail	: std_logic; 	 						
		ch_fifo_empty	: std_logic; 	 						
		ch_data_rd		: std_logic;
		ch_error 		: std_logic;		
	end record;
type channel_sig_array is array (1 to 8) of channel_sig_record; 
type std_logic_vector_array is array(natural range <>) of std_logic_vector;
type unsigned_array is array(natural range <>) of unsigned;

-- PLL signals
signal sys_clk:			std_logic;
signal ADC_clk:			std_logic;
signal clk_40:			std_logic;
attribute keep of ADC_clk: signal is true;
attribute keep of sys_clk: signal is true;
signal pll_locked:		std_logic;


-- Reset controller
signal rst_sys_clk_n_shift:	std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
signal rst_sys_clk_n:			std_logic;

-- transceivers reset signals
signal pll_areset_eth:			std_logic;
signal pll_locked_eth:			std_logic;
signal rx_freqlocked_eth:		std_logic;
signal tx_digitalreset_eth:		std_logic;
signal rx_analogreset_eth:		std_logic;
signal rx_digitalreset_eth:		std_logic;
signal pll_areset_opt:			std_logic;
signal pll_locked_opt:			std_logic;
signal rx_freqlocked_opt:		std_logic;
signal rx_enabyteord_opt:		std_logic;
signal rx_byteorderalignstatus_opt:		std_logic;
signal rx_syncstatus_opt:		std_logic_vector(1 downto 0);
signal tx_digitalreset_opt:		std_logic;
signal rx_analogreset_opt:		std_logic;
signal rx_digitalreset_opt:		std_logic;

--optical link signals
signal rx_clkout_opt:			std_logic;
signal tx_clkout_opt:			std_logic;
signal rx_dataout_opt:			std_logic_vector(15 downto 0);
signal tx_datain_opt: 			std_logic_vector(15 downto 0);
signal tx_ctrlenable_opt: 			std_logic_vector(1 downto 0);
signal rx_ctrldetect_opt:			std_logic_vector(1 downto 0);
signal rx_errdetect_opt:			std_logic_vector(1 downto 0);
attribute keep of rx_clkout_opt: signal is true;
attribute keep of rx_dataout_opt: signal is true;
attribute keep of rx_ctrldetect_opt: signal is true;
attribute keep of rx_errdetect_opt: signal is true;


-- MAC signals
signal led_auto_neg:	std_logic;
signal led_link:		std_logic;
signal led_traffic:		std_logic;
signal mac_tbi_tx_clk : std_logic;
signal mac_tbi_tx_d : 	std_logic_vector(9 downto 0);
signal mac_tbi_rx_clk : std_logic;
signal mac_tbi_rx_d : 	std_logic_vector(9 downto 0);

-- GX reconfig component signals
signal reconfig_fromgxb_0	: STD_LOGIC_VECTOR (4 DOWNTO 0);
signal reconfig_fromgxb_1	: STD_LOGIC_VECTOR (4 DOWNTO 0);
signal reconfig_busy		: STD_LOGIC ;
signal reconfig_togxb		: STD_LOGIC_VECTOR (3 DOWNTO 0);


--8 acquisition channels ADC signals
signal adc_run : std_logic_vector(8 downto 1);
signal adc_run_acq : std_logic_vector(8 downto 1);
signal adc_next : std_logic_vector(8 downto 1);
signal adc_vld : std_logic_vector(8 downto 1);
signal adc_vld_acq : std_logic_vector(8 downto 1);
signal adc_vld_acq_corr : std_logic_vector(8 downto 1);
attribute keep of adc_vld: signal is true;

signal adc_data : std_logic_vector_array(8 downto 1) (15 downto 0);
signal adc_data_corr : std_logic_vector_array(8 downto 1) (15 downto 0);
attribute keep of adc_data: signal is true;
signal raw_data : std_logic_vector(8 downto 1);
signal adc_data_abs : std_logic_vector_array(8 downto 1) (15 downto 0);

-- 8 acquisition channels
signal proc_data : std_logic_vector_array (8 downto 1) (19 downto 0);
signal data_debug : std_logic_vector_array (8 downto 1) (19 downto 0);
signal proc_data_en : std_logic_vector(8 downto 1);
signal proc_data_filt : std_logic_vector_array (8 downto 1) (19 downto 0);
signal proc_data_filt_en : std_logic_vector(8 downto 1);
attribute keep of proc_data: signal is true;
--attribute keep of proc_data_filt: signal is true;
signal enable_filter : std_logic_vector(8 downto 1);
signal period_pulse : std_logic_vector(8 downto 1);
signal channel_signals : channel_sig_array;
signal ch_fifo_full : std_logic_vector(8 downto 1);
signal ch_fifo_wr_en : std_logic_vector(8 downto 1);

-- channel multiplexer signals
signal out_data			: std_logic_vector(31 downto 0);
signal out_data_swapped	: std_logic_vector(31 downto 0);
signal out_data_wr		: std_logic;								-- data write request
signal out_chksum_wr	: std_logic;								-- check sum write request
signal out_wr			: std_logic;								-- write request
signal out_address		: std_logic_vector(10 downto 0);			-- address
signal buff_rdy			: std_logic;								-- buffer ready strobe
signal buff_last_addr	: std_logic_vector(10 downto 0);			-- address of the end of the last buffer
signal buff_offset		: std_logic_vector(31 downto 0);			-- address of the end of the last buffer
signal ch_mux_rst		: std_logic;								-- reset signal

-- 8 channels processed data debug counters
signal proc_inc_dbg_cnt : unsigned_array (8 downto 1) (5 downto 0);

-- status data generator signals
signal cnt_1s : natural range 0 to sys_clk_freq - 1;
signal cnt_1s_done : std_logic;
attribute keep of cnt_1s_done: signal is true;

-- acquisition interface signals
signal acq_iface_control : std_logic_vector(31 downto 0);
signal acq_iface_status : std_logic_vector(31 downto 0);
signal acq_iface_reset : std_logic;

--streaming FIFO signals
signal sfifo_q : std_logic_vector(31 downto 0);
signal stream_fifo_reset : std_logic;
signal sfifo_empty : std_logic;
signal sfifo_full : std_logic;	
signal sfifo_wr_enable : std_logic;	
signal sfifo_wr : std_logic;	
signal sfifo_ready : std_logic;
signal sfifo_valid : std_logic;


--status data signals	
signal status_int:		std_logic;
signal status_sent:		std_logic;

-- command PIO signals
signal command_pio_0_export:	std_logic_vector(31 downto 0);
signal command_pio_1_export:	std_logic_vector(31 downto 0);
signal command_pio_2_export:	std_logic_vector(31 downto 0);

-- digital potentiometer signals
signal dp_acc:              std_logic_vector(8 downto 1);
signal dp_toggle_bit:       std_logic_vector(8 downto 1);
signal dp_toggle_en:        std_logic_vector(8 downto 1);
signal dp_wrreq_vld:        std_logic_vector(8 downto 1);
signal dp_wrreq_vld_int:    std_logic_vector(8 downto 1);
signal dp_wrreq_next:       std_logic_vector(8 downto 1);
signal dp_wrreq_data:       slv_array(8 downto 1) (17 downto 0);

-- relays signal	
signal relay_set : std_logic_vector(7 downto 0);

-- scan ADC range signals
signal adc_max : std_logic_vector_array(8 downto 1) (15 downto 0);
signal adc_min : std_logic_vector_array(8 downto 1) (15 downto 0);
attribute keep of adc_max: signal is true;
attribute keep of adc_min: signal is true;

signal adc_fixed_range_max : std_logic_vector(15 downto 0);
signal adc_fixed_range_min : std_logic_vector(15 downto 0);

signal DADC_en:		STD_LOGIC_VECTOR (7 DOWNTO 0);

-- Diagnostic reader constants
constant pkg_data_size:     integer := 17;
constant pkg_tag_size:      integer := 8;
constant scoreboard_words:  integer := 16;
constant diag_ram_addr_size:     integer := 9;
constant diag_ram_data_size:     integer := pkg_data_size - 1;
-- Diagnostic reader signals
signal diag_tx_data:        std_logic_vector(pkg_data_size-1 downto 0);
signal diag_tx_vld:         std_logic;
signal diag_tx_next:        std_logic;
signal diag_rx_data:        std_logic_vector(pkg_data_size-1 downto 0);
signal diag_rx_vld:         std_logic;
signal diag_rx_next:        std_logic;
signal sb_tx_data:          std_logic_vector(pkg_data_size-1 downto 0);
signal sb_tx_vld:           std_logic;
signal sb_tx_next:          std_logic;
signal sb_rx_data:          std_logic_vector(pkg_data_size-1 downto 0);
signal sb_rx_vld:           std_logic;
signal sb_rx_next:          std_logic;
signal fifo_tx_data:        std_logic_vector(pkg_data_size-1 downto 0);
signal fifo_tx_vld:         std_logic;
signal fifo_tx_next:        std_logic;
signal fifo_rx_data:        std_logic_vector(pkg_data_size-1 downto 0);
signal fifo_rx_vld:         std_logic;
signal fifo_rx_next:        std_logic;
signal diag_ram_addr:            std_logic_vector(diag_ram_addr_size-1 downto 0);
signal diag_ram_data:            std_logic_vector(diag_ram_data_size-1 downto 0);
signal diag_ram_data_swapped:    std_logic_vector(31 downto 0);
signal diag_ram_wr_en:           std_logic;
signal diag_ram_rdout_busy:      std_logic;

-- acquisition path signals
signal acq_path_fifo_in_vld : std_logic;
attribute keep of acq_path_fifo_in_vld: signal is true;

signal acq_cnt : unsigned(23 downto 0);

signal acq_path_out : std_logic_vector(17 downto 0);
signal acq_path_out_vld : std_logic;
signal acq_path_out_next : std_logic;



-- Verliog Component declaration
component nios_eth_sys is
        port (
            clk_clk                              : in    std_logic                     := 'X';             -- clk
            reset_reset_n                        : in    std_logic                     := 'X';             -- reset_n
            mac_led_an                           : out   std_logic;                                        -- led_an
            mac_led_char_err                     : out   std_logic;                                        -- led_char_err
            mac_led_link                         : out   std_logic;                                        -- led_link
            mac_led_disp_err                     : out   std_logic;                                        -- led_disp_err
            mac_led_crs                          : out   std_logic;                                        -- led_crs
            mac_led_col                          : out   std_logic;                                        -- led_col
            mac_powerdown                        : out   std_logic;                                        -- powerdown
            mac_sd_loopback                      : out   std_logic;                                        -- sd_loopback
            mac_tbi_tx_clk                       : in    std_logic                     := 'X';             -- tbi_tx_clk
            mac_tbi_tx_d                         : out   std_logic_vector(9 downto 0);                     -- tbi_tx_d
            mac_tbi_rx_clk                       : in    std_logic                     := 'X';             -- tbi_rx_clk
            mac_tbi_rx_d                         : in    std_logic_vector(9 downto 0)  := (others => 'X'); -- tbi_rx_d
            acq_mem_s2_1_address                 : in    std_logic_vector(10 downto 0) := (others => 'X'); -- address
            acq_mem_s2_1_chipselect              : in    std_logic                     := 'X';             -- chipselect
            acq_mem_s2_1_clken                   : in    std_logic                     := 'X';             -- clken
            acq_mem_s2_1_readdata                : out   std_logic_vector(31 downto 0);                    -- readdata
            acq_mem_s2_1_write                   : in    std_logic                     := 'X';             -- write
            acq_mem_s2_1_writedata               : in    std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
            acq_mem_s2_1_byteenable              : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- byteenable
            acq_mem_clk2_1_clk                   : in    std_logic                     := 'X';             -- clk
            acq_mem_reset2_1_reset               : in    std_logic                     := 'X';             -- reset
            acq_int_pio_export                   : in    std_logic                     := 'X';             -- export
            acq_offset_pio_export                : in    std_logic_vector(31 downto 0) := (others => 'X'); -- export
            i2c_scl_pio_export                   : out   std_logic;                                        -- export
            i2c_sda_pio_export                   : inout std_logic                     := 'X';             -- export
            acq_iface_control_pio_export         : out   std_logic_vector(31 downto 0);                    -- export
            acq_iface_status_pio_export          : in    std_logic_vector(31 downto 0) := (others => 'X'); -- export
            status_mem_s2_address                : in    std_logic_vector(8 downto 0)  := (others => 'X'); -- address
            status_mem_s2_chipselect             : in    std_logic                     := 'X';             -- chipselect
            status_mem_s2_clken                  : in    std_logic                     := 'X';             -- clken
            status_mem_s2_readdata               : out   std_logic_vector(31 downto 0);                    -- readdata
            status_mem_s2_write                  : in    std_logic                     := 'X';             -- write
            status_mem_s2_writedata              : in    std_logic_vector(31 downto 0) := (others => 'X'); -- writedata
            status_mem_s2_byteenable             : in    std_logic_vector(3 downto 0)  := (others => 'X'); -- byteenable
            status_mem_clk2_clk                  : in    std_logic                     := 'X';             -- clk
            status_mem_reset2_reset              : in    std_logic                     := 'X';             -- reset
            status_int_pio_export                : in    std_logic                     := 'X';             -- export
            status_sent_pio_export               : out   std_logic;                                        -- export
            bledp_packet_generator_0_bledp_data  : in    std_logic_vector(31 downto 0) := (others => 'X'); -- data
            bledp_packet_generator_0_bledp_valid : in    std_logic                     := 'X';             -- valid
            bledp_packet_generator_0_bledp_ready : out   std_logic;                                        -- ready
            command_pio_0_export                 : out   std_logic_vector(31 downto 0);                    -- export
            command_pio_1_export                 : out   std_logic_vector(31 downto 0);                    -- export
            command_pio_2_export                 : out   std_logic_vector(31 downto 0)                     -- export
        );
    end component nios_eth_sys;

-- Verliog Component declaration
component AD7626 is
        generic(
                t_clk_period_ns: integer := 4 -- 250 MHz (default)
        );
        port(
                clk_i:                          in std_logic;   --250MHz Clock phase 0
                adc_cnv_start_i:        in std_logic;   --ADC start of conversion
                dco_i:                          in std_logic;   --ECHOED CLK from ADC
                d_i:                            in std_logic;   --SERIAL data from ADC
               
                cnv_o:                          out std_logic;  --CNV to ADC
                clk_o:                          out std_logic;  --Serial Clk to ADC
                par_data_o:                     out std_logic_vector(15 downto 0);      --Parallel data out
                par_data_rdy_o:         out std_logic   --Data ready flag out
               
        );
end component;

-- Verliog Component declaration
component cross_clk_synch IS
        PORT
        (
                clk_1                   : IN STD_LOGIC;
                clk_2                   : IN STD_LOGIC;
                clk_1_strobe    : IN STD_LOGIC;
                clk_2_strobe    : OUT STD_LOGIC
        );
end component;
	

BEGIN

		------------------------------------------------------------------
	-- PLL
	------------------------------------------------------------------
	
	PLL1_inst: entity work.pll
	port map 
	( 
		areset	=> '0',		-- No reset. This port is needed for the simulation
		inclk0 	=> REFCLK2,	 
		c0 		=> sys_clk, --100MHz
		c1 		=> clk_40, --40MHz
		c2 		=> adc_clk, --250MHz
		locked	=> pll_locked
	);
	

	------------------------------------------------------------------
	-- synchronous reset circuits
	------------------------------------------------------------------
	sys_rst_controller: process (sys_clk, pll_locked) -- rst (from reset) synchronises to sys_clk
	begin
		if pll_locked = '0' then
			rst_sys_clk_n_shift <= (others => '0');      
		elsif rising_edge (sys_clk) then
			rst_sys_clk_n_shift <= rst_sys_clk_n_shift(rst_sys_clk_n_shift'HIGH-1 downto rst_sys_clk_n_shift'LOW) & '1';
		end if;
	end process;
	-- reset is the output of the high bit of the shift register.
	rst_sys_clk_n <= rst_sys_clk_n_shift(rst_sys_clk_n_shift'HIGH);
	
	
	------------------------------------------------------------------
	-- GX reconfiguration components
	------------------------------------------------------------------
	
	GX_reconfig_eth_i: entity work.gx_reconfig
	PORT MAP
	(
		reconfig_clk 		=> clk_40,
		reconfig_fromgxb 	=> reconfig_fromgxb_1 & reconfig_fromgxb_0,
		busy				=> reconfig_busy,
		reconfig_togxb		=> reconfig_togxb
	);
	
	------------------------------------------------------------------
	-- GX reset controllers
	------------------------------------------------------------------
	altgx0_rst_i: entity work.altgx_rst_control
	generic map (
		t_clk_period_ns => sys_clk_ns_period,
		t_ltd_auto_ns => 4000,
		t_digitalreset_ns => 200,
		t_analogreset_ns => 50
	)
	port map (
		--local clock and reset
		clk 	=> sys_clk,
		nrst 	=> rst_sys_clk_n,
	
		--transceiver status
		busy 			=> reconfig_busy,
		pll_locked 		=> pll_locked_opt,
		rx_freqlocked 	=> rx_freqlocked_opt,
	
		--transceiver reset signals
		pll_areset 		=> pll_areset_opt,
		tx_digitalreset => tx_digitalreset_opt,
		rx_analogreset 	=> rx_analogreset_opt,
		rx_digitalreset => rx_digitalreset_opt
	);
	
	altgx1_rst_i: entity work.altgx_rst_control
	generic map (
		t_clk_period_ns => sys_clk_ns_period,
		t_ltd_auto_ns => 4000,
		t_digitalreset_ns => 200,
		t_analogreset_ns => 50
	)
	port map (
		--local clock and reset
		clk 	=> sys_clk,
		nrst 	=> rst_sys_clk_n,
	
		--transceiver status
		busy 			=> reconfig_busy,
		pll_locked 		=> pll_locked_eth,
		rx_freqlocked 	=> rx_freqlocked_eth,
	
		--transceiver reset signals
		pll_areset 		=> pll_areset_eth,
		tx_digitalreset => tx_digitalreset_eth,
		rx_analogreset 	=> rx_analogreset_eth,
		rx_digitalreset => rx_digitalreset_eth
	);
	
	------------------------------------------------------------------
	-- Nios II system
	------------------------------------------------------------------
	
	
    nios_eth_sys_i : nios_eth_sys
        port map (
            clk_clk                => sys_clk,			--             clk.clk
            reset_reset_n          => rst_sys_clk_n,			--           reset.reset_n

            mac_led_an             => led_auto_neg,		--                .led_an
            mac_led_char_err       => open,				--                .led_char_err
            mac_led_link           => led_link,			--                .led_link
            mac_led_disp_err       => open,				--                .led_disp_err
            mac_led_crs            => led_traffic,		--                .led_crs
            mac_led_col            => open,				--                .led_col
			
            --mac_cal_blk_clk_export => sys_clk, 			-- 			mac_cal_blk_clk.export
            --mac_txp                => GXP_TX2,			--                .txp
            --mac_rxp                => GXP_RX2,			--                .rxp
            --mac_ref_clk            => REFCLK1,			--                .ref_clk
            --mac_rx_recovclkout     => open,				--                .rx_recovclkout
            --mac_reconfig_clk       => clk_40,				--                .reconfig_clk
            --mac_reconfig_togxb     => reconfig_togxb,		--                .reconfig_togxb
            --mac_reconfig_fromgxb(4 downto 0)   => reconfig_fromgxb_1,		--                .reconfig_fromgxb
            --mac_reconfig_busy      => reconfig_busy,		--                .reconfig_busy
			
            mac_powerdown			=> open,
            mac_sd_loopback			=> open,
            mac_tbi_tx_clk			=> mac_tbi_tx_clk,
            mac_tbi_tx_d			=> mac_tbi_tx_d,
            mac_tbi_rx_clk			=> mac_tbi_rx_clk,
            mac_tbi_rx_d			=> mac_tbi_rx_d,

			acq_mem_s2_1_address	=> out_address,    		--     acq_mem_s2_1.address
            acq_mem_s2_1_chipselect	=> '1', 				--                 .chipselect
            acq_mem_s2_1_clken		=> '1',      			--                 .clken
            acq_mem_s2_1_readdata	=> open,   				--                 .readdata
            acq_mem_s2_1_write		=> out_wr,      		--                 .write
            acq_mem_s2_1_writedata	=> out_data_swapped,  			--                 .writedata
            acq_mem_s2_1_byteenable	=> "1111", 				--                 .byteenable
            acq_mem_clk2_1_clk		=> sys_clk,      		--   acq_mem_clk2_1.clk
            acq_mem_reset2_1_reset	=> rst_sys_clk_n,   		-- acq_mem_reset2_1.reset
			
			acq_int_pio_export      => buff_rdy,      		--      acq_int_pio.export
            acq_offset_pio_export   => buff_offset,			--   acq_offset_pio.export
			acq_iface_control_pio_export => acq_iface_control, -- acq_iface_control_pio.export
            acq_iface_status_pio_export  => acq_iface_status,   --  acq_iface_status_pio.export
			
			i2c_scl_pio_export		=> SCL_OPT_2,			--         i2c_scl_pio.export
            i2c_sda_pio_export		=> SDA_OPT_2,			--       i2c_sda_pio.export
			
			status_mem_s2_address    => diag_ram_addr,    --     status_mem_s2.address
            status_mem_s2_chipselect => '1', --                       .chipselect
            status_mem_s2_clken      => '1',      --                       .clken
            status_mem_s2_readdata   => open,   --                       .readdata
            status_mem_s2_write      => diag_ram_wr_en,      --                       .write
            status_mem_s2_writedata  => diag_ram_data_swapped,  --                       .writedata
            status_mem_s2_byteenable => "1111", --                       .byteenable
            status_mem_clk2_clk      => sys_clk,      --   status_mem_clk2.clk
            status_mem_reset2_reset  => rst_sys_clk_n,  -- status_mem_reset2.reset
			
			status_int_pio_export	=> status_int,         --        status_int_pio.export
            status_sent_pio_export	=> status_sent,       --        status_sent_pio.export
			
			bledp_packet_generator_0_bledp_data  => sfifo_q,  		-- bledp_packet_generator_0_bledp.data
            bledp_packet_generator_0_bledp_valid => sfifo_valid,  	--                               .valid
			bledp_packet_generator_0_bledp_ready => sfifo_ready,		-- 							 .ready
			
			command_pio_0_export                 => command_pio_0_export,                 --                  command_pio_0.export
            command_pio_1_export                 => command_pio_1_export,                 --                  command_pio_1.export
            command_pio_2_export                 => command_pio_2_export                  --                  command_pio_2.export

        );

		
	out_wr <= out_data_wr or out_chksum_wr;
	buff_offset <= "000000000000000000000" & buff_last_addr;

	TX_DISABLE_1 	<= '0';
	RATE_SELECT_1 	<= '0';
	TX_DISABLE_2 	<= '0';
	RATE_SELECT_2 	<= '0';

	--LED_1 <= led_auto_neg;
	--LED_2 <= led_link;
	--LED_3 <= led_traffic;
	
	LED_1 <= '1';
	LED_2 <= not led_link;
	LED_3 <= '0';
	
	
	------------------------------------------------------------------
	-- Transceiver Instances for:
	-- Ethernet (used by TSE MAC)
	-- Optical link (custom protocol)
	------------------------------------------------------------------
	
	altgx_opt_i : entity work.altgx_ch0 PORT MAP (
		cal_blk_clk	 				=> sys_clk,
		pll_inclk(0) 				=> REFCLK2,
		reconfig_clk	 			=> clk_40,
		reconfig_togxb	 			=> reconfig_togxb,
		rx_analogreset(0)			=> rx_analogreset_opt,
		rx_ctrldetect		 		=> rx_ctrldetect_opt,
		rx_datain(0)	 			=> GXP_RX1,
		rx_digitalreset(0)			=> rx_digitalreset_opt,
		tx_ctrlenable	 			=> tx_ctrlenable_opt,	--one control signal per byte
		tx_datain	 				=> tx_datain_opt,
		tx_digitalreset(0)			=> tx_digitalreset_opt,
		reconfig_fromgxb			=> reconfig_fromgxb_0,
		rx_clkout(0)				=> rx_clkout_opt,
		rx_dataout					=> rx_dataout_opt,
		tx_clkout(0)				=> tx_clkout_opt,
		tx_dataout(0)				=> GXP_TX1,
		pll_areset(0)				=> pll_areset_opt,
		pll_locked(0)				=> pll_locked_opt,
		rx_errdetect				=> rx_errdetect_opt,
		rx_freqlocked(0)			=> rx_freqlocked_opt,
		rx_syncstatus			    => rx_syncstatus_opt,
		rx_enabyteord(0)            => rx_enabyteord_opt,
        rx_byteorderalignstatus(0)  => rx_byteorderalignstatus_opt
	);
    
    -- re-trigger built-in byte ordering module after rising edge of any of the rx_syncstatus bits
    -- rx_syncstatus becomes one when the built-in word aligner finds the synchronization pattern
    -- after the word synchronization the byte order module looks for the byte order pattern
    -- and restores the correct byte order
    rx_enabyteord_opt <= rx_syncstatus_opt(1) or rx_syncstatus_opt(0);
	
	altgx_eth_i : entity work.altgx_ch1 PORT MAP (
		cal_blk_clk	 				=> sys_clk,
		pll_inclk(0) 				=> REFCLK1,
		reconfig_clk	 			=> clk_40,
		reconfig_togxb	 			=> reconfig_togxb,
		rx_analogreset(0) 			=> rx_analogreset_eth,
		rx_datain(0) 				=> GXP_RX2,
		rx_digitalreset(0) 			=> rx_digitalreset_eth,
		tx_datain	 				=> mac_tbi_tx_d,
		tx_digitalreset(0) 			=> tx_digitalreset_eth,
		reconfig_fromgxb	 		=> reconfig_fromgxb_1,
		rx_clkout(0) 				=> mac_tbi_rx_clk,
		rx_dataout	 				=> mac_tbi_rx_d,
		tx_clkout(0) 				=> mac_tbi_tx_clk,
		tx_dataout(0) 				=> GXP_TX2,
		pll_areset(0)				=> pll_areset_eth,
		pll_locked(0)				=> pll_locked_eth,
		rx_freqlocked(0)			=> rx_freqlocked_eth
	);
	
	------------------------------------------------------------------
	-- Protocol for the optical transceiver
	------------------------------------------------------------------
	protocol_tx_i : entity work.protocol_tx 
	PORT MAP (
		--local clock and reset
		proto_clk			=> sys_clk,
		proto_nrst			=> rst_sys_clk_n,
		
		--input port
		proto_data				=> acq_path_out(15 downto 0),
		proto_startofpacket		=> acq_path_out(17),
		proto_endofpacket		=> acq_path_out(16),
		proto_valid				=> acq_path_out_vld,
		proto_ready				=> acq_path_out_next,
		
		--transceiver signals
		tx_digitalreset		=> tx_digitalreset_opt,
		tx_cntrlenable		=> tx_ctrlenable_opt,
		tx_datain			=> tx_datain_opt,
		tx_clockout			=> tx_clkout_opt
	);

	
	------------------------------------------------------------------
	-- Channels multiplexer to the acquisition memory
	------------------------------------------------------------------
	
	ch_mux_rst <= rst_sys_clk_n and not acq_iface_reset;
	
	channel_mux_i: entity work.channel_mux
		generic map(
			ADDR_NBITS		=> 11,			-- number of address bits
			TXBUFF_SIZE		=> 1400,        -- TX buffer size [bytes]
			RESET_ACTIVE	=> '0'          -- polarity of the reset
		)
		port map (
			-- global signals
			clk			=> sys_clk,	
			rst			=> ch_mux_rst,	

			-- source read signals
			ch0_data		=> channel_signals(1).ch_data, 			-- data in
			ch0_data_avail	=> channel_signals(1).ch_data_avail,	-- data available flag
			ch0_data_rd		=> channel_signals(1).ch_data_rd,		-- data read request
			ch1_data		=> channel_signals(2).ch_data, 			-- data in
			ch1_data_avail	=> channel_signals(2).ch_data_avail,	-- data available flag
			ch1_data_rd		=> channel_signals(2).ch_data_rd,		-- data read request
			ch2_data		=> channel_signals(3).ch_data, 			-- data in
			ch2_data_avail	=> channel_signals(3).ch_data_avail,	-- data available flag
			ch2_data_rd		=> channel_signals(3).ch_data_rd,		-- data read request
			ch3_data		=> channel_signals(4).ch_data, 			-- data in
			ch3_data_avail	=> channel_signals(4).ch_data_avail,	-- data available flag
			ch3_data_rd		=> channel_signals(4).ch_data_rd,		-- data read request
			ch4_data		=> channel_signals(5).ch_data, 			-- data in
			ch4_data_avail	=> channel_signals(5).ch_data_avail,	-- data available flag
			ch4_data_rd		=> channel_signals(5).ch_data_rd,		-- data read request
			ch5_data		=> channel_signals(6).ch_data, 			-- data in
			ch5_data_avail	=> channel_signals(6).ch_data_avail,	-- data available flag
			ch5_data_rd		=> channel_signals(6).ch_data_rd,		-- data read request
			ch6_data		=> channel_signals(7).ch_data, 			-- data in
			ch6_data_avail	=> channel_signals(7).ch_data_avail,	-- data available flag
			ch6_data_rd		=> channel_signals(7).ch_data_rd,		-- data read request
			ch7_data		=> channel_signals(8).ch_data, 			-- data in
			ch7_data_avail	=> channel_signals(8).ch_data_avail,	-- data available flag
			ch7_data_rd		=> channel_signals(8).ch_data_rd,		-- data read request

			-- destination write signals
			out_data		=> out_data, 			-- data out
			out_data_wr		=> out_data_wr,			-- data write request
			out_chksum_wr	=> out_chksum_wr,
			out_address		=> out_address,

			-- buffer ready signals
			buff_rdy		=> buff_rdy,
			buff_last_addr	=> buff_last_addr

  );

	-- Nios is little endian so it is better to swap bytes order - Network order is big endian 
	-- Without this it would be necessary to manipulate the data in the software (htonl)
	out_data_swapped(31 downto 24)	<= out_data(7 downto 0);
	out_data_swapped(23 downto 16)	<= out_data(15 downto 8);
	out_data_swapped(15 downto 8)	<= out_data(23 downto 16);
	out_data_swapped(7 downto 0)	<= out_data(31 downto 24);
	
	------------------------------------------------------------------
	-- generate loop for the acquisition logic (8 channels)
	------------------------------------------------------------------
	
	ch_acquisition_gen: for I in 1 to 8 generate
		
		acq_adc_i: AD7626 
		generic map
		(
			t_clk_period_ns => 4
		)
		port map 
		( 
			clk_i			=> adc_clk,
			adc_cnv_start_i	=> adc_run(I),
			dco_i			=> DCO(I),
			d_i				=> D(I),
			clk_o			=> CLK(I),
			cnv_o			=> CNV(I),
			par_data_o		=> adc_data(I),
			par_data_rdy_o	=> adc_vld(I)
		);
		
		-- synchronize sys clk to adc clk domain
		sys_to_adc_clk_i: cross_clk_synch
		port map 
		(
			clk_1 			=> sys_clk,
			clk_2			=> adc_clk,
			clk_1_strobe 	=> adc_run_acq(I),
			clk_2_strobe	=> adc_run(I)
		);
		
		-- synchronize adc clk to sys clk domain
		adc_to_sys_clk_i: cross_clk_synch
		port map 
		(
			clk_1 			=> adc_clk,
			clk_2			=> sys_clk,
			clk_1_strobe 	=> adc_vld(I),
			clk_2_strobe	=> adc_vld_acq(I)
		);
		
		--data processing component for each channel
		data_combine_i: entity work.bledp_acquisition 
		generic map
		(
			t_clk_period_ns		=> sys_clk_ns_period,
			t_integration_ns	=> 2000,
			ADC_dynamic_range	=> false
		)
		port map 
		(
			clk 				=> sys_clk,
			nrst				=> rst_sys_clk_n,
			adc_run 			=> adc_run_acq(I),
			adc_next			=> adc_next(I),
			adc_vld				=> adc_vld_acq(I),
			adc_data			=> adc_data(I),
			fdfc_pos			=> COUNT_M(I),
			fdfc_neg			=> COUNT_P(I),
			saturation			=> open,
			period_pulse 		=> period_pulse(I),
			raw_data			=> raw_data(I),
			adc_range_reset		=> cnt_1s_done,
			adc_fixed_range_max	=> adc_fixed_range_max,
			adc_fixed_range_min	=> adc_fixed_range_min,
			adc_max				=> adc_max(I),
			adc_min				=> adc_min(I),
			data				=> proc_data(I),
			data_debug			=> data_debug(I),
			data_en				=> proc_data_en(I)
		);
		
		low_current_filter_i: entity work.low_current_filter
		generic map (
			current_threshold => 500
		)
		port map (
			clk 			=> sys_clk,
			nrst			=> rst_sys_clk_n,
			fdfc_pos		=> COUNT_M(I),
			fdfc_neg		=> COUNT_P(I),
			period_pulse 	=> period_pulse(I),
			enable_filter	=> enable_filter(I),
			-- input data
			data_in 		=> proc_data(I),
			data_in_en 		=> proc_data_en(I),
			-- output data
			data_out 		=> proc_data_filt(I),
			data_out_en 	=> proc_data_filt_en(I)
		);
		
		
		-- increasing counter to insert to the channel's data payload
		-- for processed data it has 6 bits (counts from 0 to 63)
		proc_inc_dbg_cnt_p: process(sys_clk, rst_sys_clk_n)
		begin
			if rst_sys_clk_n = '0' then
				proc_inc_dbg_cnt(I) <= (others=>'0');
			elsif rising_edge(sys_clk) then
				if acq_iface_reset = '1' then
					proc_inc_dbg_cnt(I) <= (others=>'0');
				elsif proc_data_filt_en(I) = '1' then
				--elsif proc_data_en(I) = '1' then
					proc_inc_dbg_cnt(I) <= proc_inc_dbg_cnt(I) + 1;
				end if;
			end if;
		end process;
		
		--FIFO component for each channel
		--it is written by the acquisition component
		--it is read by the channel_mux component
		ch_fifo_i: entity work.channel_fifo
			port map
			(
				aclr		=> acq_iface_reset,
				
				data		=> channel_signals(I).ch_input,
				wrclk		=> sys_clk,
				wrreq		=> channel_signals(I).ch_in_wr,
				
				rdclk		=> sys_clk,
				rdreq		=> channel_signals(I).ch_data_rd,
				q			=> channel_signals(I).ch_data,
				rdempty		=> channel_signals(I).ch_fifo_empty,
				rdfull		=> channel_signals(I).ch_error
			);
			
		--connect fifo read side to the channel_mux component
		channel_signals(I).ch_data_avail <= not channel_signals(I).ch_fifo_empty;
		ch_fifo_full(I) <= channel_signals(I).ch_error;
		
		-- generate no test mode logic
		no_test_mode_gen : if raw_comb_test_mode = false generate
			--connect fifo write side to the bledp_acquisition component
			--when write to the channel is enabled (ch_fifo_wr_en) write the processed data when it is available (proc_data_filt_en)
			channel_signals(I).ch_in_wr <=	ch_fifo_wr_en(I) and proc_data_filt_en(I) when DADC_en(I-1) = '0' else
											ch_fifo_wr_en(I) and adc_vld_acq(I);
			--combine BLEDP frame: proc_inc_dbg_cnt & proc_data
			channel_signals(I).ch_input <= 	"000" & std_logic_vector(to_unsigned(I-1 ,3)) & std_logic_vector(proc_inc_dbg_cnt(I)) & proc_data_filt(I) when DADC_en(I-1) = '0' else
											"001" & std_logic_vector(to_unsigned(I-1 ,3)) & std_logic_vector(proc_inc_dbg_cnt(I)) & "0000" & adc_data_abs(I);
		end generate no_test_mode_gen;
		
		-- generate test mode logic - 4 channels with combined data (ch 1 to 4) and the same data in raw mode (ch 5 to 8)
		raw_comb_test_mode_gen : if raw_comb_test_mode = true generate
			ch1to4_gen : if I < 5 generate
				channel_signals(I).ch_in_wr <= 	ch_fifo_wr_en(I) and proc_data_filt_en(I) when DADC_en(I-1) = '0' else
												ch_fifo_wr_en(I) and adc_vld_acq(I);
				channel_signals(I).ch_input <=	"000" & std_logic_vector(to_unsigned(I-1 ,3)) & std_logic_vector(proc_inc_dbg_cnt(I)) & proc_data_filt(I) when DADC_en(I-1) = '0' else
												"001" & std_logic_vector(to_unsigned(I-1 ,3)) & std_logic_vector(proc_inc_dbg_cnt(I)) & "0000" & adc_data_abs(I);
			end generate ch1to4_gen;
			ch5to8_gen : if I >= 5 generate
				channel_signals(I).ch_in_wr <= 	ch_fifo_wr_en(I) and proc_data_en((I-4)) when DADC_en(I-1) = '0' else
												ch_fifo_wr_en(I) and adc_vld_acq((I-4));
				channel_signals(I).ch_input <=	"000" & std_logic_vector(to_unsigned(I-1 ,3)) & std_logic_vector(proc_inc_dbg_cnt((I-4))) & data_debug((I-4)) when DADC_en(I-1) = '0' else
												"001" & std_logic_vector(to_unsigned(I-1 ,3)) & std_logic_vector(proc_inc_dbg_cnt((I-4))) & "0000" & adc_data_abs((I-4));
			end generate ch5to8_gen;
		end generate raw_comb_test_mode_gen;
		
		
		adc_data_abs(I) <= std_logic_vector(abs(signed(adc_data(I))+20));
		
		
	end generate ch_acquisition_gen;
	
	acq_iface_status <= ch_fifo_full & sfifo_full & "0000000000000000000" & POS4 & POS3 & POS2 & POS1;
	acq_iface_reset <= acq_iface_control(0);
	stream_fifo_reset <= acq_iface_control(1);
	ch_fifo_wr_en(8) <= acq_iface_control(31);
	ch_fifo_wr_en(7) <= acq_iface_control(30);
	ch_fifo_wr_en(6) <= acq_iface_control(29);
	ch_fifo_wr_en(5) <= acq_iface_control(28);
	ch_fifo_wr_en(4) <= acq_iface_control(27);
	ch_fifo_wr_en(3) <= acq_iface_control(26);
	ch_fifo_wr_en(2) <= acq_iface_control(25);
	ch_fifo_wr_en(1) <= acq_iface_control(24);
	
	sfifo_wr_enable <= acq_iface_control(23);
	
	--FIFO component for streaming interface
	stream_fifo_i: entity work.stream_fifo
	port map
	(
		clock		=> sys_clk,
		
		sclr		=> stream_fifo_reset,
		
		data		=> out_data,
		wrreq		=> sfifo_wr,
		
		rdreq		=> sfifo_ready,
		q			=> sfifo_q,
		empty		=> sfifo_empty,
		full		=> sfifo_full
	);
	
	sfifo_valid <= not sfifo_empty;	
	sfifo_wr <= sfifo_wr_enable and out_data_wr;
	
	
	
	------------------------------------------------------------------
	-- Acquisition Path
	------------------------------------------------------------------
    
    
    -- increasing counter to insert to the acquisition packets sent via the optical links
    acq_cnt_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' then
                acq_cnt <= (others=>'0');
            elsif acq_path_fifo_in_vld = '1' then
                acq_cnt <= acq_cnt + 1;
            end if;
        end if;
    end process;
    
	acq_path_fifo_in_vld <= proc_data_filt_en(1);
	
	acq_path_i: entity work.acq_packet_creator
	port map
	(
        --clock and reset
        gen_clk     => sys_clk,
        gen_nrst    => rst_sys_clk_n, 
        
        --acquisition channel inputs
        acq_ch1     => proc_data_filt(1),
        acq_ch2     => proc_data_filt(2),
        acq_ch3     => proc_data_filt(3),
        acq_ch4     => proc_data_filt(4),
        acq_ch5     => proc_data_filt(5),
        acq_ch6     => proc_data_filt(6),
        acq_ch7     => proc_data_filt(7),
        acq_ch8     => proc_data_filt(8),
        
        --header information
        gen_timestamp   => std_logic_vector(acq_cnt),
        gen_card_no     => x"41",
        gen_pkt_type    => x"00",
        
        --generator output port
        gen_data            => acq_path_out(15 downto 0),
        gen_startofpacket   => acq_path_out(17),
        gen_endofpacket     => acq_path_out(16),
        gen_valid           => acq_path_out_vld,
        gen_ready           => acq_path_out_next,
        
        --control
        gen_done            => open,
        gen_enable          => acq_path_fifo_in_vld
	);
	
	------------------------------------------------------------------
	-- status data logic
	------------------------------------------------------------------
	
	-- 1s free running counter (status data rate)
	status_per_p: process(sys_clk, rst_sys_clk_n)
	begin
		if rst_sys_clk_n = '0' then
			cnt_1s <= 0;
		elsif rising_edge(sys_clk) then
			if cnt_1s_done = '0' then
				cnt_1s <= cnt_1s - 1;
			else
				cnt_1s <= (sys_clk_freq-1);
			end if;
		end if;
	end process;
	
	cnt_1s_done <= '1' when cnt_1s = 0 else '0';
	
	
	------------------------------------------------------------------
	-- power supply enable and monitor logic
	------------------------------------------------------------------
	
	ON_CB1		<= command_pio_0_export(0);
	ON_CB2		<= command_pio_0_export(1);
	DCDC_ENABLE	<= '0'; -- not command_pio_0_export(2);
	
	--=================--
	-- FDFC Startup and saturation (SS) controller
	--=================--
	
	ss_ctrl_cb1_gen: for i in 1 to 4 generate
		fdfc_ss_controller_i: entity work.fdfc_ss_controller 
		GENERIC MAP
		(
			Tclk_ns => sys_clk_ns_period
		)
		PORT MAP
		(
			clk			=> sys_clk,
			nrst		=> rst_sys_clk_n,
			ON_CB		=> command_pio_0_export(0),
			STOPOUT 	=> STOPOUT(i),
			COUNT_P		=> COUNT_P(i),
			COUNT_M		=> COUNT_M(i),
			CLR			=> CLR(i),
			PRE			=> PRE(i),
			STOPL		=> STOPL(i),
			startup_dir	=> '0',
			saturation	=> open,
			startup		=> open
		);		
	end generate;
	
	ss_ctrl_cb2_gen: for i in 5 to 8 generate
		fdfc_ss_controller_i: entity work.fdfc_ss_controller 
		GENERIC MAP
		(
			Tclk_ns => sys_clk_ns_period
		)
		PORT MAP
		(
			clk			=> sys_clk,
			nrst		=> rst_sys_clk_n,
			ON_CB		=> command_pio_0_export(1),
			STOPOUT 	=> STOPOUT(i),
			COUNT_P		=> COUNT_P(i),
			COUNT_M		=> COUNT_M(i),
			CLR			=> CLR(i),
			PRE			=> PRE(i),
			STOPL		=> STOPL(i),
			startup_dir	=> '0',
			saturation	=> open,
			startup		=> open
		);		
	end generate;
	
	------------------------------------
	-- Digital Potentiometer Glue Logic
	------------------------------------
	
	
	gl_digpot: for i in 1 to 8 generate
        
        -- create access ctrl bit
        dp_acc(i)   <= dp_toggle_bit(i) XOR command_pio_0_export(i+11);  --command_pio_0_export(19 downto 12)
        
        process(all) is
        begin
            --DEFAULT
            dp_wrreq_vld_int(i)	<= '0';
            dp_toggle_en(i)     <= '0';
            
            if dp_acc(i) = '1' then
                if dp_wrreq_next(i) = '1' then
                    dp_wrreq_vld_int(i) <= '1';
                    dp_toggle_en(i)     <= '1';
                end if;
            end if;
            
        end process;
        
        -- Output Register for REQ and WRITE CMI
        process (sys_clk) is
        begin
            if rising_edge(sys_clk) then
                if dp_wrreq_next(i) = '1' then
                    dp_wrreq_vld(i)     <= dp_wrreq_vld_int(i);
                    dp_wrreq_data(i)    <= "00" & "00" & command_pio_0_export(20) & "000" & command_pio_0_export(11) & '1' & command_pio_0_export(10 downto 3);
                end if;
            end if;
        end process;
        
        -- update toggle bit
        process (sys_clk) is
        begin
            if rising_edge(sys_clk) then
                if dp_toggle_en(i) = '1' then
                    dp_toggle_bit(i) <= NOT dp_toggle_bit(i);
                end if;
            end if;
        end process;

	
	end generate;
	
	------------------------------------
	-- NIOS Commands Connections
	------------------------------------
	
	enable_filter(8 downto 1) <= command_pio_2_export(7 downto 0);
	raw_data(8 downto 1) <= command_pio_2_export(15 downto 8);
	DADC_en <= command_pio_2_export(23 downto 16);
	relay_set <= command_pio_2_export(31 downto 24);
	
	adc_fixed_range_max <= command_pio_1_export(31 downto 16);
	adc_fixed_range_min <= command_pio_1_export(15 downto 0);
	
	
	RELAY_ACTIVATION_FPGA_1 <= relay_set(0);
	RELAY_ACTIVATION_FPGA_2 <= relay_set(1);
	RELAY_ACTIVATION_FPGA_3 <= relay_set(2);
	RELAY_ACTIVATION_FPGA_4 <= relay_set(3);
	RELAY_ACTIVATION_FPGA_5 <= relay_set(4);
	RELAY_ACTIVATION_FPGA_6	<= relay_set(5);
	RELAY_ACTIVATION_FPGA_7 <= relay_set(6);
	RELAY_ACTIVATION_FPGA_8 <= relay_set(7);
	
	
	
	--====================--
    -- DIAGONOSTIC READER --
    --====================--
    
    diag_read:  entity work.bledp_diag_reader
    GENERIC MAP
    (
        Tclk_ns         => sys_clk_ns_period,
        pkg_data_size   => pkg_data_size,
        pkg_tag_size    => pkg_tag_size,
		sfp1_n_tags     => 46,                  -- 46 for Optical SFP, 36 for Ethernet SFP
		sfp1_mif_file   => "sgen_sfp_opt.mif",  -- _opt for optical,_eth for ethernet
		sfp2_n_tags     => 36,                  -- 46 for Optical SFP, 36 for Ethernet SFP
		sfp2_mif_file   => "sgen_sfp_eth.mif"  -- _opt for optical, _eth for ethernet
    )
    PORT MAP
    (
        clk                 => sys_clk,
        POWERGOOD_1_2       => POWERGOOD_1_2,
        POWERGOOD_CB1       => POWERGOOD_CB1,
        POWERGOOD_CB2       => POWERGOOD_CB2,
        CB1_ACT             => command_pio_0_export(0),
        CB2_ACT             => command_pio_0_export(1),
        DCDC_EN             => command_pio_0_export(2),
        bledp_fw            => x"4E494F53",			--"NIOS"
        ID                  => ID,
        global_addr         => POS4 & POS3 & POS2 & POS1,
        SHT15_DATA          => SHT15_DATA,
        SHT15_SCK           => SHT15_SCK,
        RELAY_ACT           => relay_set,
        RELAYS_STATUS       => RELAYS_STATUS_FPGA,
        STOPOUT             => STOPOUT,
        STOPOUT_smpl        => proc_data_en,
        AD_I_CLK            => AD_I_CLK,
        AD_I_CS_N           => AD_I_CS_N,
        AD_I_DIN            => AD_I_DIN,
        AD_I_DOUT           => AD_I_DOUT,
        SDA                 => SDA,
        SCL                 => SCL,
        WP                  => WP,
        CB1_wakeup          => command_pio_0_export(0),
        CB2_wakeup          => command_pio_0_export(1),
        DP_wr_req_data      => dp_wrreq_data,
        DP_wr_req_vld       => dp_wrreq_vld,
        DP_wr_req_next      => dp_wrreq_next,
        SDA_OPT_1           => SDA_OPT_1,
        SCL_OPT_1           => SCL_OPT_1,
        SFP1_wr_req_data    => (16 downto 0 => '0'),
        SFP1_wr_req_vld     => '0',
        SFP1_wr_req_next    => open,
        LOSS_OF_SIGNAL_1    => LOSS_OF_SIGNAL_1,
        MODULE_DETECT_1     => MODULE_DETECT_1,
        TX_FAULT_1          => TX_FAULT_1,
        RATE_SELECT_1       => '0',
        TX_DISABLE_1        => '0',
        SDA_OPT_2           => open,
        SCL_OPT_2           => open,
        SFP2_wr_req_data    => (16 downto 0 => '0'),
        SFP2_wr_req_vld     => '0',
        SFP2_wr_req_next    => open,
        LOSS_OF_SIGNAL_2    => LOSS_OF_SIGNAL_2,
        MODULE_DETECT_2     => MODULE_DETECT_2,
        TX_FAULT_2          => TX_FAULT_2,
        RATE_SELECT_2       => '0',
        TX_DISABLE_2        => '0',
        diag_data           => diag_tx_data,
        diag_vld            => diag_tx_vld,
        diag_next           => diag_tx_next,
        upd                 => status_int,
		DAmode              => "00000000",
		DAmode_smpl         => "11111111"
    );

    
    --===============--
    -- RDOUT STORAGE --
    --===============--
    
    -- CMI DIAG --
    diag_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => sys_clk,
        tx_data                 => diag_tx_data,
        tx_vld                  => diag_tx_vld,
        tx_next                 => diag_tx_next,
        rx_data                 => diag_rx_data,
        scalarize(rx_vld)       => diag_rx_vld,
        rx_next                 => vectorize(diag_rx_next)
    );
    
    -- Scoreboard --
    sb: entity work.scoreboard
    GENERIC MAP
    (
        pkg_data_size   => pkg_data_size,
        word_n          => scoreboard_words
    )
    PORT MAP
    (
        clk                 => sys_clk,
        pkg_data            => diag_rx_data,
        pkg_vld             => diag_rx_vld,
        pkg_next            => diag_rx_next,
        pkg_scored_data     => sb_tx_data,
        pkg_scored_vld      => sb_tx_vld,
        pkg_scored_next     => sb_tx_next,
        trigger             => status_int
    );
    
    -- CMI SB --
    sb_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => sys_clk,
        tx_data                 => sb_tx_data,
        tx_vld                  => sb_tx_vld,
        tx_next                 => sb_tx_next,
        rx_data                 => sb_rx_data,
        scalarize(rx_vld)       => sb_rx_vld,
        rx_next                 => vectorize(sb_rx_next)
    );
    
    
    -- FIFO --
    fifo: entity work.fifo_CMA
    GENERIC MAP
    (
        data_size   => pkg_data_size,
        fifo_depth  => 512
    )
    PORT MAP
    (
        clk                 => sys_clk,
        inp_data            => sb_rx_data,
        inp_vld             => sb_rx_vld,
        inp_next            => sb_rx_next,
        out_data            => fifo_tx_data,
        out_vld             => fifo_tx_vld,
        out_next            => fifo_tx_next
    );
    
    
    -- CMI FIFO --
    fifo_CMI: entity work.old_CMI
    GENERIC MAP
    (
        rx_number   => 1,
        data_size   => pkg_data_size,
        outreg_incl => 0
    )
    PORT MAP
    (
        clk                     => sys_clk,
        tx_data                 => fifo_tx_data,
        tx_vld                  => fifo_tx_vld,
        tx_next                 => fifo_tx_next,
        rx_data                 => fifo_rx_data,
        scalarize(rx_vld)       => fifo_rx_vld,
        rx_next                 => vectorize(fifo_rx_next)
    );
    
	-- status interrupt
	status_int <= cnt_1s_done;
	
	-- set busy flag after an update pulse
	-- it is reset by Nios as soon as the RAM is read
	diag_bsy_p: process(sys_clk, rst_sys_clk_n)
	begin
		if rst_sys_clk_n = '0' then
			diag_ram_rdout_busy  <= '0';
		elsif rising_edge(sys_clk) then
			if status_int = '1' then
				diag_ram_rdout_busy  <= '1';
			elsif status_sent = '1' then
				diag_ram_rdout_busy  <= '0';
			end if;
		end if;
	end process;	
    
    
    -- Package to RAM --
    pkgtoram: entity work.pkg_to_ram
    GENERIC MAP
    (
        pkg_data_size   => pkg_data_size,
        ram_data_size   => diag_ram_data_size,
        ram_addr_size   => diag_ram_addr_size
    )
    PORT MAP
    (
        clk                 => sys_clk,
        pkg_data            => fifo_rx_data,
        pkg_vld             => fifo_rx_vld,
        pkg_next            => fifo_rx_next,
        addr                => diag_ram_addr,
        data                => diag_ram_data,
        wr_en               => diag_ram_wr_en,
        busy                => diag_ram_rdout_busy
    );
	
	diag_ram_data_swapped(31 downto 24)	<= diag_ram_data(7 downto 0);
	diag_ram_data_swapped(23 downto 16)	<= diag_ram_data(15 downto 8);
	diag_ram_data_swapped(15 downto 8)	<= x"02";
	diag_ram_data_swapped(7 downto 0)	<= x"80";
	
end architecture rtl;
