-- Company:		CERN - BE/BI/BL
-- Engineer:	Maciej Kwiatkowski
-- Create Date: 06/07/2012
-- Module Name:	low_current_filter
-- Description:	module to remove FDFC transition spikes from the low current values

-- combiner output:  A0         A1         A2          A3          A4
-- fdfc commutation: _________________/\______________________________
-- filter output:    A0         A1         A1(st1)     A1(st2)     A4
-- A1 must be under the current_threshold


LIBRARY IEEE;
USE IEEE.STD_Logic_1164.ALL;
USE IEEE.Numeric_STD.ALL;


entity low_current_filter is
	generic (
		current_threshold 	: natural := 50
		);
	port (
		-- clock
		clk		: in std_logic;
		-- reset
		nrst	: in std_logic;
		-- integrator interface
		fdfc_pos : in std_logic;
		fdfc_neg : in std_logic;
		-- measurement period pulse
		period_pulse : in std_logic;
		-- control/status
		enable_filter : in std_logic;
		-- input data
		data_in : in std_logic_vector(19 downto 0);
		data_in_en : in std_logic;
		-- output data
		data_out : out std_logic_vector(19 downto 0);
		data_out_en : out std_logic
		);
end low_current_filter;

------------------------------------------------------------------
architecture rtl of low_current_filter is 
------------------------------------------------------------------

attribute keep: boolean;

-- FDFC pulses flip-flops and edge detector signals
signal fdfc_neg_ff1 : std_logic;
signal fdfc_neg_ff2 : std_logic;
signal fdfd_pos_ff1 : std_logic;
signal fdfd_pos_ff2 : std_logic;
signal fdfc_neg_fedge : std_logic;
signal fdfc_pos_fedge : std_logic;
signal fdfc_any_fedge : std_logic;

-- previous value register
signal data_in_a2 : std_logic_vector(19 downto 0);
signal data_in_a1 : std_logic_vector(19 downto 0);
-- current value register
signal data_in_a3 : std_logic_vector(19 downto 0);
signal data_in_a3_en : std_logic;
-- commutation glitch signals
signal commutation_glitch_a3 : std_logic;
signal commutation_glitch_a2 : std_logic;
signal commutation_glitch_a1 : std_logic;
-- filter signals
signal filter_data_st1 : std_logic;
signal filter_data_st2 : std_logic;

attribute keep of fdfc_neg_fedge: signal is true;
attribute keep of fdfc_pos_fedge: signal is true;
attribute keep of commutation_glitch_a3: signal is true;
attribute keep of commutation_glitch_a2: signal is true;
attribute keep of commutation_glitch_a1: signal is true;
attribute keep of period_pulse: signal is true;
attribute keep of filter_data_st1: signal is true;
attribute keep of enable_filter: signal is true;
attribute keep of data_in_a2: signal is true;
attribute keep of data_in_a3: signal is true;
attribute keep of data_in_a3_en: signal is true;
attribute keep of data_in: signal is true;
attribute keep of data_in_en: signal is true;


begin

-- assertions
assert current_threshold < 2**16-1 report "Current threshold is higher than the full ADC range" severity error;
-- simulation assertions
-- build a testbench which verifies the following cases
assert not (period_pulse = '1' and fdfc_any_fedge = '1') report "TB case 1: Integration period ends when there is the FDFC switch. Verify fdfc_any_fedge and its influence on the output data" severity warning;

-- FDFC negative pulses falling edge detector
fdfc_neg_ff_p: process(clk, nrst)
begin
	if nrst = '0' then
		fdfc_neg_ff1 <= '0';
		fdfc_neg_ff2 <= '0';
	elsif rising_edge(clk) then
		fdfc_neg_ff1 <= fdfc_neg;
		fdfc_neg_ff2 <= fdfc_neg_ff1;
	end if;
end process;
fdfc_neg_fedge <= not fdfc_neg_ff1 and fdfc_neg_ff2;

-- FDFC positive pulses falling edge detector
fdfc_pos_ff_p: process(clk, nrst)
begin
	if nrst = '0' then
		fdfd_pos_ff1 <= '0';
		fdfd_pos_ff2 <= '0';
	elsif rising_edge(clk) then
		fdfd_pos_ff1 <= fdfc_pos;
		fdfd_pos_ff2 <= fdfd_pos_ff1;
	end if;
end process;
fdfc_pos_fedge <= not fdfd_pos_ff1 and fdfd_pos_ff2;

fdfc_any_fedge <= fdfc_neg_fedge or fdfc_pos_fedge;

-- register current combined value
curr_d_reg_p: process(clk, nrst)
begin
	if nrst = '0' then
		data_in_a3 <= (others=>'0');
		data_in_a3_en <= '0';
	elsif rising_edge(clk) then
		if data_in_en = '1' then
			data_in_a3 <= data_in;
		end if;
		-- data strobe
		data_in_a3_en <= data_in_en;
	end if;
end process;

-- register previous combined values
prev_d_reg_p: process(clk, nrst)
begin
	if nrst = '0' then
		data_in_a2 <= (others=>'0');
		data_in_a1 <= (others=>'0');
	elsif rising_edge(clk) then
		if data_in_en = '1' then
			data_in_a2 <= data_in_a3;
			data_in_a1 <= data_in_a2;
		end if;
	end if;
end process;

-- register commutation glitch if present in the current integration period
curr_g_reg_p: process(clk, nrst)
begin
	if nrst = '0' then
		commutation_glitch_a3 <= '0';
	elsif rising_edge(clk) then
		if period_pulse = '1' and fdfc_any_fedge = '0' then
			commutation_glitch_a3 <= '0';
		elsif fdfc_any_fedge = '1' then
			commutation_glitch_a3 <= '1';
		end if;
	end if;
end process;

-- register commutation glitch before data_in_en arrive
prev_g_reg_p: process(clk, nrst)
begin
	if nrst = '0' then
		commutation_glitch_a2 <= '0';
		commutation_glitch_a1 <= '0';
	elsif rising_edge(clk) then
		if period_pulse = '1' then
			commutation_glitch_a2 <= commutation_glitch_a3;
			commutation_glitch_a1 <= commutation_glitch_a2;
		end if;
	end if;
end process;

--decide if filtering should be applied (in 2 stages)
filter_data_st1 <= '1' when unsigned(data_in_a2) <= current_threshold and commutation_glitch_a2 = '1' and enable_filter = '1' else '0';
filter_data_st2 <= '1' when unsigned(data_in_a1) <= current_threshold and commutation_glitch_a1 = '1' and enable_filter = '1' else '0';

-- register output data
out_reg_p: process(clk, nrst)
begin
	if nrst = '0' then
		data_out_en <= '0';
		data_out <= (others=>'0');
	elsif rising_edge(clk) then
		if filter_data_st1 = '1' then
			data_out <= data_in_a2;
		elsif filter_data_st2 = '1' then
			data_out <= data_in_a1;
		else
			data_out <= data_in_a3;
		end if;
		data_out_en <= data_in_a3_en;
	end if;
end process;



end rtl;

