# Example tcl script (Modelsim/Questa)
vlib lib
vmap work lib

# compile just once to save the time
# uncomment if compiled for the first time
# path is specific for each EDA installation
#vcom -93 C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/220pack.vhd 
#vcom -explicit C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/220model.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf_components.vhd 
#vcom -93 C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf.vhd 

vcom -novopt -O0 "../../../src/multiplier.vhd"
vcom -novopt -O0 "../../../src/low_current_filter.vhd"
vcom -novopt -O0 "../../../src/bledp_acquisition.vhd"

#Analog devices AD7626 module
vlog -novopt -O0 "../../../src/AD7626.v"

#Marcel's AD7626 module dependencies
#vcom -novopt -O0 "../cores/ddrclk.vhd"
#vcom -2008 -novopt -O0 "../../../../../library/modules/vhdl/packages/vhdl_func_pkg.vhd"

#Marcel's AD7626 module
#vcom -2008 -novopt -O0 "../../../../../library/modules/vhdl/interfaces/AD7626.vhd"

vlog -novopt -O0 "../src/TB_bledp_acquisition.v"

vsim -novopt work.TB_bledp_acquisition
#add wave *
#TB signals
add wave sim:/TB_bledp_acquisition/clk
#add wave sim:/TB_bledp_acquisition/fdfc_pos
#add wave sim:/TB_bledp_acquisition/fdfc_neg
add wave sim:/TB_bledp_acquisition/analog_ramp_ns
add wave sim:/TB_bledp_acquisition/analog_step
#add wave sim:/TB_bledp_acquisition/analog_noise
add wave sim:/TB_bledp_acquisition/analog
#add wave sim:/TB_bledp_acquisition/uut/fdfc_neg_fedge
#add wave sim:/TB_bledp_acquisition/uut/fdfc_pos_fedge
add wave sim:/TB_bledp_acquisition/uut/fdfc_slope
#add wave sim:/TB_bledp_acquisition/uut/fdfc_slope_1
#add wave sim:/TB_bledp_acquisition/uut/fdfc_slope_2
add wave sim:/TB_bledp_acquisition/out_data_reg
#add wave sim:/TB_bledp_acquisition/current_reg
#add wave sim:/TB_bledp_acquisition/adc_run
#add wave sim:/TB_bledp_acquisition/adc_vld
#add wave sim:/TB_bledp_acquisition/adc_d
add wave sim:/TB_bledp_acquisition/adc_data
add wave sim:/TB_bledp_acquisition/fdfc_pos
add wave sim:/TB_bledp_acquisition/fdfc_neg
#add wave sim:/TB_bledp_acquisition/adc_clk
#add wave sim:/TB_bledp_acquisition/adc_clk_echo
#add wave sim:/TB_bledp_acquisition/adc_cnv
#add wave sim:/TB_bledp_acquisition/ADC_sample_hold
#add wave sim:/TB_bledp_acquisition/integration_period
#add wave sim:/TB_bledp_acquisition/sim_fdfc_pulses
#UUT signals
#add wave sim:/TB_bledp_acquisition/uut/fdfc_neg_cnt
#add wave sim:/TB_bledp_acquisition/uut/fdfc_pos_cnt

#add wave sim:/TB_bledp_acquisition/uut/fdfc_slope
#add wave sim:/TB_bledp_acquisition/uut/fdfc_slope_1
#add wave sim:/TB_bledp_acquisition/uut/fdfc_slope_2
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff_1
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff_2
#add wave sim:/TB_bledp_acquisition/uut/saturation

#add wave sim:/TB_bledp_acquisition/uut/data
#add wave sim:/TB_bledp_acquisition/uut/data_en
add wave sim:/TB_bledp_acquisition/uut/p_cnt_done
#add wave sim:/TB_bledp_acquisition/uut/fdfc_neg_reg
#add wave sim:/TB_bledp_acquisition/uut/fdfc_pos_reg
#add wave sim:/TB_bledp_acquisition/uut/fdfc_count_reg_en
#add wave sim:/TB_bledp_acquisition/uut/fdfc_counts_sum
#add wave sim:/TB_bledp_acquisition/uut/fdfc_counts_sum_en
#add wave sim:/TB_bledp_acquisition/uut/adc_range_mul_counts
#add wave sim:/TB_bledp_acquisition/uut/adc_range_mul_counts_en
add wave sim:/TB_bledp_acquisition/adc_vld
add wave sim:/TB_bledp_acquisition/uut/adc_data_hold
add wave sim:/TB_bledp_acquisition/uut/adc_sample_1
add wave sim:/TB_bledp_acquisition/uut/adc_sample_2
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_en
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff_1
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff_2
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff_en
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff_abs
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff_abs_1
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff_abs_2
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diffs_abs_sum
#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff_abs_en
#add wave sim:/TB_bledp_acquisition/uut/data_combined_case_1
#add wave sim:/TB_bledp_acquisition/uut/data_combined_case_2_3
#add wave sim:/TB_bledp_acquisition/uut/zero_counts
#add wave sim:/TB_bledp_acquisition/data
#add wave sim:/TB_bledp_acquisition/data_en

#add wave sim:/TB_bledp_acquisition/uut/adc_sample_diff
#add wave sim:/TB_bledp_acquisition/uut/fixed_ADC_range/adc_max_val
#add wave sim:/TB_bledp_acquisition/uut/fixed_ADC_range/adc_min_val



#add wave sim:/TB_bledp_acquisition/adc_min
#add wave sim:/TB_bledp_acquisition/adc_max
add wave sim:/TB_bledp_acquisition/uut/adc_max_reg
add wave sim:/TB_bledp_acquisition/uut/adc_min_reg
#add wave sim:/TB_bledp_acquisition/uut/dynamic_ADC_range/scan_max_reg
#add wave sim:/TB_bledp_acquisition/uut/dynamic_ADC_range/scan_min_reg


#add wave sim:/TB_bledp_acquisition/uut1/commutation_glitch
#add wave sim:/TB_bledp_acquisition/uut1/data_in_curr
#add wave sim:/TB_bledp_acquisition/uut1/data_in_prev
#add wave sim:/TB_bledp_acquisition/uut1/filter_data

run -all
wave zoomfull
