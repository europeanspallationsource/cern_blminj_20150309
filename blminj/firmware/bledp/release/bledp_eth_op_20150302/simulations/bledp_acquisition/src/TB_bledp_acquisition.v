
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_bledp_acquisition;

reg clk;
reg nrst;
reg fdfc_pos = 1;
reg fdfc_neg = 1;
reg raw_data;
reg adc_range_reset;
reg [15:0] adc_fixed_range_max;
reg [15:0] adc_fixed_range_min;
wire saturation;
wire signed [15:0] adc_min;
wire signed [15:0] adc_max;

reg i = 0;
reg j = 0;
integer ADC_bit_readout = 0;
reg slope = 0;
//real analog_real = 0;
real analog_real = 32700;
reg signed [15:0] analog = 0;
reg signed [15:0] ADC_sample_hold[1:0];
reg adc_d = 0;
integer analog_fdfc_lnt_cnt = 0;
integer analog_ramp_ns = 0;
integer analog_inc = 0;
integer analog_noise = 0;
real analog_step = 0;
integer sim_fdfc_pulses = 0;
integer fdfc_pulses = 0;
reg [19:0] out_data_reg;
integer current_reg;
reg integration_period;

wire adc_next;
wire adc_vld;
wire signed [15:0] adc_data;
wire adc_run;
wire adc_clk_t1;
wire adc_clk_t2;
wire adc_clk;
wire adc_clk_echo;
wire adc_cnv;
wire [19:0] data_out;
wire [19:0] data;
wire data_out_en;
wire data_en;
reg enable_filter;


bledp_acquisition #(
	.t_clk_period_ns(4),
	.t_integration_ns(2000),
	.ADC_dynamic_range(0)
) uut(
	.clk(clk),
	.nrst(nrst),
	.adc_run(adc_run),
	.adc_next(adc_next),
	.adc_vld(adc_vld),
	.adc_data(adc_data),
	.fdfc_pos(fdfc_pos),
	.fdfc_neg(fdfc_neg),
	.saturation(saturation),
	.raw_data(raw_data),
	.adc_range_reset(adc_range_reset),
	.adc_fixed_range_max(adc_fixed_range_max),
	.adc_fixed_range_min(adc_fixed_range_min),
	.adc_max(adc_max),
	.adc_min(adc_min),
	.data(data),
	.data_en(data_en)
);


low_current_filter #(
	.current_threshold(500)
) uut1(
	.clk(clk),
	.nrst(nrst),
	.fdfc_pos(fdfc_pos),
	.fdfc_neg(fdfc_neg),
	.period_pulse(adc_run),
	.enable_filter(enable_filter),
	.data_in(data),
	.data_in_en(data_en),
	.data_out(data_out),
	.data_out_en(data_out_en)
);

//Marcel's AD7626 module
/*
AD7626 #(
	.Tclk_ns(4)
) u1(
	.intclk(clk),
	.CLK(adc_clk),		//out
	.CNV(adc_cnv),		//out
	.DCO(adc_clk_echo),		//in
	.D(adc_d),		//in
	.adc_next(adc_next),
	.adc_vld(adc_vld),
	.adc_data(adc_data),
	.start_of_cnv(adc_run)
);


*/

//Analog devices AD7626 module, merged and modified by MK

AD7626 #(
	.t_clk_period_ns(4)
) u1(
	.clk_i(clk),
	.adc_cnv_start_i(adc_run),	//in
	.dco_i(adc_clk_echo),		//in
	.d_i(adc_d),				//in
	.clk_o(adc_clk),			//out
	.cnv_o(adc_cnv),			//out
	.par_data_rdy_o(adc_vld),
	.par_data_o(adc_data)
);


//250mhz clk
always
	#2 clk = ~clk;


//input current changes modelling
//parameter analog_min = 90000000;		//analog threshold value
//parameter analog_max = 90000000;		//analog threshold value
parameter analog_min = 250;		//analog threshold value
parameter analog_max = 5000;		//analog threshold value
always
begin
	if (analog_ramp_ns < analog_max)
	begin
		analog_inc = analog_inc + 1;
		analog_ramp_ns = analog_min+analog_inc;
	end
	else
	begin
		analog_inc = 0;
		analog_ramp_ns = analog_min;
	end
	#100;
	//#12000;
end


	
//FDFC ideal analog signal modeling process
parameter real analog_threshold = 32767.0;		//analog threshold value
parameter analog_fdfc_pulse_ns = 20; 	//length of the positive and negative pulses
always
begin
	#1 	
	analog_step = 2*analog_threshold/analog_ramp_ns;
	
	if (analog_real >= analog_threshold-analog_step)
	begin
		//model the comutation glitch
		//analog = analog - 1000;
		slope = 1;
		fdfc_pos = 0;
	end
	if (analog_real <= -1*analog_threshold+analog_step)
	begin
		//model the comutation glitch
		//analog = analog + 1000;
		slope = 0;
		fdfc_neg = 0;
	end
	
	if (fdfc_pos == 0 || fdfc_neg == 0)
	begin
		if (analog_fdfc_lnt_cnt < analog_fdfc_pulse_ns)
			analog_fdfc_lnt_cnt = analog_fdfc_lnt_cnt + 1;
		else
		begin
			analog_fdfc_lnt_cnt = 0;
			fdfc_pos = 1;
			fdfc_neg = 1;
		end
	end
	
	
	if (~slope)
		analog_real = analog_real + analog_step;
	else
		analog_real = analog_real - analog_step;
	
	analog = $rtoi(analog_real);
	
end

//ADC sampling simulation process
//sample on cnv rising edge
//to simulate the ADC behaviour 2 hold registers are required
//when first sample is read out next sample can be acquired at 10MSPS (100 ns)
//index i is used to index current sample
always
begin
	wait (adc_cnv==1);
	analog_noise = ($random % 5);
	
	if (analog + analog_noise > analog_threshold)
		ADC_sample_hold[i] = analog_threshold;
	else if (analog + analog_noise < -1*analog_threshold)
		ADC_sample_hold[i] = -1*analog_threshold;
	else
		ADC_sample_hold[i] = analog + analog_noise;
		
	//ADC_sample_hold[i] = analog;
	
	
	
	i = i + 1;
	wait (adc_cnv==0);
end

//ADC echo clock modeling (6ns)
//according to the AD7626 specification: typ 4 ns, max 7 ns
buf #2 u2(adc_clk_t1, adc_clk);
buf #2 u3(adc_clk_t2, adc_clk_t1);
buf #2 u4(adc_clk_echo, adc_clk_t2);

//ADC bit count process
//when all the bits were read then it will switch to the next (newer) sample (j)
always
begin

	wait (adc_clk_t2==1);
	
	if(ADC_bit_readout < 15)
		ADC_bit_readout = ADC_bit_readout + 1;
	else
	begin
		ADC_bit_readout = 0;
		j = j + 1;
	end
	
	wait (adc_clk_t2==0);
end

//ADC readout simulation process
//use j index to read the oldest sample
always
begin

	wait (adc_clk_t2==1);
	
	adc_d <= ADC_sample_hold[j][15-ADC_bit_readout];
		
	wait (adc_clk_t2==0);

end

//store and process output data
always
begin

	//wait (data_en==1);
	wait (data_out_en==1);
	//out_data_reg = data;
	out_data_reg = data_out;
	//current_reg = $rtoi(10000000.0/data);
	//current_reg = data;
	//wait (data_en==0);
	wait (data_out_en==0);
end

//create pulse every integration period
always
begin
	integration_period = 1;
	#1 integration_period = 0;
	#1999;
end

//count FDFC signal tops and bottoms
always @(edge slope or posedge integration_period)
begin
	if (integration_period == 1)
	begin
		sim_fdfc_pulses = fdfc_pulses;
		fdfc_pulses = 0;
	end
	else
		fdfc_pulses = fdfc_pulses + 1;
end

//adc range reset
always
begin
	#200000 adc_range_reset = 1'b1;
	#4 adc_range_reset = 1'b0;
end

initial
begin
	clk = 1'b0;
	fdfc_pos = 1'b1;
	fdfc_neg = 1'b1;
	raw_data = 1'b0;
	adc_range_reset = 1'b0;
	enable_filter = 1'b1;
	adc_fixed_range_max = 16'h7fff;
	adc_fixed_range_min = 16'h8000;
	
	nrst = 1'b0;
	#8 nrst = 1'b1;
	
	//$display($time, " analog_step %d", analog_step);
	
	
	//#96000000	$stop;
	#600000	$stop;

end


	


endmodule
