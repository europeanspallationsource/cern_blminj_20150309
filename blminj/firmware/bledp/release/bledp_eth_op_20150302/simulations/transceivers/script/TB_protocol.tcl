# #create lpm library and map it to work
# #compile the library
#vlib lpm
#vmap work lpm
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/220pack.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/220model.vhd 


# #create altera_mf library and map it to work
# #compile the library
#vlib altera_mf
#vmap work altera_mf
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf_components.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf.vhd 

vlib lib
vmap work lib

vcom -novopt -O0 "../../../../../../../library/hdl/modules/special/bledp/optical_link/protocol_fifo.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/special/bledp/optical_link/protocol_tx.vhd"
vlog -novopt -O0 "../src/TB_protocol.v"



vsim -novopt work.TB_protocol -t 1ps
add wave *
#TB signals
add wave -radix hex sim:/TB_protocol/uut1/tx_datain
add wave sim:/TB_protocol/uut1/state
add wave sim:/TB_protocol/uut1/fifo_rdrequest
add wave sim:/TB_protocol/uut1/fifo_rdempty
add wave sim:/TB_protocol/uut1/tx_digitalreset_sync

#add wave sim:/TB_protocol/uut2/pll_locked_opt
#add wave sim:/TB_protocol/uut2/tx_digitalreset_opt
#add wave sim:/TB_protocol/uut2/rx_analogreset_opt
#add wave sim:/TB_protocol/uut2/rx_freqlocked_opt
#add wave sim:/TB_protocol/uut2/rx_digitalreset_opt


run -all
wave zoomfull
