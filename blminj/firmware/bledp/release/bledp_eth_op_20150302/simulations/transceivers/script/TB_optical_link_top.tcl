#all the libraries necesary for the altgx simulation can be compiled just once
#it makes the compilation much faster
#uncomment if compiling for the first time

#create lpm library and map it to work
#compile the library
#vlib lpm
#vmap work lpm
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/220pack.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/220model.vhd 

#create sgate library and map it to work
#compile the library
#vlib sgate
#vmap work sgate
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/sgate_pack.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/sgate.vhd 

#create altera_mf library and map it to work
#compile the library
#vlib altera_mf
#vmap work altera_mf
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf_components.vhd 
#com C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf.vhd 

#create cycloneiv_hssi library and map it to work
#compile the library
#vlib cycloneiv_hssi
#vmap work cycloneiv_hssi
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/cycloneiv_hssi_components.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/cycloneiv_hssi_atoms.vhd 

#create lib library and map it to work
#compile the library
vlib lib
vmap work lib

vcom -novopt -O0 "../../../cores/altgx_ch0.vhd"
vcom -novopt -O0 "../../../cores/altgx_ch1.vhd"
vcom -novopt -O0 "../../../cores/gx_reconfig.vhd"
vcom -novopt -O0 "../../../cores/pll.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/special/bledp/optical_link/altgx_rst_control.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/special/bledp/optical_link/protocol_fifo.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/special/bledp/optical_link/protocol_tx.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/special/bledp/optical_link/protocol_rx.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/special/bledp/optical_link/CRC32_D16.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/special/bledp/optical_link/packet_generator.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/special/bledp/optical_link/packet_checker.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/special/bledp/optical_link/checker_statistics.vhd"
vcom -novopt -O0 "../src/optical_link_top.vhd"
vlog -novopt -O0 "../src/TB_optical_link_top.v"



vsim -novopt work.TB_optical_link_top -t 1ps
#add wave *

add wave sim:/TB_optical_link_top/rx
add wave sim:/TB_optical_link_top/tx

add wave sim:/TB_optical_link_top/sys_clk_out_1
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/crc_calc_rst
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/crc_calc_en
add wave -radix hex sim:/TB_optical_link_top/uut1/proto_data_tx
add wave sim:/TB_optical_link_top/uut1/proto_startofpacket_tx
add wave sim:/TB_optical_link_top/uut1/proto_endofpacket_tx
add wave sim:/TB_optical_link_top/uut1/proto_valid_tx
add wave sim:/TB_optical_link_top/uut1/proto_ready_tx

add wave sim:/TB_optical_link_top/sys_clk_out_2
add wave -radix hex sim:/TB_optical_link_top/uut2/proto_data_rx
add wave sim:/TB_optical_link_top/uut2/proto_startofpacket_rx
add wave sim:/TB_optical_link_top/uut2/proto_endofpacket_rx
add wave sim:/TB_optical_link_top/uut2/proto_valid_rx
add wave sim:/TB_optical_link_top/uut2/proto_ready_rx

add wave -radix hex sim:/TB_optical_link_top/uut2/packet_checker_i/pkt_dwords_cnt
add wave -radix hex sim:/TB_optical_link_top/uut2/packet_checker_i/chk_length
add wave sim:/TB_optical_link_top/uut2/packet_checker_i/chk_endofpacket_d1
add wave -radix hex sim:/TB_optical_link_top/uut2/packet_checker_i/crc_reg
add wave sim:/TB_optical_link_top/uut2/packet_checker_i/chk_crc_err
add wave -radix hex sim:/TB_optical_link_top/uut2/packet_checker_i/seq_received
add wave -radix hex sim:/TB_optical_link_top/uut2/packet_checker_i/seq_received_prev
add wave sim:/TB_optical_link_top/uut2/packet_checker_i/chk_seq_err
add wave -radix hex sim:/TB_optical_link_top/uut2/packet_checker_i/hdr_received
add wave sim:/TB_optical_link_top/uut2/packet_checker_i/chk_hdr_err
add wave sim:/TB_optical_link_top/uut2/packet_checker_i/chk_sop_err
add wave sim:/TB_optical_link_top/uut2/packet_checker_i/chk_eop_err
add wave sim:/TB_optical_link_top/uut2/packet_checker_i/chk_no_err
add wave -radix unsigned sim:/TB_optical_link_top/uut2/stat_no_error
add wave -radix unsigned sim:/TB_optical_link_top/uut2/stat_crc_errors
add wave -radix unsigned sim:/TB_optical_link_top/uut2/stat_hdr_errors
add wave -radix unsigned sim:/TB_optical_link_top/uut2/stat_seq_errors
add wave -radix unsigned sim:/TB_optical_link_top/uut2/stat_sop_errors
add wave -radix unsigned sim:/TB_optical_link_top/uut2/stat_eop_errors
add wave -radix unsigned sim:/TB_optical_link_top/uut2/stat_rx_fifo_errors


add wave sim:/TB_optical_link_top/uut1/protocol_tx_i/tx_digitalreset
add wave sim:/TB_optical_link_top/uut1/protocol_tx_i/tx_digitalreset_sync

add wave sim:/TB_optical_link_top/uut1/protocol_tx_i/tx_clockout
add wave sim:/TB_optical_link_top/uut1/protocol_tx_i/state
add wave -radix hex sim:/TB_optical_link_top/uut1/protocol_tx_i/tx_datain
add wave sim:/TB_optical_link_top/uut1/protocol_tx_i/tx_cntrlenable
add wave sim:/TB_optical_link_top/uut1/protocol_tx_i/fifo_rdempty
add wave sim:/TB_optical_link_top/uut1/protocol_tx_i/fifo_rdrequest
add wave -radix hex sim:/TB_optical_link_top/uut1/protocol_tx_i/fifo_data

add wave sim:/TB_optical_link_top/uut1/packet_generator_i/gen_data
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/gen_startofpacket
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/gen_endofpacket
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/gen_valid
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/gen_ready
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/pkt_dwords_cnt
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/pkt_dwords_done
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/crc_calc_en
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/crc_reg





add wave sim:/TB_optical_link_top/uut2/protocol_rx_i/rx_digitalreset
add wave sim:/TB_optical_link_top/uut2/protocol_rx_i/rx_digitalreset_sync

add wave sim:/TB_optical_link_top/uut2/protocol_rx_i/rx_clockout
add wave -radix hex sim:/TB_optical_link_top/uut2/protocol_rx_i/rx_dataout
add wave sim:/TB_optical_link_top/uut2/protocol_rx_i/rx_ctrldetect
add wave sim:/TB_optical_link_top/uut2/protocol_rx_i/state
add wave sim:/TB_optical_link_top/uut2/protocol_rx_i/rx_startofpacket
add wave sim:/TB_optical_link_top/uut2/protocol_rx_i/rx_endofpacket

add wave sim:/TB_optical_link_top/uut2/protocol_rx_i/rx_startofpacket_d2
add wave -radix hex sim:/TB_optical_link_top/uut2/protocol_rx_i/rx_dataout_d1
add wave -radix hex sim:/TB_optical_link_top/uut2/protocol_rx_i/fifo_din
add wave sim:/TB_optical_link_top/uut2/protocol_rx_i/fifo_wrrequest_d1
add wave sim:/TB_optical_link_top/uut2/protocol_rx_i/proto_err0


#add wave sim:/TB_optical_link_top/uut2/pll_areset_opt
#add wave sim:/TB_optical_link_top/uut2/pll_locked_opt
#add wave sim:/TB_optical_link_top/uut2/tx_digitalreset_opt
#add wave sim:/TB_optical_link_top/uut2/rx_analogreset_opt
#add wave sim:/TB_optical_link_top/uut2/rx_freqlocked_opt
#add wave sim:/TB_optical_link_top/uut2/rx_digitalreset_opt


add wave sim:/TB_optical_link_top/uut1/packet_generator_i/pkt_number_cnt
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/pkt_number_done
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/gen_enable
add wave sim:/TB_optical_link_top/uut1/packet_generator_i/gen_packets

run -all
wave zoomfull
