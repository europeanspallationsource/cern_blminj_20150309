
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_protocol;

reg proto_clk;
reg tx_clockout;
reg proto_nrst;
reg tx_digitalreset;
reg [15:0] proto_data;
reg proto_startofpacket;
reg proto_endofpacket;
reg proto_valid;

wire proto_ready;
wire [1:0] tx_cntrlenable;
wire [15:0] tx_datain;

wire [7:0] error_code;
wire error_code_en;



protocol_tx uut1(
	//local clock and reset
	.proto_clk(proto_clk),
	.proto_nrst(proto_nrst),
	
	//input port
	.proto_data(proto_data),
	.proto_startofpacket(proto_startofpacket),
	.proto_endofpacket(proto_endofpacket),
	.proto_valid(proto_valid),
	.proto_ready(proto_ready),
	
	//transceiver signals
	.tx_digitalreset(tx_digitalreset),
	.tx_cntrlenable(tx_cntrlenable),
	.tx_datain(tx_datain),
	.tx_clockout(tx_clockout),
	
	//error code signals
	.error_code(error_code),
	.error_code_en(error_code_en)
);



//100mhz clk
always
	#5 proto_clk = ~proto_clk;

//100mhz clk
initial 
begin
	tx_clockout = 1'b0;
	#2;						//phase shifted in respect to proto_clk
	forever #5 tx_clockout = ~tx_clockout;
end


initial
begin
	proto_clk = 1'b0;
	tx_digitalreset = 1'b1;
	proto_nrst = 1'b1;
	proto_data = 16'h0000;
	proto_startofpacket = 1'b0;
	proto_endofpacket = 1'b0;
	proto_valid = 1'b0;
	
	#100 proto_nrst = 1'b0;
	#100 tx_digitalreset = 1'b0;
	
	@(posedge proto_clk)
	proto_data = 16'hABCD;
	proto_startofpacket = 1'b1;
	//proto_endofpacket = 1'b1;
	proto_valid = 1'b1;
	@(posedge proto_clk)
	proto_startofpacket = 1'b0;
	proto_data = 16'h1234;
	@(posedge proto_clk)
	proto_valid = 1'b0;
	
	
	#100;
	@(posedge proto_clk)
	proto_data = 16'hEF32;
	//proto_startofpacket = 1'b1;
	proto_endofpacket = 1'b1;
	proto_valid = 1'b1;
	@(posedge proto_clk)
	proto_valid = 1'b0;
	proto_endofpacket = 1'b0;
	
	
	
	#1000 $stop;
	
	
	
end




endmodule
