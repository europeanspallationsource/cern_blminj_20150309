
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_optical_link_top;

reg clk;
reg areset;

reg gen_enable_1;
reg gen_enable_2;
reg [31:0] gen_packets_1;
reg [31:0] gen_packets_2;

wire [15:0] chk_header_1;
wire [15:0] chk_length_1;
wire [31:0] chk_sequence_1;
wire [31:0] stat_no_error_1;
wire [31:0] stat_crc_errors_1;
wire [31:0] stat_hdr_errors_1;
wire [31:0] stat_seq_errors_1;
wire [31:0] stat_sop_errors_1;
wire [31:0] stat_eop_errors_1;
wire [31:0] stat_rx_fifo_errors_1;


wire [15:0] chk_header_2;
wire [15:0] chk_length_2;
wire [31:0] chk_sequence_2;
wire [31:0] stat_no_error_2;
wire [31:0] stat_crc_errors_2;
wire [31:0] stat_hdr_errors_2;
wire [31:0] stat_seq_errors_2;
wire [31:0] stat_sop_errors_2;
wire [31:0] stat_eop_errors_2;
wire [31:0] stat_rx_fifo_errors_2;

wire rx;
wire tx;

wire sys_clk_out_1;
wire sys_clk_out_2;


optical_link_top uut1(
	.areset(areset),
	
	.chk_header(chk_header_1),
	.chk_sequence(chk_sequence_1),
	.chk_length(chk_length_1),
	.stat_no_error(stat_no_error_1),
	.stat_crc_errors(stat_crc_errors_1),
	.stat_hdr_errors(stat_hdr_errors_1),
	.stat_seq_errors(stat_seq_errors_1),
	.stat_sop_errors(stat_sop_errors_1),
	.stat_eop_errors(stat_eop_errors_1),
	.stat_rx_fifo_errors(stat_rx_fifo_errors_1),
	.gen_packets(gen_packets_1),
	.gen_enable(gen_enable_1),
	
	.REFCLK2(clk),
	.REFCLK1(clk),
	.GXP_RX1(tx),
	.GXP_TX1(rx),
	
	.sys_clk_out(sys_clk_out_1),
	
	.POS1(1'b1),
	.POS2(1'b0),
	.POS3(1'b0),
	.POS4(1'b0)
);


optical_link_top uut2(
	.areset(areset),
	
	.chk_header(chk_header_2),
	.chk_sequence(chk_sequence_2),
	.chk_length(chk_length_2),
	.stat_no_error(stat_no_error_2),
	.stat_crc_errors(stat_crc_errors_2),
	.stat_hdr_errors(stat_hdr_errors_2),
	.stat_seq_errors(stat_seq_errors_2),
	.stat_sop_errors(stat_sop_errors_2),
	.stat_eop_errors(stat_eop_errors_2),
	.stat_rx_fifo_errors(stat_rx_fifo_errors_2),
	.gen_packets(gen_packets_2),
	.gen_enable(gen_enable_2),
	
	.REFCLK2(clk),
	.REFCLK1(clk),
	.GXP_RX1(rx),
	.GXP_TX1(tx),
	
	.sys_clk_out(sys_clk_out_2),
	
	.POS1(1'b0),
	.POS2(1'b1),
	.POS3(1'b0),
	.POS4(1'b0)
);


//125mhz clk
always
	#4 clk = ~clk;


//Transmitter process (sys_clk_out_1)
	
initial
begin
	clk = 1'b0;
	gen_enable_1 = 1'b0;
	gen_enable_2 = 1'b0;
	gen_packets_1 = 32'h0000000a;
	gen_packets_2 = 32'h0000000a;
	areset = 1'b1;
	
/*	proto_data_tx_1 = 16'h0000;
	proto_startofpacket_tx_1 = 1'b0;
	proto_endofpacket_tx_1 = 1'b0;
	proto_valid_tx_1 = 1'b0;
	proto_ready_rx_1 = 1'b0;
	*/
	
	#100 areset = 1'b0;
	
	#7000 gen_enable_1 = 1'b1;
	gen_enable_2 = 1'b1;
	#100 gen_enable_1 = 1'b0;
	gen_enable_2 = 1'b0;
	
	
	
	
	#3000 gen_enable_1 = 1'b1;
	gen_enable_2 = 1'b1;
	#100 gen_enable_1 = 1'b0;
	gen_enable_2 = 1'b0;
	
/*	
	#6500;
	
	@(posedge sys_clk_out_1)
	
	proto_data_tx_1 = 16'h1234;
	proto_startofpacket_tx_1 = 1'b1;
	proto_endofpacket_tx_1 = 1'b0;
	proto_valid_tx_1 = 1'b1;
	@(posedge sys_clk_out_1)
	
	proto_data_tx_1 = 16'h5678;
	proto_startofpacket_tx_1 = 1'b0;
	proto_endofpacket_tx_1 = 1'b0;
	proto_valid_tx_1 = 1'b1;
	@(posedge sys_clk_out_1)
	
	//uncomment for pause
	proto_valid_tx_1 = 1'b0;
	#200;
	@(posedge sys_clk_out_1)
	
	proto_data_tx_1 = 16'h9ABC;
	proto_startofpacket_tx_1 = 1'b0;
	proto_endofpacket_tx_1 = 1'b1;
	proto_valid_tx_1 = 1'b1;
	@(posedge sys_clk_out_1)
	
	proto_data_tx_1 = 16'h0000;
	proto_startofpacket_tx_1 = 1'b0;
	proto_endofpacket_tx_1 = 1'b0;
	proto_valid_tx_1 = 1'b0;
	*/
	
	@(posedge sys_clk_out_1)
	#9500;
	
	@(posedge sys_clk_out_1)
	
	#1000 $stop;
	
	
	
end


//Receiver process (sys_clk_out_2)
	
/*
initial
begin	
	
	proto_data_tx_2 = 16'h0000;
	proto_startofpacket_tx_2 = 1'b0;
	proto_endofpacket_tx_2 = 1'b0;
	proto_valid_tx_2 = 1'b0;
	proto_ready_rx_2 = 1'b0;
	
	while(1) 
	begin 
	
		@(posedge sys_clk_out_2)
		
		if (proto_valid_rx_2 == 1)
			proto_ready_rx_2 = 1'b1;
		else
			proto_ready_rx_2 = 1'b0;
		
	end 
	
end
*/



endmodule
