------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    12/03/2012
--Module Name:    optical_link_top
--Project Name:   optical_link_top
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity optical_link_top is

port(

	areset:					in std_logic;
	
	--checker output
	chk_header:				out std_logic_vector(15 downto 0);
	chk_sequence:			out std_logic_vector(31 downto 0);
	chk_length:				out std_logic_vector(15 downto 0);
	
	--statistics counters outputs
	stat_no_error:			out std_logic_vector(31 downto 0);
	stat_crc_errors:		out std_logic_vector(31 downto 0);
	stat_hdr_errors:		out std_logic_vector(31 downto 0);
	stat_seq_errors:		out std_logic_vector(31 downto 0);
	stat_sop_errors:		out std_logic_vector(31 downto 0);
	stat_eop_errors:		out std_logic_vector(31 downto 0);
	stat_rx_fifo_errors:	out std_logic_vector(31 downto 0);
	
	gen_packets:			in std_logic_vector(31 downto 0);
	gen_enable:				in std_logic;
	
	
	-- Reference Clock 125 MHz
	REFCLK2:				in std_logic;
	-- Reference Clock 125 MHz
	REFCLK1:				in std_logic;
	
	-- IX. Optical/Network SPF Link
	GXP_RX1:					in std_logic;
	GXP_TX1:					out std_logic;

	GXP_RX2:					in std_logic;
	GXP_TX2:					out std_logic;
	
	sys_clk_out:				out std_logic;
	
	POS1:						in std_logic;
	POS2:						in std_logic;
	POS3:						in std_logic;
	POS4:						in std_logic

);
	
end entity optical_link_top;


architecture rtl of optical_link_top is

attribute keep: boolean;

constant sys_clk_freq : natural := 83333333;
--constant sys_clk_freq : natural := 50000000;
constant sys_clk_ns_period : natural := 1000000000/sys_clk_freq;



-- PLL signals
signal sys_clk:			std_logic;
signal clk_40:			std_logic;
signal pll_locked:		std_logic;

signal pll_areset_eth:			std_logic;
signal pll_locked_eth:			std_logic;
signal rx_freqlocked_eth:		std_logic;
signal pll_locked_opt:			std_logic;
signal rx_freqlocked_opt:		std_logic;
signal tx_digitalreset_opt:		std_logic;
signal tx_digitalreset_eth:		std_logic;
signal pll_areset_opt:			std_logic;
signal rx_clkout_opt:			std_logic;
signal tx_clkout_opt:			std_logic;
signal rx_dataout_opt:			std_logic_vector(15 downto 0);


-- Reset controller
signal rst_sys_clk_n_shift:	std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
signal rx_analogreset_shift:	std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
signal tx_digitalreset_opt_shift:	std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
signal tx_digitalreset_eth_shift:	std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
signal rst_sys_clk_n:			std_logic;
signal rx_analogreset_opt:			std_logic;
signal rx_analogreset_eth:			std_logic;
signal rx_digitalreset_opt:		std_logic;
signal rx_digitalreset_eth:		std_logic;


-- GX reconfig component signals
signal reconfig_fromgxb_0	: STD_LOGIC_VECTOR (4 DOWNTO 0);
signal reconfig_fromgxb_1	: STD_LOGIC_VECTOR (4 DOWNTO 0);
signal reconfig_fromgxb	: STD_LOGIC_VECTOR (9 DOWNTO 0);
signal reconfig_busy		: STD_LOGIC ;
signal reconfig_togxb		: STD_LOGIC_VECTOR (3 DOWNTO 0);

signal tx_ctrlenable: std_logic_vector(1 downto 0);
signal tx_datain: std_logic_vector(15 downto 0);
signal rx_ctrldetect:			 std_logic_vector(1 downto 0);
signal rx_errdetect:			 std_logic_vector(1 downto 0);

signal gen_header_sig:			std_logic_vector(15 downto 0);
signal proto_data_tx:			std_logic_vector(15 downto 0);
signal proto_startofpacket_tx:	std_logic;
signal proto_endofpacket_tx:	std_logic;
signal proto_valid_tx:			std_logic;
signal proto_ready_tx:			std_logic;

signal proto_data_rx:			std_logic_vector(15 downto 0);
signal proto_startofpacket_rx:	std_logic;
signal proto_endofpacket_rx:	std_logic;
signal proto_valid_rx:			std_logic;
signal proto_ready_rx:			std_logic;
signal proto_error_rx:			std_logic_vector(7 downto 0);

signal chk_no_err:			std_logic;
signal chk_crc_err:			std_logic;
signal chk_hdr_err:			std_logic;
signal chk_seq_err:			std_logic;
signal chk_sop_err:			std_logic;
signal chk_eop_err:			std_logic;

BEGIN

	------------------------------------------------------------------
	-- PLL
	------------------------------------------------------------------
	
	PLL1_inst: entity work.pll
	port map 
	( 
		areset	=> areset,
		inclk0 	=> REFCLK2,	 
		c0 		=> sys_clk, --100.0 MHz
		c1 		=> clk_40, --40MHz
		c2 		=> open, --250MHz
		locked	=> pll_locked
	);
	
	sys_clk_out <= sys_clk;
	
	------------------------------------------------------------------
	-- synchronous reset circuits
	------------------------------------------------------------------
	sys_rst_controller: process (sys_clk, pll_locked) -- rst (from reset) synchronises to sys_clk
	begin
		if pll_locked = '0' then
			rst_sys_clk_n_shift <= (others => '0');      
		elsif rising_edge (sys_clk) then
			rst_sys_clk_n_shift <= rst_sys_clk_n_shift(rst_sys_clk_n_shift'HIGH-1 downto rst_sys_clk_n_shift'LOW) & '1';
		end if;
	end process;
	-- reset is the output of the high bit of the shift register.
	rst_sys_clk_n <= rst_sys_clk_n_shift(rst_sys_clk_n_shift'HIGH);
	

	
	
	------------------------------------------------------------------
	-- GX reconfiguration components
	------------------------------------------------------------------
	
	GX_reconfig_eth_i: entity work.gx_reconfig
	PORT MAP
	(
		reconfig_clk 		=> clk_40,
		reconfig_fromgxb 	=> reconfig_fromgxb,
		busy				=> reconfig_busy,
		reconfig_togxb		=> reconfig_togxb
	);
	
	reconfig_fromgxb <= reconfig_fromgxb_1 & reconfig_fromgxb_0;
	
	------------------------------------------------------------------
	-- GX reset controllers
	------------------------------------------------------------------
	altgx0_rst_i: entity work.altgx_rst_control
	generic map (
		t_clk_period_ns => sys_clk_ns_period,
		t_ltd_auto_ns => 4000,
		t_digitalreset_ns => 200,
		t_analogreset_ns => 50
	)
	port map (
		--local clock and reset
		clk 	=> sys_clk,
		nrst 	=> rst_sys_clk_n,
	
		--transceiver status
		busy 			=> reconfig_busy,
		pll_locked 		=> pll_locked_opt,
		rx_freqlocked 	=> rx_freqlocked_opt,
	
		--transceiver reset signals
		pll_areset 		=> pll_areset_opt,
		tx_digitalreset => tx_digitalreset_opt,
		rx_analogreset 	=> rx_analogreset_opt,
		rx_digitalreset => rx_digitalreset_opt
	);
	
	altgx1_rst_i: entity work.altgx_rst_control
	generic map (
		t_clk_period_ns => sys_clk_ns_period,
		t_ltd_auto_ns => 4000,
		t_analogreset_ns => 50
	)
	port map (
		--local clock and reset
		clk 	=> sys_clk,
		nrst 	=> rst_sys_clk_n,
	
		--transceiver status
		busy 			=> reconfig_busy,
		pll_locked 		=> pll_locked_eth,
		rx_freqlocked 	=> rx_freqlocked_eth,
	
		--transceiver reset signals
		pll_areset 		=> pll_areset_eth,
		tx_digitalreset => tx_digitalreset_eth,
		rx_analogreset 	=> rx_analogreset_eth,
		rx_digitalreset => rx_digitalreset_eth
	);
	
	------------------------------------------------------------------
	-- Transceiver Instances
	------------------------------------------------------------------
	
	altgx_opt_i : entity work.altgx_ch0 PORT MAP (
		cal_blk_clk	 				=> sys_clk,
		pll_inclk(0) 				=> REFCLK2,
		reconfig_clk	 			=> clk_40,
		reconfig_togxb	 			=> reconfig_togxb,
		rx_analogreset(0)			=> rx_analogreset_opt,
		rx_ctrldetect		 		=> rx_ctrldetect,
		rx_datain(0)	 			=> GXP_RX1,
		rx_digitalreset(0)			=> rx_digitalreset_opt,
		tx_ctrlenable	 			=> tx_ctrlenable,	--one control signal per byte
		tx_datain	 				=> tx_datain,
		tx_digitalreset(0)			=> tx_digitalreset_opt,
		reconfig_fromgxb			=> reconfig_fromgxb_0,
		rx_clkout(0)				=> rx_clkout_opt,
		rx_dataout					=> rx_dataout_opt,
		tx_clkout(0)				=> tx_clkout_opt,
		tx_dataout(0)				=> GXP_TX1,
		pll_areset(0)				=> pll_areset_opt,
		pll_locked(0)				=> pll_locked_opt,
		rx_errdetect				=> rx_errdetect,
		rx_freqlocked(0)			=> rx_freqlocked_opt
	);
	
	altgx_eth_i : entity work.altgx_ch1 PORT MAP (
		cal_blk_clk	 				=> sys_clk,
		pll_inclk(0) 				=> REFCLK1,
		reconfig_clk	 			=> clk_40,
		reconfig_togxb	 			=> reconfig_togxb,
		rx_analogreset(0) 			=> rx_analogreset_eth,
		rx_datain(0) 				=> GXP_RX2,
		rx_digitalreset(0) 			=> rx_digitalreset_eth,
		tx_datain	 				=> "0000000000",
		tx_digitalreset(0) 			=> tx_digitalreset_eth,
		reconfig_fromgxb	 		=> reconfig_fromgxb_1,
--		rx_clkout(0) 				=> open,
--		rx_dataout	 				=> open,
--		tx_clkout(0) 				=> open,
		tx_dataout(0) 				=> GXP_TX2,
		pll_areset(0)				=> pll_areset_eth,
		pll_locked(0)				=> pll_locked_eth,
		rx_freqlocked(0)			=> rx_freqlocked_eth
	);
	
	
	------------------------------------------------------------------
	-- Protocol for the optical transceiver
	------------------------------------------------------------------
	protocol_tx_i : entity work.protocol_tx 
	PORT MAP (
		--local clock and reset
		proto_clk			=> sys_clk,
		proto_nrst			=> rst_sys_clk_n,
		
		--input port
		proto_data				=> proto_data_tx,
		proto_startofpacket		=> proto_startofpacket_tx,
		proto_endofpacket		=> proto_endofpacket_tx,
		proto_valid				=> proto_valid_tx,
		proto_ready				=> proto_ready_tx,
		
		--transceiver signals
		tx_digitalreset		=> tx_digitalreset_opt,
		tx_cntrlenable		=> tx_ctrlenable,
		tx_datain			=> tx_datain,
		tx_clockout			=> tx_clkout_opt
	);
	
	protocol_rx_i : entity work.protocol_rx 
	PORT MAP (
		--local clock and reset
		proto_clk			=> sys_clk,
		proto_nrst			=> rst_sys_clk_n,
		
		--output port
		proto_data				=> proto_data_rx,
		proto_startofpacket		=> proto_startofpacket_rx,
		proto_endofpacket		=> proto_endofpacket_rx,
		proto_valid				=> proto_valid_rx,
		proto_ready				=> proto_ready_rx,
		
		--proto error signal
		proto_error				=> proto_error_rx,
		
		--transceiver signals
		rx_digitalreset		=> rx_digitalreset_opt,
		rx_errdetect		=> rx_errdetect,
		rx_ctrldetect		=> rx_ctrldetect,
		rx_dataout			=> rx_dataout_opt,
		rx_clockout			=> rx_clkout_opt
	);
	
	------------------------------------------------------------------
	-- Test paket generator
	------------------------------------------------------------------
	packet_generator_i : entity work.packet_generator 
	generic map(
		t_clk_period_ns 	=> sys_clk_ns_period,
		--t_pkt_interval_ns	=> 2*sys_clk_ns_period
		t_pkt_interval_ns	=> 0
	)
	port map(
		--clock and reset
		gen_clk		=> sys_clk,
		gen_nrst	=> rst_sys_clk_n,
		
		--generator output port
		gen_data			=> proto_data_tx,
		gen_startofpacket	=> proto_startofpacket_tx,
		gen_endofpacket		=> proto_endofpacket_tx,
		gen_valid			=> proto_valid_tx,
		gen_ready			=> proto_ready_tx,
		
		--header
		gen_header			=> gen_header_sig,
		
		--control
		gen_packets			=> gen_packets,
		gen_done			=> open,
		gen_enable 			=> gen_enable
	);
	
	gen_header_sig <= x"000" & POS4 & POS3 & POS2 & POS1;
	
	------------------------------------------------------------------
	-- Test paket checker
	------------------------------------------------------------------
	packet_checker_i : entity work.packet_checker
	port map(
		--clock and reset
		chk_clk		=> sys_clk,
		chk_nrst	=> rst_sys_clk_n,
		
		--checker input port
		chk_data			=> proto_data_rx,
		chk_startofpacket	=> proto_startofpacket_rx,
		chk_endofpacket		=> proto_endofpacket_rx,
		chk_valid			=> proto_valid_rx,
		chk_ready			=> proto_ready_rx,
		
		--checker output
		chk_no_err		=> chk_no_err,
		chk_header		=> chk_header,	
		chk_sequence	=> chk_sequence,
		chk_length		=> chk_length,	
		chk_crc_err		=> chk_crc_err,	
		chk_hdr_err		=> chk_hdr_err,	
		chk_seq_err		=> chk_seq_err,	
		chk_sop_err		=> chk_sop_err,
		chk_eop_err		=> chk_eop_err
	);
	
	------------------------------------------------------------------
	-- Test paket checker statistics
	------------------------------------------------------------------
	checker_statistics_i : entity work.checker_statistics
	port map (
		--clock and reset
		clk				=> sys_clk,
		nrst			=> rst_sys_clk_n,
		
		--reset counters input
		cnt_rst			=> '0',
		
		--correct frame
		no_err			=> chk_no_err,
		
		--error inputs
		crc_err			=> chk_crc_err,
		hdr_err			=> chk_hdr_err,
		seq_err			=> chk_seq_err,
		sop_err			=> chk_sop_err,
		eop_err			=> chk_eop_err,
		rx_fifo_err		=> proto_error_rx(0),
		
		--counters outputs
		no_error		=> stat_no_error,
		crc_errors		=> stat_crc_errors,
		hdr_errors		=> stat_hdr_errors,
		seq_errors		=> stat_seq_errors,
		sop_errors		=> stat_sop_errors,
		eop_errors		=> stat_eop_errors,
		rx_fifo_errors	=> stat_rx_fifo_errors
		
	);

end architecture rtl;
