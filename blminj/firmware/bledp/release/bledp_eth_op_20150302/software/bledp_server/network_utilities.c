/******************************************************************************
* Copyright (c) 2006 Altera Corporation, San Jose, California, USA.           *
* All rights reserved. All use of this software and documentation is          *
* subject to the License Agreement located at the end of this file below.     *
******************************************************************************
* Date - October 24, 2006                                                     *
* Module - network_utilities.c                                                *
*                                                                             *
******************************************************************************/

#include <alt_types.h>
#include <ctype.h>
#include <errno.h>
#include <stdio.h>
#include <sys/alt_flash.h>
#include "includes.h"
#include "io.h"
#include "bledp_server.h"

#include <alt_iniche_dev.h>

#include "ipport.h"
#include "tcpport.h"
#include "network_utilities.h"
#include "bledp_server.h"
#include "altera_avalon_pio_regs.h"
#include "crc8_maxim.h"

#define IP4_ADDR(ipaddr, a,b,c,d) ipaddr = \
    htonl((((alt_u32)(a & 0xff) << 24) | ((alt_u32)(b & 0xff) << 16) | \
          ((alt_u32)(c & 0xff) << 8) | (alt_u32)(d & 0xff)))



/*
* get_mac_addr
*
* Read the MAC address in a board specific way. Prompt user to enter serial 
* number to generate MAC address if failed to read from flash.
*
*/
int get_mac_addr(NET net, unsigned char mac_addr[6])
{
    error_t error = 0;
	int t1, t2;
	unsigned char ID_readout[7];
	alt_u8 ID_crc, ID_crc_cal, family_code;
	
	t1 = OSTimeGet();
	
	debug_print("wait for the ID readout for ... \n");
	
	//wait until DS2411 readout is done
	/*	use IORD macros to bypass the cache
		otherwise first read from this memory will be cached
		and data written from the external logic will be invisible */
	/*	0x1 is the DS2411 family code
		this value is expected when the FPGA will finish the DS2411 readout 
		correct CRC is also expected
		there is a timeout after 10 seconds */
		
	do {
	
		//reset status busy flag in the custom logic
		IOWR_ALTERA_AVALON_PIO_DATA(STATUS_SENT_PIO_BASE, 1);
		IOWR_ALTERA_AVALON_PIO_DATA(STATUS_SENT_PIO_BASE, 0);
		
		ID_crc = ((alt_u64)IORD_32DIRECT(STATUS_MEM_BASE, 6*4)  & 0x00FF0000) >> 16;
		ID_readout[0] = (IORD_32DIRECT(STATUS_MEM_BASE, 6*4) & 0xFF000000) >> 24;
		ID_readout[1] = (IORD_32DIRECT(STATUS_MEM_BASE, 7*4) & 0x00FF0000) >> 16;
		ID_readout[2] = (IORD_32DIRECT(STATUS_MEM_BASE, 7*4) & 0xFF000000) >> 24;
		ID_readout[3] = (IORD_32DIRECT(STATUS_MEM_BASE, 8*4) & 0x00FF0000) >> 16;
		ID_readout[4] = (IORD_32DIRECT(STATUS_MEM_BASE, 8*4) & 0xFF000000) >> 24;
		ID_readout[5] = (IORD_32DIRECT(STATUS_MEM_BASE, 9*4) & 0x00FF0000) >> 16;
		ID_readout[6] = (IORD_32DIRECT(STATUS_MEM_BASE, 9*4) & 0xFF000000) >> 24;
		family_code = (IORD_32DIRECT(STATUS_MEM_BASE, 9*4) & 0xFF000000) >> 24;
		
		//calculate crc of the ID and family code
		ID_crc_cal = CRC8Maxim(0, ID_readout, 56);
		
		//break the loop after timeout (10s)
		t2 = OSTimeGet();
		if((t2-t1)>10*OS_TICKS_PER_SEC)
		{
			debug_print("(timeout) \n");
			break;
		}
	
	}
	while(ID_crc!=ID_crc_cal || family_code != 0x01);
	
	debug_print("%d ticks\n",t2-t1);
	
	//compare calculated crc with the readout
	if(ID_crc==ID_crc_cal)
	{
		debug_print("DS2411 readout 0x%.2hhX%.2hhX%.2hhX%.2hhX%.2hhX%.2hhX%.2hhX - CRC ok 0x%X == 0x%X\n", ID_readout[0], ID_readout[1], ID_readout[2], ID_readout[3], ID_readout[4], ID_readout[5], ID_readout[6], ID_crc_cal, ID_crc);
		
		/* Use DS2411 two last significant bytes and crc as the MAC address*/
		mac_addr[3] = ID_readout[4];
		mac_addr[4] = ID_readout[5];
		mac_addr[5] = ID_crc;
	}
	else
	{
		debug_print("DS2411 readout 0x%.2hhX%.2hhX%.2hhX%.2hhX%.2hhX%.2hhX%.2hhX - CRC not ok 0x%X != 0x%X\n", ID_readout[0], ID_readout[1], ID_readout[2], ID_readout[3], ID_readout[4], ID_readout[5], ID_readout[6], ID_crc_cal, ID_crc);
		mac_addr[3] = 0;
		mac_addr[4] = 0;
		mac_addr[5] = 0;
	}
	
	/* The MSB bytes of the MAC is set to the Altera Vendor ID */
	mac_addr[0] = 0x0;
	mac_addr[1] = 0x7;
	mac_addr[2] = 0xed;
	
	
	debug_print("Your Ethernet MAC address is %02x:%02x:%02x:%02x:%02x:%02x\n", 
            mac_addr[0],
            mac_addr[1],
            mac_addr[2],
            mac_addr[3],
            mac_addr[4],
            mac_addr[5]);
    
    return error;
}

/*
 * get_ip_addr()
 *
 * This routine is called by InterNiche to obtain an IP address for the
 * specified network adapter. Like the MAC address, obtaining an IP address is
 * very system-dependant and therefore this function is exported for the
 * developer to control.
 *
 * In our system, we are either attempting DHCP auto-negotiation of IP address,
 * or we are setting our own static IP, Gateway, and Subnet Mask addresses our
 * self. This routine is where that happens.
 */
int get_ip_addr(alt_iniche_dev *p_dev,
                ip_addr* ipaddr,
                ip_addr* netmask,
                ip_addr* gw,
                int* use_dhcp)
{

	unsigned int slot;
	
	/* use static IP defined by the slot number until there is no proper dhcp server
	*/
	slot = IORD_ALTERA_AVALON_PIO_DATA(ACQ_IFACE_STATUS_PIO_BASE) & 0xF;
	slot *= 10;
	IP4_ADDR(*ipaddr, IPADDR0, IPADDR1, IPADDR2, slot);
	debug_print("Set IP address to %d.%d.%d.%d\n", IPADDR0, IPADDR1, IPADDR2, slot);
	
    /*IP4_ADDR(*ipaddr, IPADDR0, IPADDR1, IPADDR2, IPADDR3);*/
    IP4_ADDR(*gw, GWADDR0, GWADDR1, GWADDR2, GWADDR3);
    IP4_ADDR(*netmask, MSKADDR0, MSKADDR1, MSKADDR2, MSKADDR3);

#ifdef DHCP_CLIENT
    *use_dhcp = 1;
#else /* not DHCP_CLIENT */
    *use_dhcp = 0;

    debug_print("Static IP Address is %d.%d.%d.%d\n",
        ip4_addr1(*ipaddr),
        ip4_addr2(*ipaddr),
        ip4_addr3(*ipaddr),
        ip4_addr4(*ipaddr));
#endif /* not DHCP_CLIENT */

    /* Non-standard API: return 1 for success */
    return 1;
}

