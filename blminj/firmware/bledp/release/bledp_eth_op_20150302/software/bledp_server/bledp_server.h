
 
 /* Validate supported Software components specified on system library properties page.
  */
#ifndef __BLEDP_SERVER_H__
#define __BLEDP_SERVER_H__

#if !defined (ALT_INICHE)
  #error The BLEDP Server requires the 
  #error NicheStack TCP/IP Stack Software Component. Please see the Nichestack
  #error Tutorial for details on Nichestack TCP/IP Stack - Nios II Edition,
  #error including notes on migrating applications from lwIP to NicheStack.
#endif

#ifndef __ucosii__
  #error This BLEDP Server requires 
  #error the MicroC/OS-II Intellectual Property Software Component.
#endif

#if defined (ALT_LWIP)
  #error The BLEDP Server requires the 
  #error NicheStack TCP/IP Stack Software Component, and no longer works
  #error with the lwIP networking stack.  Please see the Altera Nichstack
  #error Tutorial for details on Nichestack TCP/IP Stack - Nios II Edition,
  #error including notes on migrating applications from lwIP to NicheStack.
#endif

/* 
 * structure to manage a single client connection
 */
typedef struct ACQ_CONN
{
	/* TCP connection socket (-1 when there is no client connected) */
	int tcp_socket;
	
	/* 	numbers of acquisition bundles requested by the client
		-1 when no acquisition bundles were requested or when the TX is completed 
		0 when continious data transmission 
		number of acquisition bundles otherwise */
	int acq_bundles_requested;
	
	/* number of selected channel */
	int acq_ch_number;
	
	/* number of acquisition bundles sent to the client */
	int acq_bundles_sent;
	
	/* limit of buffers flag */
	int acq_buff_limit;
	
	/* address of the connected client */
	unsigned long incoming_addr;
	
	/* UDP port number */
	int udp_port;
	
	/* UDP streaming socket */
	int udp_socket;
	
} AcqConn;

/* BLEDP TCP server port number */
#define BLEDP_TCP_PORT 30
/* BLEDP UDP streaming default port number */
#define BLEDP_UDP_STR_DEF_PORT 8090


/* BLEDP server settings */
#define BLEDP_DEBUG 0
#define BLEDP_SEND_MAC 1
#define BLEDP_SEND_STATUSES 1


#define debug_print(fmt, ...) \
        do { if (BLEDP_DEBUG) fprintf(stderr, "%s:%d:%s(): " fmt, __FILE__, \
                                __LINE__, __func__, ##__VA_ARGS__); } while (0)



/* Nichestack definitions */
#include "ipport.h"
#include "tcpport.h"
#include "libport.h"
#include "osport.h"
#include "bsdsock.h"

/*
 * Task Prototypes:
 * 
 */

void BLEDPAcqServerTask();


/* Acquisition server functions prototypes */
void acq_reset_interface(void);
void acq_decode_command(AcqConn*);
void acq_write_data(AcqConn*, volatile void*);
int acq_build_select_list(fd_set *, fd_set *, int*, AcqConn*);
void acq_handle_new_connection(int, AcqConn*);
void acq_close_connection(AcqConn*);

void status_write_data(AcqConn*, volatile void*);
void status_init_buffer(volatile void*);
void enable_status(void);
void disable_status(void);

int enable_udp_stream(AcqConn*, alt_u32);
int disable_udp_stream(void);

int write_digital_potentiometer(unsigned int value, int number, int threshold);


/*
 *  Task Priorities:
 * 
 *  MicroC/OS-II only allows one task (thread) per priority number.   
 */

#define ACQUISITION_TASK_PRIORITY           4
#define INITIAL_TASK_PRIORITY               6

/* 
 * The IP, gateway, and subnet mask address below are used as a last resort
 * if no network settings can be found, and DHCP (if enabled) fails. You can
 * edit these as a quick-and-dirty way of changing network settings if desired.
 * 
 * Default IP addresses are set to all zeros so that DHCP server packets will
 * penetrate secure routers. They are NOT intended to be valid static IPs, 
 * these values are only a valid default on networks with DHCP server. 
 * 
 * If DHCP will not be used, select valid static IP addresses here, for example:
 *           IP: 192.168.1.234
 *      Gateway: 192.168.1.1
 *  Subnet Mask: 255.255.255.0
 */
#define IPADDR0   192
#define IPADDR1   168
#define IPADDR2   137
#define IPADDR3   10
//#define IPADDR3   30

#define GWADDR0   192
#define GWADDR1   168
#define GWADDR2   137
#define GWADDR3   1

#define MSKADDR0  255
#define MSKADDR1  255
#define MSKADDR2  255
#define MSKADDR3  0


/* Definition of Task Stack size for tasks not using Nichestack */
#define   TASK_STACKSIZE       2048


/* Define command PIO bits */
#define P0_CB1_ON_MASK		(0x00000001)
#define P0_CB1_ON_OFFSET	(0)
#define P0_CB2_ON_MASK		(0x00000002)
#define P0_CB2_ON_OFFSET	(1)
#define P0_DCDC_ON_MASK		(0x00000004)
#define P0_DCDC_ON_OFFSET	(2)
#define P0_DP_DATA_MASK		(0x000007F8)
#define P0_DP_DATA_OFFSET	(3)
#define P0_DP_CH_MASK		(0x00000800)
#define P0_DP_CH_OFFSET		(11)
#define P0_DP1_WR_MASK		(0x00001000)
#define P0_DP1_WR_OFFSET	(12)
#define P0_DP2_WR_MASK		(0x00002000)
#define P0_DP2_WR_OFFSET	(13)
#define P0_DP3_WR_MASK		(0x00004000)
#define P0_DP3_WR_OFFSET	(14)
#define P0_DP4_WR_MASK		(0x00008000)
#define P0_DP4_WR_OFFSET	(15)
#define P0_DP5_WR_MASK		(0x00010000)
#define P0_DP5_WR_OFFSET	(16)
#define P0_DP6_WR_MASK		(0x00020000)
#define P0_DP6_WR_OFFSET	(17)
#define P0_DP7_WR_MASK		(0x00040000)
#define P0_DP7_WR_OFFSET	(18)
#define P0_DP8_WR_MASK		(0x00080000)
#define P0_DP8_WR_OFFSET	(19)
#define P0_DP_EEMEM_MASK	(0x00100000)
#define P0_DP_EEMEM_OFFSET	(20)

#define P1_ADC_RNG_MIN_MASK		(0x0000FFFF)
#define P1_ADC_RNG_MIN_OFFSET	(0)
#define P1_ADC_RNG_MAX_MASK		(0xFFFF0000)
#define P1_ADC_RNG_MAX_OFFSET	(16)

#define P2_CH1_FLT_MASK		(0x00000001)
#define P2_CH1_FLT_OFFSET	(0)
#define P2_CH2_FLT_MASK		(0x00000002)
#define P2_CH2_FLT_OFFSET	(1)
#define P2_CH3_FLT_MASK		(0x00000004)
#define P2_CH3_FLT_OFFSET	(2)
#define P2_CH4_FLT_MASK		(0x00000008)
#define P2_CH4_FLT_OFFSET	(3)
#define P2_CH5_FLT_MASK		(0x00000010)
#define P2_CH5_FLT_OFFSET	(4)
#define P2_CH6_FLT_MASK		(0x00000020)
#define P2_CH6_FLT_OFFSET	(5)
#define P2_CH7_FLT_MASK		(0x00000040)
#define P2_CH7_FLT_OFFSET	(6)
#define P2_CH8_FLT_MASK		(0x00000080)
#define P2_CH8_FLT_OFFSET	(7)

#define P2_CH1_RAW_MASK		(0x00000100)
#define P2_CH1_RAW_OFFSET	(8)
#define P2_CH2_RAW_MASK		(0x00000200)
#define P2_CH2_RAW_OFFSET	(9)
#define P2_CH3_RAW_MASK		(0x00000400)
#define P2_CH3_RAW_OFFSET	(10)
#define P2_CH4_RAW_MASK		(0x00000800)
#define P2_CH4_RAW_OFFSET	(11)
#define P2_CH5_RAW_MASK		(0x00001000)
#define P2_CH5_RAW_OFFSET	(12)
#define P2_CH6_RAW_MASK		(0x00002000)
#define P2_CH6_RAW_OFFSET	(13)
#define P2_CH7_RAW_MASK		(0x00004000)
#define P2_CH7_RAW_OFFSET	(14)
#define P2_CH8_RAW_MASK		(0x00008000)
#define P2_CH8_RAW_OFFSET	(15)

#define P2_CH1_DADC_MASK	(0x00010000)
#define P2_CH1_DADC_OFFSET	(16)
#define P2_CH2_DADC_MASK	(0x00020000)
#define P2_CH2_DADC_OFFSET	(17)
#define P2_CH3_DADC_MASK	(0x00040000)
#define P2_CH3_DADC_OFFSET	(18)
#define P2_CH4_DADC_MASK	(0x00080000)
#define P2_CH4_DADC_OFFSET	(19)
#define P2_CH5_DADC_MASK	(0x00100000)
#define P2_CH5_DADC_OFFSET	(20)
#define P2_CH6_DADC_MASK	(0x00200000)
#define P2_CH6_DADC_OFFSET	(21)
#define P2_CH7_DADC_MASK	(0x00400000)
#define P2_CH7_DADC_OFFSET	(22)
#define P2_CH8_DADC_MASK	(0x00800000)
#define P2_CH8_DADC_OFFSET	(23)

#define P2_CH1_RLY_MASK		(0x01000000)
#define P2_CH1_RLY_OFFSET	(24)
#define P2_CH2_RLY_MASK		(0x02000000)
#define P2_CH2_RLY_OFFSET	(25)
#define P2_CH3_RLY_MASK		(0x04000000)
#define P2_CH3_RLY_OFFSET	(26)
#define P2_CH4_RLY_MASK		(0x08000000)
#define P2_CH4_RLY_OFFSET	(27)
#define P2_CH5_RLY_MASK		(0x10000000)
#define P2_CH5_RLY_OFFSET	(28)
#define P2_CH6_RLY_MASK		(0x20000000)
#define P2_CH6_RLY_OFFSET	(29)
#define P2_CH7_RLY_MASK		(0x40000000)
#define P2_CH7_RLY_OFFSET	(30)
#define P2_CH8_RLY_MASK		(0x80000000)
#define P2_CH8_RLY_OFFSET	(31)

/* Commands default non-zero values */
#define ADC_MAX_DEF		(15650)
#define ADC_MIN_DEF		(-15660)

#endif /* __BLEDP_SERVER_H__ */

