
//LFSR with x8+x5+x4+1 polynomial
char CRC8Maxim_LFSR(char crc8, char bit)
{
	char new_crc8;
	
	// crc_bit_0 = crc_bit_7 xor data_bit
	new_crc8 =  1 & ((crc8>>7)^bit);
	// crc_bit_1 to crc_bit_3 do shift left
	new_crc8 |= 0x0E & (crc8<<1);
	// crc_bit_4 = crc_bit_3 xor crc_bit_7 xor data_bit
	new_crc8 |= 16 & ((crc8<<1)^(crc8>>3)^(bit<<4));
	// crc_bit_5 = crc_bit_4 xor crc_bit_7 xor data_bit
	new_crc8 |= 32 & ((crc8<<1)^(crc8>>2)^(bit<<5));
	// crc_bit_6 to crc_bit_7 do shift left
	new_crc8 |= 0xC0 & (crc8<<1);
	
	return new_crc8;
}


char CRC8Maxim(char crc8, char* data, int nbits)
{
	int i;
	char crc8_rev = 0;
	
	//shift all bits to the LFSR in reversed order (from LSB to MSB)
	for(i=0; i<nbits; i++)
		crc8 = CRC8Maxim_LFSR(crc8, data[nbits/8-i/8-1]>>(i%8)); 
	
	//reverse CRC bits
	//for(i=8; crc8; crc8>>=1)
	for(i=8; i>0; crc8>>=1)
	{
		crc8_rev <<= 1;
		crc8_rev |= crc8 & 1;
		i--;
	}
	crc8_rev <<= i;
	
	return crc8_rev;

}


