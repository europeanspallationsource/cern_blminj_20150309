/******************************************************************************
* Maciej Kwiatkowski
* European Organisation for Nuclear Research
* BE-BI-BL
* CERN, Geneva, Switzerland,  CH-1211
* 865/R-A01
*******************************************************************************
* Date - April 23, 2012                                                     *
* Module - bledp_server.c                                             *
*                                                                             *
******************************************************************************/
 
#include <stdio.h>
#include <string.h>
#include <ctype.h> 
#include <fcntl.h>
#include <sys/alt_cache.h>

/* MicroC/OS-II definitions */
#include "includes.h"

/* BLEDP Server definitions */
#include "bledp_server.h"                                                                    
#include "alt_error_handler.h"


#include "altera_avalon_pio_regs.h"
#include "sys/alt_irq.h"
#include "network_utilities.h"
#include "crc8_maxim.h"

/* UDP hardware stream definitions */
#include "bledp_packet_generator.h"
#include "udp_payload_inserter.h"


/*	acquisition interface parameters should corespond to the VHDL generics */
/*	size of the acquisition buffer including the checksum */
#define ACQ_BUFF_SIZE 351
/*	size of the acquisition data without the checksum */
#define ACQ_DATA_SIZE (ACQ_BUFF_SIZE-1)
/*	size of the status data */
#define STATUS_DATA_SIZE 313
/*	size of the acquisition memory in double words */
#define ACQ_MEM_SIZE 2048
/*	number of acquisition buffers which fit into the given acquisition memory */
#define ACQ_BUFF_AVAIL (ACQ_MEM_SIZE/ACQ_BUFF_SIZE)

#if BLEDP_SEND_STATUSES == 0

#ifndef STATUS_MEM_BASE
#define STATUS_MEM_BASE 0
#endif /* STATUS_MEM_BASE */

#ifndef STATUS_MEM_SPAN
#define STATUS_MEM_SPAN 0
#endif /* STATUS_MEM_SPAN */

#endif /* BLEDP_SEND_STATUSES == 0 */

/*	global variables used in the interrupt and in the server's task */
/*	interrupts counter */
volatile unsigned int acq_buff_int_number = 0;
volatile unsigned int status_int_number = 0;
/*	simple queue which is written in the interrupt
	and read in the server's task 
	it is primitive but faster than the ucosii queue */
#define TX_BUFF_END_Q_SIZE (ACQ_BUFF_AVAIL+1)
volatile unsigned int tx_buff_end_wr = 0;
volatile unsigned int tx_buff_end_rd = 0;
unsigned int tx_buff_end_queue[TX_BUFF_END_Q_SIZE];




void acq_reset_interface(void)
{
	//debug_print("Reset of the acquisition interface\n");
	
	//reset the queue
	acq_buff_int_number = 0;
	tx_buff_end_wr = 0;
	tx_buff_end_rd = 0;

	//set acq_iface_reset bit
	IOWR_ALTERA_AVALON_PIO_SET_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 1);
	
	//zero the acquisition memory
	//memset((void*)ACQ_MEM_BASE, 0, ACQ_MEM_SPAN);
	
	//clear acq_iface_reset bit
	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 1);
	
}

#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
static void acq_buf_rdy_int_routine(void* context)
#else
static void acq_buf_rdy_int_routine(void* context, alt_u32 id)
#endif
{
	//acuisition interrupt routine
	//do as little as possible here
	
	//store last buffer offset in the queue
	//the offset is pointing the end of the buffer
	tx_buff_end_queue[tx_buff_end_wr] = IORD_ALTERA_AVALON_PIO_DATA(ACQ_OFFSET_PIO_BASE);
	tx_buff_end_wr++;
	if(tx_buff_end_wr>=TX_BUFF_END_Q_SIZE)
		tx_buff_end_wr = 0;
	
	//count interrupt
	//this number is decreased when the data is transmitted
	//it should never be more than the number of available buffers in the HW
	//if it is more then we probably lost some data due to the overflow
	acq_buff_int_number++;
	
	/* If TX is too slow - disable FIFO write of all the channels */
	if(acq_buff_int_number >= ACQ_BUFF_AVAIL-1)
		IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 0xFF<<24);
	
	//reset the edge capture register
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(ACQ_INT_PIO_BASE, 0);
}


#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
static void status_int_routine(void* context)
#else
static void status_int_routine(void* context, alt_u32 id)
#endif
{
	//status interrupt routine
	//do as little as possible here
	
	//disable interrupt
	//after data is sent it will be re-enabled
	disable_status();
	
	//check if it can be more than 1?
	status_int_number++;
	
	//debug_print("Status interrupt number %d\n",status_int_number);
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(STATUS_INT_PIO_BASE, 0x0);
}



/*
 * BLEDPAcqServerTask()
 * 
 */
 
void BLEDPAcqServerTask()
{
	int fd_listen, max_socket;
	struct sockaddr_in addr;
	AcqConn client;
	fd_set readfds;
	fd_set writefds;
	struct timeval timeout;  /* Timeout for select */
	int select_res;
	
	
	volatile void* acq_data;
	volatile void* status_data;
	
	//acq_data = (alt_u32*) ACQ_MEM_BASE;
	acq_data = alt_remap_uncached((void*)ACQ_MEM_BASE, ACQ_MEM_SPAN);
	//status_data = (alt_u32*) STATUS_MEM_BASE;
	status_data = alt_remap_uncached((void*)STATUS_MEM_BASE, STATUS_MEM_SPAN);
	
	//initialize the status memory
	status_init_buffer(status_data);
	
	//Set defaults:
	
	//Enable DCDC, CB1, CB2
	debug_print("Enable circuit breakers\n");
	IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_0_BASE, P0_CB1_ON_MASK|P0_CB2_ON_MASK|P0_DCDC_ON_MASK);
	
	//Set default ADC range
	debug_print("Set adc range register to default value (%d, %d) - > %x\n", ADC_MIN_DEF, ADC_MAX_DEF, ((ADC_MAX_DEF<<P1_ADC_RNG_MAX_OFFSET)&P1_ADC_RNG_MAX_MASK) | ((ADC_MIN_DEF<<P1_ADC_RNG_MIN_OFFSET)&P1_ADC_RNG_MIN_MASK));
	IOWR_ALTERA_AVALON_PIO_DATA(COMMAND_PIO_1_BASE, ((ADC_MAX_DEF<<P1_ADC_RNG_MAX_OFFSET)&P1_ADC_RNG_MAX_MASK) | ((ADC_MIN_DEF<<P1_ADC_RNG_MIN_OFFSET)&P1_ADC_RNG_MIN_MASK));
	
	//Set low current filters enabled
	debug_print("Enable low current filters\n");
	IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_2_BASE, P2_CH1_FLT_MASK|P2_CH2_FLT_MASK|P2_CH3_FLT_MASK|P2_CH4_FLT_MASK|P2_CH5_FLT_MASK|P2_CH6_FLT_MASK|P2_CH7_FLT_MASK|P2_CH8_FLT_MASK);	
	
	//reset acquisition interface
	acq_reset_interface();
	/* Register the interrupt handlers. */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
	alt_ic_isr_register(ACQ_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, ACQ_INT_PIO_IRQ, acq_buf_rdy_int_routine, 0, NULL);
	alt_ic_isr_register(STATUS_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, STATUS_INT_PIO_IRQ, status_int_routine, 0, NULL);
#else
	alt_irq_register(ACQ_INT_PIO_IRQ, 0, acq_buf_rdy_int_routine);
	alt_irq_register(STATUS_INT_PIO_IRQ, 0, status_int_routine);
#endif  

	//disable TCP buffer and status interrupts
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
	alt_ic_irq_disable(ACQ_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, ACQ_INT_PIO_IRQ);
	alt_ic_irq_disable(STATUS_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, STATUS_INT_PIO_IRQ);
#else
	alt_irq_disable(ACQ_INT_PIO_IRQ);
	alt_irq_disable(STATUS_INT_PIO_IRQ);
#endif 
	//set PIO registers to enable interrupts
	/* Reset the edge capture registers. */
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(ACQ_INT_PIO_BASE, 0x0);
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(STATUS_INT_PIO_BASE, 0x0);
	/* Enable the buffer ready interrupt */
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(ACQ_INT_PIO_BASE, 0x1);
	IOWR_ALTERA_AVALON_PIO_IRQ_MASK(STATUS_INT_PIO_BASE, 0x1);

			

	
	/*	Create listening socket
	*/ 
	if ((fd_listen = socket(AF_INET, SOCK_STREAM, 0)) < 0)
	{
		alt_NetworkErrorHandler(EXPANDED_DIAGNOSIS_CODE,"[acquisition_task] Socket creation failed");
	}
  
	/*	Bind an address to the listening socket
	*/
	addr.sin_family = AF_INET;
	addr.sin_port = htons(BLEDP_TCP_PORT);
	addr.sin_addr.s_addr = INADDR_ANY;
  
	if ((bind(fd_listen,(struct sockaddr *)&addr,sizeof(addr))) < 0)
	{
		alt_NetworkErrorHandler(EXPANDED_DIAGNOSIS_CODE,"[acquisition_task] Bind failed");
	}
    
	/* Indicate the willingness to accept incoming connections on this socket */ 
	if ((listen(fd_listen, 1)) < 0)
	{
		alt_NetworkErrorHandler(EXPANDED_DIAGNOSIS_CODE,"[acquisition_task] Listen failed");
	}

	/* At this point we have successfully created a socket which is listening
	* on BLEDP_TCP_PORT for connection requests from any remote address.
	*/
	client.tcp_socket = -1;
	client.acq_bundles_requested = -1;
	client.acq_bundles_sent = 0;
	client.acq_buff_limit = 0;
	client.udp_port = BLEDP_UDP_STR_DEF_PORT;
	
	debug_print("BLEDP acquisition server listening on port %d\n", BLEDP_TCP_PORT);
	
	/* The inactivity timeout is 5 seconds */
	timeout.tv_sec = 5;
	timeout.tv_usec = 0;
   
	while(1)
	{
		max_socket = acq_build_select_list(&readfds, &writefds, &fd_listen, &client);

		/* select will block until any of its conditions is met */
		select_res = select(max_socket, &readfds, &writefds, NULL, &timeout);
		/* Error */
		if(select_res < 0)
		{
			alt_NetworkErrorHandler(EXPANDED_DIAGNOSIS_CODE,"[acquisition_task] Select failed");
		}
		/* Timeout */
		else if(select_res == 0)
		{
			/* Close the connection if the timeout has occured */
			if(client.tcp_socket!=-1)
			{
				debug_print("Timeout\n");
				acq_close_connection(&client);
			}
		}
		/* Activity on any of the sockets */
		else
		{
			/* new connection request */
			if (FD_ISSET(fd_listen, &readfds))
				acq_handle_new_connection(fd_listen, &client);
			
			/* new command arrived */
			if (FD_ISSET(client.tcp_socket, &readfds) && client.tcp_socket!=-1)
				acq_decode_command(&client);
			
			/* acquisition data can be written to the client */
			if (FD_ISSET(client.tcp_socket, &writefds) && client.tcp_socket!=-1 && client.acq_bundles_requested!=-1)
			{
				acq_write_data(&client, acq_data);
			}
#if BLEDP_SEND_STATUSES
			/* status data can be written to the client */
			if (FD_ISSET(client.tcp_socket, &writefds) && client.tcp_socket!=-1)
			{
				status_write_data(&client, status_data);
			}
#endif /* BLEDP_SEND_STATUSES */
			
		}
		
	} /* while(1) */
}



void acq_decode_command(AcqConn* client)
{
	alt_u64 command;
	char command_b[7];
	
	alt_u16 sub_command;
	alt_u8 data_type;
	alt_u8 start;
	int command_bytes;
	
	//read the command from the socket
	command_bytes = recv(client->tcp_socket, (void*)command_b, 7, 0);
	
	//return if the command is less than 7 bytes
	if(command_bytes!=7)
	{
		/* if select fired and there is no data to read (0) or there is an error (-1) or command in not complete (7 bytes)*/
		/* close the connection */
		debug_print("recv returned %d\n", command_bytes);
		acq_close_connection(client);
		return;
	}
	
	//calculate anc check the CRC (lsb)
	if(CRC8Maxim(0, command_b, 48) != command_b[6])
	{
		debug_print("Command 0x%.2hhX%.2hhX%.2hhX%.2hhX%.2hhX%.2hhX%.2hhX - CRC not ok 0x%.2hhX != 0x%.2hhX\n", 
			(unsigned char)command_b[0], 
			(unsigned char)command_b[1], 
			(unsigned char)command_b[2], 
			(unsigned char)command_b[3], 
			(unsigned char)command_b[4], 
			(unsigned char)command_b[5], 
			(unsigned char)command_b[6], 
			(unsigned char)command_b[6], 
			(unsigned char)CRC8Maxim(0, command_b, 48));
		return;
	}
	
	//convert received command to the Nios endian (little)
	//command = ntohl(command);
	//combine command dword excluding the CRC byte (lsb)
	command  = (alt_u64)(command_b[0]&0xFF) << 40;
	command |= (alt_u64)(command_b[1]&0xFF) << 32;
	command |= (alt_u64)(command_b[2]&0xFF) << 24;
	command |= (alt_u64)(command_b[3]&0xFF) << 16;
	command |= (alt_u64)(command_b[4]&0xFF) << 8;
	command |= (alt_u64)(command_b[5]&0xFF);
	
	//debug_print("Received command 0x%llX\n", command);
	
	
	//start = (command&0x80000000)>>31;
	start = (command&0x800000000000ULL)>>47;
	
	/* if other than start TX command */
	if(!start)
	{
		//decode sub-command field (7 bits - 128 sub-commands)
		//sub_command = (command&0x7F000000)>>24;
		sub_command = (command&0x7FFF00000000ULL)>>32;
		
		//stop all active TX
		if(sub_command==0)
		{
			debug_print("SUB-CMD: stop all TX\n");
			
			/* Disable the buffer ready interrupt */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
			alt_ic_irq_disable(ACQ_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, ACQ_INT_PIO_IRQ);
#else
			alt_irq_disable(ACQ_INT_PIO_IRQ);
#endif 
			
			/* disable UDP stream hardware */
			disable_udp_stream();
			
			client->acq_bundles_requested = -1;
			client->acq_bundles_sent = 0;
			client->acq_buff_limit = 0;
			/* disable FIFO write of all the channels */
			IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 0xFF<<24);
			/* Reset the edge capture registers. */
			IOWR_ALTERA_AVALON_PIO_EDGE_CAP(ACQ_INT_PIO_BASE, 0x0);
			/* reset acquisition interface */
			acq_reset_interface();
		}
		//set UDP HW port
		if(sub_command==1)
		{
			debug_print("SUB-CMD: change UDP streaming port from %d to %d\n", client->udp_port, (int)(command&0x00000000FFFF));
			client->udp_port = (command&0x00000000FFFF);
		}
		//disable/enable circut breaker 1
		if(sub_command==2)
		{
			debug_print("SUB-CMD: set circuit breaker 1 to %d\n", (int)command&0x000000000001);
			if(command&0x000000000001)
				IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_0_BASE, P0_CB1_ON_MASK);
			else
				IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_0_BASE, P0_CB1_ON_MASK);
		}
		//disable/enable circut breaker 2
		if(sub_command==3)
		{
			debug_print("SUB-CMD: set circuit breaker 2 to %d\n", (int)command&0x000000000001);
			if(command&0x000000000001)
				IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_0_BASE, P0_CB2_ON_MASK);
			else
				IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_0_BASE, P0_CB2_ON_MASK);
		}
		//disable/enable dcdc
		if(sub_command==4)
		{
			debug_print("SUB-CMD: set DCDC converter to %d\n", (int)command&0x000000000001);
			if(command&0x000000000001)
				IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_0_BASE, P0_DCDC_ON_MASK);
			else
				IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_0_BASE, P0_DCDC_ON_MASK);
		}
		//set max ADC range
		if(sub_command==5)
		{
			debug_print("SUB-CMD: set max ADC range to %hd (0x%X)\n", (short)command&0x00000000FFFF, (int)command&0x00000000FFFF);
			//clear selected bits before setting the new value
			IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_1_BASE, P1_ADC_RNG_MAX_MASK);
			IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_1_BASE, (((command&0x00000000FFFF)<<P1_ADC_RNG_MAX_OFFSET)&P1_ADC_RNG_MAX_MASK));
			
		}
		//set min ADC range
		if(sub_command==6)
		{
			debug_print("SUB-CMD: set min ADC range to %hd (0x%X)\n", (short)command&0x00000000FFFF, (int)command&0x00000000FFFF);
			//clear selected bits before setting the new value
			IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_1_BASE, P1_ADC_RNG_MIN_MASK);
			IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_1_BASE, (((command&0x00000000FFFF)<<P1_ADC_RNG_MIN_OFFSET)&P1_ADC_RNG_MIN_MASK));
		}
		//enable/disable low current filters
		if(sub_command>=7 && sub_command<=14)
		{
			debug_print("SUB-CMD: set low current filter of channel %d to %d\n", (int)sub_command-6, (int)command&0x000000000001);
			if(command&0x000000000001)
				IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_2_BASE, P2_CH1_FLT_MASK<<(sub_command-7));
			else
				IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_2_BASE, P2_CH1_FLT_MASK<<(sub_command-7));
		}
		//enable/disable raw data
		if(sub_command>=15 && sub_command<=22)
		{
			debug_print("SUB-CMD: set raw data mode of channel %d to %d\n", (int)sub_command-14, (int)command&0x000000000001);
			if(command&0x000000000001)
				IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_2_BASE, P2_CH1_RAW_MASK<<(sub_command-15));
			else
				IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_2_BASE, P2_CH1_RAW_MASK<<(sub_command-15));
		}
		//enable/disable relays
		//only one relay at the time can be enabled
		if(sub_command>=23 && sub_command<=30)
		{
			debug_print("SUB-CMD: set relay of channel %d to %d\n", (int)sub_command-22, (int)command&0x000000000001);
			if(command&0x000000000001)
			{
				//only one relay at the time can be enabled
				//clear all bits before setting requested one
				IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_2_BASE, P2_CH1_RLY_MASK|P2_CH2_RLY_MASK|P2_CH3_RLY_MASK|P2_CH4_RLY_MASK|P2_CH5_RLY_MASK|P2_CH6_RLY_MASK|P2_CH7_RLY_MASK|P2_CH8_RLY_MASK);
				IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_2_BASE, P2_CH1_RLY_MASK<<(sub_command-23));
			}
			else
				IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_2_BASE, P2_CH1_RLY_MASK<<(sub_command-23));
		}
		//set offset digital potentiometers
		if(sub_command>=31 && sub_command<=38)
		{
			debug_print("SUB-CMD: set offset potentiometer of channel %d to %d (0x%X)\n", (int)sub_command-30, (int)command&0x0000000000FF, (int)command&0x00000000001FF);
			write_digital_potentiometer(command&0x0000000001FF, sub_command-30, 0);
			
		}
		//set threshold digital potentiometers
		if(sub_command>=39 && sub_command<=46)
		{
			debug_print("SUB-CMD: set threshold potentiometer of channel %d to %d (0x%X)\n", (int)sub_command-38, (int)command&0x0000000000FF, (int)command&0x00000000001FF);
			write_digital_potentiometer(command&0x0000000001FF, sub_command-38, 1);
		}
		//enable/disable DADC mode
		if(sub_command>=47 && sub_command<=54)
		{
			debug_print("SUB-CMD: set DADC mode of channel %d to %d\n", (int)sub_command-46, (int)command&0x000000000001);
			if(command&0x000000000001)
				IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_2_BASE, P2_CH1_DADC_MASK<<(sub_command-47));
			else
				IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_2_BASE, P2_CH1_DADC_MASK<<(sub_command-47));
		}
		
		/* Sub-command handling is done */
		
	}
	/* else if TX statrt command*/
	else
	{

		/* decode data type requested */
		//data_type  = (command&0x60000000)>>29;
		data_type  = (command&0x600000000000ULL)>>45;
		
		/* 	enable TCP transfer if
			if single channel request */
		if(data_type==0)
		{
		
			/* Enable interrupt for the TCP buffer ready */
			/* Reset the edge capture registers. */
			IOWR_ALTERA_AVALON_PIO_EDGE_CAP(ACQ_INT_PIO_BASE, 0x0);
			/* Enable the buffer ready interrupt */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
			alt_ic_irq_enable(ACQ_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, ACQ_INT_PIO_IRQ);
#else
			alt_irq_enable(ACQ_INT_PIO_IRQ);
#endif 
		
			/* ignore data request command if there is pending data transfer */
			if(client->acq_bundles_requested!=-1)
			{
				debug_print("ACQ CMD single channel request ignored due to the pending data transfer\n");
				return;
			}
			
			//client->acq_bundles_requested = (command&0x03FFFFFF);
			//client->acq_ch_number  = (command&0x1C000000)>>26;
			client->acq_bundles_requested = (command&0xFFFFFFFF);
			client->acq_ch_number  = (command&0x1C0000000000ULL)>>42;
			
			//debug_print("Requested bundles count %x\n", client->acq_bundles_requested);
			//debug_print("Requested channel number %d\n", client->acq_ch_number);
			
			/* reset acquisition interface */
			acq_reset_interface();
			
			//Set FIFO write enable strobe of requested channel 
			IOWR_ALTERA_AVALON_PIO_SET_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 1<<(24+(client->acq_ch_number&0x7)));
			
		}
		/* 	enable UDP streaming if
			if multiple channel request */
		if(data_type==1)
		{
			/* ignore data request command if there is pending data transfer */
			if(is_packet_generator_running((void *)BLEDP_PACKET_GENERATOR_0_BASE))
			{
				debug_print("ACQ CMD multiple channel request ignored due to the pending data transfer\n");
				return;
			}
			
			//enable_udp_stream(client, command&0x03FFFFFF);
			enable_udp_stream(client, command&0xFFFFFFFF);
			
			IOWR_ALTERA_AVALON_PIO_SET_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 0xFF<<24); //all channels
			//IOWR_ALTERA_AVALON_PIO_SET_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 0x0F<<24); //half channels
		}
		
	}
	
}


/*
 * acq_write_data()
 */

void acq_write_data(AcqConn* client, volatile void* acq_data)
{
	alt_u32 acq_buff_chksum;
	int acq_bytes_sent;
	
	alt_u16 tx_buff_begin;	
	
	
	unsigned int tx_buff_end;
	
	
	if (client->acq_bundles_sent < client->acq_bundles_requested || client->acq_bundles_requested == 0)
	{
		
		//return if there were no interrupts yet
		if(!acq_buff_int_number)
			return;
		
		//disable interrupt to manipulate global variables
		//TODO: check if an interrupt which arrive here is served after re-enabling it
		
		
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
		alt_ic_irq_disable(ACQ_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, ACQ_INT_PIO_IRQ);
#else
		alt_irq_disable(ACQ_INT_PIO_IRQ);
#endif  
		
		/*	Check if there were more interrupts than available TX buffers - 1.
			If the TX is too slow set client->acq_buff_limit flag
			The FIFO of all the channels were already disabled in the interrupt routine
			so this number won't increase (no interrupts until acq_buff_int_number == 0) */
		if(acq_buff_int_number >= ACQ_BUFF_AVAIL-1)
		{
			if(!client->acq_buff_limit)
				debug_print("TX was too slow!!! %d\n", acq_buff_int_number);
			
			client->acq_buff_limit = 1;
		}
				
		//decrement interrupt number as the data will be transmitted
		acq_buff_int_number--;
		
		//get the oldest element from the queue
		tx_buff_end = tx_buff_end_queue[tx_buff_end_rd];
		
		//move the queue's read pointer to the next element
		tx_buff_end_rd++;
		if(tx_buff_end_rd>=TX_BUFF_END_Q_SIZE)
			tx_buff_end_rd = 0;
		
		//re-enable interrupt
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
		alt_ic_irq_enable(ACQ_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, ACQ_INT_PIO_IRQ);
#else
		alt_irq_enable(ACQ_INT_PIO_IRQ);
#endif  
		
		//debug_print("tx_buff_end %d\n", tx_buff_end);
		
		//calculate begining of the buffer
		tx_buff_begin = tx_buff_end - (ACQ_BUFF_SIZE-1);
		
		//checksum calculated in the HW for the future use in the TCP driver
		acq_buff_chksum = IORD_32DIRECT(ACQ_MEM_BASE, tx_buff_end*4);
		
		/*	if TX was too slow and the acquisition is stopped (disabled FIFOS)
			send one error packet at end of the data */
		/*if(acq_buff_int_number == 0 && client->acq_buff_limit)
		{
			//	TXerror packet is 0x80000000
			//	but data bytes in the memory are swapped 
			IOWR_32DIRECT(ACQ_MEM_BASE, (tx_buff_end-1)*4, 0x00000080);			
		}*/
			
		//send one acquisition bundle (350 bledp packets == 1400 bytes)
		acq_bytes_sent=send(client->tcp_socket, (void*)(acq_data+tx_buff_begin*4), (ACQ_DATA_SIZE)*4, 0);

		//check the result
		if(acq_bytes_sent != (ACQ_DATA_SIZE)*4)
		{
			/* if send returned an error (-1) or if it sent less data */
			/* something is wrong - close the connection */
			debug_print("send returned %d\n", acq_bytes_sent);
			acq_close_connection(client);
			return;
		}
		//if whole buffer was transmitted
		else if(client->acq_bundles_requested!=0)
			client->acq_bundles_sent++;
		
		//if TX was to slow but all the data were already sent
		//reset ACQ interface, clear acq_buff_limit flag and re-enable FIFO of the selected channel
		if(acq_buff_int_number == 0 && client->acq_buff_limit)
		{
			acq_reset_interface();
			client->acq_buff_limit = 0;
			//Set FIFO write enable strobe of requested channel 
			IOWR_ALTERA_AVALON_PIO_SET_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 1<<(24+(client->acq_ch_number&0x7)));
		}
		
	}
	
	//if all the bundles were sent then TX request is completed
	if(client->acq_bundles_sent>=client->acq_bundles_requested && client->acq_bundles_requested != 0)
	{
		/* Disable the buffer ready interrupt */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
		alt_ic_irq_disable(ACQ_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, ACQ_INT_PIO_IRQ);
#else
		alt_irq_disable(ACQ_INT_PIO_IRQ);
#endif 
		
		/* disable FIFO write of all the channels */
		IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 0xFF<<24);
		client->acq_bundles_sent = 0;
		client->acq_bundles_requested = -1;
		client->acq_buff_limit = 0;
		
		/* Reset the edge capture registers. */
		IOWR_ALTERA_AVALON_PIO_EDGE_CAP(ACQ_INT_PIO_BASE, 0x0);
	}	

	
	return;
}

/*
 * status_write_data()
 */

 
void status_write_data(AcqConn* client, volatile void* status_data)
{
	int status_bytes_sent;	
	
	
	//return if there were no interrupts yet
	if(!status_int_number)
		return;
		
	
	//decrement interrupt number as the data will be transmitted
	status_int_number--;
		
	//send one status bundle (bledp packets*4 == bytes to send)
	status_bytes_sent=send(client->tcp_socket, (void*)status_data, (STATUS_DATA_SIZE)*4, 0);
	
	//re-enable interrupt
	enable_status();

	//check the result
	if(status_bytes_sent != (STATUS_DATA_SIZE)*4)
	{
		/* if send returned an error (-1) or if it sent less data */
		/* something is wrong - close the connection */
		debug_print("send returned %d\n", status_bytes_sent);
		acq_close_connection(client);
		return;
	}
	
	//TEST if suffiecient to replace the scoreboard decoding in JAVA
	//re-initialize (zero) the status memory
	status_init_buffer(status_data);
	
	//reset status busy flag in the custom logic
	IOWR_ALTERA_AVALON_PIO_DATA(STATUS_SENT_PIO_BASE, 1);
	IOWR_ALTERA_AVALON_PIO_DATA(STATUS_SENT_PIO_BASE, 0);	
	
	return;
}

int acq_build_select_list(fd_set * readfds, fd_set * writefds, int* fd_listen, AcqConn* client)
{
	int max_socket;
	
	/* FD_ZERO() clears out the fd_set called socks, so that
		it doesn't contain any file descriptors. */
	
	FD_ZERO(readfds);
	FD_ZERO(writefds);
	
	/* FD_SET() adds the file descriptor "fd_listen" to the fd_set,
		so that select() will return if a connection comes in
		on that socket (which means you have to do accept(), etc. */
	
	FD_SET(*fd_listen,readfds);
	max_socket = *fd_listen + 1;
	
	/* 	we accept only one connection 
		add it to the list if it is already established */
	if(client->tcp_socket != -1)
	{
		FD_SET(client->tcp_socket, readfds);
		max_socket = client->tcp_socket + 1;
		
		/* 	if the acquisition command was received
			add it also to the writeable sockets */
		if(client->acq_bundles_requested)
			FD_SET(client->tcp_socket, writefds);
	}
	
	return max_socket;
}

void acq_handle_new_connection(int fd_listen, AcqConn* client) 
{
	int socket, len;
	struct sockaddr_in  incoming_addr;
	len = sizeof(incoming_addr);
	alt_u8 my_src_mac[6];
	alt_u32 mac_addr_status_buffer[3];
	
	/* Accept incoming connection */
	socket = accept(fd_listen, (struct sockaddr*)&incoming_addr, &len);
	if (socket < 0) {
		alt_NetworkErrorHandler(EXPANDED_DIAGNOSIS_CODE, "[acq_handle_new_connection] accept failed");
	}
	
	/* see if there isn't another connection established */
	if(client->tcp_socket == -1)
	{
		client->tcp_socket = socket;
		client->incoming_addr = incoming_addr.sin_addr.s_addr;
		debug_print("accepted connection request from %s\n", inet_ntoa(incoming_addr.sin_addr));
		
		/* Send MAC address after connection was established */
#if BLEDP_SEND_MAC
		// dig our own MAC address out of the stack variables
		memmove(&my_src_mac, nets[0]->n_mib->ifPhysAddress, 6);
		
		// prepare the MAC buffer
		mac_addr_status_buffer[0] = 0x80040000 | (my_src_mac[0] << 8);
		mac_addr_status_buffer[0] |= 0x80040000 | my_src_mac[1];
		mac_addr_status_buffer[0] = htonl(mac_addr_status_buffer[0]);
		mac_addr_status_buffer[1] = 0x80050000 | (my_src_mac[2] << 8);
		mac_addr_status_buffer[1] |= 0x80050000 | my_src_mac[3];
		mac_addr_status_buffer[1] = htonl(mac_addr_status_buffer[1]);
		mac_addr_status_buffer[2] = 0x80060000 | (my_src_mac[4] << 8);
		mac_addr_status_buffer[2] |= 0x80060000 | my_src_mac[5];
		mac_addr_status_buffer[2] = htonl(mac_addr_status_buffer[2]);	
		
		//send the MAC buffer
		send(client->tcp_socket, (void*)mac_addr_status_buffer, 12, 0);
#endif /* BLEDP_SEND_MAC */

		/* enable status data interrupts (every second) */
		enable_status();
	}
	else
	{
		close(socket);
		debug_print("rejected connection request from %s\n", inet_ntoa(incoming_addr.sin_addr));
	}
}

void acq_close_connection(AcqConn* client)
{
	/* disable status interrupts */
	disable_status();
	
	/* Disable the buffer ready interrupt */
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
	alt_ic_irq_disable(ACQ_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, ACQ_INT_PIO_IRQ);
#else
	alt_irq_disable(ACQ_INT_PIO_IRQ);
#endif 
	
	/* disable UDP stream hardware */
	disable_udp_stream();
	
	/* close the connection */
	client->acq_bundles_requested=-1;
	client->acq_bundles_sent = 0;
	client->acq_buff_limit = 0;
	close(client->tcp_socket);
	client->tcp_socket = -1;
	/* disable FIFO write of all the channels */
	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 0xFF<<24);
	/* Reset the edge capture registers. */
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(ACQ_INT_PIO_BASE, 0x0);
	/* reset acquisition interface */
	acq_reset_interface();
	debug_print("Connection closed\n");
}

void enable_status(void)
{
#if BLEDP_SEND_STATUSES
	//enable status data interrupts (every second)
	IOWR_ALTERA_AVALON_PIO_EDGE_CAP(STATUS_INT_PIO_BASE, 0x0);
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
	alt_ic_irq_enable(STATUS_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, STATUS_INT_PIO_IRQ);
#else
	alt_irq_enable(STATUS_INT_PIO_IRQ);
#endif 

#endif /* BLEDP_SEND_STATUSES */
}


void disable_status(void)
{
#if BLEDP_SEND_STATUSES
	//disable status interrupt
#ifdef ALT_ENHANCED_INTERRUPT_API_PRESENT
	alt_ic_irq_disable(STATUS_INT_PIO_IRQ_INTERRUPT_CONTROLLER_ID, STATUS_INT_PIO_IRQ);
#else
	alt_irq_disable(STATUS_INT_PIO_IRQ);
#endif 

#endif /* BLEDP_SEND_STATUSES */
}

void status_init_buffer(volatile void* status_data)
{
	
	alt_u32* status_dword;
	int i;
	
	status_dword = (alt_u32*)status_data;
	
	for(i=0; i < STATUS_MEM_SPAN/4; i++)
	{
		if (i >= 12 && i <= 15)		//don't reset SHT15 readout to avoid time misalignment in the client
			status_dword[i]|=0x00000280;
		else
			status_dword[i]=0x00000280;
	}
	
	return;
}


int enable_udp_stream(AcqConn* client, alt_u32 chunks_requested) 
{

	int result;
	alt_u16 my_source_port;
	//alt_u16 the_destination_port;
	struct sockaddr_in my_addr;
	struct arptabent *arpent;
	ip_addr the_dest_ip;
	ip_addr first_hop;
	ip_addr my_src_ip;
	alt_u8 the_dest_mac[6];
	alt_u8 my_src_mac[6];
	UDP_INS_STATS insert_stat;
	OS_CPU_SR  cpu_sr = 0;
	
	
	//the_destination_port = 8090;										// well save this off as our UDP dest port for this session
	debug_print("UDP destination port is %d\n", client->udp_port);
	
	// allocate a local udp socket for our source port
	if(client->udp_socket > 0) 
	{
		debug_print("Close UDP socket %d\n", client->udp_socket);		// we want to bind our UDP source port to a socket with our
		close(client->udp_socket);
		client->udp_socket = 0;											// local stack so that it doesn't get reused while we are
	}																	// using it, so we allocate ourselves a socket.  But first
	client->udp_socket = socket(PF_INET, SOCK_DGRAM, 0);				// we close any sockets that we may have attempted to use
	if(client->udp_socket == -1) 
	{												
		debug_print("Cannot allocate local UDP socket\n");
		return -1;
	}
	
	// IP SOURCE ADDRESS
	my_src_ip = nets[0]->n_ipaddr;											// we dig our IP address out of the stack variables
	
    OS_ENTER_CRITICAL();
	// UDP SOURCE PORT
	my_source_port = htons(udp_socket());									// we get the stack to allocate us a UDP port as our source
    OS_EXIT_CRITICAL();
	
	my_addr.sin_family = AF_INET;					// HBO					// and now we want to bind our IP address and UDP port to
	my_addr.sin_port = my_source_port;				// NBO					// the socket that we allocated
	my_addr.sin_addr.s_addr = my_src_ip;			// NBO
	memset(my_addr.sin_zero, '\0', sizeof my_addr.sin_zero);
	
	result = bind(client->udp_socket, (struct sockaddr *)&my_addr, sizeof my_addr);	// if we can't bind to our socket then we deny the request
	if(result == -1) 
	{
		debug_print("Cannot bind the UDP socket\n");
		return -1;
	}
	
	//
	// get the destination mac address
	//
	// IP DEST ADDRESS														// we get the destination IP address out of the session
	the_dest_ip = client->incoming_addr;									// request information passed into us.
	
	// lookup the first hop route for this destination
    OS_ENTER_CRITICAL();													// since we could be in a routed network sending data to
	result = (int)(iproute(the_dest_ip, &first_hop));						// a client on the other side of a router, or many routers,
    OS_EXIT_CRITICAL();														// we ask the stack to give us the IP address of the first
	if(result == 0) 														// hop that our packets should be sent to for the 
	{
		debug_print("Cannot find the first hop route for the destination\n");
		return -1;
	}
	
	// get the MAC address from the arp table
    OS_ENTER_CRITICAL();
	arpent = find_oldest_arp(first_hop);									// we want the MAC address for the first hop that our
	if (arpent->t_pro_addr == first_hop) 									// outbound packets should take
	{
		// DEST MAC ADDRESS
		memmove(&the_dest_mac, arpent->t_phy_addr, 6);
	} 
	else 
	{																		// if we can't locate the first hop MAC address, then we
	    OS_EXIT_CRITICAL();													// deny the request
		debug_print("Cannot get the MAC address from the arp table\n");
		return -1;
	}
    OS_EXIT_CRITICAL();
	
	// SOURCE MAC ADDRESS
	memmove(&my_src_mac, nets[0]->n_mib->ifPhysAddress, 6);					// we dig our own MAC address out of the stack variables
	
	my_source_port = htons(my_source_port);									// we need the IP addresses and UDP ports in Network Byte
	the_dest_ip = htonl(the_dest_ip);										// order for the UDP Payload Insertion peripheral.
	my_src_ip = htonl(my_src_ip);
	
	// start the udp payload inserter
	insert_stat.udp_dst = client->udp_port;								// we fill out this insert_stat struct to pass into the 
	insert_stat.udp_src = my_source_port;									// payload inserter utility function
	insert_stat.ip_dst = the_dest_ip;
	insert_stat.ip_src = my_src_ip;
	insert_stat.mac_dst_hi = (the_dest_mac[0] << 24) | (the_dest_mac[1] << 16) | (the_dest_mac[2] << 8) | (the_dest_mac[3]);
	insert_stat.mac_dst_lo = (the_dest_mac[4] << 8) | (the_dest_mac[5]);
	insert_stat.mac_src_hi = (my_src_mac[0] << 24) | (my_src_mac[1] << 16) | (my_src_mac[2] << 8) | (my_src_mac[3]);
	insert_stat.mac_src_lo = (my_src_mac[4] << 8) | (my_src_mac[5]);
	
	//printf("UDP payload inserter state is %x\n", (IORD((void *)UDP_PAYLOAD_INSERTER_0_BASE, 0)&0x18)>>3);
	//printf("UDP payload inserter status is %x\n", IORD((void *)UDP_PAYLOAD_INSERTER_0_BASE, 0));
	
	if(start_udp_payload_inserter((void *)UDP_PAYLOAD_INSERTER_0_BASE, &insert_stat)) 
	{
		// if we can't start the payload inserter we deny
		debug_print("UDP payload inserter is already running\n");
		return -1;
	}

	/* set the UDP payload size to 352 dwords which can be divided by 8 without a remainder
	 * it should prevent a time drift between the channels when some UDP packets are lost
	 */
	result = start_packet_generator((void *)BLEDP_PACKET_GENERATOR_0_BASE, 352, chunks_requested);
	
	if(result!=0)
	{
		debug_print("BLEDP packet generator is already running (%d)\n", result);
	}
	else
	{
		//reset streaming FIFO
		IOWR_ALTERA_AVALON_PIO_SET_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 1<<1);
		IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 1<<1);
		
		//enable streaming FIFO 
		IOWR_ALTERA_AVALON_PIO_SET_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 1<<23);
	}
	
	return result;
}



int disable_udp_stream(void) 
{
	int res = 0;
	
	if(stop_packet_generator((void *)BLEDP_PACKET_GENERATOR_0_BASE)) 
	{																	//  Disable the packet
		debug_print("BLEDP packet generator is already stopped\n");		//  generator and wait for it to indicate that it is stopped
		res = -1;
	}
	//printf("waiting wait_until_packet_generator_stops_running\n");
	wait_until_packet_generator_stops_running((void *)BLEDP_PACKET_GENERATOR_0_BASE);
	//printf("done wait_until_packet_generator_stops_running\n");
	
	//clear the BLEDP chunks counter
	clear_bledp_chunks_counter((void *)BLEDP_PACKET_GENERATOR_0_BASE);
	
	if(stop_udp_payload_inserter((void *)UDP_PAYLOAD_INSERTER_0_BASE)) 
	{
		debug_print("UDP payload inserter is already stopped\n");	
		res = -1;
	}
	
	//disable streaming FIFO 
	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 1<<23);
	//reset streaming FIFO
	IOWR_ALTERA_AVALON_PIO_SET_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 1<<1);
	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(ACQ_IFACE_CONTROL_PIO_BASE, 1<<1);
	
	return res;
}


int write_digital_potentiometer(unsigned int value, int number, int threshold)
{
	
	//printf("value %d number %d threshold %d\n", value, number, threshold);
	
	if(number>8 || number<1)
	{
		printf("Wrong digital potentiometer number %d\n", number);
		return -1;
	}
	
	//write to EEMEM if the most significant bit (8) of the value (7..0) is set
	if(value&0x100)
	{
		write_digital_potentiometer(value&0xFF, number, threshold);				// if EEMEM then write RDAC first
		IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_0_BASE, P0_DP_EEMEM_MASK);	// '1' - EEMEM
	}
	else
	{
		IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_0_BASE, P0_DP_EEMEM_MASK);	// '0' - RDAC
	}
	
	//set common data bits
	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_0_BASE,P0_DP_DATA_MASK);
	IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_0_BASE,((value<<P0_DP_DATA_OFFSET)&P0_DP_DATA_MASK));
	
	
	//select potentiometer
	if(threshold)
		IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_0_BASE, P0_DP_CH_MASK);	// '1' - threshold
	else
		IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_0_BASE, P0_DP_CH_MASK);	// '0' - offset
		
	//write data to channel's potentiometer given by "number" 
	//toggle the channel's bit
	if(IORD_ALTERA_AVALON_PIO_DATA(COMMAND_PIO_0_BASE) & (P0_DP1_WR_MASK<<(number-1)))
		IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_0_BASE, (P0_DP1_WR_MASK<<(number-1)));
	else
		IOWR_ALTERA_AVALON_PIO_SET_BITS(COMMAND_PIO_0_BASE, (P0_DP1_WR_MASK<<(number-1)));
	
	usleep(1000);
	
	//clear EEMEM bit
	IOWR_ALTERA_AVALON_PIO_CLEAR_BITS(COMMAND_PIO_0_BASE, P0_DP_EEMEM_MASK);	// '0' - RDAC
	
	return 0;
}

