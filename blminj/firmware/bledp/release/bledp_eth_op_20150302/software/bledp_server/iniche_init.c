/******************************************************************************
* Copyright (c) 2006 Altera Corporation, San Jose, California, USA.           *
* All rights reserved. All use of this software and documentation is          *
* subject to the License Agreement located at the end of this file below.     *
*******************************************************************************                                                                             *
* Date - October 24, 2006                                                     *
* Module - iniche_init.c                                                      *
*                                                                             *                                                                             *
******************************************************************************/

/******************************************************************************
 * NicheStack TCP/IP stack initialization and Operating System Start in main()
 * 
 * This example demonstrates the use of MicroC/OS-II running on NIOS II.       
 * In addition it is to serve as a good starting point for designs using       
 * MicroC/OS-II and Altera NicheStack TCP/IP Stack - NIOS II Edition.                                                                                           
 *      
 * Please refer to the Altera NicheStack Tutorial documentation for details on 
 * this software example, as well as details on how to configure the NicheStack
 * TCP/IP networking stack and MicroC/OS-II Real-Time Operating System.  
 */
  
#include <stdio.h>


/* MicroC/OS-II definitions */
#include "includes.h"

/* Server definitions */
#include "bledp_server.h"                                                                   
#include "alt_error_handler.h"

/* Nichestack definitions */
#include "ipport.h"
#include "libport.h"
#include "osport.h"

#include "altera_avalon_pio_regs.h"
#include "alt_2_wire.h"


// SFP Module PHY Registers Definition
#define SFP_SLAVE_ADDR_WRITE            0xAC
#define SFP_SLAVE_ADDR_READ             0xAD
#define SFP_PHY_CONTROL                 0x00
#define SFP_PHY_STATUS                  0x01
#define SFP_PHY_ID0                     0x02
#define SFP_PHY_ID1                     0x03
#define SFP_PHY_AUTONEG_ADVERTISEMENT   0x04
#define SFP_PHY_LINK_PARTNER_ABILITY    0x05
#define SFP_PHY_1000BASET_CONTROL       0x09
#define SFP_PHY_PHY_SPECIFIC_STAT       0x11
#define SFP_PHY_EXT_PHY_SPECIFIC_STAT   0x1B

void sfp_reg_write (int address, int data)
{
    int ret;
    
    alt_2_wireStart();
    ret = alt_2_wireSendByte(SFP_SLAVE_ADDR_WRITE);
    ret = alt_2_wireSendByte(address);
    ret = alt_2_wireSendByte(data >> 8);
    ret = alt_2_wireSendByte(data & 0xFF);
    alt_2_wireStop();
}

int sfp_reg_read (int address)
{
    int ret;
    int data;

    
    alt_2_wireStart();
    ret = alt_2_wireSendByte(SFP_SLAVE_ADDR_WRITE);
    ret = alt_2_wireSendByte(address);
    alt_2_wireStart();
    ret = alt_2_wireSendByte(SFP_SLAVE_ADDR_READ);
    data = alt_2_wireReadByte(SEND_ACK);
    ret = alt_2_wireReadByte(SEND_NACK);
    data = ((data << 8) | ret);
    alt_2_wireStop();
    return data;
}


// TSE Register Offsets (General)

#define TSE_REV                    0x00000000
#define TSE_CMD_CONFIG             0x00000008
#define TSE_MAC_0                  0x0000000C
#define TSE_MAC_1                  0x00000010
#define TSE_FRM_LENGTH             0x00000014
#define TSE_RX_SECTION_EMPTY       0x0000001C
#define TSE_RX_SECTION_FULL        0x00000020
#define TSE_TX_SECTION_EMPTY       0x00000024
#define TSE_TX_SECTION_FULL        0x00000028
#define TSE_RX_ALMOST_EMPTY        0x0000002C
#define TSE_RX_ALMOST_FULL         0x00000030
#define TSE_TX_ALMOST_EMPTY        0x00000034
#define TSE_TX_ALMOST_FULL         0x00000038
#define TSE_TX_IPG_LENGTH          0x0000005C
#define TSE_PCS_CONTROL            0x00000200
#define TSE_PCS_STATUS             0x00000204
#define TSE_PCS_PARTNER_ABILITY    0x00000214
#define TSE_PCS_LINK_TIMER         0x00000248
#define TSE_PCS_IF_MODE            0x00000250


void sfp_config (int register4, int register9)
{
    int status;
    int readdata;
    alt_two_wire * sfp_serial;
    alt_two_wire serial_wire;
    sfp_serial = &serial_wire;
    alt_2_wireSetDelay(100);
    
    // ===================================================
    // Configure the SFP Module to Auto-Neg at selected mode
    // ===================================================
    sfp_serial->scl_pio = I2C_SCL_PIO_BASE;
    sfp_serial->sda_pio = I2C_SDA_PIO_BASE;
    alt_2_wireInit( sfp_serial );
    
    sfp_reg_write (SFP_PHY_EXT_PHY_SPECIFIC_STAT, 0x9084);
    sfp_reg_write (SFP_PHY_CONTROL, 0x9140);
    sfp_reg_write (SFP_PHY_1000BASET_CONTROL, register9);
    sfp_reg_write (SFP_PHY_AUTONEG_ADVERTISEMENT, register4);
    sfp_reg_write (SFP_PHY_CONTROL, 0x9140);


    status = 0x0;
    while (!(status & 0x4)) {
        status = sfp_reg_read (SFP_PHY_STATUS);
    };
    
    debug_print("AUTO_NEG DONE! \n");
    
    // ===================================================
    // Configure TSE0 & TSE1 PCS registers to SGMII
    // ===================================================
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_PCS_CONTROL, 0x00001200);
    //IOWR_32DIRECT(TSE1_BASE, TSE_PCS_CONTROL, 0x00001200); 
   
    // Poll TSE0 PCS Status register to see if Link_Up
	debug_print("waiting for link ...\n");
    status = 0x0;
    readdata = 0;
    while (!(status & 0x20))
    {
       status = IORD_32DIRECT(TSE_MAC_BASE, TSE_PCS_STATUS);
    };
    readdata = IORD_32DIRECT(TSE_MAC_BASE, TSE_PCS_PARTNER_ABILITY);

    debug_print("TSE PCS -- LINK PARTNER ABILITY => 0x%x\n", readdata);
	
	
    
}


/* Definition of task stack for the initial task which will initialize the NicheStack
 * TCP/IP Stack and then initialize the rest of the server tasks. 
 */
OS_STK    InitialTaskStk[TASK_STACKSIZE];

/* Declarations for creating a task with TK_NEWTASK.  
 * All tasks which use NicheStack (those that use sockets) must be created this way.
 * TK_OBJECT macro creates the static task object used by NicheStack during operation.
 * TK_ENTRY macro corresponds to the entry point, or defined function name, of the task.
 * inet_taskinfo is the structure used by TK_NEWTASK to create the task.
 */
TK_OBJECT(to_acquisition_task);
TK_ENTRY(BLEDPAcqServerTask);

struct inet_taskinfo acquisition_task = {
      &to_acquisition_task,
      "BLEDP acquisition server",
      BLEDPAcqServerTask,
      ACQUISITION_TASK_PRIORITY,
      APP_STACK_SIZE,
};


/* InitialTask will initialize the NicheStack
 * TCP/IP Stack and then initialize the rest of the server 
 * RTOS structures and tasks. 
 */
void InitialTask(void *task_data)
{
	INT8U error_code;
	
	/* 	200 ms delay before we start 
		the auto-negotiation  */
	OSTimeDlyHMSM(0,0,0,200);
	
	// Initialize SFP (PHY) module
	
	
	//tse reset
	
	int status = 0;
	
	
	IOWR_32DIRECT(TSE_MAC_BASE, TSE_CMD_CONFIG, 0x00002000);
    status = 0x2000;
    while (status & 0x2000)
    {
        status = IORD_32DIRECT(TSE_MAC_BASE,TSE_CMD_CONFIG);
    };
	
	debug_print("reset done\n");
	
	//tse config
	
	IOWR_32DIRECT(TSE_MAC_BASE, TSE_MAC_0, 12323123);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_MAC_1, 35345231);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_FRM_LENGTH, 0x00002580);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_TX_IPG_LENGTH, 0x0000000C);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_RX_SECTION_EMPTY, 0x00000000);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_RX_SECTION_FULL, 0x00000010);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_TX_SECTION_EMPTY, 0x00000010);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_TX_SECTION_FULL, 0x00000010);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_RX_ALMOST_EMPTY, 0x00000008);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_RX_ALMOST_FULL, 0x00000008);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_TX_ALMOST_EMPTY, 0x00000008); 
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_TX_ALMOST_FULL, 0x00000003);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_PCS_IF_MODE, 0x00000003);
    IOWR_32DIRECT(TSE_MAC_BASE, TSE_PCS_LINK_TIMER, 0x00003333);      
	
	//link config
	
	//sfp_config(0x0D01, 0x0C00); //100-FD
	sfp_config(0x0C01, 0x0E00); //1000-FD

	//IOWR_32DIRECT(TSE_MAC_BASE, TSE_CMD_CONFIG, 0x00000223); //100-FD
	IOWR_32DIRECT(TSE_MAC_BASE, TSE_CMD_CONFIG, 0x0000022B); //1000-FD
	
  
  /*
   * Initialize Altera NicheStack TCP/IP Stack - Nios II Edition specific code.
   * NicheStack is initialized from a task, so that RTOS will have started, and 
   * I/O drivers are available.  Two tasks are created:
   *    "Inet main"  task with priority 2
   *    "clock tick" task with priority 3
   */   
  alt_iniche_init();
  netmain(); 

  /* Wait for the network stack to be ready before proceeding. 
   * iniche_net_ready indicates that TCP/IP stack is ready, and IP address is obtained.
   */
  while (!iniche_net_ready)
    TK_SLEEP(1);

  /* Now that the stack is running, perform the application initialization steps */
  
  /* Application Specific Task Launching Code Block Begin */


  /* Create the main server task. */
  /* Tasks which use Interniche sockets must be created with TK_NEWTASK */
  debug_print("\nBLEDP acquisition server starting up\n");
  TK_NEWTASK(&acquisition_task);
  
  /*create os data structures */


  /* create the other tasks */


  /* Application Specific Task Launching Code Block End */
  
  /*This task is deleted because there is no need for it to run again */
  error_code = OSTaskDel(OS_PRIO_SELF);
  alt_uCOSIIErrorHandler(error_code, 0);
  
  while (1); /* Correct Program Flow should never get here */
}

/* Main creates a single task, InitialTask, and starts task scheduler.
 */

int main (int argc, char* argv[], char* envp[])
{
  
  INT8U error_code;

  /* Clear the RTOS timer */
  OSTimeSet(0);  
  
  
  /* InitialTask will initialize the NicheStack
   * TCP/IP Stack and then initialize the rest of the server 
   * RTOS structures and tasks. 
   */  
  error_code = OSTaskCreateExt(InitialTask,
                             NULL,
                             (void *)&InitialTaskStk[TASK_STACKSIZE],
                             INITIAL_TASK_PRIORITY,
                             INITIAL_TASK_PRIORITY,
                             InitialTaskStk,
                             TASK_STACKSIZE,
                             NULL,
                             0);
  alt_uCOSIIErrorHandler(error_code, 0);

  /*
   * As with all MicroC/OS-II designs, once the initial thread(s) and 
   * associated RTOS resources are declared, we start the RTOS. That's it!
   */
  OSStart();

  
  while(1); /* Correct Program Flow never gets here. */

  return -1;
}

/******************************************************************************
*                                                                             *
* License Agreement                                                           *
*                                                                             *
* Copyright (c) 2006 Altera Corporation, San Jose, California, USA.           *
* All rights reserved.                                                        *
*                                                                             *
* Permission is hereby granted, free of charge, to any person obtaining a     *
* copy of this software and associated documentation files (the "Software"),  *
* to deal in the Software without restriction, including without limitation   *
* the rights to use, copy, modify, merge, publish, distribute, sublicense,    *
* and/or sell copies of the Software, and to permit persons to whom the       *
* Software is furnished to do so, subject to the following conditions:        *
*                                                                             *
* The above copyright notice and this permission notice shall be included in  *
* all copies or substantial portions of the Software.                         *
*                                                                             *
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR  *
* IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,    *
* FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE *
* AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER      *
* LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING     *
* FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER         *
* DEALINGS IN THE SOFTWARE.                                                   *
*                                                                             *
* This agreement shall be governed in all respects by the laws of the State   *
* of California and by the laws of the United States of America.              *
* Altera does not recommend, suggest or require that this reference design    *
* file be used in conjunction or combination with any other product.          *
******************************************************************************/
