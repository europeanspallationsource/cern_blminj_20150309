#include "bledp_packet_generator.h"
#include "bledp_packet_generator_regs.h"

//
// bledp_packet_generator utility routines
//

int start_packet_generator(void *base, alt_u16 dword_count, alt_u32 chunks_requested) {
    
    alt_u32 current_csr;

    // is the packet generator already running?
    current_csr = BLEDP_PACKET_GENERATOR_RD_CSR(base);
    if(current_csr & BLEDP_PACKET_GENERATOR_CSR_GO_BIT_MASK) {
        return 1;
    }
    if(current_csr & BLEDP_PACKET_GENERATOR_CSR_RUNNING_BIT_MASK) {
        return 2;
    }
    
    // clear the counter    
    BLEDP_PACKET_GENERATOR_CLEAR_PACKET_COUNTER(base);
    
    // write the parameter registers
    BLEDP_PACKET_GENERATOR_WR_DWORD_COUNT(base, dword_count);
    BLEDP_PACKET_GENERATOR_WR_PACKET_REQUEST(base, chunks_requested);
    
    // and set the go bit
    BLEDP_PACKET_GENERATOR_WR_CSR(base, BLEDP_PACKET_GENERATOR_CSR_GO_BIT_MASK);
    
    return 0;
}

int stop_packet_generator(void *base) {
    
    // is the packet generator already stopped?
    if(!(BLEDP_PACKET_GENERATOR_RD_CSR(base) & BLEDP_PACKET_GENERATOR_CSR_GO_BIT_MASK)) {
        return 1;
    }

    // clear the go bit
    BLEDP_PACKET_GENERATOR_WR_CSR(base, 0);
    
    return 0;
}

int is_packet_generator_running(void *base) {
    
    // is the packet generator running?
    if((BLEDP_PACKET_GENERATOR_RD_CSR(base) & BLEDP_PACKET_GENERATOR_CSR_RUNNING_BIT_MASK)) {
        return 1;
    }

    return 0;
}


int wait_until_packet_generator_stops_running(void *base) {
    
    // wait until packet generator stops running?
    while(is_packet_generator_running(base));

    return 0;
}

int clear_bledp_chunks_counter(void *base) {
    
    //not allowed if the packet generator is running
    if(is_packet_generator_running(base))
		return -1;
	
	BLEDP_PACKET_GENERATOR_CLEAR_PACKET_COUNTER(base);

    return 0;
}

int get_packet_generator_stats(void *base, PKT_GEN_STATS *stats) {
    
    stats->csr_state        = BLEDP_PACKET_GENERATOR_RD_CSR(base);
    stats->dword_count      = BLEDP_PACKET_GENERATOR_RD_DWORD_COUNT(base);
    stats->packet_count     = BLEDP_PACKET_GENERATOR_RD_PACKET_COUNTER(base);
    stats->packet_request   = BLEDP_PACKET_GENERATOR_RD_PACKET_REQUEST(base);
    
    return 0;
}
