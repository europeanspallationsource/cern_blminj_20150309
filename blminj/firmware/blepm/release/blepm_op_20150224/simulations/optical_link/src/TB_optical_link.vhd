library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use ieee.math_real.all;
use STD.textio.all;
use IEEE.STD_LOGIC_TEXTIO.all;

entity TB_optical_link is
end TB_optical_link;


architecture behaviour of TB_optical_link is

constant CLOCK_PERIOD : time := 8 ns;

signal done_sim          : STD_LOGIC;


signal clk          : STD_LOGIC;
signal areset_tx    : STD_LOGIC;
signal areset_rx    : STD_LOGIC;
signal break_tx     : STD_LOGIC;
signal tx           : STD_LOGIC;
signal tx_out       : STD_LOGIC;

signal SIM_CH1_IN   : std_logic_vector(19 downto 0);
signal SIM_CH2_IN   : std_logic_vector(19 downto 0);
signal SIM_CH3_IN   : std_logic_vector(19 downto 0);
signal SIM_CH4_IN   : std_logic_vector(19 downto 0);
signal SIM_CH5_IN   : std_logic_vector(19 downto 0);
signal SIM_CH6_IN   : std_logic_vector(19 downto 0);
signal SIM_CH7_IN   : std_logic_vector(19 downto 0);
signal SIM_CH8_IN   : std_logic_vector(19 downto 0);
signal SIM_CH1_OUT  : std_logic_vector(19 downto 0);
signal SIM_CH2_OUT  : std_logic_vector(19 downto 0);
signal SIM_CH3_OUT  : std_logic_vector(19 downto 0);
signal SIM_CH4_OUT  : std_logic_vector(19 downto 0);
signal SIM_CH5_OUT  : std_logic_vector(19 downto 0);
signal SIM_CH6_OUT  : std_logic_vector(19 downto 0);
signal SIM_CH7_OUT  : std_logic_vector(19 downto 0);
signal SIM_CH8_OUT  : std_logic_vector(19 downto 0);
signal SIM_CH_OUT_VALID: std_logic;

signal PIM_SIG_ODD :  std_logic_vector(24 downto 1);
signal PIM_SIG_EVEN : std_logic_vector(24 downto 1);
signal ctrv_basic_period : STD_LOGIC;
signal ctrv_beam_in : STD_LOGIC;
signal ctrv_beam_out : STD_LOGIC;


--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------	Architecture begin  ------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
begin

DUT_TX_BLEDP: entity work.bledp_top
port map (
    areset      => areset_tx,
	
	REFCLK2     => clk,
	REFCLK1     => clk,
    
	GXP_RX1     => '0',
	GXP_TX1     => tx_out,
    
    -- simulated acquisition values inputs
    SIM_CH1_IN  => SIM_CH1_IN,
    SIM_CH2_IN  => SIM_CH2_IN,
    SIM_CH3_IN  => SIM_CH3_IN,
    SIM_CH4_IN  => SIM_CH4_IN,
    SIM_CH5_IN  => SIM_CH5_IN,
    SIM_CH6_IN  => SIM_CH6_IN,
    SIM_CH7_IN  => SIM_CH7_IN,
    SIM_CH8_IN  => SIM_CH8_IN,
    
	
	POS1        => '1',
	POS2        => '0',
	POS3        => '0',
	POS4        => '0'
);


DUT_RX_BLEPM: entity work.blepm_top
port map (
    areset      => areset_rx,
	
	REFCLK2     => clk,
	REFCLK1     => clk,
	
	PIM_SIG_ODD		=> PIM_SIG_ODD,
    PIM_SIG_EVEN	=> PIM_SIG_EVEN,
    
	GXP_RX1     => tx,
	GXP_TX1     => open,
    
    -- simulated acquisition values outputs
    SIM_CH1_OUT         => SIM_CH1_OUT,
    SIM_CH2_OUT         => SIM_CH2_OUT,
    SIM_CH3_OUT         => SIM_CH3_OUT,
    SIM_CH4_OUT         => SIM_CH4_OUT,
    SIM_CH5_OUT         => SIM_CH5_OUT,
    SIM_CH6_OUT         => SIM_CH6_OUT,
    SIM_CH7_OUT         => SIM_CH7_OUT,
    SIM_CH8_OUT         => SIM_CH8_OUT,
    SIM_CH_OUT_VALID    => SIM_CH_OUT_VALID,
	
	POS1        => '1',
	POS2        => '0',
	POS3        => '0',
	POS4        => '0'
);

-- 125MHz clk process
process
begin
loop
	clk<='0' ;
	wait for CLOCK_PERIOD/2;
    clk<='1';
	wait for CLOCK_PERIOD/2;
	if done_sim = '1' then
		wait;
	end if;
end loop;
end process;

-- timing events process
-- full cycle (BP, Bin, Bout) is equal to 120 us for faster simulation results
process
begin

ctrv_basic_period <= '0';
ctrv_beam_in <= '0';
ctrv_beam_out <= '0';

wait for 20 us;
wait for 21367 ns;

loop
	ctrv_basic_period <= '1';
	wait for 333 ns;
    ctrv_basic_period <= '0';
	
	wait for 30 us;
	
	ctrv_beam_in <= '1';
	wait for 333 ns;
    ctrv_beam_in <= '0';
	
	
	wait for 50 us;
	
	ctrv_beam_out <= '1';
	wait for 333 ns;
    ctrv_beam_out <= '0';
	
	
	wait for 39 us;
	
	
	if done_sim = '1' then
		wait;
	end if;
end loop;
end process;
PIM_SIG_ODD(21) <= ctrv_basic_period;
PIM_SIG_ODD(22) <= ctrv_beam_in;
PIM_SIG_ODD(23) <= ctrv_beam_out;

-- TX line multiplexer to simulate disconnected optic fibre by setting break_tx bit
tx <= tx_out when break_tx = '0' else '0';


-- Main simulation flow process
process
begin
    done_sim <= '0';
    break_tx <= '0';
    areset_rx <= '1';
    areset_tx <= '1';
    
    SIM_CH1_IN <= x"00001";
    SIM_CH2_IN <= x"00002";
    SIM_CH3_IN <= x"00003";
    SIM_CH4_IN <= x"00004";
    SIM_CH5_IN <= x"00005";
    SIM_CH6_IN <= x"00006";
    SIM_CH7_IN <= x"00007";
    SIM_CH8_IN <= x"00008";
    
    wait for 100 ns;
    areset_tx <= '0';
    
    wait for 6190 ns;
    areset_rx <= '0';
    
    --wait for 18300 ns;
    wait for 18250 ns;
    break_tx <= '1';
    
    wait for 100 ns;
    break_tx <= '0';
    
    --wait for 40000 ns;
    wait for 40000000 ns;
    done_sim <= '1';
    
    report "Simulation completed" severity failure;

end process;


-- compare in-out process
process
variable SIM_CH1_OUT_INT : integer;
variable SIM_CH2_OUT_INT : integer;
variable SIM_CH3_OUT_INT : integer;
variable SIM_CH4_OUT_INT : integer;
variable SIM_CH5_OUT_INT : integer;
variable SIM_CH6_OUT_INT : integer;
variable SIM_CH7_OUT_INT : integer;
variable SIM_CH8_OUT_INT : integer;
variable SIM_CH1_IN_INT : integer;
variable SIM_CH2_IN_INT : integer;
variable SIM_CH3_IN_INT : integer;
variable SIM_CH4_IN_INT : integer;
variable SIM_CH5_IN_INT : integer;
variable SIM_CH6_IN_INT : integer;
variable SIM_CH7_IN_INT : integer;
variable SIM_CH8_IN_INT : integer;
begin

    wait until falling_edge(SIM_CH_OUT_VALID);
    wait for CLOCK_PERIOD/2;
    
    SIM_CH1_OUT_INT := to_integer(unsigned(SIM_CH1_OUT));
    SIM_CH2_OUT_INT := to_integer(unsigned(SIM_CH2_OUT));
    SIM_CH3_OUT_INT := to_integer(unsigned(SIM_CH3_OUT));
    SIM_CH4_OUT_INT := to_integer(unsigned(SIM_CH4_OUT));
    SIM_CH5_OUT_INT := to_integer(unsigned(SIM_CH5_OUT));
    SIM_CH6_OUT_INT := to_integer(unsigned(SIM_CH6_OUT));
    SIM_CH7_OUT_INT := to_integer(unsigned(SIM_CH7_OUT));
    SIM_CH8_OUT_INT := to_integer(unsigned(SIM_CH8_OUT));
    SIM_CH1_IN_INT := to_integer(unsigned(SIM_CH1_IN));
    SIM_CH2_IN_INT := to_integer(unsigned(SIM_CH2_IN));
    SIM_CH3_IN_INT := to_integer(unsigned(SIM_CH3_IN));
    SIM_CH4_IN_INT := to_integer(unsigned(SIM_CH4_IN));
    SIM_CH5_IN_INT := to_integer(unsigned(SIM_CH5_IN));
    SIM_CH6_IN_INT := to_integer(unsigned(SIM_CH6_IN));
    SIM_CH7_IN_INT := to_integer(unsigned(SIM_CH7_IN));
    SIM_CH8_IN_INT := to_integer(unsigned(SIM_CH8_IN));
    
    -- Print messages when incorrect
    assert SIM_CH1_IN = SIM_CH1_OUT report "Channel 1 received output (" & integer'image(SIM_CH1_OUT_INT) & ") differs the input (" & integer'image(SIM_CH1_IN_INT) & ")." severity error;
    assert SIM_CH2_IN = SIM_CH2_OUT report "Channel 2 received output (" & integer'image(SIM_CH2_OUT_INT) & ") differs the input (" & integer'image(SIM_CH2_IN_INT) & ")." severity error;
    assert SIM_CH3_IN = SIM_CH3_OUT report "Channel 3 received output (" & integer'image(SIM_CH3_OUT_INT) & ") differs the input (" & integer'image(SIM_CH3_IN_INT) & ")." severity error;
    assert SIM_CH4_IN = SIM_CH4_OUT report "Channel 4 received output (" & integer'image(SIM_CH4_OUT_INT) & ") differs the input (" & integer'image(SIM_CH4_IN_INT) & ")." severity error;
    assert SIM_CH5_IN = SIM_CH5_OUT report "Channel 5 received output (" & integer'image(SIM_CH5_OUT_INT) & ") differs the input (" & integer'image(SIM_CH5_IN_INT) & ")." severity error;
    assert SIM_CH6_IN = SIM_CH6_OUT report "Channel 6 received output (" & integer'image(SIM_CH6_OUT_INT) & ") differs the input (" & integer'image(SIM_CH6_IN_INT) & ")." severity error;
    assert SIM_CH7_IN = SIM_CH7_OUT report "Channel 7 received output (" & integer'image(SIM_CH7_OUT_INT) & ") differs the input (" & integer'image(SIM_CH7_IN_INT) & ")." severity error;
    assert SIM_CH8_IN = SIM_CH8_OUT report "Channel 8 received output (" & integer'image(SIM_CH8_OUT_INT) & ") differs the input (" & integer'image(SIM_CH8_IN_INT) & ")." severity error;
    
    -- Print messages if correct
    assert SIM_CH1_IN /= SIM_CH1_OUT report "Channel 1 received output (" & integer'image(SIM_CH1_OUT_INT) & ") is equal to the input (" & integer'image(SIM_CH1_IN_INT) & ")." severity note;
    assert SIM_CH2_IN /= SIM_CH2_OUT report "Channel 2 received output (" & integer'image(SIM_CH2_OUT_INT) & ") is equal to the input (" & integer'image(SIM_CH2_IN_INT) & ")." severity note;
    assert SIM_CH3_IN /= SIM_CH3_OUT report "Channel 3 received output (" & integer'image(SIM_CH3_OUT_INT) & ") is equal to the input (" & integer'image(SIM_CH3_IN_INT) & ")." severity note;
    assert SIM_CH4_IN /= SIM_CH4_OUT report "Channel 4 received output (" & integer'image(SIM_CH4_OUT_INT) & ") is equal to the input (" & integer'image(SIM_CH4_IN_INT) & ")." severity note;
    assert SIM_CH5_IN /= SIM_CH5_OUT report "Channel 5 received output (" & integer'image(SIM_CH5_OUT_INT) & ") is equal to the input (" & integer'image(SIM_CH5_IN_INT) & ")." severity note;
    assert SIM_CH6_IN /= SIM_CH6_OUT report "Channel 6 received output (" & integer'image(SIM_CH6_OUT_INT) & ") is equal to the input (" & integer'image(SIM_CH6_IN_INT) & ")." severity note;
    assert SIM_CH7_IN /= SIM_CH7_OUT report "Channel 7 received output (" & integer'image(SIM_CH7_OUT_INT) & ") is equal to the input (" & integer'image(SIM_CH7_IN_INT) & ")." severity note;
    assert SIM_CH8_IN /= SIM_CH8_OUT report "Channel 8 received output (" & integer'image(SIM_CH8_OUT_INT) & ") is equal to the input (" & integer'image(SIM_CH8_IN_INT) & ")." severity note;

end process;

end behaviour;


