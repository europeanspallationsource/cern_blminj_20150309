------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    07/11/2014
--Module Name:    bledp_top
--Project Name:   bledp_top
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity bledp_top is

port(

	areset:					in std_logic;	
	
	-- Reference Clock 125 MHz
	REFCLK2:				in std_logic;
	-- Reference Clock 125 MHz
	REFCLK1:				in std_logic;
	
	-- IX. Optical/Network SPF Link
	GXP_RX1:                in  std_logic;
    GXP_TX1:                out std_logic;
    
    -- simulated acquisition values inputs
    SIM_CH1_IN:             in std_logic_vector(19 downto 0);
    SIM_CH2_IN:             in std_logic_vector(19 downto 0);
    SIM_CH3_IN:             in std_logic_vector(19 downto 0);
    SIM_CH4_IN:             in std_logic_vector(19 downto 0);
    SIM_CH5_IN:             in std_logic_vector(19 downto 0);
    SIM_CH6_IN:             in std_logic_vector(19 downto 0);
    SIM_CH7_IN:             in std_logic_vector(19 downto 0);
    SIM_CH8_IN:             in std_logic_vector(19 downto 0);
	
	POS1:						in std_logic;
	POS2:						in std_logic;
	POS3:						in std_logic;
	POS4:						in std_logic

);
	
end entity bledp_top;


architecture rtl of bledp_top is

attribute keep: boolean;

--constant sys_clk_freq : natural := 83333333;
--constant sys_clk_freq : natural := 50000000;
constant sys_clk_freq : natural := 40000000;
constant sys_clk_ns_period : natural := 1000000000/sys_clk_freq;



-- PLL signals
signal sys_clk:			std_logic;
signal clk_40:			std_logic;
signal pll_locked:		std_logic;

signal pll_areset_eth:			std_logic;
signal pll_locked_eth:			std_logic;
signal rx_freqlocked_eth:		std_logic;
signal pll_locked_opt:			std_logic;
signal rx_freqlocked_opt:		std_logic;
signal rx_enabyteord_opt:		std_logic;
signal rx_byteorderalignstatus_opt:		std_logic;
signal rx_syncstatus_opt:		std_logic_vector(1 downto 0);
signal tx_digitalreset_opt:		std_logic;
signal tx_digitalreset_eth:		std_logic;
signal pll_areset_opt:			std_logic;
signal rx_clkout_opt:			std_logic;
signal tx_clkout_opt:			std_logic;
signal rx_dataout_opt:			std_logic_vector(15 downto 0);


-- Reset controller
signal rst_sys_clk_n_shift:	std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
signal rx_analogreset_shift:	std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
signal tx_digitalreset_opt_shift:	std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
signal tx_digitalreset_eth_shift:	std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
signal rst_sys_clk_n:			std_logic;
signal rx_analogreset_opt:			std_logic;
signal rx_analogreset_eth:			std_logic;
signal rx_digitalreset_opt:		std_logic;
signal rx_digitalreset_eth:		std_logic;


-- GX reconfig component signals
signal reconfig_fromgxb_0	: STD_LOGIC_VECTOR (4 DOWNTO 0);
signal reconfig_fromgxb_1	: STD_LOGIC_VECTOR (4 DOWNTO 0);
signal reconfig_fromgxb	: STD_LOGIC_VECTOR (9 DOWNTO 0);
signal reconfig_busy		: STD_LOGIC ;
signal reconfig_togxb		: STD_LOGIC_VECTOR (3 DOWNTO 0);

signal tx_ctrlenable_opt: std_logic_vector(1 downto 0);
signal tx_datain_opt: std_logic_vector(15 downto 0);
signal rx_ctrldetect:			 std_logic_vector(1 downto 0);
signal rx_errdetect:			 std_logic_vector(1 downto 0);

signal gen_header_sig:			std_logic_vector(15 downto 0);
signal proto_data_tx:			std_logic_vector(15 downto 0);
signal proto_startofpacket_tx:	std_logic;
signal proto_endofpacket_tx:	std_logic;
signal proto_valid_tx:			std_logic;
signal proto_ready_tx:			std_logic;

signal proto_data_rx:			std_logic_vector(15 downto 0);
signal proto_startofpacket_rx:	std_logic;
signal proto_endofpacket_rx:	std_logic;
signal proto_valid_rx:			std_logic;
signal proto_ready_rx:			std_logic;
signal proto_error_rx:			std_logic_vector(7 downto 0);

signal chk_no_err:			std_logic;
signal chk_crc_err:			std_logic;
signal chk_hdr_err:			std_logic;
signal chk_seq_err:			std_logic;
signal chk_sop_err:			std_logic;
signal chk_eop_err:			std_logic;

-- acquisition path signals
signal acq_path_fifo_in : std_logic_vector(159 downto 0);
signal acq_path_fifo_in_vld : std_logic;
attribute keep of acq_path_fifo_in: signal is true;
attribute keep of acq_path_fifo_in_vld: signal is true;

signal acq_cnt : unsigned(23 downto 0);

signal acq_path_out : std_logic_vector(17 downto 0);
signal acq_path_out_vld : std_logic;
signal acq_path_out_next : std_logic;

constant p_cnt_max : natural :=  2000/sys_clk_ns_period - 1;
signal p_cnt : integer range 0 to p_cnt_max;
signal p_cnt_done : std_logic;

BEGIN

	------------------------------------------------------------------
	-- PLL
	------------------------------------------------------------------
    
    PLL_inst: entity work.REFCLK_PLL
    port map 
    ( 
        areset  => areset, -- for simulation
        inclk0 	=> REFCLK2,
        c0      => sys_clk,
        c1      => clk_40,
        locked  => pll_locked
    );
	
	------------------------------------------------------------------
	-- synchronous reset circuits
	------------------------------------------------------------------
	sys_rst_controller: process (sys_clk, pll_locked) -- rst (from reset) synchronises to sys_clk
	begin
		if pll_locked = '0' then
			rst_sys_clk_n_shift <= (others => '0');      
		elsif rising_edge (sys_clk) then
			rst_sys_clk_n_shift <= rst_sys_clk_n_shift(rst_sys_clk_n_shift'HIGH-1 downto rst_sys_clk_n_shift'LOW) & '1';
		end if;
	end process;
	-- reset is the output of the high bit of the shift register.
	rst_sys_clk_n <= rst_sys_clk_n_shift(rst_sys_clk_n_shift'HIGH);
	

	
	
	------------------------------------------------------------------
	-- GX reconfiguration components
	------------------------------------------------------------------
	
	GX_reconfig_eth_i: entity work.gx_reconfig
	PORT MAP
	(
		reconfig_clk 		=> clk_40,
		reconfig_fromgxb 	=> reconfig_fromgxb,
		busy				=> reconfig_busy,
		reconfig_togxb		=> reconfig_togxb
	);
	
	reconfig_fromgxb <= reconfig_fromgxb_1 & reconfig_fromgxb_0;
	
	------------------------------------------------------------------
	-- GX reset controllers
	------------------------------------------------------------------
	altgx0_rst_i: entity work.altgx_rst_control
	generic map (
		t_clk_period_ns => sys_clk_ns_period,
		t_ltd_auto_ns => 4000,
		t_digitalreset_ns => 200,
		t_analogreset_ns => 50
	)
	port map (
		--local clock and reset
		clk 	=> sys_clk,
		nrst 	=> rst_sys_clk_n,
	
		--transceiver status
		busy 			=> reconfig_busy,
		pll_locked 		=> pll_locked_opt,
		rx_freqlocked 	=> rx_freqlocked_opt,
	
		--transceiver reset signals
		pll_areset 		=> pll_areset_opt,
		tx_digitalreset => tx_digitalreset_opt,
		rx_analogreset 	=> rx_analogreset_opt,
		rx_digitalreset => rx_digitalreset_opt
	);
	
	------------------------------------------------------------------
	-- Transceiver Instance
	------------------------------------------------------------------
	
	altgx_opt_i : entity work.altgx_ch0 PORT MAP (
		cal_blk_clk	 				=> sys_clk,
		pll_inclk(0) 				=> REFCLK2,
		reconfig_clk	 			=> clk_40,
		reconfig_togxb	 			=> reconfig_togxb,
		rx_analogreset(0)			=> rx_analogreset_opt,
		rx_ctrldetect		 		=> rx_ctrldetect,
		rx_datain(0)	 			=> GXP_RX1,
		rx_digitalreset(0)			=> rx_digitalreset_opt,
		tx_ctrlenable	 			=> tx_ctrlenable_opt,	--one control signal per byte
		tx_datain	 				=> tx_datain_opt,
		tx_digitalreset(0)			=> tx_digitalreset_opt,
		reconfig_fromgxb			=> reconfig_fromgxb_0,
		rx_clkout(0)				=> rx_clkout_opt,
		rx_dataout					=> rx_dataout_opt,
		tx_clkout(0)				=> tx_clkout_opt,
		tx_dataout(0)				=> GXP_TX1,
		pll_areset(0)				=> pll_areset_opt,
		pll_locked(0)				=> pll_locked_opt,
		rx_errdetect				=> rx_errdetect,
		rx_freqlocked(0)			=> rx_freqlocked_opt,
		rx_syncstatus			    => rx_syncstatus_opt,
		rx_enabyteord(0)            => rx_enabyteord_opt,
        rx_byteorderalignstatus(0)  => rx_byteorderalignstatus_opt
	);
    
    rx_enabyteord_opt <= rx_syncstatus_opt(1) or rx_syncstatus_opt(0);
	
	------------------------------------------------------------------
	-- Protocol for the optical transceiver
	------------------------------------------------------------------
	protocol_tx_i : entity work.protocol_tx 
	PORT MAP (
		--local clock and reset
		proto_clk			=> sys_clk,
		proto_nrst			=> rst_sys_clk_n,
		
		--input port
		proto_data				=> acq_path_out(15 downto 0),
		proto_startofpacket		=> acq_path_out(17),
		proto_endofpacket		=> acq_path_out(16),
		proto_valid				=> acq_path_out_vld,
		proto_ready				=> acq_path_out_next,
		
		--transceiver signals
		tx_digitalreset		=> tx_digitalreset_opt,
		tx_cntrlenable		=> tx_ctrlenable_opt,
		tx_datain			=> tx_datain_opt,
		tx_clockout			=> tx_clkout_opt
	);
    
    ------------------------------------------------------------------
	-- Acquisition Path
	------------------------------------------------------------------
    
    
    -- increasing counter to insert to the acquisition packets sent via the optical links
    acq_cnt_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' then
                acq_cnt <= (others=>'0');
            elsif acq_path_fifo_in_vld = '1' then
                acq_cnt <= acq_cnt + 1;
            end if;
        end if;
    end process;
    
	acq_path_fifo_in <= SIM_CH1_IN & SIM_CH2_IN & SIM_CH3_IN & SIM_CH4_IN & SIM_CH5_IN & SIM_CH6_IN & SIM_CH7_IN & SIM_CH8_IN;
	acq_path_fifo_in_vld <= p_cnt_done;
    
    -- 2us measurement period free running counter
    per_cnt_p: process(sys_clk, rst_sys_clk_n)
    begin
        if rst_sys_clk_n = '0' then
            p_cnt <= 0;
        elsif rising_edge(sys_clk) then
            if p_cnt_done = '1' then
                p_cnt <= 0;
            else
                p_cnt <= p_cnt + 1;
            end if;
        end if;
    end process;
    p_cnt_done <= '1' when p_cnt = p_cnt_max else '0';
	
	-- Acquisition path component
    acq_path_i: entity work.acq_packet_creator
	port map
	(
        --clock and reset
        gen_clk     => sys_clk,
        gen_nrst    => rst_sys_clk_n, 
        
        --acquisition channel inputs
        acq_ch1     => SIM_CH1_IN,
        acq_ch2     => SIM_CH2_IN,
        acq_ch3     => SIM_CH3_IN,
        acq_ch4     => SIM_CH4_IN,
        acq_ch5     => SIM_CH5_IN,
        acq_ch6     => SIM_CH6_IN,
        acq_ch7     => SIM_CH7_IN,
        acq_ch8     => SIM_CH8_IN,
        
        --header information
        gen_timestamp   => std_logic_vector(acq_cnt),
        gen_card_no     => x"41",
        gen_pkt_type    => x"00",
        
        --generator output port
        gen_data            => acq_path_out(15 downto 0),
        gen_startofpacket   => acq_path_out(17),
        gen_endofpacket     => acq_path_out(16),
        gen_valid           => acq_path_out_vld,
        gen_ready           => acq_path_out_next,
        
        --control
        gen_done            => open,
        gen_enable          => p_cnt_done
	);
	

end architecture rtl;
