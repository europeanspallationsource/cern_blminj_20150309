------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    07/11/2014
--Module Name:    blepm_top
--Project Name:   blepm_top
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use work.vhdl_func_pkg.all;
use work.BLMINJ_pkg.all;

entity blepm_top is

port(

	areset:					in std_logic;	
	
	-- Reference Clock 125 MHz
	REFCLK2:				in std_logic;
	-- Reference Clock 125 MHz
	REFCLK1:				in std_logic;
	
	-- II. Header J2
    PIM_SIG_ODD:            in  std_logic_vector(24 downto 1);
    PIM_SIG_EVEN:           out std_logic_vector(24 downto 1);
	
	-- IX. Optical/Network SPF Link
	GXP_RX1:                in  std_logic;
    GXP_TX1:                out std_logic;
    
    -- simulated acquisition values outputs
    SIM_CH1_OUT:            out std_logic_vector(19 downto 0);
    SIM_CH2_OUT:            out std_logic_vector(19 downto 0);
    SIM_CH3_OUT:            out std_logic_vector(19 downto 0);
    SIM_CH4_OUT:            out std_logic_vector(19 downto 0);
    SIM_CH5_OUT:            out std_logic_vector(19 downto 0);
    SIM_CH6_OUT:            out std_logic_vector(19 downto 0);
    SIM_CH7_OUT:            out std_logic_vector(19 downto 0);
    SIM_CH8_OUT:            out std_logic_vector(19 downto 0);
    SIM_CH_OUT_VALID:       out std_logic;
	
	POS1:						in std_logic;
	POS2:						in std_logic;
	POS3:						in std_logic;
	POS4:						in std_logic

);
	
end entity blepm_top;


architecture rtl of blepm_top is

    --===============--
    -- CMA CONSTANTS --
    --===============--
    
    -- GXprot_CMI
    constant GXprot_data_size:      integer := 18;
    constant GXprot_rx_number:      integer := 1;

    -- checkCRC_CMI
    constant checkCRC_data_size:    integer := 18;
    constant checkCRC_rx_number:    integer := 1;
    
    -- PCinp_CMI
    constant PCinp_data_size:       integer := 160;
    constant PCinp_rx_number:       integer := 1;
    
    -- PCout_CMI 
    constant PCout_data_size:       integer := 66;
    constant PCout_rx_number:       integer := 1;
    
    -- proc SCMI
    constant proc_data_size:        integer := 66;
    -- cmd SCMI
    constant cmd_data_size:         integer := 18;
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    -- Clocks
    constant sys_clk_freq:          natural := 40000000;
    constant sys_clk_ns_period:     natural := 1000000000/sys_clk_freq;    
    
    -- Timing
    constant tim_cnt_size:          integer := 32;
    
    -- Processing Core
    constant RS_number_per_ch:      integer := 3;
    constant channel_max:           integer := 8;
    constant PM_active:             integer := 0;  
    
    -- proc_FIFO
    constant PCfifo_data_size:      integer := proc_data_size;
    constant PCfifo_depth:          integer := 4096;
    
    -- Synchronisation
    constant sync_stages:           integer := 2;
    
    --========--
    -- CLOCKS --
    --========--
    
    signal clk_main:                std_logic;
    signal clk_40:                  std_logic;
    signal pll_locked:              std_logic;

    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    -- Reset Controller
    signal rst_sys_clk_n_shift:	    std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
    signal rst_sys_clk_n:		    std_logic;
    
    -- Transceivers	--
    
    -- transceivers reset signals
    signal pll_areset_opt:			std_logic;
    signal pll_locked_opt:			std_logic;
    signal rx_freqlocked_opt:		std_logic;
    signal rx_enabyteord_opt:		std_logic;
    signal rx_byteorderalignstatus_opt:		std_logic;
    signal rx_syncstatus_opt:		std_logic_vector(1 downto 0);
    signal tx_digitalreset_opt:		std_logic;
    signal rx_analogreset_opt:		std_logic;
    signal rx_digitalreset_opt:		std_logic;

    --optical link signals
    signal rx_clkout_opt:			std_logic;
    signal tx_clkout_opt:			std_logic;
    signal rx_dataout_opt:			std_logic_vector(15 downto 0);
    signal tx_datain_opt: 			std_logic_vector(15 downto 0);
    signal tx_ctrlenable_opt: 		std_logic_vector(1 downto 0);
    signal rx_ctrldetect_opt:		std_logic_vector(1 downto 0);
    signal rx_errdetect_opt:		std_logic_vector(1 downto 0);

    -- GX reconfig component signals
    signal reconfig_fromgxb_0:      std_logic_vector(4 downto 0);
    signal reconfig_fromgxb_1:      std_logic_vector(4 downto 0);
    signal reconfig_busy:           std_logic;
    signal reconfig_togxb:          std_logic_vector(3 downto 0);
    
    -- SFP Module Internal Signals
    signal RATE_SELECT_1_int:       std_logic;
    signal RATE_SELECT_2_int:       std_logic;
    signal TX_DISABLE_1_int:        std_logic;
    signal TX_DISABLE_2_int:        std_logic;
    
    -- Parameters --
    
    signal PARAM_CAP_select:        slv_array(0 to 7)(3 downto 0);
    signal PARAM_LoC_TH:            slv_array(0 to 7)(63 downto 0);
    signal PARAM_LoBP_TH:           slv_array(0 to 7)(63 downto 0);
    signal PARAM_RS_TH:             slv_array_array(0 to 7)(0 to 2)(63 downto 0);
    signal PARAM_CAP_START:         std_logic_vector(31 downto 0);
    signal PARAM_CAP_STOP:          std_logic_vector(31 downto 0);
    signal PARAM_CAP_SMPLRATE:      std_logic_vector(31 downto 0);
    signal PARAM_EVO_SMPLRATE:      std_logic_vector(31 downto 0);
    signal PARAM_MS_SMPLRATE:       std_logic_vector(31 downto 0);
    signal PARAM_MACH_TYPE:         std_logic_vector(31 downto 0);
    
    
     
    -- Timing Controller --
    
    -- External Timing Inputs
    signal BP_TIM:                  std_logic;
    signal BEAM_IN_TIM:             std_logic;
    signal BEAM_OUT_TIM:            std_logic;
    signal observed_smpl:           std_logic;
    
    -- Timing Pulses
    signal BP_pulse:                std_logic;
    signal BIn_pulse:               std_logic;
    signal BOut_pulse:              std_logic;
    signal EVOstart_pulse:          std_logic;
    signal EVOstop_pulse:           std_logic;
    signal EVO_pulse:               std_logic;
    signal CAPstart_pulse:          std_logic;
    signal CAPstop_pulse:           std_logic;
    signal CAP_pulse:               std_logic;
    -- Timing Status
    signal BEAM_ACT:                std_logic;
    -- Timing Counter
    signal ACQ_CNT:                 std_logic_vector(tim_cnt_size-1 downto 0);
    signal BP_CNT:                  std_logic_vector(tim_cnt_size-1 downto 0);
    signal EVO_CNT:                 std_logic_vector(tim_cnt_size-1 downto 0);
    signal CAP_CNT:                 std_logic_vector(tim_cnt_size-1 downto 0);

    -- SSM --
    signal BIS_1:                   std_logic;
    signal BIS_2:                   std_logic;
    
    --=============--
    -- CMA SIGNALS --
    --=============--
    
    -- proc SCMI
    signal proc_scmi_rx_tag:        std_logic;
    signal proc_scmi_tx_tag:        std_logic;
    signal proc_scmi_tx_data:       std_logic_vector(proc_data_size-1 downto 0);
    signal proc_tx_data:            std_logic_vector(proc_data_size-1 downto 0);
    signal proc_tx_vld:             std_logic;
    signal proc_tx_next:            std_logic;
    
    -- cmd SCMI
    signal cmd_scmi_rx_tag:         std_logic;
    signal cmd_scmi_tx_tag:         std_logic;
    signal cmd_scmi_tx_data:        std_logic_vector(cmd_data_size-1 downto 0);
    signal cmd_rx_data:             std_logic_vector(cmd_data_size-1 downto 0);
    signal cmd_rx_vld:              std_logic;
    signal cmd_rx_next:             std_logic;
    
    -- GXprot CMI
    signal GXprot_tx_data:          std_logic_vector(GXprot_data_size-1 downto 0);
    signal GXprot_tx_vld:           std_logic;
    signal GXprot_tx_next:          std_logic;
    signal GXprot_rx_data:          slv_array(GXprot_rx_number-1 downto 0)(GXprot_data_size-1 downto 0);
    signal GXprot_rx_vld:           std_logic_vector(GXprot_rx_number-1 downto 0);
    signal GXprot_rx_next:          std_logic_vector(GXprot_rx_number-1 downto 0);
    
    
    -- PCinp_CMI
    signal PCinp_tx_data:           std_logic_vector(PCinp_data_size-1 downto 0);
    signal PCinp_tx_data_ch_mirror: std_logic_vector(PCinp_data_size-1 downto 0);
    signal PCinp_tx_vld:            std_logic;
    signal PCinp_tx_next:           std_logic;
    signal PCinp_rx_data:           slv_array(PCinp_rx_number-1 downto 0)(PCinp_data_size-1 downto 0);
    signal PCinp_rx_vld:            std_logic_vector(PCinp_rx_number-1 downto 0);
    signal PCinp_rx_next:           std_logic_vector(PCinp_rx_number-1 downto 0);
    
    -- PCout_CMI
    signal PCout_tx_data:           std_logic_vector(PCout_data_size-1 downto 0);
    signal PCout_tx_vld:            std_logic;
    signal PCout_tx_next:           std_logic;
    signal PCout_rx_data:           slv_array(PCout_rx_number-1 downto 0)(PCout_data_size-1 downto 0);
    signal PCout_rx_vld:            std_logic_vector(PCout_rx_number-1 downto 0);
    signal PCout_rx_next:           std_logic_vector(PCout_rx_number-1 downto 0);
    
    signal proto_data:				std_logic_vector(15 downto 0);
	signal proto_startofpacket:	    std_logic;
	signal proto_endofpacket:		std_logic;
	signal proto_valid:			    std_logic;
	signal proto_ready:			    std_logic;
    
    signal validator_data:				std_logic_vector(15 downto 0);
	signal validator_startofpacket:	    std_logic;
	signal validator_endofpacket:		std_logic;
	signal validator_valid:			    std_logic;
	signal validator_ready:			    std_logic;
    signal packet_length:       std_logic_vector(7 downto 0);
    signal packet_card_no:      std_logic_vector(3 downto 0);
    signal packet_type_no:      std_logic_vector(3 downto 0);
    signal packet_timestamp:    std_logic_vector(23 downto 0);
    signal valid_packet:        std_logic;
    signal err_drop_word:       std_logic;
    signal err_drop_bad_crc:    std_logic;
    signal err_drop_no_eop:     std_logic;
    signal err_tmp_fifo_full:   std_logic;
    
    signal err_drop_word_mux:   std_logic;

BEGIN

	--============--
    -- CLOCK PLLs --
    --============--
    
    PLL_inst: entity work.REFCLK_PLL
    port map 
    ( 
        areset  => areset, -- for simulation
        inclk0 	=> REFCLK2,
        c0      => clk_main,
        c1      => clk_40,
        locked  => pll_locked
    );
	
	
	--======--
    -- SYNC --
    --======--
    
    sync_BP: entity work.sync
    generic map
    (
        data_width  => 1,
        stages      => sync_stages,
        init_value  => '0'
    )
    port map
    (
        dest_clk                => clk_main,
        async_data              => vectorize(PIM_SIG_ODD(21)),
        scalarize(sync_data)    => BP_TIM
    );
    
    sync_BIn: entity work.sync
    generic map
    (
        data_width  => 1,
        stages      => sync_stages,
        init_value  => '0'
    )
    port map
    (
        dest_clk                => clk_main,
        async_data              => vectorize(PIM_SIG_ODD(22)),
        scalarize(sync_data)    => BEAM_IN_TIM
    );
    
    sync_BOut: entity work.sync
    generic map
    (
        data_width  => 1,
        stages      => sync_stages,
        init_value  => '0'
    )
    port map
    (
        dest_clk                => clk_main,
        async_data              => vectorize(PIM_SIG_ODD(23)),
        scalarize(sync_data)    => BEAM_OUT_TIM
    );
    
	
	--================================--
    -- TRANSCEIVER SYNC RESET CIRCUIT --
    --================================--
    
    sys_rst_controller: process (clk_main, pll_locked) -- rst (from reset) synchronises to sys_clk
	begin
		if pll_locked = '0' then
			rst_sys_clk_n_shift <= (others => '0');      
		elsif rising_edge (clk_main) then
			rst_sys_clk_n_shift <= rst_sys_clk_n_shift(rst_sys_clk_n_shift'HIGH-1 downto rst_sys_clk_n_shift'LOW) & '1';
		end if;
	end process;
	-- reset is the output of the high bit of the shift register.
	rst_sys_clk_n <= rst_sys_clk_n_shift(rst_sys_clk_n_shift'HIGH);
    
    --=============--
    -- TRANSCEIVER --
    --=============--
    
    -- GX Reset Controller (user-defined)
    altgx0_rst_ctrl: entity work.altgx_rst_control
	GENERIC MAP
    (
		t_clk_period_ns     => sys_clk_ns_period,
		t_ltd_auto_ns       => 4000,
		t_digitalreset_ns   => 200,
		t_analogreset_ns    => 50
	)
	PORT MAP
    (
		--local clock and reset
		clk 	        => clk_main,
		nrst 	        => rst_sys_clk_n,
		--transceiver status
		busy 			=> reconfig_busy,
		pll_locked 		=> pll_locked_opt,
		rx_freqlocked 	=> rx_freqlocked_opt,
		--transceiver reset signals
		pll_areset 		=> pll_areset_opt,
		tx_digitalreset => tx_digitalreset_opt,
		rx_analogreset 	=> rx_analogreset_opt,
		rx_digitalreset => rx_digitalreset_opt
	);
    
    -- GX Reconfig
    GX_reconfig: entity work.gx_reconfig
	PORT MAP
	(
		reconfig_clk 		=> clk_40,
		reconfig_fromgxb 	=> reconfig_fromgxb_1 & reconfig_fromgxb_0,
		busy				=> reconfig_busy,
		reconfig_togxb		=> reconfig_togxb
	);
     
    -- GX Core Channel 0
    GX_chan0 : entity work.altgx_ch0 
    PORT MAP
    (
		cal_blk_clk	 				=> clk_main,
		pll_inclk(0) 				=> REFCLK2,
		reconfig_clk	 			=> clk_40,
		reconfig_togxb	 			=> reconfig_togxb,
		rx_analogreset(0)			=> rx_analogreset_opt,
		rx_ctrldetect		 		=> rx_ctrldetect_opt,
		rx_datain(0)	 			=> GXP_RX1,
		rx_digitalreset(0)			=> rx_digitalreset_opt,
		tx_ctrlenable	 			=> tx_ctrlenable_opt,	--one control signal per byte
		tx_datain	 				=> tx_datain_opt,
		tx_digitalreset(0)			=> tx_digitalreset_opt,
		reconfig_fromgxb			=> reconfig_fromgxb_0,
		rx_clkout(0)				=> rx_clkout_opt,
		rx_dataout					=> rx_dataout_opt,
		tx_clkout(0)				=> tx_clkout_opt,
		tx_dataout(0)				=> GXP_TX1,
		pll_areset(0)				=> pll_areset_opt,
		pll_locked(0)				=> pll_locked_opt,
		rx_errdetect				=> rx_errdetect_opt,
		rx_freqlocked(0)			=> rx_freqlocked_opt,
		rx_syncstatus			    => rx_syncstatus_opt,
		rx_enabyteord(0)            => rx_enabyteord_opt,
        rx_byteorderalignstatus(0)  => rx_byteorderalignstatus_opt
	);
    
    rx_enabyteord_opt <= rx_syncstatus_opt(1) or rx_syncstatus_opt(0);
     
    --======================--
    -- RECEIVER GX PROTOCOL --
    --======================--
    
    protocol_rx_i: entity work.protocol_rx 
	PORT MAP 
    (
		--local clock and reset
		proto_clk			    => clk_main,
		proto_nrst			    => rst_sys_clk_n,	
		--output port
		proto_data				=> proto_data,
		proto_startofpacket		=> proto_startofpacket,
		proto_endofpacket		=> proto_endofpacket,
		proto_valid				=> proto_valid,
		proto_ready				=> proto_ready,
		--proto error signal
		proto_error				=> open,
		--transceiver signals
		rx_digitalreset		    => rx_digitalreset_opt,
		rx_errdetect		    => rx_errdetect_opt,
		rx_ctrldetect		    => rx_ctrldetect_opt,
		rx_dataout			    => rx_dataout_opt,
		rx_clockout			    => rx_clkout_opt
	);
    
    --======================--
    -- Received packet validator --
    --======================--
    
    packet_validator_i: entity work.packet_validator 
	PORT MAP 
    (
		--local clock and reset
		clk			        => clk_main,
		nrst		        => rst_sys_clk_n,
        --input port
        in_data             => proto_data,
        in_startofpacket    => proto_startofpacket,
        in_endofpacket      => proto_endofpacket,
        in_valid            => proto_valid,
        in_ready            => proto_ready,
        --output port   
        out_data            => validator_data,
        out_startofpacket   => validator_startofpacket,
        out_endofpacket     => validator_endofpacket,
        out_valid           => validator_valid,
        out_ready           => validator_ready,
        --status port
        packet_length       => packet_length,
        packet_card_no      => packet_card_no,
        packet_type_no      => packet_type_no,
        packet_timestamp    => packet_timestamp,
        valid_packet        => valid_packet,
        err_drop_word       => err_drop_word,
        err_drop_bad_crc    => err_drop_bad_crc,
        err_drop_no_eop     => err_drop_no_eop,
        err_tmp_fifo_full   => err_tmp_fifo_full
    );
    
    --======================--
    -- Acquisition/Status packet MUX --
    --======================--
    
    acq_stat_packet_mux_i: entity work.acq_stat_packet_mux 
	PORT MAP 
    (
		--local clock and reset
		clk			            => clk_main,
		nrst		            => rst_sys_clk_n,
        --input port    
        in_data                 => validator_data,
        in_startofpacket        => validator_startofpacket,
        in_endofpacket          => validator_endofpacket,
        in_valid                => validator_valid,
        in_ready                => validator_ready,
        --acquisition output port
        out_acq_data            => GXprot_tx_data(GXprot_data_size-3 downto 0),
        out_acq_startofpacket   => GXprot_tx_data(GXprot_data_size-1),
        out_acq_endofpacket     => GXprot_tx_data(GXprot_data_size-2),
        out_acq_valid           => GXprot_tx_vld,
        out_acq_ready           => GXprot_tx_next,
        --status output port
        out_stat_data           => open,
        out_stat_startofpacket  => open,
        out_stat_endofpacket    => open,
        out_stat_valid          => open,
        out_stat_ready          => '1',
        --status port
        err_drop_word           => err_drop_word_mux
    );
    
    
    --================--
    -- PROC UNPACKAGE --
    --================--
    
    -- CMI --
    GXprot_CMI: entity work.CMI 
    generic map
    (
        data_size       => GXprot_data_size,
        rx_number       => GXprot_rx_number
    )
    port map 
    (
        clk         => clk_main,
        tx_data     => GXprot_tx_data,
        tx_vld      => GXprot_tx_vld,
        tx_next     => GXprot_tx_next,
        rx_data     => GXprot_rx_data,
        rx_vld      => GXprot_rx_vld,
        rx_next     => GXprot_rx_next
    );
    
    BLEPM_ProcessingUnpackage_CMA_inst: entity work.BLEPM_ProcessingUnpackage_CMA 
    port map 
    (
        clk         => clk_main,
        inp_data    => GXprot_rx_data(0),
        inp_vld     => GXprot_rx_vld(0),
        inp_next    => GXprot_rx_next(0),
        out_data    => PCinp_tx_data,
        out_vld     => PCinp_tx_vld,
        out_next    => PCinp_tx_next
    );
    
    -- quick fix to un-mirror the data channels
    PCinp_tx_data_ch_mirror <= PCinp_tx_data(19 downto 0) & PCinp_tx_data(39 downto 20) & PCinp_tx_data(59 downto 40) & PCinp_tx_data(79 downto 60) & PCinp_tx_data(99 downto 80) & PCinp_tx_data(119 downto 100) & PCinp_tx_data(139 downto 120) & PCinp_tx_data(159 downto 140);
    
    -- store received channels for the simulation
    out_reg_p: process(clk_main)
    begin
        if rising_edge(clk_main) then
            if PCinp_tx_vld = '1' then
                SIM_CH8_OUT <= PCinp_tx_data(19 downto 0);
                SIM_CH7_OUT <= PCinp_tx_data(39 downto 20);
                SIM_CH6_OUT <= PCinp_tx_data(59 downto 40);
                SIM_CH5_OUT <= PCinp_tx_data(79 downto 60);
                SIM_CH4_OUT <= PCinp_tx_data(99 downto 80);
                SIM_CH3_OUT <= PCinp_tx_data(119 downto 100);
                SIM_CH2_OUT <= PCinp_tx_data(139 downto 120);
                SIM_CH1_OUT <= PCinp_tx_data(159 downto 140);
            end if;
        end if;
    end process;
    SIM_CH_OUT_VALID <= PCinp_tx_vld;
    
    -- force reading
    --PCinp_tx_next <= '1';
    
    
    
    ----------------------------------------------------------------------------------------------------------------------------------------------
    
    -- CMI --
    PCinp_CMI: entity work.CMI 
    generic map
    (
        data_size       => PCinp_data_size,
        rx_number       => PCinp_rx_number
    )
    port map 
    (
        clk         => clk_main,
        --tx_data     => PCinp_tx_data,
        tx_data     => PCinp_tx_data_ch_mirror,
        tx_vld      => PCinp_tx_vld,
        tx_next     => PCinp_tx_next,
        rx_data     => PCinp_rx_data,
        rx_vld      => PCinp_rx_vld,
        rx_next     => PCinp_rx_next
    );
    
    --=====================--
    -- PROCESSING CORE CMA --
    --=====================--
    
    ProccesingCore_inst: entity work.BLEPM_ProcessingCore_CMA 
    generic map
    (
        inp_data_size       => PCinp_data_size,
        RS_number_per_ch    => RS_number_per_ch,
        channel_max         => channel_max,
        PM_active           => PM_active
    )
    port map 
    (
        clk                 => clk_main,
        -- Timing
        BP_pulse            => BP_pulse,
        BIn_pulse           => BIn_pulse,
        BOut_pulse          => BOut_pulse,
        EVOstart_pulse      => EVOstart_pulse,
        EVOstop_pulse       => EVOstop_pulse,
        EVO_pulse           => EVO_pulse,
        CAPstart_pulse      => CAPstart_pulse,
        CAPstop_pulse       => CAPstop_pulse,
        CAP_pulse           => CAP_pulse,
        BEAM_ACT            => BEAM_ACT,
        ACQ_CNT             => ACQ_CNT,
        BP_CNT              => BP_CNT,
        EVO_CNT             => EVO_CNT,
        CAP_CNT             => CAP_CNT,
        obs_sample          => observed_smpl,
        -- Params
        PARAM_LoC_TH        => PARAM_LoC_TH,
        PARAM_LoBP_TH       => PARAM_LoBP_TH,
        PARAM_RS_TH         => PARAM_RS_TH,
        -- CMI
        inp_data            => PCinp_rx_data(0),
        inp_vld             => PCinp_rx_vld(0),
        inp_next            => PCinp_rx_next(0),
        out_data            => PCout_tx_data,
        out_vld             => PCout_tx_vld,
        out_next            => PCout_tx_next,
        TC_results          => open
    );
    
    --======--
    -- FIFO --
    --======--
    
    -- CMI --
    PCout_CMI: entity work.CMI 
    generic map
    (
        data_size       => PCout_data_size,
        rx_number       => PCout_rx_number
    )
    port map 
    (
        clk         => clk_main,
        tx_data     => PCout_tx_data,
        tx_vld      => PCout_tx_vld,
        tx_next     => PCout_tx_next,
        rx_data     => PCout_rx_data,
        rx_vld      => PCout_rx_vld,
        rx_next     => PCout_rx_next
    );
    
    -- CMI --
    PCfifo_inst: entity work.fifo_CMA 
    generic map
    (
        data_size       => PCfifo_data_size,
        fifo_depth      => PCfifo_depth
    )
    port map 
    (
        clk         => clk_main,
        inp_data    => PCout_rx_data(0),
        inp_vld     => PCout_rx_vld(0),
        inp_next    => PCout_rx_next(0),
        out_data    => proc_tx_data,
        out_vld     => proc_tx_vld,
        out_next    => proc_tx_next
    );
       
    --==============--
    -- PROC SCMI TX --
    --==============--
    
    proc_SCMI: entity work.S_CMI_TX 
    generic map
    (
        data_size       => proc_data_size
    )
    port map 
    (
        tx_clk          => clk_main,
        tx_data         => proc_tx_data,
        tx_vld          => proc_tx_vld,
        tx_next         => proc_tx_next,
        scmi_tx_data    => proc_scmi_tx_data,
        scmi_tx_tag     => proc_scmi_tx_tag,
        scmi_rx_tag     => proc_scmi_rx_tag
    );
    
    
    -- force reading
    proc_scmi_rx_tag <= '1';
    
    
    --===================--
    -- TIMING CONTROLLER --
    --===================--
    
    BLEPM_TimingControl_inst: entity work.BLEPM_TimingControl
	GENERIC MAP
    (
        cnt_size            => tim_cnt_size
	)
	PORT MAP
    (
        clk                 => clk_main,
        BP_pulse_sync       => BP_TIM,
        BIn_pulse_sync      => BEAM_IN_TIM,
        BOut_pulse_sync     => BEAM_OUT_TIM,
        observed_smpl       => observed_smpl,
        BP_pulse            => BP_pulse,
        BIn_pulse           => BIn_pulse,
        BOut_pulse          => BOut_pulse,
        EVOstart_pulse      => EVOstart_pulse,
        EVOstop_pulse       => EVOstop_pulse,
        EVO_pulse           => EVO_pulse,
        CAPstart_pulse      => CAPstart_pulse,
        CAPstop_pulse       => CAPstop_pulse,
        CAP_pulse           => CAP_pulse,
        BEAM_ACT            => BEAM_ACT,
        BP_CNT              => BP_CNT,
        ACQ_CNT             => ACQ_CNT,
        EVO_CNT             => EVO_CNT,
        CAP_CNT             => CAP_CNT,
        PARAM_CAP_START     => PARAM_CAP_START,
        PARAM_CAP_STOP      => PARAM_CAP_STOP,
        PARAM_CAP_SMPLRATE  => PARAM_CAP_SMPLRATE,
        PARAM_EVO_SMPLRATE  => PARAM_EVO_SMPLRATE,
        PARAM_MS_SMPLRATE  => PARAM_MS_SMPLRATE,
        PARAM_MACH_TYPE     => PARAM_MACH_TYPE
        
	);
    
    --==================--
    -- PARAMETER TABLES --
    --==================--
    
    BLEPM_TIMING_TABLE_inst: entity work.BLEPM_TIMING_TABLE
	PORT MAP
    (
        clk                 => clk_main,
        inp_data            => (others => '0'),
        inp_vld             => '0',
        inp_next            => open,
        PARAM_CAP_START     => PARAM_CAP_START,
        PARAM_CAP_STOP      => PARAM_CAP_STOP,
        PARAM_CAP_SMPLRATE  => PARAM_CAP_SMPLRATE,
        PARAM_EVO_SMPLRATE  => PARAM_EVO_SMPLRATE,
        PARAM_MS_SMPLRATE   => PARAM_MS_SMPLRATE,
        PARAM_MACH_TYPE     => PARAM_MACH_TYPE
	);
    
    
    BLEPM_PROC_TH_TABLE_inst: entity work.BLEPM_PROC_TH_TABLE
	PORT MAP
    (
        clk                 => clk_main,
        inp_data            => (others => '0'),
        inp_vld             => '0',
        inp_next            => open,
        PARAM_LoC_TH        => PARAM_LoC_TH,
        PARAM_LoBP_TH       => PARAM_LoBP_TH,
        PARAM_RS_TH         => PARAM_RS_TH
	);

end architecture rtl;
