#all the libraries necesary for the altgx simulation can be compiled just once
#it makes the compilation much faster
#uncomment if compiling for the first time

#create lpm library and map it to work
#compile the library
#vlib lpm
#vmap work lpm
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/220pack.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/220model.vhd 

#create sgate library and map it to work
#compile the library
#vlib sgate
#vmap work sgate
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/sgate_pack.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/sgate.vhd 

#create altera_mf library and map it to work
#compile the library
#vlib altera_mf
#vmap work altera_mf
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf_components.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/altera_mf.vhd 

#create cycloneiv_hssi library and map it to work
#compile the library
#vlib cycloneiv_hssi
#vmap work cycloneiv_hssi
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/cycloneiv_hssi_components.vhd 
#vcom C:/EDA/Altera/v11_1sp2/quartus/eda/sim_lib/cycloneiv_hssi_atoms.vhd 

#create lib library and map it to work
#compile the library
vlib lib
vmap work lib

vcom -novopt -O0 "../../../cores/altgx_ch0.vhd"
vcom -novopt -O0 "../../../cores/gx_reconfig.vhd"
vcom -novopt -O0 "../../../cores/REFCLK_PLL.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/packages/vhdl_func_pkg.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/packages/BLMINJ_pkg.vhd"

# Uncomment when compiling for the first time!!!!
# It is commented out to make the compilation much faster
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/CMI/Interconnect/CMI.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/CMI/Interconnect/T_CMI.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/CMI/Interconnect/MM_CMI.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/CMI/Interconnect/DM_CMI.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/CMI/Interconnect/S_CMI_TX.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/packaging/package_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/packaging/appCRC32_DATA16_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/slice_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/compare_pass_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/max_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/const_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/delay_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/window_accu_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/STD/general/sync_pulse.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/param_table_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/packaging/PackageCreator16_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/fifo_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/BLMINJ/BLEDP_AcquisitionPackaging_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/combine_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/packaging/unpackage_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/packaging/remCRC32_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/packaging/checkCRC32_DATA16_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/packaging/PackageDismantle_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/packaging/appCRC32_DATA64_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/BLMINJ/BLEPM_ProcessingUnpackage_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/combine_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/smpl_detect_CMA.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/STD/general/basic_cnt.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/STD/general/pulser.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/STD/general/sync.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/CMI/DM/data_splitter_DM.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/CMI/DM/accu_smpl_filter_DM.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/CMI/ADM/ID_ADM.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/CMI/ARB/RR_wait_mux_ARB.vhd"
#vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/CMI/ARB/priority_pkg_mux_ARB.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/general/accu_CMA.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/BLMINJ/AccuLoss_CMA.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/BLMINJ/AccuLoss_noTC_CMA.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/BLMINJ/PostMortem_CMA.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/BLMINJ/RunningSum_CMA.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/BLMINJ/BLEPM_ProcessingChannel_CMA.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/BLMINJ/BLEPM_ProcessingCore_CMA.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/STD/BLMINJ/BLEPM_TimingControl.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/BLMINJ/BLEPM_TIMING_TABLE.vhd"
vcom -novopt -O0 -2008 "../../../../../../../library/hdl/modules/CMA/v2/MOD/BLMINJ/BLEPM_PROC_TH_TABLE.vhd"


vcom -novopt -O0 "../../../../../../../library/hdl/modules/streaming/protocol_fifo.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/streaming/CRC32_D16.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/streaming/acq_packet_creator.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/streaming/packet_validator.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/streaming/acq_stat_packet_mux.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/streaming/altgx_rst_control.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/streaming/protocol_tx.vhd"
vcom -novopt -O0 "../../../../../../../library/hdl/modules/streaming/protocol_rx.vhd"
vcom -novopt -O0 -2008 "../src/blepm_top.vhd"
vcom -novopt -O0 "../src/bledp_top.vhd"
vcom -novopt -O0 "../src/TB_optical_link.vhd"



vsim -novopt -msgmode both work.TB_optical_link -t 1ps

#add wave *

########################################################
# Transmitter signals
########################################################

# Clock
add wave sim:/TB_optical_link/DUT_TX_BLEDP/sys_clk
# serial stream
add wave sim:/TB_optical_link/DUT_RX_BLEPM/GXP_RX1
# optic fibre break signal
add wave sim:/TB_optical_link/break_tx

# Gigabit transmitter input signals
#add wave sim:/TB_optical_link/DUT_TX_BLEDP/altgx_opt_i/tx_ctrlenable
#add wave -radix hexadecimal sim:/TB_optical_link/DUT_TX_BLEDP/altgx_opt_i/tx_datain

#BLEDP packet creator signals
#add wave -radix unsigned sim:/TB_optical_link/DUT_TX_BLEDP/acq_path_i/pkt_dwords_cnt
#add wave -radix hexadecimal sim:/TB_optical_link/DUT_TX_BLEDP/acq_path_i/gen_data
#add wave sim:/TB_optical_link/DUT_TX_BLEDP/acq_path_i/gen_startofpacket
#add wave sim:/TB_optical_link/DUT_TX_BLEDP/acq_path_i/gen_endofpacket
#add wave sim:/TB_optical_link/DUT_TX_BLEDP/acq_path_i/gen_valid

#protocol_tx signals
#add wave sim:/TB_optical_link/DUT_TX_BLEDP/protocol_tx_i/fifo_din
#add wave sim:/TB_optical_link/DUT_TX_BLEDP/protocol_tx_i/fifo_rdrequest
#add wave sim:/TB_optical_link/DUT_TX_BLEDP/protocol_tx_i/fifo_output
#add wave sim:/TB_optical_link/DUT_TX_BLEDP/protocol_tx_i/state
#add wave -radix unsigned sim:/TB_optical_link/DUT_TX_BLEDP/acq_path_i/pkt_dwords_cnt
#add wave sim:/TB_optical_link/DUT_TX_BLEDP/acq_path_i/pkt_dwords_done
#add wave sim:/TB_optical_link/DUT_TX_BLEDP/acq_path_i/crc_calc_rst
#add wave -radix hexadecimal sim:/TB_optical_link/DUT_TX_BLEDP/acq_path_i/crc_reg

########################################################
# Receiver signals
########################################################


# Output signals of gigabit receiver
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/GX_chan0/rx_syncstatus
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/GX_chan0/rx_enabyteord
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/GX_chan0/rx_byteorderalignstatus

#add wave sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/rx_digitalreset
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/rx_errdetect
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/rx_ctrldetect
#add wave -radix hexadecimal sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/rx_dataout
#add wave -radix hexadecimal sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/fifo_wrrequest
#add wave -radix hexadecimal sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/state


# Output signals of protocol_rx
#add wave -radix hexadecimal sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/proto_data
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/proto_startofpacket
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/proto_endofpacket
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/proto_valid
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/protocol_rx_i/proto_ready

# signals of the packet validator
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/state
#add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/dword_cnt
#add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/hdr_cnt
#add wave -radix hexadecimal sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/crc_reg
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/tmp_fifo_wrreq
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/tmp_fifo_rdreq
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/tmp_fifo_rdempty
#add wave -radix hexadecimal sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/tmp_fifo_output
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/crc_ok
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/crc_reset
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/crc_load
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/out_startofpacket
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/out_endofpacket
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/out_data
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/out_valid
add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/packet_timestamp
add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/packet_length
add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/valid_packet
add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/err_drop_word
add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/err_drop_bad_crc
add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/err_drop_no_eop
add wave sim:/TB_optical_link/DUT_RX_BLEPM/packet_validator_i/err_tmp_fifo_full

# signals of the acq_stat_packet_mux
add wave sim:/TB_optical_link/DUT_RX_BLEPM/acq_stat_packet_mux_i/state

# Output signals of ProcessingUnpackage_CMA
#add wave -radix hexadecimal sim:/TB_optical_link/DUT_RX_BLEPM/PCinp_tx_data_ch_mirror
#add wave sim:/TB_optical_link/DUT_RX_BLEPM/PCinp_tx_vld

########################################################
# Acquisition channel inputs (bledp) and outputs (blepm)
########################################################
add wave -radix unsigned sim:/TB_optical_link/DUT_TX_BLEDP/SIM_CH1_IN
add wave -radix unsigned sim:/TB_optical_link/DUT_TX_BLEDP/SIM_CH2_IN
add wave -radix unsigned sim:/TB_optical_link/DUT_TX_BLEDP/SIM_CH3_IN
add wave -radix unsigned sim:/TB_optical_link/DUT_TX_BLEDP/SIM_CH4_IN
add wave -radix unsigned sim:/TB_optical_link/DUT_TX_BLEDP/SIM_CH5_IN
add wave -radix unsigned sim:/TB_optical_link/DUT_TX_BLEDP/SIM_CH6_IN
add wave -radix unsigned sim:/TB_optical_link/DUT_TX_BLEDP/SIM_CH7_IN
add wave -radix unsigned sim:/TB_optical_link/DUT_TX_BLEDP/SIM_CH8_IN

add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/SIM_CH1_OUT
add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/SIM_CH2_OUT
add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/SIM_CH3_OUT
add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/SIM_CH4_OUT
add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/SIM_CH5_OUT
add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/SIM_CH6_OUT
add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/SIM_CH7_OUT
add wave -radix unsigned sim:/TB_optical_link/DUT_RX_BLEPM/SIM_CH8_OUT
add wave sim:/TB_optical_link/DUT_RX_BLEPM/SIM_CH_OUT_VALID


########################################################
# Processing and output signals
########################################################

add wave -radix hexadecimal sim:/TB_optical_link/DUT_RX_BLEPM/proc_scmi_tx_data
add wave sim:/TB_optical_link/DUT_RX_BLEPM/proc_scmi_tx_tag

# Clock
add wave sim:/TB_optical_link/DUT_RX_BLEPM/clk_main

add wave sim:/TB_optical_link/DUT_RX_BLEPM/observed_smpl
add wave sim:/TB_optical_link/DUT_RX_BLEPM/BLEPM_TimingControl_inst/BP_pulse_sync
add wave sim:/TB_optical_link/DUT_RX_BLEPM/BLEPM_TimingControl_inst/BP_pulse_sync_to_OS


add wave sim:/TB_optical_link/ctrv_basic_period
add wave sim:/TB_optical_link/ctrv_beam_in
add wave sim:/TB_optical_link/ctrv_beam_out


run -all
wave zoomfull
