------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    03/06/2014
--Module Name:    Low_voltage
--Project Name:   Low_voltage
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity Low_voltage is
generic(
	sys_clk_freq:		natural := 40000000
);
port(
	-- Input Clock
	sys_clk:			in std_logic;
	rst_sys_clk_n:		in std_logic;
	
	-- VME registers signals
	vme3v3_value_rd:		out std_logic_vector(31 downto 0);
	vme5v_value_rd:			out std_logic_vector(31 downto 0);
	analog_p15v_value_rd:	out std_logic_vector(31 downto 0);
	analog_n15v_value_rd:	out std_logic_vector(31 downto 0);
	analog_5v_value_rd:		out std_logic_vector(31 downto 0);
	ref_5v_value_rd:		out std_logic_vector(31 downto 0);
	ref_10va_value_rd:		out std_logic_vector(31 downto 0);
	ref_10vb_value_rd:		out std_logic_vector(31 downto 0);
	
	vme3v3_min_rd:			out std_logic_vector(31 downto 0);
	vme3v3_max_rd:			out std_logic_vector(31 downto 0);
	vme5v_min_rd:			out std_logic_vector(31 downto 0);
	vme5v_max_rd:			out std_logic_vector(31 downto 0);
	analog_p15v_min_rd:		out std_logic_vector(31 downto 0);
	analog_p15v_max_rd:		out std_logic_vector(31 downto 0);
	analog_n15v_min_rd:		out std_logic_vector(31 downto 0);
	analog_n15v_max_rd:		out std_logic_vector(31 downto 0);
	analog_5v_min_rd:		out std_logic_vector(31 downto 0);
	analog_5v_max_rd:		out std_logic_vector(31 downto 0);
	ref_5v_min_rd:			out std_logic_vector(31 downto 0);
	ref_5v_max_rd:			out std_logic_vector(31 downto 0);
	ref_10va_min_rd:		out std_logic_vector(31 downto 0);
	ref_10va_max_rd:		out std_logic_vector(31 downto 0);
	ref_10vb_min_rd:		out std_logic_vector(31 downto 0);
	ref_10vb_max_rd:		out std_logic_vector(31 downto 0);
	
	lv_comparators_rd:		out std_logic_vector(31 downto 0);
	analog5v_cnt_rd:		out std_logic_vector(31 downto 0);
	analog15v_cnt_rd:		out std_logic_vector(31 downto 0);
	vme3v3_cnt_rd:			out std_logic_vector(31 downto 0);
	vme5v_cnt_rd:			out std_logic_vector(31 downto 0);
	vme12v_cnt_rd:			out std_logic_vector(31 downto 0);
	
	-- Start of basic period pulse
	start_of_bp:			in std_logic;
	
	-- Low voltage comparators
	PS_3V3_ON:	in std_logic;
	PS_12V_ON:	in std_logic;
	PS_5V_ON:	in std_logic;
	PS_15V_ON:	in std_logic;
	PS_5VA_ON:	in std_logic;
	
	-- AD7327 SPI signals
	sclk:		out std_logic;
	dout:		out std_logic;
	din:		in std_logic;
	cs_n:		out std_logic

);
	
end entity Low_voltage;


architecture rtl of Low_voltage is

attribute keep: boolean;

signal vme3v3_min_reg:		unsigned(12 downto 0);
signal vme3v3_max_reg:		unsigned(12 downto 0);
signal vme5v_min_reg:		unsigned(12 downto 0);
signal vme5v_max_reg:		unsigned(12 downto 0);
signal analog_p15v_min_reg:	unsigned(12 downto 0);
signal analog_p15v_max_reg:	unsigned(12 downto 0);
signal analog_n15v_min_reg:	unsigned(12 downto 0);
signal analog_n15v_max_reg:	unsigned(12 downto 0);
signal analog_5v_min_reg:	unsigned(12 downto 0);
signal analog_5v_max_reg:	unsigned(12 downto 0);
signal ref_5v_min_reg:		unsigned(12 downto 0);
signal ref_5v_max_reg:		unsigned(12 downto 0);
signal ref_10va_min_reg:	unsigned(12 downto 0);
signal ref_10va_max_reg:	unsigned(12 downto 0);
signal ref_10vb_min_reg:	unsigned(12 downto 0);
signal ref_10vb_max_reg:	unsigned(12 downto 0);


signal data_0: std_logic_vector(12 downto 0);
signal data_1: std_logic_vector(12 downto 0);
signal data_2: std_logic_vector(12 downto 0);
signal data_3: std_logic_vector(12 downto 0);
signal data_4: std_logic_vector(12 downto 0);
signal data_5: std_logic_vector(12 downto 0);
signal data_6: std_logic_vector(12 downto 0);
signal data_7: std_logic_vector(12 downto 0);

signal data_rdy: std_logic;
	

BEGIN

	------------------------------------------------------
	-- AD7327 instance
	------------------------------------------------------
	AD7327_i: entity work.AD7327
    generic map
    (
        sys_clk_freq	=> sys_clk_freq,
		spi_clk_freq	=> 1000000
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- ADC control signals
		cnv_start	=> '1',
		data_0		=> data_0,
		data_1		=> data_1,
		data_2		=> data_2,
		data_3		=> data_3,
		data_4		=> data_4,
		data_5		=> data_5,
		data_6		=> data_6,
		data_7		=> data_7,
		data_rdy	=> data_rdy,
		busy		=> open,
		
		--SPI signals
		sclk		=> sclk,
		dout		=> dout,
		din			=> din,
		cs_n		=> cs_n
	);
	
	------------------------------------------------------
	-- register voltage readouts every basic period
	------------------------------------------------------
	
	lvi_regs_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if start_of_bp = '1' then
				vme3v3_value_rd			<= x"0000" & "000" & data_6;
			    vme5v_value_rd			<= x"0000" & "000" & data_7;
			    analog_p15v_value_rd	<= x"0000" & "000" & data_0;
			    analog_n15v_value_rd	<= x"0000" & "000" & data_1;
			    analog_5v_value_rd		<= x"0000" & "000" & data_5;
			    ref_5v_value_rd			<= x"0000" & "000" & data_4;
			    ref_10va_value_rd		<= x"0000" & "000" & data_2;
			    ref_10vb_value_rd		<= x"0000" & "000" & data_3;
				lv_comparators_rd <= x"000000" & "000" & not PS_5VA_ON & not PS_15V_ON & not PS_3V3_ON & not PS_5V_ON & not PS_12V_ON;
			end if;
		end if;
	end process;
	
	
	------------------------------------------------------
	-- track min and max readouts in a basic period
	------------------------------------------------------
	
	vme3v3_min_max_i: entity work.unsigned_min_max
    generic map
    (
		in_bits	=> 13
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- input value to track
		in_value	=> unsigned(data_6),
		in_value_en	=> data_rdy,
		
		-- period pulse in which min/max is tracked
		period_en	=> start_of_bp,
		
		-- min/max found in the period
		max_value	=> vme3v3_max_reg,
		min_value	=> vme3v3_min_reg
	);
	vme3v3_max_rd <= x"0000" & "000" & std_logic_vector(vme3v3_max_reg);
	vme3v3_min_rd <= x"0000" & "000" & std_logic_vector(vme3v3_min_reg);
	
	vme5v_min_max_i: entity work.unsigned_min_max
    generic map
    (
		in_bits	=> 13
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- input value to track
		in_value	=> unsigned(data_7),
		in_value_en	=> data_rdy,
		
		-- period pulse in which min/max is tracked
		period_en	=> start_of_bp,
		
		-- min/max found in the period
		max_value	=> vme5v_max_reg,
		min_value	=> vme5v_min_reg
	);
	vme5v_max_rd <= x"0000" & "000" & std_logic_vector(vme5v_max_reg);
	vme5v_min_rd <= x"0000" & "000" & std_logic_vector(vme5v_min_reg);
	
	analog_p15v_min_max_i: entity work.unsigned_min_max
    generic map
    (
		in_bits	=> 13
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- input value to track
		in_value	=> unsigned(data_0),
		in_value_en	=> data_rdy,
		
		-- period pulse in which min/max is tracked
		period_en	=> start_of_bp,
		
		-- min/max found in the period
		max_value	=> analog_p15v_max_reg,
		min_value	=> analog_p15v_min_reg
	);
	analog_p15v_max_rd <= x"0000" & "000" & std_logic_vector(analog_p15v_max_reg);
	analog_p15v_min_rd <= x"0000" & "000" & std_logic_vector(analog_p15v_min_reg);
	
	analog_n15v_min_max_i: entity work.unsigned_min_max
    generic map
    (
		in_bits	=> 13
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- input value to track
		in_value	=> unsigned(data_1),
		in_value_en	=> data_rdy,
		
		-- period pulse in which min/max is tracked
		period_en	=> start_of_bp,
		
		-- min/max found in the period
		max_value	=> analog_n15v_max_reg,
		min_value	=> analog_n15v_min_reg
	);
	analog_n15v_max_rd <= x"0000" & "000" & std_logic_vector(analog_n15v_max_reg);
	analog_n15v_min_rd <= x"0000" & "000" & std_logic_vector(analog_n15v_min_reg);
	
	analog_5v_min_max_i: entity work.unsigned_min_max
    generic map
    (
		in_bits	=> 13
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- input value to track
		in_value	=> unsigned(data_5),
		in_value_en	=> data_rdy,
		
		-- period pulse in which min/max is tracked
		period_en	=> start_of_bp,
		
		-- min/max found in the period
		max_value	=> analog_5v_max_reg,
		min_value	=> analog_5v_min_reg
	);
	analog_5v_max_rd <= x"0000" & "000" & std_logic_vector(analog_5v_max_reg);
	analog_5v_min_rd <= x"0000" & "000" & std_logic_vector(analog_5v_min_reg);
	
	ref_5v_min_max_i: entity work.unsigned_min_max
    generic map
    (
		in_bits	=> 13
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- input value to track
		in_value	=> unsigned(data_4),
		in_value_en	=> data_rdy,
		
		-- period pulse in which min/max is tracked
		period_en	=> start_of_bp,
		
		-- min/max found in the period
		max_value	=> ref_5v_max_reg,
		min_value	=> ref_5v_min_reg
	);
	ref_5v_max_rd <= x"0000" & "000" & std_logic_vector(ref_5v_max_reg);
	ref_5v_min_rd <= x"0000" & "000" & std_logic_vector(ref_5v_min_reg);
	
	ref_10va_min_max_i: entity work.unsigned_min_max
    generic map
    (
		in_bits	=> 13
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- input value to track
		in_value	=> unsigned(data_2),
		in_value_en	=> data_rdy,
		
		-- period pulse in which min/max is tracked
		period_en	=> start_of_bp,
		
		-- min/max found in the period
		max_value	=> ref_10va_max_reg,
		min_value	=> ref_10va_min_reg
	);
	ref_10va_max_rd <= x"0000" & "000" & std_logic_vector(ref_10va_max_reg);
	ref_10va_min_rd <= x"0000" & "000" & std_logic_vector(ref_10va_min_reg);
	
	ref_10vb_min_max_i: entity work.unsigned_min_max
    generic map
    (
		in_bits	=> 13
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- input value to track
		in_value	=> unsigned(data_3),
		in_value_en	=> data_rdy,
		
		-- period pulse in which min/max is tracked
		period_en	=> start_of_bp,
		
		-- min/max found in the period
		max_value	=> ref_10vb_max_reg,
		min_value	=> ref_10vb_min_reg
	);
	ref_10vb_max_rd <= x"0000" & "000" & std_logic_vector(ref_10vb_max_reg);
	ref_10vb_min_rd <= x"0000" & "000" & std_logic_vector(ref_10vb_min_reg);
    
    
    ------------------------------------------------------
	-- count for how many clock cycles the voltages were NOT OK
	------------------------------------------------------
    
	analog5v_not_ok_i: entity work.events_counter
    generic map
    (
		cnt_bits	=> 32,
		rising	    => false,
		falling	    => false,
		high_l	    => false,
		low_l	    => true
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- event which is counted
		in_event	    => PS_5VA_ON,
		
		-- period pulse in which events are counted
        period_en	    => start_of_bp,
		
		-- events detected in the period
        events_number	=> analog5v_cnt_rd,
		events_ov	    => open
	);
    
    
    analog15v_not_ok_i: entity work.events_counter
    generic map
    (
		cnt_bits	=> 32,
		rising	    => false,
		falling	    => false,
		high_l	    => false,
		low_l	    => true
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- event which is counted
		in_event	    => PS_15V_ON,
		
		-- period pulse in which events are counted
        period_en	    => start_of_bp,
		
		-- events detected in the period
        events_number	=> analog15v_cnt_rd,
		events_ov	    => open
	);
    
    
	vme3v3_not_ok_i: entity work.events_counter
    generic map
    (
		cnt_bits	=> 32,
		rising	    => false,
		falling	    => false,
		high_l	    => false,
		low_l	    => true
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- event which is counted
		in_event	    => PS_3V3_ON,
		
		-- period pulse in which events are counted
        period_en	    => start_of_bp,
		
		-- events detected in the period
        events_number	=> vme3v3_cnt_rd,
		events_ov	    => open
	);
    
    
    vme5v_not_ok_i: entity work.events_counter
    generic map
    (
		cnt_bits	=> 32,
		rising	    => false,
		falling	    => false,
		high_l	    => false,
		low_l	    => true
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- event which is counted
		in_event	    => PS_5V_ON,
		
		-- period pulse in which events are counted
        period_en	    => start_of_bp,
		
		-- events detected in the period
        events_number	=> vme5v_cnt_rd,
		events_ov	    => open
	);
    
    vme12v_not_ok_i: entity work.events_counter
    generic map
    (
		cnt_bits	=> 32,
		rising	    => false,
		falling	    => false,
		high_l	    => false,
		low_l	    => true
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- event which is counted
		in_event	    => PS_12V_ON,
		
		-- period pulse in which events are counted
        period_en	    => start_of_bp,
		
		-- events detected in the period
        events_number	=> vme12v_cnt_rd,
		events_ov	    => open
	);

	
end architecture rtl;
