------------------------------------------------------------------
--Design Unit : 	Test bench unit for the Interlock_interface component
--					
--
--
--Author: 			Maciej Kwiatkowski
--		  	 		European Organisation for Nuclear Research
--		 	 		BE-BI-BL
--		 	 		CERN, Geneva, Switzerland,  CH-1211
--		 	 		865/R-A01
--
--Simulator:		Modelsim
------------------------------------------------------------------
--Vsn 	Author	Date			Changes
--
--0.1	MK		03.04.2014		First version
------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use ieee.math_real.all;
use STD.textio.all;
use IEEE.STD_LOGIC_TEXTIO.all;

entity TB_Interlock_interface is
end TB_Interlock_interface;

architecture behaviour of TB_Interlock_interface is

constant sys_clk_freq : natural := 40000000;
constant sys_clk_ns_period : natural := 1000000000/sys_clk_freq;


signal done_sim     : STD_LOGIC;

signal clk			: STD_LOGIC;
signal n_reset		: STD_LOGIC;

signal comm_hw_ilock1_wr : std_logic_vector(31 downto 0);
signal comm_hw_ilock2_wr : std_logic_vector(31 downto 0);
signal comm_sw_ilock1_wr : std_logic_vector(31 downto 0);
signal comm_sw_ilock2_wr : std_logic_vector(31 downto 0);
signal comm_set_user_wr : std_logic_vector(31 downto 0);
signal comm_set_wdg_time_wr : std_logic_vector(31 downto 0);
signal comm_reset_wdg_wr : std_logic_vector(31 downto 0);

signal comm_hw_ilock1_wr_en : std_logic;
signal comm_hw_ilock2_wr_en : std_logic;
signal comm_sw_ilock1_wr_en : std_logic;
signal comm_sw_ilock2_wr_en : std_logic;
signal comm_set_user_wr_en : std_logic;
signal comm_set_wdg_time_wr_en : std_logic;
signal comm_reset_wdg_wr_en : std_logic;
signal beam_allowed_1 : std_logic;
signal beam_allowed_2 : std_logic;

signal cibu1_status_rd : std_logic_vector(31 downto 0);
signal cibu2_status_rd : std_logic_vector(31 downto 0);
signal hw_ilock1_rd : std_logic_vector(31 downto 0);
signal hw_ilock2_rd : std_logic_vector(31 downto 0);
signal sw_ilock1_rd : std_logic_vector(31 downto 0);
signal sw_ilock2_rd : std_logic_vector(31 downto 0);
signal current_user_rd : std_logic_vector(31 downto 0);
signal wdg_timer_rd : std_logic_vector(31 downto 0);
signal wdg_time_rd : std_logic_vector(31 downto 0);
signal CIBU1_A : std_logic;
signal CIBU1_B : std_logic;
signal CIBU2_A : std_logic;
signal CIBU2_B : std_logic;

procedure clear_sw_interlock ( constant user : in natural;
							signal clk : in std_logic;
							signal data_wr : out std_logic_vector(31 downto 0);
							signal data_wr_en : out std_logic
							) is
begin
		
	data_wr_en <= '0';
	data_wr <= (others=>'0');
		
	wait until falling_edge(clk);
	
	data_wr <= x"5AA55A" & std_logic_vector(to_unsigned(user, 8));
	data_wr_en <= '1';
	
	wait until falling_edge(clk);
	
	data_wr_en <= '0';
	data_wr <= (others=>'0');

end procedure clear_sw_interlock ;


procedure set_sw_interlock ( constant user : in natural;
							signal clk : in std_logic;
							signal data_wr : out std_logic_vector(31 downto 0);
							signal data_wr_en : out std_logic
							) is
begin
		
	data_wr_en <= '0';
	data_wr <= (others=>'0');
		
	wait until falling_edge(clk);
	
	data_wr <= x"A55AA5" & std_logic_vector(to_unsigned(user, 8));
	data_wr_en <= '1';
	
	wait until falling_edge(clk);
	
	data_wr_en <= '0';
	data_wr <= (others=>'0');

end procedure set_sw_interlock ;


procedure clear_hw_interlock ( signal clk : in std_logic;
							signal data_wr : out std_logic_vector(31 downto 0);
							signal data_wr_en : out std_logic
							) is
begin
	
	clear_sw_interlock(165, clk, data_wr, data_wr_en);

end procedure clear_hw_interlock ;


procedure set_user ( constant user : in natural;
							signal clk : in std_logic;
							signal data_wr : out std_logic_vector(31 downto 0);
							signal data_wr_en : out std_logic
							) is
begin
		
	set_sw_interlock(user, clk, data_wr, data_wr_en);

end procedure set_user ;

procedure set_wdg_time ( constant wdg_time_ms : in natural;
							signal clk : in std_logic;
							signal data_wr : out std_logic_vector(31 downto 0);
							signal data_wr_en : out std_logic
							) is
begin
		
	data_wr_en <= '0';
	data_wr <= (others=>'0');
		
	wait until falling_edge(clk);
	
	data_wr <= x"A5" & std_logic_vector(to_unsigned(wdg_time_ms, 24));
	data_wr_en <= '1';
	
	wait until falling_edge(clk);
	
	data_wr_en <= '0';
	data_wr <= (others=>'0');

end procedure set_wdg_time ;


procedure reset_wdg_timer ( signal clk : in std_logic;
							signal data_wr : out std_logic_vector(31 downto 0);
							signal data_wr_en : out std_logic
							) is
begin
		
	clear_hw_interlock(clk, data_wr, data_wr_en);

end procedure reset_wdg_timer ;

--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------	Architecture begin  ------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
begin

	DUT_i: entity work.Interlock_interface
    generic map
    (
        sys_clk_freq => sys_clk_freq,
		mono_clk_freq => 10000000,
		wdg_time_ms_default => 2400
    )
    port map
    (   
		sys_clk				=> clk,
		rst_sys_clk_n		=> n_reset,
        
		cibu1_status_rd			=> cibu1_status_rd,
		cibu2_status_rd			=> cibu2_status_rd,
		hw_ilock1_rd			=> hw_ilock1_rd,
		hw_ilock2_rd			=> hw_ilock2_rd,
		sw_ilock1_rd			=> sw_ilock1_rd,
		sw_ilock2_rd			=> sw_ilock2_rd,
		current_user_rd			=> current_user_rd,
		wdg_timer_rd			=> wdg_timer_rd,
		wdg_time_rd				=> wdg_time_rd,
		comm_hw_ilock1_wr		=> comm_hw_ilock1_wr,
		comm_hw_ilock1_wr_en	=> comm_hw_ilock1_wr_en,
		comm_hw_ilock2_wr		=> comm_hw_ilock2_wr,
		comm_hw_ilock2_wr_en	=> comm_hw_ilock2_wr_en,
		comm_sw_ilock1_wr		=> comm_sw_ilock1_wr,
		comm_sw_ilock1_wr_en	=> comm_sw_ilock1_wr_en,
		comm_sw_ilock2_wr		=> comm_sw_ilock2_wr,
		comm_sw_ilock2_wr_en	=> comm_sw_ilock2_wr_en,
		comm_set_user_wr		=> comm_set_user_wr,
		comm_set_user_wr_en		=> comm_set_user_wr_en,
		comm_set_wdg_time_wr	=> comm_set_wdg_time_wr,
		comm_set_wdg_time_wr_en	=> comm_set_wdg_time_wr_en,
		comm_reset_wdg_wr		=> comm_reset_wdg_wr,
		comm_reset_wdg_wr_en	=> comm_reset_wdg_wr_en,
		
		-- Interlock Inputs (DC signal from BLEPT - falling edge = interlock)
		beam_allowed_1 => beam_allowed_1,
		beam_allowed_2 => beam_allowed_2,
		
		-- Interlock Outputs (clk > 1MHz to a monostable circuit driving CIBUs)
		CIBU1_A => CIBU1_A,
		CIBU1_B => CIBU1_B,
		CIBU2_A => CIBU2_A,
		CIBU2_B => CIBU2_B
    );


process

begin
loop
	clk<='1' ;
	wait for  12 ns;
    clk<='0';
	wait for 13 ns;
	if done_sim = '1' then
		wait;
	end if;
end loop;
end process;


process
begin

	done_sim <= '0';
	
	n_reset <= '0';	
	
	
	comm_hw_ilock1_wr <= (others=>'0');
	comm_hw_ilock2_wr <= (others=>'0');
	comm_sw_ilock1_wr <= (others=>'0');
	comm_sw_ilock2_wr <= (others=>'0');
	comm_set_user_wr <= (others=>'0');
	comm_set_wdg_time_wr <= (others=>'0');
	comm_reset_wdg_wr <= (others=>'0');
	
	comm_hw_ilock1_wr_en <= '0';
	comm_hw_ilock2_wr_en <= '0';
	comm_sw_ilock1_wr_en <= '0';
	comm_sw_ilock2_wr_en <= '0';
	comm_set_user_wr_en <= '0';
	comm_set_wdg_time_wr_en <= '0';
	comm_reset_wdg_wr_en <= '0';
	beam_allowed_1 <= '1';
	beam_allowed_2 <= '1';
	
	wait for 100 ns;
	n_reset <= '1';
	
	wait for 10000000 ns;
	
	set_user(16, clk, comm_set_user_wr, comm_set_user_wr_en);
	set_wdg_time(15, clk, comm_set_wdg_time_wr, comm_set_wdg_time_wr_en);
	clear_hw_interlock(clk, comm_hw_ilock1_wr, comm_hw_ilock1_wr_en);
	clear_hw_interlock(clk, comm_hw_ilock2_wr, comm_hw_ilock2_wr_en);
	
	for I in 0 to 31 loop
		clear_sw_interlock(I, clk, comm_sw_ilock1_wr, comm_sw_ilock1_wr_en);
	end loop;
	wait for 1000 ns;
	for I in 0 to 31 loop
		clear_sw_interlock(I, clk, comm_sw_ilock2_wr, comm_sw_ilock2_wr_en);
	end loop;
	
	wait for 10000000 ns;
	
	reset_wdg_timer(clk, comm_reset_wdg_wr, comm_reset_wdg_wr_en);
	
	wait for 14995513 ns;
	clear_sw_interlock(0, clk, comm_sw_ilock1_wr, comm_sw_ilock1_wr_en);
	clear_sw_interlock(1, clk, comm_sw_ilock2_wr, comm_sw_ilock2_wr_en);
	wait for 5000000 ns;
	
	set_wdg_time(2000, clk, comm_set_wdg_time_wr, comm_set_wdg_time_wr_en);
	for I in 0 to 31 loop
		clear_sw_interlock(I, clk, comm_sw_ilock1_wr, comm_sw_ilock1_wr_en);
	end loop;
	wait for 1000 ns;
	for I in 0 to 31 loop
		clear_sw_interlock(I, clk, comm_sw_ilock2_wr, comm_sw_ilock2_wr_en);
	end loop;
	
	wait for 1000000 ns;
	
	
	beam_allowed_1 <= '0';
	clear_hw_interlock(clk, comm_hw_ilock1_wr, comm_hw_ilock1_wr_en);
	clear_hw_interlock(clk, comm_hw_ilock2_wr, comm_hw_ilock2_wr_en);
	beam_allowed_1 <= '1';
	
	wait for 1000000 ns;
	
	beam_allowed_2 <= '0';
	clear_hw_interlock(clk, comm_hw_ilock1_wr, comm_hw_ilock1_wr_en);
	clear_hw_interlock(clk, comm_hw_ilock2_wr, comm_hw_ilock2_wr_en);
	beam_allowed_2 <= '1';
	
	wait for 1000000 ns;
	
	clear_hw_interlock(clk, comm_hw_ilock1_wr, comm_hw_ilock1_wr_en);
	clear_hw_interlock(clk, comm_hw_ilock2_wr, comm_hw_ilock2_wr_en);
	
	for I in 0 to 31 loop
		set_user(I, clk, comm_set_user_wr, comm_set_user_wr_en);
		for K in 0 to 31 loop
			set_sw_interlock(K, clk, comm_sw_ilock1_wr, comm_sw_ilock1_wr_en);
			wait for 1000 ns;
			clear_sw_interlock(K, clk, comm_sw_ilock1_wr, comm_sw_ilock1_wr_en);
		end loop;
	end loop;
	
	
	for I in 0 to 31 loop
		set_user(I, clk, comm_set_user_wr, comm_set_user_wr_en);
		for K in 0 to 31 loop
			set_sw_interlock(K, clk, comm_sw_ilock2_wr, comm_sw_ilock2_wr_en);
			wait for 1000 ns;
			clear_sw_interlock(K, clk, comm_sw_ilock2_wr, comm_sw_ilock2_wr_en);
		end loop;
	end loop;
	
	
	
	set_wdg_time(15, clk, comm_set_wdg_time_wr, comm_set_wdg_time_wr_en);
	clear_hw_interlock(clk, comm_hw_ilock1_wr, comm_hw_ilock1_wr_en);
	clear_hw_interlock(clk, comm_hw_ilock2_wr, comm_hw_ilock2_wr_en);
	clear_sw_interlock(16#A5#, clk, comm_sw_ilock1_wr, comm_sw_ilock1_wr_en);
	clear_sw_interlock(16#A5#, clk, comm_sw_ilock2_wr, comm_sw_ilock2_wr_en);
	
	wait for 16000000 ns;
	
	set_wdg_time(16#ffffff#, clk, comm_set_wdg_time_wr, comm_set_wdg_time_wr_en);
	clear_sw_interlock(16#A5#, clk, comm_sw_ilock1_wr, comm_sw_ilock1_wr_en);
	clear_sw_interlock(16#A5#, clk, comm_sw_ilock2_wr, comm_sw_ilock2_wr_en);
	
	wait for 1000000 ns;
	
	set_sw_interlock(16#5A#, clk, comm_sw_ilock1_wr, comm_sw_ilock1_wr_en);
	set_sw_interlock(16#5A#, clk, comm_sw_ilock2_wr, comm_sw_ilock2_wr_en);
	
	wait for 1000000 ns;
	
	clear_sw_interlock(16#A5#, clk, comm_sw_ilock1_wr, comm_sw_ilock1_wr_en);
	clear_sw_interlock(16#A5#, clk, comm_sw_ilock2_wr, comm_sw_ilock2_wr_en);
	
	wait for 1000000 ns;
	
	done_sim <= '1';
	wait;

end process;



end behaviour;
