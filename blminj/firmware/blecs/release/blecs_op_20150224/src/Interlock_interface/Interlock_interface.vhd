------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    01/04/2014
--Module Name:    Interlock_interface
--Project Name:   Interlock_interface
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity Interlock_interface is
generic(
	sys_clk_freq:			natural := 40000000;
	mono_clk_freq:			natural := 10000000;
	wdg_time_ms_default:	natural := 2400
);
port(
	-- Input Clock
	sys_clk:		in std_logic;
	rst_sys_clk_n:	in std_logic;
	
	-- VME interlock registers signals
	cibu1_status_rd:		out std_logic_vector(31 downto 0);
	cibu2_status_rd:		out std_logic_vector(31 downto 0);
	hw_ilock1_rd:			out std_logic_vector(31 downto 0);
	hw_ilock2_rd:			out std_logic_vector(31 downto 0);
	sw_ilock1_rd:			out std_logic_vector(31 downto 0);
	sw_ilock2_rd:			out std_logic_vector(31 downto 0);
	current_user_rd:		out std_logic_vector(31 downto 0);
	wdg_timer_rd:			out std_logic_vector(31 downto 0);
	wdg_time_rd:			out std_logic_vector(31 downto 0);
	comm_hw_ilock1_wr:		in std_logic_vector(31 downto 0);
	comm_hw_ilock1_wr_en:	in std_logic;
	comm_hw_ilock2_wr:		in std_logic_vector(31 downto 0);
	comm_hw_ilock2_wr_en:	in std_logic;
	comm_sw_ilock1_wr:		in std_logic_vector(31 downto 0);
	comm_sw_ilock1_wr_en:	in std_logic;
	comm_sw_ilock2_wr:		in std_logic_vector(31 downto 0);
	comm_sw_ilock2_wr_en:	in std_logic;
	comm_set_user_wr:		in std_logic_vector(31 downto 0);
	comm_set_user_wr_en:	in std_logic;
	comm_set_wdg_time_wr:		in std_logic_vector(31 downto 0);
	comm_set_wdg_time_wr_en:	in std_logic;
	comm_reset_wdg_wr:		in std_logic_vector(31 downto 0);
	comm_reset_wdg_wr_en:	in std_logic;
	
	-- Interlock Inputs (DC signal from BLEPT - low = interlock)
	beam_allowed_1:	in std_logic;
	beam_allowed_2:	in std_logic;
	
	-- Beam info inputs
	beam_info_u_in:	in std_logic;
	beam_info_m_in:	in std_logic;
	
	-- Interlock Outputs (clk > 1MHz to a monostable circuit driving CIBUs)
	CIBU1_A:		out std_logic;
	CIBU1_B:		out std_logic;
	CIBU2_A:		out std_logic;
	CIBU2_B:		out std_logic;
	bp_status:		out std_logic_vector(3 downto 0);
	
	-- Beam info synchronised output
	beam_info_u:	out std_logic;
	beam_info_m:	out std_logic

);
	
end entity Interlock_interface;


architecture rtl of Interlock_interface is

attribute keep: boolean;

signal beam_allowed_1_d1 : std_logic;
signal beam_allowed_2_d1 : std_logic;
signal beam_dump_1 : std_logic;
signal beam_dump_2 : std_logic;
signal hw_ilock_1 : std_logic;
signal hw_ilock_2 : std_logic;
signal set_hw_ilock_1 : std_logic;
signal set_hw_ilock_2 : std_logic;
signal clear_hw_ilock_1 : std_logic;
signal clear_hw_ilock_2 : std_logic;


signal msec_en_cnt : integer range 0 to sys_clk_freq/1000 - 1;
signal msec_en_cnt_done : std_logic;
signal wdg_cnt : unsigned(23 downto 0);
signal wdg_time_val : unsigned(23 downto 0);
signal wdg_time_reg : unsigned(23 downto 0);
signal wdg_time_reg_en : std_logic;
signal wdg_time_reg_en_d1 : std_logic;
signal wdg_cnt_expired : std_logic;
signal wdg_cnt_rst : std_logic;
signal user_reg : unsigned(4 downto 0);
signal user_reg_en : std_logic;

signal sw_ilock_1 : std_logic_vector(31 downto 0);
signal sw_ilock_2 : std_logic_vector(31 downto 0);
signal set_sw_ilock_1 : std_logic_vector(31 downto 0);
signal set_sw_ilock_2 : std_logic_vector(31 downto 0);
signal clear_sw_ilock_1 : std_logic_vector(31 downto 0);
signal clear_sw_ilock_2 : std_logic_vector(31 downto 0);
signal sw_ilock_actual_1 : std_logic_vector(31 downto 0);
signal sw_ilock_actual_2 : std_logic_vector(31 downto 0);

signal activate_ilock_1 : std_logic;
signal activate_ilock_2 : std_logic;

signal mono_cnt : integer range 0 to sys_clk_freq/(2*mono_clk_freq) - 1;
signal mono_output : std_logic;
signal mono_cnt_done : std_logic;
	

BEGIN

	-- asserts
	assert wdg_time_ms_default <= 2**16 - 1 report "Maximum default watchdog time (wdg_time_ms_default) is 65535 ms" severity failure;
	assert sys_clk_freq >= 2*mono_clk_freq report "The clock frequency must be at least 2 times higher than mono_clk_freq." severity failure;
	assert sys_clk_freq mod (2*mono_clk_freq) = 0 report "The clock frequency selected does not divide by 2*mono_clk_freq. Verify the output clock to the monostable circuit." severity failure;

	------------------------------------------------------
	-- Hardware interlock circuits
	------------------------------------------------------
	
	-- synchronise beam allowed chains from the BLEPT cards
	-- low level = beam dump request = interlock
	dump_sync_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			beam_allowed_1_d1 <= beam_allowed_1;
			beam_allowed_2_d1 <= beam_allowed_2;
			beam_info_u <= beam_info_u_in;
			beam_info_m <= beam_info_m_in;
		end if;
	end process;
	beam_dump_1 <= not beam_allowed_1_d1;
	beam_dump_2 <= not beam_allowed_2_d1;
	
	-- hardware interlock registers
	hwilock_regs_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if set_hw_ilock_1 = '1' or clear_hw_ilock_1 = '1' then
				hw_ilock_1 <= set_hw_ilock_1;
			end if;
			
			if set_hw_ilock_2 = '1' or clear_hw_ilock_2 = '1' then
				hw_ilock_2 <= set_hw_ilock_2;
			end if;
		end if;
	end process;
	
	-- set hw_ilock when: after power-up, when event on the chains, ...
	set_hw_ilock_1 <= (not rst_sys_clk_n) or beam_dump_1;
	set_hw_ilock_2 <= (not rst_sys_clk_n) or beam_dump_2;
	-- clear hw_ilock by the VME access and a magic number written like specified
	clear_hw_ilock_1 <= '1' when comm_hw_ilock1_wr = x"5AA55AA5" and comm_hw_ilock1_wr_en = '1' else '0';
	clear_hw_ilock_2 <= '1' when comm_hw_ilock2_wr = x"5AA55AA5" and comm_hw_ilock2_wr_en = '1' else '0';
	
	-- make hw interlock registers available to read via the VME interface
	hw_ilock1_rd <= x"0000000" & "000" & hw_ilock_1;
	hw_ilock2_rd <= x"0000000" & "000" & hw_ilock_2;
	
	
	------------------------------------------------------
	-- Watch-dog circuits for the software interlock
	------------------------------------------------------
	
	-- one millisecond free running counter for the watch-dog timer
	msec_en_cnt_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if msec_en_cnt_done = '1' then
				msec_en_cnt <= 0;
			else
				msec_en_cnt <= msec_en_cnt + 1;
			end if;
		end if;
	end process;
	msec_en_cnt_done <= '1' when msec_en_cnt = sys_clk_freq/1000 - 1 else '0';
	
	-- watchdog time register
	-- it stores the watchdog time in ms which can be set via the VME access
	wdg_time_reg_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if wdg_time_reg_en = '1' then
				wdg_time_reg <= wdg_time_val;
			end if;
			wdg_time_reg_en_d1 <= wdg_time_reg_en;
		end if;
	end process;
	-- set the default wdg value or the VME written value with a magic number to enable write access to the register
	wdg_time_val <= to_unsigned(wdg_time_ms_default, 24) when rst_sys_clk_n = '0' else unsigned(comm_set_wdg_time_wr(23 downto 0));
	wdg_time_reg_en <= '1' when (rst_sys_clk_n = '0') or (comm_set_wdg_time_wr(31 downto 24) = x"A5" and comm_set_wdg_time_wr_en = '1') else '0';
	
	-- watchdog free running timer
	-- should be reset via the VME access before it reaches 0
	-- when timeout occurs it is reloaded and the software interlock is applied
	wdg_cnt_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if wdg_cnt_rst = '1' then
				wdg_cnt <= wdg_time_reg;
			elsif msec_en_cnt_done = '1' then
				wdg_cnt <= wdg_cnt - 1;
			end if;
		end if;
	end process;
	wdg_cnt_expired <= '1' when wdg_cnt = 0 else '0';
	wdg_cnt_rst <= '1' when wdg_time_reg_en_d1 = '1' or wdg_cnt_expired = '1' or (comm_reset_wdg_wr = x"5AA55AA5" and comm_reset_wdg_wr_en = '1') else '0';
	
	-- make the wdg timer and register available via the VME interface
	wdg_timer_rd <= x"00" & std_logic_vector(wdg_cnt);
	wdg_time_rd <= x"00" & std_logic_vector(wdg_time_reg);
	
	------------------------------------------------------
	-- Software interlock circuits
	------------------------------------------------------
	
	-- current user register
	-- it stores the user id from 0 to 31 which should be set via the VME access
	user_reg_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if rst_sys_clk_n = '0' then
				user_reg <= (others => '0');
			elsif user_reg_en = '1' then
				user_reg <= unsigned(comm_set_user_wr(4 downto 0));
			end if;
		end if;
	end process;
	-- set the current user written via VME with a magic number to enable write access to the register
	user_reg_en <= '1' when comm_set_user_wr(31 downto 8) = x"A55AA5" and comm_set_user_wr_en = '1' else '0';
	
	-- read back of the current user via the VME
	current_user_rd <= x"000000" & "000" & std_logic_vector(user_reg);
	
	-- generate software interlock registers for 32 users
	sw_ilock_gen: for I in 0 to 31 generate
		
		-- software interlock registers
		swilock_regs_p: process (sys_clk)
		begin
			if rising_edge (sys_clk) then
				if set_sw_ilock_1(I) = '1' or clear_sw_ilock_1(I) = '1' then
					sw_ilock_1(I) <= set_sw_ilock_1(I);
				end if;
				
				if set_sw_ilock_2(I) = '1' or clear_sw_ilock_2(I) = '1' then
					sw_ilock_2(I) <= set_sw_ilock_2(I);
				end if;
			end if;
		end process;
	
		-- set sw_ilock when: after power-up, when the wdg expires, when requested via a VME command ...
		set_sw_ilock_1(I) <= '1' when rst_sys_clk_n = '0' or wdg_cnt_expired = '1' or ((comm_sw_ilock1_wr = x"A55AA5" & std_logic_vector(to_unsigned(I, 8)) or comm_sw_ilock1_wr = x"A55AA55A") and comm_sw_ilock1_wr_en = '1') else '0';
		set_sw_ilock_2(I) <= '1' when rst_sys_clk_n = '0' or wdg_cnt_expired = '1' or ((comm_sw_ilock2_wr = x"A55AA5" & std_logic_vector(to_unsigned(I, 8)) or comm_sw_ilock2_wr = x"A55AA55A") and comm_sw_ilock2_wr_en = '1') else '0';
		-- clear sw_ilock when requested via a VME command
		clear_sw_ilock_1(I) <= '1' when (comm_sw_ilock1_wr = x"5AA55A" & std_logic_vector(to_unsigned(I, 8)) or comm_sw_ilock1_wr = x"5AA55AA5") and comm_sw_ilock1_wr_en = '1' else '0';
		clear_sw_ilock_2(I) <= '1' when (comm_sw_ilock2_wr = x"5AA55A" & std_logic_vector(to_unsigned(I, 8)) or comm_sw_ilock2_wr = x"5AA55AA5") and comm_sw_ilock2_wr_en = '1' else '0';
		
		-- generate software interlock vector
		-- when any of its bits is '1' then the interlock should be activated for a current user
		sw_ilock_actual_1(I) <= '1' when sw_ilock_1(I) = '1' and user_reg = to_unsigned(I, 5) else '0';
		sw_ilock_actual_2(I) <= '1' when sw_ilock_2(I) = '1' and user_reg = to_unsigned(I, 5) else '0';
		
	end generate sw_ilock_gen;
	
	-- make sw interlock registers available to read via the VME interface
	sw_ilock1_rd <= sw_ilock_1;
	sw_ilock2_rd <= sw_ilock_2;
	
	------------------------------------------------------
	-- Output clock to the monostable
	------------------------------------------------------
	
	-- mono_clk_freq [Hz] free running counter for the monostable retriggering
	mono_cnt_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if mono_cnt_done = '1' then
				mono_cnt <= 0;
			else
				mono_cnt <= mono_cnt + 1;
			end if;
			
			-- toggle the output
			if rst_sys_clk_n = '0' then
				mono_output <= '0';
			elsif mono_cnt_done = '1' then
				mono_output <= not mono_output;
			end if;
			
		end if;
	end process;
	mono_cnt_done <= '1' when mono_cnt = sys_clk_freq/(2*mono_clk_freq) - 1 else '0';
	
	activate_ilock_1 <= '1' when hw_ilock_1 = '1' or sw_ilock_actual_1 /= x"00000000" else '0';
	activate_ilock_2 <= '1' when hw_ilock_2 = '1' or sw_ilock_actual_2 /= x"00000000" else '0';
	
	cibu2_status_rd(31 downto 1) <= (others=>'0');
	cibu2_status_rd(0) <= activate_ilock_2;
	cibu1_status_rd(31 downto 1) <= (others=>'0');
	cibu1_status_rd(0) <= activate_ilock_1;
	
	-- retrigger monostable at 10 MHz when there is no interlock condition
	CIBU1_A <= mono_output and not activate_ilock_1;
	CIBU1_B <= mono_output and not activate_ilock_1;
	CIBU2_A <= mono_output and not activate_ilock_2;
	CIBU2_B <= mono_output and not activate_ilock_2;
	
	bp_status <= not activate_ilock_1 & not activate_ilock_1 & not activate_ilock_2 & not activate_ilock_2;
	
	
end architecture rtl;
