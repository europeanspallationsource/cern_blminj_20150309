
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_SPI_master;

reg clk;
reg nrst;

reg spi_data_in;
reg [7:0] data_in;
reg start;


wire busy_m0;
wire spi_sclk_m0;
wire spi_ssel_n_m0;
wire spi_data_out_m0;
wire [7:0] data_out_m0;

wire busy_m1;
wire spi_sclk_m1;
wire spi_ssel_n_m1;
wire spi_data_out_m1;
wire [7:0] data_out_m1;

wire busy_m2;
wire spi_sclk_m2;
wire spi_ssel_n_m2;
wire spi_data_out_m2;
wire [7:0] data_out_m2;

wire busy_m3;
wire spi_sclk_m3;
wire spi_ssel_n_m3;
wire spi_data_out_m3;
wire [7:0] data_out_m3;


SPI_master #(
	.sys_clk_freq(40000000),
	.spi_clk_freq(1000000),
	.spi_bits(8),
	.spi_cpol(0),
	.spi_cpha(0)
) uut_spi_m0(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.data_in(data_in),
	.data_out(data_out_m0),
	.start(start),
	.busy(busy_m0),
	.spi_sclk(spi_sclk_m0),
	.spi_data_out(spi_data_out_m0),
	.spi_data_in(spi_data_in),
	.spi_ssel_n(spi_ssel_n_m0)
);


SPI_master #(
	.sys_clk_freq(40000000),
	.spi_clk_freq(1000000),
	.spi_bits(8),
	.spi_cpol(0),
	.spi_cpha(1)
) uut_spi_m1(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.data_in(data_in),
	.data_out(data_out_m1),
	.start(start),
	.busy(busy_m1),
	.spi_sclk(spi_sclk_m1),
	.spi_data_out(spi_data_out_m1),
	.spi_data_in(spi_data_in),
	.spi_ssel_n(spi_ssel_n_m1)
);



SPI_master #(
	.sys_clk_freq(40000000),
	.spi_clk_freq(1000000),
	.spi_bits(8),
	.spi_cpol(1),
	.spi_cpha(0)
) uut_spi_m2(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.data_in(data_in),
	.data_out(data_out_m2),
	.start(start),
	.busy(busy_m2),
	.spi_sclk(spi_sclk_m2),
	.spi_data_out(spi_data_out_m2),
	.spi_data_in(spi_data_in),
	.spi_ssel_n(spi_ssel_n_m2)
);


SPI_master #(
	.sys_clk_freq(40000000),
	.spi_clk_freq(1000000),
	.spi_bits(8),
	.spi_cpol(1),
	.spi_cpha(1)
) uut_spi_m3(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.data_in(data_in),
	.data_out(data_out_m3),
	.start(start),
	.busy(busy_m3),
	.spi_sclk(spi_sclk_m3),
	.spi_data_out(spi_data_out_m3),
	.spi_data_in(spi_data_in),
	.spi_ssel_n(spi_ssel_n_m3)
);




//40mhz clk
always
begin
	#12 clk = 1'b1;
	#13 clk = 1'b0;
end




initial
begin
	clk = 1'b0;
	data_in = 8'h00;
	spi_data_in = 1'b1;
	
	nrst = 1'b0;
	#50 nrst = 1'b1;
	
	data_in = 8'h5A;
	#50 start = 1'b1;
	#25 start = 1'b0;
	
	@(negedge busy_m0);
	#1000	$stop;

end


	


endmodule
