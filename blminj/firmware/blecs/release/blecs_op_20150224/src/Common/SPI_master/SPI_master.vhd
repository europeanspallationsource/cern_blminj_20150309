------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    12/05/2014
--Module Name:    SPI_master
--Project Name:   SPI_master
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity SPI_master is
generic(
	sys_clk_freq:	natural := 40000000;
	spi_clk_freq:	natural := 1000000;
	spi_bits:		natural := 24;
	spi_cpol:		natural := 1;
	spi_cpha:		natural := 0
);
port(
	-- Input Clock
	sys_clk:		in std_logic;
	rst_sys_clk_n:	in std_logic;
	
	-- SPI master control signals
	data_in:		in std_logic_vector(spi_bits-1 downto 0);
	data_out:		out std_logic_vector(spi_bits-1 downto 0);
	start:			in std_logic;
	busy:			out std_logic;
	
	--SPI signals
	spi_sclk:		out std_logic;
	spi_data_out:	out std_logic;
	spi_data_in:	in std_logic;
	spi_ssel_n:		out std_logic
);
	
end entity SPI_master;


architecture rtl of SPI_master is

attribute keep: boolean;


signal spi_cnt : integer range 0 to sys_clk_freq/(2*spi_clk_freq) - 1;
signal spi_cnt_done: std_logic;

signal shift_out_reg: std_logic_vector(spi_bits-1 downto 0);
signal shift_out_reg_en: std_logic;
signal shift_out_reg_en_d0: std_logic;
signal shift_out_reg_en_d1: std_logic;
signal shift_in_reg: std_logic_vector(spi_bits-1 downto 0);
signal shift_in_reg_en: std_logic;
signal shift_in_reg_en_d0: std_logic;
signal shift_in_reg_en_d1: std_logic;

signal transmit_bits_cnt: integer range 0 to spi_bits;
signal transmit_bits_load: std_logic;
signal transmit_bits_done: std_logic;

signal fsm_busy: std_logic;
signal fsm_spi_sclk: std_logic;

type STATE_TYPE IS (idle, first_h1, h1, h2, last_h1);
signal state, nextState : STATE_TYPE;

BEGIN

	-- asserts
	assert sys_clk_freq >= 4*spi_clk_freq report "The system clock frequency must be at least 4 times higher than SPI clock frequency." severity failure;
	assert sys_clk_freq mod (2*spi_clk_freq) = 0 report "The clock frequency selected does not divide by 2*SPI clock frequency. Verify the output SPI clock." severity failure;
	assert spi_cpol <= 1 report "Wrong cpol - must be 0 or 1" severity failure;
	assert spi_cpha <= 1 report "Wrong cpha - must be 0 or 1" severity failure;

	------------------------------------------------------
	-- SPI half clock counter
	------------------------------------------------------

	spi_cnt_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if spi_cnt_done = '1' or (start = '1' and fsm_busy = '0') then
				spi_cnt <= 0;
			else
				spi_cnt <= spi_cnt + 1;
			end if;
		end if;
	end process;
	spi_cnt_done <= '1' when spi_cnt = sys_clk_freq/(2*spi_clk_freq) - 1 else '0';
	
	------------------------------------------------------
	-- output data shift register
	------------------------------------------------------
	
	shift_out_reg_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if start = '1' and fsm_busy = '0' then
				spi_data_out <= '0';
				shift_out_reg <= data_in;
			elsif shift_out_reg_en = '1' then
				spi_data_out <= shift_out_reg(spi_bits-1);
				shift_out_reg <= shift_out_reg(spi_bits-2 downto 0) & '0';
			end if;
		end if;
	end process;
	
	
	
	------------------------------------------------------
	-- input data shift register
	------------------------------------------------------
	
	shift_in_reg_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if start = '1' and fsm_busy = '0' then
				shift_in_reg <= (others=>'0');
			elsif shift_in_reg_en = '1' then
				shift_in_reg <= shift_in_reg(spi_bits-2 downto 0) & spi_data_in;
			end if;
		end if;
	end process;
	data_out <= shift_in_reg;
	
	
	------------------------------------------------------
	-- SPI bit counter
	------------------------------------------------------
	
	spi_bitcnt_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if transmit_bits_load = '1' then
				transmit_bits_cnt <= spi_bits;
			elsif shift_out_reg_en = '1' and transmit_bits_done = '0' then
				transmit_bits_cnt <= transmit_bits_cnt - 1;
			end if;
		end if;
	end process;
	
	transmit_bits_done <= '1' when transmit_bits_cnt = 0 else '0';
	
	------------------------------------------------------
	-- SPI state machine
	------------------------------------------------------
	
	fsm_reg_p: process(sys_clk, rst_sys_clk_n)
	begin
		if rst_sys_clk_n = '0' then
			state <= idle;
		elsif rising_edge(sys_clk) then
			state <= nextState;
		end if;
	end process;
	
	
	fsm_cl_p: process(state, start, transmit_bits_done, spi_cnt_done)
	begin
	
	nextState <= state;	
	fsm_spi_sclk <= '0';
	spi_ssel_n <= '1';
	fsm_busy <= '0';
	transmit_bits_load <= '0';
		 	
	case state is
	
		when idle =>
			transmit_bits_load <= '1';
			if start = '1' then
		 		nextState <= first_h1;
			end if;
			
		when first_h1 =>
			fsm_busy <= '1';
			spi_ssel_n <= '0';
			if spi_cnt_done = '1' then
				nextState <= h2;
			end if;
			
		when h1 =>
			fsm_busy <= '1';
			spi_ssel_n <= '0';
			if spi_cnt_done = '1' then
				nextState <= h2;
			end if;
		
		when h2 =>
			fsm_busy <= '1';
			spi_ssel_n <= '0';
			fsm_spi_sclk <= '1';
			if transmit_bits_done = '1' and spi_cnt_done = '1' then
				nextState <= last_h1;
			elsif spi_cnt_done = '1' then
				nextState <= h1;
			end if;
		
		when last_h1 =>
			fsm_busy <= '1';
			spi_ssel_n <= '0';
			if spi_cnt_done = '1' then
				nextState <= idle;
			end if;
				
		
		end case;
	end process;
	
	busy <= fsm_busy;
	spi_sclk <= fsm_spi_sclk when spi_cpol = 0 else not fsm_spi_sclk;
	
	
	Gen_CPHA0: if spi_cpha = 0 generate
		shift_out_reg_en_d0 <= '1' when state = h1 or state = first_h1 else '0';
		shift_in_reg_en_d0 <= '1' when state = h2 else '0';
		ff1_p: process(sys_clk, rst_sys_clk_n)
		begin
			if rst_sys_clk_n = '0' then
				shift_out_reg_en_d1 <= '0';
				shift_in_reg_en_d1 <= '0';
			elsif rising_edge(sys_clk) then
				shift_out_reg_en_d1 <= shift_out_reg_en_d0;
				shift_in_reg_en_d1 <= shift_in_reg_en_d0;
			end if;
		end process;
		shift_out_reg_en <= shift_out_reg_en_d0 and not shift_out_reg_en_d1;
		shift_in_reg_en <= shift_in_reg_en_d0 and not shift_in_reg_en_d1;
		
		
	end generate;

	Gen_CPHA1: if spi_cpha = 1 generate
		shift_out_reg_en_d0 <= '1' when state = h2 else '0';
		shift_in_reg_en_d0 <= '1' when state = h1 or state = last_h1 else '0';
		ff1_p: process(sys_clk, rst_sys_clk_n)
		begin
			if rst_sys_clk_n = '0' then
				shift_out_reg_en_d1 <= '0';
				shift_in_reg_en_d1 <= '0';
			elsif rising_edge(sys_clk) then
				shift_out_reg_en_d1 <= shift_out_reg_en_d0;
				shift_in_reg_en_d1 <= shift_in_reg_en_d0;
			end if;
		end process;
		shift_out_reg_en <= shift_out_reg_en_d0 and not shift_out_reg_en_d1;
		shift_in_reg_en <= shift_in_reg_en_d0 and not shift_in_reg_en_d1;
		
	end generate;
	
	
	
end architecture rtl;
