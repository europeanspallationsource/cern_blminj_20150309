------------------------------
--
--Company:          CERN - BE/BI/BL
--Engineer:         Maciej Kwiatkowski
--Create Date:      03/06/2014
--Module Name:      signed_min_max
--Project Name:     signed_min_max
--Description:      Module tracks min and max values of the in_value in a period given by period_en pulse.
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity signed_min_max is
generic(
	in_bits:		natural := 32
);
port(
	-- Input Clock
	sys_clk:		in std_logic;
	rst_sys_clk_n:	in std_logic;
	
	-- input value to track
	in_value:		in signed(in_bits-1 downto 0);
	in_value_en:	in std_logic;
	
	-- period pulse in which min/max is tracked
	period_en:		in std_logic;
	
	-- min/max found in the period
	max_value:		out signed(in_bits-1 downto 0);
	min_value:		out signed(in_bits-1 downto 0)
);
	
end entity signed_min_max;


architecture rtl of signed_min_max is

attribute keep: boolean;



signal min_value_reg: signed(in_bits-1 downto 0);
signal max_value_reg: signed(in_bits-1 downto 0);
	

BEGIN

	-- asserts
	assert false report "Component signed_min_max is untested. Test it and remove the assert." severity failure;

	------------------------------------------------------
	-- track min and max values in a period
	------------------------------------------------------
	
	-- scan min values
	-- reset every period
	scan_min_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if period_en = '1' or rst_sys_clk_n = '0' then
				min_value_reg <=  to_signed(2**(in_bits-1)-1, in_bits);
			elsif in_value_en = '1' and min_value_reg > in_value then
				min_value_reg <= in_value;
			end if;
		end if;
	end process;
	
	-- store min values from the last period
	adc_min_reg_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				min_value <= (others=>'0');
			elsif period_en = '1' then
				min_value <= min_value_reg;
			end if;
		end if;
	end process;
	
	-- scan max values
	-- reset every period
	scan_adc_max_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if period_en = '1' or rst_sys_clk_n = '0' then
				max_value_reg <= to_signed(-1*(2**(in_bits-1)), in_bits);
			elsif in_value_en = '1' and max_value_reg < in_value then
				max_value_reg <= in_value;
			end if;
		end if;
	end process;
	
	-- store max values from the last period
	adc_max_reg_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				max_value <= (others=>'0');
			elsif period_en = '1' then
				max_value <= max_value_reg;
			end if;
		end if;
	end process;
	
end architecture rtl;
