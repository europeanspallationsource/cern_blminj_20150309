------------------------------
--
--Company:          CERN - BE/BI/BL
--Engineer:         Maciej Kwiatkowski
--Create Date:      04/06/2014
--Module Name:      events_counter
--Project Name:     events_counter
--Description:      Module counts events in a period given by period_en pulse. Events input in_event can be configured as edge or level sensitive.
--                  When level sensitive then the number of events is equal to the number of sys_clk cycles when the active level was present.
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity events_counter is
generic(
    cnt_bits:       natural := 32;
    -- only one of following booleans should be true
    -- rising egde, falling edge, higl level, low level sensitivity
    rising:         boolean := true;
    falling:        boolean := false;
    high_l:         boolean := false;
    low_l:          boolean := false
);
port(
    -- Input Clock
    sys_clk:        in std_logic;
    rst_sys_clk_n:  in std_logic;
    
    -- event which is counted
    in_event:       in std_logic;
    
    -- period pulse in which events are counted
    period_en:      in std_logic;
    
    -- events detected in the period
    events_number:  out std_logic_vector(cnt_bits-1 downto 0);
    
    -- overflow signal
    events_ov:      out std_logic
);
    
end entity events_counter;



architecture rtl of events_counter is

attribute keep: boolean;

signal events_number_cnt: unsigned(cnt_bits-1 downto 0);
signal events_number_cnt_done: std_logic;
signal in_event_en: std_logic;
signal in_event_d1: std_logic;
signal in_event_d2: std_logic;

BEGIN

    ------------------------------------------------------
    -- select what type of input event enables the counter
    -- rising egde, falling edge, higl level, low level sensitivity
    ------------------------------------------------------
    
    ff_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' then
                in_event_d1 <= '0';
                in_event_d2 <= '0';
            else
                in_event_d1 <= in_event;
                in_event_d2 <= in_event_d1;
            end if;
        end if;
    end process;
    
    rising_edge_sensitive: if rising = true generate
        -- detect rising edge of the event
        in_event_en <= in_event_d1 and not in_event_d2;
    end generate;
    
    falling_edge_sensitive: if falling = true generate
        -- detect falling edge of the event
        in_event_en <= not in_event_d1 and in_event_d2;
    end generate;
    
    high_level_sensitive: if high_l = true generate
        in_event_en <= in_event_d2;
    end generate;
    
    
    low_level_sensitive: if low_l = true generate
        in_event_en <= not in_event_d2;
    end generate;
    
    ------------------------------------------------------
    -- count events in a period
    ------------------------------------------------------
    
    evcnt_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if period_en = '1' or rst_sys_clk_n = '0' then
                events_number_cnt <= (others=>'0');
            elsif in_event_en = '1' and events_number_cnt_done = '0' then
                events_number_cnt <= events_number_cnt + 1;
            end if;
        end if;
    end process;
    
    events_number_cnt_done <= '1' when events_number_cnt = unsigned'(events_number_cnt'range => '1') else '0';
    --events_number_cnt_done <= '1' when events_number_cnt = 2**cnt_bits-1 else '0';    -- this gives integer type overflow when cnt_bits = 32
    events_ov <= events_number_cnt_done;

    
    ------------------------------------------------------
    -- store min values from the last period
    ------------------------------------------------------
    
    evreg_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' then
                events_number <= (others=>'0');
            elsif period_en = '1' then
                events_number <= std_logic_vector(events_number_cnt);
            end if;
        end if;
    end process;
    
end architecture rtl;
