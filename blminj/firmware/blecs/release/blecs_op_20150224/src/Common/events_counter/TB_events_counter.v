
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_events_counter;

reg clk;
reg nrst;

reg in_event;
reg period_en;

wire [7:0] events_number_rising;
wire [7:0] events_number_falling;
wire [7:0] events_number_high;
wire [7:0] events_number_low;
wire events_ov_rising;
wire events_ov_falling;
wire events_ov_high;
wire events_ov_low;


events_counter #(
	.cnt_bits(8),
	.rising(1),
	.falling(0),
	.high_l(0),
	.low_l(0)
) uut_events_counter_rising(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.in_event(in_event),
	.period_en(period_en),
	.events_number(events_number_rising),
	.events_ov(events_ov_rising)
);

events_counter #(
	.cnt_bits(8),
	.rising(0),
	.falling(1),
	.high_l(0),
	.low_l(0)
) uut_events_counter_falling(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.in_event(in_event),
	.period_en(period_en),
	.events_number(events_number_falling),
	.events_ov(events_ov_falling)
);

events_counter #(
	.cnt_bits(8),
	.rising(0),
	.falling(0),
	.high_l(1),
	.low_l(0)
) uut_events_counter_high(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.in_event(in_event),
	.period_en(period_en),
	.events_number(events_number_high),
	.events_ov(events_ov_high)
);


events_counter #(
	.cnt_bits(8),
	.rising(0),
	.falling(0),
	.high_l(0),
	.low_l(1)
) uut_events_counter_low(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.in_event(in_event),
	.period_en(period_en),
	.events_number(events_number_low),
	.events_ov(events_ov_low)
);

//40mhz clk
always
begin
	#12 clk = 1'b1;
	#13 clk = 1'b0;
end




initial
begin
	clk = 1'b0;
	in_event = 1'b0;
	period_en = 1'b0;
	
	nrst = 1'b0;
	#50 nrst = 1'b1;
	
	//event to count
	#50 in_event = 1'b1;
	#25 in_event = 1'b0;
    
    #50 in_event = 1'b1;
	#8000 in_event = 1'b0;
	
    
    //end of first period pulse
    
    #50 period_en = 1'b1;
    #25 period_en = 1'b0;
    
    
    //end of second period pulse
    
    #8000 period_en = 1'b1;
    #25 period_en = 1'b0;
	
	#100	$stop;

end


	


endmodule
