------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    04/06/2014
--Module Name:    i2c_acknowledge
--Project Name:   i2c_acknowledge
--Description:    this module is used in the simulation only to emulate ack or nack of a physical device
--                this module is simulating ack by pulling sda low every 9th scl cycle
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity i2c_acknowledge is
port(
    ack   : in std_logic;       -- 0 - ack, 1 - nack
	-- i2c lines
	scl   : in std_logic;       -- i2c clock line
	sda   : inout std_logic     -- i2c data line
);
    
end entity i2c_acknowledge;


architecture rtl of i2c_acknowledge is


signal sda_i : std_logic;
signal scl_i : std_logic;
signal stop_condition : std_logic;
signal scl_cnt_done : std_logic;
signal scl_cnt_done_d1 : std_logic;
signal scl_cnt : unsigned(3 downto 0) := (others=>'0');

BEGIN
    
    sda <= ack when (scl_cnt_done_d1 = '1') else 'Z';
    
    sda_i <= '0' when sda = '0' else '1';
    scl_i <= '0' when scl = '0' else '1';
    
    stop_det_p: process(sda_i)
    begin
        if rising_edge(sda_i) then
            if scl_i = '1' then
                stop_condition <= '1';
            end if;
        elsif falling_edge(sda_i) then
            if scl_i = '1' then
                stop_condition <= '0';
            end if;
        end if;
    end process;
    
    
    scl_cnt_p: process(scl, stop_condition)
    begin
        if stop_condition = '1' then
            scl_cnt <= (others=>'0');
        elsif rising_edge(scl) then
            if scl_cnt_done = '1' then
                scl_cnt <= (others=>'0');
            else
                scl_cnt <= scl_cnt + 1;
            end if;
        end if;
    end process;
    
    ack_del_p: process(scl)
    begin
        if falling_edge(scl) then
            scl_cnt_done_d1 <= scl_cnt_done;
        end if;
    end process;
    
    scl_cnt_done <= '1' when scl_cnt = 8 else '0';
    
end architecture rtl;
