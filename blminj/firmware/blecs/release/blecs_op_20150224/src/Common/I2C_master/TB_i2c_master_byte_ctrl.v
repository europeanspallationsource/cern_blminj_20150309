
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_i2c_master_byte_ctrl;

reg clk;
reg nrst;

reg [7:0] din;
reg start;
reg stop;
reg read;
reg write;
reg ack_in;
//pull-up wires scl and sda!!!!
wire (strong0, weak1) scl = 1;
wire (strong0, weak1) sda = 1;


wire cmd_ack;
wire ack_out;
wire i2c_busy;
wire i2c_al;
wire [7:0] dout;


//unit under test
i2c_master_top
 uut_i2c_master_top(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.start(start),
	.stop(stop),
	.read(read),
	.write(write),
	.ack_in(ack_in),
	.din(din),
	.cmd_ack(cmd_ack),
	.ack_out(ack_out),
	.i2c_busy(i2c_busy),
	.i2c_al(i2c_al),
	.dout(dout),
	.sda(sda),
	.scl(scl)
);

//this module is simulating ack by pulling sda low every 9th scl cycle
i2c_acknowledge
 sim_i2c_acknowledge(
	.ack(1'b0),     //0 - ack, 1 - nack
	.scl(scl),
	.sda(sda)
);

//40mhz clk
always
begin
	#12 clk = 1'b1;
	#13 clk = 1'b0;
end

//write 3 bytes to to the slave
initial
begin
	clk = 1'b0;
	din = 8'h00;
	start = 1'b0;
	stop = 1'b0;
	read = 1'b0;
	write = 1'b0;
	ack_in = 1'b0;
	
    //reset
	nrst = 1'b0;
	#50 nrst = 1'b1;
	
    //generate start and write first byte (this will be an address)
	din = 8'h5A;
	#50 start = 1'b1;
    write = 1'b1;
	#25 start = 1'b0;
	
    //keep writing second byte
	@(posedge cmd_ack);
    din = 8'hAA;
    
    //keep writing third byte
    @(posedge cmd_ack);
    din = 8'h55;
    
    //generate stop
    @(posedge cmd_ack);
    write = 1'b0;
    #50 stop = 1'b1;
	#25 stop = 1'b0;
    
    @(posedge cmd_ack);
    
    
	#100	$stop;

end


	


endmodule
