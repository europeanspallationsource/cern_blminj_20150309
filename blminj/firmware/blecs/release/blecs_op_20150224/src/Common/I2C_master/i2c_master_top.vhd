------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    04/06/2014
--Module Name:    i2c_master_top
--Project Name:   i2c_master_top
--Description:    Top entity and wrapper for the i2c_master_byte_ctrl with tri-state buffers
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity i2c_master_top is
generic(
    sys_clk_freq:            natural := 40000000;
    i2c_clk_freq:            natural := 1000000
);
port(
    -- Input Clock
    sys_clk:            in std_logic;
    rst_sys_clk_n:      in std_logic;
 
	-- input signals
	start,
	stop,
	read,
	write,
	ack_in : std_logic;
	din    : in std_logic_vector(7 downto 0);
 
	-- output signals
	cmd_ack  : out std_logic; -- command done
	ack_out  : out std_logic;
	i2c_busy : out std_logic; -- arbitration lost
	i2c_al   : out std_logic; -- i2c bus busy
	dout     : out std_logic_vector(7 downto 0);
 
	-- i2c lines
	scl   : inout std_logic;  -- i2c clock line
	sda   : inout std_logic  -- i2c data line
);
    
end entity i2c_master_top;


architecture rtl of i2c_master_top is

signal scl_o : std_logic;
signal scl_i : std_logic;
signal scl_oen : std_logic;
signal sda_o : std_logic;
signal sda_i : std_logic;
signal sda_oen : std_logic;
signal rst : std_logic;
constant clk_cnt : std_logic_vector(15 downto 0) := std_logic_vector(to_unsigned(sys_clk_freq/(5*i2c_clk_freq)-1, 16));

BEGIN

    -- asserts
    assert sys_clk_freq/(5*i2c_clk_freq)-1 >= 0 report "i2c_master_top: sys_clk_freq must be at lest 5 times higher than i2c_clk_freq" severity failure;
    
    -- master i2c controller instance
    i2c_master_byte_ctrl_i: entity work.i2c_master_byte_ctrl
    port map
    (
        clk    => sys_clk,
		rst    => rst,
		nReset => '1',
		ena    => '1',
		clk_cnt => clk_cnt,
		start   => start,
		stop    => stop,
		read    => read,
		write   => write,
		ack_in  => ack_in,
		din     => din,
		cmd_ack => cmd_ack,
		ack_out => ack_out,
		i2c_busy=> i2c_busy,
		i2c_al  => i2c_al,
		dout    => dout,
		scl_i   => scl_i,
		scl_o   => scl_o,
		scl_oen => scl_oen,
		sda_i   => sda_i,
		sda_o   => sda_o,
		sda_oen => sda_oen
    );
    
    scl <= scl_o when (scl_oen = '0') else 'Z';
    sda <= sda_o when (sda_oen = '0') else 'Z';
    scl_i <= scl;
    sda_i <= sda;
    
    rst <= not rst_sys_clk_n;
    
end architecture rtl;
