
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_DAC8532;

reg clk;
reg nrst;

reg [15:0] dac_a;
reg [15:0] dac_b;
reg [1:0] pwr_down_a;
reg [1:0] pwr_down_b;
reg dac_a_set;
reg dac_b_set;
reg dac_ab_set;

wire busy;
wire sclk;
wire data;
wire sync_n;




DAC8532 #(
	.sys_clk_freq(40000000),
	.spi_clk_freq(1000000)
) uut_dac8532(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.dac_a(dac_a),
	.pwr_down_a(pwr_down_a),
	.dac_b(dac_b),
	.pwr_down_b(pwr_down_b),
	.dac_a_set(dac_a_set),
	.dac_b_set(dac_b_set),
	.dac_ab_set(dac_ab_set),
	.busy(busy),
	.sclk(sclk),
	.data(data),
	.sync_n(sync_n)
);




//40mhz clk
always
begin
	#12 clk = 1'b1;
	#13 clk = 1'b0;
end




initial
begin
	clk = 1'b0;
	dac_a = 16'h5AA5;
	dac_b = 16'hA55A;
	pwr_down_a = 2'b00;
	pwr_down_b = 2'b00;
	dac_a_set = 1'b0;
	dac_b_set = 1'b0;
	dac_ab_set = 1'b0;
	
	nrst = 1'b0;
	#50 nrst = 1'b1;
	
	//set dac a
	#50 dac_a_set = 1'b1;
	#25 dac_a_set = 1'b0;
	
	@(negedge busy);
	
	//set dac b
	#50 dac_b_set = 1'b1;
	#25 dac_b_set = 1'b0;
	
	@(negedge busy);
	
	//set both dacs
	#50 dac_ab_set = 1'b1;
	#25 dac_ab_set = 1'b0;
	
	@(negedge busy);
	
	#1000	$stop;

end


	


endmodule
