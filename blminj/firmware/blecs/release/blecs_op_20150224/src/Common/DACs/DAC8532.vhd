------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    12/05/2014
--Module Name:    DAC8532
--Project Name:   DAC8532
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity DAC8532 is
generic(
	sys_clk_freq:			natural := 40000000;
	spi_clk_freq:			natural := 1000000
);
port(
	-- Input Clock
	sys_clk:		in std_logic;
	rst_sys_clk_n:	in std_logic;
	
	-- DAC control signals
	dac_a:			in std_logic_vector(15 downto 0);
	pwr_down_a:		in std_logic_vector(1 downto 0);
	
	dac_b:			in std_logic_vector(15 downto 0);
	pwr_down_b:		in std_logic_vector(1 downto 0);
	
	dac_a_set:		in std_logic;
	dac_b_set:		in std_logic;
	dac_ab_set:		in std_logic;
	
	busy:			out std_logic;
	
	--SPI signals
	sclk:			out std_logic;
	data:			out std_logic;
	sync_n:			out std_logic
);
	
end entity DAC8532;


architecture rtl of DAC8532 is

attribute keep: boolean;

signal dac8532_data: std_logic_vector(23 downto 0);
signal dac8532_data_en: std_logic;
signal spi_master_busy: std_logic;
signal spi_transfer_done: std_logic;
signal start_spi_1: std_logic;
signal start_spi_1_d1: std_logic;
signal start_spi_1_en: std_logic;
signal start_spi_2: std_logic;
signal start_spi_2_d1: std_logic;
signal start_spi_2_en: std_logic;
constant delay_100ns : natural := integer(ceil(real(sys_clk_freq)/10000000.0))-1;
signal delay_cnt : natural range 0 to delay_100ns;

type STATE_TYPE IS (idle, load_single_a, load_single_b, write_a, write_b_load_ab);
signal state, nextState : STATE_TYPE;

BEGIN

	-- asserts
	assert spi_clk_freq < 30000000 report "Maximum clock frequency of the DAC8532 is 30 MHz" severity failure;
	
	------------------------------------------------------
	-- Generic SPI master controller instance
	------------------------------------------------------
	SPI_master_i: entity work.SPI_master
    generic map
    (
        sys_clk_freq	=> sys_clk_freq,
		spi_clk_freq	=> spi_clk_freq,
		spi_bits		=> 24,
		spi_cpol		=> 1,
		spi_cpha		=> 0
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- SPI master control signals
		data_in			=> dac8532_data,
		data_out		=> open,
		start			=> dac8532_data_en,
		busy			=> spi_master_busy,
		
		--SPI signals
		spi_sclk		=> sclk,
		spi_data_out	=> data,
		spi_data_in		=> '0',
		spi_ssel_n		=> sync_n
	);
	
	dac8532_data <= 	"000000" & pwr_down_a & dac_a when state = write_a else
						"001101" & pwr_down_b & dac_b when state = write_b_load_ab else
						"000100" & pwr_down_a & dac_a when state = load_single_a else
						"001001" & pwr_down_b & dac_b when state = load_single_b else
						(others=>'0');
						
	
	------------------------------------------------------
	-- DAC8532 state machine
	------------------------------------------------------
	
	fsm_reg_p: process(sys_clk, rst_sys_clk_n)
	begin
		if rst_sys_clk_n = '0' then
			state <= idle;
		elsif rising_edge(sys_clk) then
			state <= nextState;
		end if;
	end process;
	
	
	fsm_cl_p: process(state, spi_transfer_done, dac_ab_set, dac_a_set, dac_b_set)
	begin
	
	nextState <= state;	
	start_spi_1 <= '0';
	start_spi_2 <= '0';
	
	
	case state is
	
		when idle =>
			if dac_ab_set = '1' then
		 		nextState <= write_a;
			elsif dac_a_set = '1' then
				nextState <= load_single_a;
			elsif dac_b_set = '1' then
				nextState <= load_single_b;
			end if;
			
		when load_single_a =>
			start_spi_1 <= '1';
			if spi_transfer_done = '1' then
		 		nextState <= idle;
			end if;
			
		when load_single_b =>
			start_spi_1 <= '1';
			if spi_transfer_done = '1' then
		 		nextState <= idle;
			end if;
			
		when write_a =>
			start_spi_1 <= '1';
			if spi_transfer_done = '1' then
		 		nextState <= write_b_load_ab;
			end if;
			
		when write_b_load_ab =>
			start_spi_2 <= '1';
			if spi_transfer_done = '1' then
		 		nextState <= idle;
			end if;
		
		end case;
	end process;
	
	-- rising edge detectors for selected states (when FSM enters)
	ff1_p: process(sys_clk, rst_sys_clk_n)
	begin
		if rst_sys_clk_n = '0' then
			start_spi_1_d1 <= '0';
			start_spi_2_d1 <= '0';
		elsif rising_edge(sys_clk) then
			start_spi_1_d1 <= start_spi_1;
			start_spi_2_d1 <= start_spi_2;
		end if;
	end process;
	start_spi_1_en <= start_spi_1 and not start_spi_1_d1;
	start_spi_2_en <= start_spi_2 and not start_spi_2_d1;
	dac8532_data_en <= start_spi_1_en or start_spi_2_en;
	
	------------------------------------------------------
	-- Count 100 ns delay after SPI transfer
	-- required by the DAC8532
	------------------------------------------------------
	
	spi_done_cnt_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if rst_sys_clk_n = '0' or dac8532_data_en = '1' or state = idle then
				delay_cnt <= 0;
			elsif spi_transfer_done = '0' and spi_master_busy = '0' then
				delay_cnt <= delay_cnt + 1;
			end if;
		end if;
	end process;
	
	spi_transfer_done <= '1' when delay_cnt = delay_100ns and dac8532_data_en = '0' else '0';
	
	busy <= start_spi_1 or start_spi_2;
	
end architecture rtl;
