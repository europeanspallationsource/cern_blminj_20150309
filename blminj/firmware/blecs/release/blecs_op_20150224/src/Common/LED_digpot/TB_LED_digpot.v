
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_LED_digpot;

reg clk;
reg nrst;

reg [15:0] LED_data;
reg [7:0] pot1_data;
reg [7:0] pot2_data;
reg [7:0] pot3_data;
reg [7:0] pot4_data;
reg start_wr;

//pull-up wires scl and sda!!!!
wire (strong0, weak1) scl = 1;
wire (strong0, weak1) sda = 1;


wire [1:0] i2c_ack_error;
wire busy;
reg ack;


//unit under test
LED_digpot #(
	.sys_clk_freq(40000000),
	.spi_clk_freq(400000)
)
 uut_LED_digpot(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.LED_data(LED_data),
	.pot1_data(pot1_data),
	.pot2_data(pot2_data),
	.pot3_data(pot3_data),
	.pot4_data(pot4_data),
	.start_wr(start_wr),
	.i2c_ack_error(i2c_ack_error),
	.busy(busy),
	.I2C_SDA_POT_LED(sda),
	.I2C_SCL_POT_LED(scl)
);

//this module is simulating ack by pulling sda low every 9th scl cycle
i2c_acknowledge
 sim_i2c_acknowledge(
	.ack(ack),     //0 - ack, 1 - nack
	.scl(scl),
	.sda(sda)
);

//40mhz clk
always
begin
	#12 clk = 1'b1;
	#13 clk = 1'b0;
end


initial
begin
	clk = 1'b0;
	LED_data = 16'h0000;
	pot1_data = 16'h00;
	pot2_data = 16'h00;
	pot3_data = 16'h00;
	pot4_data = 16'h00;
	start_wr = 1'b0;
	ack = 1'b0;
	
    //reset
	nrst = 1'b0;
	#50 nrst = 1'b1;
	
    //generate start of the first round
	LED_data = 16'h5AA5;
	pot1_data = 16'h55;
	pot2_data = 16'hF0;
	pot3_data = 16'h0F;
	pot4_data = 16'hAA;
	#50 start_wr = 1'b1;
	#25 start_wr = 1'b0;
	//wait for completion of the first round
    @(negedge busy);
    
    //generate start of the second round
	#50000 start_wr = 1'b1;
    
    //simulate noack (failure) of first device in the second round
	#10325 ack = 1'b1;
	#1150 ack = 1'b0;
    //wait for completion of the second round
    @(negedge busy);
    
    //third round starts as the start_wr was left high
    //remove start_wr
    #50 start_wr = 1'b0;
    
    //simulate noack (failure) of second device in the third round
    //place noak in the third byte of the writing to the first potentiometer of the device
	#64400 ack = 1'b1;
	#1150 ack = 1'b0;
    
    //wait for completion of the third round
    @(negedge busy);
    
    #50000	$stop;

end


	


endmodule
