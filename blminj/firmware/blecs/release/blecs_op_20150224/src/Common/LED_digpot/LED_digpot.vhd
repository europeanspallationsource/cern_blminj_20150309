------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    04/06/2014
--Module Name:    LED_digpot
--Project Name:   LED_digpot
--Description:    This module is to write data to the PCF8575 IO expander and AD5263 digital potentioneter
--                both connected to the same I2C bus
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity LED_digpot is
generic(
    sys_clk_freq:            natural := 40000000;
    i2c_clk_freq:            natural := 400000
);
port(
    -- Input Clock
    sys_clk:            in std_logic;
    rst_sys_clk_n:      in std_logic;
    
    -- LED and digipot control signals
    LED_data:           in std_logic_vector(15 downto 0);
    pot1_data:          in std_logic_vector(7 downto 0);
    pot2_data:          in std_logic_vector(7 downto 0);
    pot3_data:          in std_logic_vector(7 downto 0);
    pot4_data:          in std_logic_vector(7 downto 0);
    start_wr:           in std_logic;
    i2c_ack_error:      out std_logic_vector(1 downto 0);   -- i2c_ack_error(0) = PCF8575 ACK/NOACK, i2c_ack_error(1) = AD5263 ACK/NOACK
    busy:               out std_logic;
    
    --I2C signals
    I2C_SDA_POT_LED:    inout std_logic;
    I2C_SCL_POT_LED:    inout std_logic
);
    
end entity LED_digpot;


architecture rtl of LED_digpot is

attribute keep: boolean;
signal command_index : natural range 0 to 20;
signal command_index_done : std_logic;
signal command_index_done_d1 : std_logic;
signal command_index_done_en : std_logic;
signal command_index_en : std_logic;
signal start_wr_reg : std_logic;

signal i2c_data_wr : std_logic_vector(10 downto 0);
signal i2c_start : std_logic;
signal i2c_stop : std_logic;
signal i2c_write : std_logic;
signal i2c_cmd_ack : std_logic;
signal i2c_ack_out : std_logic;


BEGIN

    -- asserts
	assert i2c_clk_freq <= 400000 report "Maximum clock frequency of the PCF8575 is 400 kHz" severity failure;
    
    ------------------------------------------------------
    -- Generic I2C master controller instance
    ------------------------------------------------------
    i2c_master_i: entity work.i2c_master_top
    generic map
    (
        sys_clk_freq    => sys_clk_freq,
        i2c_clk_freq    => i2c_clk_freq
    )
    port map
    (
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        start           => i2c_start,
        stop            => i2c_stop,        
        read            => '0',             
        write           => i2c_write,     
        ack_in          => '0',        
        din             => i2c_data_wr(7 downto 0),            
        cmd_ack         => i2c_cmd_ack,   
        ack_out         => i2c_ack_out,   -- when 1 then there is no ack from the device
        i2c_busy        => open,
        i2c_al          => open,
        dout            => open,
        scl             => I2C_SCL_POT_LED,
        sda             => I2C_SDA_POT_LED
    );
    
    
    
    ---------------------------------------------------------------------------
    -- Select 8 bit data to I2C bus according to current wr index
    -- Three MSB bits are indicating I2C commands: write, start and stop
    ---------------------------------------------------------------------------
    
    i2c_data_wr <=
            '1' & '1' & '0' & "01000000"              when command_index = 0 else    -- address of PCF8575 device - LSB 0 to indicate WR
            '1' & '0' & '0' & LED_data(15 downto 8)   when command_index = 1 else    -- write MSB to PCF8575 IO expander
            '1' & '0' & '0' & LED_data(7 downto 0)    when command_index = 2 else    -- write LSB to PCF8575 IO expander
            '0' & '0' & '1' & "00000000"              when command_index = 3 else    -- generate I2C stop
            
            '1' & '1' & '0' & "01011000"              when command_index = 4 else    -- address of AD5263 device - LSB 0 to indicate WR
            '1' & '0' & '0' & "00000000"              when command_index = 5 else    -- address AD5263 digital potentioneter 1 register
            '1' & '0' & '0' & pot1_data               when command_index = 6 else    -- write AD5263 digital potentioneter 1 register
            '0' & '0' & '1' & "00000000"              when command_index = 7 else    -- generate I2C stop
            
            '1' & '1' & '0' & "01011000"              when command_index = 8 else    -- address of AD5263 device - LSB 0 to indicate WR
            '1' & '0' & '0' & "00100000"              when command_index = 9 else    -- address AD5263 digital potentioneter 2 register
            '1' & '0' & '0' & pot2_data               when command_index = 10 else   -- write AD5263 digital potentioneter 2 register
            '0' & '0' & '1' & "00000000"              when command_index = 11 else   -- generate I2C stop
            
            '1' & '1' & '0' & "01011000"              when command_index = 12 else   -- address of AD5263 device - LSB 0 to indicate WR
            '1' & '0' & '0' & "01000000"              when command_index = 13 else   -- address AD5263 digital potentioneter 3 register
            '1' & '0' & '0' & pot3_data               when command_index = 14 else   -- write AD5263 digital potentioneter 3 register
            '0' & '0' & '1' & "00000000"              when command_index = 15 else   -- generate I2C stop
            
            '1' & '1' & '0' & "01011000"              when command_index = 16 else   -- address of AD5263 device - LSB 0 to indicate WR
            '1' & '0' & '0' & "01100000"              when command_index = 17 else   -- address AD5263 digital potentioneter 4 register
            '1' & '0' & '0' & pot4_data               when command_index = 18 else   -- write AD5263 digital potentioneter 4 register
            '0' & '0' & '1' & "00000000"              when command_index = 19 else   -- generate I2C stop
            (others=>'0');
    
    i2c_stop <= i2c_data_wr(8);
    i2c_start <= i2c_data_wr(9);
    i2c_write <= i2c_data_wr(10);

    ------------------------------------------------------
    -- Start transfer bit
    -- used to enable the data index counter
    ------------------------------------------------------
    
    start_reg_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' or command_index_done_en = '1' then
                start_wr_reg <= '0';
            elsif start_wr = '1' then
                start_wr_reg <= start_wr;
            end if;
            command_index_done_d1 <= command_index_done;
        end if;
    end process;
    command_index_done_en <= command_index_done and not command_index_done_d1;
    
    busy <= start_wr_reg;
    
    ------------------------------------------------------
    -- Data index counter
    ------------------------------------------------------
    
    data_i_cnt_p: process(sys_clk)
    begin
        if rising_edge (sys_clk) then
            if rst_sys_clk_n = '0' or (start_wr = '1' and start_wr_reg = '0')  then
                command_index <= 0;
            elsif command_index_en = '1' then
                command_index <= command_index + 1;
            end if;
        end if;
    end process;
    
    -- count when transfer is started and the previous I2C command completed
    command_index_en <= '1' when start_wr_reg = '1' and i2c_cmd_ack = '1' else '0';
    -- end of full cycle - all data is sent
    command_index_done <= '1' when command_index = 20 else '0';
    
    
    ------------------------------------------------------
    -- Store I2C ack error for each device separately
    ------------------------------------------------------
    
    ack_err_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' or (start_wr = '1' and start_wr_reg = '0')  then
                i2c_ack_error <= "00";
            elsif i2c_ack_out = '1' and command_index <= 3 then
                i2c_ack_error(0) <= i2c_ack_out;
            elsif i2c_ack_out = '1' and command_index > 3 then
                i2c_ack_error(1) <= i2c_ack_out;
            end if;
        end if;
    end process;
    
end architecture rtl;
