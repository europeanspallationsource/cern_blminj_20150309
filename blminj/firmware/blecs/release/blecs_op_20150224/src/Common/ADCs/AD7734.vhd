------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    12/05/2014
--Module Name:    AD7734
--Project Name:   AD7734
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity AD7734 is
generic(
	sys_clk_freq:			natural := 40000000;
	spi_clk_freq:			natural := 1000000
);
port(
	-- Input Clock
	sys_clk:		in std_logic;
	rst_sys_clk_n:	in std_logic;
	
	-- ADC control signals
	cnv_start:		in std_logic;
	data_0	:		out std_logic_vector(23 downto 0);
	data_1	:		out std_logic_vector(23 downto 0);
	data_2	:		out std_logic_vector(23 downto 0);
	data_3	:		out std_logic_vector(23 downto 0);
	data_rdy :		out std_logic;
	busy:			out std_logic;
	
	--SPI signals
	sclk:			out std_logic;
	dout:			out std_logic;
	din:			in std_logic;
	cs_n:			out std_logic;
	
	-- ADC data ready pin
	rdy_n:			in std_logic
);
	
end entity AD7734;


architecture rtl of AD7734 is

attribute keep: boolean;

signal index_mux : std_logic_vector(13 downto 0);

signal AD7734_data_wr : std_logic_vector(7 downto 0);
signal AD7734_data_rd : std_logic_vector(7 downto 0);
signal AD7734_data_en : std_logic;

signal data_wr_index : natural range 0 to 38;
signal data_wr_index_done : std_logic;
signal data_wr_index_en : std_logic;
signal wait_for_conversion : std_logic;
signal cnv_start_reg : std_logic;
signal wait_rdy : std_logic;
signal rd_data : std_logic_vector(2 downto 0);
signal channel : std_logic_vector(1 downto 0);
signal rdy_n_sync : std_logic;
signal spi_master_busy : std_logic;
signal spi_master_busy_d1 : std_logic;
signal spi_master_done : std_logic;

signal data_0_reg : std_logic_vector(23 downto 0);
signal data_1_reg : std_logic_vector(23 downto 0);
signal data_2_reg : std_logic_vector(23 downto 0);
signal data_3_reg : std_logic_vector(23 downto 0);

BEGIN

	-- asserts
	assert spi_clk_freq < 10000000 report "Maximum clock frequency of the AD7734 is 10 MHz" severity failure;
	
	------------------------------------------------------
	-- Generic SPI master controller instance
	------------------------------------------------------
	SPI_master_i: entity work.SPI_master
    generic map
    (
        sys_clk_freq	=> sys_clk_freq,
		spi_clk_freq	=> spi_clk_freq,
		spi_bits		=> 8,
		spi_cpol		=> 1,
		spi_cpha		=> 1
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- SPI master control signals
		data_in			=> AD7734_data_wr,
		data_out		=> AD7734_data_rd,
		start			=> AD7734_data_en,
		busy			=> spi_master_busy,
		
		--SPI signals
		spi_sclk		=> sclk,
		spi_data_out	=> dout,
		spi_data_in		=> din,
		spi_ssel_n		=> open
	);
	
	
	
	---------------------------------------------------------------------------
	-- Select 8 bit data to write to ADC according to current wr index
	---------------------------------------------------------------------------
	
	index_mux <= 	
			x"00" & "0" & "00" & "000" when data_wr_index = 0 else	-- reset sequence part 1
			x"FF" & "0" & "00" & "000" when data_wr_index = 1 else	-- reset sequence part 2
			x"FF" & "0" & "00" & "000" when data_wr_index = 2 else	-- reset sequence part 3
			x"FF" & "0" & "00" & "000" when data_wr_index = 3 else	-- reset sequence part 4
			x"FF" & "0" & "00" & "000" when data_wr_index = 4 else	-- reset sequence part 5
			x"28" & "0" & "00" & "000" when data_wr_index = 5 else	-- channel 0 setup address to comm register
			x"01" & "0" & "00" & "000" when data_wr_index = 6 else	-- data to channel 0 setup register - (01h: 0-10V)
			x"29" & "0" & "00" & "000" when data_wr_index = 7 else	-- channel 1 setup address to comm register
			x"01" & "0" & "00" & "000" when data_wr_index = 8 else	-- data to channel 1 setup register - (01h: 0-10V)
			x"2A" & "0" & "00" & "000" when data_wr_index = 9 else	-- channel 2 setup address to comm register
			x"01" & "0" & "00" & "000" when data_wr_index = 10 else	-- data to channel 2 setup register - (01h: 0-10V)
			x"2B" & "0" & "00" & "000" when data_wr_index = 11 else	-- channel 3 setup address to comm register
			x"01" & "0" & "00" & "000" when data_wr_index = 12 else	-- data to channel 3 setup register - (01h: 0-10V)
			
			-- start ain0 conversion
			x"38" & "0" & "00" & "000" when data_wr_index = 13 else	-- mode ch0 register address to comm register
			x"43" & "1" & "00" & "000" when data_wr_index = 14 else	-- data to mode ch0 register - (43h: single conversion, 24 bit data, clamp)
			-- before continuing writing to the ADC wait for rdy_n pin falling edge
			x"48" & "0" & "00" & "000" when data_wr_index = 15 else	-- read channel 0 data to comm register
			x"00" & "0" & "00" & "100" when data_wr_index = 16 else	-- write zeros and READ CH0 DATA(23 downto 16)
			x"00" & "0" & "00" & "010" when data_wr_index = 17 else	-- write zeros and READ CH0 DATA(15 downto 8)
			x"00" & "0" & "00" & "001" when data_wr_index = 18 else	-- write zeros and READ CH0 DATA(7 downto 0)
			-- ain0 conversion and read done
			
			-- start ain1 conversion
			x"39" & "0" & "00" & "000" when data_wr_index = 19 else	-- mode ch1 register address to comm register
			x"43" & "1" & "00" & "000" when data_wr_index = 20 else	-- data to mode ch1 register - (43h: single conversion, 24 bit data, clamp)
			-- before continuing writing to the ADC wait for rdy_n pin falling edge
			x"49" & "0" & "00" & "000" when data_wr_index = 21 else	-- read channel 0 data to comm register
			x"00" & "0" & "01" & "100" when data_wr_index = 22 else	-- write zeros and READ CH1 DATA(23 downto 16)
			x"00" & "0" & "01" & "010" when data_wr_index = 23 else	-- write zeros and READ CH1 DATA(15 downto 8)
			x"00" & "0" & "01" & "001" when data_wr_index = 24 else	-- write zeros and READ CH1 DATA(7 downto 0)
			-- ain1 conversion and read done
			
			-- start ain2 conversion
			x"3A" & "0" & "00" & "000" when data_wr_index = 25 else	-- mode ch2 register address to comm register
			x"43" & "1" & "00" & "000" when data_wr_index = 26 else	-- data to mode ch2 register - (43h: single conversion, 24 bit data, clamp)
			-- before continuing writing to the ADC wait for rdy_n pin falling edge
			x"4A" & "0" & "00" & "000" when data_wr_index = 27 else	-- read channel 0 data to comm register
			x"00" & "0" & "10" & "100" when data_wr_index = 28 else	-- write zeros and READ CH2 DATA(23 downto 16)
			x"00" & "0" & "10" & "010" when data_wr_index = 29 else	-- write zeros and READ CH2 DATA(15 downto 8)
			x"00" & "0" & "10" & "001" when data_wr_index = 30 else	-- write zeros and READ CH2 DATA(7 downto 0)
			-- ain2 conversion and read done
			
			-- start ain3 conversion
			x"3B" & "0" & "00" & "000" when data_wr_index = 31 else	-- mode ch3 register address to comm register
			x"43" & "1" & "00" & "000" when data_wr_index = 32 else	-- data to mode ch3 register - (43h: single conversion, 24 bit data, clamp)
			-- before continuing writing to the ADC wait for rdy_n pin falling edge
			x"4B" & "0" & "00" & "000" when data_wr_index = 33 else	-- read channel 0 data to comm register
			x"00" & "0" & "11" & "100" when data_wr_index = 34 else	-- write zeros and READ CH3 DATA(23 downto 16)
			x"00" & "0" & "11" & "010" when data_wr_index = 35 else	-- write zeros and READ CH3 DATA(15 downto 8)
			x"00" & "0" & "11" & "001" when data_wr_index = 36 else	-- write zeros and READ CH3 DATA(7 downto 0)
			-- ain3 conversion and read done
			
			(others=>'0');
			
	AD7734_data_wr <= index_mux(13 downto 6);
	AD7734_data_en <= '1' when data_wr_index_en = '1' and data_wr_index < 37 else '0';
	
	ff2_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				wait_rdy <= '0';
				channel <= (others=>'0');
				rd_data <= (others=>'0');
			elsif data_wr_index_en = '1' then
				wait_rdy <= index_mux(5);
				channel <= index_mux(4 downto 3);
				rd_data <= index_mux(2 downto 0);
			end if;
		end if;
	end process;
	
	
	
	------------------------------------------------------
	-- Start conversion bit
	-- used to enable the data index counter
	------------------------------------------------------
	
	start_reg_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' or data_wr_index_done = '1' then
				cnv_start_reg <= '0';
			elsif cnv_start = '1' then
				cnv_start_reg <= cnv_start;
			end if;
		end if;
	end process;
	
	busy <= cnv_start_reg;
	cs_n <= not cnv_start_reg;
	
	------------------------------------------------------
	-- Data index counter
	------------------------------------------------------
	
	data_i_cnt_p: process(sys_clk)
	begin
		if rising_edge (sys_clk) then
			if rst_sys_clk_n = '0' or data_wr_index_done = '1' then
				data_wr_index <= 0;
			elsif data_wr_index_en = '1' then
				data_wr_index <= data_wr_index + 1;
			end if;
			rdy_n_sync <= rdy_n;
		end if;
	end process;
	
	-- signal to stop the counter waiting for the conversion to be finished
	wait_for_conversion <= '1' when wait_rdy = '1' and rdy_n_sync = '1' else '0';
	-- counter enable signal
	data_wr_index_en <= '1' when cnv_start_reg = '1' and wait_for_conversion = '0' and spi_master_busy = '0' else '0';
	-- end of full cycle - all data is ready
	data_wr_index_done <= '1' when data_wr_index = 38 else '0';
	-- inform that all data is ready
	data_rdy <= data_wr_index_done;
	
	
	------------------------------------------------------
	-- SPI master done signal
	------------------------------------------------------
	
	ff1_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				spi_master_busy_d1 <= '0';
			else
				spi_master_busy_d1 <= spi_master_busy;
			end if;
		end if;
	end process;
	
	-- done when busy falling edge
	spi_master_done <= spi_master_busy_d1 and not spi_master_busy;

	------------------------------------------------------
	-- Data registers
	------------------------------------------------------
	
	d0_reg_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				data_0_reg <= (others=>'0');
			elsif spi_master_done = '1' and channel = "00" and rd_data(2) = '1' then
				data_0_reg(23 downto 16) <= AD7734_data_rd;
			elsif spi_master_done = '1' and channel = "00" and rd_data(1) = '1' then
				data_0_reg(15 downto 8) <= AD7734_data_rd;
			elsif spi_master_done = '1' and channel = "00" and rd_data(0) = '1' then
				data_0_reg(7 downto 0) <= AD7734_data_rd;
			end if;
		end if;
	end process;
	
	d1_reg_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				data_1_reg <= (others=>'0');
			elsif spi_master_done = '1' and channel = "01" and rd_data(2) = '1' then
				data_1_reg(23 downto 16) <= AD7734_data_rd;
			elsif spi_master_done = '1' and channel = "01" and rd_data(1) = '1' then
				data_1_reg(15 downto 8) <= AD7734_data_rd;
			elsif spi_master_done = '1' and channel = "01" and rd_data(0) = '1' then
				data_1_reg(7 downto 0) <= AD7734_data_rd;
			end if;
		end if;
	end process;
	
	d2_reg_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				data_2_reg <= (others=>'0');
			elsif spi_master_done = '1' and channel = "10" and rd_data(2) = '1' then
				data_2_reg(23 downto 16) <= AD7734_data_rd;
			elsif spi_master_done = '1' and channel = "10" and rd_data(1) = '1' then
				data_2_reg(15 downto 8) <= AD7734_data_rd;
			elsif spi_master_done = '1' and channel = "10" and rd_data(0) = '1' then
				data_2_reg(7 downto 0) <= AD7734_data_rd;
			end if;
		end if;
	end process;
	
	d3_reg_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				data_3_reg <= (others=>'0');
			elsif spi_master_done = '1' and channel = "11" and rd_data(2) = '1' then
				data_3_reg(23 downto 16) <= AD7734_data_rd;
			elsif spi_master_done = '1' and channel = "11" and rd_data(1) = '1' then
				data_3_reg(15 downto 8) <= AD7734_data_rd;
			elsif spi_master_done = '1' and channel = "11" and rd_data(0) = '1' then
				data_3_reg(7 downto 0) <= AD7734_data_rd;
			end if;
		end if;
	end process;
	
	data_0 <= data_0_reg;
	data_1 <= data_1_reg;
	data_2 <= data_2_reg;
	data_3 <= data_3_reg;
	
end architecture rtl;
