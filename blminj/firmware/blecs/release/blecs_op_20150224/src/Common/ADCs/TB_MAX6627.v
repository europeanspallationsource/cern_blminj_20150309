
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_MAX6627;

reg clk;
reg nrst;

reg cnv_start;
reg din;

wire [12:0] data;
wire data_rdy;
wire busy;
wire sclk;
wire cs_n;


MAX6627 #(
	.sys_clk_freq(40000000),
	.spi_clk_freq(1000000)
) uut_MAX6627(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.cnv_start(cnv_start),
	.data(data),
	.data_rdy(data_rdy),
	.busy(busy),
	.sclk(sclk),
	.din(din),
	.cs_n(cs_n)
);




//40mhz clk
always
begin
	#12 clk = 1'b1;
	#13 clk = 1'b0;
end




initial
begin
	clk = 1'b0;
	cnv_start = 1'b0;
	din = 1'b1;
	
	nrst = 1'b0;
	#50 nrst = 1'b1;
	
	//start conversion
	#50 cnv_start = 1'b1;
	#25 cnv_start = 1'b0;
	
	@(negedge busy);
    
    #5000 cnv_start = 1'b1;
    @(negedge busy);
    
    #40000 din = 1'b0;
	
	#60000	$stop;

end


	


endmodule
