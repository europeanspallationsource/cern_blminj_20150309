------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    03/06/2014
--Module Name:    AD7327
--Project Name:   AD7327
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity AD7327 is
generic(
	sys_clk_freq:			natural := 40000000;
	spi_clk_freq:			natural := 1000000
);
port(
	-- Input Clock
	sys_clk:		in std_logic;
	rst_sys_clk_n:	in std_logic;
	
	-- ADC control signals
	cnv_start :		in std_logic;
	data_0	:		out std_logic_vector(12 downto 0);
	data_1	:		out std_logic_vector(12 downto 0);
	data_2	:		out std_logic_vector(12 downto 0);
	data_3	:		out std_logic_vector(12 downto 0);
	data_4	:		out std_logic_vector(12 downto 0);
	data_5	:		out std_logic_vector(12 downto 0);
	data_6	:		out std_logic_vector(12 downto 0);
	data_7	:		out std_logic_vector(12 downto 0);
	data_rdy :		out std_logic;
	busy:			out std_logic;
	
	--SPI signals
	sclk:			out std_logic;
	dout:			out std_logic;
	din:			in std_logic;
	cs_n:			out std_logic
);
	
end entity AD7327;


architecture rtl of AD7327 is

attribute keep: boolean;


signal AD7327_data_wr : std_logic_vector(15 downto 0);
signal AD7327_data_rd : std_logic_vector(15 downto 0);
signal AD7327_data_en : std_logic;

signal data_wr_index : natural range 0 to 14;
signal data_wr_index_done : std_logic;
signal data_wr_index_en : std_logic;
signal cnv_start_reg : std_logic;
signal spi_master_busy : std_logic;
signal spi_master_busy_d1 : std_logic;
signal spi_master_done : std_logic;
signal rd_data : std_logic;

BEGIN

	-- asserts
	assert spi_clk_freq < 10000000 report "Maximum clock frequency of the AD7327 is 10 MHz" severity failure;
	
	------------------------------------------------------
	-- Generic SPI master controller instance
	------------------------------------------------------
	SPI_master_i: entity work.SPI_master
    generic map
    (
        sys_clk_freq	=> sys_clk_freq,
		spi_clk_freq	=> spi_clk_freq,
		spi_bits		=> 16,
		spi_cpol		=> 1,
		spi_cpha		=> 0
    )
    port map
	(
		-- Input Clock
		sys_clk			=> sys_clk,
		rst_sys_clk_n	=> rst_sys_clk_n,
		
		-- SPI master control signals
		data_in			=> AD7327_data_wr,
		data_out		=> AD7327_data_rd,
		start			=> AD7327_data_en,
		busy			=> spi_master_busy,
		
		--SPI signals
		spi_sclk		=> sclk,
		spi_data_out	=> dout,
		spi_data_in		=> din,
		spi_ssel_n		=> cs_n
	);
	
	
	
	---------------------------------------------------------------------------
	-- Select 16 bit data to write to ADC according to current wr index
	---------------------------------------------------------------------------
	
	AD7327_data_wr <= 	
			'1' & "01" & "1100111100000" when data_wr_index = 0 else	-- write range register 1: ch0, ch2, ch3 0 to 10V and ch 1 +-10V
			'1' & "10" & "1111111100000" when data_wr_index = 1 else	-- write range register 2: ch0, ch1, ch2, ch3 0 to 10V
			'1' & "11" & "1111111100000" when data_wr_index = 2 else	-- write sequence register: include all 8 channels in the sequence
			'1' & "00" & "0000000110100" when data_wr_index = 3 else	-- write control register: straight binary coding, internal reference, seq12="01" to start sequence
			'0' & "00" & "0000000000000" when data_wr_index = 4 else	-- write zeros to read data
			'0' & "00" & "0000000000000" when data_wr_index = 5 else	-- write zeros to read data
			'0' & "00" & "0000000000000" when data_wr_index = 6 else	-- write zeros to read data
			'0' & "00" & "0000000000000" when data_wr_index = 7 else	-- write zeros to read data
			'0' & "00" & "0000000000000" when data_wr_index = 8 else	-- write zeros to read data
			'0' & "00" & "0000000000000" when data_wr_index = 9 else	-- write zeros to read data
			'0' & "00" & "0000000000000" when data_wr_index = 10 else	-- write zeros to read data
			'0' & "00" & "0000000000000" when data_wr_index = 11 else	-- write zeros to read data
			'1' & "00" & "0000000110000" when data_wr_index = 12 else	-- write control register: straight binary coding, internal reference, seq12="00" to stop sequence
			(others=>'0');
	
	ff2_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				rd_data <= '0';
			elsif data_wr_index_en = '1' then
				rd_data <= not AD7327_data_wr(15);
			end if;
		end if;
	end process;
	
	
	------------------------------------------------------
	-- Start conversion bit
	-- used to enable the data index counter
	------------------------------------------------------
	
	start_reg_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' or data_wr_index_done = '1' then
				cnv_start_reg <= '0';
			elsif cnv_start = '1' then
				cnv_start_reg <= cnv_start;
			end if;
		end if;
	end process;
	
	busy <= cnv_start_reg;
	
	------------------------------------------------------
	-- Data index counter
	------------------------------------------------------
	
	data_i_cnt_p: process(sys_clk)
	begin
		if rising_edge (sys_clk) then
			if rst_sys_clk_n = '0' or data_wr_index_done = '1' then
				data_wr_index <= 0;
			elsif data_wr_index_en = '1' then
				data_wr_index <= data_wr_index + 1;
			end if;
		end if;
	end process;
	
	-- counter enable signal
	data_wr_index_en <= '1' when cnv_start_reg = '1' and spi_master_busy = '0' else '0';
	-- end of full cycle - all data is ready
	data_wr_index_done <= '1' when data_wr_index = 14 else '0';
	-- inform that all data is ready
	data_rdy <= data_wr_index_done;
	-- start SPI master
	AD7327_data_en <= '1' when data_wr_index_en = '1' and data_wr_index < 13 else '0';
	
	
	------------------------------------------------------
	-- SPI master done signal
	------------------------------------------------------
	
	ff1_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				spi_master_busy_d1 <= '0';
			else
				spi_master_busy_d1 <= spi_master_busy;
			end if;
		end if;
	end process;
	
	-- done when busy falling edge
	spi_master_done <= spi_master_busy_d1 and not spi_master_busy;

	------------------------------------------------------
	-- Data registers
	------------------------------------------------------
	
	data_regs_p: process(sys_clk)
	begin
		if rising_edge(sys_clk) then
			if rst_sys_clk_n = '0' then
				data_0 <= (others=>'0');
				data_1 <= (others=>'0');
				data_2 <= (others=>'0');
				data_3 <= (others=>'0');
				data_4 <= (others=>'0');
				data_5 <= (others=>'0');
				data_6 <= (others=>'0');
				data_7 <= (others=>'0');
			elsif spi_master_done = '1' and rd_data = '1' then
				if AD7327_data_rd(15 downto 13) = "000" then
					data_0 <= AD7327_data_rd(12 downto 0);
				elsif AD7327_data_rd(15 downto 13) = "001" then
					data_1 <= AD7327_data_rd(12 downto 0);
				elsif AD7327_data_rd(15 downto 13) = "010" then
					data_2 <= AD7327_data_rd(12 downto 0);
				elsif AD7327_data_rd(15 downto 13) = "011" then
					data_3 <= AD7327_data_rd(12 downto 0);
				elsif AD7327_data_rd(15 downto 13) = "100" then
					data_4 <= AD7327_data_rd(12 downto 0);
				elsif AD7327_data_rd(15 downto 13) = "101" then
					data_5 <= AD7327_data_rd(12 downto 0);
				elsif AD7327_data_rd(15 downto 13) = "110" then
					data_6 <= AD7327_data_rd(12 downto 0);
				elsif AD7327_data_rd(15 downto 13) = "111" then
					data_7 <= AD7327_data_rd(12 downto 0);
				end if;
			end if;
		end if;
	end process;
	
end architecture rtl;
