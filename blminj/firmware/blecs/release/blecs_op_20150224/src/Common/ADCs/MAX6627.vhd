------------------------------
--
--Company:          CERN - BE/BI/BL
--Engineer:         Maciej Kwiatkowski
--Create Date:      10/06/2014
--Module Name:      MAX6627
--Project Name:     MAX6627
--Description:      Temperature sensor with SPI interface
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity MAX6627 is
generic(
    sys_clk_freq:   natural := 40000000;
    spi_clk_freq:   natural := 1000000
);
port(
    -- Input Clock
    sys_clk:        in std_logic;
    rst_sys_clk_n:  in std_logic;
    
    -- ADC control signals
    cnv_start :     in std_logic;
    data    :       out std_logic_vector(12 downto 0);
    data_rdy :      out std_logic;
    busy:           out std_logic;
    
    --SPI signals
    sclk:           out std_logic;
    din:            in std_logic;
    cs_n:           out std_logic
);
    
end entity MAX6627;


architecture rtl of MAX6627 is

attribute keep: boolean;


signal MAX6627_data : std_logic_vector(15 downto 0);
signal busy_d0 : std_logic;
signal busy_d1 : std_logic;


BEGIN

    -- asserts
    assert spi_clk_freq < 5000000 report "Maximum clock frequency of the MAX6627 is 5 MHz" severity failure;
    
    ------------------------------------------------------
    -- Generic SPI master controller instance
    ------------------------------------------------------
    SPI_master_i: entity work.SPI_master
    generic map
    (
        sys_clk_freq    => sys_clk_freq,
        spi_clk_freq    => spi_clk_freq,
        spi_bits        => 16,
        spi_cpol        => 0,
        spi_cpha        => 0
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- SPI master control signals
        data_in         => x"0000",
        data_out        => MAX6627_data,
        start           => cnv_start,
        busy            => busy_d0,
        
        --SPI signals
        spi_sclk        => sclk,
        spi_data_out    => open,
        spi_data_in     => din,
        spi_ssel_n      => cs_n
    );
    
    busy <= busy_d0;
    
    -- detect falling edge of the busy signal and use it as the data strobe
    ff_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if rst_sys_clk_n = '0' then
				busy_d1 <= '0';
			else
				busy_d1 <= busy_d0;
			end if;
		end if;
	end process;
	data_rdy <= busy_d1 and not busy_d0;
    
    
    -- select and register temperature data bits
    reg_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
            if rst_sys_clk_n = '0' then
                data <= (others=>'0');
            elsif busy_d1 = '1' and busy_d0 = '0' then
                data <= MAX6627_data(15 downto 3);
            end if;
        end if;
	end process;
    
    
end architecture rtl;
