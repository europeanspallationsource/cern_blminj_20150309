
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_AD7734;

reg clk;
reg nrst;

reg cnv_start;
reg din;
reg rdy_n;

wire [23:0] data_0;
wire [23:0] data_1;
wire [23:0] data_2;
wire [23:0] data_3;
wire data_rdy;
wire busy;
wire sclk;
wire dout;
wire cs_n;


AD7734 #(
	.sys_clk_freq(40000000),
	.spi_clk_freq(1000000)
) uut_AD7734(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.cnv_start(cnv_start),
	.data_0(data_0),
	.data_1(data_1),
	.data_2(data_2),
	.data_3(data_3),
	.data_rdy(data_rdy),
	.busy(busy),
	.sclk(sclk),
	.dout(dout),
	.din(din),
	.cs_n(cs_n),
	.rdy_n(rdy_n)
);




//40mhz clk
always
begin
	#12 clk = 1'b1;
	#13 clk = 1'b0;
end




initial
begin
	clk = 1'b0;
	rdy_n = 1'b0;
	cnv_start = 1'b0;
	din = 1'b1;
	
	nrst = 1'b0;
	#50 nrst = 1'b1;
	
	//start conversion
	#50 cnv_start = 1'b1;
	#25 cnv_start = 1'b0;
	
	//simulate ADC rdy_n pin for each channel
	#127362 rdy_n = 0'b1;
	#10000 rdy_n = 0'b0;
	
	#49175 rdy_n = 0'b1;
	#10000 rdy_n = 0'b0;
	
	#49175 rdy_n = 0'b1;
	#10000 rdy_n = 0'b0;
	
	#49175 rdy_n = 0'b1;
	#10000 rdy_n = 0'b0;
	
	@(negedge busy);
	
	#100000	$stop;

end


	


endmodule
