------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    15/05/2014
--Module Name:    High_voltage
--Project Name:   High_voltage
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use work.sine_package.all;

entity High_voltage is
generic(
    sys_clk_freq:           natural := 40000000
);
port(
    -- Input Clock
    sys_clk:                in std_logic;
    rst_sys_clk_n:          in std_logic;
    
    -- VME registers signals
    hv1_volt_value_rd:      out std_logic_vector(31 downto 0);
    hv2_volt_value_rd:      out std_logic_vector(31 downto 0);
    hv1_volt_max_rd:        out std_logic_vector(31 downto 0);
    hv2_volt_max_rd:        out std_logic_vector(31 downto 0);
    hv1_volt_min_rd:        out std_logic_vector(31 downto 0);
    hv2_volt_min_rd:        out std_logic_vector(31 downto 0);
    hv1_curr_value_rd:      out std_logic_vector(31 downto 0);
    hv2_curr_value_rd:      out std_logic_vector(31 downto 0);
    hv1_curr_max_rd:        out std_logic_vector(31 downto 0);
    hv2_curr_max_rd:        out std_logic_vector(31 downto 0);
    hv1_curr_min_rd:        out std_logic_vector(31 downto 0);
    hv2_curr_min_rd:        out std_logic_vector(31 downto 0);
    hv_comparators_rd:      out std_logic_vector(31 downto 0);
    hv1_volt_high_cnt_rd:   out std_logic_vector(31 downto 0);
    hv1_volt_low_cnt_rd:    out std_logic_vector(31 downto 0);
    hv1_curr_high_cnt_rd:   out std_logic_vector(31 downto 0);
    hv1_curr_low_cnt_rd:    out std_logic_vector(31 downto 0);
    hv2_volt_high_cnt_rd:   out std_logic_vector(31 downto 0);
    hv2_volt_low_cnt_rd:    out std_logic_vector(31 downto 0);
    hv2_curr_high_cnt_rd:   out std_logic_vector(31 downto 0);
    hv2_curr_low_cnt_rd:    out std_logic_vector(31 downto 0);
    hv1_dc_level_wr:        in std_logic_vector(31 downto 0);
    hv1_dc_level_wr_en:     in std_logic;
    hv1_dc_level_rd:        out std_logic_vector(31 downto 0);
    hv1_ac_amplitude_wr:    in std_logic_vector(31 downto 0);
    hv1_ac_amplitude_wr_en: in std_logic;
    hv1_ac_amplitude_rd:    out std_logic_vector(31 downto 0);
    hv1_ac_gain_p1_wr:      in std_logic_vector(31 downto 0);
    hv1_ac_gain_p1_wr_en:   in std_logic;
    hv1_ac_gain_p1_rd:      out std_logic_vector(31 downto 0);
    hv1_ac_gain_p2_wr:      in std_logic_vector(31 downto 0);
    hv1_ac_gain_p2_wr_en:   in std_logic;
    hv1_ac_gain_p2_rd:      out std_logic_vector(31 downto 0);
    hv2_dc_level_wr:        in std_logic_vector(31 downto 0);
    hv2_dc_level_wr_en:     in std_logic;
    hv2_dc_level_rd:        out std_logic_vector(31 downto 0);
    hv2_ac_amplitude_wr:    in std_logic_vector(31 downto 0);
    hv2_ac_amplitude_wr_en: in std_logic;
    hv2_ac_amplitude_rd:    out std_logic_vector(31 downto 0);
    hv2_ac_gain_p1_wr:      in std_logic_vector(31 downto 0);
    hv2_ac_gain_p1_wr_en:   in std_logic;
    hv2_ac_gain_p1_rd:      out std_logic_vector(31 downto 0);
    hv2_ac_gain_p2_wr:      in std_logic_vector(31 downto 0);
    hv2_ac_gain_p2_wr_en:   in std_logic;
    hv2_ac_gain_p2_rd:      out std_logic_vector(31 downto 0);
    hv_ac_frequency_wr:     in std_logic_vector(31 downto 0);
    hv_ac_frequency_wr_en:  in std_logic;
    hv_ac_frequency_rd:     out std_logic_vector(31 downto 0);
    
    -- Start of basic period pulse
    start_of_bp:            in std_logic;
    
    -- High voltage and current comparators
    P2_IMon_1_higher:       in std_logic;
    P2_IMon_1_lower:        in std_logic;
    P2_IMon_2_higher:       in std_logic;
    P2_IMon_2_lower:        in std_logic;
    P2_VMon_1_higher:       in std_logic;
    P2_VMon_1_lower:        in std_logic;
    P2_VMon_2_higher:       in std_logic;
    P2_VMon_2_lower:        in std_logic;
    
    -- AD7734 SPI signals
    AD7734_sclk:        out std_logic;
    AD7734_dout:        out std_logic;
    AD7734_din:         in std_logic;
    AD7734_cs_n:        out std_logic;
    
    -- AD7734 data ready pin
    AD7734_rdy_n:       in std_logic;
    
    -- Two DACs (DAC8532) SPI signals
    DAC8532_DIN:        out std_logic;
    DAC8532_SCLK:       out std_logic;
    DAC8532_nSYNC:      out std_logic;
    DAC8532_nSYNC_2:    out std_logic

);
    
end entity High_voltage;


architecture rtl of High_voltage is

attribute keep: boolean;

-- VME registers signals
signal hv1_volt_min_reg: unsigned(23 downto 0);
signal hv1_volt_max_reg: unsigned(23 downto 0);
signal hv2_volt_min_reg: unsigned(23 downto 0);
signal hv2_volt_max_reg: unsigned(23 downto 0);
signal hv1_curr_min_reg: unsigned(23 downto 0);
signal hv1_curr_max_reg: unsigned(23 downto 0);
signal hv2_curr_min_reg: unsigned(23 downto 0);
signal hv2_curr_max_reg: unsigned(23 downto 0);
signal hv1_dc_level_reg: std_logic_vector(15 downto 0);
signal hv1_ac_amplitude_reg: std_logic_vector(7 downto 0);
signal hv1_ac_gain_p1_reg: std_logic_vector(7 downto 0);
signal hv1_ac_gain_p2_reg: std_logic_vector(7 downto 0);
signal hv2_dc_level_reg: std_logic_vector(15 downto 0);
signal hv2_ac_amplitude_reg: std_logic_vector(7 downto 0);
signal hv2_ac_gain_p1_reg: std_logic_vector(7 downto 0);
signal hv2_ac_gain_p2_reg: std_logic_vector(7 downto 0);
signal hv_ac_frequency_reg: std_logic_vector(31 downto 0);

-- ADC signals
signal data_0: std_logic_vector(23 downto 0);
signal data_1: std_logic_vector(23 downto 0);
signal data_2: std_logic_vector(23 downto 0);
signal data_3: std_logic_vector(23 downto 0);
signal data_rdy: std_logic;

-- DAC signals
signal DAC8532_1_SCLK: std_logic;
signal DAC8532_2_SCLK: std_logic;
signal DAC8532_1_DIN: std_logic;
signal DAC8532_2_DIN: std_logic;
signal dac1_a_set: std_logic;
signal dac1_b_set: std_logic;
signal dac2_a_set: std_logic;
signal dac2_b_set: std_logic;
signal dac1_a_set_d1: std_logic;
signal dac1_b_set_d1: std_logic;
signal dac2_a_set_d1: std_logic;
signal dac2_b_set_d1: std_logic;
signal dac1_a_set_en: std_logic;
signal dac1_b_set_en: std_logic;
signal dac2_a_set_en: std_logic;
signal dac2_b_set_en: std_logic;
signal dac1_busy: std_logic;
signal dac2_busy: std_logic;
signal dac1_busy_d1: std_logic;
signal dac2_busy_d1: std_logic;
signal dac1_busy_en: std_logic;
signal dac2_busy_en: std_logic;

-- DACs driving FSM (HV modulation and HV level writes)
type STATE_TYPE IS (hv1_mod, hv2_mod, hv1_level, hv2_level, wait_completed);
signal state, nextState : STATE_TYPE;
signal new_hv_level : std_logic;
signal new_hv_set : std_logic;

-- sine wave generatos signals
signal reset : std_logic;
signal hv1_modulation: std_logic_vector(15 downto 0);
signal hv2_modulation: std_logic_vector(15 downto 0);
signal sine_wave_d: std_logic_vector(7 downto 0);
signal sine_wave_en: std_logic;
signal hv_ac_frequency_cnt: unsigned(31 downto 0);
-- sine wave look-up table has 128 rows containing sin(0) to sin(pi/2)
-- for this reason to get 1Hz need to count up to (sys_clk_freq/512 - 1) in between samples
constant hv_ac_frequency_1hz: unsigned(31 downto 0) := to_unsigned(sys_clk_freq/512 - 1, 32); 

BEGIN

    ------------------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------------------
    -- High Voltage Control
    ------------------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------------------
    
    -- VME registers implementation
    vme_regs_p: process (sys_clk)
    begin
        if rising_edge (sys_clk) then
            if rst_sys_clk_n = '0' then
                hv1_dc_level_reg <= (others=>'0');
            elsif hv1_dc_level_wr_en = '1' then
                hv1_dc_level_reg <= hv1_dc_level_wr(15 downto 0);
            end if;
            if rst_sys_clk_n = '0' then
                hv1_ac_amplitude_reg <= (others=>'0');
            elsif hv1_ac_amplitude_wr_en = '1' then
                hv1_ac_amplitude_reg <= hv1_ac_amplitude_wr(7 downto 0);
            end if;
            if rst_sys_clk_n = '0' then
                hv1_ac_gain_p1_reg <= (others=>'0');
            elsif hv1_ac_gain_p1_wr_en = '1' then
                hv1_ac_gain_p1_reg <= hv1_ac_gain_p1_wr(7 downto 0);
            end if;
            if rst_sys_clk_n = '0' then
                hv1_ac_gain_p2_reg <= (others=>'0');
            elsif hv1_ac_gain_p2_wr_en = '1' then
                hv1_ac_gain_p2_reg <= hv1_ac_gain_p2_wr(7 downto 0);
            end if;
            if rst_sys_clk_n = '0' then
                hv2_dc_level_reg <= (others=>'0');
            elsif hv2_dc_level_wr_en = '1' then
                hv2_dc_level_reg <= hv2_dc_level_wr(15 downto 0);
            end if;
            if rst_sys_clk_n = '0' then
                hv2_ac_amplitude_reg <= (others=>'0');
            elsif hv2_ac_amplitude_wr_en = '1' then
                hv2_ac_amplitude_reg <= hv2_ac_amplitude_wr(7 downto 0);
            end if;
            if rst_sys_clk_n = '0' then
                hv2_ac_gain_p1_reg <= (others=>'0');
            elsif hv2_ac_gain_p1_wr_en = '1' then
                hv2_ac_gain_p1_reg <= hv2_ac_gain_p1_wr(7 downto 0);
            end if;
            if rst_sys_clk_n = '0' then
                hv2_ac_gain_p2_reg <= (others=>'0');
            elsif hv2_ac_gain_p2_wr_en = '1' then
                hv2_ac_gain_p2_reg <= hv2_ac_gain_p2_wr(7 downto 0);
            end if;
            if rst_sys_clk_n = '0' then
                hv_ac_frequency_reg <= std_logic_vector(hv_ac_frequency_1hz);
            elsif hv_ac_frequency_wr_en = '1' then
                hv_ac_frequency_reg <= hv_ac_frequency_wr;
            end if;
        end if;
    end process;
    hv1_dc_level_rd <= x"0000" & hv1_dc_level_reg;
    hv1_ac_amplitude_rd <= x"000000" & hv1_ac_amplitude_reg;
    hv1_ac_gain_p1_rd <= x"000000" & hv1_ac_gain_p1_reg;
    hv1_ac_gain_p2_rd <= x"000000" & hv1_ac_gain_p2_reg;
    hv2_dc_level_rd <= x"0000" & hv2_dc_level_reg;
    hv2_ac_amplitude_rd <= x"000000" & hv2_ac_amplitude_reg;
    hv2_ac_gain_p1_rd <= x"000000" & hv2_ac_gain_p1_reg;
    hv2_ac_gain_p2_rd <= x"000000" & hv2_ac_gain_p2_reg;
    hv_ac_frequency_rd <= hv_ac_frequency_reg;
    
    ------------------------------------------------------
    -- First DAC (DAC8532) instance
    ------------------------------------------------------
    DAC8532_1_i: entity work.DAC8532
    generic map
    (
        sys_clk_freq    => sys_clk_freq,
        spi_clk_freq    => 1000000
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- DAC control signals
        dac_a           => hv1_dc_level_reg,
        pwr_down_a      => "00",
        
        dac_b           => hv1_modulation,
        pwr_down_b      => "00",
        
        dac_a_set       => dac1_a_set_en,
        dac_b_set       => dac1_b_set_en,
        dac_ab_set      => '0',
        
        busy            => dac1_busy,
        
        --SPI signals
        sclk        => DAC8532_1_SCLK,
        data        => DAC8532_1_DIN,
        sync_n      => DAC8532_nSYNC
    );
    
    ------------------------------------------------------
    -- Second DAC (DAC8532) instance
    ------------------------------------------------------
    DAC8532_2_i: entity work.DAC8532
    generic map
    (
        sys_clk_freq    => sys_clk_freq,
        spi_clk_freq    => 1000000
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- DAC control signals
        dac_a           => hv2_dc_level_reg,
        pwr_down_a      => "00",
        
        dac_b           => hv2_modulation,
        pwr_down_b      => "00",
        
        dac_a_set       => dac2_a_set_en,
        dac_b_set       => dac2_b_set_en,
        dac_ab_set      => '0',
        
        busy            => dac2_busy,
        
        --SPI signals
        sclk        => DAC8532_2_SCLK,
        data        => DAC8532_2_DIN,
        sync_n      => DAC8532_nSYNC_2
    );
    
    -- connect signals from two DACs instances to common FPGA lines
    -- DAC1 is has higher priority
    DAC8532_SCLK <= DAC8532_1_SCLK when dac1_busy = '1' else DAC8532_2_SCLK;
    DAC8532_DIN <= DAC8532_1_DIN when dac1_busy = '1' else DAC8532_2_DIN;
    
    ------------------------------------------------------
    -- DACs driving FSM writes HV modulation and HV level
    -- HV modulation samples are writen with maximum sampling frequency to two DACs (~20kHz)
    -- HV levels are written every basic period when there is a new value
    ------------------------------------------------------
    
    fsm_reg_p: process(sys_clk, rst_sys_clk_n)
    begin
        if rst_sys_clk_n = '0' then
            state <= hv1_level;        -- the FSM starts by writing default HV levels
        elsif rising_edge(sys_clk) then
            state <= nextState;
        end if;
    end process;
    
    
    fsm_cl_p: process(state, dac1_busy, dac2_busy, dac1_busy_en, dac2_busy_en, new_hv_level, start_of_bp)
    begin
    
    nextState <= state;    
    dac1_a_set <= '0';
    dac1_b_set <= '0';
    dac2_a_set <= '0';
    dac2_b_set <= '0';
    new_hv_set <= '0';
             
    case state is
    
        when hv1_level =>
            dac1_a_set <= '1';
            new_hv_set <= '1';
            if dac1_busy_en = '1' then
                nextState <= hv2_level;
            end if;
        
        when hv2_level =>
            dac2_a_set <= '1';
            if dac2_busy_en = '1' then
                nextState <= hv1_mod;
            end if;
            
        when hv1_mod =>
            dac1_b_set <= '1';
            if new_hv_level = '1' and start_of_bp = '1' then 
                nextState <= wait_completed;
            elsif dac1_busy_en = '1' then
                nextState <= hv2_mod;
            end if;
            
        when hv2_mod =>
            dac2_b_set <= '1';
            if new_hv_level = '1' and start_of_bp = '1' then 
                nextState <= wait_completed;
            elsif dac2_busy_en = '1' then
                nextState <= hv1_mod;
            end if;
            
        when wait_completed =>
            if dac1_busy = '0' and dac2_busy = '0' then 
                nextState <= hv1_level;
            end if;
        
        when others =>
            nextState <= hv1_level;
        
        end case;
    end process;
    
    -- new HV level flag
    new_hv_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' or (new_hv_set = '1' and dac1_a_set_en = '1') then
                new_hv_level <= '0';
            elsif hv1_dc_level_wr_en = '1' or hv2_dc_level_wr_en = '1' then
                new_hv_level <= hv1_dc_level_wr_en or hv2_dc_level_wr_en;
            end if;
        end if;
    end process;
    
    
    -- detect falling edges of busy flags
    -- detect rising edges of dac enable signals
    busy_ff_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' then
                dac1_busy_d1 <= '0';
                dac2_busy_d1 <= '0';
                dac1_a_set_d1 <= '0';
                dac1_b_set_d1 <= '0';
                dac2_a_set_d1 <= '0';
                dac2_b_set_d1 <= '0';
            else
                dac1_busy_d1 <= dac1_busy;
                dac2_busy_d1 <= dac2_busy;
                dac1_a_set_d1 <= dac1_a_set;
                dac1_b_set_d1 <= dac1_b_set;
                dac2_a_set_d1 <= dac2_a_set;
                dac2_b_set_d1 <= dac2_b_set;
            end if;
        end if;
    end process;
    dac1_busy_en <= dac1_busy_d1 and not dac1_busy;
    dac2_busy_en <= dac2_busy_d1 and not dac2_busy;
    dac1_a_set_en <= dac1_a_set and not dac1_a_set_d1;
    dac1_b_set_en <= dac1_b_set and not dac1_b_set_d1;
    dac2_a_set_en <= dac2_a_set and not dac2_a_set_d1;
    dac2_b_set_en <= dac2_b_set and not dac2_b_set_d1;
    
    ------------------------------------------------------
    -- HV modulation sine wave generator
    -- The sine wave is generated independently of the DAC "sampling" frequency
    -- If the sine wave frequency is too high then it will not be reproduced properly
    ------------------------------------------------------
    sine_wave_i: entity work.sine_wave
    port map
    (
        clock       => sys_clk,
        reset       => reset,
        enable      => sine_wave_en,
        wave_out    => sine_wave_d
    );
    
    reset <= not rst_sys_clk_n;
    
    ------------------------------------------------------
    -- HV modulation amplitude 8 bit multipliers
    ------------------------------------------------------
    hv1_mult_8b_8b_i: entity work.mult_8b_8b
    port map
    (
        dataa       => sine_wave_d,
        datab       => hv1_ac_amplitude_reg,
        result      => hv1_modulation
    );
    
    hv2_mult_8b_8b_i: entity work.mult_8b_8b
    port map
    (
        dataa       => sine_wave_d,
        datab       => hv2_ac_amplitude_reg,
        result      => hv2_modulation
    );
    
    
    ------------------------------------------------------
    -- HV modulation frequency counter
    ------------------------------------------------------
    freq_cnt_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' or sine_wave_en = '1' or hv_ac_frequency_wr_en = '1' then
                hv_ac_frequency_cnt <= (others=>'0');
            else
                hv_ac_frequency_cnt <= hv_ac_frequency_cnt + 1;
            end if;
        end if;
    end process;
    
    sine_wave_en <= '1' when hv_ac_frequency_cnt = unsigned(hv_ac_frequency_reg) else '0';

    ------------------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------------------
    -- High Voltage Survey
    ------------------------------------------------------------------------------------------------------------
    ------------------------------------------------------------------------------------------------------------

    ------------------------------------------------------
    -- ADC (AD7734) instance
    ------------------------------------------------------
    AD7734_i: entity work.AD7734
    generic map
    (
        sys_clk_freq    => sys_clk_freq,
        spi_clk_freq    => 1000000
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- ADC control signals
        cnv_start   => '1',
        data_0      => data_0,
        data_1      => data_1,
        data_2      => data_2,
        data_3      => data_3,
        data_rdy    => data_rdy,
        busy        => open,
        
        --SPI signals
        sclk    => AD7734_sclk,
        dout    => AD7734_dout,
        din     => AD7734_din,
        cs_n    => AD7734_cs_n,
        
        -- ADC data ready pin
        rdy_n   => AD7734_rdy_n
    );
    
    ------------------------------------------------------
    -- register voltage and current readouts every basic period
    ------------------------------------------------------
    
    hvi_regs_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if start_of_bp = '1' then
                hv1_volt_value_rd <= x"00" & data_0;
                hv1_curr_value_rd <= x"00" & data_1;
                hv2_volt_value_rd <= x"00" & data_2;
                hv2_curr_value_rd <= x"00" & data_3;
                hv_comparators_rd <= x"000000" & P2_VMon_1_higher & P2_VMon_1_lower & P2_IMon_1_higher & P2_IMon_1_lower & P2_VMon_2_higher & P2_VMon_2_lower & P2_IMon_2_higher & P2_IMon_2_lower;
            end if;
        end if;
    end process;
    
    
    
    ------------------------------------------------------
    -- track min and max readouts in a basic period
    ------------------------------------------------------
    
    hv1_volt_min_max_i: entity work.unsigned_min_max
    generic map
    (
        in_bits     => 24
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- input value to track
        in_value        => unsigned(data_0),
        in_value_en     => data_rdy,
        
        -- period pulse in which min/max is tracked
        period_en       => start_of_bp,
        
        -- min/max found in the period
        max_value       => hv1_volt_max_reg,
        min_value       => hv1_volt_min_reg
    );
    hv1_volt_max_rd <= x"00" & std_logic_vector(hv1_volt_max_reg);
    hv1_volt_min_rd <= x"00" & std_logic_vector(hv1_volt_min_reg);
    
    
    hv2_volt_min_max_i: entity work.unsigned_min_max
    generic map
    (
        in_bits     => 24
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- input value to track
        in_value        => unsigned(data_2),
        in_value_en     => data_rdy,
        
        -- period pulse in which min/max is tracked
        period_en       => start_of_bp,
        
        -- min/max found in the period
        max_value       => hv2_volt_max_reg,
        min_value       => hv2_volt_min_reg
    );
    hv2_volt_max_rd <= x"00" & std_logic_vector(hv2_volt_max_reg);
    hv2_volt_min_rd <= x"00" & std_logic_vector(hv2_volt_min_reg);
    
    hv1_curr_min_max_i: entity work.unsigned_min_max
    generic map
    (
        in_bits     => 24
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- input value to track
        in_value        => unsigned(data_1),
        in_value_en     => data_rdy,
        
        -- period pulse in which min/max is tracked
        period_en       => start_of_bp,
        
        -- min/max found in the period
        max_value       => hv1_curr_max_reg,
        min_value       => hv1_curr_min_reg
    );
    hv1_curr_max_rd <= x"00" & std_logic_vector(hv1_curr_max_reg);
    hv1_curr_min_rd <= x"00" & std_logic_vector(hv1_curr_min_reg);
    
    hv2_curr_min_max_i: entity work.unsigned_min_max
    generic map
    (
        in_bits     => 24
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- input value to track
        in_value        => unsigned(data_3),
        in_value_en     => data_rdy,
        
        -- period pulse in which min/max is tracked
        period_en       => start_of_bp,
        
        -- min/max found in the period
        max_value       => hv2_curr_max_reg,
        min_value       => hv2_curr_min_reg
    );
    hv2_curr_max_rd <= x"00" & std_logic_vector(hv2_curr_max_reg);
    hv2_curr_min_rd <= x"00" & std_logic_vector(hv2_curr_min_reg);
    
    ------------------------------------------------------
    -- count for how many clock cycles the voltages and currents were above and below the threshold
    ------------------------------------------------------
    
    hv1_volt_higher_i: entity work.events_counter
    generic map
    (
        cnt_bits    => 32,
        rising      => false,
        falling     => false,
        high_l      => true,
        low_l       => false
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- event which is counted
        in_event        => P2_VMon_1_higher,
        
        -- period pulse in which events are counted
        period_en       => start_of_bp,
        
        -- events detected in the period
        events_number   => hv1_volt_high_cnt_rd,
        events_ov       => open
    );
    
    hv1_volt_lower_i: entity work.events_counter
    generic map
    (
        cnt_bits    => 32,
        rising      => false,
        falling     => false,
        high_l      => true,
        low_l       => false
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- event which is counted
        in_event        => P2_VMon_1_lower,
        
        -- period pulse in which events are counted
        period_en       => start_of_bp,
        
        -- events detected in the period
        events_number   => hv1_volt_low_cnt_rd,
        events_ov       => open
    );
    
    hv1_curr_higher_i: entity work.events_counter
    generic map
    (
        cnt_bits    => 32,
        rising      => false,
        falling     => false,
        high_l      => true,
        low_l       => false
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- event which is counted
        in_event        => P2_IMon_1_higher,
        
        -- period pulse in which events are counted
        period_en       => start_of_bp,
        
        -- events detected in the period
        events_number   => hv1_curr_high_cnt_rd,
        events_ov       => open
    );
    
    hv1_curr_lower_i: entity work.events_counter
    generic map
    (
        cnt_bits    => 32,
        rising      => false,
        falling     => false,
        high_l      => true,
        low_l       => false
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- event which is counted
        in_event        => P2_IMon_1_lower,
        
        -- period pulse in which events are counted
        period_en       => start_of_bp,
        
        -- events detected in the period
        events_number   => hv1_curr_low_cnt_rd,
        events_ov       => open
    );
     
    hv2_volt_higher_i: entity work.events_counter
    generic map
    (
        cnt_bits    => 32,
        rising      => false,
        falling     => false,
        high_l      => true,
        low_l       => false
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- event which is counted
        in_event        => P2_VMon_2_higher,
        
        -- period pulse in which events are counted
        period_en       => start_of_bp,
        
        -- events detected in the period
        events_number   => hv2_volt_high_cnt_rd,
        events_ov       => open
    );
    
    hv2_volt_lower_i: entity work.events_counter
    generic map
    (
        cnt_bits    => 32,
        rising      => false,
        falling     => false,
        high_l      => true,
        low_l       => false
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- event which is counted
        in_event        => P2_VMon_2_lower,
        
        -- period pulse in which events are counted
        period_en       => start_of_bp,
        
        -- events detected in the period
        events_number   => hv2_volt_low_cnt_rd,
        events_ov       => open
    );
    
    hv2_curr_higher_i: entity work.events_counter
    generic map
    (
        cnt_bits    => 32,
        rising      => false,
        falling     => false,
        high_l      => true,
        low_l       => false
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- event which is counted
        in_event        => P2_IMon_2_higher,
        
        -- period pulse in which events are counted
        period_en       => start_of_bp,
        
        -- events detected in the period
        events_number   => hv2_curr_high_cnt_rd,
        events_ov       => open
    );
    
    hv2_curr_lower_i: entity work.events_counter
    generic map
    (
        cnt_bits    => 32,
        rising      => false,
        falling     => false,
        high_l      => true,
        low_l       => false
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- event which is counted
        in_event        => P2_IMon_2_lower,
        
        -- period pulse in which events are counted
        period_en       => start_of_bp,
        
        -- events detected in the period
        events_number   => hv2_curr_low_cnt_rd,
        events_ov       => open
    );
     
     
    
end architecture rtl;
