
//------------------------------------------------------------------------------
//----------- Module Declaration -----------------------------------------------
//------------------------------------------------------------------------------

`timescale 1 ns / 100 ps

module TB_High_voltage;

reg clk;
reg nrst;

reg start_of_bp;
reg [31:0] hv1_dc_level_wr;
reg [31:0] hv2_dc_level_wr;
reg [31:0] hv_ac_frequency_wr;
reg hv1_dc_level_wr_en;
reg hv2_dc_level_wr_en;
reg hv_ac_frequency_wr_en;


wire DAC8532_DIN;
wire DAC8532_SCLK;
wire DAC8532_nSYNC;
wire DAC8532_nSYNC_2;




High_voltage #(
	.sys_clk_freq(40000000)
) uut_High_voltage(
	.sys_clk(clk),
	.rst_sys_clk_n(nrst),
	.start_of_bp(start_of_bp),
    .hv1_dc_level_wr(hv1_dc_level_wr),
    .hv1_dc_level_wr_en(hv1_dc_level_wr_en),
    .hv2_dc_level_wr(hv2_dc_level_wr),
    .hv2_dc_level_wr_en(hv2_dc_level_wr_en),
    .hv_ac_frequency_wr(hv_ac_frequency_wr),
    .hv_ac_frequency_wr_en(hv_ac_frequency_wr_en),
	.DAC8532_DIN(DAC8532_DIN),
	.DAC8532_SCLK(DAC8532_SCLK),
	.DAC8532_nSYNC(DAC8532_nSYNC),
	.DAC8532_nSYNC_2(DAC8532_nSYNC_2)
);



//40mhz clk
always
begin
	#12 clk = 1'b1;
	#13 clk = 1'b0;
end




initial
begin
	clk = 1'b0;
	start_of_bp = 1'b0;
	hv1_dc_level_wr_en = 1'b0;
	hv2_dc_level_wr_en = 1'b0;
    hv1_dc_level_wr = 32'h00000000;
    hv2_dc_level_wr = 32'h00000000;
    hv_ac_frequency_wr = 32'h00000000;
	
	nrst = 1'b0;
	#50 nrst = 1'b1;
	
	#50 hv_ac_frequency_wr_en = 1'b1;
	#25 hv_ac_frequency_wr_en = 1'b0;
    
	#50 start_of_bp = 1'b1;
	#25 start_of_bp = 1'b0;
    
    
    #1000 hv1_dc_level_wr = 32'h5aa55aa5;
    hv1_dc_level_wr_en = 1'b1;
    #25 hv1_dc_level_wr_en = 1'b0;
    
    
    #5000 hv2_dc_level_wr = 32'h5aa55aa5;
    hv2_dc_level_wr_en = 1'b1;
    #25 hv2_dc_level_wr_en = 1'b0;
    
    #50000 start_of_bp = 1'b1;
	#25 start_of_bp = 1'b0;
    
	
	#1000000	$stop;

end


	


endmodule
