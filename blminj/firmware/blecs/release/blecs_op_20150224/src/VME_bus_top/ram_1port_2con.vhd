------------------------------
/*
Company:       CERN - BE/BI/BL
Engineer:       Marcel Alsdorf
Updated:        15/05/2013
Module Name:    ram_1port_2con


-----------
Description
-----------

    This is the CMI wrapper module for a 1-PORT ALTERA SYNCRAM with 2 CMI ports.


------------------
Generics/Constants
------------------

    ...
    
----------------------------
Necessary Packages/Libraries
----------------------------
    
    vhdl_func_pkg
    
-----------------
Necessary Modules
-----------------

    none
    

---------------
Necessary Cores
---------------

    none 
    
--------------
Implementation
--------------
    
------------------------
Not implemented Features
------------------------

*/
------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;

library altera_mf;
    use altera_mf.altera_mf_components.all;

entity ram_1port_2con is

generic
(
    wrreq_data_size:    integer;
    rdout_data_size:    integer;
    addr_size:          integer;
    be_size:            integer;
    ram_data_size:      integer;
    ram_depth:          integer;
    init_file:          string := "UNUSED";
    ram_type:           string := "AUTO";
    device_family:      string
);

port
(
    -- Clock
    clk:            in  std_logic;
    -- CMI Input
    rdreq_data:     in  std_logic_vector(addr_size-1 downto 0);
    rdreq_vld:      in  std_logic;
    rdreq_next:     out std_logic;
    wrreq_data:     in  std_logic_vector(wrreq_data_size-1 downto 0);
    wrreq_vld:      in  std_logic;
    wrreq_next:     out std_logic;
    
    -- CMI Output
    rdout_data:     out std_logic_vector(rdout_data_size-1 downto 0);
    rdout_vld:      out std_logic;
    rdout_next:     in  std_logic
);

end entity ram_1port_2con;


architecture struct of ram_1port_2con is
    
    --===========--
    -- CONSTANTS --
    --===========--

    constant int_addr_size:     integer := get_vsize_addr(ram_depth);
    
    constant rd_addr_high_bnd:  integer := int_addr_size;
    constant wr_addr_high_bnd:  integer := wrreq_data_size-addr_size+int_addr_size;
    constant wr_be_high_bnd:    integer := wrreq_data_size-addr_size;
    constant wr_data_high_bnd:  integer := wrreq_data_size-addr_size-be_size;
    
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal addr_int:        std_logic_vector(int_addr_size-1 downto 0);
    signal wren_int:        std_logic;
    
    
BEGIN
    
    
    --==================--
    -- CMI OUTREG RDREQ --
    --==================--
    
    process(clk) is
    begin
        if rising_edge(clk) then
            if rdout_next = '1' then
                rdout_vld <= rdreq_vld;
            end if;
        end if;
    end process;
    
    
    --=============--
    -- ARBITRATION --
    --=============--
    
    process(all) is
    begin
    -- defaults
    rdreq_next <= '1';
    wrreq_next <= '1';
    
    
        if wrreq_vld = '1' then
            addr_int    <= wrreq_data(wr_addr_high_bnd-1 downto wr_be_high_bnd);
            wren_int    <= '1';
            rdreq_next  <= NOT rdreq_vld;
        else
            addr_int <= rdreq_data(rd_addr_high_bnd-1 downto 0);
            wren_int <= '0';
        end if;
    end process;
    
    
    
    --===================--
    -- ALTERA 1 PORT RAM --
    --===================--
    
    altsyncram_component : altsyncram
    GENERIC MAP 
    (
        -- header
        operation_mode              => "SINGLE_PORT",
        lpm_type                    => "altsyncram",
        intended_device_family      => device_family,
        ram_block_type              => ram_type,
        init_file                   => init_file,
        outdata_reg_a               => "UNREGISTERED",
        -- sizes
        width_a                     => ram_data_size,
        widthad_a                   => int_addr_size,
        numwords_a                  => ram_depth,
        -- Byte Enable Port A
        width_byteena_a             => be_size,
        byte_size                   => 8,
        byteena_aclr_a              => "NONE",
        -- ACLR
        address_aclr_a              => "NONE",
        indata_aclr_a               => "NONE",
        outdata_aclr_a              => "NONE",
        wrcontrol_aclr_a            => "NONE",
        -- misc
        power_up_uninitialized      => "FALSE"
    )
    PORT MAP 
    (
        clock0      => clk,
        address_a   => addr_int,
        data_a      => wrreq_data(wr_data_high_bnd-1 downto 0),
        q_a         => rdout_data,
        byteena_a   => wrreq_data(wr_be_high_bnd-1 downto wr_data_high_bnd),
        wren_a      => wren_int
    );
    
   
    
end architecture struct;
