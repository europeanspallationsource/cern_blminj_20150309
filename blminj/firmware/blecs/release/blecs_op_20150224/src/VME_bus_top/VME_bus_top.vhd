------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    14/03/2014
--Module Name:    VME_bus_top
--Project Name:   VME_bus_top
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use work.vhdl_func_pkg.all;

entity VME_bus_top is
generic(
	sys_clk_ns_period:		natural
);
port(
	-- Input Clock
	sys_clk:				in std_logic;
	
	
	-- VME Inputs
    VME_GA:                 in  std_logic_vector(4 downto 0);
    VME_GAP:                in  std_logic;
    VME_wrtN:               in  std_logic;
    VME_AM:                 in  std_logic_vector(5 downto 0);
    VME_asN:                in  std_logic;
    VME_ds0N:               in  std_logic;
    VME_ds1N:               in  std_logic;
    VME_iackN:              in  std_logic;
    VME_iackinN:            in  std_logic;
    VME_SYSCLK:             in  std_logic;
    VME_SYSRESETn:          in  std_logic;
    -- VME IOs
    VME_ADD:                inout std_logic_vector(31 downto 1);
    VME_lwordN:             inout std_logic;
    VME_DATA:               inout std_logic_vector(31 downto 0);
    -- VME Outputs
    vme_BERRn:              out std_logic;
    VME_RETRYn:             out std_logic;
    VME_iack_outN:          out std_logic;
    VME_irqN:               out std_logic_vector(3 downto 1);
    -- DAB64 IOs
    Stratix_Irq_VectorN:    out std_logic;
    IntLev_reg:             in  std_logic_vector(2 downto 0);		-- called VME_IRQ_Level in the DAB64x card
    IRQ_Vector_Enable:      in  std_logic;							-- called IRQ_Vec_OEn
    Board_Rst_OnOffN:       in  std_logic;
    FP_VME_ACC_LEDn:        out std_logic;
    Stratix_VME_ACCn:       out std_logic;
    VME_ForceRetry:         out std_logic;  -- activate with RETRY
    VME_2e_Cycle:           out std_logic;  -- use for A[31:1] & LWORDn direction
    VME_DTACK_EN:           inout std_logic;  -- for releasing the DTACK line
    STX_245_oeN:            out std_logic;  -- OE for data lines D[31:0]				called Stratix_245_OEn in the DAB64x card
    STX_dtackN:             out std_logic;  -- VME_DTACKn								called Stratix_dtackN in the DAB64x card
    Stratix_SYSFAILn:       out std_logic;  -- VME_SYSFAILn								renamed from VME_SYSFAILn to Stratix_SYSFAILn
	
	-- Combiner specific VME pin
	VME_data_dir:			out std_logic;
	VME_BR_WRITE2:			out std_logic;
	VME_BR_WRITE3:			out std_logic;
	VME_BBSY_WRITE	:		out std_logic;
	
	--------------------------------------------------------
	-- registers interface to/from the logic
	--------------------------------------------------------
	
	-- status registers to/from the logic
	-- pass through register, VME R
	synth_date_rd:          in std_logic_vector(31 downto 0);
    synth_time_rd:          in std_logic_vector(31 downto 0);
    serial_id_h_rd:         in std_logic_vector(31 downto 0);
    serial_id_l_rd:         in std_logic_vector(31 downto 0);
    status_bits_rd:         in std_logic_vector(31 downto 0);
    temperature_rd:         in std_logic_vector(31 downto 0);
	
	-- timing registers from the logic
	-- pass through registers, VME R
	beam_in_msec_rd:		in std_logic_vector(31 downto 0);
	beam_out_msec_rd:		in std_logic_vector(31 downto 0);
	basic_period_msec_rd:	in std_logic_vector(31 downto 0);
	
	-- interlock registers to/from the logic
	-- pass through registers, VME R
	cibu1_status_rd:		in std_logic_vector(31 downto 0);
	cibu2_status_rd:		in std_logic_vector(31 downto 0);
	hw_ilock1_rd:			in std_logic_vector(31 downto 0);
	hw_ilock2_rd:			in std_logic_vector(31 downto 0);
	sw_ilock1_rd:			in std_logic_vector(31 downto 0);
	sw_ilock2_rd:			in std_logic_vector(31 downto 0);
	current_user_rd:		in std_logic_vector(31 downto 0);
	wdg_timer_rd:			in std_logic_vector(31 downto 0);
	wdg_time_rd:			in std_logic_vector(31 downto 0);
	-- pass through registers, VME W
	comm_hw_ilock1_wr:		out std_logic_vector(31 downto 0);
	comm_hw_ilock1_wr_en:	out std_logic;
	comm_hw_ilock2_wr:		out std_logic_vector(31 downto 0);
	comm_hw_ilock2_wr_en:	out std_logic;
	comm_sw_ilock1_wr:		out std_logic_vector(31 downto 0);
	comm_sw_ilock1_wr_en:	out std_logic;
	comm_sw_ilock2_wr:		out std_logic_vector(31 downto 0);
	comm_sw_ilock2_wr_en:	out std_logic;
	comm_set_user_wr:		out std_logic_vector(31 downto 0);
	comm_set_user_wr_en:	out std_logic;
	comm_set_wdg_time_wr:		out std_logic_vector(31 downto 0);
	comm_set_wdg_time_wr_en:	out std_logic;
	comm_reset_wdg_wr:		out std_logic_vector(31 downto 0);
	comm_reset_wdg_wr_en:	out std_logic;
	-- high voltage registers to/from the logic
	-- pass through registers, VME R
	hv1_volt_value_rd:		in std_logic_vector(31 downto 0);
	hv2_volt_value_rd:		in std_logic_vector(31 downto 0);
	hv1_volt_max_rd:		in std_logic_vector(31 downto 0);
	hv2_volt_max_rd:		in std_logic_vector(31 downto 0);
	hv1_volt_min_rd:		in std_logic_vector(31 downto 0);
	hv2_volt_min_rd:		in std_logic_vector(31 downto 0);
	hv1_curr_value_rd:		in std_logic_vector(31 downto 0);
	hv2_curr_value_rd:		in std_logic_vector(31 downto 0);
	hv1_curr_max_rd:		in std_logic_vector(31 downto 0);
	hv2_curr_max_rd:		in std_logic_vector(31 downto 0);
	hv1_curr_min_rd:		in std_logic_vector(31 downto 0);
	hv2_curr_min_rd:		in std_logic_vector(31 downto 0);
	hv_comparators_rd:		in std_logic_vector(31 downto 0);
    hv1_volt_high_cnt_rd:   in std_logic_vector(31 downto 0);
	hv1_volt_low_cnt_rd:    in std_logic_vector(31 downto 0);
	hv1_curr_high_cnt_rd:   in std_logic_vector(31 downto 0);
	hv1_curr_low_cnt_rd:    in std_logic_vector(31 downto 0);
    hv2_volt_high_cnt_rd:   in std_logic_vector(31 downto 0);
	hv2_volt_low_cnt_rd:    in std_logic_vector(31 downto 0);
	hv2_curr_high_cnt_rd:   in std_logic_vector(31 downto 0);
	hv2_curr_low_cnt_rd:    in std_logic_vector(31 downto 0);
    -- pass through registers, VME RW
    hv1_dc_level_wr:        out std_logic_vector(31 downto 0);
    hv1_dc_level_wr_en:     out std_logic;
    hv1_dc_level_rd:        in std_logic_vector(31 downto 0);
    hv1_ac_amplitude_wr:    out std_logic_vector(31 downto 0);
    hv1_ac_amplitude_wr_en: out std_logic;
    hv1_ac_amplitude_rd:    in std_logic_vector(31 downto 0);
    hv1_ac_gain_p1_wr:      out std_logic_vector(31 downto 0);
    hv1_ac_gain_p1_wr_en:   out std_logic;
    hv1_ac_gain_p1_rd:      in std_logic_vector(31 downto 0);
    hv1_ac_gain_p2_wr:      out std_logic_vector(31 downto 0);
    hv1_ac_gain_p2_wr_en:   out std_logic;
    hv1_ac_gain_p2_rd:      in std_logic_vector(31 downto 0);
    hv2_dc_level_wr:        out std_logic_vector(31 downto 0);
    hv2_dc_level_wr_en:     out std_logic;
    hv2_dc_level_rd:        in std_logic_vector(31 downto 0);
    hv2_ac_amplitude_wr:    out std_logic_vector(31 downto 0);
    hv2_ac_amplitude_wr_en: out std_logic;
    hv2_ac_amplitude_rd:    in std_logic_vector(31 downto 0);
    hv2_ac_gain_p1_wr:      out std_logic_vector(31 downto 0);
    hv2_ac_gain_p1_wr_en:   out std_logic;
    hv2_ac_gain_p1_rd:      in std_logic_vector(31 downto 0);
    hv2_ac_gain_p2_wr:      out std_logic_vector(31 downto 0);
    hv2_ac_gain_p2_wr_en:   out std_logic;
    hv2_ac_gain_p2_rd:      in std_logic_vector(31 downto 0);
    hv_ac_frequency_wr:     out std_logic_vector(31 downto 0);
    hv_ac_frequency_wr_en:  out std_logic;
    hv_ac_frequency_rd:     in std_logic_vector(31 downto 0);
    
	-- low voltage registers to/from the logic
	-- pass through registers, VME R
	vme3v3_value_rd:		in std_logic_vector(31 downto 0);
	vme5v_value_rd:			in std_logic_vector(31 downto 0);
	analog_p15v_value_rd:	in std_logic_vector(31 downto 0);
	analog_n15v_value_rd:	in std_logic_vector(31 downto 0);
	analog_5v_value_rd:		in std_logic_vector(31 downto 0);
	ref_5v_value_rd:		in std_logic_vector(31 downto 0);
	ref_10va_value_rd:		in std_logic_vector(31 downto 0);
	ref_10vb_value_rd:		in std_logic_vector(31 downto 0);
	vme3v3_min_rd:			in std_logic_vector(31 downto 0);
	vme3v3_max_rd:			in std_logic_vector(31 downto 0);
	vme5v_min_rd:			in std_logic_vector(31 downto 0);
	vme5v_max_rd:			in std_logic_vector(31 downto 0);
	analog_p15v_min_rd:		in std_logic_vector(31 downto 0);
	analog_p15v_max_rd:		in std_logic_vector(31 downto 0);
	analog_n15v_min_rd:		in std_logic_vector(31 downto 0);
	analog_n15v_max_rd:		in std_logic_vector(31 downto 0);
	analog_5v_min_rd:		in std_logic_vector(31 downto 0);
	analog_5v_max_rd:		in std_logic_vector(31 downto 0);
	ref_5v_min_rd:			in std_logic_vector(31 downto 0);
	ref_5v_max_rd:			in std_logic_vector(31 downto 0);
	ref_10va_min_rd:		in std_logic_vector(31 downto 0);
	ref_10va_max_rd:		in std_logic_vector(31 downto 0);
	ref_10vb_min_rd:		in std_logic_vector(31 downto 0);
	ref_10vb_max_rd:		in std_logic_vector(31 downto 0);
	lv_comparators_rd:		in std_logic_vector(31 downto 0);
	analog5v_cnt_rd:		in std_logic_vector(31 downto 0);
	analog15v_cnt_rd:		in std_logic_vector(31 downto 0);
	vme3v3_cnt_rd:			in std_logic_vector(31 downto 0);
	vme5v_cnt_rd:			in std_logic_vector(31 downto 0);
	vme12v_cnt_rd:			in std_logic_vector(31 downto 0)

);
	
end entity VME_bus_top;


architecture rtl of VME_bus_top is

attribute keep: boolean;

-- CMI Connectors
signal vmeacc_rdreq_tx_data:    std_logic_vector(60 downto 0);
signal vmeacc_rdreq_tx_vld:     std_logic;
signal vmeacc_rdreq_tx_next:    std_logic;
attribute keep of vmeacc_rdreq_tx_data: signal is true;
attribute keep of vmeacc_rdreq_tx_vld: signal is true;
attribute keep of vmeacc_rdreq_tx_next: signal is true;

signal vmeacc_wrreq_tx_data:    std_logic_vector(132 downto 0);
signal vmeacc_wrreq_tx_vld:     std_logic;
signal vmeacc_wrreq_tx_next:    std_logic;
attribute keep of vmeacc_wrreq_tx_data: signal is true;
attribute keep of vmeacc_wrreq_tx_vld: signal is true;
attribute keep of vmeacc_wrreq_tx_next: signal is true;

signal rdout_rx_data:           std_logic_vector(63 downto 0);
signal rdout_rx_vld:            std_logic;
signal rdout_rx_next:           std_logic;
attribute keep of rdout_rx_data: signal is true;
attribute keep of rdout_rx_vld: signal is true;
attribute keep of rdout_rx_next: signal is true;


-- between VME_Interface and to_DAB64x
signal VME_ACC:             std_logic;
signal VME_DATA_BUF_OEn:    std_logic;
signal VME_ADDR_BUF_OEn:    std_logic;
signal VME_DTACK_BUF_OEn:   std_logic;
signal VME_RETRY_BUF_OEn:   std_logic;
signal VME_BERR_BUF_OEn:    std_logic;
signal VME_DATA_BUF_DIR:    std_logic;
signal VME_ADDR_BUF_DIR:    std_logic;
signal VME_DTACKn:          std_logic;
signal VME_SYSFAILn:        std_logic;
signal VME_irqN_sig:        std_logic_vector(7 downto 1);
signal VME_LWORDn_OUT:      std_logic;
signal VME_DATA_OUT:        std_logic_vector(31 downto 0);
signal VME_ADDR_OUT:        std_logic_vector(31 downto 1);
signal VME_LWORDn_IN:       std_logic;
signal VME_DATA_IN:         std_logic_vector(31 downto 0);
signal VME_ADDR_IN:         std_logic_vector(31 downto 1);

-- Memory mapped bus
signal read_address:        std_logic_vector(23 downto 0);
signal read_data:           std_logic_vector(31 downto 0);
signal read_req:            std_logic;
signal write_address:       std_logic_vector(23 downto 0);
signal write_data:          std_logic_vector(31 downto 0);
signal write_req:           std_logic;

signal ram0_address:        std_logic_vector(13 downto 0);
signal ram0_read_data:      std_logic_vector(31 downto 0);
signal ram0_write_data:     std_logic_vector(31 downto 0);
signal ram0_wr_enable:      std_logic;

BEGIN
	
	--==========--
    -- VME Core --
    --==========--
    
    vmecore: entity work.VME_Core_CMA
    generic map
    (
        Tclk_ns             => real(sys_clk_ns_period),
		rdreq_data_size		=> 61,
		wrreq_data_size		=> 133,
		rdout_data_size		=> 64
    )
    port map
    (   
        clk                 => sys_clk,
        VME_GA              => VME_GA,
        VME_GAP             => VME_GAP,
        VME_WRn             => VME_wrtN,
        VME_AM              => VME_AM,
        VME_ASn             => VME_ASn,
        VME_DS0n            => VME_DS0n,
        VME_DS1n            => VME_DS1n,
        VME_IACKn           => VME_IACKn,
        VME_IACKINn         => VME_IACKINn,
        VME_SYSCLK          => VME_SYSCLK,
        VME_SYSRESETn       => VME_SYSRESETn,
        VME_LWORDn_OUT      => VME_LWORDn_OUT,
        VME_DATA_OUT        => VME_DATA_OUT,
        VME_ADDR_OUT        => VME_ADDR_OUT,
        VME_LWORDn_IN       => VME_LWORDn_IN,
        VME_DATA_IN         => VME_DATA_IN,
        VME_ADDR_IN         => VME_ADDR_IN,
        VME_DTACKn          => VME_DTACKn,
        VME_BERRn           => VME_BERRn,
        VME_RETRYn          => VME_RETRYn,
        VME_IACKOUTn        => VME_iack_outN,
        VME_IRQn			=> VME_irqN_sig,
        VME_SYSFAILn        => VME_SYSFAILn,
        VME_ACC             => VME_ACC,
        VME_BUS_BUSY        => '0',                 -- should be used to share VME bus with flash access
        VME_DATA_BUF_OEn    => VME_DATA_BUF_OEn,
        VME_ADDR_BUF_OEn    => VME_ADDR_BUF_OEn,  
        VME_DTACK_BUF_OEn   => VME_DTACK_BUF_OEn,  
        VME_RETRY_BUF_OEn   => VME_RETRY_BUF_OEn,  
        VME_BERR_BUF_OEn    => VME_BERR_BUF_OEn,   
        VME_DATA_BUF_DIR    => VME_DATA_BUF_DIR,   
        VME_ADDR_BUF_DIR    => VME_ADDR_BUF_DIR,   
        rdreq_data          => vmeacc_rdreq_tx_data,
        rdreq_vld           => vmeacc_rdreq_tx_vld,
        rdreq_next          => vmeacc_rdreq_tx_next,
        wrreq_data          => vmeacc_wrreq_tx_data,
        wrreq_vld           => vmeacc_wrreq_tx_vld,
        wrreq_next          => vmeacc_wrreq_tx_next,
        rdout_data          => rdout_rx_data,
        rdout_vld           => rdout_rx_vld,
        rdout_next          => rdout_rx_next
    );
    
    --===============--
    -- VME To DAB64x --
    --===============--
    
    vmetodab64x : entity work.VME_to_DAB64x
    port map
    (   
        Stratix_IRQ_VECTORn => Stratix_IRQ_VECTORn,
        VME_IRQ_Level       => IntLev_reg,
        IRQ_Vec_OEn         => IRQ_Vector_Enable,
        Board_Rst_ONOFFn    => Board_Rst_ONOFFn,
        Stratix_VME_ACCn    => Stratix_VME_ACCn,
        VME_ForceRetry      => VME_ForceRetry,
        VME_2e_Cycle        => VME_2e_Cycle,
        VME_DTACK_EN        => VME_DTACK_EN,
        Stratix_245_OEn     => STX_245_oeN,
        Stratix_dtackN      => STX_dtackN,
        Stratix_SYSFAILn    => Stratix_SYSFAILn,
        VME_LWORDn_OUT      => VME_LWORDn_OUT,
        VME_DATA_OUT        => VME_DATA_OUT,
        VME_ADDR_OUT        => VME_ADDR_OUT,
        VME_LWORDn_IN       => VME_LWORDn_IN,
        VME_DATA_IN         => VME_DATA_IN,
        VME_ADDR_IN         => VME_ADDR_IN,
        VME_ADDR            => VME_ADD,
        VME_LWORDn          => VME_LWORDn,
        VME_DATA            => VME_DATA,
        VME_ACC             => VME_ACC,
        VME_DATA_BUF_OEn    => VME_DATA_BUF_OEn,
        VME_ADDR_BUF_OEn    => VME_ADDR_BUF_OEn,  
        VME_DTACK_BUF_OEn   => VME_DTACK_BUF_OEn,  
        VME_RETRY_BUF_OEn   => VME_RETRY_BUF_OEn,  
        VME_BERR_BUF_OEn    => VME_BERR_BUF_OEn,   
        VME_DATA_BUF_DIR    => VME_DATA_BUF_DIR,   
        VME_ADDR_BUF_DIR    => VME_ADDR_BUF_DIR,    
        VME_DTACKn          => VME_DTACKn,
        VME_SYSFAILn        => VME_SYSFAILn,
        FP_VME_ACC_LEDn     => FP_VME_ACC_LEDn
    );
    
	-- set pin defaults
	VME_data_dir <= VME_wrtN;	-- data buffers direction (VME_data_dir pin is CS board specific)
	VME_BR_WRITE2 <= '1';
	VME_BR_WRITE3 <= '1';
	VME_BBSY_WRITE <= '1';
	VME_irqN <= VME_irqN_sig(3 downto 1);
	
    
    -- CMI to Memory Mapped Bus converter
	VMECMI_MMbus32_i: entity work.VMECMI_MMbus32
    PORT MAP
    (
        --local clock and reset
        clk                 => sys_clk,
        nrst                => '1',
        
        -- VME Access Controller
        rdreq_data          => vmeacc_rdreq_tx_data,
        rdreq_vld           => vmeacc_rdreq_tx_vld,
        rdreq_next          => vmeacc_rdreq_tx_next,
        wrreq_data          => vmeacc_wrreq_tx_data,
        wrreq_vld           => vmeacc_wrreq_tx_vld,
        wrreq_next          => vmeacc_wrreq_tx_next,
        rdout_data          => rdout_rx_data,
        rdout_vld           => rdout_rx_vld,
        rdout_next          => rdout_rx_next,
        
        -- Memory mapped bus
        read_address        => read_address,
        read_data           => read_data,
        read_req            => read_req,
        write_address       => write_address,
        write_data          => write_data,
        write_req           => write_req
    );
  
    
    -- test ram block
    ram_block_i: ENTITY work.ram_32
    PORT MAP
    (
        address     => ram0_address,
        clock       => sys_clk,
        data        => ram0_write_data,
        wren        => ram0_wr_enable,
        q           => ram0_read_data
    );
    
    ram0_address <= write_address(15 downto 2) when ram0_wr_enable = '1' else read_address(15 downto 2);
    
    -- CS card registers block
    vme_cs_registers_i: entity work.vme_cs_registers
    PORT MAP
    (
        clk                 => sys_clk,
        
        -- Memory mapped bus
        read_address        => read_address,
        read_data           => read_data,
        read_req            => read_req,
        write_address       => write_address,
        write_data          => write_data,
        write_req           => write_req,
        
        --------------------------------------------------------
		-- memory interface to/from the logic
		--------------------------------------------------------
        ram0_wr             => ram0_write_data,
        ram0_wr_en          => ram0_wr_enable,
        ram0_rd             => ram0_read_data,

        --------------------------------------------------------
		-- registers interface to/from the logic
		--------------------------------------------------------
		
		-- status registers to/from the logic
		-- pass through register, VME R
        synth_date_rd           => synth_date_rd,
        synth_time_rd           => synth_time_rd,
        serial_id_h_rd          => serial_id_h_rd,
        serial_id_l_rd          => serial_id_l_rd,
        status_bits_rd          => status_bits_rd,
        temperature_rd          => temperature_rd,
		
		-- timing registers from the logic
		-- pass through registers, VME R
		beam_in_msec_rd			=> beam_in_msec_rd,
		beam_out_msec_rd		=> beam_out_msec_rd,
		basic_period_msec_rd	=> basic_period_msec_rd,
		
		-- interlock registers to/from the logic
		-- pass through registers, VME R
		cibu1_status_rd	=> cibu1_status_rd,
		cibu2_status_rd	=> cibu2_status_rd,
		hw_ilock1_rd	=> hw_ilock1_rd,
		hw_ilock2_rd	=> hw_ilock2_rd,
		sw_ilock1_rd	=> sw_ilock1_rd,
		sw_ilock2_rd	=> sw_ilock2_rd,
		current_user_rd	=> current_user_rd,
		wdg_timer_rd	=> wdg_timer_rd,
		wdg_time_rd		=> wdg_time_rd,
		-- pass through registers, VME W
		comm_hw_ilock1_wr		=> comm_hw_ilock1_wr,
		comm_hw_ilock1_wr_en	=> comm_hw_ilock1_wr_en,
		comm_hw_ilock2_wr		=> comm_hw_ilock2_wr,
		comm_hw_ilock2_wr_en	=> comm_hw_ilock2_wr_en,
		comm_sw_ilock1_wr		=> comm_sw_ilock1_wr,
		comm_sw_ilock1_wr_en	=> comm_sw_ilock1_wr_en,
		comm_sw_ilock2_wr		=> comm_sw_ilock2_wr,
		comm_sw_ilock2_wr_en	=> comm_sw_ilock2_wr_en,
		comm_set_user_wr		=> comm_set_user_wr,
		comm_set_user_wr_en		=> comm_set_user_wr_en,
		comm_set_wdg_time_wr	=> comm_set_wdg_time_wr,
		comm_set_wdg_time_wr_en	=> comm_set_wdg_time_wr_en,
		comm_reset_wdg_wr		=> comm_reset_wdg_wr,
		comm_reset_wdg_wr_en	=> comm_reset_wdg_wr_en,
		-- high voltage registers to/from the logic
		-- pass through registers, VME R
		hv1_volt_value_rd		=> hv1_volt_value_rd,
		hv2_volt_value_rd		=> hv2_volt_value_rd,
		hv1_volt_max_rd 		=> hv1_volt_max_rd,
		hv2_volt_max_rd 		=> hv2_volt_max_rd,
		hv1_volt_min_rd 		=> hv1_volt_min_rd,
		hv2_volt_min_rd 		=> hv2_volt_min_rd,
		hv1_curr_value_rd		=> hv1_curr_value_rd,
		hv2_curr_value_rd		=> hv2_curr_value_rd,
		hv1_curr_max_rd			=> hv1_curr_max_rd,
		hv2_curr_max_rd			=> hv2_curr_max_rd,
		hv1_curr_min_rd			=> hv1_curr_min_rd,
		hv2_curr_min_rd			=> hv2_curr_min_rd,
		hv_comparators_rd		=> hv_comparators_rd,
        hv1_volt_high_cnt_rd    => hv1_volt_high_cnt_rd,
        hv1_volt_low_cnt_rd     => hv1_volt_low_cnt_rd,
        hv1_curr_high_cnt_rd    => hv1_curr_high_cnt_rd,
        hv1_curr_low_cnt_rd     => hv1_curr_low_cnt_rd,
        hv2_volt_high_cnt_rd    => hv2_volt_high_cnt_rd,
        hv2_volt_low_cnt_rd     => hv2_volt_low_cnt_rd,
        hv2_curr_high_cnt_rd    => hv2_curr_high_cnt_rd,
        hv2_curr_low_cnt_rd     => hv2_curr_low_cnt_rd,
        hv1_dc_level_wr         => hv1_dc_level_wr,
        hv1_dc_level_wr_en      => hv1_dc_level_wr_en,
        hv1_dc_level_rd         => hv1_dc_level_rd,
        hv1_ac_amplitude_wr     => hv1_ac_amplitude_wr,
        hv1_ac_amplitude_wr_en  => hv1_ac_amplitude_wr_en,
        hv1_ac_amplitude_rd     => hv1_ac_amplitude_rd,
        hv1_ac_gain_p1_wr       => hv1_ac_gain_p1_wr,
        hv1_ac_gain_p1_wr_en    => hv1_ac_gain_p1_wr_en,
        hv1_ac_gain_p1_rd       => hv1_ac_gain_p1_rd,
        hv1_ac_gain_p2_wr       => hv1_ac_gain_p2_wr,
        hv1_ac_gain_p2_wr_en    => hv1_ac_gain_p2_wr_en,
        hv1_ac_gain_p2_rd       => hv1_ac_gain_p2_rd,
        hv2_dc_level_wr         => hv2_dc_level_wr,
        hv2_dc_level_wr_en      => hv2_dc_level_wr_en,
        hv2_dc_level_rd         => hv2_dc_level_rd,
        hv2_ac_amplitude_wr     => hv2_ac_amplitude_wr,
        hv2_ac_amplitude_wr_en  => hv2_ac_amplitude_wr_en,
        hv2_ac_amplitude_rd     => hv2_ac_amplitude_rd,
        hv2_ac_gain_p1_wr       => hv2_ac_gain_p1_wr,
        hv2_ac_gain_p1_wr_en    => hv2_ac_gain_p1_wr_en,
        hv2_ac_gain_p1_rd       => hv2_ac_gain_p1_rd,
        hv2_ac_gain_p2_wr       => hv2_ac_gain_p2_wr,
        hv2_ac_gain_p2_wr_en    => hv2_ac_gain_p2_wr_en,
        hv2_ac_gain_p2_rd       => hv2_ac_gain_p2_rd,
        hv_ac_frequency_wr      => hv_ac_frequency_wr,
        hv_ac_frequency_wr_en   => hv_ac_frequency_wr_en,
        hv_ac_frequency_rd      => hv_ac_frequency_rd,
		
		-- low voltage registers to/from the logic
		-- pass through registers, VME R
		vme3v3_value_rd		    => vme3v3_value_rd,
		vme5v_value_rd			=> vme5v_value_rd,
		analog_p15v_value_rd	=> analog_p15v_value_rd,
		analog_n15v_value_rd	=> analog_n15v_value_rd,
		analog_5v_value_rd		=> analog_5v_value_rd,
		ref_5v_value_rd		    => ref_5v_value_rd,
		ref_10va_value_rd		=> ref_10va_value_rd,
		ref_10vb_value_rd		=> ref_10vb_value_rd,
		vme3v3_min_rd			=> vme3v3_min_rd,
		vme3v3_max_rd			=> vme3v3_max_rd,
		vme5v_min_rd			=> vme5v_min_rd,
		vme5v_max_rd			=> vme5v_max_rd,
		analog_p15v_min_rd		=> analog_p15v_min_rd,
		analog_p15v_max_rd		=> analog_p15v_max_rd,
		analog_n15v_min_rd		=> analog_n15v_min_rd,
		analog_n15v_max_rd		=> analog_n15v_max_rd,
		analog_5v_min_rd		=> analog_5v_min_rd,
		analog_5v_max_rd		=> analog_5v_max_rd,
		ref_5v_min_rd			=> ref_5v_min_rd,
		ref_5v_max_rd			=> ref_5v_max_rd,
		ref_10va_min_rd		    => ref_10va_min_rd,
		ref_10va_max_rd		    => ref_10va_max_rd,
		ref_10vb_min_rd		    => ref_10vb_min_rd,
		ref_10vb_max_rd		    => ref_10vb_max_rd,
		lv_comparators_rd		=> lv_comparators_rd,
		analog5v_cnt_rd		    => analog5v_cnt_rd,
		analog15v_cnt_rd		=> analog15v_cnt_rd,
		vme3v3_cnt_rd			=> vme3v3_cnt_rd,
		vme5v_cnt_rd			=> vme5v_cnt_rd,
		vme12v_cnt_rd			=> vme12v_cnt_rd
    );
	
	
end architecture rtl;
