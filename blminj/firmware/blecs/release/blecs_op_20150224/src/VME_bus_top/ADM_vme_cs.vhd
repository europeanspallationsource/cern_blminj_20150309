-- -------------------------------------------------------------------
-- Company:        CERN - BE/BI/BL
-- Engineer:       Maciej Kwiatkowski
-- Written:        10/03/2014
-- Module Name:    ADM_vme_cs

-- Update:         23/10/2014 (Maciej & Christos)
--                    made the address space for the card human readable and to follow the memory map given to FESA. 

-- -------------------------------------------------------------------
-- Description
-- --------------------------------------------------------------------

    -- The Address Decoder and Mapper for the combiner and survey card.

-- -------------------------------------------------------------------
-- Generics/Constants
-- --------------------------------------------------------------------

    -- rx_number       := number of connected RAMs/slaves
    -- data_size       := size of input and output (this ADM has the same input and output size)
    -- addr_size       := size of the address part
    
-- --------------------------------------------------------------------
-- Necessary Packages/Libraries
-- --------------------------------------------------------------------
    
    -- vhdl_func_pkg
    
-- --------------------------------------------------------------------
-- Necessary Modules
-- --------------------------------------------------------------------
    
    -- none

-- --------------------------------------------------------------------
-- Necessary Cores
-- --------------------------------------------------------------------

    -- none 
    
-- --------------------------------------------------------------------
-- Implementation
-- --------------------------------------------------------------------
    
-- --------------------------------------------------------------------
-- Not implemented Features
-- --------------------------------------------------------------------


-- --------------------------------------------------------------------
-- --------------------------------------------------------------------

library IEEE;
    use IEEE.std_logic_1164.all;
    use IEEE.numeric_std.all;
    use IEEE.math_real.all;
    use work.vhdl_func_pkg.all;
   
entity ADM_vme_cs is

generic
(
    rx_number:      integer := 2;
    data_size:      integer; 
    addr_size:      integer
);

port
(
    ADM_tx_data:    in  std_logic_vector(data_size-1 downto 0);
    ADM_rx_data:    out std_logic_vector(data_size-1 downto 0);
    ADM_sel:        out std_logic_vector(get_vsize_addr(rx_number)-1 downto 0)
);

end entity ADM_vme_cs;

architecture struct of ADM_vme_cs is
    
    attribute keep: boolean;
    
    --===========--
    -- CONSTANTS --
    --===========--
    
    constant act_data_size: integer := data_size-addr_size;
    
    --=================--
    -- GENERAL SIGNALS --
    --=================--
    
    signal addr_full:   UNSIGNED(addr_size-1 downto 0);            -- full VME address without 3 LSB.         [normally 61 bits]
    signal addr_space:  UNSIGNED(addr_size-1+3 downto 0);          -- clean VME address (i.e. masked 40 MSB). [normally 64 bits] 
    signal addr_mapped: std_logic_vector(addr_size-1+3 downto 0);  -- output address for connected component. [normally 64 bits]
    
    attribute keep of addr_space: signal is true;
    attribute keep of addr_mapped: signal is true;
    
BEGIN
    
    -- Cast
    addr_full <= UNSIGNED(ADM_tx_data(data_size-1 downto act_data_size));
    addr_space <= X"00000000" & x"00" & UNSIGNED(addr_full(20 downto 0)) & "000";
    
    process(all)
    begin
        -- defaults
        ADM_sel     <= STD_LOGIC_VECTOR(TO_UNSIGNED(0, get_vsize_addr(rx_number)));
        addr_mapped <= (63 downto 0 => '0');
        
        -- memory mapping and selection
        if      addr_space(23 downto 0) >= X"000000" AND addr_space(23 downto 0) < X"000800" then
                    addr_mapped     <= (63 downto 24 => '0') & STD_LOGIC_VECTOR(addr_space(23 downto 0));
                    ADM_sel         <= STD_LOGIC_VECTOR(TO_UNSIGNED(0, get_vsize_addr(rx_number)));
        elsif   addr_space(23 downto 0) >= X"001000" AND addr_space(23 downto 0) < X"001800" then
                    addr_mapped     <= (63 downto 24 => '0') & STD_LOGIC_VECTOR(addr_space(23 downto 0) - X"001000");
                    ADM_sel         <= STD_LOGIC_VECTOR(TO_UNSIGNED(1, get_vsize_addr(rx_number)));
        
        -- registers mapping and selection
        -- VME range for the registers is x800000 to xFFFFFF - it equals to 8 Mbytes
        elsif   addr_space(23 downto 0) >= X"800000" AND addr_space(23 downto 0) < X"FFFFFF" then
                    addr_mapped     <= (63 downto 24 => '0') & STD_LOGIC_VECTOR(addr_space(23 downto 0) - X"800000");
                    ADM_sel         <= STD_LOGIC_VECTOR(TO_UNSIGNED(2, get_vsize_addr(rx_number)));
        end if;
    end process;
    
    -- OUTPUT
   ADM_rx_data <= addr_mapped(addr_size-1+3 downto 3) & ADM_tx_data(act_data_size-1 downto 0);
    
end architecture struct;
