# pre flow script
# add the following to the qsf to be executed before the synthesis:
# set_global_assignment -name PRE_FLOW_SCRIPT_FILE "quartus_sh:src/first.tcl"



# This line accommodates script automation
foreach { flow project revision } $quartus(args) { break }

set file_name ${project}.qpf


if { [project_exists $project] } {
    project_open $project
    set val_to_set [expr 0x[clock format [clock seconds] -format "%Y"]]
    set_parameter -name SYNTHESIS_YR $val_to_set
    post_message "Successfully set SYNTHESIS_YR generic to ${val_to_set}"
    set val_to_set [expr 0x[clock format [clock seconds] -format "%m"]]
    set_parameter -name SYNTHESIS_M $val_to_set
    post_message "Successfully set SYNTHESIS_M generic to ${val_to_set}"
    set val_to_set [expr 0x[clock format [clock seconds] -format "%d"]]
    set_parameter -name SYNTHESIS_D $val_to_set
    post_message "Successfully set SYNTHESIS_D generic to ${val_to_set}"
    set val_to_set [expr 0x[clock format [clock seconds] -format "%H"]]
    set_parameter -name SYNTHESIS_HR $val_to_set
    post_message "Successfully set SYNTHESIS_HR generic to ${val_to_set}"
    set val_to_set [expr 0x[clock format [clock seconds] -format "%M"]]
    set_parameter -name SYNTHESIS_MIN $val_to_set
    post_message "Successfully set SYNTHESIS_MIN generic to ${val_to_set}"
}

