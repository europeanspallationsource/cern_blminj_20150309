------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    12/11/2013
--Module Name:    blecs_op
--Project Name:   blecs_op
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use work.vhdl_func_pkg.all;

entity blecs_op is
generic
(
    SYNTHESIS_YR:          integer := 0;   -- this should be set by the first.tcl script
    SYNTHESIS_M:           integer := 0;   -- this should be set by the first.tcl script
    SYNTHESIS_D:           integer := 0;   -- this should be set by the first.tcl script
    SYNTHESIS_HR:          integer := 0;   -- this should be set by the first.tcl script
    SYNTHESIS_MIN:         integer := 0    -- this should be set by the first.tcl script
);
port(
	-- Input Clock 40 MHz
	STX_40MHZ_Osc:			in std_logic;
	
	-- Lemo Inputs
	FP_LEMO2:				in std_logic;
	FP_LEMO1:				in std_logic;
	FP_LEMO3:				in std_logic;
	FP_LEMO4:				in std_logic;
	
	-- Lemo Outputs
	FP_LEMO6:				out std_logic;
	FP_LEMO5:				out std_logic;
	
	--Lemo control signals
	FP_LEMO_DIR:			out std_logic;
	FP_LEMO_OE:				out std_logic;
	
	--VME user pins
	VME_USER_P0A:			inout std_logic_vector(7 downto 0);
	VME_USER_P0A_DIR:		out std_logic;
	
	
	-- VME Inputs
    VME_GA:                 in  std_logic_vector(4 downto 0);
    VME_GAP:                in  std_logic;
    VME_wrtN:               in  std_logic;
    VME_AM:                 in  std_logic_vector(5 downto 0);
    VME_asN:                in  std_logic;
    VME_ds0N:               in  std_logic;
    VME_ds1N:               in  std_logic;
    VME_iackN:              in  std_logic;
    VME_iackinN:            in  std_logic;
    VME_SYSCLK:             in  std_logic;
    VME_SYSRESETn:          in  std_logic;
    -- VME IOs
    VME_ADD:                inout std_logic_vector(31 downto 1);
    VME_lwordN:             inout std_logic;
    VME_DATA:               inout std_logic_vector(31 downto 0);
    -- VME Outputs
    vme_BERRn:              out std_logic;
    VME_RETRYn:             out std_logic;
    VME_iack_outN:          out std_logic;
    VME_irqN:               out std_logic_vector(3 downto 1);
    -- DAB64 IOs
    Stratix_Irq_VectorN:    out std_logic;
    IntLev_reg:             in  std_logic_vector(2 downto 0);		-- called VME_IRQ_Level in the DAB64x card
    IRQ_Vector_Enable:      in  std_logic;							-- called IRQ_Vec_OEn
    Board_Rst_OnOffN:       in  std_logic;
    FP_VME_ACC_LEDn:        out std_logic;
    Stratix_VME_ACCn:       out std_logic;
    VME_ForceRetry:         out std_logic;  -- activate with RETRY
    VME_2e_Cycle:           out std_logic;  -- use for A[31:1] & LWORDn direction
    VME_DTACK_EN:           inout std_logic;  -- for releasing the DTACK line
    STX_245_oeN:            out std_logic;  -- OE for data lines D[31:0]				called Stratix_245_OEn in the DAB64x card
    STX_dtackN:             out std_logic;  -- VME_DTACKn								called Stratix_dtackN in the DAB64x card
    Stratix_SYSFAILn:       out std_logic;  -- VME_SYSFAILn								renamed from VME_SYSFAILn to Stratix_SYSFAILn
	
	-- Combiner specific VME pin
	VME_data_dir:			out std_logic;
	VME_BR_WRITE2:			out std_logic;
	VME_BR_WRITE3:			out std_logic;
	VME_BBSY_WRITE	:		out std_logic;
	
	
	-- Interlock Inputs (DC signal from BLEPT - low level = interlock)
	P0_BLM_DCin1:			in std_logic;
	P0_BLM_DCin2:			in std_logic;
	-- combiners chain input (should be terminated)
	FPGA_U_A_neg_In:		in std_logic;
	FPGA_U_B_neg_In:		in std_logic;
	FPGA_M_A_neg_In:		in std_logic;
	FPGA_M_B_neg_In:		in std_logic;
		
	-- Interlock Outputs (clk > 1MHz to a monostable circuit driving CIBUs)
	FPGA_U_A_neg_Out:		out std_logic;
	FPGA_U_B_neg_Out:		out std_logic;
	FPGA_M_A_neg_Out:		out std_logic;
	FPGA_M_B_neg_Out:		out std_logic;
	
	-- Beam Info input
	FPGA_U_S_pos:			in std_logic;
	FPGA_M_S_pos:			in std_logic;
	
	-- AD7734 (high voltage ADC) signals
	AD7734_MCLK:			out std_logic;
	AD7734_SCLK:			out std_logic;		-- sclk
	AD7734_DOUT:			in std_logic;		-- dout
	AD7734_DIN:				out std_logic;		-- din
	AD7734_nCS:				out std_logic;		-- cs_n
	AD7734_nRDY:			in std_logic;		-- rdy_n
    
    -- Two DACs (DAC8532) SPI signals
    DAC8532_DIN:            out std_logic;
    DAC8532_SCLK:           out std_logic;
    DAC8532_nSYNC:          out std_logic;
    DAC8532_nSYNC_2:        out std_logic;
	
	-- High voltage and current comparators
	P2_IMon_1_higher:		in std_logic;
	P2_IMon_1_lower:		in std_logic;
	P2_IMon_2_higher:		in std_logic;
	P2_IMon_2_lower:		in std_logic;
	P2_VMon_1_higher:		in std_logic;
	P2_VMon_1_lower:		in std_logic;
	P2_VMon_2_higher:		in std_logic;
	P2_VMon_2_lower:		in std_logic;
	
	-- AD7327 (high voltage ADC) signals
	ADC7327_SCLK:			out std_logic;
	ADC7327_DIN:			out std_logic;
	ADC7327_DOUT:			in std_logic;
	ADC7327_nCS:			out std_logic;
	
	-- Low voltage comparators
	PS_3V3_ON:	        in std_logic;
	PS_12V_ON:	        in std_logic;
	PS_5V_ON:	        in std_logic;
	PS_15V_ON:	        in std_logic;
	PS_5VA_ON:	        in std_logic;
    
    -- I2C signals of the digital potentiometers and IO expander (LEDs)
    I2C_SCL_POT_LED:    inout std_logic;
    I2C_SDA_POT_LED:    inout std_logic;
    
    -- 1 wire signal of ds2401
    ModuleID:           inout std_logic;
    
    -- MAX6627 temperature sensor SPI interface
    Temp_CSn:           out std_logic;
    Temp_Clk:           out std_logic;
    Temp_data:          in std_logic
    
	
);
	
end entity blecs_op;


architecture rtl of blecs_op is

attribute keep: boolean;

constant sys_clk_freq : natural := 40000000;
constant sys_clk_ns_period : natural := 1000000000/sys_clk_freq;

-- PLL signals
signal sys_clk:			std_logic;
attribute keep of sys_clk: signal is true;
signal pll_locked:		std_logic;


-- Reset controller
signal rst_sys_clk_n_shift:	std_logic_vector(3 downto 0) := "0000";  -- edit range for delay
signal rst_sys_clk_n:			std_logic;


-- VME register signals
signal synth_date_rd:           std_logic_vector(31 downto 0);
signal synth_time_rd:           std_logic_vector(31 downto 0);
signal serial_id_h_rd:          std_logic_vector(31 downto 0);
signal serial_id_l_rd:          std_logic_vector(31 downto 0);
signal status_bits_rd:          std_logic_vector(31 downto 0);
signal temperature_rd:          std_logic_vector(31 downto 0);
signal beam_in_msec_rd:			std_logic_vector(31 downto 0);
signal beam_out_msec_rd:		std_logic_vector(31 downto 0);
signal basic_period_msec_rd:	std_logic_vector(31 downto 0);
signal cibu1_status_rd:			std_logic_vector(31 downto 0);
signal cibu2_status_rd:			std_logic_vector(31 downto 0);
signal hw_ilock1_rd:			std_logic_vector(31 downto 0);
signal hw_ilock2_rd:			std_logic_vector(31 downto 0);
signal sw_ilock1_rd:			std_logic_vector(31 downto 0);
signal sw_ilock2_rd:			std_logic_vector(31 downto 0);
signal current_user_rd:			std_logic_vector(31 downto 0);
signal wdg_timer_rd:			std_logic_vector(31 downto 0);
signal wdg_time_rd:				std_logic_vector(31 downto 0);
signal comm_hw_ilock1_wr:		std_logic_vector(31 downto 0);
signal comm_hw_ilock1_wr_en:	std_logic;
signal comm_hw_ilock2_wr:		std_logic_vector(31 downto 0);
signal comm_hw_ilock2_wr_en:	std_logic;
signal comm_sw_ilock1_wr:		std_logic_vector(31 downto 0);
signal comm_sw_ilock1_wr_en:	std_logic;
signal comm_sw_ilock2_wr:		std_logic_vector(31 downto 0);
signal comm_sw_ilock2_wr_en:	std_logic;
signal comm_set_user_wr:		std_logic_vector(31 downto 0);
signal comm_set_user_wr_en:		std_logic;
signal comm_set_wdg_time_wr:	std_logic_vector(31 downto 0);
signal comm_set_wdg_time_wr_en:	std_logic;
signal comm_reset_wdg_wr:		std_logic_vector(31 downto 0);
signal comm_reset_wdg_wr_en:	std_logic;
signal hv1_volt_value_rd:		std_logic_vector(31 downto 0);
signal hv2_volt_value_rd:		std_logic_vector(31 downto 0);
signal hv1_curr_value_rd:		std_logic_vector(31 downto 0);
signal hv2_curr_value_rd:		std_logic_vector(31 downto 0);
signal hv1_volt_max_rd:			std_logic_vector(31 downto 0);
signal hv2_volt_max_rd:			std_logic_vector(31 downto 0);
signal hv1_volt_min_rd:			std_logic_vector(31 downto 0);
signal hv2_volt_min_rd:			std_logic_vector(31 downto 0);
signal hv1_curr_max_rd:			std_logic_vector(31 downto 0);
signal hv2_curr_max_rd:			std_logic_vector(31 downto 0);
signal hv1_curr_min_rd:			std_logic_vector(31 downto 0);
signal hv2_curr_min_rd:			std_logic_vector(31 downto 0);
signal hv_comparators_rd:		std_logic_vector(31 downto 0);
signal hv1_volt_high_cnt_rd:    std_logic_vector(31 downto 0);
signal hv1_volt_low_cnt_rd:     std_logic_vector(31 downto 0);
signal hv1_curr_high_cnt_rd:    std_logic_vector(31 downto 0);
signal hv1_curr_low_cnt_rd:     std_logic_vector(31 downto 0);
signal hv2_volt_high_cnt_rd:    std_logic_vector(31 downto 0);
signal hv2_volt_low_cnt_rd:     std_logic_vector(31 downto 0);
signal hv2_curr_high_cnt_rd:    std_logic_vector(31 downto 0);
signal hv2_curr_low_cnt_rd:     std_logic_vector(31 downto 0);
signal hv1_dc_level_wr:         std_logic_vector(31 downto 0);
signal hv1_dc_level_rd:         std_logic_vector(31 downto 0);
signal hv1_dc_level_wr_en:      std_logic;
signal hv1_ac_amplitude_wr:     std_logic_vector(31 downto 0);
signal hv1_ac_amplitude_rd:     std_logic_vector(31 downto 0);
signal hv1_ac_amplitude_wr_en:  std_logic;
signal hv1_ac_gain_p1_wr:       std_logic_vector(31 downto 0);
signal hv1_ac_gain_p1_rd:       std_logic_vector(31 downto 0);
signal hv1_ac_gain_p1_wr_en:    std_logic;
signal hv1_ac_gain_p2_wr:       std_logic_vector(31 downto 0);
signal hv1_ac_gain_p2_rd:       std_logic_vector(31 downto 0);
signal hv1_ac_gain_p2_wr_en:    std_logic;
signal hv2_dc_level_wr:         std_logic_vector(31 downto 0);
signal hv2_dc_level_rd:         std_logic_vector(31 downto 0);
signal hv2_dc_level_wr_en:      std_logic;
signal hv2_ac_amplitude_wr:     std_logic_vector(31 downto 0);
signal hv2_ac_amplitude_rd:     std_logic_vector(31 downto 0);
signal hv2_ac_amplitude_wr_en:  std_logic;
signal hv2_ac_gain_p1_wr:       std_logic_vector(31 downto 0);
signal hv2_ac_gain_p1_rd:       std_logic_vector(31 downto 0);
signal hv2_ac_gain_p1_wr_en:    std_logic;
signal hv2_ac_gain_p2_wr:       std_logic_vector(31 downto 0);
signal hv2_ac_gain_p2_rd:       std_logic_vector(31 downto 0);
signal hv2_ac_gain_p2_wr_en:    std_logic;
signal hv_ac_frequency_wr:      std_logic_vector(31 downto 0);
signal hv_ac_frequency_rd:      std_logic_vector(31 downto 0);
signal hv_ac_frequency_wr_en:   std_logic;
signal vme3v3_value_rd:			std_logic_vector(31 downto 0);
signal vme5v_value_rd:			std_logic_vector(31 downto 0);
signal analog_p15v_value_rd:	std_logic_vector(31 downto 0);
signal analog_n15v_value_rd:	std_logic_vector(31 downto 0);
signal analog_5v_value_rd:		std_logic_vector(31 downto 0);
signal ref_5v_value_rd:			std_logic_vector(31 downto 0);
signal ref_10va_value_rd:		std_logic_vector(31 downto 0);
signal ref_10vb_value_rd:		std_logic_vector(31 downto 0);
signal vme3v3_min_rd:			std_logic_vector(31 downto 0);
signal vme3v3_max_rd:			std_logic_vector(31 downto 0);
signal vme5v_min_rd:			std_logic_vector(31 downto 0);
signal vme5v_max_rd:			std_logic_vector(31 downto 0);
signal analog_p15v_min_rd:		std_logic_vector(31 downto 0);
signal analog_p15v_max_rd:		std_logic_vector(31 downto 0);
signal analog_n15v_min_rd:		std_logic_vector(31 downto 0);
signal analog_n15v_max_rd:		std_logic_vector(31 downto 0);
signal analog_5v_min_rd:		std_logic_vector(31 downto 0);
signal analog_5v_max_rd:		std_logic_vector(31 downto 0);
signal ref_5v_min_rd:			std_logic_vector(31 downto 0);
signal ref_5v_max_rd:			std_logic_vector(31 downto 0);
signal ref_10va_min_rd:			std_logic_vector(31 downto 0);
signal ref_10va_max_rd:			std_logic_vector(31 downto 0);
signal ref_10vb_min_rd:			std_logic_vector(31 downto 0);
signal ref_10vb_max_rd:			std_logic_vector(31 downto 0);
signal lv_comparators_rd:		std_logic_vector(31 downto 0);
signal analog5v_cnt_rd:			std_logic_vector(31 downto 0);
signal analog15v_cnt_rd:		std_logic_vector(31 downto 0);
signal vme3v3_cnt_rd:			std_logic_vector(31 downto 0);
signal vme5v_cnt_rd:			std_logic_vector(31 downto 0);
signal vme12v_cnt_rd:			std_logic_vector(31 downto 0);
	
signal hw_ilock_sim:			std_logic_vector(2 downto 0);
signal hw_ilock1_mux:			std_logic;
signal hw_ilock2_mux:			std_logic;
signal beam_info_m:				std_logic;
signal beam_info_u:				std_logic;
signal bp_status:				std_logic_vector(3 downto 0);


signal start_of_bp:			    std_logic;
signal timing_lost:			    std_logic_vector(2 downto 0);

signal led_digpot_ack_error:	std_logic_vector(1 downto 0);
signal LED_data_sr:	            std_logic_vector(15 downto 0);


BEGIN

	------------------------------------------------------------------
	-- PLL
	------------------------------------------------------------------
	
	PLL1_inst: entity work.pll
	port map 
	( 
		inclk0 	=> STX_40MHZ_Osc,	 
		c0 		=> sys_clk, 		--40 MHz
		c1 		=> AD7734_MCLK, 	--6.15 MHz
		locked	=> pll_locked
	);
	

	------------------------------------------------------------------
	-- synchronous reset circuits
	------------------------------------------------------------------
	sys_rst_controller: process (sys_clk, pll_locked) -- rst (from reset) synchronises to sys_clk
	begin
		if pll_locked = '0' then
			rst_sys_clk_n_shift <= (others => '0');      
		elsif rising_edge (sys_clk) then
			rst_sys_clk_n_shift <= rst_sys_clk_n_shift(rst_sys_clk_n_shift'HIGH-1 downto rst_sys_clk_n_shift'LOW) & '1';
		end if;
	end process;
	-- reset is the output of the high bit of the shift register.
	rst_sys_clk_n <= rst_sys_clk_n_shift(rst_sys_clk_n_shift'HIGH);
	
	
	------------------------------------------------------------------
	-- timing circuits
	------------------------------------------------------------------
	
	Timing_circuits_i: entity work.Timing_circuits
    generic map
    (
        sys_clk_ns_period => sys_clk_ns_period
    )
    port map
    (   
		sys_clk				=> sys_clk,
		rst_sys_clk_n		=> rst_sys_clk_n,
        
		FP_LEMO2			=> FP_LEMO2		,
		FP_LEMO1			=> FP_LEMO1		,
		FP_LEMO3			=> FP_LEMO3		,
		FP_LEMO4			=> FP_LEMO4		,
		FP_LEMO6			=> FP_LEMO6		,
		FP_LEMO5			=> FP_LEMO5		,
		FP_LEMO_DIR			=> FP_LEMO_DIR		,
		FP_LEMO_OE			=> FP_LEMO_OE		,
		VME_USER_P0A		=> VME_USER_P0A	,
		VME_USER_P0A_DIR	=> VME_USER_P0A_DIR,
		beam_in_msec		=> beam_in_msec_rd,
		beam_out_msec		=> beam_out_msec_rd,
		basic_period_msec	=> basic_period_msec_rd,
		timing_lost	        => timing_lost,
		start_of_bp			=> start_of_bp

    );
	
	------------------------------------------------------------------
	-- VME bus top
	------------------------------------------------------------------
    
    vmebus_i: entity work.VME_bus_top
    generic map
    (
        sys_clk_ns_period => sys_clk_ns_period
    )
    port map
    (   
		sys_clk				=> sys_clk,
        VME_GA				=> VME_GA,				
        VME_GAP				=> VME_GAP,				
		VME_wrtN			=> VME_wrtN,			
		VME_AM				=> VME_AM,				
		VME_asN				=> VME_asN,				
		VME_ds0N			=> VME_ds0N,		
		VME_ds1N			=> VME_ds1N,		
		VME_iackN			=> VME_iackN,			
		VME_iackinN			=> VME_iackinN,			
		VME_SYSCLK			=> VME_SYSCLK,			
		VME_SYSRESETn		=> VME_SYSRESETn,
		VME_ADD				=> VME_ADD,				
		VME_lwordN			=> VME_lwordN,			
		VME_DATA			=> VME_DATA,
		vme_BERRn			=> vme_BERRn,			
		VME_RETRYn			=> VME_RETRYn,			
		VME_iack_outN		=> VME_iack_outN,		
		VME_irqN			=> VME_irqN,			
		Stratix_Irq_VectorN	=> Stratix_Irq_VectorN,	
		IntLev_reg			=> IntLev_reg,			
		IRQ_Vector_Enable	=> IRQ_Vector_Enable,	
		Board_Rst_OnOffN	=> Board_Rst_OnOffN,	
		FP_VME_ACC_LEDn		=> FP_VME_ACC_LEDn,		
		Stratix_VME_ACCn	=> Stratix_VME_ACCn,	
		VME_ForceRetry		=> VME_ForceRetry,		
		VME_2e_Cycle		=> VME_2e_Cycle,		
		VME_DTACK_EN		=> VME_DTACK_EN,		
		STX_245_oeN			=> STX_245_oeN,			
		STX_dtackN			=> STX_dtackN,			
		Stratix_SYSFAILn	=> Stratix_SYSFAILn,	
		VME_data_dir		=> VME_data_dir,		
		VME_BR_WRITE2		=> VME_BR_WRITE2,		
		VME_BR_WRITE3		=> VME_BR_WRITE3,		
		VME_BBSY_WRITE		=> VME_BBSY_WRITE,		
		
		-- Status registers
        synth_date_rd           => synth_date_rd,
        synth_time_rd           => synth_time_rd,
        serial_id_h_rd          => serial_id_h_rd,
        serial_id_l_rd          => serial_id_l_rd,
        status_bits_rd          => status_bits_rd,
        temperature_rd          => temperature_rd,
		-- Timing registers
		beam_in_msec_rd			=> beam_in_msec_rd,
		beam_out_msec_rd		=> beam_out_msec_rd,
		basic_period_msec_rd	=> basic_period_msec_rd,
		-- Interlock registers
		cibu1_status_rd			=> cibu1_status_rd,
		cibu2_status_rd			=> cibu2_status_rd,
		hw_ilock1_rd			=> hw_ilock1_rd,
		hw_ilock2_rd			=> hw_ilock2_rd,
		sw_ilock1_rd			=> sw_ilock1_rd,
		sw_ilock2_rd			=> sw_ilock2_rd,
		current_user_rd			=> current_user_rd,
		wdg_timer_rd			=> wdg_timer_rd,
		wdg_time_rd				=> wdg_time_rd,
		comm_hw_ilock1_wr		=> comm_hw_ilock1_wr,
		comm_hw_ilock1_wr_en	=> comm_hw_ilock1_wr_en,
		comm_hw_ilock2_wr		=> comm_hw_ilock2_wr,
		comm_hw_ilock2_wr_en	=> comm_hw_ilock2_wr_en,
		comm_sw_ilock1_wr		=> comm_sw_ilock1_wr,
		comm_sw_ilock1_wr_en	=> comm_sw_ilock1_wr_en,
		comm_sw_ilock2_wr		=> comm_sw_ilock2_wr,
		comm_sw_ilock2_wr_en	=> comm_sw_ilock2_wr_en,
		comm_set_user_wr		=> comm_set_user_wr,
		comm_set_user_wr_en		=> comm_set_user_wr_en,
		comm_set_wdg_time_wr	=> comm_set_wdg_time_wr,
		comm_set_wdg_time_wr_en	=> comm_set_wdg_time_wr_en,
		comm_reset_wdg_wr		=> comm_reset_wdg_wr,
		comm_reset_wdg_wr_en	=> comm_reset_wdg_wr_en,
		-- High voltage registers
		hv1_volt_value_rd		=> hv1_volt_value_rd,
		hv2_volt_value_rd		=> hv2_volt_value_rd,
		hv1_volt_max_rd 		=> hv1_volt_max_rd,
		hv2_volt_max_rd 		=> hv2_volt_max_rd,
		hv1_volt_min_rd 		=> hv1_volt_min_rd,
		hv2_volt_min_rd 		=> hv2_volt_min_rd,
		hv1_curr_value_rd		=> hv1_curr_value_rd,
		hv2_curr_value_rd		=> hv2_curr_value_rd,
		hv1_curr_max_rd			=> hv1_curr_max_rd,
		hv2_curr_max_rd			=> hv2_curr_max_rd,
		hv1_curr_min_rd			=> hv1_curr_min_rd,
		hv2_curr_min_rd			=> hv2_curr_min_rd,
		hv_comparators_rd		=> hv_comparators_rd,
        hv1_volt_high_cnt_rd    => hv1_volt_high_cnt_rd,
        hv1_volt_low_cnt_rd     => hv1_volt_low_cnt_rd,
        hv1_curr_high_cnt_rd    => hv1_curr_high_cnt_rd,
        hv1_curr_low_cnt_rd     => hv1_curr_low_cnt_rd,
        hv2_volt_high_cnt_rd    => hv2_volt_high_cnt_rd,
        hv2_volt_low_cnt_rd     => hv2_volt_low_cnt_rd,
        hv2_curr_high_cnt_rd    => hv2_curr_high_cnt_rd,
        hv2_curr_low_cnt_rd     => hv2_curr_low_cnt_rd,
        hv1_dc_level_wr         => hv1_dc_level_wr,
        hv1_dc_level_wr_en      => hv1_dc_level_wr_en,
        hv1_dc_level_rd         => hv1_dc_level_rd,
        hv1_ac_amplitude_wr     => hv1_ac_amplitude_wr,
        hv1_ac_amplitude_wr_en  => hv1_ac_amplitude_wr_en,
        hv1_ac_amplitude_rd     => hv1_ac_amplitude_rd,
        hv1_ac_gain_p1_wr       => hv1_ac_gain_p1_wr,
        hv1_ac_gain_p1_wr_en    => hv1_ac_gain_p1_wr_en,
        hv1_ac_gain_p1_rd       => hv1_ac_gain_p1_rd,
        hv1_ac_gain_p2_wr       => hv1_ac_gain_p2_wr,
        hv1_ac_gain_p2_wr_en    => hv1_ac_gain_p2_wr_en,
        hv1_ac_gain_p2_rd       => hv1_ac_gain_p2_rd,
        hv2_dc_level_wr         => hv2_dc_level_wr,
        hv2_dc_level_wr_en      => hv2_dc_level_wr_en,
        hv2_dc_level_rd         => hv2_dc_level_rd,
        hv2_ac_amplitude_wr     => hv2_ac_amplitude_wr,
        hv2_ac_amplitude_wr_en  => hv2_ac_amplitude_wr_en,
        hv2_ac_amplitude_rd     => hv2_ac_amplitude_rd,
        hv2_ac_gain_p1_wr       => hv2_ac_gain_p1_wr,
        hv2_ac_gain_p1_wr_en    => hv2_ac_gain_p1_wr_en,
        hv2_ac_gain_p1_rd       => hv2_ac_gain_p1_rd,
        hv2_ac_gain_p2_wr       => hv2_ac_gain_p2_wr,
        hv2_ac_gain_p2_wr_en    => hv2_ac_gain_p2_wr_en,
        hv2_ac_gain_p2_rd       => hv2_ac_gain_p2_rd,
        hv_ac_frequency_wr      => hv_ac_frequency_wr,
        hv_ac_frequency_wr_en   => hv_ac_frequency_wr_en,
        hv_ac_frequency_rd      => hv_ac_frequency_rd,
        
		-- Low voltage registers
		vme3v3_value_rd		    => vme3v3_value_rd,
		vme5v_value_rd			=> vme5v_value_rd,
		analog_p15v_value_rd	=> analog_p15v_value_rd,
		analog_n15v_value_rd	=> analog_n15v_value_rd,
		analog_5v_value_rd		=> analog_5v_value_rd,
		ref_5v_value_rd		    => ref_5v_value_rd,
		ref_10va_value_rd		=> ref_10va_value_rd,
		ref_10vb_value_rd		=> ref_10vb_value_rd,
		vme3v3_min_rd			=> vme3v3_min_rd,
		vme3v3_max_rd			=> vme3v3_max_rd,
		vme5v_min_rd			=> vme5v_min_rd,
		vme5v_max_rd			=> vme5v_max_rd,
		analog_p15v_min_rd		=> analog_p15v_min_rd,
		analog_p15v_max_rd		=> analog_p15v_max_rd,
		analog_n15v_min_rd		=> analog_n15v_min_rd,
		analog_n15v_max_rd		=> analog_n15v_max_rd,
		analog_5v_min_rd		=> analog_5v_min_rd,
		analog_5v_max_rd		=> analog_5v_max_rd,
		ref_5v_min_rd			=> ref_5v_min_rd,
		ref_5v_max_rd			=> ref_5v_max_rd,
		ref_10va_min_rd		    => ref_10va_min_rd,
		ref_10va_max_rd		    => ref_10va_max_rd,
		ref_10vb_min_rd		    => ref_10vb_min_rd,
		ref_10vb_max_rd		    => ref_10vb_max_rd,
		lv_comparators_rd		=> lv_comparators_rd,
		analog5v_cnt_rd		    => analog5v_cnt_rd,
		analog15v_cnt_rd		=> analog15v_cnt_rd,
		vme3v3_cnt_rd			=> vme3v3_cnt_rd,
		vme5v_cnt_rd			=> vme5v_cnt_rd,
		vme12v_cnt_rd			=> vme12v_cnt_rd
	
    );
    
	------------------------------------------------------------------
	-- Interlock circuits
	------------------------------------------------------------------
	
	Ilock_circuits_i: entity work.Interlock_interface
    generic map
    (
        sys_clk_freq => sys_clk_freq,
		mono_clk_freq => 10000000,
		wdg_time_ms_default => 2400
    )
    port map
    (   
		sys_clk				=> sys_clk,
		rst_sys_clk_n		=> rst_sys_clk_n,
        
		cibu1_status_rd			=> cibu1_status_rd,
		cibu2_status_rd			=> cibu2_status_rd,
		hw_ilock1_rd			=> hw_ilock1_rd,
		hw_ilock2_rd			=> hw_ilock2_rd,
		sw_ilock1_rd			=> sw_ilock1_rd,
		sw_ilock2_rd			=> sw_ilock2_rd,
		current_user_rd			=> current_user_rd,
		wdg_timer_rd			=> wdg_timer_rd,
		wdg_time_rd				=> wdg_time_rd,
		comm_hw_ilock1_wr		=> comm_hw_ilock1_wr,
		comm_hw_ilock1_wr_en	=> comm_hw_ilock1_wr_en,
		comm_hw_ilock2_wr		=> comm_hw_ilock2_wr,
		comm_hw_ilock2_wr_en	=> comm_hw_ilock2_wr_en,
		comm_sw_ilock1_wr		=> comm_sw_ilock1_wr,
		comm_sw_ilock1_wr_en	=> comm_sw_ilock1_wr_en,
		comm_sw_ilock2_wr		=> comm_sw_ilock2_wr,
		comm_sw_ilock2_wr_en	=> comm_sw_ilock2_wr_en,
		comm_set_user_wr		=> comm_set_user_wr,
		comm_set_user_wr_en		=> comm_set_user_wr_en,
		comm_set_wdg_time_wr	=> comm_set_wdg_time_wr,
		comm_set_wdg_time_wr_en	=> comm_set_wdg_time_wr_en,
		comm_reset_wdg_wr		=> comm_reset_wdg_wr,
		comm_reset_wdg_wr_en	=> comm_reset_wdg_wr_en,
		
		-- Interlock Inputs (DC signal from BLEPT - low level = interlock)
		--beam_allowed_1 => P0_BLM_DCin1,
		--beam_allowed_2 => P0_BLM_DCin2,
		beam_allowed_1 => hw_ilock1_mux,
		beam_allowed_2 => hw_ilock2_mux,
		
		beam_info_u_in => FPGA_U_S_pos,
		beam_info_m_in => FPGA_M_S_pos,
		
		-- Interlock Outputs (clk > 1MHz to a monostable circuit driving CIBUs)
		CIBU1_A => FPGA_U_A_neg_Out,
		CIBU1_B => FPGA_U_B_neg_Out,
		CIBU2_A => FPGA_M_A_neg_Out,
		CIBU2_B => FPGA_M_B_neg_Out,
		
		bp_status => bp_status,
		
		beam_info_u => beam_info_u,
		beam_info_m => beam_info_m
		
    );
	
	
	-- ISSP to test HW interlocks
	hw_ilock_issp_i: entity work.hw_ilock_issp
	port map 
	( 
		probe	=> FPGA_U_A_neg_In & FPGA_U_B_neg_In & FPGA_M_A_neg_In & FPGA_M_B_neg_In & beam_info_u & beam_info_m, 
		source	=> hw_ilock_sim
	);
	
	hw_ilock1_mux <= P0_BLM_DCin1 when hw_ilock_sim(2) = '0' else hw_ilock_sim(0);
	hw_ilock2_mux <= P0_BLM_DCin2 when hw_ilock_sim(2) = '0' else hw_ilock_sim(1);	
	
    ------------------------------------------------------------------
	-- Status registers component
	------------------------------------------------------------------
	
	Status_registers_i: entity work.Status_registers
    generic map
    (
        sys_clk_freq    => sys_clk_freq,
        SYNTHESIS_YR    => SYNTHESIS_YR,
        SYNTHESIS_M     => SYNTHESIS_M,
        SYNTHESIS_D     => SYNTHESIS_D,
        SYNTHESIS_HR    => SYNTHESIS_HR,
        SYNTHESIS_MIN   => SYNTHESIS_MIN
    )
    port map
    (   
		sys_clk             => sys_clk,
		rst_sys_clk_n       => rst_sys_clk_n,
        
        -- VME registers signals
        synth_date_rd       => synth_date_rd,
        synth_time_rd       => synth_time_rd,
        serial_id_h_rd      => serial_id_h_rd,
        serial_id_l_rd      => serial_id_l_rd,
        status_bits_rd      => status_bits_rd,
        temperature_rd      => temperature_rd,
        
        -- Start of basic period pulse
        start_of_bp         => start_of_bp,
        
        -- status bits
        led_digpot_ack_error    => led_digpot_ack_error,
        timing_lost             => timing_lost,
        
        -- DS2401 1 wire signal
        ModuleID            => ModuleID,
        
        -- MAX6627 temperature sensor SPI interface
        Temp_CSn            => Temp_CSn,
        Temp_Clk            => Temp_Clk,
        Temp_data           => Temp_data
    );
	
	------------------------------------------------------------------
	-- High voltage survey and control circuits
	------------------------------------------------------------------
	
	High_voltage_i: entity work.High_voltage
    generic map
    (
        sys_clk_freq => sys_clk_freq
    )
    port map
    (   
		sys_clk				=> sys_clk,
		rst_sys_clk_n		=> rst_sys_clk_n,
        
		-- VME registers signals
		hv1_volt_value_rd	=> hv1_volt_value_rd,
		hv2_volt_value_rd	=> hv2_volt_value_rd,
		hv1_volt_max_rd 	=> hv1_volt_max_rd,
		hv2_volt_max_rd 	=> hv2_volt_max_rd,
		hv1_volt_min_rd 	=> hv1_volt_min_rd,
		hv2_volt_min_rd 	=> hv2_volt_min_rd,
		hv1_curr_value_rd	=> hv1_curr_value_rd,
		hv2_curr_value_rd	=> hv2_curr_value_rd,
		hv1_curr_max_rd		=> hv1_curr_max_rd,
		hv2_curr_max_rd		=> hv2_curr_max_rd,
		hv1_curr_min_rd		=> hv1_curr_min_rd,
		hv2_curr_min_rd		=> hv2_curr_min_rd,
		hv_comparators_rd	=> hv_comparators_rd,
        hv1_volt_high_cnt_rd    => hv1_volt_high_cnt_rd,
        hv1_volt_low_cnt_rd     => hv1_volt_low_cnt_rd,
        hv1_curr_high_cnt_rd    => hv1_curr_high_cnt_rd,
        hv1_curr_low_cnt_rd     => hv1_curr_low_cnt_rd,
        hv2_volt_high_cnt_rd    => hv2_volt_high_cnt_rd,
        hv2_volt_low_cnt_rd     => hv2_volt_low_cnt_rd,
        hv2_curr_high_cnt_rd    => hv2_curr_high_cnt_rd,
        hv2_curr_low_cnt_rd     => hv2_curr_low_cnt_rd,
        hv1_dc_level_wr         => hv1_dc_level_wr,
        hv1_dc_level_wr_en      => hv1_dc_level_wr_en,
        hv1_dc_level_rd         => hv1_dc_level_rd,
        hv1_ac_amplitude_wr     => hv1_ac_amplitude_wr,
        hv1_ac_amplitude_wr_en  => hv1_ac_amplitude_wr_en,
        hv1_ac_amplitude_rd     => hv1_ac_amplitude_rd,
        hv1_ac_gain_p1_wr       => hv1_ac_gain_p1_wr,
        hv1_ac_gain_p1_wr_en    => hv1_ac_gain_p1_wr_en,
        hv1_ac_gain_p1_rd       => hv1_ac_gain_p1_rd,
        hv1_ac_gain_p2_wr       => hv1_ac_gain_p2_wr,
        hv1_ac_gain_p2_wr_en    => hv1_ac_gain_p2_wr_en,
        hv1_ac_gain_p2_rd       => hv1_ac_gain_p2_rd,
        hv2_dc_level_wr         => hv2_dc_level_wr,
        hv2_dc_level_wr_en      => hv2_dc_level_wr_en,
        hv2_dc_level_rd         => hv2_dc_level_rd,
        hv2_ac_amplitude_wr     => hv2_ac_amplitude_wr,
        hv2_ac_amplitude_wr_en  => hv2_ac_amplitude_wr_en,
        hv2_ac_amplitude_rd     => hv2_ac_amplitude_rd,
        hv2_ac_gain_p1_wr       => hv2_ac_gain_p1_wr,
        hv2_ac_gain_p1_wr_en    => hv2_ac_gain_p1_wr_en,
        hv2_ac_gain_p1_rd       => hv2_ac_gain_p1_rd,
        hv2_ac_gain_p2_wr       => hv2_ac_gain_p2_wr,
        hv2_ac_gain_p2_wr_en    => hv2_ac_gain_p2_wr_en,
        hv2_ac_gain_p2_rd       => hv2_ac_gain_p2_rd,
        hv_ac_frequency_wr      => hv_ac_frequency_wr,
        hv_ac_frequency_wr_en   => hv_ac_frequency_wr_en,
        hv_ac_frequency_rd      => hv_ac_frequency_rd,
		
		-- Start of basic period pulse
		start_of_bp	=> start_of_bp,
		
		-- High voltage and current comparators
		P2_IMon_1_higher	=> P2_IMon_1_higher,
		P2_IMon_1_lower		=> P2_IMon_1_lower,
		P2_IMon_2_higher	=> P2_IMon_2_higher,
		P2_IMon_2_lower		=> P2_IMon_2_lower,
		P2_VMon_1_higher	=> P2_VMon_1_higher,
		P2_VMon_1_lower		=> P2_VMon_1_lower,
		P2_VMon_2_higher	=> P2_VMon_2_higher,
		P2_VMon_2_lower		=> P2_VMon_2_lower,
		
		-- AD7734 SPI signals
		AD7734_sclk	        => AD7734_SCLK,
		AD7734_dout	        => AD7734_DIN,
		AD7734_din          => AD7734_DOUT,
		AD7734_cs_n         => AD7734_nCS,
		
		-- AD7734 data ready pin
		AD7734_rdy_n        => AD7734_nRDY,
        
        -- Two DACs (DAC8532) SPI signals
        DAC8532_DIN         => DAC8532_DIN,
        DAC8532_SCLK        => DAC8532_SCLK,
        DAC8532_nSYNC       => DAC8532_nSYNC,
        DAC8532_nSYNC_2     => DAC8532_nSYNC_2
    );
	
	
	------------------------------------------------------------------
	-- Low voltage survey circuits
	------------------------------------------------------------------
	
	Low_voltage_i: entity work.Low_voltage
    generic map
    (
        sys_clk_freq => sys_clk_freq
    )
    port map
    (   
		sys_clk				=> sys_clk,
		rst_sys_clk_n		=> rst_sys_clk_n,
        
		-- VME registers signals
		vme3v3_value_rd		    => vme3v3_value_rd,
		vme5v_value_rd			=> vme5v_value_rd,
		analog_p15v_value_rd	=> analog_p15v_value_rd,
		analog_n15v_value_rd	=> analog_n15v_value_rd,
		analog_5v_value_rd		=> analog_5v_value_rd,
		ref_5v_value_rd		    => ref_5v_value_rd,
		ref_10va_value_rd		=> ref_10va_value_rd,
		ref_10vb_value_rd		=> ref_10vb_value_rd,
		vme3v3_min_rd			=> vme3v3_min_rd,
		vme3v3_max_rd			=> vme3v3_max_rd,
		vme5v_min_rd			=> vme5v_min_rd,
		vme5v_max_rd			=> vme5v_max_rd,
		analog_p15v_min_rd		=> analog_p15v_min_rd,
		analog_p15v_max_rd		=> analog_p15v_max_rd,
		analog_n15v_min_rd		=> analog_n15v_min_rd,
		analog_n15v_max_rd		=> analog_n15v_max_rd,
		analog_5v_min_rd		=> analog_5v_min_rd,
		analog_5v_max_rd		=> analog_5v_max_rd,
		ref_5v_min_rd			=> ref_5v_min_rd,
		ref_5v_max_rd			=> ref_5v_max_rd,
		ref_10va_min_rd		    => ref_10va_min_rd,
		ref_10va_max_rd		    => ref_10va_max_rd,
		ref_10vb_min_rd		    => ref_10vb_min_rd,
		ref_10vb_max_rd		    => ref_10vb_max_rd,
		lv_comparators_rd		=> lv_comparators_rd,
		analog5v_cnt_rd		    => analog5v_cnt_rd,
		analog15v_cnt_rd		=> analog15v_cnt_rd,
		vme3v3_cnt_rd			=> vme3v3_cnt_rd,
		vme5v_cnt_rd			=> vme5v_cnt_rd,
		vme12v_cnt_rd			=> vme12v_cnt_rd,
		
		-- Start of basic period pulse
		start_of_bp	=> start_of_bp,
		
		-- Low voltage comparators
		PS_3V3_ON	=> PS_3V3_ON,
		PS_12V_ON	=> PS_12V_ON,
		PS_5V_ON	=> PS_5V_ON,
		PS_15V_ON	=> PS_15V_ON,
		PS_5VA_ON	=> PS_5VA_ON,
		
		-- AD7327 SPI signals
		sclk		=> ADC7327_SCLK,
		dout		=> ADC7327_DIN,
		din			=> ADC7327_DOUT,
		cs_n		=> ADC7327_nCS
    );
    
    
    ------------------------------------------------------------------
	-- Digital potentiometers (HV) and LED interface
	------------------------------------------------------------------
	
	LED_digpot_i: entity work.LED_digpot
    generic map
    (
        sys_clk_freq => sys_clk_freq,
        i2c_clk_freq => 400000
    )
    port map
    (   
		sys_clk				=> sys_clk,
		rst_sys_clk_n		=> rst_sys_clk_n,
        
        -- LED and digipot control signals
        LED_data            => LED_data_sr,
        pot1_data           => hv1_ac_gain_p1_rd(7 downto 0),
        pot2_data           => hv2_ac_gain_p1_rd(7 downto 0),
        pot3_data           => hv1_ac_gain_p2_rd(7 downto 0),
        pot4_data           => hv2_ac_gain_p2_rd(7 downto 0),
        start_wr            => start_of_bp,             -- update potentiometers and LEDs every start_of_bp?
        i2c_ack_error       => led_digpot_ack_error,    -- i2c_ack_error(0) = PCF8575 ACK/NOACK, i2c_ack_error(1) = AD5263 ACK/NOACK
        busy                => open,
        
        --I2C signals
        I2C_SDA_POT_LED     => I2C_SDA_POT_LED,
        I2C_SCL_POT_LED     => I2C_SCL_POT_LED
    );
    
    -- update LED every start_of_bp
	-- Not connected lines:
	--LED_data_sr(7)
	--LED_data_sr(6)
	--LED_data_sr(5)
	--LED_data_sr(4)
	-- TODO: do reverse engeneering and find which label on the FP correspond to which LED!!! In all cases U can be M and A can be B.
    ff3_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' then
                LED_data_sr <= (others=>'1');
            elsif start_of_bp = '1' then
				
				LED_data_sr(13) <= not beam_info_u;
				LED_data_sr(12) <= not beam_info_m;
				
				LED_data_sr(11) <= FPGA_U_A_neg_In;
				LED_data_sr(10) <= FPGA_U_B_neg_In;
				LED_data_sr(9) <= FPGA_M_A_neg_In;
				LED_data_sr(8) <= FPGA_M_B_neg_In;
				
				LED_data_sr(3) <= not bp_status(3);
				LED_data_sr(2) <= not bp_status(2);
				LED_data_sr(1) <= not bp_status(1);
				LED_data_sr(0) <= not bp_status(0);
            end if;
        end if;
    end process;
	
end architecture rtl;
