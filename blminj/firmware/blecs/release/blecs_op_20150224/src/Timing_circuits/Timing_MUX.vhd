------------------------------
--
--Company:          CERN - BE/BI/BL
--Engineer:         Maciej Kwiatkowski
--Create Date:      19/06/2014
--Module Name:      Timing_MUX
--Project Name:     Timing_MUX
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity Timing_MUX is
generic(
    period_ticks:       natural := 48000000;
    timeout_ticks:      natural := 52000000
);
port(
    -- input clock
    sys_clk:            in std_logic;
    -- (not) reset input
    rst_sys_clk_n:      in std_logic;
    -- clock enable input (optional, connect to '1' when unused)
    clock_enable:       in std_logic;
    -- timing input
    timing_in:          in std_logic;
    -- timing output
    timing_out:         out std_logic;
    -- timing lost
    timing_lost:        out std_logic    
);
    
end entity Timing_MUX;


architecture rtl of Timing_MUX is

attribute keep: boolean;

-- local period counter
signal period_cnt : integer range 0 to period_ticks-1;
signal period_cnt_done : std_logic;
signal local_period : std_logic;
signal local_period_d1 : std_logic;
signal timing_mux : std_logic;
signal local_period_en : std_logic;

-- timing lost counter
signal tim_loss_cnt : integer range 0 to timeout_ticks-1;
signal tim_loss_cnt_done : std_logic;

attribute keep of tim_loss_cnt_done: signal is true;
attribute keep of local_period_en: signal is true;



BEGIN

    -- asserts
	assert timeout_ticks > period_ticks report "Timeout ticks must be higher than period ticks" severity failure;
    
    ------------------------------------------------------------------
    -- Synchronise local counter to the external start of period pulse
    -- Create local start of period pulse
    ------------------------------------------------------------------
    period_cnt_p: process (sys_clk)
    begin
        if rising_edge (sys_clk) then
            if rst_sys_clk_n = '0' or local_period = '1' or timing_in = '1' then
                period_cnt <= 0;
            elsif clock_enable = '1' then
                period_cnt <= period_cnt + 1;
            end if;
            local_period_d1 <= local_period;
        end if;
    end process;
    local_period <= '1' when period_cnt = period_ticks-1 else '0';
    local_period_en <= local_period and not local_period_d1;
    
    ------------------------------------------------------------------
    -- Count the timeout and switch to the local timing pulse 
    -- when the external timing is not present
    ------------------------------------------------------------------
    tim_loss_cnt_p: process (sys_clk)
    begin
        if rising_edge (sys_clk) then
            if rst_sys_clk_n = '0' or timing_in = '1' then
                tim_loss_cnt <= 0;
            elsif clock_enable = '1' and tim_loss_cnt_done = '0' then
                tim_loss_cnt <= tim_loss_cnt + 1;
            end if;
        end if;
    end process;
    tim_loss_cnt_done <= '1' when tim_loss_cnt = timeout_ticks-1 else '0';
    
    timing_lost <= tim_loss_cnt_done;
    timing_mux <= timing_in when tim_loss_cnt_done = '0' else local_period_en;
	
	-- synchronise the output
	out_sync_p: process (sys_clk)
    begin
        if rising_edge (sys_clk) then
            timing_out <= timing_mux;
        end if;
    end process;
    
    
    
end architecture rtl;
