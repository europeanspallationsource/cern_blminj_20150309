------------------------------------------------------------------
--Design Unit :     Test bench unit for the Timing_circuits component
--                    
--
--
--Author:             Maciej Kwiatkowski
--                       European Organisation for Nuclear Research
--                      BE-BI-BL
--                      CERN, Geneva, Switzerland,  CH-1211
--                      865/R-A01
--
--Simulator:        Modelsim
------------------------------------------------------------------
--Vsn     Author    Date            Changes
--
--0.1    MK        12.06.2014        First version
------------------------------------------------------------------


library ieee;
use ieee.std_logic_1164.all;
use IEEE.NUMERIC_STD.ALL;
use ieee.math_real.all;
use STD.textio.all;
use IEEE.STD_LOGIC_TEXTIO.all;

entity TB_Timing_MUX is
end TB_Timing_MUX;

architecture behaviour of TB_Timing_MUX is

constant sys_clk_freq : natural := 40000000;
constant sys_clk_ns_period : natural := 1000000000/sys_clk_freq;


signal done_sim     : STD_LOGIC;
signal timing_out     : STD_LOGIC;
signal start_of_bp     : STD_LOGIC;
signal start_bp     : STD_LOGIC;
signal start_bp_mux     : STD_LOGIC;
signal timing_on     : STD_LOGIC;
signal timing_lost     : STD_LOGIC;

signal clk            : STD_LOGIC;
signal n_reset        : STD_LOGIC;



--------------------------------------------------------------------------------------------------------------------------------------
--------------------------------------------    Architecture begin  ------------------------------------------------------------------
--------------------------------------------------------------------------------------------------------------------------------------
begin

    DUT_i: entity work.Timing_MUX
    generic map
    (
        period_ticks    => 48001,
        timeout_ticks   => 52000
    )
    port map
    (   
        sys_clk             => clk,
        rst_sys_clk_n       => n_reset,
        clock_enable        => '1',
        timing_in           => start_bp_mux,
        timing_out          => timing_out,
        timing_lost         => timing_lost
    );


    -- clock
process
begin
loop
    clk<='1' ;
    wait for  12 ns;
    clk<='0';
    wait for 13 ns;
    if done_sim = '1' then
        wait;
    end if;
end loop;
end process;

-- basic period pulse emulation (shorter than the real one - 1.2 ms)
process
begin
loop
    start_bp <='1' ;
    wait for  25 ns;
    start_bp <='0';
    wait for 1200 us - 25 ns;
    if done_sim = '1' then
        wait;
    end if;
end loop;
end process;
start_bp_mux <= start_bp when timing_on = '1' else '0';

process
begin

    done_sim <= '0';
    timing_on <= '1';        
    n_reset <= '0';        
    wait for 100 ns;
    n_reset <= '1';
    
    wait until timing_out'event and timing_out='1';
    
    wait for 2 ms;
    timing_on <= '0';
    --wait until timing_out'event and timing_out='1';
    wait for 1600000 ns;
    timing_on <= '1';
    wait for 2400 us;
    timing_on <= '0';
    wait until timing_out'event and timing_out='1';
    
    done_sim <= '1';
    wait;

end process;



end behaviour;
