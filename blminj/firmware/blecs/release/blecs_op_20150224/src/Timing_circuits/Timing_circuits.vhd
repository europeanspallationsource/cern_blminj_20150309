------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    14/03/2014
--Module Name:    Timing_circuits
--Project Name:   Timing_circuits
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;

entity Timing_circuits is
generic(
	sys_clk_ns_period:	natural := 10
);
port(
	-- Input Clock
	sys_clk:		in std_logic;
	-- Reset input
	rst_sys_clk_n:	in std_logic;
	
	-- Lemo Inputs
	FP_LEMO2:				in std_logic;
	FP_LEMO1:				in std_logic;
	FP_LEMO3:				in std_logic;
	FP_LEMO4:				in std_logic;
	
	-- Lemo Outputs
	FP_LEMO6:				out std_logic;
	FP_LEMO5:				out std_logic;
	
	--Lemo control signals
	FP_LEMO_DIR:			out std_logic;
	FP_LEMO_OE:				out std_logic;
	
	--VME user pins
	VME_USER_P0A:			inout std_logic_vector(7 downto 0);
	VME_USER_P0A_DIR:		out std_logic;
	
	-- Status signals
	beam_in_msec: 			out std_logic_vector(31 downto 0);
	beam_out_msec: 			out std_logic_vector(31 downto 0);
	basic_period_msec: 		out std_logic_vector(31 downto 0);
    timing_lost:            out std_logic_vector(2 downto 0);   -- 0 - status of SoBP, 1 - status of Bin, 2 - status of Bout
	
	-- start of basic period strobe
	start_of_bp: 			out std_logic	
	
);
	
end entity Timing_circuits;


architecture rtl of Timing_circuits is

attribute keep: boolean;

-- lemo flip-flop signals
signal start_of_bp_d0 : std_logic;
signal beam_in : std_logic;
signal beam_out : std_logic;
attribute keep of start_of_bp_d0: signal is true;
attribute keep of beam_in: signal is true;
attribute keep of beam_out: signal is true;
signal start_of_bp_d1 : std_logic;
signal beam_in_d1 : std_logic;
signal beam_out_d1 : std_logic;
signal start_of_bp_en : std_logic;
signal beam_in_en : std_logic;
signal beam_out_en : std_logic;

-- microsecond counter signals
constant microsecond_ticks : natural := 1000/sys_clk_ns_period;
signal microsecond_cnt : integer range 0 to microsecond_ticks-1;
signal microsecond_en : std_logic;

-- timing counters and registers
signal bp_del_cnt : unsigned(20 downto 0);
signal beam_in_reg : unsigned(20 downto 0);
signal beam_out_reg : unsigned(20 downto 0);
signal basic_period_reg : unsigned(20 downto 0);

-- timing lost counter
type tim_cnt_array is array (0 to 1) of integer range 0 to 1300000;
signal tim_loss_cnt : tim_cnt_array;
signal tim_loss_cnt_done : std_logic_vector(1 downto 0);
signal tim_pulse : std_logic_vector(1 downto 0);
signal start_of_bp_local_en : std_logic;
attribute keep of start_of_bp_local_en: signal is true;


BEGIN
	
	------------------------------------------------------------------
	-- LEMO circuits
	------------------------------------------------------------------
	
	-- enable lemo buffer
	FP_LEMO_OE <= '0';
	-- set B to A direction of the buffer
	FP_LEMO_DIR <= '0';
	
	-- lemo double flip-flops
	status_per_p: process(sys_clk, rst_sys_clk_n)
	begin
		if rst_sys_clk_n = '0' then
			start_of_bp_d0 <= '0';
			beam_in <= '0';
			beam_out <= '0';
			start_of_bp_d1 <= '0';
			beam_in_d1 <= '0';
			beam_out_d1 <= '0';
		elsif rising_edge(sys_clk) then
			start_of_bp_d0 <= FP_LEMO2;
			start_of_bp_d1 <= start_of_bp_d0;
			beam_in <= FP_LEMO1;
			beam_in_d1 <= beam_in;
			beam_out <= FP_LEMO3;
			beam_out_d1 <= beam_out;
		end if;
	end process;
	
	--edge detectors
	start_of_bp_en <= start_of_bp_d0 and not start_of_bp_d1;
	beam_in_en <= beam_in and not beam_in_d1;
	beam_out_en <= beam_out and not beam_out_d1;
	
	--start of basic period strobe 
	--start_of_bp <= start_of_bp_en;
    
    ------------------------------------------------------------------
	-- Synchronise local counter to the external start of basic period pulse
    -- switch to the local start of basic period pulse
	------------------------------------------------------------------
    
    Timing_MUX_i : entity work.Timing_MUX
    generic map(
        period_ticks        => integer(real((1000000000/sys_clk_ns_period))*1.2),
        timeout_ticks       => integer(real((1000000000/sys_clk_ns_period))*1.3)
    )
    port map(
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        clock_enable    => '1',
        timing_in       => start_of_bp_en,
        timing_out      => start_of_bp_local_en,
        timing_lost     => timing_lost(0)
    );
    
    --start of basic period strobe 
	start_of_bp <= start_of_bp_local_en;
    
    ------------------------------------------------------------------
    -- Detect loss of beam in and beam out external timing pulses
	------------------------------------------------------------------
    
    tim_pulse(0) <= beam_in_en;
    tim_pulse(1) <= beam_out_en;
    
    tim_loss_gen: for I in 0 to 1 generate
        tim_loss_p: process (sys_clk)
        begin
            if rising_edge (sys_clk) then
                if rst_sys_clk_n = '0' or tim_pulse(I) = '1' then
                    tim_loss_cnt(I) <= 0;
                elsif microsecond_en = '1' and tim_loss_cnt_done(I) = '0' then
                    tim_loss_cnt(I) <= tim_loss_cnt(I) + 1;
                end if;
            end if;
        end process;
        tim_loss_cnt_done(I) <= '1' when tim_loss_cnt(I) >= 1300000 else '0';    
    end generate tim_loss_gen;
    
    --lost timing flags (0 - status of SoBP, 1 - status of Bin, 2 - status of Bout)
    timing_lost(2 downto 1) <= tim_loss_cnt_done(1 downto 0);
	
	------------------------------------------------------------------
	-- VME P0 circuits
	------------------------------------------------------------------
	
	-- set A to B direction of the P0A buffer
	VME_USER_P0A_DIR <= '1';
	
	VME_USER_P0A(0) <= start_of_bp_d0;
	VME_USER_P0A(1) <= beam_in;
	VME_USER_P0A(2) <= beam_out;
	
	
	------------------------------------------------------------------
	-- Measure beam-in, beam-out and basic period times
	------------------------------------------------------------------
	
	-- microsecond counter
	-- starts when start of BP pulse arrives
	us_cnt_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if microsecond_en = '1' or start_of_bp_local_en = '1' then
				microsecond_cnt <= 0;
			else
				microsecond_cnt <= microsecond_cnt + 1;
			end if;
		end if;
	end process;
	microsecond_en <= '1' when microsecond_cnt = microsecond_ticks-1 else '0';
	
	-- count microseconds after start of BP
	bp_del_cnt_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if start_of_bp_local_en = '1' then
				bp_del_cnt <= (others=>'0');
			elsif microsecond_en = '1' then
				bp_del_cnt <= bp_del_cnt + 1;
			end if;
		end if;
	end process;
	
	-- beam in delay register
	bin_reg_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if beam_in_en = '1' then
				beam_in_reg <= bp_del_cnt;
			end if;
		end if;
	end process;
	
	-- beam out delay register
	bout_reg_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if beam_out_en = '1' then
				beam_out_reg <= bp_del_cnt;
			end if;
		end if;
	end process;
	
	-- next basic period delay register
	bp_reg_p: process (sys_clk)
	begin
		if rising_edge (sys_clk) then
			if start_of_bp_local_en = '1' then
				basic_period_reg <= bp_del_cnt;
			end if;
		end if;
	end process;
	
	
	beam_in_msec <= "00000000000" & std_logic_vector(beam_in_reg);
	beam_out_msec <= "00000000000" & std_logic_vector(beam_out_reg);
	basic_period_msec <= "00000000000" & std_logic_vector(basic_period_reg);
	
	
end architecture rtl;
