------------------------------
--
--Company: CERN - BE/BI/BL
--Engineer: Maciej Kwiatkowski
--Create Date:    14/03/2014
--Module Name:    Status_registers
--Project Name:   Status_registers
--Description:   
--
--
------------------------------

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;
use IEEE.math_real.all;
use work.vhdl_func_pkg.all;

entity Status_registers is
generic(
    sys_clk_freq:       natural := 40000000;
    SYNTHESIS_YR:       integer := 0;   -- this should be set by the first.tcl script
    SYNTHESIS_M:        integer := 0;   -- this should be set by the first.tcl script
    SYNTHESIS_D:        integer := 0;   -- this should be set by the first.tcl script
    SYNTHESIS_HR:       integer := 0;   -- this should be set by the first.tcl script
    SYNTHESIS_MIN:      integer := 0    -- this should be set by the first.tcl script
);
port(
    -- Input Clock
    sys_clk:            in std_logic;
    rst_sys_clk_n:      in std_logic;
    
    -- VME registers signals
    synth_date_rd:      out std_logic_vector(31 downto 0);
    synth_time_rd:      out std_logic_vector(31 downto 0);
    serial_id_h_rd:     out std_logic_vector(31 downto 0);
    serial_id_l_rd:     out std_logic_vector(31 downto 0);
    status_bits_rd:     out std_logic_vector(31 downto 0);
    temperature_rd:     out std_logic_vector(31 downto 0);
    
    -- Start of basic period pulse
    start_of_bp:            in std_logic;
    
    -- status bits
    led_digpot_ack_error:   in std_logic_vector(1 downto 0);
    timing_lost:            in std_logic_vector(2 downto 0);
    
    -- DS2401 1 wire signal
    ModuleID:               inout std_logic;
    
    -- MAX6627 temperature sensor SPI interface
    Temp_CSn:               out std_logic;
    Temp_Clk:               out std_logic;
    Temp_data:              in std_logic

);
    
end entity Status_registers;


architecture rtl of Status_registers is

attribute keep: boolean;

signal DS2401_rdout_data : std_logic_vector(63 downto 0);
signal DS2401_rdout_data_en : std_logic;

BEGIN
    
    -- synthesis info register
    synth_date_rd <= std_logic_vector(to_unsigned(SYNTHESIS_D, 8)) & std_logic_vector(to_unsigned(SYNTHESIS_M, 8)) & std_logic_vector(to_unsigned(SYNTHESIS_YR, 16));
    synth_time_rd <= std_logic_vector(to_unsigned(0, 16)) & std_logic_vector(to_unsigned(SYNTHESIS_HR, 8)) & std_logic_vector(to_unsigned(SYNTHESIS_MIN, 8));
    
    -- status bits register
    status_bits_rd(31 downto 5) <= (others=>'0');
    status_bits_rd(4 downto 2) <= timing_lost;      -- lost timing flags (0 - status of SoBP, 1 - status of Bin, 2 - status of Bout)
    status_bits_rd(1) <= led_digpot_ack_error(1);   -- AD5263 ACK/NOACK
    status_bits_rd(0) <= led_digpot_ack_error(0);   -- PCF8575 ACK/NOACK
    
    
    -- instance of the DS2401 interface to read the ID number
    
    DS2411_i: entity work.DS2411
    generic map
    (
        Tclk_ns    => 1000000000/sys_clk_freq
    )
    port map
    (
        -- Internal Clock
        clk         => sys_clk,
        -- 1-WIRE
        DATA        => ModuleID,
        -- CMI Request Input
        rd_req_data => '0',
        rd_req_vld  => start_of_bp,
        rd_req_next => open,
        -- CMI Output
        rdout_data  => DS2401_rdout_data,
        rdout_vld   => DS2401_rdout_data_en,
        rdout_next  => '1'
    );
    
    -- register the DS2401 readout
    idreg_p: process(sys_clk)
    begin
        if rising_edge(sys_clk) then
            if rst_sys_clk_n = '0' then
                serial_id_h_rd <= (others=>'0');
                serial_id_l_rd <= (others=>'0');
            elsif DS2401_rdout_data_en = '1' then
                serial_id_h_rd <= DS2401_rdout_data(63 downto 32);
                serial_id_l_rd <= DS2401_rdout_data(31 downto 0);
            end if;
        end if;
    end process;
    
    -- instance of the MAX6627 interface to read the temperature
    
    MAX6627_i: entity work.MAX6627
    generic map
    (
        sys_clk_freq    => sys_clk_freq,
        spi_clk_freq    => 1000000
    )
    port map
    (
        -- Input Clock
        sys_clk         => sys_clk,
        rst_sys_clk_n   => rst_sys_clk_n,
        
        -- ADC control signals
        cnv_start       => start_of_bp,
        data            => temperature_rd(12 downto 0),
        data_rdy        => open,
        busy            => open,
        
        --SPI signals
        sclk            => Temp_Clk,
        din             => Temp_data,
        cs_n            => Temp_CSn
    );
    
    temperature_rd(31 downto 13) <= (others=>'0');
    
end architecture rtl;
